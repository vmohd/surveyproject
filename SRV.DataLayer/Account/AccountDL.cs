﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SRV.BaseLayer;
using SRV.BaseLayer.Account;
using SRV.Utility;

namespace SRV.DataLayer.Account
{
   public class AccountDL
    {
        #region Declaration
        DataSet dsContainer;
        DataTable dtContainer;
        #endregion

        #region Login_Load
        public DataTable Login_Load(AccountBase accountBase)
        {
            dsContainer = new DataSet();
            dtContainer = new DataTable();
            try
            {
                MyParameter[] myParams = {
                                           new MyParameter("@Email", accountBase.Email),
                                           new MyParameter("@Password", accountBase.Password),
                                         };
                Common.Set_Procedures("Login_Load");
                Common.Set_ParameterLength(2);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region ForgotPassword
        public DataTable ForgotPassword(AccountBase accountBase)
        {
            dtContainer = new DataTable();
            try
            {
                MyParameter[] myParams ={
                         new MyParameter("@Email",accountBase.Email)
                     };
                Common.Set_Procedures("ForgotPassword");
                Common.Set_Parameters(myParams);
                Common.Set_ParameterLength(1);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region ResetPassword
        public DataTable ResetPassword(AccountBase accountBase)
        {
            dtContainer = new DataTable();
            try
            {
                MyParameter[] myparams ={
                                            new MyParameter("@Guid",accountBase.UserGuid),
                                            new MyParameter("@NewPassword",accountBase.Password)
                                        };
                Common.Set_Procedures("ResetPassword");
                Common.Set_Parameters(myparams);
                Common.Set_ParameterLength(myparams.Length);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region get apps
        public DataTable get_app_Info()
        {
            dsContainer = new DataSet();
            dtContainer = new DataTable();
            try
            {
                MyParameter[] myParams = { };
                Common.Set_Procedures("get_app_Info");
                Common.Set_ParameterLength(0);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region Insert USer_app_Info
        public DataTable insert_user_app_Info(user_AppInfoBase userApps)
        {
            dtContainer = new DataTable();
            try
            {
                MyParameter[] myparams ={
                                            new MyParameter("@UserId",userApps.UserId),
                                            new MyParameter("@appid",userApps.appId)
                                        };
                Common.Set_Procedures("insert_user_app_Info");
                Common.Set_Parameters(myparams);
                Common.Set_ParameterLength(myparams.Length);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion
    }
}
