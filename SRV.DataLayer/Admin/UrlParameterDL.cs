﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using SRV.Utility;
using SRV.BaseLayer.Admin;

namespace SRV.DataLayer.Admin
{
    public class UrlParameterDL
    {
        #region Declaration
        DataSet dsContainer;
        DataTable dtContainer;
        #endregion


        #region UrlParameter_Insert
        public DataTable UrlParameter_Insert(UrlParameterBase parameterBase)
        {
            dtContainer = new DataTable();
            dsContainer = new DataSet();
            try
            {
                MyParameter[] myParams ={                                   
                                           new MyParameter("@ParameterName", parameterBase.ParameterName)
                                        };
                Common.Set_Procedures("UrlParameter_Insert");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region UrlParameter_Delete_ById
        public DataTable UrlParameter_Delete_ById(UrlParameterBase parameterBase)
        {
            dsContainer = new DataSet();
            dtContainer = new DataTable();
            try
            {
                MyParameter[] myParams = {
                                             new MyParameter("@Id",parameterBase.Id)
                                         };
                Common.Set_Procedures("UrlParameter_Delete_ById");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region UrlParameter_LoadAll
        public DataTable UrlParameter_LoadAll()
        {
            dsContainer = new DataSet();
            dtContainer = new DataTable();
            try
            {
                MyParameter[] myParams = { };
                Common.Set_Procedures("UrlParameters_LoadAll");
                Common.Set_ParameterLength(0);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion
    }
}
