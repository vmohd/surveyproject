﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SRV.BaseLayer.Admin;
using SRV.Utility;

namespace SRV.DataLayer.Admin
{
    public class AdminDL
    {
        #region Declaration
        DataSet dsContainer;
        DataTable dtContainer;
        #endregion

        #region Method Project_Insert_Update
        public DataTable Project_Insert_Update(ProjectBase project)
        {
            dtContainer = new DataTable();
            dsContainer = new DataSet();
            try
            {
                MyParameter[] myParams ={
                                           new MyParameter("@Id", project.Id),
                                           new MyParameter("@ProjectName", project.ProjectName),
                                           new MyParameter("@WelcomeMessage", project.WelcomeMessage),
                                           new MyParameter("@PageHeader", project.PageHeader),
                                           new MyParameter("@PageFooter", project.PageFooter),
                                           new MyParameter("@ProjectCompletionMesssage", project.ProjectCompletionMesssage),
                                           new MyParameter("@RedirectUrl", project.RedirectUrl),
                                           new MyParameter("@InvitationMessage", project.InvitationMessage),
                                           new MyParameter("@OwnerId", project.OwnerId),
                                           new MyParameter("@CreatedBy", project.CreatedBy),
                                           new MyParameter("@ModifiedBy", project.ModifiedBy),
                                           new MyParameter("@ProjectQuota", project.ProjectQuota),
                                           new MyParameter("@LanguageId", project.LanguageId),
                                           new MyParameter("@ProjectTerminationMessage",project.ProjectTerminationMessage),
                                           new MyParameter("@ProjectType",project.ProjectType),
                                           new MyParameter("@ProductProjectXML",project.ProductProjectXML),
                                           new MyParameter("@SuperProjectId",project.SuperProjectId),
                                            new MyParameter("@IsIncludeWelcomeMessage",project.IsIncludeWelcomeMessage),
                                            new MyParameter("@IsIncludePageHeader",project.IsIncludePageHeader),
                                            new MyParameter("@IsIncludePageFooter",project.IsIncludePageFooter),
                                            new MyParameter("@IsIncludeProjectCompletionMesssage",project.IsIncludeProjectCompletionMesssage),
                                            new MyParameter("@IsIncludeProjectTerminationMessage",project.IsIncludeTerminationMessage),
                                            new MyParameter("@RedirectParameters",project.RedirectParameters),
                                            new MyParameter("@IsRedirectParameter",project.IsRedirectParameter),
                                            new MyParameter("@IsIncludeFinishImage",project.IsIncludeFinishImage),
                                            new MyParameter("@CloseButtonText",project.CloseButtonText)
                                            //new MyParameter("@ProductMessage",project.ProductMessage)
                                        };
                Common.Set_Procedures("Project_Insert_Update");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region Method ClusterProject_Insert
        public DataTable ClusterProject_Insert(ProjectBase project)
        {
            dtContainer = new DataTable();
            dsContainer = new DataSet();
            try
            {
                MyParameter[] myParams ={
                                           new MyParameter("@CurrentProjectId", project.Id),
                                           new MyParameter("@ProjectTitle", project.ProjectName),                                          
                                           new MyParameter("@OwnerId", project.OwnerId),                                                
                                           new MyParameter("@ProjectType",project.ProjectType),
                                           new MyParameter("@ClustersCount",2),
                                           new MyParameter("@ResponseTable",project.ClusterTable),
                                           new MyParameter("@ReportType",project.ReportType)

                                        };
                Common.Set_Procedures("CreateClusterProject");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region Method NotParticipateRes_Insert
        public DataTable NotParticipateRes_Insert(ProjectBase project)
        {
            dtContainer = new DataTable();
            dsContainer = new DataSet();
            try
            {
                MyParameter[] myParams ={
                                           new MyParameter("@CurrentProjectId", project.Id),
                                           new MyParameter("@ProjectTitle", project.NotParticipateProjectName),
                                           new MyParameter("@OwnerId", project.OwnerId),
                                           new MyParameter("@ProjectType",4),
                                           new MyParameter("@ReportType",project.ReportType),
                                           new MyParameter("@NotParticipateRes",project.NotParticipateRespondent),
                                           new MyParameter("@NotParticipate","true")

                                        };
                Common.Set_Procedures("CreateNotParticipateProject");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region Method Query_Insert_Update
        public DataTable Query_Insert_Update(QueryBase query)
        {
            dtContainer = new DataTable();
            dsContainer = new DataSet();
            try
            {
                MyParameter[] myParams ={
                                           new MyParameter("@ProjectId", query.ProjectId),
                                           new MyParameter("@CaseName", query.CaseName),
                                           new MyParameter("@Query", query.Query),
                                           new MyParameter("@QueryHtml", query.QueryHtml),
                                           new MyParameter("@CaseID", query.CaseId),
                                        };
                Common.Set_Procedures("Query_Insert_Update");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region Method QueryUpdate
        public DataTable QueryUpdate(QueryBase query)
        {
            dtContainer = new DataTable();
            dsContainer = new DataSet();
            try
            {
                MyParameter[] myParams ={
                                           new MyParameter("@ProjectId", query.ProjectId),
                                           new MyParameter("@CaseName", query.CaseName),
                                           new MyParameter("@Query", query.Query),
                                           new MyParameter("@QueryHtml", query.QueryHtml),
                                        };
                Common.Set_Procedures("Query_Update");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region Method SuperProject_Insert_Update
        public DataTable SuperProject_Insert_Update(ProjectBase project)
        {
            dtContainer = new DataTable();
            dsContainer = new DataSet();
            try
            {
                MyParameter[] myParams ={
                                           new MyParameter("@Id", project.Id),
                                           new MyParameter("@ProjectName", project.SuperProjectName),
                                           new MyParameter("@ProjectOpeningDate", project.SuperProjectOpeningDate),
                                           new MyParameter("@ProjectClosingDate", project.SuperProjectClosingDate),
                                           new MyParameter("@IsRandomizeSurveys", project.SuperProjectIsRandomizeSurveys),
                                           new MyParameter("@LanguageId", project.LanguageId),
                                           new MyParameter("@OwnerId", project.OwnerId)
                                        };
                Common.Set_Procedures("SuperProject_Insert_Update");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region ProjectDelete_ById
        public DataTable ProjectDelete_ById(ProjectBase project)
        {
            dsContainer = new DataSet();
            dtContainer = new DataTable();
            try
            {
                MyParameter[] myParams = {
                                             new MyParameter("@ProjectId",project.Id)
                                         };
                Common.Set_Procedures("ProjectDelete_ById");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region QueryDelete_ByCase
        public DataTable QueryDelete_ByCase(QueryBase queryBase)
        {
            dsContainer = new DataSet();
            dtContainer = new DataTable();
            try
            {
                MyParameter[] myParams = {
                                             new MyParameter("@CaseName",queryBase.CaseName)
                                         };
                Common.Set_Procedures("QueryDelete_ByCase");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region Method ProjectSettings_InsertUpdate
        public DataTable ProjectSettings_InsertUpdate(ProjectSettingsBase projectSettings)
        {
            dtContainer = new DataTable();
            dsContainer = new DataSet();
            try
            {
                MyParameter[] myParams ={
                                           new MyParameter("@ProjectId", projectSettings.ProjectId),
                                           new MyParameter("@OpeningDate", projectSettings.OpeningDate),
                                           new MyParameter("@ClosingDate", projectSettings.ClosingDate),
                                           new MyParameter("@DisableQuestionNumbering", projectSettings.DisableQuestionNumbering),
                                           new MyParameter("@IsActive", projectSettings.IsActive),
                                           new MyParameter("@Scored", projectSettings.Scored),
                                           new MyParameter("@PrevNextPageNavigation", projectSettings.PrevNextPageNavigation),
                                           new MyParameter("@ResumeOfProgress", projectSettings.ResumeOfProgress),
                                           new MyParameter("@TestMode",projectSettings.TestMode),
                                           new MyParameter("@IsQuesRandomize",projectSettings.IsQuesRandomize),
                                           new MyParameter("@IsProductRandomize",projectSettings.IsProductRandomize)
                                        };
                Common.Set_Procedures("ProjectSettings_InsertUpdate");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region ProjectSettings_LoadById
        public DataTable ProjectSettings_LoadById(ProjectSettingsBase projectSettings)
        {
            dsContainer = new DataSet();
            dtContainer = new DataTable();
            try
            {
                MyParameter[] myParams = {
                                             new MyParameter("@ProjectId",projectSettings.ProjectId)
                                         };
                Common.Set_Procedures("ProjectSettings_LoadById");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region Method ProjectQuotaOtherInsertUpdate_ByProjectId
        public DataTable ProjectQuotaOtherInsertUpdate_ByProjectId(ProjecQuotaBase projectQuotaBase)
        {
            dtContainer = new DataTable();
            dsContainer = new DataSet();
            try
            {
                MyParameter[] myParams ={
                                              new MyParameter("@xml", projectQuotaBase.OpenXml),
                                              new MyParameter("@UserId",projectQuotaBase.Id),
                                              new MyParameter("@ProjectQuota",projectQuotaBase.ProjectQuota),
                                              new MyParameter("@PrjctId",projectQuotaBase.ProjectId),
                                              new MyParameter("@QuotaQues",projectQuotaBase.QuotaQues)
                                        };
                Common.Set_Procedures("ProjectQuotaOtherInsertUpdate_ByProjectId");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region method Register_InsertUpdate
        public DataTable Register_InsertUpdate(AdminBase adminBaseReg)
        {
            dtContainer = new DataTable();
            dsContainer = new DataSet();
            try
            {

                MyParameter[] myParams ={
                                            new MyParameter("@Id",adminBaseReg.Id),
                                            new MyParameter("@UserGuid",adminBaseReg.UserGuid),
                                            new MyParameter("@FirstName",adminBaseReg.FirstName),
                                            new MyParameter("@LastName",adminBaseReg.LastName),
                                            new MyParameter("@Email",adminBaseReg.Email),
                                            new MyParameter("@Gender",adminBaseReg.Gender),
                                            new MyParameter("@Password",adminBaseReg.Password),
                                            new MyParameter("@Address",adminBaseReg.Address),
                                            new MyParameter("@City",adminBaseReg.City),
                                            new MyParameter("@CountryId",adminBaseReg.CountryId),
                                            new MyParameter("@StateId",adminBaseReg.StateId),
                                            new MyParameter("@ZipCode",adminBaseReg.ZipCode),
                                            new MyParameter("@RoleId",adminBaseReg.RoleId),
                                            new MyParameter("@SecurityQuestion",adminBaseReg.SecurityQuestion),
                                            new MyParameter("@AnswerToSecurityQuestion",adminBaseReg.AnswerToSecurityQuestion),
                                            new MyParameter("@CountryOfResidenceId",adminBaseReg.CountryOfResidenceId),
                                            new MyParameter("@LanguageId",adminBaseReg.LanguageId),
                                            new MyParameter("@CompanyName",adminBaseReg.CompanyName),
                                            new MyParameter("@PhoneNumber",adminBaseReg.PhoneNumber),
                                            new MyParameter("@sendEmail",adminBaseReg.SendEmail),
                                            new MyParameter("@OtherCommunications",adminBaseReg.OtherCommunications)
                               };
                Common.Set_Procedures("Register_InsertUpdate");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region AdminRegistration_Confirm
        public DataTable AdminRegistration_Confirm(AdminBase adminBase)
        {
            dsContainer = new DataSet();
            dtContainer = new DataTable();
            try
            {
                MyParameter[] myParams ={
                                        new MyParameter("@UserGuid", adminBase.UserGuid)
                                       };
                Common.Set_Procedures("AdminRegistration_Confirm");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region Country_LoadAll
        public DataTable Country_LoadAll()
        {
            dsContainer = new DataSet();
            dtContainer = new DataTable();
            try
            {
                MyParameter[] myParams = { };
                Common.Set_Procedures("Country_LoadAll");
                Common.Set_ParameterLength(0);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region State_LoadAll
        public DataTable State_LoadAll(AdminBase adminBase)
        {
            dsContainer = new DataSet();
            dtContainer = new DataTable();
            try
            {
                MyParameter[] myParams = {
                                             new MyParameter("@CountryID",adminBase.CountryId)
                                         };
                Common.Set_Procedures("State_LoadAll");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region Poject_LoadAll
        public DataSet Poject_LoadAll(AdminBase adminBase)
        {
            dsContainer = new DataSet();
            dtContainer = new DataTable();
            try
            {
                MyParameter[] myParams = {
                                             new MyParameter("@OwnerId",adminBase.Id)
                                         };
                Common.Set_Procedures("Project_LoadAll");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dsContainer = Common.Execute_Procedures_Select();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dsContainer;
        }
        #endregion

        #region Project_LoadById
        public DataTable Project_LoadById(ProjectBase projectBase)
        {
            dsContainer = new DataSet();
            dtContainer = new DataTable();
            try
            {
                MyParameter[] myParams = {
                                             new MyParameter("@Id",projectBase.Id)
                                         };
                Common.Set_Procedures("Project_LoadById");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region Method Question_Insert
        public DataTable Question_Insert(QuestionBase questionBase)
        {
            dtContainer = new DataTable();
            dsContainer = new DataSet();
            try
            {
                MyParameter[] myParams ={
                                           new MyParameter("@ProjectId", questionBase.ProjectId),
                                           new MyParameter("@QuestionTitle", questionBase.QuestionTitle),
                                           new MyParameter("@QuestionTypeId", questionBase.QuestionTypeId),
                                           new MyParameter("@CreatedBy", questionBase.CreatedBy),
                                           new MyParameter("@RedirectUrl",questionBase.RedirectUrl),
                                           new MyParameter("@optionsCaption",questionBase.OptionsCaption),
                                           new MyParameter("@columnXml",questionBase.ColumnXml),
                                           new MyParameter("@ddlXml",questionBase.DDlXml),
                                           new MyParameter("@questionTag",questionBase.QuestionTag),
                                           new MyParameter("@isAnswersExplicating",questionBase.isAnswersExplicating),
                                           new MyParameter("@isAnswersPiped",questionBase.isAnswersPiped),
                                           new MyParameter("@ExplicatingFrom",questionBase.ExplicatingFrom),
                                           new MyParameter("@PipedFrom",questionBase.PipedFrom),
                                           new MyParameter("@DisplayXml",questionBase.DispOrderXml),
                                           new MyParameter("@isOptRandomize",questionBase.IsOptRandomize),
                                           new MyParameter("@redirectPassCode",questionBase.RedirectPassCode),
                                           new MyParameter("@redirectQuesParameters",questionBase.RedirectQuesParameters),
                                            new MyParameter("@IsRedirectParameter",questionBase.IsRedirectParameter)
                                        };
                Common.Set_Procedures("Question_Insert");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region Method Question_Update
        public DataTable Question_Update(QuestionBase questionBase)
        {
            dtContainer = new DataTable();
            dsContainer = new DataSet();
            try
            {
                MyParameter[] myParams ={
                                           new MyParameter("@QuestionId", questionBase.QusetionId),
                                           new MyParameter("@QuestionTitle", questionBase.QuestionTitle),
                                           new MyParameter("@QuestionTypeId", questionBase.QuestionTypeId),
                                           new MyParameter("@ModifiedBy", questionBase.ModifiedBy),
                                           new MyParameter("@RedirectUrl",questionBase.RedirectUrl),
                                           new MyParameter("@optionsCaption",questionBase.OptionsCaption),
                                           new MyParameter("@columnXml",questionBase.ColumnXml),
                                           new MyParameter("@ddlXml",questionBase.DDlXml),
                                           new MyParameter("@questionTag",questionBase.QuestionTag),
                                           new MyParameter("@isAnswersExplicating",questionBase.isAnswersExplicating),
                                           new MyParameter("@isAnswersPiped",questionBase.isAnswersPiped),
                                           new MyParameter("@ExplicatingFrom",questionBase.ExplicatingFrom),
                                           new MyParameter("@PipedFrom",questionBase.PipedFrom),
                                           new MyParameter("@DisplayXml",questionBase.DispOrderXml),
                                           new MyParameter("@isOptRandomize",questionBase.IsOptRandomize),
                                           new MyParameter("@redirectPassCode",questionBase.RedirectPassCode),
                                           new MyParameter("@redirectQuesParameters",questionBase.RedirectQuesParameters),
                                           new MyParameter("@IsRedirectParameter",questionBase.IsRedirectParameter)
                                        };
                Common.Set_Procedures("Question_Update");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region QuotaId_LoadByProjectId
        public DataTable QuotaId_LoadByProjectId(QuestionBase questionBase)
        {
            dsContainer = new DataSet();
            dtContainer = new DataTable();
            try
            {
                MyParameter[] myParams = {
                                             new MyParameter("@ProjectId",questionBase.ProjectId)
                                         };
                Common.Set_Procedures("QuotaId_LoadByProjectId");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region Option_Insert
        public DataTable Option_Insert(QuestionBase questionBase)
        {
            dsContainer = new DataSet();
            dtContainer = new DataTable();
            try
            {
                MyParameter[] myParams = {
                                             new MyParameter("@options",questionBase.Xml),
                                             new MyParameter("@projectId",questionBase.ProjectId),
                                             new MyParameter("@questionTypeId",questionBase.QuestionTypeId),
                                             new MyParameter("@IsMatrix",questionBase.IsMatrix)
                                         };
                Common.Set_Procedures("Option_Insert");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region Qusetion_LoadAllByProjectId
        public DataTable Qusetion_LoadAllByProjectId(QuestionBase questionBase)
        {
            dsContainer = new DataSet();
            dtContainer = new DataTable();
            try
            {
                MyParameter[] myParams = {
                                             new MyParameter("@projectId",questionBase.ProjectId)
                                         };
                Common.Set_Procedures("Qusetion_LoadAllByProjectId");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region Query_LoadAllByProjectId
        public DataTable Query_LoadAllByProjectId(QueryBase queryBase)
        {
            dsContainer = new DataSet();
            dtContainer = new DataTable();
            try
            {
                MyParameter[] myParams = {
                                             new MyParameter("@ProjectId",queryBase.ProjectId)
                                         };
                Common.Set_Procedures("Query_LoadAllByProjectId");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region Query_LoadAllBySuperProjectId
        public DataTable Query_LoadAllBySuperProjectId(QueryBase queryBase)
        {
            dsContainer = new DataSet();
            dtContainer = new DataTable();
            try
            {
                MyParameter[] myParams = {
                                             new MyParameter("@SuperProjectId",queryBase.SuperProjectId)
                                         };
                Common.Set_Procedures("Query_LoadAllBySuperProjectId");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region DeleteQuestion_ById
        public DataTable DeleteQuestion_ById(QuestionBase questionBase)
        {
            dsContainer = new DataSet();
            dtContainer = new DataTable();
            try
            {
                MyParameter[] myParams = {
                                             new MyParameter("@questionId",questionBase.Id)
                                         };
                Common.Set_Procedures("DeleteQuestion_ById");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region Qusetion_Option_LoadAllById
        public DataTable Qusetion_Option_LoadAllById(QuestionBase questionBase)
        {
            dsContainer = new DataSet();
            dtContainer = new DataTable();
            try
            {
                MyParameter[] myParams = {
                                             new MyParameter("@qusetionId",questionBase.QusetionId)

                                         };
                Common.Set_Procedures("Qusetion_Option_LoadAllById");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region Option_LoadAllById
        public DataTable Option_LoadAllById(QuestionBase questionBase)
        {
            dsContainer = new DataSet();
            dtContainer = new DataTable();
            try
            {
                MyParameter[] myParams = {
                                             new MyParameter("@qusetionId",questionBase.Id)

                                         };
                Common.Set_Procedures("Option_LoadAllById");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region Method Question_Update_ById
        public DataTable Question_Update_ById(QuestionBase questionBase)
        {
            dtContainer = new DataTable();
            dsContainer = new DataSet();
            try
            {
                MyParameter[] myParams ={
                                            new MyParameter("@questionId",questionBase.QusetionId),
                                           new MyParameter("@ProjectId", questionBase.ProjectId),
                                           new MyParameter("@QuestionTitle", questionBase.QuestionTitle),
                                           new MyParameter("@QuestionTypeId", questionBase.QuestionTypeId),
                                             new MyParameter("@CreatedBy", questionBase.CreatedBy),
                                             new MyParameter("@ConstantSum",questionBase.ConstantSum),
                                             new MyParameter("@RangeMin",questionBase.RangeMin),
                                             new MyParameter("@RangeMax",questionBase.RangeMax)
                                        };
                Common.Set_Procedures("Question_Update_ById");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region QuestionType_LoadAll
        public DataTable QuestionType_LoadAll()
        {
            dsContainer = new DataSet();
            dtContainer = new DataTable();
            try
            {
                MyParameter[] myParams = { };
                Common.Set_Procedures("QuestionType_LoadAll");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region QuestionsLoadAll_ByProjectId
        public DataTable QuestionsLoadAll_ByProjectId(QuestionBase question)
        {
            dsContainer = new DataSet();
            dtContainer = new DataTable();
            try
            {
                MyParameter[] myParams = {
                                         new MyParameter("ProjectId",question.ProjectId)
                                         };
                Common.Set_Procedures("QuestionsLoadAll_ByProjectId");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region QuestionsType_ByQuestionId
        public DataTable QuestionsType_ByQuestionId(QuestionBase question)
        {
            dsContainer = new DataSet();
            dtContainer = new DataTable();
            try
            {
                MyParameter[] myParams = {
                                         new MyParameter("QusetionId",question.QusetionId)
                                         };
                Common.Set_Procedures("QuestionsType_ByQuestionId");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region Method QuestionUpdate_ByQuestionId
        public DataTable QuestionUpdate_ByQuestionId(QuestionBase questionBase)
        {
            dtContainer = new DataTable();
            dsContainer = new DataSet();
            try
            {
                MyParameter[] myParams ={
                                           new MyParameter("@QuestionId", questionBase.QusetionId),
                                           new MyParameter("@ModifiedBy", questionBase.ModifiedBy),
                                           new MyParameter("@RangeMax", questionBase.RangeMax),
                                             new MyParameter("@RangeMin", questionBase.RangeMin),
                                            new MyParameter("@ConstantSum",questionBase.ConstantSum)
                                        };
                Common.Set_Procedures("QuestionUpdate_ByQuestionId");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region Method UserInfo_LoadById
        public DataTable UserInfo_LoadById(UserInfoBase userInfo)
        {
            dtContainer = new DataTable();
            dsContainer = new DataSet();
            try
            {
                MyParameter[] myParams ={
                                           new MyParameter("@Id", userInfo.Id)
                                        };
                Common.Set_Procedures("UserInfo_LoadById");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region InsertMatrix_Column
        public DataTable InsertMatrix_Column(QuestionBase questionBase)
        {
            dsContainer = new DataSet();
            dtContainer = new DataTable();
            try
            {
                MyParameter[] myParams = {
                                             new MyParameter("@Type",questionBase.Type),
                                             new MyParameter("@ColumnXml",questionBase.ColumnXml)
                                         };
                Common.Set_Procedures("InsertMatrix_Column");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region ProjectStatistics_LoadById
        public DataTable ProjectStatistics_LoadById(AdminBase adminBase)
        {
            dsContainer = new DataSet();
            dtContainer = new DataTable();
            try
            {
                MyParameter[] myParams = {
                                             new MyParameter("@OwnerId",adminBase.Id),
                                             new MyParameter ("@Id",adminBase.ProjectId)
                                         };
                Common.Set_Procedures("ProjectStatistics_LoadById");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region Question_LoadById
        public DataSet Question_LoadById(QuestionBase question)
        {
            dsContainer = new DataSet();
            dtContainer = new DataTable();
            try
            {
                MyParameter[] myParams = {
                                         new MyParameter("@QuestionId",question.Id)
                                         };
                Common.Set_Procedures("Question_LoadById");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dsContainer = Common.Execute_Procedures_Select();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dsContainer;
        }
        #endregion

        #region Answers_LoadAllByQuestionId
        public DataSet Answers_LoadAllByQuestionId(QuestionBase question)
        {
            dsContainer = new DataSet();
            dtContainer = new DataTable();
            try
            {
                MyParameter[] myParams = {
                                         new MyParameter("@QuestionId",question.QusetionId),
                                         new MyParameter("@QuestionTypeId",question.QuestionTypeId)
                                         };
                Common.Set_Procedures("Answers_LoadAllByQuestionId");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dsContainer = Common.Execute_Procedures_Select();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dsContainer;
        }
        #endregion

        #region Method DeleteOptionsBeforeUpdate_ByQuestionId
        public DataTable DeleteOptionsBeforeUpdate_ByQuestionId(QuestionBase questionBase)
        {
            dtContainer = new DataTable();
            dsContainer = new DataSet();
            try
            {
                MyParameter[] myParams ={
                                           new MyParameter("@QuestionId", questionBase.QusetionId),
                                           new MyParameter("@QuestionTypeId", questionBase.QuestionTypeId)
                                        };
                Common.Set_Procedures("DeleteOptionsBeforeUpdate_ByQuestionId");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region ProjectQuota_LoadById
        public DataTable ProjectQuota_LoadById(ProjecQuotaBase projectQuotaBase)
        {
            dsContainer = new DataSet();
            dtContainer = new DataTable();
            try
            {
                MyParameter[] myParams = {
                                         new MyParameter("@Id",projectQuotaBase.ProjectId)
                                         };
                Common.Set_Procedures("ProjectQuota_LoadById");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region ProjectQuotaOther_LoadById
        public DataTable ProjectQuotaOther_LoadById(ProjecQuotaBase projectQuotaBase)
        {
            dsContainer = new DataSet();
            dtContainer = new DataTable();
            try
            {
                MyParameter[] myParams = {
                                         new MyParameter("@Id",projectQuotaBase.ProjectId)
                                         };
                Common.Set_Procedures("ProjectQuotaOther_LoadById");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region Method ProjectInvitation_Insert_Update
        public DataTable ProjectInvitation_Insert_Update(ProjectInvitationBase projectInvitation)
        {
            dtContainer = new DataTable();
            dsContainer = new DataSet();
            try
            {
                MyParameter[] myParams ={
                                           new MyParameter("@EmailXml", projectInvitation.EmailXml)
                                        };
                Common.Set_Procedures("ProjectInvitation_Insert_Update");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region Method ProjectClone_ByProjectId
        public DataTable ProjectClone_ByProjectId(AdminBase adminBase)
        {
            dtContainer = new DataTable();
            dsContainer = new DataSet();
            try
            {
                MyParameter[] myParams ={
                                           new MyParameter("@OldProjectId", adminBase.ProjectId)
                                        };
                Common.Set_Procedures("ProjectClone_ByProjectId");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region Method ProjectQuestion_UpdateDisplayOrder
        public DataTable ProjectQuestion_UpdateDisplayOrder(QuestionBase quesBase)
        {
            dtContainer = new DataTable();
            dsContainer = new DataSet();
            try
            {
                MyParameter[] myParams ={
                                           new MyParameter("@xml", quesBase.ColumnXml),
                                           new MyParameter("@userId",quesBase.ModifiedBy),
                                            new MyParameter("@Action",quesBase.Action)
                                        };
                Common.Set_Procedures("ProjectQuestion_UpdateDisplayOrder");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region Method QuestionOptions_UpdateDisplayOrder
        public DataTable QuestionOptions_UpdateDisplayOrder(QuestionBase quesBase)
        {
            dtContainer = new DataTable();
            dsContainer = new DataSet();
            try
            {
                MyParameter[] myParams ={
                                           new MyParameter("@xml", quesBase.ColumnXml)

                                        };
                Common.Set_Procedures("QuestionOptions_UpdateDisplayOrder");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region RespondersLoad_ByProjectId
        public DataTable RespondersLoad_ByProjectId(ProjectBase projectBase)
        {
            dsContainer = new DataSet();
            dtContainer = new DataTable();
            try
            {
                MyParameter[] myParams = {
                                         new MyParameter("@ProjectId",projectBase.Id),
                                         new MyParameter("@SuperProjectId",projectBase.SuperProjectId)
                                         };
                Common.Set_Procedures("RespondersLoad_ByProjectId");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region Responders_Offline_LoadBy_ProjectId
        public DataTable Responders_Offline_LoadBy_ProjectId(ProjectBase projectBase)
        {
            dsContainer = new DataSet();
            dtContainer = new DataTable();
            try
            {
                MyParameter[] myParams = {
                                         new MyParameter("@ProjectId",projectBase.Id)
                                         };
                Common.Set_Procedures("Responders_Offline_LoadBy_ProjectId");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region Response_DeleteByIds
        public DataTable Response_DeleteByIds(SRV.BaseLayer.Respondent.SurveyResponseBase surveyResponseBase)
        {
            dsContainer = new DataSet();
            dtContainer = new DataTable();
            try
            {
                MyParameter[] myParams = {
                                         new MyParameter("@ProjectId",surveyResponseBase.ProjectId),
                                         new MyParameter("@mode",surveyResponseBase.Mode),
                                         new MyParameter("@responderId",(surveyResponseBase.Mode == "online" ? surveyResponseBase.Email : surveyResponseBase.ResponderId))
                                         };
                Common.Set_Procedures("Response_DeleteByIds");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region Language_LoadAll
        public DataTable Language_LoadAll()
        {
            dsContainer = new DataSet();
            dtContainer = new DataTable();
            try
            {
                MyParameter[] myParams = { };
                Common.Set_Procedures("Language_LoadAll");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region ProjectQuestionDetails_LoadByQuestionid
        public DataTable ProjectQuestionDetails_LoadByQuestionid(QuestionBase questionBase)
        {
            dsContainer = new DataSet();
            dtContainer = new DataTable();
            try
            {
                MyParameter[] myParams = {
                                         new MyParameter("@Questionid",questionBase.QusetionId)
                                         };
                Common.Set_Procedures("ProjectQuestionDetails_LoadByQuestionid");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region ChangePassword_ById
        public DataTable ChangePassword_ById(AdminBase adminBase)
        {
            dsContainer = new DataSet();
            dtContainer = new DataTable();
            try
            {
                MyParameter[] myParams = {
                                          new MyParameter("@UserId",adminBase.Id),
                                           new MyParameter("@Password",adminBase.Password)
                                         };
                Common.Set_Procedures("ChangePassword_ById");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region RoleNames_byUserId
        public DataTable ActionNames_byUserId(AdminBase adminBase)
        {
            dsContainer = new DataSet();
            dtContainer = new DataTable();
            try
            {
                MyParameter[] myParams ={
                                        new MyParameter("@Id", adminBase.Id)
                                       };
                Common.Set_Procedures("ActionNames_byUserId");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region Project_LoadByClosingDate
        public DataTable Project_LoadByClosingDate()
        {
            dsContainer = new DataSet();
            dtContainer = new DataTable();
            try
            {
                MyParameter[] myParams = { };
                Common.Set_Procedures("Project_LoadByClosingDate");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region CommonMethodQuestionGroups
        public DataTable CommonMethodQuestionGroups(ProjectQuestionGroupBase groupBase)
        {
            dsContainer = new DataSet();
            dtContainer = new DataTable();
            try
            {
                MyParameter[] myParams = {
                                          new MyParameter("@Id",groupBase.Id),
                                           new MyParameter("@Action",groupBase.Action),
                                           new MyParameter("@GroupName",groupBase.GroupName),
                                           new MyParameter("@GroupDescription",groupBase.GroupDescription),
                                           new MyParameter("@ProjectId",groupBase.ProjectId)
                                         };
                Common.Set_Procedures("CreateUpdateEditQuestionGroups");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region QuestionGroupsLoadAll_ByProjectId
        public DataTable QuestionGroupsLoadAll_ByProjectId(ProjectQuestionGroupBase groupBase)
        {
            dsContainer = new DataSet();
            dtContainer = new DataTable();
            try
            {
                MyParameter[] myParams = {
                                         new MyParameter("@ProjectId",groupBase.ProjectId)
                                         };
                Common.Set_Procedures("QuestionGroupsLoadAll_ByProjectId");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region DeleteQuestionGroup_ById
        public DataTable DeleteQuestionGroup_ById(ProjectQuestionGroupBase grpBase)
        {
            dsContainer = new DataSet();
            dtContainer = new DataTable();
            try
            {
                MyParameter[] myParams = {
                                             new MyParameter("@Id",grpBase.Id)
                                         };
                Common.Set_Procedures("DeleteQuestionGroup_ById");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region UpdateQuestionGroups_byProjectId
        public DataSet UpdateQuestionGroups_byProjectId(ProjectQuestionGroupBase groupBase)
        {
            dsContainer = new DataSet();
            dtContainer = new DataTable();
            try
            {
                MyParameter[] myParams = {
                                          new MyParameter("@ProjectId",groupBase.ProjectId),
                                         new MyParameter("@groupXml",groupBase.GroupXml)
                                         };
                Common.Set_Procedures("UpdateQuestionGroups_byProjectId");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dsContainer = Common.Execute_Procedures_Select();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dsContainer;
        }
        #endregion

        #region ProjectQuestionGroups_UpdateDisplayOrder
        public DataTable ProjectQuestionGroups_UpdateDisplayOrder(ProjectQuestionGroupBase groupBase)
        {
            dsContainer = new DataSet();
            dtContainer = new DataTable();
            try
            {
                MyParameter[] myParams = {
                                         new MyParameter("@xml",groupBase.GroupXml)
                                         };
                Common.Set_Procedures("ProjectQuestionGroups_UpdateDisplayOrder");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region ProductProject_LoadById
        public DataTable ProductProject_LoadById(ProductProjectBase productProjectBase)
        {
            dsContainer = new DataSet();
            dtContainer = new DataTable();
            try
            {
                MyParameter[] myParams = {
                                             new MyParameter("@ProjectId",productProjectBase.ProjectId)
                                         };
                Common.Set_Procedures("ProductProject_LoadById");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region LinkQuestion_Insert
        public DataTable LinkQuestion_Insert(ProductProjectBase productProjectBase)
        {
            dsContainer = new DataSet();
            dtContainer = new DataTable();
            try
            {
                MyParameter[] myParams = {
                                             new MyParameter("@xml",productProjectBase.Xml)
                                         };
                Common.Set_Procedures("LinkQuestion_Insert");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region SurveyLink_Insert
        public DataTable SurveyLink_Insert(ProductProjectBase productProjectBase)
        {
            dsContainer = new DataSet();
            dtContainer = new DataTable();
            try
            {
                MyParameter[] myParams = {
                                             new MyParameter("@xml",productProjectBase.Xml)
                                         };
                Common.Set_Procedures("SurveyLink_Insert");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region Qusetion_LoadAllByProductId
        public DataTable Qusetion_LoadAllByProductId(QuestionBase questionBase)
        {
            dsContainer = new DataSet();
            dtContainer = new DataTable();
            try
            {
                MyParameter[] myParams = {
                                             new MyParameter("@productId",questionBase.Id)
                                         };
                Common.Set_Procedures("Qusetion_LoadAllByProductId");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region LinkProduct_LoadAll
        public DataTable LinkProduct_LoadAll(AdminBase adminBase)
        {
            dsContainer = new DataSet();
            dtContainer = new DataTable();
            try
            {
                MyParameter[] myParams = {
                                             new MyParameter("@OwnerId",adminBase.Id)
                                         };
                Common.Set_Procedures("LinkProduct_LoadAll");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region PositionTable_Insert
        public DataTable PositionTable_Insert(PositionTable positionTable)
        {
            dsContainer = new DataSet();
            dtContainer = new DataTable();
            try
            {
                MyParameter[] myParams = {
                                             new MyParameter("@responderId",positionTable.ResponderId),
                                             new MyParameter("@surveyId",positionTable.SurveyId),
                                             new MyParameter("@surveyDisplayOrder",positionTable.SurveyDispOrder),
                                             new MyParameter("@productXml",positionTable.ProductsXML),
                                             new MyParameter("@superSurveyId",positionTable.SuperSurveyId)
                                         };
                Common.Set_Procedures("PositionTable_Insert");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region SuperProject_LoadById
        public DataTable SuperProject_LoadById(ProjectBase projectBase)
        {
            dsContainer = new DataSet();
            dtContainer = new DataTable();
            try
            {
                MyParameter[] myParams = {
                                             new MyParameter("@Id",projectBase.Id)
                                         };
                Common.Set_Procedures("SuperProject_LoadById");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region Method SubProjectsRedirection_Update
        public DataTable SubProjectsRedirection_Update(ProjectInvitationBase projectInvitation)
        {
            dtContainer = new DataTable();
            dsContainer = new DataSet();
            try
            {
                MyParameter[] myParams ={
                                           new MyParameter("@RedirectionXml", projectInvitation.EmailXml)
                                        };
                Common.Set_Procedures("SubProjectsRedirection_Update");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region SuperProjectDelete_ById
        public DataTable SuperProjectDelete_ById(ProjectBase project)
        {
            dsContainer = new DataSet();
            dtContainer = new DataTable();
            try
            {
                MyParameter[] myParams = {
                                             new MyParameter("@SuperProjectId",project.SuperProjectId)
                                         };
                Common.Set_Procedures("SuperProjectDelete_ById");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region PositionTable_LoadBySuperSurveyId
        public DataTable PositionTable_LoadBySuperSurveyId(PositionTable positionBase)
        {
            dsContainer = new DataSet();
            dtContainer = new DataTable();
            try
            {
                MyParameter[] myParams = {
                                             new MyParameter("@SuperSurveyId",positionBase.SuperSurveyId),
                                             new MyParameter("@ResponderId",positionBase.ResponderId)
                                         };
                Common.Set_Procedures("PositionTable_LoadBySuperSurveyId");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region RespondersLoad_BySuperSurveyId
        public DataTable RespondersLoad_BySuperSurveyId(PositionTable positionBase)
        {
            dsContainer = new DataSet();
            dtContainer = new DataTable();
            try
            {
                MyParameter[] myParams = {
                                             new MyParameter("@SuperSurveyId",positionBase.SuperSurveyId)
                                         };
                Common.Set_Procedures("RespondersLoad_BySuperSurveyId");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region PositionTableSurvey_UpdateDisplayOrder
        public DataTable PositionTableSurvey_UpdateDisplayOrder(PositionTable positionBase)
        {
            dsContainer = new DataSet();
            dtContainer = new DataTable();
            try
            {
                MyParameter[] myParams = {
                                         new MyParameter("@xml",positionBase.OrderXml),
                                         new MyParameter("@responderId",positionBase.ResponderId)
                                         };
                Common.Set_Procedures("PositionTableSurvey_UpdateDisplayOrder");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region Products_LoadBySuperSurveyId
        public DataSet Products_LoadBySuperSurveyId(PositionTable positionBase)
        {
            dsContainer = new DataSet();
            dtContainer = new DataTable();
            try
            {
                MyParameter[] myParams = {
                                             new MyParameter("@SuperSurveyId",positionBase.SuperSurveyId),
                                             new MyParameter("@ResponderId",positionBase.ResponderId)
                                         };
                Common.Set_Procedures("Products_LoadBySuperSurveyId");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dsContainer = Common.Execute_Procedures_Select();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dsContainer;
        }
        #endregion

        #region Method Product_UpdateDisplayOrder
        public DataTable Product_UpdateDisplayOrder(PositionTable positionBase)
        {
            dtContainer = new DataTable();
            dsContainer = new DataSet();
            try
            {
                MyParameter[] myParams ={
                                           new MyParameter("@xml", positionBase.OrderXml),
                                           new MyParameter("@responderId",positionBase.ResponderId)
                                        };
                Common.Set_Procedures("Product_UpdateDisplayOrder");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region InsertUpdateDeleteEmailGroup_byUserId
        public DataTable InsertUpdateDeleteEmailGroup_byUserId(GroupEmailMasterBase groupBase)
        {
            dsContainer = new DataSet();
            dtContainer = new DataTable();
            try
            {
                MyParameter[] myParams ={
                                        new MyParameter("@Id", groupBase.Id),
                                        new MyParameter("@UserId", groupBase.UserId),
                                         new MyParameter("@GroupName", groupBase.GroupName),
                                          new MyParameter("@Action", groupBase.Action)
                                       };
                Common.Set_Procedures("InsertUpdateDeleteEmailGroup_byUserId");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region EmailGroupList_byUserId
        public DataTable EmailGroupList_byUserId(GroupEmailMasterBase groupBase)
        {
            dsContainer = new DataSet();
            dtContainer = new DataTable();
            try
            {
                MyParameter[] myParams ={
                                        new MyParameter("@UserId", groupBase.UserId)
                                       };
                Common.Set_Procedures("EmailGroupList_byUserId");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region EmailGroup_LoadbyId
        public DataTable EmailGroup_LoadbyId(GroupEmailMasterBase groupBase)
        {
            dsContainer = new DataSet();
            dtContainer = new DataTable();
            try
            {
                MyParameter[] myParams = {
                                         new MyParameter("@Id",groupBase.Id)
                                         };
                Common.Set_Procedures("EmailGroup_LoadbyId");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region InsertUpdateGroupEmails_byGroupId
        public DataTable InsertUpdateGroupEmails_byGroupId(GroupEmailsBase emailBase)
        {
            dsContainer = new DataSet();
            dtContainer = new DataTable();
            try
            {
                MyParameter[] myParams = {
                                         new MyParameter("@GroupId",emailBase.GroupId),
                                         new MyParameter("@EmailXml",emailBase.Xml)
                                         };
                Common.Set_Procedures("InsertUpdateGroupEmails_byGroupId");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region EmailsForGroup_LoadbyGroupId
        public DataTable EmailsForGroup_LoadbyGroupId(GroupEmailsBase emailBase)
        {
            dsContainer = new DataSet();
            dtContainer = new DataTable();
            try
            {
                MyParameter[] myParams = {
                                         new MyParameter("@GroupId",emailBase.GroupId)
                                         };
                Common.Set_Procedures("EmailsForGroup_LoadbyGroupId");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion


        #region Projects_UpdateDisplayOrder
        public DataTable Projects_UpdateDisplayOrder(ProjectBase projectBase)
        {
            dsContainer = new DataSet();
            dtContainer = new DataTable();
            try
            {
                MyParameter[] myParams = {
                                             new MyParameter("@xml",projectBase.DisplayOrderXml)
                                         };
                Common.Set_Procedures("Projects_UpdateDisplayOrder");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region ProductProject_UpdateDisplayOrder
        public DataTable ProductProject_UpdateDisplayOrder(ProjectBase projectBase)
        {
            dsContainer = new DataSet();
            dtContainer = new DataTable();
            try
            {
                MyParameter[] myParams = {
                                             new MyParameter("@xml",projectBase.DisplayOrderXml)
                                         };
                Common.Set_Procedures("ProductProject_UpdateDisplayOrder");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region Responses_Offline_CheckDuplicateId
        public DataTable Responses_Offline_CheckDuplicateId(SRV.BaseLayer.Respondent.SurveyResponseBase SurveyResponse)
        {
            dsContainer = new DataSet();
            dtContainer = new DataTable();
            try
            {
                MyParameter[] myParams = {
                                             new MyParameter("@RespLst",SurveyResponse.RespList),
                                              new MyParameter("@projectId",SurveyResponse.ProjectId)
                                         };
                Common.Set_Procedures("Responses_Offline_CheckDuplicateId");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region ProductProject_UpdateMessageById
        public DataTable ProductProject_UpdateMessageById(ProductProjectBase ProductBase)
        {
            dsContainer = new DataSet();
            dtContainer = new DataTable();
            try
            {
                MyParameter[] myParams = {
                                             new MyParameter("@Id",ProductBase.Id),
                                              new MyParameter("@productMessage",ProductBase.ProductMessage)
                                         };
                Common.Set_Procedures("ProductProject_UpdateMessageById");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region Product_LoadById
        public DataTable Product_LoadById(ProductProjectBase ProductBase)
        {
            dsContainer = new DataSet();
            dtContainer = new DataTable();
            try
            {
                MyParameter[] myParams = {
                                             new MyParameter("@productId",ProductBase.Id)
                                         };
                Common.Set_Procedures("Product_LoadById");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region Response_DeleteBy_ProjectId
        public DataTable Response_DeleteBy_ProjectId(ProjectBase projectBase)
        {
            dsContainer = new DataSet();
            dtContainer = new DataTable();
            try
            {
                MyParameter[] myParams = {
                                         new MyParameter("@ProjectId",projectBase.Id)
                                         };
                Common.Set_Procedures("Response_DeleteBy_ProjectId");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region Responders_Offline_DeleteBy_ProjectId
        public DataTable Responders_Offline_DeleteBy_ProjectId(ProjectBase projectBase)
        {
            dsContainer = new DataSet();
            dtContainer = new DataTable();
            try
            {
                MyParameter[] myParams = {
                                         new MyParameter("@ProjectId",projectBase.Id)
                                         };
                Common.Set_Procedures("Responders_Offline_DeleteBy_ProjectId");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion


        #region Response_CaseSelectionReport
        public DataTable Response_CaseSelectionReport(ProjectBase projectBase)
        {
            dsContainer = new DataSet();
            dtContainer = new DataTable();
            try
            {
                MyParameter[] myParams = {
                                         new MyParameter("@caseTable",projectBase.CaseTable)
                                         };
                Common.Set_Procedures("Response_CaseSelectionReport");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region Response_CaseSelectionReportOffline
        public DataTable Response_CaseSelectionReportOffline(ProjectBase projectBase)
        {
            dsContainer = new DataSet();
            dtContainer = new DataTable();
            try
            {
                MyParameter[] myParams = {
                                         new MyParameter("@caseTable",projectBase.CaseTable)
                                         };
                Common.Set_Procedures("Response_CaseSelectionReportOffline");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region MatrixType_LoadByQuestionId
        public DataSet MatrixType_LoadByQuestionId(QuestionBase question)
        {
            dsContainer = new DataSet();
            dtContainer = new DataTable();
            try
            {
                MyParameter[] myParams = {
                                         new MyParameter("QuestionId",question.Id),
                                          new MyParameter("questionTypeId",question.QuestionTypeId)
                                         };
                Common.Set_Procedures("MatrixType_LoadByQuestionId");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dsContainer = Common.Execute_Procedures_Select();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dsContainer;
        }
        #endregion

        #region UserDirectories_LoadByUserId
        public DataTable UserDirectories_LoadByUserId(UserDirectoriesBase userDirectoriesBase)
        {
            dsContainer = new DataSet();
            dtContainer = new DataTable();
            try
            {
                MyParameter[] myParams = { new MyParameter("@UserId", userDirectoriesBase.UserId), };
                Common.Set_Procedures("UserDirectories_LoadByUserId");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region UserDirectories_InsertUpdate
        public DataTable UserDirectories_InsertUpdate(UserDirectoriesBase userDirectoriesBase)
        {
            dsContainer = new DataSet();
            dtContainer = new DataTable();
            try
            {
                MyParameter[] myParams = { new MyParameter("@Id", userDirectoriesBase.Id),
                                             new MyParameter("@UserId", userDirectoriesBase.UserId),
                                             new MyParameter("@DirName", userDirectoriesBase.DirName),
                                         };
                Common.Set_Procedures("UserDirectories_IU");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region UserDirectories_DeleteByUserId
        public DataTable UserDirectories_DeleteByUserId(UserDirectoriesBase userDirectoriesBase)
        {
            dsContainer = new DataSet();
            dtContainer = new DataTable();
            try
            {
                MyParameter[] myParams = { new MyParameter("@Id", userDirectoriesBase.Id)
                                         };
                Common.Set_Procedures("UserDirectories_DeleteByUserId");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region ProjectDirectories_LoadById
        public DataTable ProjectDirectories_LoadById(ProjectDirectoriesBase projectDirectoriesBase)
        {
            dsContainer = new DataSet();
            dtContainer = new DataTable();
            try
            {
                MyParameter[] myParams = { new MyParameter("@directoryId", projectDirectoriesBase.DirectoryId)
                                         };
                Common.Set_Procedures("ProjectDirectories_LoadById");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region ProjectDirectories_IU
        public DataTable ProjectDirectories_IU(ProjectDirectoriesBase projectDirectoriesBase)
        {
            dsContainer = new DataSet();
            dtContainer = new DataTable();
            try
            {
                MyParameter[] myParams = { new MyParameter("@directoryXML", projectDirectoriesBase.Xml)
                                         };
                Common.Set_Procedures("ProjectDirectories_IU");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region ProjectDirectories_LoadAll
        public DataSet ProjectDirectories_LoadAll(ProjectBase projectBase)
        {
            dsContainer = new DataSet();
            dtContainer = new DataTable();
            try
            {
                MyParameter[] myParams = {
                                             new MyParameter("@userId",projectBase.OwnerId)
                                         };
                Common.Set_Procedures("ProjectDirectories_LoadAll");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dsContainer = Common.Execute_Procedures_Select();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dsContainer;
        }
        #endregion

        #region PanelList_LoadAll
        public DataTable PanelList_LoadAll(AdminBase adminBase)
        {
            dsContainer = new DataSet();
            dtContainer = new DataTable();
            try
            {
                MyParameter[] myParams = { new MyParameter("@roleId", adminBase.RoleId) };
                Common.Set_Procedures("PanelList_LoadAll");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region PanelProjects_InsertUpdate
        public DataTable PanelProjects_InsertUpdate(ProjectBase projectBase)
        {
            dsContainer = new DataSet();
            dtContainer = new DataTable();
            try
            {
                MyParameter[] myParams = {
                                             new MyParameter("@PanelXml",projectBase.PanelXml),
                                             new MyParameter("@SuperProjectId",projectBase.SuperProjectId)
                                         };
                Common.Set_Procedures("PanelProjects_InsertUpdate");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region PanelProjects_LoadByProjectId
        public DataTable PanelProjects_LoadByProjectId(ProjectBase projectBase)
        {
            dsContainer = new DataSet();
            dtContainer = new DataTable();
            try
            {
                MyParameter[] myParams = {
                                             new MyParameter("@ProjectId",projectBase.Id),
                                               new MyParameter("@SuperProjectId",projectBase.SuperProjectId)
                                         };
                Common.Set_Procedures("PanelProjects_LoadByProjectId");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion


        #region ProjectDirectories_DeleteById
        public DataTable ProjectDirectories_DeleteById(ProjectDirectoriesBase projectDirectories)
        {
            dsContainer = new DataSet();
            dtContainer = new DataTable();
            try
            {
                MyParameter[] myParams = {
                                             new MyParameter("@Ids",projectDirectories.DirectoryIdList)
                                         };
                Common.Set_Procedures("ProjectDirectories_DeleteById");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region IncompleteRespondersLoad_ByProjectId
        public DataTable IncompleteRespondersLoad_ByProjectId(ProjectBase projectBase)
        {
            dsContainer = new DataSet();
            dtContainer = new DataTable();
            try
            {
                MyParameter[] myParams = {
                                         new MyParameter("@ProjectId",projectBase.Id),
                                         new MyParameter("@SuperProjectId",projectBase.SuperProjectId)
                                         };
                Common.Set_Procedures("LoadIncompleteResponders_ByProjectId");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region CompleteRespondersLoad_ByProjectId
        public DataTable CompleteRespondersLoad_ByProjectId(ProjectBase projectBase)
        {
            dsContainer = new DataSet();
            dtContainer = new DataTable();
            try
            {
                MyParameter[] myParams = {
                                         new MyParameter("@ProjectId",projectBase.Id),
                                         new MyParameter("@SuperProjectId",projectBase.SuperProjectId)
                                         };
                Common.Set_Procedures("LoadCompleteResponders_ByProjectId");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region Project_Load
        public DataTable Project_Load()
        {
            dsContainer = new DataSet();
            dtContainer = new DataTable();
            try
            {
                MyParameter[] myParams = {

                                         };
                Common.Set_Procedures("Project_Load");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region ProjectRespose_Add
        public DataTable ProjectRespose_Add(int projectId, string projectGUID, string Response, string ReportDataHtml)
        {
            dsContainer = new DataSet();
            dtContainer = new DataTable();
            try
            {
                MyParameter[] myParams = {
                                         new MyParameter("@projectId",projectId),
                                         new MyParameter("@projectGUID",projectGUID),
                                         new MyParameter("@Response",Response),
                                         new MyParameter("@html",ReportDataHtml)
                                         };
                Common.Set_Procedures("ProjectRespose_Add");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region ProjectRespose_LoadById
        public DataTable ProjectRespose_LoadById(string projectGUID)
        {
            dsContainer = new DataSet();
            dtContainer = new DataTable();
            try
            {
                MyParameter[] myParams = {
                                         new MyParameter("@projectGUID",projectGUID)
                                         };
                Common.Set_Procedures("ProjectRespose_LoadById");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region OnlineProjectRespose_Add
        public DataTable OnlineProjectRespose_Add(int projectId, string projectGUID, string Response, string ReportDataHtml)
        {
            dsContainer = new DataSet();
            dtContainer = new DataTable();
            try
            {
                MyParameter[] myParams = {
                                         new MyParameter("@projectId",projectId),
                                         new MyParameter("@projectGUID",projectGUID),
                                         new MyParameter("@Response",Response),
                                          new MyParameter("@html",ReportDataHtml)
                                         };
                Common.Set_Procedures("OnlineProjectRespose_Add");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region OnlineProjectRespose_LoadById
        public DataTable OnlineProjectRespose_LoadById(string projectGUID)
        {
            dsContainer = new DataSet();
            dtContainer = new DataTable();
            try
            {
                MyParameter[] myParams = {
                                         new MyParameter("@projectGUID",projectGUID)
                                         };
                Common.Set_Procedures("OnlineProjectRespose_LoadById");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region CombinedProjectRespose_Add
        public DataTable CombinedProjectRespose_Add(int projectId, string projectGUID, string Response, string ReportDataHtml)
        {
            dsContainer = new DataSet();
            dtContainer = new DataTable();
            try
            {
                MyParameter[] myParams = {
                                         new MyParameter("@projectId",projectId),
                                         new MyParameter("@projectGUID",projectGUID),
                                         new MyParameter("@Response",Response),
                                         new MyParameter("@html",ReportDataHtml)
                                         };
                Common.Set_Procedures("CombinedProjectRespose_Add");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region CombinedProjectRespose_LoadById
        public DataTable CombinedProjectRespose_LoadById(string projectGUID)
        {
            dsContainer = new DataSet();
            dtContainer = new DataTable();
            try
            {
                MyParameter[] myParams = {
                                         new MyParameter("@projectGUID",projectGUID)
                                         };
                Common.Set_Procedures("CombinedProjectRespose_LoadById");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region ReportQueryBuilder_Add
        public DataTable ReportQueryBuilder_Add(int projectId, string projectGUID, string SessionType, string ViewBagType, string Response)
        {
            dsContainer = new DataSet();
            dtContainer = new DataTable();
            try
            {
                MyParameter[] myParams = {
                                         new MyParameter("@projectId",projectId),
                                         new MyParameter("@projectGUID",projectGUID),
                                          new MyParameter("@SessionType",SessionType),
                                          new MyParameter("@ViewBagType",ViewBagType),
                                         new MyParameter("@Response",Response)
                                         };
                Common.Set_Procedures("ReportQueryBuilder_Add");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region ReportQueryBuilder_LoadById
        public DataTable ReportQueryBuilder_LoadById(int projectId,string projectGUID)
        {
            dsContainer = new DataSet();
            dtContainer = new DataTable();
            try
            {
                MyParameter[] myParams = {
                                          new MyParameter("@projectId",projectId),
                                          new MyParameter("@projectGUID",projectGUID)
                                         };
                Common.Set_Procedures("ReportQueryBuilder_LoadById");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region ReportQueryBuilderCases_Add
        public DataTable ReportQueryBuilderCases_Add(int projectId, string projectGUID, string CaseName, string OnlineResponse, string OfflineResponse, string CombineResponse)
        {
            dsContainer = new DataSet();
            dtContainer = new DataTable();
            try
            {
                MyParameter[] myParams = {
                                         new MyParameter("@projectId",projectId),
                                         new MyParameter("@projectGUID",projectGUID),
                                          new MyParameter("@CaseName",CaseName),
                                          new MyParameter("@OnlineResponse",OnlineResponse),
                                         new MyParameter("@OfflineResponse",OfflineResponse),
                                         new MyParameter("@CombineResponse",CombineResponse)
                                         };
                Common.Set_Procedures("ReportQueryBuilderCases_Add");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region ReportQueryBuilderCases_LoadById
        public DataTable ReportQueryBuilderCases_LoadById(int projectId, string projectGUID,string CaseName)
        {
            dsContainer = new DataSet();
            dtContainer = new DataTable();
            try
            {
                MyParameter[] myParams = {
                                          new MyParameter("@projectId",projectId),
                                          new MyParameter("@projectGUID",projectGUID),
                                          new MyParameter("@CaseName",CaseName)
                                         };
                Common.Set_Procedures("ReportQueryBuilderCases_LoadById");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region NotParticipateRes_Bind
        public DataTable NotParticipateRes_Bind(AdminBase adminBase)
        {
            dsContainer = new DataSet();
            dtContainer = new DataTable();
            try
            {
                MyParameter[] myParams = {
                                             new MyParameter("@ProjectId",adminBase.ProjectId)
                                         };
                Common.Set_Procedures("SelectNotParticipateRespondent");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region DeletePoject_LoadAll
        public DataTable DeletePoject_LoadAll(AdminBase adminBase)
        {
            dsContainer = new DataSet();
            dtContainer = new DataTable();
            try
            {
                MyParameter[] myParams = {
                                             new MyParameter("@OwnerId",adminBase.Id)
                                         };
                Common.Set_Procedures("DeletePoject_LoadAll");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region RecoverDeleteProject_ById
        public DataTable RecoverDeleteProject_ById(ProjectBase projectBase)
        {
            dsContainer = new DataSet();
            dtContainer = new DataTable();
            try
            {
                MyParameter[] myParams = {
                                             new MyParameter("@ProjectId",projectBase.Id)
                                         };
                Common.Set_Procedures("RecoverDeleteProject_ById");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region ClusterProjectNameCheck
        public DataTable ClusterProjectNameCheck(ProjectBase projectBase)
        {
            dsContainer = new DataSet();
            dtContainer = new DataTable();
            try
            {
                MyParameter[] myParams = {
                                             new MyParameter("@surveyId",projectBase.Id),
                                             new MyParameter("@projectName",projectBase.ProjectName)
                                         };
                Common.Set_Procedures("ClusterProjectNameCheck");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region OfflineSegmentationRespose_Add
        public DataTable OfflineSegmentationRespose_Add(int projectId, string projectGUID, string Response, string ReportDataHtml)
        {
            dsContainer = new DataSet();
            dtContainer = new DataTable();
            try
            {
                MyParameter[] myParams = {
                                         new MyParameter("@projectId",projectId),
                                         new MyParameter("@projectGUID",projectGUID),
                                         new MyParameter("@Response",Response),
                                         new MyParameter("@html",ReportDataHtml)
                                         };
                Common.Set_Procedures("OfflineSegmentationRespose_Add");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region OfflineSegmentation_LoadById
        public DataTable OfflineSegmentation_LoadById(string projectGUID)
        {
            dsContainer = new DataSet();
            dtContainer = new DataTable();
            try
            {
                MyParameter[] myParams = {
                                         new MyParameter("@projectGUID",projectGUID)
                                         };
                Common.Set_Procedures("OfflineSegmentation_LoadById");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region OnlineSegmentationRespose_Add
        public DataTable OnlineSegmentationRespose_Add(int projectId, string projectGUID, string Response, string ReportDataHtml)
        {
            dsContainer = new DataSet();
            dtContainer = new DataTable();
            try
            {
                MyParameter[] myParams = {
                                         new MyParameter("@projectId",projectId),
                                         new MyParameter("@projectGUID",projectGUID),
                                         new MyParameter("@Response",Response),
                                          new MyParameter("@html",ReportDataHtml)
                                         };
                Common.Set_Procedures("OnlineSegmentationRespose_Add");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region OnlineSegmentation_LoadById
        public DataTable OnlineSegmentation_LoadById(string projectGUID)
        {
            dsContainer = new DataSet();
            dtContainer = new DataTable();
            try
            {
                MyParameter[] myParams = {
                                         new MyParameter("@projectGUID",projectGUID)
                                         };
                Common.Set_Procedures("OnlineSegmentation_LoadById");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region CombineSegmentationRespose_Add
        public DataTable CombineSegmentationRespose_Add(int projectId, string projectGUID, string Response, string ReportDataHtml)
        {
            dsContainer = new DataSet();
            dtContainer = new DataTable();
            try
            {
                MyParameter[] myParams = {
                                         new MyParameter("@projectId",projectId),
                                         new MyParameter("@projectGUID",projectGUID),
                                         new MyParameter("@Response",Response),
                                         new MyParameter("@html",ReportDataHtml)
                                         };
                Common.Set_Procedures("CombineSegmentationRespose_Add");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region CombinedSegmentationRespose_LoadById
        public DataTable CombinedSegmentationRespose_LoadById(string projectGUID)
        {
            dsContainer = new DataSet();
            dtContainer = new DataTable();
            try
            {
                MyParameter[] myParams = {
                                         new MyParameter("@projectGUID",projectGUID)
                                         };
                Common.Set_Procedures("CombinedSegmentationRespose_LoadById");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region SurveyNameCheck
        public DataTable SurveyNameCheck(ProjectBase projectBase)
        {
            dsContainer = new DataSet();
            dtContainer = new DataTable();
            try
            {
                MyParameter[] myParams = {
                                             new MyParameter("@ProjectId",projectBase.SuperProjectId),
                                             new MyParameter("@ProjectName",projectBase.ProjectName)
                                         };
                Common.Set_Procedures("SurveyNameCheck");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region NotParticipateResProject_LoadAll
        public DataTable NotParticipateResProject_LoadAll(AdminBase adminBase)
        {
            dsContainer = new DataSet();
            dtContainer = new DataTable();
            try
            {
                MyParameter[] myParams = {
                                             new MyParameter("@ProjectId",adminBase.ProjectId),
                                             new MyParameter("@OwnerId",adminBase.Id),
                                         };
                Common.Set_Procedures("NotParticipateResProject_LoadAll");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region DeleteNoPeak_ById
        public DataTable DeleteNoPeak_ById(ProjectBase projectBase)
        {
            dsContainer = new DataSet();
            dtContainer = new DataTable();
            try
            {
                MyParameter[] myParams = {
                                             new MyParameter("@surveyId",projectBase.Id)
                                         };
                Common.Set_Procedures("DeleteNoPeak_ById");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

    }
}
