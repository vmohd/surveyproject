﻿using SRV.BaseLayer.Respondent;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SRV.BaseLayer.Admin;
using SRV.Utility;

namespace SRV.DataLayer.Respondent
{
    public class RespondentDL
    {
        #region Declaration
        DataSet dsContainer;
        DataTable dtContainer;
        #endregion

        #region Method ProjectInvitation_LoadByGUID
        public DataTable ProjectInvitation_LoadByGUID(SurveyLoadBase survey)
        {
            dtContainer = new DataTable();
            dsContainer = new DataSet();
            try
            {
                MyParameter[] myParams ={                                   
                                           new MyParameter("@ProjectGuid", survey.GUID),
                                           new MyParameter("@InvitationId",survey.InvitationId),
                                           new MyParameter("@SuperProjectId",survey.superProjectId)
                                        };
                Common.Set_Procedures("ProjectInvitation_LoadByGUID");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region Method Projects_LoadByResponderId
        public DataTable Projects_LoadByResponderId(RespondentBase respondent)
        {
            dtContainer = new DataTable();
            dsContainer = new DataSet();
            try
            {
                MyParameter[] myParams ={                                   
                                           new MyParameter("@Id", respondent.Id)
                                        };
                Common.Set_Procedures("Projects_LoadByResponderId");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region Method Response_LoadByProjectId
        public DataTable Response_LoadByProjectId(SurveyLoadBase survey)
        {
            dtContainer = new DataTable();
            dsContainer = new DataSet();
            try
            {
                MyParameter[] myParams ={                                   
                                           new MyParameter("@Id", survey.Id),
                                           new MyParameter("@Email",survey.Email),
                                           new MyParameter("@ProductId",survey.ProductId),
                                           new MyParameter("@ExternalResponderId",survey.ExternalResponderId)
                                        };
                Common.Set_Procedures("Response_LoadByEmailAndProjectId");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region Method Response_Offline_LoadBy_ProjectId_RespId
        public DataTable Response_Offline_LoadBy_ProjectId_RespId(SurveyLoadBase survey)
        {
            dtContainer = new DataTable();
            dsContainer = new DataSet();
            try
            {
                MyParameter[] myParams ={                                   
                                           new MyParameter("@Id", survey.Id),
                                           new MyParameter("@RespId",survey.OfflineResponderId),
                                           new MyParameter("@ProductId",survey.ProductId)
                                        };
                Common.Set_Procedures("Response_Offline_LoadBy_ProjectId_RespId");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region Method Survey_LoadByGUID
        public DataTable Survey_LoadByGUID(SurveyLoadBase survey)
        {
            dtContainer = new DataTable();
            dsContainer = new DataSet();
            try
            {
                MyParameter[] myParams ={                                   
                                           new MyParameter("@GUID", survey.GUID)
                                        };
                Common.Set_Procedures("Project_LoadByGUID");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region ProjQues_LoadByProjectId
        public DataTable ProjQues_LoadByProjectId(SurveyLoadBase survey)
        {
            dsContainer = new DataSet();
            dtContainer = new DataTable();
            try
            {
                MyParameter[] myParams = {
                                             new MyParameter("@projectId",survey.Id),
                                             new MyParameter("@IsQuesRandomize",survey.IsQuesRandomize)
                                         };
                Common.Set_Procedures("Qusetion_LoadAllByProjectId");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region Options_LoadAllByQuesId
        public DataTable Options_LoadAllByQuesId(SurveyQuestionBase questionBase)
        {
            dsContainer = new DataSet();
            dtContainer = new DataTable();
            try
            {
                MyParameter[] myParams = {
                                             new MyParameter("@qusetionId",questionBase.Id),
                                             new MyParameter("@isOptRandomize",questionBase.IsOptRandomize)
                                         };
                Common.Set_Procedures("Option_LoadAllById");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region SurveyResponse_Insert_Update
        public DataTable SurveyResponse_Insert_Update(SurveyResponseBase surveyResponse)
        {
            dsContainer = new DataSet();
            dtContainer = new DataTable();
            try
            {
                MyParameter[] myParams = {
                                             new MyParameter("@Email",surveyResponse.Email),
                                             new MyParameter("@xmlData",surveyResponse.Xml),  
                                             new MyParameter("@projectId",surveyResponse.ProjectId),
                                             new MyParameter("@ResponsesXml",surveyResponse.ResponsesCountXml),
                                             new MyParameter("@productId",surveyResponse.ProductId),
                                             new MyParameter("@positionTblId",surveyResponse.PositionTblId),
                                             new MyParameter("@pnlId",surveyResponse.PnlId),
                                             new MyParameter("@IsExternal",surveyResponse.IsExternal),
                                             new MyParameter("@ExternalResponderId",surveyResponse.ExternalResponderId),
                                             new MyParameter("@Terminate",surveyResponse.Terminate)
                                         };
                Common.Set_Procedures("SurveyResponse_Insert_Update");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region ProjQuotaOther_LoadByProjectId
        public DataTable ProjQuotaOther_LoadByProjectId(SurveyLoadBase survey)
        {
            dsContainer = new DataSet();
            dtContainer = new DataTable();
            try
            {
                MyParameter[] myParams = {
                                             new MyParameter("@projectId",survey.Id)
                                         };
                Common.Set_Procedures("ProjQuotaOther_LoadByProjectId");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region ProjectQuotaOther_Update
        public DataTable ProjectQuotaOther_Update(SurveyResponseBase surveyResponseBase)
        {
            dsContainer = new DataSet();
            dtContainer = new DataTable();
            try
            {
                MyParameter[] myParams = {
                                             new MyParameter("@Id",surveyResponseBase.QuotaId),
                                             new MyParameter("@Email",surveyResponseBase.Email)
                                         };
                Common.Set_Procedures("ProjectQuotaOther_Update");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region ProjectQuotaOther_UpdateByExUser
        public DataTable ProjectQuotaOther_UpdateByExUser(SurveyResponseBase surveyResponseBase)
        {
            dsContainer = new DataSet();
            dtContainer = new DataTable();
            try
            {
                MyParameter[] myParams = {
                                             new MyParameter("@Id",surveyResponseBase.QuotaId),
                                             new MyParameter("@ExternalResponderId",surveyResponseBase.ExternalResponderId)
                                         };
                Common.Set_Procedures("ProjectQuotaOther_UpdateByExUser");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region ConditionExpression_LoadByProjectId
        public DataTable ConditionExpression_LoadByProjectId(ProjecQuotaBase quotaBase)
        {
            dsContainer = new DataSet();
            dtContainer = new DataTable();
            try
            {
                MyParameter[] myParams = {
                                             new MyParameter("@ProjectId",quotaBase.ProjectId)
                                         };
                Common.Set_Procedures("ConditionExpression_LoadByProjectId");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region Language_LoadByProjectId
        public DataTable Language_LoadByProjectId(LanguageBase languageBase)
        {
            dsContainer = new DataSet();
            dtContainer = new DataTable();
            try
            {
                MyParameter[] myParams = {
                                             new MyParameter("@Id",languageBase.ProjectId)
                                         };
                Common.Set_Procedures("Language_LoadByProjectId");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion


        #region Method ProjectInvitation_InsertRedirectUrl
        public DataTable ProjectInvitation_InsertRedirectUrl(ProjectInvitationBase projectInvitation)
        {
            dtContainer = new DataTable();
            dsContainer = new DataSet();
            try
            {
                MyParameter[] myParams ={                                   
                                           new MyParameter("@projectId", projectInvitation.ProjectId),
                                           new MyParameter("@email",projectInvitation.Email),
                                           new MyParameter("@invitationGuid",projectInvitation.InvitationGuid)
                                        };
                Common.Set_Procedures("ProjectInvitation_InsertRedirectUrl");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion     

        #region Method DDLMatrixTypeOptions_LoadByQuestionId
        public DataTable DDLMatrixTypeOptions_LoadByQuestionId(SurveyQuestionBase surveyQuestionBase)
        {
            dtContainer = new DataTable();
            dsContainer = new DataSet();
            try
            {
                MyParameter[] myParams ={                                   
                                           new MyParameter("@questionId", surveyQuestionBase.Id)
                                          
                                        };
                Common.Set_Procedures("DDLMatrixTypeOptions_LoadByQuestionId");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion     

        #region PositionTable_LoadByIds
        public DataSet PositionTable_LoadByIds(PositionTable positionTableBase)
        {
            dsContainer = new DataSet();
            dtContainer = new DataTable();
            try
            {
                MyParameter[] myParams = {
                                             new MyParameter("@ResponderId",positionTableBase.ResponderId),
                                             new MyParameter("@SurveyId",positionTableBase.SurveyId)
                                         };

                Common.Set_Procedures("PositionTable_LoadByIds");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dsContainer = Common.Execute_Procedures_Select();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dsContainer;
        }
        #endregion

        #region ProjectInvitation_LoadByID
        public DataTable ProjectInvitation_LoadByID(ProjectInvitationBase projectInvitation)
        {
            dtContainer = new DataTable();
            dsContainer = new DataSet();
            try
            {
                MyParameter[] myParams ={                                   
                                           new MyParameter("@respId", projectInvitation.Id),
                                        };
                Common.Set_Procedures("ProjectInvitation_LoadByID");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region ResponseLoad_ByProjectId
        public DataTable ResponseLoad_ByProjectId(ProjectBase project)
        {
            dtContainer = new DataTable();
            dsContainer = new DataSet();
            try
            {
                MyParameter[] myParams ={                                   
                                           new MyParameter("@ProjectId", project.Id),
                                        };
                Common.Set_Procedures("ResponseLoad_ByProjectId");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion
    }
}
