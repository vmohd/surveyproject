﻿using SRV.BaseLayer.Panel;
using SRV.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SRV.DataLayer.Panel
{
   public class PanelDL
    {
        #region Declaration
        DataSet dsContainer;
        DataTable dtContainer;
        #endregion


        #region PanelProjects_LoadByPanelId
        public DataTable PanelProjects_LoadByPanelId(PanelBase panelBase)
        {
            dsContainer = new DataSet();
            dtContainer = new DataTable();
            try
            {
                MyParameter[] myParams = {
                                             new MyParameter("@PanelId",panelBase.PanelId)
                                         };
                Common.Set_Procedures("PanelProjects_LoadByPanelId");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion
    }
}
