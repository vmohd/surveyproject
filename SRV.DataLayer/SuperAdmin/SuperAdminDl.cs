﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SRV.BaseLayer.Admin;
using SRV.BaseLayer.SuperAdmin;
using SRV.Utility;

namespace SRV.DataLayer.SuperAdmin
{
    public class SuperAdminDl
    {
        #region Declaration
        DataSet dsContainer;
        DataTable dtContainer;
        #endregion

        #region Admin_LoadList
        public DataTable Admin_LoadList()
        {
            dsContainer = new DataSet();
            dtContainer = new DataTable();
            try
            {
                MyParameter[] myParams = {
                                            
                                         };
                Common.Set_Procedures("AdminList_LoadAll");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region AdminUpdate_LoadList
        public DataTable AdminUpdate_LoadList(AdminBase adminBaseReg)
        {
            dsContainer = new DataSet();
            dtContainer = new DataTable();
            try
            {
                MyParameter[] myParams = {
                                            new MyParameter("@id",adminBaseReg.Id)
                                         };
                Common.Set_Procedures("AdminList_LoadAll");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region AdminAccount_Delete
        public DataTable AdminAccount_Delete(AdminBase adminBaseReg)
        {
            dsContainer = new DataSet();
            dtContainer = new DataTable();
            try
            {
                MyParameter[] myParams = {
                                            new MyParameter("@id",adminBaseReg.Id)                                            
                                         };
                Common.Set_Procedures("AdminAccount_Delete");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region ChangeLogin_Status
        public DataTable ChangeLogin_Status(AdminBase adminBaseReg)
        {
            dsContainer = new DataSet();
            dtContainer = new DataTable();
            try
            {
                MyParameter[] myParams = {
                                             new MyParameter("@id",adminBaseReg.Id),
                                              new MyParameter("@status",adminBaseReg.isActive),
                                         };
                Common.Set_Procedures("[AdminStatus_Update]");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region Roles_LoadAll
        public DataTable Roles_LoadAll()
        {
            dsContainer = new DataSet();
            dtContainer = new DataTable();
            try
            {
                MyParameter[] myParams = { };
                Common.Set_Procedures("Roles_LoadAll");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region InsertRoles_ByUserId
        public DataTable InsertRoles_ByUserId(CreateRoleBase roleBase)
        {
            dsContainer = new DataSet();
            dtContainer = new DataTable();
            try
            {
                MyParameter[] myParams = { 
                                         new MyParameter("@RoleXml",roleBase.RoleXml)
                                         };
                Common.Set_Procedures("InsertRoles_ByUserId");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region UpdateRoles_ById
        public DataTable UpdateRoles_ById(CreateRoleBase roleBase)
        {
            dsContainer = new DataSet();
            dtContainer = new DataTable();
            try
            {
                MyParameter[] myParams = { 
                                         new MyParameter("@Xml",roleBase.RoleXml)
                                         };
                Common.Set_Procedures("UpdateRoles_ById");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region RoleForUser_ByUserId
        public DataTable RoleForUser_ByUserId(CreateRoleBase role)
        {
            dsContainer = new DataSet();
            dtContainer = new DataTable();
            try
            {
                MyParameter[] myParams = {
                                         new MyParameter("@UserId",role.UserId)
                                         };
                Common.Set_Procedures("RoleForUser_ByUserId");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region Password_bySuperAdminId
        public DataTable Password_bySuperAdminId(AdminBase adminBase)
        {
            dsContainer = new DataSet();
            dtContainer = new DataTable();
            try
            {
                MyParameter[] myParams = {
                                            new MyParameter("@Id",adminBase.Id)
                                         };
                Common.Set_Procedures("Password_bySuperAdminId");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion

        #region CreateUser
        public DataTable CreateUser(AdminBase adminBaseReg)
        {
            dsContainer = new DataSet();
            dtContainer = new DataTable();
            try
            {
                MyParameter[] myParams = {
                                            new MyParameter("@UserGuid",adminBaseReg.UserGuid),
                                            new MyParameter("@FirstName",adminBaseReg.FirstName),
                                            new MyParameter("@LastName",adminBaseReg.LastName),
                                            new MyParameter("@Email",adminBaseReg.Email),
                                            new MyParameter("@Gender",adminBaseReg.Gender),
                                            new MyParameter ("@RoleId",adminBaseReg.RoleId)
                                         };
                Common.Set_Procedures("CreateUser");
                Common.Set_ParameterLength(myParams.Length);
                Common.Set_Parameters(myParams);
                dtContainer = Common.Execute_Procedures_LoadData();
            }
            catch (Exception ex)
            {
                ErrorReporting.DataLayerError(ex);
            }
            return dtContainer;
        }
        #endregion
        
    }
}
