﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SRV.BaseLayer.Admin;
using SRV.BaseLayer;
using SRV.DataLayer;
using SRV.DataLayer.Admin;
using SRV.Utility;
using System.Data;


namespace SRV.ActionLayer.Admin
{
    public class AdminAction
    {
        #region Declaration

        AdminDL adminDL = new AdminDL();
        ActionResult actionResult = new ActionResult();
        #endregion

        #region Method Project_Insert_Update
        public ActionResult Project_Insert_Update(ProjectBase project)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = adminDL.Project_Insert_Update(project);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    actionResult.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region Method ClusterProject_Insert
        public ActionResult ClusterProject_Insert(ProjectBase project)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = adminDL.ClusterProject_Insert(project);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    actionResult.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region Method NotParticipateRes_Insert
        public ActionResult NotParticipateRes_Insert(ProjectBase project)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = adminDL.NotParticipateRes_Insert(project);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    actionResult.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region Method Query_Insert_Update
        public ActionResult Query_Insert_Update(QueryBase query)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = adminDL.Query_Insert_Update(query);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    actionResult.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region Method QueryUpdate
        public ActionResult QueryUpdate(QueryBase query)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = adminDL.QueryUpdate(query);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    actionResult.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region Method SuperProject_Insert_Update
        public ActionResult SuperProject_Insert_Update(ProjectBase project)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = adminDL.SuperProject_Insert_Update(project);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    actionResult.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion


        #region ProjectDelete_ById
        public ActionResult ProjectDelete_ById(ProjectBase project)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = adminDL.ProjectDelete_ById(project);
                if (actionResult != null)
                {
                    if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                    {
                        actionResult.IsSuccess = true;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region QueryDelete_ByCase
        public ActionResult QueryDelete_ByCase(QueryBase queryBase)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = adminDL.QueryDelete_ByCase(queryBase);
                if (actionResult != null)
                {
                    if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                    {
                        actionResult.IsSuccess = true;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region Method ProjectSettings_InsertUpdate
        public ActionResult ProjectSettings_InsertUpdate(ProjectSettingsBase projectSettings)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = adminDL.ProjectSettings_InsertUpdate(projectSettings);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    actionResult.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region ProjectSettings_LoadById
        public ActionResult ProjectSettings_LoadById(ProjectSettingsBase projectSettings)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = adminDL.ProjectSettings_LoadById(projectSettings);
                if (actionResult != null)
                {
                    if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                    {
                        actionResult.IsSuccess = true;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region method Register_InsertUpdate
        public ActionResult Register_InsertUpdate(AdminBase adminBaseReg)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = adminDL.Register_InsertUpdate(adminBaseReg);

                if (actionResult.dtResult != null)
                {
                    if (actionResult.dtResult.Rows.Count > 0)
                    {
                        actionResult.IsSuccess = true;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region AdminRegistration_Confirm
        public ActionResult AdminRegistration_Confirm(AdminBase adminBase)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = adminDL.AdminRegistration_Confirm(adminBase);
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region Country_LoadAll
        public ActionResult Country_LoadAll()
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = adminDL.Country_LoadAll();
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region State_LoadAll
        public ActionResult State_LoadAll(AdminBase adminBase)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = adminDL.State_LoadAll(adminBase);
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region Poject_LoadAll
        public ActionResult Poject_LoadAll(AdminBase adminBase)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dsResult = adminDL.Poject_LoadAll(adminBase);
                if (actionResult.dsResult != null && actionResult.dsResult.Tables[0].Rows.Count > 0 || actionResult.dsResult != null && actionResult.dsResult.Tables[1].Rows.Count > 0)
                {
                    actionResult.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region Project_LoadById
        public ActionResult Project_LoadById(ProjectBase projectBase)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = adminDL.Project_LoadById(projectBase);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    actionResult.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region Qusetion_LoadAllByProjectId
        public ActionResult Qusetion_LoadAllByProjectId(QuestionBase questionBase)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = adminDL.Qusetion_LoadAllByProjectId(questionBase);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    actionResult.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region Query_LoadAllByProjectId
        public ActionResult Query_LoadAllByProjectId(QueryBase queryBase)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = adminDL.Query_LoadAllByProjectId(queryBase);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    actionResult.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region Query_LoadAllBySuperProjectId
        public ActionResult Query_LoadAllBySuperProjectId(QueryBase queryBase)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = adminDL.Query_LoadAllBySuperProjectId(queryBase);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    actionResult.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region Method Question_Insert
        public ActionResult Question_Insert(QuestionBase questionBase)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = adminDL.Question_Insert(questionBase);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    actionResult.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region Method Question_Update
        public ActionResult Question_Update(QuestionBase questionBase)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = adminDL.Question_Update(questionBase);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    actionResult.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region QuotaId_LoadByProjectId
        public ActionResult QuotaId_LoadByProjectId(QuestionBase questionBase)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = adminDL.QuotaId_LoadByProjectId(questionBase);
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region Option_Insert
        public ActionResult Option_Insert(QuestionBase questionBase)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = adminDL.Option_Insert(questionBase);
                if (actionResult != null)
                {
                    if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                    {
                        actionResult.IsSuccess = true;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region DeleteQuestion_ById
        public ActionResult DeleteQuestion_ById(QuestionBase questionBase)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = adminDL.DeleteQuestion_ById(questionBase);
                if (actionResult != null)
                {
                    if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                    {
                        actionResult.IsSuccess = true;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region Qusetion_Option_LoadAllById
        public ActionResult Qusetion_Option_LoadAllById(QuestionBase questionBase)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = adminDL.Qusetion_Option_LoadAllById(questionBase);
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region Option_LoadAllById
        public ActionResult Option_LoadAllById(QuestionBase questionBase)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = adminDL.Option_LoadAllById(questionBase);
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region Method Question_Update_ById
        public ActionResult Question_Update_ById(QuestionBase questionBase)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = adminDL.Question_Update_ById(questionBase);
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region QuestionType_LoadAll
        public ActionResult QuestionType_LoadAll()
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = adminDL.QuestionType_LoadAll();
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region QuestionsLoadAll_ByProjectId
        public ActionResult QuestionsLoadAll_ByProjectId(QuestionBase question)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = adminDL.QuestionsLoadAll_ByProjectId(question);
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region QuestionsType_ByQuestionId
        public ActionResult QuestionsType_ByQuestionId(QuestionBase question)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = adminDL.QuestionsType_ByQuestionId(question);
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region Method QuestionUpdate_ByQuestionId
        public ActionResult QuestionUpdate_ByQuestionId(QuestionBase questionBase)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = adminDL.QuestionUpdate_ByQuestionId(questionBase);
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region Method UserInfo_LoadById
        public ActionResult UserInfo_LoadById(UserInfoBase userInfo)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = adminDL.UserInfo_LoadById(userInfo);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    actionResult.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region InsertMatrix_Column
        public ActionResult InsertMatrix_Column(QuestionBase questionBase)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = adminDL.InsertMatrix_Column(questionBase);
                if (actionResult != null)
                {
                    if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                    {
                        actionResult.IsSuccess = true;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region ProjectStatistics_LoadById
        public ActionResult ProjectStatistics_LoadById(AdminBase adminBase)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = adminDL.ProjectStatistics_LoadById(adminBase);
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region Question_LoadById
        public ActionResult Question_LoadById(QuestionBase question)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dsResult = adminDL.Question_LoadById(question);
                if (actionResult.dsResult != null)
                {
                    actionResult.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region UpdateQuestionGroups_byProjectId
        public ActionResult UpdateQuestionGroups_byProjectId(ProjectQuestionGroupBase groupBase)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dsResult = adminDL.UpdateQuestionGroups_byProjectId(groupBase);
                if (actionResult.dsResult != null && actionResult.dsResult.Tables[0].Rows.Count > 0)
                {
                    actionResult.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region Method DeleteOptionsBeforeUpdate_ByQuestionId
        public ActionResult DeleteOptionsBeforeUpdate_ByQuestionId(QuestionBase questionBase)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = adminDL.DeleteOptionsBeforeUpdate_ByQuestionId(questionBase);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    actionResult.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region ProjectQuotaOtherInsertUpdate_ByProjectId
        public ActionResult ProjectQuotaOtherInsertUpdate_ByProjectId(ProjecQuotaBase projectQuotaBase)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = adminDL.ProjectQuotaOtherInsertUpdate_ByProjectId(projectQuotaBase);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    actionResult.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region ProjectQuota_LoadById
        public ActionResult ProjectQuota_LoadById(ProjecQuotaBase projectQuotaBase)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = adminDL.ProjectQuota_LoadById(projectQuotaBase);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    actionResult.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region ProjectQuotaOther_LoadById
        public ActionResult ProjectQuotaOther_LoadById(ProjecQuotaBase projectQuotaBase)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = adminDL.ProjectQuotaOther_LoadById(projectQuotaBase);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    actionResult.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region Method ProjectInvitation_Insert_Update
        public ActionResult ProjectInvitation_Insert_Update(ProjectInvitationBase projectInvitation)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = adminDL.ProjectInvitation_Insert_Update(projectInvitation);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    actionResult.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region Method ProjectClone_ByProjectId
        public ActionResult ProjectClone_ByProjectId(AdminBase adminBase)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = adminDL.ProjectClone_ByProjectId(adminBase);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    actionResult.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region Method ProjectQuestion_UpdateDisplayOrder
        public ActionResult ProjectQuestion_UpdateDisplayOrder(QuestionBase quesBase)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = adminDL.ProjectQuestion_UpdateDisplayOrder(quesBase);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    actionResult.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region Method QuestionOptions_UpdateDisplayOrder
        public ActionResult QuestionOptions_UpdateDisplayOrder(QuestionBase quesBase)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = adminDL.QuestionOptions_UpdateDisplayOrder(quesBase);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    actionResult.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region RespondersLoad_ByProjectId
        public ActionResult RespondersLoad_ByProjectId(ProjectBase projectBase)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = adminDL.RespondersLoad_ByProjectId(projectBase);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    actionResult.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region Responders_Offline_LoadBy_ProjectId
        public ActionResult Responders_Offline_LoadBy_ProjectId(ProjectBase projectBase)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = adminDL.Responders_Offline_LoadBy_ProjectId(projectBase);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    actionResult.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region Response_DeleteByIds
        public ActionResult Response_DeleteByIds(SRV.BaseLayer.Respondent.SurveyResponseBase surveyResponseBase)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = adminDL.Response_DeleteByIds(surveyResponseBase);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    actionResult.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                actionResult.IsSuccess = false;
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region Language_LoadAll
        public ActionResult Language_LoadAll()
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = adminDL.Language_LoadAll();
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    actionResult.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region ProjectQuestionDetails_LoadByQuestionid
        public ActionResult ProjectQuestionDetails_LoadByQuestionid(QuestionBase questionBase)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = adminDL.ProjectQuestionDetails_LoadByQuestionid(questionBase);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    actionResult.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region ChangePassword_ById
        public ActionResult ChangePassword_ById(AdminBase adminBase)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = adminDL.ChangePassword_ById(adminBase);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    actionResult.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region RoleNames_byUserId
        public ActionResult ActionNames_byUserId(AdminBase adminBase)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = adminDL.ActionNames_byUserId(adminBase);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    actionResult.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region Project_LoadByClosingDate
        public ActionResult Project_LoadByClosingDate()
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = adminDL.Project_LoadByClosingDate();
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    actionResult.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region CommonMethodQuestionGroups
        public ActionResult CommonMethodQuestionGroups(ProjectQuestionGroupBase groupBase)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = adminDL.CommonMethodQuestionGroups(groupBase);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    actionResult.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region QuestionGroupsLoadAll_ByProjectId
        public ActionResult QuestionGroupsLoadAll_ByProjectId(ProjectQuestionGroupBase groupBase)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = adminDL.QuestionGroupsLoadAll_ByProjectId(groupBase);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    actionResult.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region DeleteQuestionGroup_ById
        public ActionResult DeleteQuestionGroup_ById(ProjectQuestionGroupBase groupBase)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = adminDL.DeleteQuestionGroup_ById(groupBase);
                if (actionResult != null)
                {
                    if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                    {
                        actionResult.IsSuccess = true;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region Answers_LoadAllByQuestionId
        public ActionResult Answers_LoadAllByQuestionId(QuestionBase question)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dsResult = adminDL.Answers_LoadAllByQuestionId(question);
                if (actionResult.dsResult != null && actionResult.dsResult.Tables[0].Rows.Count > 0)
                {
                    actionResult.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region ProjectQuestionGroups_UpdateDisplayOrder
        public ActionResult ProjectQuestionGroups_UpdateDisplayOrder(ProjectQuestionGroupBase groupBase)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = adminDL.ProjectQuestionGroups_UpdateDisplayOrder(groupBase);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    actionResult.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region Project_LoadById
        public ActionResult ProductProject_LoadById(ProductProjectBase productProjectBase)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = adminDL.ProductProject_LoadById(productProjectBase);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    actionResult.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region SurveyLink_Insert
        public ActionResult SurveyLink_Insert(ProductProjectBase productProjectBase)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = adminDL.SurveyLink_Insert(productProjectBase);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    actionResult.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region LinkQuestion_Insert
        public ActionResult LinkQuestion_Insert(ProductProjectBase productProjectBase)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = adminDL.LinkQuestion_Insert(productProjectBase);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    actionResult.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region Qusetion_LoadAllByProductId
        public ActionResult Qusetion_LoadAllByProductId(QuestionBase questionBase)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = adminDL.Qusetion_LoadAllByProductId(questionBase);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    actionResult.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region LinkProduct_LoadAll
        public ActionResult LinkProduct_LoadAll(AdminBase adminBase)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = adminDL.LinkProduct_LoadAll(adminBase);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    actionResult.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region PositionTable_Insert
        public ActionResult PositionTable_Insert(PositionTable positonTable)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = adminDL.PositionTable_Insert(positonTable);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    actionResult.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region SuperProject_LoadById
        public ActionResult SuperProject_LoadById(ProjectBase projectBase)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = adminDL.SuperProject_LoadById(projectBase);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    actionResult.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion


        #region Method SubProjectsRedirection_Update
        public ActionResult SubProjectsRedirection_Update(ProjectInvitationBase projectInvitation)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = adminDL.SubProjectsRedirection_Update(projectInvitation);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    actionResult.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region SuperProjectDelete_ById
        public ActionResult SuperProjectDelete_ById(ProjectBase project)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = adminDL.SuperProjectDelete_ById(project);
                if (actionResult != null)
                {
                    if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                    {
                        actionResult.IsSuccess = true;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region Products_LoadBySuperSurveyId
        public ActionResult Products_LoadBySuperSurveyId(PositionTable positionBase)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dsResult = adminDL.Products_LoadBySuperSurveyId(positionBase);
                if (actionResult != null)
                {
                    if (actionResult.dsResult != null && actionResult.dsResult.Tables[0].Rows.Count > 0)
                    {
                        actionResult.IsSuccess = true;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region RespondersLoad_BySuperSurveyId
        public ActionResult RespondersLoad_BySuperSurveyId(PositionTable positionBase)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = adminDL.RespondersLoad_BySuperSurveyId(positionBase);
                if (actionResult != null)
                {
                    if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                    {
                        actionResult.IsSuccess = true;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region PositionTableSurvey_UpdateDisplayOrder
        public ActionResult PositionTableSurvey_UpdateDisplayOrder(PositionTable positionBase)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = adminDL.PositionTableSurvey_UpdateDisplayOrder(positionBase);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    actionResult.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region PositionTable_LoadBySuperSurveyId
        public ActionResult PositionTable_LoadBySuperSurveyId(PositionTable positionBase)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = adminDL.PositionTable_LoadBySuperSurveyId(positionBase);
                if (actionResult != null)
                {
                    if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                    {
                        actionResult.IsSuccess = true;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region Product_UpdateDisplayOrder
        public ActionResult Product_UpdateDisplayOrder(PositionTable positionBase)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = adminDL.Product_UpdateDisplayOrder(positionBase);
                if (actionResult != null)
                {
                    if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                    {
                        actionResult.IsSuccess = true;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region InsertUpdateDeleteEmailGroup_byUserId
        public ActionResult InsertUpdateDeleteEmailGroup_byUserId(GroupEmailMasterBase groupBase)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = adminDL.InsertUpdateDeleteEmailGroup_byUserId(groupBase);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    actionResult.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region EmailGroupList_byUserId
        public ActionResult EmailGroupList_byUserId(GroupEmailMasterBase groupBase)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = adminDL.EmailGroupList_byUserId(groupBase);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    actionResult.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region EmailGroup_LoadbyId
        public ActionResult EmailGroup_LoadbyId(GroupEmailMasterBase groupBase)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = adminDL.EmailGroup_LoadbyId(groupBase);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    actionResult.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region InsertUpdateGroupEmails_byGroupId
        public ActionResult InsertUpdateGroupEmails_byGroupId(GroupEmailsBase emailBase)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = adminDL.InsertUpdateGroupEmails_byGroupId(emailBase);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    actionResult.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region EmailsForGroup_LoadbyGroupId
        public ActionResult EmailsForGroup_LoadbyGroupId(GroupEmailsBase emailBase)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = adminDL.EmailsForGroup_LoadbyGroupId(emailBase);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    actionResult.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region Method Projects_UpdateDisplayOrder
        public ActionResult Projects_UpdateDisplayOrder(ProjectBase project)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = adminDL.Projects_UpdateDisplayOrder(project);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    actionResult.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region Method ProductProject_UpdateDisplayOrder
        public ActionResult ProductProject_UpdateDisplayOrder(ProjectBase project)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = adminDL.ProductProject_UpdateDisplayOrder(project);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    actionResult.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region Method Responses_Offline_CheckDuplicateId
        public ActionResult Responses_Offline_CheckDuplicateId(SRV.BaseLayer.Respondent.SurveyResponseBase SurveyResponse)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = adminDL.Responses_Offline_CheckDuplicateId(SurveyResponse);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    actionResult.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region Method ProductProject_UpdateMessageById
        public ActionResult ProductProject_UpdateMessageById(ProductProjectBase ProductBase)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = adminDL.ProductProject_UpdateMessageById(ProductBase);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    actionResult.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region Method Product_LoadById
        public ActionResult Product_LoadById(ProductProjectBase ProductBase)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = adminDL.Product_LoadById(ProductBase);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    actionResult.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region Response_DeleteBy_ProjectId
        public ActionResult Response_DeleteBy_ProjectId(ProjectBase projectBase)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = adminDL.Response_DeleteBy_ProjectId(projectBase);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    actionResult.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region Responders_Offline_DeleteBy_ProjectId
        public ActionResult Responders_Offline_DeleteBy_ProjectId(ProjectBase projectBase)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = adminDL.Responders_Offline_DeleteBy_ProjectId(projectBase);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    actionResult.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region Response_CaseSelectionReport
        public ActionResult Response_CaseSelectionReport(ProjectBase projectBase)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = adminDL.Response_CaseSelectionReport(projectBase);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    actionResult.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region Response_CaseSelectionReportOffline
        public ActionResult Response_CaseSelectionReportOffline(ProjectBase projectBase)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = adminDL.Response_CaseSelectionReportOffline(projectBase);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    actionResult.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region MatrixType_LoadByQuestionId
        public ActionResult MatrixType_LoadByQuestionId(QuestionBase questionBase)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dsResult = adminDL.MatrixType_LoadByQuestionId(questionBase);
                if (actionResult.dsResult != null && actionResult.dsResult.Tables[0].Rows.Count > 0)
                {
                    actionResult.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region UserDirectories_LoadByUserId
        public ActionResult UserDirectories_LoadByUserId(UserDirectoriesBase userDirectoriesBase)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = adminDL.UserDirectories_LoadByUserId(userDirectoriesBase);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    actionResult.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region UserDirectories_InsertUpdate
        public ActionResult UserDirectories_InsertUpdate(UserDirectoriesBase userDirectoriesBase)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = adminDL.UserDirectories_InsertUpdate(userDirectoriesBase);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    actionResult.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region UserDirectories_DeleteByUserId
        public ActionResult UserDirectories_DeleteByUserId(UserDirectoriesBase userDirectoriesBase)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = adminDL.UserDirectories_DeleteByUserId(userDirectoriesBase);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    actionResult.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region ProjectDirectories_LoadById
        public ActionResult ProjectDirectories_LoadById(ProjectDirectoriesBase projectDirectoriesBase)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = adminDL.ProjectDirectories_LoadById(projectDirectoriesBase);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    actionResult.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region ProjectDirectories_IU
        public ActionResult ProjectDirectories_IU(ProjectDirectoriesBase projectDirectoriesBase)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = adminDL.ProjectDirectories_IU(projectDirectoriesBase);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    actionResult.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region ProjectDirectories_LoadAll
        public ActionResult ProjectDirectories_LoadAll(ProjectBase projectBase)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dsResult = adminDL.ProjectDirectories_LoadAll(projectBase);
                if (actionResult.dsResult.Tables[0] != null && actionResult.dsResult.Tables[0].Rows.Count > 0)
                {
                    actionResult.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region PanelList_LoadAll
        public ActionResult PanelList_LoadAll(AdminBase adminBase)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = adminDL.PanelList_LoadAll(adminBase);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    actionResult.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region PanelProjects_InsertUpdate
        public ActionResult PanelProjects_InsertUpdate(ProjectBase projectBase)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = adminDL.PanelProjects_InsertUpdate(projectBase);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    actionResult.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region PanelProjects_LoadByProjectId
        public ActionResult PanelProjects_LoadByProjectId(ProjectBase projectBase)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = adminDL.PanelProjects_LoadByProjectId(projectBase);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    actionResult.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region ProjectDirectories_DeleteById
        public ActionResult ProjectDirectories_DeleteById(ProjectDirectoriesBase projectDirectories)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = adminDL.ProjectDirectories_DeleteById(projectDirectories);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    actionResult.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region IncompleteRespondersLoad_ByProjectId
        public ActionResult IncompleteRespondersLoad_ByProjectId(ProjectBase projectBase)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = adminDL.IncompleteRespondersLoad_ByProjectId(projectBase);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    actionResult.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region CompleteRespondersLoad_ByProjectId
        public ActionResult CompleteRespondersLoad_ByProjectId(ProjectBase projectBase)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = adminDL.CompleteRespondersLoad_ByProjectId(projectBase);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    actionResult.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region Project_LoadById
        public ActionResult Project_Load()
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = adminDL.Project_Load();
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    actionResult.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region ProjectRespose_Add
        public ActionResult ProjectRespose_Add(int projectId, string projectGUID, string Response,string ReportDataHtml)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = adminDL.ProjectRespose_Add(projectId, projectGUID, Response, ReportDataHtml);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    actionResult.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region ProjectRespose_LoadById
        public ActionResult ProjectRespose_LoadById(string projectGUID)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = adminDL.ProjectRespose_LoadById(projectGUID);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    actionResult.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region OnlineProjectRespose_Add
        public ActionResult OnlineProjectRespose_Add(int projectId, string projectGUID, string Response,string ReportDataHtml)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = adminDL.OnlineProjectRespose_Add(projectId, projectGUID, Response, ReportDataHtml);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    actionResult.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region OnlineProjectRespose_LoadById
        public ActionResult OnlineProjectRespose_LoadById(string projectGUID)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = adminDL.OnlineProjectRespose_LoadById(projectGUID);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    actionResult.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region CombinedProjectRespose_Add
        public ActionResult CombinedProjectRespose_Add(int projectId, string projectGUID, string Response,string ReportDataHtml)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = adminDL.CombinedProjectRespose_Add(projectId, projectGUID, Response, ReportDataHtml);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    actionResult.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region CombinedProjectRespose_LoadById
        public ActionResult CombinedProjectRespose_LoadById(string projectGUID)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = adminDL.CombinedProjectRespose_LoadById(projectGUID);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    actionResult.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region ReportQueryBuilder_Add
        public ActionResult ReportQueryBuilder_Add(int projectId, string projectGUID, string SessionType, string ViewBagType, string Response)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = adminDL.ReportQueryBuilder_Add(projectId, projectGUID, SessionType,ViewBagType,Response);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    actionResult.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region ReportQueryBuilder_LoadById
        public ActionResult ReportQueryBuilder_LoadById(int projectId, string projectGUID)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = adminDL.ReportQueryBuilder_LoadById(projectId, projectGUID);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    actionResult.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region ReportQueryBuilderCases_Add
        public ActionResult ReportQueryBuilderCases_Add(int projectId, string projectGUID, string CaseName, string OnlineResponse, string OfflineResponse, string CombineResponse)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = adminDL.ReportQueryBuilderCases_Add(projectId, projectGUID, CaseName, OnlineResponse, OfflineResponse, CombineResponse);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    actionResult.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region ReportQueryBuilderCases_LoadById
        public ActionResult ReportQueryBuilderCases_LoadById(int projectId, string projectGUID, string CaseName)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = adminDL.ReportQueryBuilderCases_LoadById(projectId, projectGUID, CaseName);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    actionResult.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region NotParticipateRes_Bind
        public ActionResult NotParticipateRes_Bind(AdminBase adminBase)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = adminDL.NotParticipateRes_Bind(adminBase);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    actionResult.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region DeletePoject_LoadAll
        public ActionResult DeletePoject_LoadAll(AdminBase adminBase)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = adminDL.DeletePoject_LoadAll(adminBase);
                if (actionResult != null)
                {
                    if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                    {
                        actionResult.IsSuccess = true;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region RecoverDeleteProject_ById
        public ActionResult RecoverDeleteProject_ById(ProjectBase projectBase)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = adminDL.RecoverDeleteProject_ById(projectBase);
                if (actionResult != null)
                {
                    if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                    {
                        actionResult.IsSuccess = true;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region ClusterProjectNameCheck
        public ActionResult ClusterProjectNameCheck(ProjectBase projectBase)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = adminDL.ClusterProjectNameCheck(projectBase);
                if (actionResult != null)
                {
                    if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                    {
                        actionResult.IsSuccess = true;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region OfflineSegmentationRespose_Add
        public ActionResult OfflineSegmentationRespose_Add(int projectId, string projectGUID, string Response, string ReportDataHtml)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = adminDL.OfflineSegmentationRespose_Add(projectId, projectGUID, Response, ReportDataHtml);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    actionResult.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region OfflineSegmentation_LoadById
        public ActionResult OfflineSegmentation_LoadById(string projectGUID)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = adminDL.OfflineSegmentation_LoadById(projectGUID);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    actionResult.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region OnlineSegmentationRespose_Add
        public ActionResult OnlineSegmentationRespose_Add(int projectId, string projectGUID, string Response, string ReportDataHtml)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = adminDL.OnlineSegmentationRespose_Add(projectId, projectGUID, Response, ReportDataHtml);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    actionResult.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region OnlineSegmentation_LoadById
        public ActionResult OnlineSegmentation_LoadById(string projectGUID)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = adminDL.OnlineSegmentation_LoadById(projectGUID);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    actionResult.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region CombineSegmentationRespose_Add
        public ActionResult CombineSegmentationRespose_Add(int projectId, string projectGUID, string Response, string ReportDataHtml)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = adminDL.CombineSegmentationRespose_Add(projectId, projectGUID, Response, ReportDataHtml);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    actionResult.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region CombinedSegmentationRespose_LoadById
        public ActionResult CombinedSegmentationRespose_LoadById(string projectGUID)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = adminDL.CombinedSegmentationRespose_LoadById(projectGUID);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    actionResult.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region SurveyNameCheck
        public ActionResult SurveyNameCheck(ProjectBase projectBase)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = adminDL.SurveyNameCheck(projectBase);
                if (actionResult != null)
                {
                    if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                    {
                        actionResult.IsSuccess = true;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region NotParticipateResProject_LoadAll
        public ActionResult NotParticipateResProject_LoadAll(AdminBase adminBase)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = adminDL.NotParticipateResProject_LoadAll(adminBase);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    actionResult.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region DeleteNoPeak_ById
        public ActionResult DeleteNoPeak_ById(ProjectBase projectBase)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = adminDL.DeleteNoPeak_ById(projectBase);
                if (actionResult != null)
                {
                    if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                    {
                        actionResult.IsSuccess = true;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

    }
}
