﻿using SRV.BaseLayer;
using SRV.BaseLayer.Admin;
using SRV.DataLayer.Admin;
using SRV.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SRV.ActionLayer.Admin
{
    public class UrlParameterAction
    {
        #region Declaration
        UrlParameterDL urlParameterDL = new UrlParameterDL();
        ActionResult actionResult = new ActionResult();
        #endregion

        #region UrlParameter_Insert
        public ActionResult UrlParameter_Insert(UrlParameterBase parameterBase)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = urlParameterDL.UrlParameter_Insert(parameterBase);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    actionResult.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region UrlParameter_LoadAll
        public ActionResult UrlParameter_LoadAll()
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = urlParameterDL.UrlParameter_LoadAll();
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    actionResult.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region ProjectDelete_ById
        public ActionResult ProjectDelete_ById(UrlParameterBase parameterBase)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = urlParameterDL.UrlParameter_Delete_ById(parameterBase);
                if (actionResult != null)
                {
                    if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                    {
                        actionResult.IsSuccess = true;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion
    }
}
