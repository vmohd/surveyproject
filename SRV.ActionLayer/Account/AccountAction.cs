﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SRV.BaseLayer;
using SRV.BaseLayer.Account;
using SRV.DataLayer.Account;
using SRV.Utility;

namespace SRV.ActionLayer.Account
{
    public class AccountAction
    {

        #region Declaration
        AccountDL accountDL = new AccountDL();
        ActionResult actionResult = new ActionResult();
        #endregion

        #region Login_Load
        public ActionResult Login_Load(AccountBase accountBase)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = accountDL.Login_Load(accountBase);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    actionResult.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region ForgotPassword
        public ActionResult ForgotPassword(AccountBase accountBase)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = accountDL.ForgotPassword(accountBase);
                if (actionResult != null)
                {
                    if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                    {
                        actionResult.IsSuccess = true;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region ResetPassword
        public ActionResult ResetPassword(AccountBase accountBase)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = accountDL.ResetPassword(accountBase);
                if (actionResult != null)
                {
                    if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                    {
                        actionResult.IsSuccess = true;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region get_app_info
        public ActionResult get_app_info()
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = accountDL.get_app_Info();
                if (actionResult != null)
                {
                    if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                    {
                        actionResult.IsSuccess = true;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region Insert USer_app_Info
        public ActionResult insert_user_app_Info(List<appInfoBase> app, long UserId)
        {
           
            actionResult = new ActionResult();
            try
            {
                foreach (var items in app)
                {
                    user_AppInfoBase userinfo = new user_AppInfoBase();
                    userinfo.UserId = UserId;
                    userinfo.appId = items.appId;
                    actionResult.dtResult = accountDL.insert_user_app_Info(userinfo);
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion
    }
}
