﻿using SRV.BaseLayer;
using SRV.BaseLayer.Panel;
using SRV.DataLayer.Panel;
using SRV.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SRV.ActionLayer.Panel
{
    public class PanelAction
    {
        #region Declaration
        PanelDL panelDL = new PanelDL();
        ActionResult actionResult = new ActionResult();
        #endregion

        #region Method PanelProjects_LoadByPanelId
        public ActionResult PanelProjects_LoadByPanelId(PanelBase panel)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = panelDL.PanelProjects_LoadByPanelId(panel);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    actionResult.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion
    }
}
