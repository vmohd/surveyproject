﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SRV.BaseLayer;
using SRV.BaseLayer.Admin;
using SRV.DataLayer.SuperAdmin;
using SRV.BaseLayer.SuperAdmin;
using SRV.DataLayer.Admin;
using SRV.Utility;
namespace SRV.ActionLayer.SuperAdmin
{
    public class SuperAdminAc
    {
        SuperAdminDl SuperAdminDl = new SuperAdminDl();
        ActionResult actionResult = new ActionResult();
        AdminDL adminDL = new AdminDL();

        #region Admin_AllList
        public ActionResult Admin_AllList()
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = SuperAdminDl.Admin_LoadList();
                if (actionResult != null)
                {
                    if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                    {
                        actionResult.IsSuccess = true;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region AdminUpdate_AllList
        public ActionResult AdminUpdate_AllList(AdminBase adminBaseReg)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = SuperAdminDl.AdminUpdate_LoadList(adminBaseReg);
                if (actionResult != null)
                {
                    if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                    {
                        actionResult.IsSuccess = true;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region AdminAccount_Delete
        public ActionResult AdminAccount_Delete(AdminBase adminBaseReg)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = SuperAdminDl.AdminAccount_Delete(adminBaseReg);
                if (actionResult != null)
                {
                    if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                    {
                        actionResult.IsSuccess = true;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region ChangeLogin_Status
        public ActionResult ChangeLogin_Status(AdminBase adminBaseReg)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = SuperAdminDl.ChangeLogin_Status(adminBaseReg);
                if (actionResult != null)
                {
                    if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                    {
                        actionResult.IsSuccess = true;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region method Register_InsertUpdate
        public ActionResult Register_InsertUpdate(AdminBase adminBaseReg)
        {
            actionResult = new ActionResult();
            try
            {
                SRV.DataLayer.Admin.AdminDL adminDL = new DataLayer.Admin.AdminDL();
                actionResult.dtResult = adminDL.Register_InsertUpdate(adminBaseReg);

                if (actionResult.dtResult != null)
                {
                    if (actionResult.dtResult.Rows.Count > 0)
                    {
                        actionResult.IsSuccess = true;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region Roles_LoadAll
        public ActionResult Roles_LoadAll()
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = SuperAdminDl.Roles_LoadAll();
                if (actionResult != null)
                {
                    if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                    {
                        actionResult.IsSuccess = true;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region InsertRoles_ByUserId
        public ActionResult InsertRoles_ByUserId(CreateRoleBase role)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = SuperAdminDl.InsertRoles_ByUserId(role);
                if (actionResult != null)
                {
                    if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                    {
                        actionResult.IsSuccess = true;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region UpdateRoles_ById
        public ActionResult UpdateRoles_ById(CreateRoleBase role)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = SuperAdminDl.UpdateRoles_ById(role);
                if (actionResult != null)
                {
                    if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                    {
                        actionResult.IsSuccess = true;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region RoleForUser_ByUserId
        public ActionResult RoleForUser_ByUserId(CreateRoleBase role)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = SuperAdminDl.RoleForUser_ByUserId(role);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    actionResult.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region RespondersLoad_ByProjectId
        public ActionResult RespondersLoad_ByProjectId(ProjectBase projectBase)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = adminDL.RespondersLoad_ByProjectId(projectBase);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    actionResult.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region ProjectSettings_LoadById
        public ActionResult ProjectSettings_LoadById(ProjectSettingsBase projectSettings)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = adminDL.ProjectSettings_LoadById(projectSettings);
                if (actionResult != null)
                {
                    if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                    {
                        actionResult.IsSuccess = true;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region Method ProjectSettings_InsertUpdate
        public ActionResult ProjectSettings_InsertUpdate(ProjectSettingsBase projectSettings)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = adminDL.ProjectSettings_InsertUpdate(projectSettings);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    actionResult.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region ProjectDelete_ById
        public ActionResult ProjectDelete_ById(ProjectBase project)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = adminDL.ProjectDelete_ById(project);
                if (actionResult != null)
                {
                    if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                    {
                        actionResult.IsSuccess = true;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region Password_bySuperAdminId
        public ActionResult Password_bySuperAdminId(AdminBase adminBase)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = SuperAdminDl.Password_bySuperAdminId(adminBase);
                if (actionResult != null)
                {
                    if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                    {
                        actionResult.IsSuccess = true;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region CreateUser
        public ActionResult CreateUser(AdminBase adminBaseReg)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = SuperAdminDl.CreateUser(adminBaseReg);
                if (actionResult != null)
                {
                    if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                    {
                        actionResult.IsSuccess = true;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion
    }
}
