﻿using SRV.BaseLayer;
using SRV.BaseLayer.Respondent;
using SRV.DataLayer.Respondent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SRV.BaseLayer.Admin;
using SRV.Utility;

namespace SRV.ActionLayer.Respondent
{
    public class RespondentAction
    {
        #region Declaration

        RespondentDL respondentDL = new RespondentDL();
        ActionResult actionResult = new ActionResult();
        #endregion

        #region Method Projects_LoadByResponderId
        public ActionResult Projects_LoadByResponderId(RespondentBase respondent)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = respondentDL.Projects_LoadByResponderId(respondent);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    actionResult.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region Method ProjectInvitation_LoadByGUID
        public ActionResult ProjectInvitation_LoadByGUID(SurveyLoadBase survey)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = respondentDL.ProjectInvitation_LoadByGUID(survey);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    actionResult.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region Method Response_LoadByProjectId
        public ActionResult Response_LoadByProjectId(SurveyLoadBase survey)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = respondentDL.Response_LoadByProjectId(survey);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    actionResult.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region Method Response_Offline_LoadBy_ProjectId_RespId
        public ActionResult Response_Offline_LoadBy_ProjectId_RespId(SurveyLoadBase survey)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = respondentDL.Response_Offline_LoadBy_ProjectId_RespId(survey);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    actionResult.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region Method Survey_LoadByGUID
        public ActionResult Survey_LoadByGUID(SurveyLoadBase survey)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = respondentDL.Survey_LoadByGUID(survey);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    actionResult.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region ProjQues_LoadByProjectId
        public ActionResult ProjQues_LoadByProjectId(SurveyLoadBase survey)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = respondentDL.ProjQues_LoadByProjectId(survey);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                    actionResult.IsSuccess = true;
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region Options_LoadAllByQuesId
        public ActionResult Options_LoadAllByQuesId(SurveyQuestionBase questionBase)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = respondentDL.Options_LoadAllByQuesId(questionBase);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    actionResult.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region SurveyResponse_Insert_Update
        public ActionResult SurveyResponse_Insert_Update(SurveyResponseBase surveyResponse)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = respondentDL.SurveyResponse_Insert_Update(surveyResponse);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    actionResult.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region ProjQuotaOther_LoadByProjectId
        public ActionResult ProjQuotaOther_LoadByProjectId(SurveyLoadBase survey)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = respondentDL.ProjQuotaOther_LoadByProjectId(survey);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                    actionResult.IsSuccess = true;
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region ProjectQuotaOther_Update
        public ActionResult ProjectQuotaOther_Update(SurveyResponseBase surveyResponseBase)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = respondentDL.ProjectQuotaOther_Update(surveyResponseBase);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    actionResult.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region ProjectQuotaOther_UpdateByExUser
        public ActionResult ProjectQuotaOther_UpdateByExUser(SurveyResponseBase surveyResponseBase)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = respondentDL.ProjectQuotaOther_UpdateByExUser(surveyResponseBase);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    actionResult.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region ConditionExpression_LoadByProjectId
        public ActionResult ConditionExpression_LoadByProjectId(ProjecQuotaBase quotaBase)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = respondentDL.ConditionExpression_LoadByProjectId(quotaBase);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                    actionResult.IsSuccess = true;
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region Language_LoadByProjectId
        public ActionResult Language_LoadByProjectId(LanguageBase languageBase)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = respondentDL.Language_LoadByProjectId(languageBase);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                    actionResult.IsSuccess = true;
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region ProjectInvitation_InsertRedirectUrl
        public ActionResult ProjectInvitation_InsertRedirectUrl(ProjectInvitationBase projectInvitation)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = respondentDL.ProjectInvitation_InsertRedirectUrl(projectInvitation);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                    actionResult.IsSuccess = true;
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region DDLMatrixTypeOptions_LoadByQuestionId
        public ActionResult DDLMatrixTypeOptions_LoadByQuestionId(SurveyQuestionBase surveyQuestionBase)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = respondentDL.DDLMatrixTypeOptions_LoadByQuestionId(surveyQuestionBase);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                    actionResult.IsSuccess = true;
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region PositionTable_LoadByIds
        public ActionResult PositionTable_LoadByIds(PositionTable positionTableBase)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dsResult = respondentDL.PositionTable_LoadByIds(positionTableBase);
                if (actionResult.dsResult != null && actionResult.dsResult.Tables.Count > 0)
                    actionResult.IsSuccess = true;
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region ProjectInvitation_LoadByID
        public ActionResult ProjectInvitation_LoadByID(ProjectInvitationBase projectInvitation)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = respondentDL.ProjectInvitation_LoadByID(projectInvitation);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                    actionResult.IsSuccess = true;
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion

        #region ResponseLoad_ByProjectId
        public ActionResult ResponseLoad_ByProjectId(ProjectBase project)
        {
            actionResult = new ActionResult();
            try
            {
                actionResult.dtResult = respondentDL.ResponseLoad_ByProjectId(project);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                    actionResult.IsSuccess = true;
            }
            catch (Exception ex)
            {
                ErrorReporting.ActionLayerError(ex);
            }
            return actionResult;
        }
        #endregion
    }
}
