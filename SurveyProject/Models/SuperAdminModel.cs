﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SurveyProject.Models
{
    public class AdminModel
    {

        public int Id { get; set; }

          [Required(ErrorMessage = "First name is required.")]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

         [Required(ErrorMessage = "Last name is required.")]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

         [Required(ErrorMessage = "Email is required.")]
         [RegularExpression(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$", ErrorMessage = " Please enter valid email.")]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required(ErrorMessage="Gender is Required.")]
        [Display(Name = "Gender")]
        public bool Gender { get; set; }
        public bool Status { get; set; }
        public bool isdeleted { get; set; }
    }
    public class ManageAdminModel
    {
        public CreateRolesModel createRoleModel { get; set; }
        public List<AdminModel> Adminlist { get; set; }
        public List<CreateRolesModel> roleList { get; set; }
    }
    public class CreateRolesModel
    {
        public int Id { get; set; }
        public int RoleId { get; set; }

        [Required(ErrorMessage = "Please select User.")]
        [Display(Name = "Users")]
        public int UserId { get; set; }

        public string RoleName { get; set; }
        public string ActionName { get; set; }

        public List<SelectListItem> UserList { get; set; }
    }

    public class ChangePasswordModel
    {
        [Required(ErrorMessage = "New Password is required.")]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "New Password")]
        public string NewPassword { get; set; }

        [Display(Name = "Old Password")]
        [Required(ErrorMessage = "Old Password is required.")]
        public string OldPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm Password")]
        [System.Web.Mvc.Compare("NewPassword", ErrorMessage = "The Password and Confirmation Password does not match.")]
        public string ConfirmPassword { get; set; }
    }
}