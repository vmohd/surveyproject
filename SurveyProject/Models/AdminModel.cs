﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;



namespace SurveyProject.Models
{

    public class AdminRegisterModel
    {

        [Required(ErrorMessage = "{0} is required.")]
        [StringLength(30, ErrorMessage = "The {0} must be at least {2} characters long and less than {1} characters.", MinimumLength = 8)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Display(Name = "Old Password")]
        [Required(ErrorMessage = "{0} is required.")]
        public string OldPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm Password")]
        [Compare("Password", ErrorMessage = "{1} and {0} does not match.")]
        public string ConfirmPassword { get; set; }

        [Required(ErrorMessage = "{0} is required.")]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "{0} is required.")]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "{0} is required.")]
        [RegularExpression(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$", ErrorMessage = " Please enter valid email.")]
        [Display(Name = "Email")]
        public string Email { get; set; }


        [Display(Name = "Gender")]
        public bool Gender { get; set; }



        [Display(Name = "Security Question")]
        [Required(ErrorMessage = "Security Question is required.")]
        public string SecurityQuestion { get; set; }

        [Display(Name = "Answer to Security Question")]
        [Required(ErrorMessage = "Answer to Security Question is required.")]
        public string AnswerToSecurityQuestion { get; set; }

        [Display(Name = "City")]
        public string City { get; set; }

        [Display(Name = "State")]
        public string StateId { get; set; }

        [Display(Name = "Country of Residence")]
        [Required(ErrorMessage = "Country is required.")]
        public int CountryOfResidenceId { get; set; }

        [Display(Name = "Country")]
        public string CountryId { get; set; }

        [Display(Name = "Language")]
        [Required(ErrorMessage = "Language is required.")]
        public int LanguageId { get; set; }

        [Display(Name = "Zip Code")]
        public string ZipCode { get; set; }
        public int Id { get; set; }
        public bool status { get; set; }

        [Display(Name = "Company Name")]
        public string CompanyName { get; set; }

        [Display(Name = "Company/School Address")]
        public string Address { get; set; }

        [Display(Name = "Phone Number")]
        public string PhoneNumber { get; set; }

        [Display(Name = "E-mail")]
        public bool sendEmail { get; set; }

        [Display(Name = "Other Communications")]
        public bool OtherCommunications { get; set; }

        public List<AdminRegisterModel> AdminList { get; set; }
        public List<System.Web.Mvc.SelectListItem> CountryList { get; set; }
        public List<System.Web.Mvc.SelectListItem> StateList { get; set; }
        public List<System.Web.Mvc.SelectListItem> LanguageList { get; set; }
        public List<appInfo> appInfo { get; set; }
        public string UserGuid { get; set; }
        public string appid { get; set; }

    }

    public class CreateProjectModel
    {
        public int Id { get; set; }
        public string ProjectGuid { get; set; }
        public string InvitationGuid { get; set; }

        [Required(ErrorMessage = "Language is required.")]
        [Display(Name = "Select Language")]
        public int LanguageId { get; set; }

        [Required(ErrorMessage = "Project title is required.")]
        [Display(Name = "Project Title")]
        public string ProjectName { get; set; }


        [Required(ErrorMessage = "Welcome message is required.")]
        [Display(Name = "Welcome Messsage")]
        public string WelcomeMessage { get; set; }

        public int OwnerId { get; set; }

        public int CreatedBy { get; set; }

        [Display(Name = "Creation Date")]
        public string CreatedOn { get; set; }

        [Display(Name = "Modified Date")]
        public string ModifiedOn { get; set; }

        public int ModifiedBy { get; set; }

        [Required(ErrorMessage = "Project quota is required.")]
        [Display(Name = "Project Quota")]
        public int ProjectQuota { get; set; }

        [Required(ErrorMessage = "Project quota question is required.")]
        [Display(Name = "Project Quota Question")]
        public string QuotaQues { get; set; }

        [Required(ErrorMessage = "Page header is required.")]
        [Display(Name = "Page Header")]
        public string PageHeader { get; set; }

        [Required(ErrorMessage = "Page footer is required.")]
        [Display(Name = "Page Footer")]
        public string PageFooter { get; set; }

        [Required(ErrorMessage = "Completion message is required.")]
        [Display(Name = "Completion Messsage")]
        public string ProjectCompletionMesssage { get; set; }

        //[Required(ErrorMessage = "Redirect Url is required.")]
        [RegularExpression(@"^(http|https|ftp)\://([a-zA-Z0-9\.\-]+(\:[a-zA-Z0-9\.&amp;%\$\-]+)*@)*((25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9])\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[0-9])|localhost|([a-zA-Z0-9\-]+\.)*[a-zA-Z0-9\-]+\.(com|edu|gov|int|mil|net|org|biz|arpa|info|name|pro|aero|coop|museum|[a-zA-Z]{2}))(\:[0-9]+)*(/($|[a-zA-Z0-9\.\,\?\'\\\+&amp;%\$#\=~_\-]+))*$", ErrorMessage = " Please enter valid Url Address")]
        [Display(Name = "Redirect Url")]
        public string RedirectUrl { get; set; }

        [Required(ErrorMessage = "Invitation message is required.")]
        [Display(Name = "Invitation Message")]
        public string InvitationMessage { get; set; }

        public string Owner { get; set; }
        public string ModifiedByName { get; set; }
        public string CreatedByName { get; set; }

        public bool IsActive { get; set; }
        public bool TestMode { get; set; }
        public string TerminationMessageUpper { get; set; }
        public string TerminationMessageLower { get; set; }

        [Required(ErrorMessage = "Project Type is required.")]
        public string ProjectType { get; set; }

        public int SurveyLink { get; set; }
        public int DisplayOrder { get; set; }

        public int SuperProjectId { get; set; }
        public string SuperProjectName { get; set; }
        public string SuperProjectOpeningDate { get; set; }
        public string SuperProjectClosingDate { get; set; }
        public bool SuperProjectIsRandomizeSurveys { get; set; }

        public int SurveyType { get; set; }
        public int ProductsCount { get; set; }

        public string DeleteDate { get; set; }

        public List<CreateProjectModel> ProjectList { get; set; }
        public ProductProjectModel ProductProjectModel { get; set; }
        public List<ProductProjectModel> lstProductProjectModel { get; set; }
        public bool IsAllProductsResponded { get; set; }

        public bool IsIncludePageHeader { get; set; }
        public bool IsIncludePageFooter { get; set; }
        public bool IsIncludeWelcomeMessage { get; set; }
        public bool IsIncludeProjectCompletionMesssage { get; set; }
        public bool IsIncludeTerminationMessage { get; set; }
        public string ProductProjectXML { get; set; }
        public string ProductMessage { get; set; }
        public int DirectoryId { get; set; }
        public int DirectoryIdCol { get; set; }
        public string RedirectParameters { get; set; }
        public bool IsRedirectParameter { get; set; }
        public bool IsIncludeFinishImage { get; set; }
        public string CloseButtonText { get; set; }

        public List<QueryModel> QueryList { get; set; }

        public string NotParticipateRespondent { get; set; }

    }

    public class ProjectSettingsModel
    {
        public int Id { get; set; }
        public int ProjectId { get; set; }
        public string ProjectGUID { get; set; }

        [Display(Name = "Opening Date")]
        [Required(ErrorMessage = "opening date is required.")]
        public string OpeningDate { get; set; }

        [Display(Name = "Closing Date")]
        public string ClosingDate { get; set; }

        [Display(Name = "Disable Question Numbering")]
        public bool DisableQuestionNumbering { get; set; }

        public bool TestMode { get; set; }

        [Display(Name = "Active?")]
        public bool IsActive { get; set; }

        [Display(Name = "Scored?")]
        public bool Scored { get; set; }

        [Display(Name = "Previous Next Page Navigation?")]
        public bool PrevNextPageNavigation { get; set; }

        [Display(Name = "Resume Of Progress?")]
        public bool ResumeOfProgress { get; set; }

        public int ProjectQuota { get; set; }

        [Display(Name = "Online Responses")]
        public string OnlineResponses { get; set; }

        [Display(Name = "Offline Responses")]
        public string OfflineResponses { get; set; }

        public bool IsQuesRandomize { get; set; }

        public bool IsProductRandomize { get; set; }
        public int SuperProjectId { get; set; }
        public List<ProductProjectModel> lstProductProjectModel { get; set; }
        public int ProjectType { get; set; }

        public string IncompleteResponses { get; set; }
    }

    public class QueryModel
    {
        public int Id { get; set; }
        public string Query { get; set; }
        public string QueryHtml { get; set; }  
        public string CaseName { get; set; }        
        public int ProjectId { get; set; }
    }

    public class CreateQuestionModel
    {
        public int ProjectId { get; set; }

        public int QuestionId { get; set; }

        public string QuestionType { get; set; }

        [Required(ErrorMessage = "Question Type is required.")]
        public int QuestionTypeId { get; set; }

        [Required(ErrorMessage = "Please enter Question Title.")]
        public string QuestionTitle { get; set; }

        public int QuestionDisplayOrder { get; set; }

        public List<CreateQuestionModel> QuestionList { get; set; }

        public List<QueryModel> QueryList { get; set; }

        public string RedirectUrl { get; set; }

        public string OptionsCaption { get; set; }

        public List<MatrixTypeModel> lstMatrixModel { get; set; }

        public List<DDLMatrixTypeModel> lstDDlMatrixModel { get; set; }

        public int QuestionGruopId { get; set; }

        public bool isAnswersPiped { get; set; }
        public bool isAnswersExplicating { get; set; }
        [Required(ErrorMessage = "Piped/Expliciting from Question is required.")]
        public int PipedFrom { get; set; }
        // [Required(ErrorMessage = "Expliciting from Question is required.")]
        public int ExplicatingFrom { get; set; }
        public string QuestionTag { get; set; }
        public bool IsOptRandomize { get; set; }
        public string RedirectPassCode { get; set; }
        public string RedirectQuesParameters { get; set; }
        public bool IsRedirectParameter { get; set; }
    }

    public class CreateOptionsModel
    {
        [Required(ErrorMessage = "Question is required.")]
        public int QuestionId { get; set; }

        public bool IsConditional { get; set; }

        public string Option { get; set; }

        [Required(ErrorMessage = "Please enter Start Range.")]
        public string StartRange { get; set; }

        [Required(ErrorMessage = "Please enter End Range.")]
        public string EndRange { get; set; }

        [Required(ErrorMessage = "Please enter Constant Sum.")]
        public string TotalValue { get; set; }

        public List<QuotaListModel> QuotaList { get; set; }

        public string OptionRedirectUrl { get; set; }
    }

    public class QuotaListModel
    {
        public int QuotaId { get; set; }

        public string QutotaType { get; set; }
    }

    public class OptionListModel
    {
        public int Id { get; set; }

        public string OptionTitle { get; set; }

        public string ConditionExpression { get; set; }

        public string DisplayOrder { get; set; }
    }

    public class ProjectMailingModel
    {
        public int Id { get; set; }

        public int ProjectId { get; set; }

        [Required(ErrorMessage = "From Name is required.")]
        [Display(Name = "From :")]
        public string FromName { get; set; }

        [Required(ErrorMessage = "Subject is required.")]
        [Display(Name = "Subject:")]
        public string subject { get; set; }

        [Required(ErrorMessage = "Invitation Message is required.")]
        [Display(Name = "Invitation Message :")]
        public string InvitationMessage { get; set; }

        [Required(ErrorMessage = "Invitation List is required.")]
        [Display(Name = "Invitation List:")]
        public string InvitationList { get; set; }
        public int SuperProjectId { get; set; }
    }

    public class AnswersValue
    {
        public int QuestionId { get; set; }
    }

    public class AnswersValueWithQuota
    {
        public int QuestionId { get; set; }
        public int optionId { get; set; }
    }

    public class AnswersValueMatrix
    {
        public string QuestionId { get; set; }
        public string TerminationCondition { get; set; }
    }

    public class AnswersValueWithQuotaMatrix
    {
        public string QuestionId { get; set; }
        public string optionId { get; set; }
        public string TerminationCondition { get; set; }
    }

    public class ManageAnswerValuesMatrix
    {
        public int ProjectId { get; set; }

        public List<AnswersValueMatrix> answerValueList { get; set; }
        public List<AnswersValueWithQuotaMatrix> answerValueWithQuotaList { get; set; }
    }

    public class ManageAnswerValues
    {
        public int ProjectId { get; set; }

        public List<AnswersValue> answerValueList { get; set; }
        public List<AnswersValueWithQuota> answerValueWithQuotaList { get; set; }
    }

    public class ProjectQuotaOther
    {
        private int _id = 0;
        private int _projectId = 0;
        private string _quotaType = string.Empty;
        private decimal _quotaPerc = 0;
        private int _maxCount = 0;
        private int _quotaJump = 0;

        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }
        public int ProjectId
        {
            get { return _projectId; }
            set { _projectId = value; }
        }
        public string QuotaType
        {
            get { return _quotaType; }
            set { _quotaType = value; }
        }
        public decimal QuotaPerc
        {
            get { return _quotaPerc; }
            set { _quotaPerc = value; }
        }
        public int MaxCount
        {
            get { return _maxCount; }
            set { _maxCount = value; }
        }
        public int QuotaJump
        {
            get { return _quotaJump; }
            set { _quotaJump = value; }
        }
    }

    public class ProjectStatsModel
    {
        [Display(Name = "Opening Date")]
        [Required(ErrorMessage = "opening date is required.")]
        public string OpeningDate { get; set; }

        [Display(Name = "Closing Date")]
        public string ClosingDate { get; set; }
        [Display(Name = "Active")]
        public bool IsActive { get; set; }

        [Required(ErrorMessage = "Survey title is required.")]
        [Display(Name = "Project Title")]
        public string ProjectName { get; set; }


        [Display(Name = "Creation Date")]
        public string CreatedOn { get; set; }
    }

    public class InvitationList
    {
        #region Declaration

        private string _email = string.Empty;
        private string _invitationId = string.Empty;
        private string _projectGuid = string.Empty;
        #endregion


        #region Properties
        public string Email
        {
            get { return _email; }
            set { _email = value; }
        }
        public string InvitationGUID
        {
            get { return _invitationId; }
            set { _invitationId = value; }
        }
        public string ProjectGUID
        {
            get { return _projectGuid; }
            set { _projectGuid = value; }
        }
        #endregion


    }

    public class ManageRespondersModel
    {
        public List<RespondersModel> lstRespondersModel { get; set; }
    }

    public class RespondersModel
    {
        public int Id { get; set; }
        public string ProjectGuid { get; set; }
        public string InvitationGuid { get; set; }
        public string Email { get; set; }
        public string CreatedOn { get; set; }
        public string ModifiedOn { get; set; }
        public string OfflineResponderId { get; set; }
        public int SuperProjectId { get; set; }
        public string SuperProjectName { get; set; }
        public string Source { get; set; }
    }

    public class MatrixTypeModel
    {
        public int Id { get; set; }
        public int QuestionId { get; set; }
        public int GroupId { get; set; }
        public string ColumnName { get; set; }
        public string Type { get; set; }
        public int DisplayOrder { get; set; }
        public bool IsActive { get; set; }

    }

    public class MatrixTypeGroupModel
    {
        public int Id { get; set; }
        public string GroupCaption { get; set; }
        public int GroupCaptionDisplayOrder { get; set; }
    }

    public class DDLMatrixTypeModel
    {
        public int Id { get; set; }
        public int QuestionId { get; set; }
        public string OptionName { get; set; }
        public string CreatedOn { get; set; }
        public string ModifiedOn { get; set; }
        public int DDLDisplayOrder { get; set; }
    }

    public class ProjectQuestionGroupsModel
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Group Name is required.")]
        public string GroupName { get; set; }
        public int ProjectId { get; set; }
        public string GroupDescription { get; set; }
        public string CreatedOn { get; set; }
        public string ModifiedOn { get; set; }
        public string GroupQuestions { get; set; }
        public int DisplayOrder { get; set; }
        public List<ProjectQuestionGroupsModel> groupList { get; set; }
    }

    public class ManageGroupRandomizationModel
    {
        public List<ProjectQuestionGroupsModel> groupList { get; set; }
        public List<CreateQuestionModel> questionList { get; set; }
        public List<PositionTableModel> positionList { get; set; }
        public List<ProductProjectModel> productProjectList { get; set; }
    }

    public class ProductProjectModel
    {
        public int PositionTableId { get; set; }
        public int Id { get; set; }
        public int ProjectId { get; set; }
        public int ProductLogicalId { get; set; }
        public string Description { get; set; }
        public int DisplayOrder { get; set; }
        public string CreatedDate { get; set; }
        public string ModifiedDate { get; set; }
        public string ProductMessage { get; set; }
    }

    public class PositionTableModel
    {
        public int Id { get; set; }
        public string ResponderId { get; set; }
        public int SuperSurveyId { get; set; }
        public int SurveyId { get; set; }
        public string ProjectGuid { get; set; }
        public int ProductId { get; set; }
        public bool IsProcessed { get; set; }
        public string ProjectName { get; set; }
        public string ProductName { get; set; }
        public string SuperSurveyName { get; set; }
        public int SurveyDispOrder { get; set; }
        public int ProductDispOrder { get; set; }
    }
    public class ManageProjectModel
    {
        public List<SuperProjectModel> lstSuperProjectModel { get; set; }

        public List<CreateProjectModel> lstProjectModel { get; set; }
        //public List<UserDirectoriesModel> lstUserDirectoryModel { get; set; }
    }

    public class SuperProjectModel
    {
        public int Id { get; set; }
        public string ProjectName { get; set; }
        public string OpeningDate { get; set; }
        public int DirectoryId { get; set; }
        public int LanguageId { get; set; }
        public int OwnerId { get; set; }
        public string CreatedOn { get; set; }
        public string ModifiedOn { get; set; }
        public bool IsRandomizeSurveys { get; set; }
        public int DirectoryIdCol { get; set; }
        public List<CreateProjectModel> ProjectListModel { get; set; }
    }

    public class ManageGroupEmailModel
    {
        public List<GroupEmailMasterModel> EmailGroupsList { get; set; }
        public List<GroupEmailsModel> EmailList { get; set; }
        public GroupEmailMasterModel groupEmailMasterModel { get; set; }
        public GroupEmailsModel groupEmailModel { get; set; }
    }
    public class GroupEmailMasterModel
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        [Required(ErrorMessage="Group Name is required.")]
        public string GroupName { get; set; }
        public string CreatedOn { get; set; }
        public string ModifiedOn { get; set; }
    }

    public class GroupEmailsModel
    {
        public int Id { get; set; }
        public int GroupId { get; set; }
        public string Email { get; set; }
        public string CreatedOn { get; set; }
        public string ModifiedOn { get; set; }
    }

    public class ProjectDirectoriesModel
    {
        public int Id { get; set; }
        public int ProjectId { get; set; }
        public int DirectoryId { get; set; }
        public string CreatedDate { get; set; }
        public string ModifiedDate { get; set; }
        public string DirName { get; set; }
        public int UserId { get; set; }
    }

    public class UserDirectoriesModel
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string DirName { get; set; }
        public string CreatedDate { get; set; }
        public string ModifiedDate { get; set; }
        public int AssignCount { get; set; }
        public List<ProjectDirectoriesModel> lstProjectDirectoryModel { get; set; }

    }
}