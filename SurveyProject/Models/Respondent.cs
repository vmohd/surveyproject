﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SurveyProject.Models
{
    public class SurveyResponseModel
    {
        public bool isError { get; set; }
        public string ErrorMessage { get; set; }
        public CreateProjectModel projectModel { get; set; }
        public ProjectSettingsModel projectSettingsModel { get; set; }
        public List<ProjectQuotaOther> lstProjectQuotaOtherModel { get; set; }
        public List<QuestionModel> lstQuestionModel { get; set; }
        public Responder responder { get; set; }
        public CreateQuestionModel question { get; set; }
        public List<CreateProjectModel> lstCreateProjectModel { get; set; }
        public string pnlId { get; set; }
        public List<Query_Model> QueryList { get; set; }
    }

    public class Query_Model
    {
        public string Query { get; set; }
        public string CaseName { get; set; }
        public int ProjectId { get; set; }
    }

    public class Responder
    {
        public int ResponderId { get; set; }
        public string ResponderEmail { get; set; }
        public string OfflineResponderId { get; set; }
    }

    public class QuestionModel 
    {
        public int ProjectId { get; set; }
        public int QuestionId { get; set; }
        public string QuestionType { get; set; }
        public int QuestionTypeId { get; set; }
        public string QuestionTitle { get; set; }
        public string ConditionalExpression { get; set; }
        public int CreatedBy { get; set; }
        public int ModifiedBy { get; set; }
        public int RangeMin { get; set; }
        public int RangeMax { get; set; }
        public int ConstantSum { get; set; }
        public int DisplayOrder { get; set; }
        public string RedirectUrl { get; set; }
        public string OptionsCaption { get; set; }
        public bool isAnswersPiped { get; set; }
        public bool isAnswersExplicating { get; set; }
        public int PipedFrom { get; set; }
        public int ExplicatingFrom { get; set; }
        public List<OptionsModel> lstOptionsModel { get; set; }
        public List<MatrixTypeModel> lstMatrixTypeModel { get; set; }
        public List<MatrixTypeGroupModel> lstMatrixTypeGroup { get; set; }
      public List<DDLMatrixTypeModel> lstDDlMatrixModel { get; set; }
      public bool IsOptRandomize {get;set; }
      public string RedirectQuesParameters { get; set; }
      public bool IsRedirectParameter { get; set; }
        //public IEnumerable<DDLMatrixTypeModel> lstDDlMatrixModel { get; set; }
    }

    public class OptionsModel
    {
        public int QuestionId { get; set; }
        public int OptionId { get; set; }
        public int DisplayOrder { get; set; }
        public bool IsConditional { get; set; }
        public string Option { get; set; }
        public bool IsSkip { get; set; }
        public int SkipQuestionId { get; set; }
        public string MaxResponsesCount { get; set; }
        public string OptionRedirectUrl { get; set; }
    }
}