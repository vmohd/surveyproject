﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SurveyProject.Models
{
    public class PanelModels
    {
        public List<SurveyPanelModels> lstSurveyPanelModel { get; set; }
    }

    public class SurveyPanelModels
    {
        public int Id { get; set; }
        public int ProjectId { get; set; }
        public int SuperProjectId { get; set; }
        public int PanelId { get; set; }
        public int UserId { get; set; }
        public int LanguageId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string CreatedOn { get; set; }
        public string ModifiedOn { get; set; }
    }
}