﻿using SRV.ActionLayer.Admin;
using SRV.ActionLayer.Respondent;
using SRV.BaseLayer.Admin;
using SRV.BaseLayer.Respondent;
using SRV.Utility;
using SurveyProject.Controllers;
using SurveyProject.Helper;
using SurveyProject.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Services;
using System.Xml;
using System.Xml.Linq;

namespace SurveyProject.ParallelService
{
    /// <summary>
    /// Summary description for ParallelService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class ParallelService : System.Web.Services.WebService
    {

        #region Declaration
        QuestionBase questionBase = new QuestionBase();
        SRV.BaseLayer.ActionResult actionResult = new SRV.BaseLayer.ActionResult();
        AdminAction adminAction = new AdminAction();
        CommonMethods comMethods = new CommonMethods();
        RespondentAction respondentAction = new RespondentAction();
        ProjectBase projectBase = new ProjectBase();
        QueryBase queryBase = new QueryBase();
        ProductProjectBase productProjectBase = new ProductProjectBase();
        #endregion


        #region  ResponseAsync
        [WebMethod]
        public void ResponseAsync()
        {
            int RespCount = 0;

            SRV.BaseLayer.ActionResult actionResultProjects = new SRV.BaseLayer.ActionResult();
            AdminAction adminAction = new AdminAction();

            actionResultProjects = adminAction.Project_Load();
            if (actionResultProjects.dtResult != null && actionResultProjects.dtResult.Rows.Count > 0)
            {
                Task[] offlineTask = new Task[actionResultProjects.dtResult.Rows.Count];
                Task[] onlineTask = new Task[actionResultProjects.dtResult.Rows.Count];
                Task[] combinedTask = new Task[actionResultProjects.dtResult.Rows.Count];

                Task[] offlineSegmentationTask = new Task[actionResultProjects.dtResult.Rows.Count];
                Task[] onlineSegmentationTask = new Task[actionResultProjects.dtResult.Rows.Count];
                Task[] combinedSegmentationTask = new Task[actionResultProjects.dtResult.Rows.Count];
                
                Task[] queryBuilderTask = new Task[actionResultProjects.dtResult.Rows.Count];
                int i = 0;

                foreach (DataRow dr in actionResultProjects.dtResult.Rows)
                {
                    int projectId = dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0;
                    string projectGUID = dr["ProjectGUID"] != DBNull.Value ? dr["ProjectGUID"].ToString() : "";

                    offlineTask[i] = Task.Factory.StartNew(() =>
                    {
                        GetReportDataTableOffline(projectGUID, (RespCount + 1), projectId);
                    });
                    offlineTask[i].Wait();

                    onlineTask[i] = Task.Factory.StartNew(() =>
                    {
                        GetReportDataTableOnline(projectGUID, projectId);
                    });
                    onlineTask[i].Wait();

                    combinedTask[i] = Task.Factory.StartNew(() =>
                    {
                        GetReportDataTableCombined(projectGUID, projectId);
                    });
                    combinedTask[i].Wait();

                    queryBuilderTask[i] = Task.Factory.StartNew(() =>
                    {
                        GetQueryReportDataTable_Load(projectId, projectGUID);
                    });
                    queryBuilderTask[i].Wait();


                    // Segmentation
                    offlineSegmentationTask[i] = Task.Factory.StartNew(() =>
                    {
                        GetReportDataTableOfflineSegmentation(projectGUID, (RespCount + 1), projectId);
                    });
                    offlineSegmentationTask[i].Wait();

                    onlineSegmentationTask[i] = Task.Factory.StartNew(() =>
                    {
                        GetReportDataTableOnlineSegmentation(projectGUID, projectId);
                    });
                    onlineSegmentationTask[i].Wait();

                    combinedSegmentationTask[i] = Task.Factory.StartNew(() =>
                    {
                        GetSegmentationReportCombined(projectGUID, projectId);
                    });
                    combinedSegmentationTask[i].Wait();

                    i++;
                }
            }
        }
        #endregion

        // Report
        #region GetReportDataTableCombined
        [WebMethod]
        public void GetReportDataTableCombined(string projectGUID, int projectId)
        {
            int RespCount = 0;
            DataTable dtOnline = new DataTable();
            DataTable dtOffline = new DataTable();
            dtOnline = GetRepOnline(projectGUID, projectId);
            RespCount = (from DataRow dRow in dtOnline.Rows select dRow["Resp_Id"]).Distinct().Count();
            dtOffline = GetRepOffline(projectGUID, (RespCount + 1), projectId);
            dtOnline.Merge(dtOffline);

            if (dtOnline.Columns.Count > 0)
            {
                string ReportDataHtml = "<table id='hdnReportHtml'>";
                DataTable dtReport = dtOnline;
                if (dtReport != null)
                {
                    string[] cols = (from dc in dtReport.Columns.Cast<DataColumn>()
                                     select dc.ColumnName)
                                   .ToArray();
                    if (cols != null)
                    {
                        ReportDataHtml += "<tr class='thRow'>";
                        foreach (var item in cols)
                        {
                            ReportDataHtml += "<th>" + item + "</th>";
                        }
                        ReportDataHtml += "</tr>";
                    }

                    foreach (DataRow row in dtReport.Rows)
                    {
                        ReportDataHtml += "<tr class='tdRow'>";
                        foreach (var item in row.ItemArray)
                        {
                            ReportDataHtml += "<td>" + item + "</td>";
                        }
                        ReportDataHtml += "</tr>";
                    }
                }
                ReportDataHtml += "</table>";

                string responseXML = ConvertDatatableToXML(dtOnline);
                actionResult = adminAction.CombinedProjectRespose_Add(projectId, projectGUID, responseXML, ReportDataHtml);
            }
        }
        #endregion

        #region GetReportDataTableOnline
        [WebMethod]
        public void GetReportDataTableOnline(string Id, int proId)
        {
            RespondentController respondent = new RespondentController();
            SurveyResponseModel model = new SurveyResponseModel();
            DataTable dtF = new DataTable();
            DataTable newData = new DataTable();
            try
            {
                DataTable dt = new DataTable();

                string projectGuid = string.Empty;
                projectGuid = Id;
                dt.Columns.Add("Resp_Id", typeof(string));

                model = respondent.GetProjectPreview(projectGuid);

                //--ss
                if (model.projectModel.ProjectType == "2")
                {
                    DataRow drnew = dt.NewRow();
                    drnew[0] = "Product_LogicalId";
                    dt.Rows.Add(drnew);
                    DataRow drnew1 = dt.NewRow();
                    drnew1[0] = "Product_Name";
                    dt.Rows.Add(drnew1);
                }
                //--

                // Load all the questions of this project
                List<QuestionList> questionsList = new List<QuestionList>();
                questionBase.ProjectId = Convert.ToInt32(model.projectModel.Id);
                actionResult = adminAction.QuestionsLoadAll_ByProjectId(questionBase);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    foreach (DataRow dr in actionResult.dtResult.Rows)
                    {
                        questionsList.Add(new QuestionList
                        {
                            id = dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0,
                            questionTypeId = dr["QuestionTypeId"] != DBNull.Value ? Convert.ToInt32(dr["QuestionTypeId"]) : 0
                        });
                    }
                }

                string fileName = Guid.NewGuid().ToString().Substring(0, 5) + "_" + DateTime.Now.ToString("MM-dd-yyyy") + "_" + model.projectModel.ProjectName.Replace(" ", "");

                if (model.lstQuestionModel != null && model.lstQuestionModel.Count > 0)
                {
                    foreach (var item in model.lstQuestionModel)
                    {
                        if (item.PipedFrom > 0 && (!item.QuestionTitle.Contains("{{Q}}")))
                        {
                            if (item.QuestionTypeId == 2 || item.QuestionTypeId == 4 || item.QuestionTypeId == 5 || item.QuestionTypeId == 6)//Multi Punch || Constant Sum || Ranking || Matrix Type
                            {
                                if (model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel != null && model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel.Count > 0)
                                {
                                    foreach (var opt in model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel)
                                    {
                                        DataRow drnew = dt.NewRow();
                                        drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                        dt.Rows.Add(drnew);
                                    }
                                }
                            }

                            else if (item.QuestionTypeId == 1 || item.QuestionTypeId == 3 || item.QuestionTypeId == 7 || item.QuestionTypeId == 15)//Single Punch || Range || Simple Text
                            {
                                DataRow drnew = dt.NewRow();
                                drnew[0] = "Q" + item.QuestionId + "_S" + "-" + ReplaceHtml.ReplaceText(item.QuestionTitle);
                                dt.Rows.Add(drnew);
                            }
                            else if (item.QuestionTypeId == 8 || item.QuestionTypeId == 12)//Single punch matrix type  && Rank order type
                            {
                                //if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
                                //{
                                //foreach (var opt in item.lstOptionsModel)
                                //{
                                //    DataRow drnew = dt.NewRow();
                                //    //drnew[0] = "Q" + item.QuestionId + "_M" + opt.OptionId + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                //    drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                //    dt.Rows.Add(drnew);
                                //}
                                //}
                                if (item.lstMatrixTypeModel != null && item.lstMatrixTypeModel.Count > 0)
                                {
                                    foreach (var col in item.lstMatrixTypeModel)
                                    {
                                        DataRow drnew = dt.NewRow();
                                        //drnew[0] = "Q" + item.QuestionId + "_M" + opt.OptionId + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                        drnew[0] = "Q" + item.QuestionId + "_M" + col.DisplayOrder + "-" + ReplaceHtml.ReplaceText(col.ColumnName);
                                        dt.Rows.Add(drnew);
                                    }
                                }
                            }
                            else if (item.QuestionTypeId == 13)//Kano model matrix type 
                            {
                                if (model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel != null && model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel.Count > 0)
                                {
                                    foreach (var opt in model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel)
                                    {
                                        DataRow drnew = dt.NewRow();
                                        drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Present";
                                        dt.Rows.Add(drnew);
                                        DataRow drnew1 = dt.NewRow();
                                        drnew1[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Absent";
                                        dt.Rows.Add(drnew1);
                                    }
                                }
                            }
                            // Single Punch Matrix Type || Range Matrix Type Question ||
                            // Single punch type answer horizontal position || 
                            // Multiple punch type answer horizontal position || 
                            // Rank order type question || 
                            // KANO Model Matrix type question || Dropdown Matrix type question
                            else if (item.QuestionTypeId > 7 && item.QuestionTypeId != 15)
                            {
                                if (model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel != null && model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel.Count > 0)
                                {
                                    foreach (var opt in model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel)
                                    {
                                        foreach (var column in item.lstMatrixTypeModel)
                                        {
                                            DataRow drnew = dt.NewRow();
                                            drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "_C" + column.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + " " + ReplaceHtml.ReplaceText(column.ColumnName);
                                            dt.Rows.Add(drnew);
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (item.QuestionTypeId == 2 || item.QuestionTypeId == 4 || item.QuestionTypeId == 5 || item.QuestionTypeId == 6)//Multi Punch || Constant Sum || Ranking || Matrix Type
                            {
                                if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
                                {
                                    foreach (var opt in item.lstOptionsModel)
                                    {
                                        DataRow drnew = dt.NewRow();
                                        //drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                        drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                        dt.Rows.Add(drnew);
                                    }
                                }
                            }

                            else if (item.QuestionTypeId == 1 || item.QuestionTypeId == 3 || item.QuestionTypeId == 7 || item.QuestionTypeId == 15)//Single Punch || Range || Simple Text
                            {
                                DataRow drnew = dt.NewRow();
                                //drnew[0] = "Q" + item.DisplayOrder + "_S" + "-" + ReplaceHtml.ReplaceText(item.QuestionTitle);
                                drnew[0] = "Q" + item.QuestionId + "_S" + "-" + ReplaceHtml.ReplaceText(item.QuestionTitle);
                                dt.Rows.Add(drnew);
                            }
                            else if (item.QuestionTypeId == 8 || item.QuestionTypeId == 12)//Single punch matrix type  && Rank order type
                            {
                                //if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
                                //{
                                //foreach (var opt in item.lstOptionsModel)
                                //{
                                //    DataRow drnew = dt.NewRow();
                                //    //drnew[0] = "Q" + item.QuestionId + "_M" + opt.OptionId + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                //    drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                //    dt.Rows.Add(drnew);
                                //}
                                //}
                                if (item.lstMatrixTypeModel != null && item.lstMatrixTypeModel.Count > 0)
                                {
                                    foreach (var col in item.lstMatrixTypeModel)
                                    {
                                        DataRow drnew = dt.NewRow();
                                        //drnew[0] = "Q" + item.QuestionId + "_M" + opt.OptionId + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                        drnew[0] = "Q" + item.QuestionId + "_M" + col.DisplayOrder + "-" + ReplaceHtml.ReplaceText(col.ColumnName);
                                        dt.Rows.Add(drnew);
                                    }
                                }
                            }
                            else if (item.QuestionTypeId == 13)//Kano model matrix type 
                            {
                                if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
                                {
                                    foreach (var opt in item.lstOptionsModel)
                                    {
                                        DataRow drnew = dt.NewRow();
                                        //drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Present";
                                        drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Present";
                                        dt.Rows.Add(drnew);
                                        DataRow drnew1 = dt.NewRow();
                                        //drnew1[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Absent";
                                        drnew1[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Absent";
                                        dt.Rows.Add(drnew1);
                                    }
                                }
                            }
                            // Single Punch Matrix Type || Range Matrix Type Question ||
                            // Single punch type answer horizontal position || 
                            // Multiple punch type answer horizontal position || 
                            // Rank order type question || 
                            // KANO Model Matrix type question || Dropdown Matrix type question
                            else if (item.QuestionTypeId > 7)
                            {
                                if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
                                {
                                    foreach (var opt in item.lstOptionsModel)
                                    {
                                        foreach (var column in item.lstMatrixTypeModel)
                                        {
                                            DataRow drnew = dt.NewRow();
                                            //drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "_C" + column.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + " " + ReplaceHtml.ReplaceText(column.ColumnName);
                                            drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "_C" + column.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + " " + ReplaceHtml.ReplaceText(column.ColumnName);
                                            dt.Rows.Add(drnew);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                // Get the Excel Pattern of dt
                dtF = comMethods.GenerateTransposedTable(dt);

                // Get all the reponders
                projectBase.Id = model.projectModel.Id;
                actionResult = adminAction.CompleteRespondersLoad_ByProjectId(projectBase);
                List<RespondersModel> lstResponders = new List<RespondersModel>();
                foreach (DataRow dr in actionResult.dtResult.Rows)
                {
                    lstResponders.Add(new RespondersModel
                    {

                        InvitationGuid = dr["InvitationGuid"] != DBNull.Value ? Convert.ToString(dr["InvitationGuid"]) : "",
                        Email = dr["Email"] != DBNull.Value ? Convert.ToString(dr["Email"]) : "",

                    });
                }
                IEnumerable<RespondersModel> filteredList = lstResponders
                 .GroupBy(customer => customer.InvitationGuid).Select(group => group.First());
                // Get Responses
                SurveyLoadBase surveyLoadBase = new SurveyLoadBase();
                string json = string.Empty;
                SRV.BaseLayer.ActionResult actionResultResponses = new SRV.BaseLayer.ActionResult();
                int response_id = 1;            //ss
                foreach (var responder in filteredList)
                {
                    DataTable responsesDt = new DataTable();
                    surveyLoadBase.Id = model.projectModel.Id;
                    surveyLoadBase.Email = (!string.IsNullOrEmpty(responder.Email) ? Convert.ToString(responder.Email) : "");
                    surveyLoadBase.ExternalResponderId = (!string.IsNullOrEmpty(responder.InvitationGuid) ? Convert.ToString(responder.InvitationGuid) : "");
                    if (model.projectModel.lstProductProjectModel != null && model.projectModel.lstProductProjectModel.Count > 0)
                    {
                        foreach (var productModel in model.projectModel.lstProductProjectModel)
                        {
                            surveyLoadBase.ProductId = productModel.Id;
                            actionResultResponses = respondentAction.Response_LoadByProjectId(surveyLoadBase);
                            if (actionResultResponses.IsSuccess)
                            {
                                if (responsesDt.Rows.Count == 0)
                                    responsesDt = actionResultResponses.dtResult.Copy();
                                else
                                {
                                    foreach (DataRow drThisRespRow in actionResultResponses.dtResult.Rows)
                                        responsesDt.ImportRow(drThisRespRow);
                                }
                            }
                        }
                    }
                    else
                    {
                        actionResultResponses = respondentAction.Response_LoadByProjectId(surveyLoadBase);
                        if (actionResultResponses.IsSuccess)
                            responsesDt = actionResultResponses.dtResult.Copy();

                    }

                    // surveyLoadBase.OfflineResponderId = dr["RespId"] != DBNull.Value ? Convert.ToString(dr["RespId"]) : "";
                    //DataRow dr_dtF = dtF.NewRow();
                    // if (String.IsNullOrEmpty(surveyLoadBase.Email)) continue;

                    //actionResultResponses = respondentAction.Response_LoadByProjectId(surveyLoadBase);
                    if (responsesDt.Rows.Count > 0)//actionResultResponses.IsSuccess
                    {
                        //dr_dtF[0] = surveyLoadBase.Email;

                        int count = 1;
                        count = (model.projectModel.ProjectType == "2") ? count + 2 : count;        //ss
                        DataRow dr_dtF = dtF.NewRow();
                        DataRow drTemp = dtF.NewRow();
                        string product_desc = string.Empty;
                        string product_logiId = string.Empty;
                        int oldProductID = responsesDt.Rows[0]["ProductId"] != DBNull.Value ? Convert.ToInt32(responsesDt.Rows[0]["ProductId"]) : 0;
                        foreach (DataRow drResponse in responsesDt.Rows)// actionResultResponses.dtResult.Rows
                        {

                            int optionsCount = 0;
                            // response_id = drResponse["ReponderId"] != DBNull.Value ? (responsesDt.Rows[0]["ReponderId"]).ToString() : "";
                            int productId = drResponse["ProductId"] != DBNull.Value ? Convert.ToInt32(drResponse["ProductId"]) : 0;
                            if (productId != oldProductID)
                            {
                                count = (model.projectModel.ProjectType == "2") ? 3 : 1;
                                dr_dtF = dtF.NewRow();
                            }

                            //--ss

                            product_desc = drResponse["ProductDescription"] != DBNull.Value ? Convert.ToString(drResponse["ProductDescription"]) : "";
                            product_logiId = drResponse["ProductLogicalId"] != DBNull.Value ? Convert.ToString(drResponse["ProductLogicalId"]) : "";

                            //--ss
                            int questionId = drResponse["QuestionId"] != DBNull.Value ? Convert.ToInt32(drResponse["QuestionId"]) : 0;
                            string answerExpression = drResponse["AnswerExpression"] != DBNull.Value ? Convert.ToString(drResponse["AnswerExpression"]) : "";
                            int questionTypeId = drResponse["QuestionTypeId"] != DBNull.Value ? Convert.ToInt32(drResponse["QuestionTypeId"]) : 1;
                            int PipedFrom = drResponse["PipedFrom"] != DBNull.Value ? Convert.ToInt32(drResponse["PipedFrom"]) : 0;
                            //dr_dtF[0] = drResponse;

                            // Get Total options count for this question
                            questionBase.Id = Convert.ToInt32(questionId);
                            SRV.BaseLayer.ActionResult actionResultAnswersTemp = new SRV.BaseLayer.ActionResult();
                            actionResultAnswersTemp = adminAction.Option_LoadAllById(questionBase);
                            if (actionResultAnswersTemp.dtResult != null && actionResultAnswersTemp.dtResult.Rows.Count > 0)
                            {
                                optionsCount = actionResultAnswersTemp.dtResult.Rows.Count;
                            }

                            if (questionTypeId == 2 || questionTypeId == 4 || questionTypeId == 5 || questionTypeId == 6)
                            {
                                string[] allAnswers = answerExpression.Split('|');
                                if (allAnswers.Length > 0)
                                {
                                    if (questionTypeId == 2)// Multi Punch
                                    {
                                        string allRec = string.Empty;
                                        List<OptionsModel> matrixAns = new List<OptionsModel>();
                                        if (PipedFrom > 0 && !drResponse["QuestionTitle"].ToString().Contains("{{Q}}"))
                                        {
                                            matrixAns = model.lstQuestionModel.Where(v => v.QuestionId == PipedFrom).FirstOrDefault().lstOptionsModel;
                                        }
                                        else
                                        {
                                            matrixAns = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                        }

                                        foreach (var item in matrixAns)
                                        {
                                            string matchedId = string.Empty;
                                            foreach (string ans in allAnswers)
                                            {
                                                if (Convert.ToString(item.OptionId) == ans)
                                                {
                                                    matchedId = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            allRec += matchedId + "|";
                                        }

                                        string[] allAnswersMat = allRec.Substring(0, allRec.Length - 1).Split('|');
                                        foreach (string ans in allAnswersMat)
                                        {

                                            if (count <= dtF.Columns.Count - 1)
                                            {
                                                if ((Regex.Replace(dtF.Columns[count].ToString().Split('-')[0].Split('_')[1], @"[^\d]", String.Empty)) == ans)
                                                {
                                                    dr_dtF[count] = "1";// ans;
                                                    count++;
                                                    optionsCount--;
                                                }
                                                else
                                                {
                                                    dr_dtF[count] = "2";
                                                    count++;
                                                    optionsCount--;
                                                }
                                            }
                                        }
                                        //count = count + optionsCount;
                                    }
                                    else if (questionTypeId == 6) // matrix Type
                                    {
                                        foreach (string ans in allAnswers)
                                        {
                                            dr_dtF[count] = (ans.ToLower().IndexOf("yes") != -1) ? "1" : "2";
                                            count++;
                                            optionsCount--;
                                        }
                                        count = count + optionsCount;
                                    }
                                    else
                                    {
                                        foreach (string ans in allAnswers)
                                        {
                                            dr_dtF[count] = ans;
                                            count++;
                                            optionsCount--;
                                        }
                                        count = count + optionsCount;
                                    }
                                }
                            }
                            else if (questionTypeId == 1) // Single Punch Answers
                            {
                                //List<OptionsModel> SinglePunchAns = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                List<OptionsModel> SinglePunchAns = new List<OptionsModel>();
                                if (PipedFrom > 0 && !drResponse["QuestionTitle"].ToString().Contains("{{Q}}"))
                                {
                                    SinglePunchAns = model.lstQuestionModel.Where(v => v.QuestionId == PipedFrom).FirstOrDefault().lstOptionsModel;
                                }
                                else
                                {
                                    SinglePunchAns = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                }
                                int ansIndex = 0;
                                foreach (var item in SinglePunchAns)
                                {
                                    ansIndex++;
                                    if (Convert.ToString(item.OptionId) == answerExpression)
                                    {
                                        break;
                                    }
                                }

                                dr_dtF[count] = ansIndex;//answerExpression;
                                count++;
                                optionsCount--;
                            }
                            else if (questionTypeId == 8 || questionTypeId == 12) // single punch matrix answers
                            {
                                List<OptionsModel> optionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                List<MatrixTypeModel> columnDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstMatrixTypeModel;
                                int qustionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).Select(v => v.DisplayOrder).FirstOrDefault();
                                string[] allAnswers = answerExpression.Split('|');

                                List<string> lstColumnOrdinals = new List<string>();

                                string formattedAnswer = string.Empty;
                                string answerValue = string.Empty;
                                if (allAnswers.Length > 0)
                                {
                                    foreach (var ansExpression in allAnswers)
                                    {
                                        string displaycol = string.Empty;
                                        string displayopt = string.Empty;
                                        foreach (var item in optionDisplayOrder)
                                        {

                                            if (Convert.ToString(item.OptionId) == ansExpression.Split('~')[0])
                                            {
                                                displaycol = Convert.ToString(item.DisplayOrder);
                                                break;
                                            }
                                        }
                                        foreach (var item in columnDisplayOrder)
                                        {
                                            if (Convert.ToString(item.Id) == ansExpression.Split('~')[1])
                                            {
                                                displayopt = Convert.ToString(item.DisplayOrder);
                                                break;
                                            }
                                        }
                                        //  formattedAnswer = "Q" + questionId + "_M" + displaycol + "#" + displayopt;
                                        formattedAnswer = "Q" + questionId + "_M" + displayopt + "#" + displaycol;
                                        foreach (DataColumn column in dtF.Columns)
                                        {
                                            string[] columnHeader = column.ColumnName.Split('-');
                                            string[] answer = columnHeader[0].Split('_');
                                            if (columnHeader.Length > 1)
                                            {
                                                string answerInHeader = answer[0] + "_" + answer[1];
                                                string[] formattedAnswerArr = formattedAnswer.Split('#');
                                                if (formattedAnswerArr[0] == answerInHeader)
                                                {
                                                    int index = dr_dtF.Table.Columns[column.ColumnName].Ordinal;
                                                    lstColumnOrdinals.Add(index.ToString());
                                                    count++;
                                                    dr_dtF[index] = formattedAnswerArr[1];
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            else if (questionTypeId == 13) // Kano model matrix type
                            {
                                List<OptionsModel> optionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                List<MatrixTypeModel> columnDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstMatrixTypeModel;
                                int qustionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).Select(v => v.DisplayOrder).FirstOrDefault();
                                string[] allAnswers = answerExpression.Split('|');

                                List<string> lstColumnOrdinals = new List<string>();

                                string formattedAnswer = string.Empty;
                                string answerValue = string.Empty;
                                if (allAnswers.Length > 1)
                                {
                                    int repeatId = 0;
                                    foreach (var ansExpression in allAnswers)
                                    {
                                        string displaycol = string.Empty;
                                        string displayopt = string.Empty;
                                        string displayText = string.Empty;
                                        string text = string.Empty;
                                        if (repeatId == Convert.ToInt32(ansExpression.Split('~')[0]))
                                        {
                                            text = "_Absent";
                                            repeatId = 0;
                                        }
                                        else
                                        {
                                            text = "_Present";
                                        }
                                        foreach (var item in optionDisplayOrder)
                                        {
                                            if (Convert.ToString(item.OptionId) == ansExpression.Split('~')[0])
                                            {
                                                displayText = Convert.ToString(item.Option) + text;
                                                displaycol = Convert.ToString(item.DisplayOrder);
                                                repeatId = Convert.ToInt32(ansExpression.Split('~')[0]);
                                                break;
                                            }
                                        }
                                        foreach (var item in columnDisplayOrder)
                                        {
                                            if (Convert.ToString(item.Id) == ansExpression.Split('~')[1])
                                            {
                                                displayopt = Convert.ToString(item.DisplayOrder);
                                                break;
                                            }
                                        }
                                        //formattedAnswer = "Q" + qustionDisplayOrder + "_M" + displaycol + "-" + displayText + "#" + displayopt;
                                        formattedAnswer = "Q" + questionId + "_M" + displaycol + "-" + displayText + "#" + displayopt;
                                        foreach (DataColumn column in dtF.Columns)
                                        {

                                            string[] columnHeader = column.ColumnName.Split('-');
                                            string[] answer = columnHeader[0].Split('_');
                                            if (columnHeader.Length > 1)
                                            {
                                                string answerInHeader = answer[0] + "_" + answer[1] + "-" + columnHeader[1];
                                                string[] formattedAnswerArr = formattedAnswer.Split('#');
                                                if (Regex.Replace(formattedAnswerArr[0], @"\s+", "", RegexOptions.Multiline) == Regex.Replace(answerInHeader, @"\s+", "", RegexOptions.Multiline))
                                                {
                                                    int index = dr_dtF.Table.Columns[column.ColumnName].Ordinal;
                                                    lstColumnOrdinals.Add(index.ToString());
                                                    count++;
                                                    dr_dtF[index] = formattedAnswerArr[1];
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            else if (questionTypeId > 7 && questionTypeId != 15)
                            {
                                List<OptionsModel> optionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                List<MatrixTypeModel> columnDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstMatrixTypeModel;
                                List<DDLMatrixTypeModel> dropDownDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstDDlMatrixModel;
                                int optDisplayOrder = 0;
                                if(optionDisplayOrder!=null)
                                {
                                    optDisplayOrder = optionDisplayOrder.Count;
                                }
                                int colDisplayOrder = 0;
                                if (columnDisplayOrder != null)
                                {
                                    colDisplayOrder = columnDisplayOrder.Count;
                                }
                                count = count + (optDisplayOrder * colDisplayOrder);
                                int qustionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).Select(v => v.DisplayOrder).FirstOrDefault();
                                string[] allAnswers = answerExpression.Split('|');

                                List<string> lstColumnOrdinals = new List<string>();

                                string formattedAnswer = string.Empty;
                                string answerValue = string.Empty;
                                int DDlDisplayOrder = 0;
                                if (allAnswers.Length > 1)
                                {
                                    foreach (var ansExpression in allAnswers)
                                    {
                                        string displaycol = string.Empty;
                                        string displayopt = string.Empty;

                                        if (questionTypeId == 9 || questionTypeId == 14)
                                        {
                                            foreach (var item in optionDisplayOrder)
                                            {
                                                if (Convert.ToString(item.OptionId) == ansExpression.Split('~')[0].Split('*')[0])
                                                {
                                                    displaycol = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            foreach (var item in columnDisplayOrder)
                                            {
                                                if (Convert.ToString(item.Id) == ansExpression.Split('~')[1])
                                                {
                                                    displayopt = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            //formattedAnswer = "Q" + qustionDisplayOrder + "_M" + displaycol + "_C" + displayopt;
                                            formattedAnswer = "Q" + questionId + "_M" + displaycol + "_C" + displayopt;
                                            answerValue = ansExpression.Split('~')[0].Split('*')[1];
                                            if (questionTypeId == 14)
                                                DDlDisplayOrder = dropDownDisplayOrder.Where(l => l.Id == Convert.ToInt32(answerValue)).FirstOrDefault().DDLDisplayOrder;
                                        }
                                        else
                                        {
                                            foreach (var item in optionDisplayOrder)
                                            {
                                                if (Convert.ToString(item.OptionId) == ansExpression.Split('~')[0])
                                                {
                                                    displaycol = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            foreach (var item in columnDisplayOrder)
                                            {
                                                if (Convert.ToString(item.Id) == ansExpression.Split('~')[1])
                                                {
                                                    displayopt = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            //formattedAnswer = "Q" + qustionDisplayOrder + "_M" + displaycol + "_C" + displayopt;
                                            formattedAnswer = "Q" + questionId + "_M" + displaycol + "_C" + displayopt;
                                        }
                                        foreach (DataColumn column in dtF.Columns)
                                        {
                                            string[] columnHeader = column.ColumnName.Split('-');

                                            if (columnHeader.Length > 1)
                                            {
                                                string[] answer = columnHeader[0].Split('_');

                                                if (answer.Length > 2)
                                                {
                                                    string answerInHeader = answer[0] + "_" + answer[1] + "_" + answer[2];

                                                    if (formattedAnswer == answerInHeader)
                                                    {
                                                        int index = dr_dtF.Table.Columns[column.ColumnName].Ordinal;
                                                        lstColumnOrdinals.Add(index.ToString());
                                                        //count++;
                                                        if (questionTypeId == 9)
                                                        {
                                                            dr_dtF[index] = answerValue;
                                                        }
                                                        else if (questionTypeId == 14)
                                                        {
                                                            dr_dtF[index] = DDlDisplayOrder;
                                                        }
                                                        else
                                                        {
                                                            dr_dtF[index] = "1";
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }

                                // Fill All the Empty cells with 2

                                for (int i = 0; i <= dtF.Columns.Count - 1; i++)
                                    if (String.IsNullOrEmpty(Convert.ToString(dr_dtF[i])))
                                        dr_dtF[i] = "";
                            }

                            else
                            {
                                dr_dtF[count] = answerExpression;
                                count++;
                                optionsCount--;
                            }

                            if (productId != oldProductID)
                            {
                                dtF.Rows.Add(drTemp);
                                //  drTemp[0] = dtF.Rows.IndexOf(drTemp) + 1;
                                drTemp[0] = response_id;
                                oldProductID = productId;
                            }

                            drTemp = dr_dtF;
                            //---ss
                            if (model.projectModel.ProjectType == "2")
                            {
                                drTemp[2] = product_desc;
                                drTemp[1] = product_logiId;
                            }
                            //---ss

                        }

                        dtF.Rows.Add(dr_dtF);
                        //dr_dtF[0] = dtF.Rows.IndexOf(dr_dtF) + 1;
                        drTemp[0] = response_id;
                        //---ss
                        if (model.projectModel.ProjectType == "2")
                        {
                            dr_dtF[2] = product_desc;
                            dr_dtF[1] = product_logiId;
                        }
                        response_id++;
                        //---ss
                    }
                }

                if (dtF != null && dtF.Rows.Count > 0)
                {
                    //DataTable dtF = comMethods.GenerateTransposedTable(dt);
                    newData = comMethods.GenerateTransposedTable(dt);
                    foreach (DataRow dr in dtF.Rows)
                    {
                        if (model.projectModel.ProjectType == "2")
                        {
                            DataRow rw = newData.AsEnumerable().FirstOrDefault(tt => tt.Field<string>("Resp_Id") == dr["Resp_Id"].ToString() && tt.Field<string>("Product_Name") == dr["Product_Name"].ToString());
                            if (rw == null)
                            {
                                DataRow dr_dtF = newData.NewRow();

                                newData.Rows.Add(dr_dtF);
                                foreach (DataColumn dcTempKano in dtF.Columns)
                                {

                                    dr_dtF[dcTempKano.ToString()] = dr[dcTempKano.ToString()];

                                }
                                // row exists
                            }
                        }
                        else
                        {
                            DataRow rw = newData.AsEnumerable().FirstOrDefault(tt => tt.Field<string>("Resp_Id") == dr["Resp_Id"].ToString());
                            if (rw == null)
                            {
                                DataRow dr_dtF = newData.NewRow();

                                newData.Rows.Add(dr_dtF);
                                foreach (DataColumn dcTempKano in dtF.Columns)
                                {

                                    dr_dtF[dcTempKano.ToString()] = dr[dcTempKano.ToString()];

                                }
                                // row exists
                            }
                        }


                    }
                }
            }
            catch (Exception ex)
            {

            }
            if (newData.Columns.Count > 0)
            {
                string ReportDataHtml = "<table id='hdnReportHtml'>";
                DataTable dtReport = newData;
                if (dtReport != null)
                {
                    string[] cols = (from dc in dtReport.Columns.Cast<DataColumn>()
                                     select dc.ColumnName)
                                   .ToArray();
                    if (cols != null)
                    {
                        ReportDataHtml += "<tr class='thRow'>";
                        foreach (var item in cols)
                        {
                            ReportDataHtml += "<th>" + item + "</th>";
                        }
                        ReportDataHtml += "</tr>";
                    }

                    foreach (DataRow row in dtReport.Rows)
                    {
                        ReportDataHtml += "<tr class='tdRow'>";
                        foreach (var item in row.ItemArray)
                        {
                            ReportDataHtml += "<td>" + item + "</td>";
                        }
                        ReportDataHtml += "</tr>";
                    }
                }
                ReportDataHtml += "</table>";
                string onlineResponseXML = ConvertDatatableToXML(newData);
                actionResult = adminAction.OnlineProjectRespose_Add(proId, Id, onlineResponseXML, ReportDataHtml);
            }
        }
        #endregion

        #region GetReportDataTableOffline
        [WebMethod]
        public void GetReportDataTableOffline(string Id, int ResponseCount, int proId)
        {
            RespondentController respondent = new RespondentController();
            SurveyResponseModel model = new SurveyResponseModel();
            DataTable dtF = new DataTable();
            try
            {
                DataTable dt = new DataTable();

                string projectGuid = string.Empty;
                projectGuid = Id;
                dt.Columns.Add("Resp_Id", typeof(string));

                model = respondent.GetProjectPreview(projectGuid);

                //--ss
                if (model.projectModel.ProjectType == "2")
                {
                    DataRow drnew = dt.NewRow();
                    drnew[0] = "Product_LogicalId";
                    dt.Rows.Add(drnew);
                    DataRow drnew1 = dt.NewRow();
                    drnew1[0] = "Product_Name";
                    dt.Rows.Add(drnew1);
                }
                //--

                // Load all the questions of this project
                List<QuestionList> questionsList = new List<QuestionList>();
                questionBase.ProjectId = Convert.ToInt32(model.projectModel.Id);
                actionResult = adminAction.QuestionsLoadAll_ByProjectId(questionBase);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    foreach (DataRow dr in actionResult.dtResult.Rows)
                    {
                        questionsList.Add(new QuestionList
                        {
                            id = dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0,
                            questionTypeId = dr["QuestionTypeId"] != DBNull.Value ? Convert.ToInt32(dr["QuestionTypeId"]) : 0
                        });
                    }
                }

                string fileName = Guid.NewGuid().ToString().Substring(0, 5) + "_" + DateTime.Now.ToString("MM-dd-yyyy") + "_" + model.projectModel.ProjectName.Replace(" ", "");

                if (model.lstQuestionModel != null && model.lstQuestionModel.Count > 0)
                {
                    foreach (var item in model.lstQuestionModel)
                    {
                        if (item.PipedFrom > 0 && (!item.QuestionTitle.Contains("{{Q}}")))
                        {
                            if (item.QuestionTypeId == 2 || item.QuestionTypeId == 4 || item.QuestionTypeId == 5 || item.QuestionTypeId == 6)//Multi Punch || Constant Sum || Ranking || Matrix Type
                            {
                                if (model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel != null && model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel.Count > 0)
                                {
                                    foreach (var opt in model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel)
                                    {
                                        DataRow drnew = dt.NewRow();
                                        drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                        dt.Rows.Add(drnew);
                                    }
                                }
                            }

                            else if (item.QuestionTypeId == 1 || item.QuestionTypeId == 3 || item.QuestionTypeId == 7 || item.QuestionTypeId == 15)//Single Punch || Range || Simple Text
                            {
                                DataRow drnew = dt.NewRow();
                                drnew[0] = "Q" + item.QuestionId + "_S" + "-" + ReplaceHtml.ReplaceText(item.QuestionTitle);
                                dt.Rows.Add(drnew);
                            }
                            else if (item.QuestionTypeId == 8 || item.QuestionTypeId == 12)//Single punch matrix type  && Rank order type
                            {
                                //if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
                                //{
                                //foreach (var opt in item.lstOptionsModel)
                                //{
                                //    DataRow drnew = dt.NewRow();
                                //    //drnew[0] = "Q" + item.QuestionId + "_M" + opt.OptionId + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                //    drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                //    dt.Rows.Add(drnew);
                                //}
                                //}
                                if (item.lstMatrixTypeModel != null && item.lstMatrixTypeModel.Count > 0)
                                {
                                    foreach (var col in item.lstMatrixTypeModel)
                                    {
                                        DataRow drnew = dt.NewRow();
                                        //drnew[0] = "Q" + item.QuestionId + "_M" + opt.OptionId + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                        drnew[0] = "Q" + item.QuestionId + "_M" + col.DisplayOrder + "-" + ReplaceHtml.ReplaceText(col.ColumnName);
                                        dt.Rows.Add(drnew);
                                    }
                                }
                            }
                            else if (item.QuestionTypeId == 13)//Kano model matrix type 
                            {
                                if (model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel != null && model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel.Count > 0)
                                {
                                    foreach (var opt in model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel)
                                    {
                                        DataRow drnew = dt.NewRow();
                                        drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Present";
                                        dt.Rows.Add(drnew);
                                        DataRow drnew1 = dt.NewRow();
                                        drnew1[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Absent";
                                        dt.Rows.Add(drnew1);
                                    }
                                }
                            }
                            // Single Punch Matrix Type || Range Matrix Type Question ||
                            // Single punch type answer horizontal position || 
                            // Multiple punch type answer horizontal position || 
                            // Rank order type question || 
                            // KANO Model Matrix type question || Dropdown Matrix type question
                            else if (item.QuestionTypeId > 7 && item.QuestionTypeId != 15)
                            {
                                if (model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel != null && model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel.Count > 0)
                                {
                                    foreach (var opt in model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel)
                                    {
                                        foreach (var column in item.lstMatrixTypeModel)
                                        {
                                            DataRow drnew = dt.NewRow();
                                            drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "_C" + column.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + " " + ReplaceHtml.ReplaceText(column.ColumnName);
                                            dt.Rows.Add(drnew);
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (item.QuestionTypeId == 2 || item.QuestionTypeId == 4 || item.QuestionTypeId == 5 || item.QuestionTypeId == 6)//Multi Punch || Constant Sum || Ranking || Matrix Type
                            {
                                if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
                                {
                                    foreach (var opt in item.lstOptionsModel)
                                    {
                                        DataRow drnew = dt.NewRow();
                                        //drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                        drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                        dt.Rows.Add(drnew);
                                    }
                                }
                            }

                            else if (item.QuestionTypeId == 1 || item.QuestionTypeId == 3 || item.QuestionTypeId == 7 || item.QuestionTypeId == 15)//Single Punch || Range || Simple Text
                            {
                                DataRow drnew = dt.NewRow();
                                //drnew[0] = "Q" + item.DisplayOrder + "_S" + "-" + ReplaceHtml.ReplaceText(item.QuestionTitle);
                                drnew[0] = "Q" + item.QuestionId + "_S" + "-" + ReplaceHtml.ReplaceText(item.QuestionTitle);
                                dt.Rows.Add(drnew);
                            }
                            else if (item.QuestionTypeId == 8 || item.QuestionTypeId == 12)//Single punch matrix type  && Rank order type
                            {
                                //if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
                                //{
                                //foreach (var opt in item.lstOptionsModel)
                                //{
                                //    DataRow drnew = dt.NewRow();
                                //    //drnew[0] = "Q" + item.QuestionId + "_M" + opt.OptionId + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                //    drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                //    dt.Rows.Add(drnew);
                                //}
                                //}
                                if (item.lstMatrixTypeModel != null && item.lstMatrixTypeModel.Count > 0)
                                {
                                    foreach (var col in item.lstMatrixTypeModel)
                                    {
                                        DataRow drnew = dt.NewRow();
                                        //drnew[0] = "Q" + item.QuestionId + "_M" + opt.OptionId + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                        drnew[0] = "Q" + item.QuestionId + "_M" + col.DisplayOrder + "-" + ReplaceHtml.ReplaceText(col.ColumnName);
                                        dt.Rows.Add(drnew);
                                    }
                                }
                            }
                            else if (item.QuestionTypeId == 13)//Kano model matrix type 
                            {
                                if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
                                {
                                    foreach (var opt in item.lstOptionsModel)
                                    {
                                        DataRow drnew = dt.NewRow();
                                        //drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Present";
                                        drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Present";
                                        dt.Rows.Add(drnew);
                                        DataRow drnew1 = dt.NewRow();
                                        //drnew1[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Absent";
                                        drnew1[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Absent";
                                        dt.Rows.Add(drnew1);
                                    }
                                }
                            }
                            // Single Punch Matrix Type || Range Matrix Type Question ||
                            // Single punch type answer horizontal position || 
                            // Multiple punch type answer horizontal position || 
                            // Rank order type question || 
                            // KANO Model Matrix type question || Dropdown Matrix type question
                            else if (item.QuestionTypeId > 7)
                            {
                                if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
                                {
                                    foreach (var opt in item.lstOptionsModel)
                                    {
                                        foreach (var column in item.lstMatrixTypeModel)
                                        {
                                            DataRow drnew = dt.NewRow();
                                            //drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "_C" + column.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + " " + ReplaceHtml.ReplaceText(column.ColumnName);
                                            drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "_C" + column.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + " " + ReplaceHtml.ReplaceText(column.ColumnName);
                                            dt.Rows.Add(drnew);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                // Get the Excel Pattern of dt
                dtF = comMethods.GenerateTransposedTable(dt);

                // Get all the reponders
                projectBase.Id = model.projectModel.Id;
                actionResult = adminAction.Responders_Offline_LoadBy_ProjectId(projectBase);

                // Get Responses
                SurveyLoadBase surveyLoadBase = new SurveyLoadBase();
                string json = string.Empty;
                SRV.BaseLayer.ActionResult actionResultResponses = new SRV.BaseLayer.ActionResult();
                int response_id = ResponseCount;            //ss
                foreach (DataRow dr in actionResult.dtResult.Rows)
                {
                    surveyLoadBase.Id = model.projectModel.Id;
                    surveyLoadBase.OfflineResponderId = dr["RespId"] != DBNull.Value ? Convert.ToString(dr["RespId"]) : "";
                    //DataRow dr_dtF = dtF.NewRow();

                    if (String.IsNullOrEmpty(surveyLoadBase.OfflineResponderId)) continue;


                    DataTable responsesDt = new DataTable();


                    if (model.projectModel.lstProductProjectModel != null && model.projectModel.lstProductProjectModel.Count > 0)
                    {
                        foreach (var productModel in model.projectModel.lstProductProjectModel)
                        {
                            surveyLoadBase.ProductId = productModel.Id;
                            actionResultResponses = respondentAction.Response_Offline_LoadBy_ProjectId_RespId(surveyLoadBase);
                            if (actionResultResponses.IsSuccess)
                            {
                                if (responsesDt.Rows.Count == 0)
                                    responsesDt = actionResultResponses.dtResult.Copy();
                                else
                                {
                                    foreach (DataRow drThisRespRow in actionResultResponses.dtResult.Rows)
                                        responsesDt.ImportRow(drThisRespRow);

                                }
                            }
                        }
                    }
                    else
                    {
                        actionResultResponses = respondentAction.Response_Offline_LoadBy_ProjectId_RespId(surveyLoadBase);
                        if (actionResultResponses.IsSuccess)
                            responsesDt = actionResultResponses.dtResult.Copy();

                    }


                    //actionResultResponses = respondentAction.Response_Offline_LoadBy_ProjectId_RespId(surveyLoadBase);
                    if (responsesDt.Rows.Count > 0)//actionResultResponses.IsSuccess
                    {
                        //dr_dtF[0] = surveyLoadBase.Email;

                        int count = 1;
                        count = (model.projectModel.ProjectType == "2") ? count + 2 : count;        //ss
                        DataRow dr_dtF = dtF.NewRow();
                        DataRow drTemp = dtF.NewRow();
                        string product_desc = string.Empty;
                        string product_logiId = string.Empty;
                        int oldProductID = responsesDt.Rows[0]["ProductId"] != DBNull.Value ? Convert.ToInt32(responsesDt.Rows[0]["ProductId"]) : 0;
                        foreach (DataRow drResponse in responsesDt.Rows)// actionResultResponses.dtResult.Rows
                        {

                            int optionsCount = 0;

                            int productId = drResponse["ProductId"] != DBNull.Value ? Convert.ToInt32(drResponse["ProductId"]) : 0;
                            if (productId != oldProductID)
                            {
                                count = (model.projectModel.ProjectType == "2") ? 3 : 1;
                                dr_dtF = dtF.NewRow();
                            }

                            //--ss

                            product_desc = drResponse["ProductDescription"] != DBNull.Value ? Convert.ToString(drResponse["ProductDescription"]) : "";
                            product_logiId = drResponse["ProductLogicalId"] != DBNull.Value ? Convert.ToString(drResponse["ProductLogicalId"]) : "";

                            //--ss
                            int questionId = drResponse["QuestionId"] != DBNull.Value ? Convert.ToInt32(drResponse["QuestionId"]) : 0;
                            string answerExpression = drResponse["AnswerExpression"] != DBNull.Value ? Convert.ToString(drResponse["AnswerExpression"]) : "";
                            int questionTypeId = drResponse["QuestionTypeId"] != DBNull.Value ? Convert.ToInt32(drResponse["QuestionTypeId"]) : 1;
                            int PipedFrom = drResponse["PipedFrom"] != DBNull.Value ? Convert.ToInt32(drResponse["PipedFrom"]) : 0;
                            //dr_dtF[0] = drResponse;

                            // Get Total options count for this question
                            questionBase.Id = Convert.ToInt32(questionId);
                            SRV.BaseLayer.ActionResult actionResultAnswersTemp = new SRV.BaseLayer.ActionResult();
                            actionResultAnswersTemp = adminAction.Option_LoadAllById(questionBase);
                            if (actionResultAnswersTemp.dtResult != null && actionResultAnswersTemp.dtResult.Rows.Count > 0)
                            {
                                optionsCount = actionResultAnswersTemp.dtResult.Rows.Count;
                            }

                            if (questionTypeId == 2 || questionTypeId == 4 || questionTypeId == 5 || questionTypeId == 6)
                            {
                                string[] allAnswers = answerExpression.Split('|');
                                if (allAnswers.Length > 0)
                                {
                                    if (questionTypeId == 2)// Multi Punch
                                    {
                                        string allRec = string.Empty;
                                        //List<OptionsModel> matrixAns = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                        List<OptionsModel> matrixAns = new List<OptionsModel>();
                                        if (PipedFrom > 0 && !drResponse["QuestionTitle"].ToString().Contains("{{Q}}"))
                                        {
                                            matrixAns = model.lstQuestionModel.Where(v => v.QuestionId == PipedFrom).FirstOrDefault().lstOptionsModel;
                                        }
                                        else
                                        {
                                            matrixAns = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                        }
                                        foreach (var item in matrixAns)
                                        {
                                            string matchedId = string.Empty;
                                            foreach (string ans in allAnswers)
                                            {
                                                if (Convert.ToString(item.OptionId) == ans)
                                                {
                                                    matchedId = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            allRec += matchedId + "|";
                                        }

                                        string[] allAnswersMat = allRec.Substring(0, allRec.Length - 1).Split('|');
                                        foreach (string ans in allAnswersMat)
                                        {

                                            if (count <= dtF.Columns.Count - 1)
                                            {
                                                if ((Regex.Replace(dtF.Columns[count].ToString().Split('-')[0].Split('_')[1], @"[^\d]", String.Empty)) == ans)
                                                {
                                                    dr_dtF[count] = "1";// ans;
                                                    count++;
                                                    optionsCount--;
                                                }
                                                else
                                                {
                                                    dr_dtF[count] = "2";
                                                    count++;
                                                    optionsCount--;
                                                }
                                            }
                                        }
                                        //count = count + optionsCount;
                                    }
                                    else if (questionTypeId == 6) // matrix Type
                                    {
                                        foreach (string ans in allAnswers)
                                        {
                                            dr_dtF[count] = (ans.ToLower().IndexOf("yes") != -1) ? "1" : "2";
                                            count++;
                                            optionsCount--;
                                        }
                                        count = count + optionsCount;
                                    }
                                    else
                                    {
                                        foreach (string ans in allAnswers)
                                        {
                                            dr_dtF[count] = ans;
                                            count++;
                                            optionsCount--;
                                        }
                                        count = count + optionsCount;
                                    }
                                }
                            }
                            else if (questionTypeId == 1) // Single Punch Answers
                            {
                                //List<OptionsModel> SinglePunchAns = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                List<OptionsModel> SinglePunchAns = new List<OptionsModel>();
                                if (PipedFrom > 0 && !drResponse["QuestionTitle"].ToString().Contains("{{Q}}"))
                                {
                                    SinglePunchAns = model.lstQuestionModel.Where(v => v.QuestionId == PipedFrom).FirstOrDefault().lstOptionsModel;
                                }
                                else
                                {
                                    SinglePunchAns = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                }
                                int ansIndex = 0;
                                foreach (var item in SinglePunchAns)
                                {
                                    ansIndex++;
                                    if (Convert.ToString(item.OptionId) == answerExpression)
                                    {
                                        break;
                                    }
                                }

                                dr_dtF[count] = ansIndex;//answerExpression;
                                count++;
                                optionsCount--;
                            }
                            else if (questionTypeId == 8 || questionTypeId == 12) // single punch matrix answers
                            {
                                List<OptionsModel> optionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                List<MatrixTypeModel> columnDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstMatrixTypeModel;
                                int qustionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).Select(v => v.DisplayOrder).FirstOrDefault();
                                string[] allAnswers = answerExpression.Split('|');

                                List<string> lstColumnOrdinals = new List<string>();

                                string formattedAnswer = string.Empty;
                                string answerValue = string.Empty;
                                if (allAnswers.Length > 0)
                                {
                                    foreach (var ansExpression in allAnswers)
                                    {
                                        if (!string.IsNullOrEmpty(ansExpression))
                                        {
                                            string displaycol = string.Empty;
                                            string displayopt = string.Empty;
                                            foreach (var item in optionDisplayOrder)
                                            {

                                                if (Convert.ToString(item.OptionId) == ansExpression.Split('~')[0])
                                                {
                                                    displaycol = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            foreach (var item in columnDisplayOrder)
                                            {
                                                if (Convert.ToString(item.Id) == ansExpression.Split('~')[1])
                                                {
                                                    displayopt = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            //  formattedAnswer = "Q" + questionId + "_M" + displaycol + "#" + displayopt;
                                            formattedAnswer = "Q" + questionId + "_M" + displayopt + "#" + displaycol;
                                            foreach (DataColumn column in dtF.Columns)
                                            {
                                                string[] columnHeader = column.ColumnName.Split('-');
                                                string[] answer = columnHeader[0].Split('_');
                                                if (columnHeader.Length > 1)
                                                {
                                                    string answerInHeader = answer[0] + "_" + answer[1];
                                                    string[] formattedAnswerArr = formattedAnswer.Split('#');
                                                    if (formattedAnswerArr[0] == answerInHeader)
                                                    {
                                                        int index = dr_dtF.Table.Columns[column.ColumnName].Ordinal;
                                                        lstColumnOrdinals.Add(index.ToString());
                                                        count++;
                                                        dr_dtF[index] = formattedAnswerArr[1];
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            else if (questionTypeId == 13) // Kano model matrix type
                            {
                                List<OptionsModel> optionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                List<MatrixTypeModel> columnDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstMatrixTypeModel;
                                int qustionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).Select(v => v.DisplayOrder).FirstOrDefault();
                                string[] allAnswers = answerExpression.Split('|');

                                List<string> lstColumnOrdinals = new List<string>();

                                string formattedAnswer = string.Empty;
                                string answerValue = string.Empty;
                                if (allAnswers.Length > 1)
                                {
                                    int repeatId = 0;
                                    foreach (var ansExpression in allAnswers)
                                    {
                                        string displaycol = string.Empty;
                                        string displayopt = string.Empty;
                                        string displayText = string.Empty;
                                        string text = string.Empty;
                                        if (repeatId == Convert.ToInt32(ansExpression.Split('~')[0]))
                                        {
                                            text = "_Absent";
                                            repeatId = 0;
                                        }
                                        else
                                        {
                                            text = "_Present";
                                        }
                                        foreach (var item in optionDisplayOrder)
                                        {
                                            if (Convert.ToString(item.OptionId) == ansExpression.Split('~')[0])
                                            {
                                                displayText = Convert.ToString(item.Option) + text;
                                                displaycol = Convert.ToString(item.DisplayOrder);
                                                repeatId = Convert.ToInt32(ansExpression.Split('~')[0]);
                                                break;
                                            }
                                        }
                                        foreach (var item in columnDisplayOrder)
                                        {
                                            if (Convert.ToString(item.Id) == ansExpression.Split('~')[1])
                                            {
                                                displayopt = Convert.ToString(item.DisplayOrder);
                                                break;
                                            }
                                        }
                                        //formattedAnswer = "Q" + qustionDisplayOrder + "_M" + displaycol + "-" + displayText + "#" + displayopt;
                                        formattedAnswer = "Q" + questionId + "_M" + displaycol + "-" + displayText + "#" + displayopt;
                                        foreach (DataColumn column in dtF.Columns)
                                        {

                                            string[] columnHeader = column.ColumnName.Split('-');
                                            string[] answer = columnHeader[0].Split('_');
                                            if (columnHeader.Length > 1)
                                            {
                                                string answerInHeader = answer[0] + "_" + answer[1] + "-" + columnHeader[1];
                                                string[] formattedAnswerArr = formattedAnswer.Split('#');
                                                if (Regex.Replace(formattedAnswerArr[0], @"\s+", "", RegexOptions.Multiline) == Regex.Replace(answerInHeader, @"\s+", "", RegexOptions.Multiline))
                                                {
                                                    int index = dr_dtF.Table.Columns[column.ColumnName].Ordinal;
                                                    lstColumnOrdinals.Add(index.ToString());
                                                    count++;
                                                    dr_dtF[index] = formattedAnswerArr[1];
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            else if (questionTypeId > 7 && questionTypeId != 15)
                            {
                                List<OptionsModel> optionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                List<MatrixTypeModel> columnDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstMatrixTypeModel;
                                List<DDLMatrixTypeModel> dropDownDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstDDlMatrixModel;

                                int optDisplayCount = 0;
                                int colDisplayOrder = 0;
                                if (optionDisplayOrder!=null)
                                {
                                    optDisplayCount = optionDisplayOrder.Count;
                                }
                                if (columnDisplayOrder != null)
                                {
                                    colDisplayOrder = columnDisplayOrder.Count;
                                }
                                count = count + (optDisplayCount * colDisplayOrder);
                                int qustionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).Select(v => v.DisplayOrder).FirstOrDefault();
                                string[] allAnswers = answerExpression.Split('|');

                                List<string> lstColumnOrdinals = new List<string>();

                                string formattedAnswer = string.Empty;
                                string answerValue = string.Empty;
                                int DDlDisplayOrder = 0;
                                if (allAnswers.Length > 1)
                                {
                                    foreach (var ansExpression in allAnswers)
                                    {
                                        string displaycol = string.Empty;
                                        string displayopt = string.Empty;

                                        if (questionTypeId == 9 || questionTypeId == 14)
                                        {
                                            foreach (var item in optionDisplayOrder)
                                            {
                                                if (Convert.ToString(item.OptionId) == ansExpression.Split('~')[0].Split('*')[0])
                                                {
                                                    displaycol = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            foreach (var item in columnDisplayOrder)
                                            {
                                                if (Convert.ToString(item.Id) == ansExpression.Split('~')[1])
                                                {
                                                    displayopt = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            //formattedAnswer = "Q" + qustionDisplayOrder + "_M" + displaycol + "_C" + displayopt;
                                            formattedAnswer = "Q" + questionId + "_M" + displaycol + "_C" + displayopt;
                                            answerValue = ansExpression.Split('~')[0].Split('*')[1];
                                            if (questionTypeId == 14)
                                                DDlDisplayOrder = dropDownDisplayOrder.Where(l => l.Id == Convert.ToInt32(answerValue)).FirstOrDefault().DDLDisplayOrder;
                                        }
                                        else
                                        {
                                            foreach (var item in optionDisplayOrder)
                                            {
                                                if (Convert.ToString(item.OptionId) == ansExpression.Split('~')[0])
                                                {
                                                    displaycol = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            foreach (var item in columnDisplayOrder)
                                            {
                                                if (Convert.ToString(item.Id) == ansExpression.Split('~')[1])
                                                {
                                                    displayopt = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            //formattedAnswer = "Q" + qustionDisplayOrder + "_M" + displaycol + "_C" + displayopt;
                                            formattedAnswer = "Q" + questionId + "_M" + displaycol + "_C" + displayopt;
                                        }
                                        foreach (DataColumn column in dtF.Columns)
                                        {
                                            string[] columnHeader = column.ColumnName.Split('-');

                                            if (columnHeader.Length > 1)
                                            {
                                                string[] answer = columnHeader[0].Split('_');

                                                if (answer.Length > 2)
                                                {
                                                    string answerInHeader = answer[0] + "_" + answer[1] + "_" + answer[2];

                                                    if (formattedAnswer == answerInHeader)
                                                    {
                                                        int index = dr_dtF.Table.Columns[column.ColumnName].Ordinal;
                                                        lstColumnOrdinals.Add(index.ToString());
                                                        //count++;
                                                        if (questionTypeId == 9)
                                                        {
                                                            dr_dtF[index] = answerValue;
                                                        }
                                                        else if (questionTypeId == 14)
                                                        {
                                                            dr_dtF[index] = DDlDisplayOrder;
                                                        }
                                                        else
                                                        {
                                                            dr_dtF[index] = "1";
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }

                                // Fill All the Empty cells with 2

                                for (int i = 0; i <= dtF.Columns.Count - 1; i++)
                                    if (String.IsNullOrEmpty(Convert.ToString(dr_dtF[i])))
                                        dr_dtF[i] = "";
                            }

                            else
                            {
                                dr_dtF[count] = answerExpression;
                                count++;
                                optionsCount--;
                            }



                            if (productId != oldProductID)
                            {
                                dtF.Rows.Add(drTemp);
                                drTemp[0] = response_id;
                                oldProductID = productId;
                            }

                            drTemp = dr_dtF;
                            //---ss
                            if (model.projectModel.ProjectType == "2")
                            {
                                drTemp[2] = product_desc;
                                drTemp[1] = product_logiId;
                            }
                            //---ss


                        }


                        dtF.Rows.Add(dr_dtF);
                        drTemp[0] = response_id;
                        //---ss
                        if (model.projectModel.ProjectType == "2")
                        {
                            dr_dtF[2] = product_desc;
                            dr_dtF[1] = product_logiId;
                        }
                        response_id++;
                        //---ss
                    }
                }
            }
            catch (Exception ex)
            {

            }
            if (dtF.Columns.Count > 0)
            {
                string ReportDataHtml = "<table id='hdnReportHtml'>";
                DataTable dtReport = dtF;
                if (dtReport != null)
                {
                    string[] cols = (from dc in dtReport.Columns.Cast<DataColumn>()
                                     select dc.ColumnName)
                                   .ToArray();
                    if (cols != null)
                    {
                        ReportDataHtml += "<tr class='thRow'>";
                        foreach (var item in cols)
                        {
                            ReportDataHtml += "<th>" + item + "</th>";
                        }
                        ReportDataHtml += "</tr>";
                    }

                    foreach (DataRow row in dtReport.Rows)
                    {
                        ReportDataHtml += "<tr class='tdRow'>";
                        foreach (var item in row.ItemArray)
                        {
                            ReportDataHtml += "<td>" + item + "</td>";
                        }
                        ReportDataHtml += "</tr>";
                    }
                }
                ReportDataHtml += "</table>";
                string responseXML = ConvertDatatableToXML(dtF);
                actionResult = adminAction.ProjectRespose_Add(proId, Id, responseXML, ReportDataHtml);
            }
        }
        #endregion

        #region GetReportDataTableOnline for Combined
        [WebMethod]
        public DataTable GetRepOnline(string Id, int proId)
        {
            RespondentController respondent = new RespondentController();
            SurveyResponseModel model = new SurveyResponseModel();
            DataTable dtF = new DataTable();
            DataTable newData = new DataTable();
            try
            {
                DataTable dt = new DataTable();

                string projectGuid = string.Empty;
                projectGuid = Id;
                dt.Columns.Add("Resp_Id", typeof(string));

                model = respondent.GetProjectPreview(projectGuid);

                //--ss
                if (model.projectModel.ProjectType == "2")
                {
                    DataRow drnew = dt.NewRow();
                    drnew[0] = "Product_LogicalId";
                    dt.Rows.Add(drnew);
                    DataRow drnew1 = dt.NewRow();
                    drnew1[0] = "Product_Name";
                    dt.Rows.Add(drnew1);
                }
                //--

                // Load all the questions of this project
                List<QuestionList> questionsList = new List<QuestionList>();
                questionBase.ProjectId = Convert.ToInt32(model.projectModel.Id);
                actionResult = adminAction.QuestionsLoadAll_ByProjectId(questionBase);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    foreach (DataRow dr in actionResult.dtResult.Rows)
                    {
                        questionsList.Add(new QuestionList
                        {
                            id = dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0,
                            questionTypeId = dr["QuestionTypeId"] != DBNull.Value ? Convert.ToInt32(dr["QuestionTypeId"]) : 0
                        });
                    }
                }

                string fileName = Guid.NewGuid().ToString().Substring(0, 5) + "_" + DateTime.Now.ToString("MM-dd-yyyy") + "_" + model.projectModel.ProjectName.Replace(" ", "");

                if (model.lstQuestionModel != null && model.lstQuestionModel.Count > 0)
                {
                    foreach (var item in model.lstQuestionModel)
                    {
                        if (item.PipedFrom > 0 && (!item.QuestionTitle.Contains("{{Q}}")))
                        {
                            if (item.QuestionTypeId == 2 || item.QuestionTypeId == 4 || item.QuestionTypeId == 5 || item.QuestionTypeId == 6)//Multi Punch || Constant Sum || Ranking || Matrix Type
                            {
                                if (model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel != null && model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel.Count > 0)
                                {
                                    foreach (var opt in model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel)
                                    {
                                        DataRow drnew = dt.NewRow();
                                        drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                        dt.Rows.Add(drnew);
                                    }
                                }
                            }

                            else if (item.QuestionTypeId == 1 || item.QuestionTypeId == 3 || item.QuestionTypeId == 7 || item.QuestionTypeId == 15)//Single Punch || Range || Simple Text
                            {
                                DataRow drnew = dt.NewRow();
                                drnew[0] = "Q" + item.QuestionId + "_S" + "-" + ReplaceHtml.ReplaceText(item.QuestionTitle);
                                dt.Rows.Add(drnew);
                            }
                            else if (item.QuestionTypeId == 8 || item.QuestionTypeId == 12)//Single punch matrix type  && Rank order type
                            {
                                //if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
                                //{
                                //foreach (var opt in item.lstOptionsModel)
                                //{
                                //    DataRow drnew = dt.NewRow();
                                //    //drnew[0] = "Q" + item.QuestionId + "_M" + opt.OptionId + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                //    drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                //    dt.Rows.Add(drnew);
                                //}
                                //}
                                if (item.lstMatrixTypeModel != null && item.lstMatrixTypeModel.Count > 0)
                                {
                                    foreach (var col in item.lstMatrixTypeModel)
                                    {
                                        DataRow drnew = dt.NewRow();
                                        //drnew[0] = "Q" + item.QuestionId + "_M" + opt.OptionId + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                        drnew[0] = "Q" + item.QuestionId + "_M" + col.DisplayOrder + "-" + ReplaceHtml.ReplaceText(col.ColumnName);
                                        dt.Rows.Add(drnew);
                                    }
                                }
                            }
                            else if (item.QuestionTypeId == 13)//Kano model matrix type 
                            {
                                if (model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel != null && model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel.Count > 0)
                                {
                                    foreach (var opt in model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel)
                                    {
                                        DataRow drnew = dt.NewRow();
                                        drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Present";
                                        dt.Rows.Add(drnew);
                                        DataRow drnew1 = dt.NewRow();
                                        drnew1[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Absent";
                                        dt.Rows.Add(drnew1);
                                    }
                                }
                            }
                            // Single Punch Matrix Type || Range Matrix Type Question ||
                            // Single punch type answer horizontal position || 
                            // Multiple punch type answer horizontal position || 
                            // Rank order type question || 
                            // KANO Model Matrix type question || Dropdown Matrix type question
                            else if (item.QuestionTypeId > 7 && item.QuestionTypeId != 15)
                            {
                                if (model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel != null && model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel.Count > 0)
                                {
                                    foreach (var opt in model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel)
                                    {
                                        foreach (var column in item.lstMatrixTypeModel)
                                        {
                                            DataRow drnew = dt.NewRow();
                                            drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "_C" + column.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + " " + ReplaceHtml.ReplaceText(column.ColumnName);
                                            dt.Rows.Add(drnew);
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (item.QuestionTypeId == 2 || item.QuestionTypeId == 4 || item.QuestionTypeId == 5 || item.QuestionTypeId == 6)//Multi Punch || Constant Sum || Ranking || Matrix Type
                            {
                                if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
                                {
                                    foreach (var opt in item.lstOptionsModel)
                                    {
                                        DataRow drnew = dt.NewRow();
                                        //drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                        drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                        dt.Rows.Add(drnew);
                                    }
                                }
                            }

                            else if (item.QuestionTypeId == 1 || item.QuestionTypeId == 3 || item.QuestionTypeId == 7 || item.QuestionTypeId == 15)//Single Punch || Range || Simple Text
                            {
                                DataRow drnew = dt.NewRow();
                                //drnew[0] = "Q" + item.DisplayOrder + "_S" + "-" + ReplaceHtml.ReplaceText(item.QuestionTitle);
                                drnew[0] = "Q" + item.QuestionId + "_S" + "-" + ReplaceHtml.ReplaceText(item.QuestionTitle);
                                dt.Rows.Add(drnew);
                            }
                            else if (item.QuestionTypeId == 8 || item.QuestionTypeId == 12)//Single punch matrix type  && Rank order type
                            {
                                //if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
                                //{
                                //foreach (var opt in item.lstOptionsModel)
                                //{
                                //    DataRow drnew = dt.NewRow();
                                //    //drnew[0] = "Q" + item.QuestionId + "_M" + opt.OptionId + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                //    drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                //    dt.Rows.Add(drnew);
                                //}
                                //}
                                if (item.lstMatrixTypeModel != null && item.lstMatrixTypeModel.Count > 0)
                                {
                                    foreach (var col in item.lstMatrixTypeModel)
                                    {
                                        DataRow drnew = dt.NewRow();
                                        //drnew[0] = "Q" + item.QuestionId + "_M" + opt.OptionId + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                        drnew[0] = "Q" + item.QuestionId + "_M" + col.DisplayOrder + "-" + ReplaceHtml.ReplaceText(col.ColumnName);
                                        dt.Rows.Add(drnew);
                                    }
                                }
                            }
                            else if (item.QuestionTypeId == 13)//Kano model matrix type 
                            {
                                if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
                                {
                                    foreach (var opt in item.lstOptionsModel)
                                    {
                                        DataRow drnew = dt.NewRow();
                                        //drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Present";
                                        drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Present";
                                        dt.Rows.Add(drnew);
                                        DataRow drnew1 = dt.NewRow();
                                        //drnew1[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Absent";
                                        drnew1[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Absent";
                                        dt.Rows.Add(drnew1);
                                    }
                                }
                            }
                            // Single Punch Matrix Type || Range Matrix Type Question ||
                            // Single punch type answer horizontal position || 
                            // Multiple punch type answer horizontal position || 
                            // Rank order type question || 
                            // KANO Model Matrix type question || Dropdown Matrix type question
                            else if (item.QuestionTypeId > 7)
                            {
                                if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
                                {
                                    foreach (var opt in item.lstOptionsModel)
                                    {
                                        foreach (var column in item.lstMatrixTypeModel)
                                        {
                                            DataRow drnew = dt.NewRow();
                                            //drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "_C" + column.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + " " + ReplaceHtml.ReplaceText(column.ColumnName);
                                            drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "_C" + column.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + " " + ReplaceHtml.ReplaceText(column.ColumnName);
                                            dt.Rows.Add(drnew);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                // Get the Excel Pattern of dt
                dtF = comMethods.GenerateTransposedTable(dt);

                // Get all the reponders
                projectBase.Id = model.projectModel.Id;
                actionResult = adminAction.CompleteRespondersLoad_ByProjectId(projectBase);
                List<RespondersModel> lstResponders = new List<RespondersModel>();
                foreach (DataRow dr in actionResult.dtResult.Rows)
                {
                    lstResponders.Add(new RespondersModel
                    {

                        InvitationGuid = dr["InvitationGuid"] != DBNull.Value ? Convert.ToString(dr["InvitationGuid"]) : "",
                        Email = dr["Email"] != DBNull.Value ? Convert.ToString(dr["Email"]) : "",

                    });
                }
                IEnumerable<RespondersModel> filteredList = lstResponders
                 .GroupBy(customer => customer.InvitationGuid).Select(group => group.First());
                // Get Responses
                SurveyLoadBase surveyLoadBase = new SurveyLoadBase();
                string json = string.Empty;
                SRV.BaseLayer.ActionResult actionResultResponses = new SRV.BaseLayer.ActionResult();
                int response_id = 1;            //ss
                foreach (var responder in filteredList)
                {
                    DataTable responsesDt = new DataTable();
                    surveyLoadBase.Id = model.projectModel.Id;
                    surveyLoadBase.Email = (!string.IsNullOrEmpty(responder.Email) ? Convert.ToString(responder.Email) : "");
                    surveyLoadBase.ExternalResponderId = (!string.IsNullOrEmpty(responder.InvitationGuid) ? Convert.ToString(responder.InvitationGuid) : "");
                    if (model.projectModel.lstProductProjectModel != null && model.projectModel.lstProductProjectModel.Count > 0)
                    {
                        foreach (var productModel in model.projectModel.lstProductProjectModel)
                        {
                            surveyLoadBase.ProductId = productModel.Id;
                            actionResultResponses = respondentAction.Response_LoadByProjectId(surveyLoadBase);
                            if (actionResultResponses.IsSuccess)
                            {
                                if (responsesDt.Rows.Count == 0)
                                    responsesDt = actionResultResponses.dtResult.Copy();
                                else
                                {
                                    foreach (DataRow drThisRespRow in actionResultResponses.dtResult.Rows)
                                        responsesDt.ImportRow(drThisRespRow);
                                }
                            }
                        }
                    }
                    else
                    {
                        actionResultResponses = respondentAction.Response_LoadByProjectId(surveyLoadBase);
                        if (actionResultResponses.IsSuccess)
                            responsesDt = actionResultResponses.dtResult.Copy();

                    }

                    // surveyLoadBase.OfflineResponderId = dr["RespId"] != DBNull.Value ? Convert.ToString(dr["RespId"]) : "";
                    //DataRow dr_dtF = dtF.NewRow();
                    // if (String.IsNullOrEmpty(surveyLoadBase.Email)) continue;

                    //actionResultResponses = respondentAction.Response_LoadByProjectId(surveyLoadBase);
                    if (responsesDt.Rows.Count > 0)//actionResultResponses.IsSuccess
                    {
                        //dr_dtF[0] = surveyLoadBase.Email;

                        int count = 1;
                        count = (model.projectModel.ProjectType == "2") ? count + 2 : count;        //ss
                        DataRow dr_dtF = dtF.NewRow();
                        DataRow drTemp = dtF.NewRow();
                        string product_desc = string.Empty;
                        string product_logiId = string.Empty;
                        int oldProductID = responsesDt.Rows[0]["ProductId"] != DBNull.Value ? Convert.ToInt32(responsesDt.Rows[0]["ProductId"]) : 0;
                        foreach (DataRow drResponse in responsesDt.Rows)// actionResultResponses.dtResult.Rows
                        {

                            int optionsCount = 0;
                            // response_id = drResponse["ReponderId"] != DBNull.Value ? (responsesDt.Rows[0]["ReponderId"]).ToString() : "";
                            int productId = drResponse["ProductId"] != DBNull.Value ? Convert.ToInt32(drResponse["ProductId"]) : 0;
                            if (productId != oldProductID)
                            {
                                count = (model.projectModel.ProjectType == "2") ? 3 : 1;
                                dr_dtF = dtF.NewRow();
                            }

                            //--ss

                            product_desc = drResponse["ProductDescription"] != DBNull.Value ? Convert.ToString(drResponse["ProductDescription"]) : "";
                            product_logiId = drResponse["ProductLogicalId"] != DBNull.Value ? Convert.ToString(drResponse["ProductLogicalId"]) : "";

                            //--ss
                            int questionId = drResponse["QuestionId"] != DBNull.Value ? Convert.ToInt32(drResponse["QuestionId"]) : 0;
                            string answerExpression = drResponse["AnswerExpression"] != DBNull.Value ? Convert.ToString(drResponse["AnswerExpression"]) : "";
                            int questionTypeId = drResponse["QuestionTypeId"] != DBNull.Value ? Convert.ToInt32(drResponse["QuestionTypeId"]) : 1;
                            int PipedFrom = drResponse["PipedFrom"] != DBNull.Value ? Convert.ToInt32(drResponse["PipedFrom"]) : 0;
                            //dr_dtF[0] = drResponse;

                            // Get Total options count for this question
                            questionBase.Id = Convert.ToInt32(questionId);
                            SRV.BaseLayer.ActionResult actionResultAnswersTemp = new SRV.BaseLayer.ActionResult();
                            actionResultAnswersTemp = adminAction.Option_LoadAllById(questionBase);
                            if (actionResultAnswersTemp.dtResult != null && actionResultAnswersTemp.dtResult.Rows.Count > 0)
                            {
                                optionsCount = actionResultAnswersTemp.dtResult.Rows.Count;
                            }

                            if (questionTypeId == 2 || questionTypeId == 4 || questionTypeId == 5 || questionTypeId == 6)
                            {
                                string[] allAnswers = answerExpression.Split('|');
                                if (allAnswers.Length > 0)
                                {
                                    if (questionTypeId == 2)// Multi Punch
                                    {
                                        string allRec = string.Empty;
                                        List<OptionsModel> matrixAns = new List<OptionsModel>();
                                        if (PipedFrom > 0 && !drResponse["QuestionTitle"].ToString().Contains("{{Q}}"))
                                        {
                                            matrixAns = model.lstQuestionModel.Where(v => v.QuestionId == PipedFrom).FirstOrDefault().lstOptionsModel;
                                        }
                                        else
                                        {
                                            matrixAns = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                        }

                                        foreach (var item in matrixAns)
                                        {
                                            string matchedId = string.Empty;
                                            foreach (string ans in allAnswers)
                                            {
                                                if (Convert.ToString(item.OptionId) == ans)
                                                {
                                                    matchedId = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            allRec += matchedId + "|";
                                        }

                                        string[] allAnswersMat = allRec.Substring(0, allRec.Length - 1).Split('|');
                                        foreach (string ans in allAnswersMat)
                                        {

                                            if (count <= dtF.Columns.Count - 1)
                                            {
                                                if ((Regex.Replace(dtF.Columns[count].ToString().Split('-')[0].Split('_')[1], @"[^\d]", String.Empty)) == ans)
                                                {
                                                    dr_dtF[count] = "1";// ans;
                                                    count++;
                                                    optionsCount--;
                                                }
                                                else
                                                {
                                                    dr_dtF[count] = "2";
                                                    count++;
                                                    optionsCount--;
                                                }
                                            }
                                        }
                                        //count = count + optionsCount;
                                    }
                                    else if (questionTypeId == 6) // matrix Type
                                    {
                                        foreach (string ans in allAnswers)
                                        {
                                            dr_dtF[count] = (ans.ToLower().IndexOf("yes") != -1) ? "1" : "2";
                                            count++;
                                            optionsCount--;
                                        }
                                        count = count + optionsCount;
                                    }
                                    else
                                    {
                                        foreach (string ans in allAnswers)
                                        {
                                            dr_dtF[count] = ans;
                                            count++;
                                            optionsCount--;
                                        }
                                        count = count + optionsCount;
                                    }
                                }
                            }
                            else if (questionTypeId == 1) // Single Punch Answers
                            {
                                //List<OptionsModel> SinglePunchAns = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                List<OptionsModel> SinglePunchAns = new List<OptionsModel>();
                                if (PipedFrom > 0 && !drResponse["QuestionTitle"].ToString().Contains("{{Q}}"))
                                {
                                    SinglePunchAns = model.lstQuestionModel.Where(v => v.QuestionId == PipedFrom).FirstOrDefault().lstOptionsModel;
                                }
                                else
                                {
                                    SinglePunchAns = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                }
                                int ansIndex = 0;
                                foreach (var item in SinglePunchAns)
                                {
                                    ansIndex++;
                                    if (Convert.ToString(item.OptionId) == answerExpression)
                                    {
                                        break;
                                    }
                                }

                                dr_dtF[count] = ansIndex;//answerExpression;
                                count++;
                                optionsCount--;
                            }
                            else if (questionTypeId == 8 || questionTypeId == 12) // single punch matrix answers
                            {
                                List<OptionsModel> optionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                List<MatrixTypeModel> columnDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstMatrixTypeModel;
                                int qustionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).Select(v => v.DisplayOrder).FirstOrDefault();
                                string[] allAnswers = answerExpression.Split('|');

                                List<string> lstColumnOrdinals = new List<string>();

                                string formattedAnswer = string.Empty;
                                string answerValue = string.Empty;
                                if (allAnswers.Length > 0)
                                {
                                    foreach (var ansExpression in allAnswers)
                                    {
                                        string displaycol = string.Empty;
                                        string displayopt = string.Empty;
                                        foreach (var item in optionDisplayOrder)
                                        {

                                            if (Convert.ToString(item.OptionId) == ansExpression.Split('~')[0])
                                            {
                                                displaycol = Convert.ToString(item.DisplayOrder);
                                                break;
                                            }
                                        }
                                        foreach (var item in columnDisplayOrder)
                                        {
                                            if (Convert.ToString(item.Id) == ansExpression.Split('~')[1])
                                            {
                                                displayopt = Convert.ToString(item.DisplayOrder);
                                                break;
                                            }
                                        }
                                        //  formattedAnswer = "Q" + questionId + "_M" + displaycol + "#" + displayopt;
                                        formattedAnswer = "Q" + questionId + "_M" + displayopt + "#" + displaycol;
                                        foreach (DataColumn column in dtF.Columns)
                                        {
                                            string[] columnHeader = column.ColumnName.Split('-');
                                            string[] answer = columnHeader[0].Split('_');
                                            if (columnHeader.Length > 1)
                                            {
                                                string answerInHeader = answer[0] + "_" + answer[1];
                                                string[] formattedAnswerArr = formattedAnswer.Split('#');
                                                if (formattedAnswerArr[0] == answerInHeader)
                                                {
                                                    int index = dr_dtF.Table.Columns[column.ColumnName].Ordinal;
                                                    lstColumnOrdinals.Add(index.ToString());
                                                    count++;
                                                    dr_dtF[index] = formattedAnswerArr[1];
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            else if (questionTypeId == 13) // Kano model matrix type
                            {
                                List<OptionsModel> optionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                List<MatrixTypeModel> columnDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstMatrixTypeModel;
                                int qustionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).Select(v => v.DisplayOrder).FirstOrDefault();
                                string[] allAnswers = answerExpression.Split('|');

                                List<string> lstColumnOrdinals = new List<string>();

                                string formattedAnswer = string.Empty;
                                string answerValue = string.Empty;
                                if (allAnswers.Length > 1)
                                {
                                    int repeatId = 0;
                                    foreach (var ansExpression in allAnswers)
                                    {
                                        string displaycol = string.Empty;
                                        string displayopt = string.Empty;
                                        string displayText = string.Empty;
                                        string text = string.Empty;
                                        if (repeatId == Convert.ToInt32(ansExpression.Split('~')[0]))
                                        {
                                            text = "_Absent";
                                            repeatId = 0;
                                        }
                                        else
                                        {
                                            text = "_Present";
                                        }
                                        foreach (var item in optionDisplayOrder)
                                        {
                                            if (Convert.ToString(item.OptionId) == ansExpression.Split('~')[0])
                                            {
                                                displayText = Convert.ToString(item.Option) + text;
                                                displaycol = Convert.ToString(item.DisplayOrder);
                                                repeatId = Convert.ToInt32(ansExpression.Split('~')[0]);
                                                break;
                                            }
                                        }
                                        foreach (var item in columnDisplayOrder)
                                        {
                                            if (Convert.ToString(item.Id) == ansExpression.Split('~')[1])
                                            {
                                                displayopt = Convert.ToString(item.DisplayOrder);
                                                break;
                                            }
                                        }
                                        //formattedAnswer = "Q" + qustionDisplayOrder + "_M" + displaycol + "-" + displayText + "#" + displayopt;
                                        formattedAnswer = "Q" + questionId + "_M" + displaycol + "-" + displayText + "#" + displayopt;
                                        foreach (DataColumn column in dtF.Columns)
                                        {

                                            string[] columnHeader = column.ColumnName.Split('-');
                                            string[] answer = columnHeader[0].Split('_');
                                            if (columnHeader.Length > 1)
                                            {
                                                string answerInHeader = answer[0] + "_" + answer[1] + "-" + columnHeader[1];
                                                string[] formattedAnswerArr = formattedAnswer.Split('#');
                                                if (Regex.Replace(formattedAnswerArr[0], @"\s+", "", RegexOptions.Multiline) == Regex.Replace(answerInHeader, @"\s+", "", RegexOptions.Multiline))
                                                {
                                                    int index = dr_dtF.Table.Columns[column.ColumnName].Ordinal;
                                                    lstColumnOrdinals.Add(index.ToString());
                                                    count++;
                                                    dr_dtF[index] = formattedAnswerArr[1];
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            else if (questionTypeId > 7 && questionTypeId != 15)
                            {
                                List<OptionsModel> optionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                List<MatrixTypeModel> columnDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstMatrixTypeModel;
                                List<DDLMatrixTypeModel> dropDownDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstDDlMatrixModel;
                                //count = count + (optionDisplayOrder.Count * columnDisplayOrder.Count);
                                int optDisplayOrder = 0;
                                if (optionDisplayOrder != null)
                                {
                                    optDisplayOrder = optionDisplayOrder.Count;
                                }
                                int colDisplayOrder = 0;
                                if (columnDisplayOrder != null)
                                {
                                    colDisplayOrder = columnDisplayOrder.Count;
                                }
                                int qustionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).Select(v => v.DisplayOrder).FirstOrDefault();
                                string[] allAnswers = answerExpression.Split('|');

                                List<string> lstColumnOrdinals = new List<string>();

                                string formattedAnswer = string.Empty;
                                string answerValue = string.Empty;
                                int DDlDisplayOrder = 0;
                                if (allAnswers.Length > 1)
                                {
                                    foreach (var ansExpression in allAnswers)
                                    {
                                        string displaycol = string.Empty;
                                        string displayopt = string.Empty;

                                        if (questionTypeId == 9 || questionTypeId == 14)
                                        {
                                            foreach (var item in optionDisplayOrder)
                                            {
                                                if (Convert.ToString(item.OptionId) == ansExpression.Split('~')[0].Split('*')[0])
                                                {
                                                    displaycol = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            foreach (var item in columnDisplayOrder)
                                            {
                                                if (Convert.ToString(item.Id) == ansExpression.Split('~')[1])
                                                {
                                                    displayopt = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            //formattedAnswer = "Q" + qustionDisplayOrder + "_M" + displaycol + "_C" + displayopt;
                                            formattedAnswer = "Q" + questionId + "_M" + displaycol + "_C" + displayopt;
                                            answerValue = ansExpression.Split('~')[0].Split('*')[1];
                                            if (questionTypeId == 14)
                                                DDlDisplayOrder = dropDownDisplayOrder.Where(l => l.Id == Convert.ToInt32(answerValue)).FirstOrDefault().DDLDisplayOrder;
                                        }
                                        else
                                        {
                                            foreach (var item in optionDisplayOrder)
                                            {
                                                if (Convert.ToString(item.OptionId) == ansExpression.Split('~')[0])
                                                {
                                                    displaycol = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            foreach (var item in columnDisplayOrder)
                                            {
                                                if (Convert.ToString(item.Id) == ansExpression.Split('~')[1])
                                                {
                                                    displayopt = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            //formattedAnswer = "Q" + qustionDisplayOrder + "_M" + displaycol + "_C" + displayopt;
                                            formattedAnswer = "Q" + questionId + "_M" + displaycol + "_C" + displayopt;
                                        }
                                        foreach (DataColumn column in dtF.Columns)
                                        {
                                            string[] columnHeader = column.ColumnName.Split('-');

                                            if (columnHeader.Length > 1)
                                            {
                                                string[] answer = columnHeader[0].Split('_');

                                                if (answer.Length > 2)
                                                {
                                                    string answerInHeader = answer[0] + "_" + answer[1] + "_" + answer[2];

                                                    if (formattedAnswer == answerInHeader)
                                                    {
                                                        int index = dr_dtF.Table.Columns[column.ColumnName].Ordinal;
                                                        lstColumnOrdinals.Add(index.ToString());
                                                        //count++;
                                                        if (questionTypeId == 9)
                                                        {
                                                            dr_dtF[index] = answerValue;
                                                        }
                                                        else if (questionTypeId == 14)
                                                        {
                                                            dr_dtF[index] = DDlDisplayOrder;
                                                        }
                                                        else
                                                        {
                                                            dr_dtF[index] = "1";
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }

                                // Fill All the Empty cells with 2

                                for (int i = 0; i <= dtF.Columns.Count - 1; i++)
                                    if (String.IsNullOrEmpty(Convert.ToString(dr_dtF[i])))
                                        dr_dtF[i] = "";
                            }

                            else
                            {
                                dr_dtF[count] = answerExpression;
                                count++;
                                optionsCount--;
                            }

                            if (productId != oldProductID)
                            {
                                dtF.Rows.Add(drTemp);
                                //  drTemp[0] = dtF.Rows.IndexOf(drTemp) + 1;
                                drTemp[0] = response_id;
                                oldProductID = productId;
                            }

                            drTemp = dr_dtF;
                            //---ss
                            if (model.projectModel.ProjectType == "2")
                            {
                                drTemp[2] = product_desc;
                                drTemp[1] = product_logiId;
                            }
                            //---ss

                        }

                        dtF.Rows.Add(dr_dtF);
                        //dr_dtF[0] = dtF.Rows.IndexOf(dr_dtF) + 1;
                        drTemp[0] = response_id;
                        //---ss
                        if (model.projectModel.ProjectType == "2")
                        {
                            dr_dtF[2] = product_desc;
                            dr_dtF[1] = product_logiId;
                        }
                        response_id++;
                        //---ss
                    }
                }

                if (dtF != null && dtF.Rows.Count > 0)
                {
                    //DataTable dtF = comMethods.GenerateTransposedTable(dt);
                    newData = comMethods.GenerateTransposedTable(dt);
                    foreach (DataRow dr in dtF.Rows)
                    {
                        if (model.projectModel.ProjectType == "2")
                        {
                            DataRow rw = newData.AsEnumerable().FirstOrDefault(tt => tt.Field<string>("Resp_Id") == dr["Resp_Id"].ToString() && tt.Field<string>("Product_Name") == dr["Product_Name"].ToString());
                            if (rw == null)
                            {
                                DataRow dr_dtF = newData.NewRow();

                                newData.Rows.Add(dr_dtF);
                                foreach (DataColumn dcTempKano in dtF.Columns)
                                {

                                    dr_dtF[dcTempKano.ToString()] = dr[dcTempKano.ToString()];

                                }
                                // row exists
                            }
                        }
                        else
                        {
                            DataRow rw = newData.AsEnumerable().FirstOrDefault(tt => tt.Field<string>("Resp_Id") == dr["Resp_Id"].ToString());
                            if (rw == null)
                            {
                                DataRow dr_dtF = newData.NewRow();

                                newData.Rows.Add(dr_dtF);
                                foreach (DataColumn dcTempKano in dtF.Columns)
                                {

                                    dr_dtF[dcTempKano.ToString()] = dr[dcTempKano.ToString()];

                                }
                                // row exists
                            }
                        }


                    }
                }
            }
            catch (Exception ex)
            {

            }
            return newData;
        }
        #endregion

        #region GetReportOffline for Combined
        [WebMethod]
        public DataTable GetRepOffline(string Id, int ResponseCount, int proId)
        {
            RespondentController respondent = new RespondentController();
            SurveyResponseModel model = new SurveyResponseModel();
            DataTable dtF = new DataTable();
            try
            {
                DataTable dt = new DataTable();

                string projectGuid = string.Empty;
                projectGuid = Id;
                dt.Columns.Add("Resp_Id", typeof(string));

                model = respondent.GetProjectPreview(projectGuid);

                //--ss
                if (model.projectModel.ProjectType == "2")
                {
                    DataRow drnew = dt.NewRow();
                    drnew[0] = "Product_LogicalId";
                    dt.Rows.Add(drnew);
                    DataRow drnew1 = dt.NewRow();
                    drnew1[0] = "Product_Name";
                    dt.Rows.Add(drnew1);
                }
                //--

                // Load all the questions of this project
                List<QuestionList> questionsList = new List<QuestionList>();
                questionBase.ProjectId = Convert.ToInt32(model.projectModel.Id);
                actionResult = adminAction.QuestionsLoadAll_ByProjectId(questionBase);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    foreach (DataRow dr in actionResult.dtResult.Rows)
                    {
                        questionsList.Add(new QuestionList
                        {
                            id = dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0,
                            questionTypeId = dr["QuestionTypeId"] != DBNull.Value ? Convert.ToInt32(dr["QuestionTypeId"]) : 0
                        });
                    }
                }

                string fileName = Guid.NewGuid().ToString().Substring(0, 5) + "_" + DateTime.Now.ToString("MM-dd-yyyy") + "_" + model.projectModel.ProjectName.Replace(" ", "");

                if (model.lstQuestionModel != null && model.lstQuestionModel.Count > 0)
                {
                    foreach (var item in model.lstQuestionModel)
                    {
                        if (item.PipedFrom > 0 && (!item.QuestionTitle.Contains("{{Q}}")))
                        {
                            if (item.QuestionTypeId == 2 || item.QuestionTypeId == 4 || item.QuestionTypeId == 5 || item.QuestionTypeId == 6)//Multi Punch || Constant Sum || Ranking || Matrix Type
                            {
                                if (model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel != null && model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel.Count > 0)
                                {
                                    foreach (var opt in model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel)
                                    {
                                        DataRow drnew = dt.NewRow();
                                        drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                        dt.Rows.Add(drnew);
                                    }
                                }
                            }

                            else if (item.QuestionTypeId == 1 || item.QuestionTypeId == 3 || item.QuestionTypeId == 7 || item.QuestionTypeId == 15)//Single Punch || Range || Simple Text
                            {
                                DataRow drnew = dt.NewRow();
                                drnew[0] = "Q" + item.QuestionId + "_S" + "-" + ReplaceHtml.ReplaceText(item.QuestionTitle);
                                dt.Rows.Add(drnew);
                            }
                            else if (item.QuestionTypeId == 8 || item.QuestionTypeId == 12)//Single punch matrix type  && Rank order type
                            {
                                //if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
                                //{
                                //foreach (var opt in item.lstOptionsModel)
                                //{
                                //    DataRow drnew = dt.NewRow();
                                //    //drnew[0] = "Q" + item.QuestionId + "_M" + opt.OptionId + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                //    drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                //    dt.Rows.Add(drnew);
                                //}
                                //}
                                if (item.lstMatrixTypeModel != null && item.lstMatrixTypeModel.Count > 0)
                                {
                                    foreach (var col in item.lstMatrixTypeModel)
                                    {
                                        DataRow drnew = dt.NewRow();
                                        //drnew[0] = "Q" + item.QuestionId + "_M" + opt.OptionId + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                        drnew[0] = "Q" + item.QuestionId + "_M" + col.DisplayOrder + "-" + ReplaceHtml.ReplaceText(col.ColumnName);
                                        dt.Rows.Add(drnew);
                                    }
                                }
                            }
                            else if (item.QuestionTypeId == 13)//Kano model matrix type 
                            {
                                if (model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel != null && model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel.Count > 0)
                                {
                                    foreach (var opt in model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel)
                                    {
                                        DataRow drnew = dt.NewRow();
                                        drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Present";
                                        dt.Rows.Add(drnew);
                                        DataRow drnew1 = dt.NewRow();
                                        drnew1[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Absent";
                                        dt.Rows.Add(drnew1);
                                    }
                                }
                            }
                            // Single Punch Matrix Type || Range Matrix Type Question ||
                            // Single punch type answer horizontal position || 
                            // Multiple punch type answer horizontal position || 
                            // Rank order type question || 
                            // KANO Model Matrix type question || Dropdown Matrix type question
                            else if (item.QuestionTypeId > 7 && item.QuestionTypeId != 15)
                            {
                                if (model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel != null && model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel.Count > 0)
                                {
                                    foreach (var opt in model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel)
                                    {
                                        foreach (var column in item.lstMatrixTypeModel)
                                        {
                                            DataRow drnew = dt.NewRow();
                                            drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "_C" + column.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + " " + ReplaceHtml.ReplaceText(column.ColumnName);
                                            dt.Rows.Add(drnew);
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (item.QuestionTypeId == 2 || item.QuestionTypeId == 4 || item.QuestionTypeId == 5 || item.QuestionTypeId == 6)//Multi Punch || Constant Sum || Ranking || Matrix Type
                            {
                                if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
                                {
                                    foreach (var opt in item.lstOptionsModel)
                                    {
                                        DataRow drnew = dt.NewRow();
                                        //drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                        drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                        dt.Rows.Add(drnew);
                                    }
                                }
                            }

                            else if (item.QuestionTypeId == 1 || item.QuestionTypeId == 3 || item.QuestionTypeId == 7 || item.QuestionTypeId == 15)//Single Punch || Range || Simple Text
                            {
                                DataRow drnew = dt.NewRow();
                                //drnew[0] = "Q" + item.DisplayOrder + "_S" + "-" + ReplaceHtml.ReplaceText(item.QuestionTitle);
                                drnew[0] = "Q" + item.QuestionId + "_S" + "-" + ReplaceHtml.ReplaceText(item.QuestionTitle);
                                dt.Rows.Add(drnew);
                            }
                            else if (item.QuestionTypeId == 8 || item.QuestionTypeId == 12)//Single punch matrix type  && Rank order type
                            {
                                //if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
                                //{
                                //foreach (var opt in item.lstOptionsModel)
                                //{
                                //    DataRow drnew = dt.NewRow();
                                //    //drnew[0] = "Q" + item.QuestionId + "_M" + opt.OptionId + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                //    drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                //    dt.Rows.Add(drnew);
                                //}
                                //}
                                if (item.lstMatrixTypeModel != null && item.lstMatrixTypeModel.Count > 0)
                                {
                                    foreach (var col in item.lstMatrixTypeModel)
                                    {
                                        DataRow drnew = dt.NewRow();
                                        //drnew[0] = "Q" + item.QuestionId + "_M" + opt.OptionId + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                        drnew[0] = "Q" + item.QuestionId + "_M" + col.DisplayOrder + "-" + ReplaceHtml.ReplaceText(col.ColumnName);
                                        dt.Rows.Add(drnew);
                                    }
                                }
                            }
                            else if (item.QuestionTypeId == 13)//Kano model matrix type 
                            {
                                if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
                                {
                                    foreach (var opt in item.lstOptionsModel)
                                    {
                                        DataRow drnew = dt.NewRow();
                                        //drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Present";
                                        drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Present";
                                        dt.Rows.Add(drnew);
                                        DataRow drnew1 = dt.NewRow();
                                        //drnew1[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Absent";
                                        drnew1[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Absent";
                                        dt.Rows.Add(drnew1);
                                    }
                                }
                            }
                            // Single Punch Matrix Type || Range Matrix Type Question ||
                            // Single punch type answer horizontal position || 
                            // Multiple punch type answer horizontal position || 
                            // Rank order type question || 
                            // KANO Model Matrix type question || Dropdown Matrix type question
                            else if (item.QuestionTypeId > 7)
                            {
                                if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
                                {
                                    foreach (var opt in item.lstOptionsModel)
                                    {
                                        foreach (var column in item.lstMatrixTypeModel)
                                        {
                                            DataRow drnew = dt.NewRow();
                                            //drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "_C" + column.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + " " + ReplaceHtml.ReplaceText(column.ColumnName);
                                            drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "_C" + column.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + " " + ReplaceHtml.ReplaceText(column.ColumnName);
                                            dt.Rows.Add(drnew);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                // Get the Excel Pattern of dt
                dtF = comMethods.GenerateTransposedTable(dt);

                // Get all the reponders
                projectBase.Id = model.projectModel.Id;
                actionResult = adminAction.Responders_Offline_LoadBy_ProjectId(projectBase);

                // Get Responses
                SurveyLoadBase surveyLoadBase = new SurveyLoadBase();
                string json = string.Empty;
                SRV.BaseLayer.ActionResult actionResultResponses = new SRV.BaseLayer.ActionResult();
                int response_id = ResponseCount;            //ss
                foreach (DataRow dr in actionResult.dtResult.Rows)
                {
                    surveyLoadBase.Id = model.projectModel.Id;
                    surveyLoadBase.OfflineResponderId = dr["RespId"] != DBNull.Value ? Convert.ToString(dr["RespId"]) : "";
                    //DataRow dr_dtF = dtF.NewRow();

                    if (String.IsNullOrEmpty(surveyLoadBase.OfflineResponderId)) continue;


                    DataTable responsesDt = new DataTable();


                    if (model.projectModel.lstProductProjectModel != null && model.projectModel.lstProductProjectModel.Count > 0)
                    {
                        foreach (var productModel in model.projectModel.lstProductProjectModel)
                        {
                            surveyLoadBase.ProductId = productModel.Id;
                            actionResultResponses = respondentAction.Response_Offline_LoadBy_ProjectId_RespId(surveyLoadBase);
                            if (actionResultResponses.IsSuccess)
                            {
                                if (responsesDt.Rows.Count == 0)
                                    responsesDt = actionResultResponses.dtResult.Copy();
                                else
                                {
                                    foreach (DataRow drThisRespRow in actionResultResponses.dtResult.Rows)
                                        responsesDt.ImportRow(drThisRespRow);

                                }
                            }
                        }
                    }
                    else
                    {
                        actionResultResponses = respondentAction.Response_Offline_LoadBy_ProjectId_RespId(surveyLoadBase);
                        if (actionResultResponses.IsSuccess)
                            responsesDt = actionResultResponses.dtResult.Copy();

                    }


                    //actionResultResponses = respondentAction.Response_Offline_LoadBy_ProjectId_RespId(surveyLoadBase);
                    if (responsesDt.Rows.Count > 0)//actionResultResponses.IsSuccess
                    {
                        //dr_dtF[0] = surveyLoadBase.Email;

                        int count = 1;
                        count = (model.projectModel.ProjectType == "2") ? count + 2 : count;        //ss
                        DataRow dr_dtF = dtF.NewRow();
                        DataRow drTemp = dtF.NewRow();
                        string product_desc = string.Empty;
                        string product_logiId = string.Empty;
                        int oldProductID = responsesDt.Rows[0]["ProductId"] != DBNull.Value ? Convert.ToInt32(responsesDt.Rows[0]["ProductId"]) : 0;
                        foreach (DataRow drResponse in responsesDt.Rows)// actionResultResponses.dtResult.Rows
                        {

                            int optionsCount = 0;

                            int productId = drResponse["ProductId"] != DBNull.Value ? Convert.ToInt32(drResponse["ProductId"]) : 0;
                            if (productId != oldProductID)
                            {
                                count = (model.projectModel.ProjectType == "2") ? 3 : 1;
                                dr_dtF = dtF.NewRow();
                            }

                            //--ss

                            product_desc = drResponse["ProductDescription"] != DBNull.Value ? Convert.ToString(drResponse["ProductDescription"]) : "";
                            product_logiId = drResponse["ProductLogicalId"] != DBNull.Value ? Convert.ToString(drResponse["ProductLogicalId"]) : "";

                            //--ss
                            int questionId = drResponse["QuestionId"] != DBNull.Value ? Convert.ToInt32(drResponse["QuestionId"]) : 0;
                            string answerExpression = drResponse["AnswerExpression"] != DBNull.Value ? Convert.ToString(drResponse["AnswerExpression"]) : "";
                            int questionTypeId = drResponse["QuestionTypeId"] != DBNull.Value ? Convert.ToInt32(drResponse["QuestionTypeId"]) : 1;
                            int PipedFrom = drResponse["PipedFrom"] != DBNull.Value ? Convert.ToInt32(drResponse["PipedFrom"]) : 0;
                            //dr_dtF[0] = drResponse;

                            // Get Total options count for this question
                            questionBase.Id = Convert.ToInt32(questionId);
                            SRV.BaseLayer.ActionResult actionResultAnswersTemp = new SRV.BaseLayer.ActionResult();
                            actionResultAnswersTemp = adminAction.Option_LoadAllById(questionBase);
                            if (actionResultAnswersTemp.dtResult != null && actionResultAnswersTemp.dtResult.Rows.Count > 0)
                            {
                                optionsCount = actionResultAnswersTemp.dtResult.Rows.Count;
                            }

                            if (questionTypeId == 2 || questionTypeId == 4 || questionTypeId == 5 || questionTypeId == 6)
                            {
                                string[] allAnswers = answerExpression.Split('|');
                                if (allAnswers.Length > 0)
                                {
                                    if (questionTypeId == 2)// Multi Punch
                                    {
                                        string allRec = string.Empty;
                                        //List<OptionsModel> matrixAns = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                        List<OptionsModel> matrixAns = new List<OptionsModel>();
                                        if (PipedFrom > 0 && !drResponse["QuestionTitle"].ToString().Contains("{{Q}}"))
                                        {
                                            matrixAns = model.lstQuestionModel.Where(v => v.QuestionId == PipedFrom).FirstOrDefault().lstOptionsModel;
                                        }
                                        else
                                        {
                                            matrixAns = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                        }
                                        foreach (var item in matrixAns)
                                        {
                                            string matchedId = string.Empty;
                                            foreach (string ans in allAnswers)
                                            {
                                                if (Convert.ToString(item.OptionId) == ans)
                                                {
                                                    matchedId = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            allRec += matchedId + "|";
                                        }

                                        string[] allAnswersMat = allRec.Substring(0, allRec.Length - 1).Split('|');
                                        foreach (string ans in allAnswersMat)
                                        {

                                            if (count <= dtF.Columns.Count - 1)
                                            {
                                                if ((Regex.Replace(dtF.Columns[count].ToString().Split('-')[0].Split('_')[1], @"[^\d]", String.Empty)) == ans)
                                                {
                                                    dr_dtF[count] = "1";// ans;
                                                    count++;
                                                    optionsCount--;
                                                }
                                                else
                                                {
                                                    dr_dtF[count] = "2";
                                                    count++;
                                                    optionsCount--;
                                                }
                                            }
                                        }
                                        //count = count + optionsCount;
                                    }
                                    else if (questionTypeId == 6) // matrix Type
                                    {
                                        foreach (string ans in allAnswers)
                                        {
                                            dr_dtF[count] = (ans.ToLower().IndexOf("yes") != -1) ? "1" : "2";
                                            count++;
                                            optionsCount--;
                                        }
                                        count = count + optionsCount;
                                    }
                                    else
                                    {
                                        foreach (string ans in allAnswers)
                                        {
                                            dr_dtF[count] = ans;
                                            count++;
                                            optionsCount--;
                                        }
                                        count = count + optionsCount;
                                    }
                                }
                            }
                            else if (questionTypeId == 1) // Single Punch Answers
                            {
                                //List<OptionsModel> SinglePunchAns = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                List<OptionsModel> SinglePunchAns = new List<OptionsModel>();
                                if (PipedFrom > 0 && !drResponse["QuestionTitle"].ToString().Contains("{{Q}}"))
                                {
                                    SinglePunchAns = model.lstQuestionModel.Where(v => v.QuestionId == PipedFrom).FirstOrDefault().lstOptionsModel;
                                }
                                else
                                {
                                    SinglePunchAns = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                }
                                int ansIndex = 0;
                                foreach (var item in SinglePunchAns)
                                {
                                    ansIndex++;
                                    if (Convert.ToString(item.OptionId) == answerExpression)
                                    {
                                        break;
                                    }
                                }

                                dr_dtF[count] = ansIndex;//answerExpression;
                                count++;
                                optionsCount--;
                            }
                            else if (questionTypeId == 8 || questionTypeId == 12) // single punch matrix answers
                            {
                                List<OptionsModel> optionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                List<MatrixTypeModel> columnDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstMatrixTypeModel;
                                int qustionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).Select(v => v.DisplayOrder).FirstOrDefault();
                                string[] allAnswers = answerExpression.Split('|');

                                List<string> lstColumnOrdinals = new List<string>();

                                string formattedAnswer = string.Empty;
                                string answerValue = string.Empty;
                                if (allAnswers.Length > 0)
                                {
                                    foreach (var ansExpression in allAnswers)
                                    {
                                        if (!string.IsNullOrEmpty(ansExpression))
                                        {
                                            string displaycol = string.Empty;
                                            string displayopt = string.Empty;
                                            foreach (var item in optionDisplayOrder)
                                            {

                                                if (Convert.ToString(item.OptionId) == ansExpression.Split('~')[0])
                                                {
                                                    displaycol = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            foreach (var item in columnDisplayOrder)
                                            {
                                                if (Convert.ToString(item.Id) == ansExpression.Split('~')[1])
                                                {
                                                    displayopt = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            //  formattedAnswer = "Q" + questionId + "_M" + displaycol + "#" + displayopt;
                                            formattedAnswer = "Q" + questionId + "_M" + displayopt + "#" + displaycol;
                                            foreach (DataColumn column in dtF.Columns)
                                            {
                                                string[] columnHeader = column.ColumnName.Split('-');
                                                string[] answer = columnHeader[0].Split('_');
                                                if (columnHeader.Length > 1)
                                                {
                                                    string answerInHeader = answer[0] + "_" + answer[1];
                                                    string[] formattedAnswerArr = formattedAnswer.Split('#');
                                                    if (formattedAnswerArr[0] == answerInHeader)
                                                    {
                                                        int index = dr_dtF.Table.Columns[column.ColumnName].Ordinal;
                                                        lstColumnOrdinals.Add(index.ToString());
                                                        count++;
                                                        dr_dtF[index] = formattedAnswerArr[1];
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            else if (questionTypeId == 13) // Kano model matrix type
                            {
                                List<OptionsModel> optionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                List<MatrixTypeModel> columnDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstMatrixTypeModel;
                                int qustionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).Select(v => v.DisplayOrder).FirstOrDefault();
                                string[] allAnswers = answerExpression.Split('|');

                                List<string> lstColumnOrdinals = new List<string>();

                                string formattedAnswer = string.Empty;
                                string answerValue = string.Empty;
                                if (allAnswers.Length > 1)
                                {
                                    int repeatId = 0;
                                    foreach (var ansExpression in allAnswers)
                                    {
                                        string displaycol = string.Empty;
                                        string displayopt = string.Empty;
                                        string displayText = string.Empty;
                                        string text = string.Empty;
                                        if (repeatId == Convert.ToInt32(ansExpression.Split('~')[0]))
                                        {
                                            text = "_Absent";
                                            repeatId = 0;
                                        }
                                        else
                                        {
                                            text = "_Present";
                                        }
                                        foreach (var item in optionDisplayOrder)
                                        {
                                            if (Convert.ToString(item.OptionId) == ansExpression.Split('~')[0])
                                            {
                                                displayText = Convert.ToString(item.Option) + text;
                                                displaycol = Convert.ToString(item.DisplayOrder);
                                                repeatId = Convert.ToInt32(ansExpression.Split('~')[0]);
                                                break;
                                            }
                                        }
                                        foreach (var item in columnDisplayOrder)
                                        {
                                            if (Convert.ToString(item.Id) == ansExpression.Split('~')[1])
                                            {
                                                displayopt = Convert.ToString(item.DisplayOrder);
                                                break;
                                            }
                                        }
                                        //formattedAnswer = "Q" + qustionDisplayOrder + "_M" + displaycol + "-" + displayText + "#" + displayopt;
                                        formattedAnswer = "Q" + questionId + "_M" + displaycol + "-" + displayText + "#" + displayopt;
                                        foreach (DataColumn column in dtF.Columns)
                                        {

                                            string[] columnHeader = column.ColumnName.Split('-');
                                            string[] answer = columnHeader[0].Split('_');
                                            if (columnHeader.Length > 1)
                                            {
                                                string answerInHeader = answer[0] + "_" + answer[1] + "-" + columnHeader[1];
                                                string[] formattedAnswerArr = formattedAnswer.Split('#');
                                                if (Regex.Replace(formattedAnswerArr[0], @"\s+", "", RegexOptions.Multiline) == Regex.Replace(answerInHeader, @"\s+", "", RegexOptions.Multiline))
                                                {
                                                    int index = dr_dtF.Table.Columns[column.ColumnName].Ordinal;
                                                    lstColumnOrdinals.Add(index.ToString());
                                                    count++;
                                                    dr_dtF[index] = formattedAnswerArr[1];
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            else if (questionTypeId > 7 && questionTypeId != 15)
                            {
                                List<OptionsModel> optionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                List<MatrixTypeModel> columnDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstMatrixTypeModel;
                                List<DDLMatrixTypeModel> dropDownDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstDDlMatrixModel;
                                //count = count + (optionDisplayOrder.Count * columnDisplayOrder.Count);
                                int optDisplayOrder = 0;
                                if (optionDisplayOrder != null)
                                {
                                    optDisplayOrder = optionDisplayOrder.Count;
                                }
                                int colDisplayOrder = 0;
                                if (columnDisplayOrder != null)
                                {
                                    colDisplayOrder = columnDisplayOrder.Count;
                                }
                                int qustionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).Select(v => v.DisplayOrder).FirstOrDefault();
                                string[] allAnswers = answerExpression.Split('|');

                                List<string> lstColumnOrdinals = new List<string>();

                                string formattedAnswer = string.Empty;
                                string answerValue = string.Empty;
                                int DDlDisplayOrder = 0;
                                if (allAnswers.Length > 1)
                                {
                                    foreach (var ansExpression in allAnswers)
                                    {
                                        string displaycol = string.Empty;
                                        string displayopt = string.Empty;

                                        if (questionTypeId == 9 || questionTypeId == 14)
                                        {
                                            foreach (var item in optionDisplayOrder)
                                            {
                                                if (Convert.ToString(item.OptionId) == ansExpression.Split('~')[0].Split('*')[0])
                                                {
                                                    displaycol = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            foreach (var item in columnDisplayOrder)
                                            {
                                                if (Convert.ToString(item.Id) == ansExpression.Split('~')[1])
                                                {
                                                    displayopt = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            //formattedAnswer = "Q" + qustionDisplayOrder + "_M" + displaycol + "_C" + displayopt;
                                            formattedAnswer = "Q" + questionId + "_M" + displaycol + "_C" + displayopt;
                                            answerValue = ansExpression.Split('~')[0].Split('*')[1];
                                            if (questionTypeId == 14)
                                                DDlDisplayOrder = dropDownDisplayOrder.Where(l => l.Id == Convert.ToInt32(answerValue)).FirstOrDefault().DDLDisplayOrder;
                                        }
                                        else
                                        {
                                            foreach (var item in optionDisplayOrder)
                                            {
                                                if (Convert.ToString(item.OptionId) == ansExpression.Split('~')[0])
                                                {
                                                    displaycol = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            foreach (var item in columnDisplayOrder)
                                            {
                                                if (Convert.ToString(item.Id) == ansExpression.Split('~')[1])
                                                {
                                                    displayopt = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            //formattedAnswer = "Q" + qustionDisplayOrder + "_M" + displaycol + "_C" + displayopt;
                                            formattedAnswer = "Q" + questionId + "_M" + displaycol + "_C" + displayopt;
                                        }
                                        foreach (DataColumn column in dtF.Columns)
                                        {
                                            string[] columnHeader = column.ColumnName.Split('-');

                                            if (columnHeader.Length > 1)
                                            {
                                                string[] answer = columnHeader[0].Split('_');

                                                if (answer.Length > 2)
                                                {
                                                    string answerInHeader = answer[0] + "_" + answer[1] + "_" + answer[2];

                                                    if (formattedAnswer == answerInHeader)
                                                    {
                                                        int index = dr_dtF.Table.Columns[column.ColumnName].Ordinal;
                                                        lstColumnOrdinals.Add(index.ToString());
                                                        //count++;
                                                        if (questionTypeId == 9)
                                                        {
                                                            dr_dtF[index] = answerValue;
                                                        }
                                                        else if (questionTypeId == 14)
                                                        {
                                                            dr_dtF[index] = DDlDisplayOrder;
                                                        }
                                                        else
                                                        {
                                                            dr_dtF[index] = "1";
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }

                                // Fill All the Empty cells with 2

                                for (int i = 0; i <= dtF.Columns.Count - 1; i++)
                                    if (String.IsNullOrEmpty(Convert.ToString(dr_dtF[i])))
                                        dr_dtF[i] = "";
                            }

                            else
                            {
                                dr_dtF[count] = answerExpression;
                                count++;
                                optionsCount--;
                            }



                            if (productId != oldProductID)
                            {
                                dtF.Rows.Add(drTemp);
                                drTemp[0] = response_id;
                                oldProductID = productId;
                            }

                            drTemp = dr_dtF;
                            //---ss
                            if (model.projectModel.ProjectType == "2")
                            {
                                drTemp[2] = product_desc;
                                drTemp[1] = product_logiId;
                            }
                            //---ss


                        }


                        dtF.Rows.Add(dr_dtF);
                        drTemp[0] = response_id;
                        //---ss
                        if (model.projectModel.ProjectType == "2")
                        {
                            dr_dtF[2] = product_desc;
                            dr_dtF[1] = product_logiId;
                        }
                        response_id++;
                        //---ss
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return dtF;
        }
        #endregion        

        // QueryBuilder

        #region GetQueryReportDataTable_Load
        [WebMethod]
        public void GetQueryReportDataTable_Load(int projIdStr = 0, string projectGUID = null)
        {
            try {
                int projId = Convert.ToInt32(projIdStr);
                CreateProjectModel model = new CreateProjectModel();
                DataTable CaseContainer1 = new DataTable();
                CreateProjectModel projectModel = new CreateProjectModel();
                projectModel = GetProjectDetailById(projId);
                string ViewBagType = null;
                if (projectModel.lstProductProjectModel != null)
                {
                    ViewBagType = projectModel.lstProductProjectModel.Count.ToString();
                }

                List<int> currentQueryList = new List<int>();
                int currentQuery = 0;
                int currentQuery1 = 0;
                int check = 0;

                QueryModel queryModel = new QueryModel();
                List<QueryModel> QueryList = new List<QueryModel>();
                queryBase.ProjectId = Convert.ToInt32(projId);
                actionResult = adminAction.Query_LoadAllByProjectId(queryBase);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    foreach (DataRow dr in actionResult.dtResult.Rows)
                    {
                        queryModel = new QueryModel();
                        queryModel.ProjectId = dr["ProjectId"] != DBNull.Value ? Convert.ToInt32(dr["ProjectId"]) : 0;
                        queryModel.CaseName = dr["CaseName"] != DBNull.Value ? Convert.ToString(dr["CaseName"]) : "";
                        queryModel.Query = dr["Query"] != DBNull.Value ? Convert.ToString(dr["Query"]) : "";
                        queryModel.QueryHtml = dr["QueryHtml"] != DBNull.Value ? Convert.ToString(dr["QueryHtml"]) : "";
                        currentQuery = dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0;
                        QueryList.Add(queryModel);
                        currentQuery = dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0;
                        if (currentQuery > 0)
                        {
                            currentQueryList.Add(currentQuery);
                        }
                    }
                }
                ///
                if (projectModel.SuperProjectId > 0)
                {
                    queryBase.SuperProjectId = projectModel.SuperProjectId;
                    //(projectModel.ProjectType == 2)
                    actionResult = adminAction.Query_LoadAllBySuperProjectId(queryBase);
                    if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                    {
                        check = 0;
                        foreach (DataRow dr in actionResult.dtResult.Rows)
                        {
                            queryModel = new QueryModel();
                            queryModel.ProjectId = dr["ProjectId"] != DBNull.Value ? Convert.ToInt32(dr["ProjectId"]) : 0;
                            queryModel.CaseName = dr["CaseName"] != DBNull.Value ? Convert.ToString(dr["CaseName"]) : "";
                            queryModel.Query = dr["Query"] != DBNull.Value ? Convert.ToString(dr["Query"]) : "";
                            queryModel.QueryHtml = dr["QueryHtml"] != DBNull.Value ? Convert.ToString(dr["QueryHtml"]) : "";
                            currentQuery1 = dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0;
                            foreach (var item in currentQueryList)
                            {
                                if (item == currentQuery1)
                                {
                                    check = 1;
                                }
                            }
                            if (check == 0)
                            {
                                QueryList.Add(queryModel);
                            }
                        }
                    }
                }
                ///

                model.QueryList = QueryList;


                string QueryBuilder = string.Empty;

                int ProjectId = projId;
                //    int ProjectId = mainProjectId;
                if (CaseContainer1.Columns.Count == 0)
                {
                    CaseContainer1.Columns.Add("CaseName", typeof(string));
                    CaseContainer1.Columns.Add("Online", typeof(DataTable));
                    CaseContainer1.Columns.Add("Offline", typeof(DataTable));
                    CaseContainer1.Columns.Add("Combined", typeof(DataTable));
                }



                foreach (var item in QueryList)
                {
                    QueryBuilder = item.Query;
                    if (QueryBuilder != null && QueryBuilder != string.Empty)
                    {
                        string[] query = QueryBuilder.TrimEnd('*').Split('*');
                        DataTable dtCase = new DataTable();
                        dtCase.Columns.Add("Id", typeof(int));
                        dtCase.Columns.Add("QuestionId", typeof(int));
                        dtCase.Columns.Add("QuestionTypeId", typeof(int));
                        //dtCase.Columns.Add("OptionIdOrDisplayOrder", typeof(int));
                        //dtCase.Columns.Add("ColumnIdOrDisplayOrder", typeof(int));
                        dtCase.Columns.Add("OptionIdOrDisplayOrder", typeof(string));
                        dtCase.Columns.Add("ColumnIdOrDisplayOrder", typeof(string));
                        dtCase.Columns.Add("Expression", typeof(string));
                        dtCase.Columns.Add("NextQuestionAddWith", typeof(string));


                        if (query.Length > 0)
                        {
                            for (int i = 0; i <= query.Length - 1; i++)
                            {
                                string[] questionArr = query[i].Split('_'); //1602_5$>=11#OR|2~
                                int questionId = Convert.ToInt32(questionArr[0]);
                                string[] exprArr = questionArr[1].Split('$');
                                int questionTypeId = Convert.ToInt32(exprArr[0]);
                                string Expression = exprArr[1].Split('#')[0];
                                string[] optionIdArr = exprArr[1].Split('#')[1].Split('|');
                                string NextQuestionAddWith = optionIdArr[0];
                                //int optionId = optionIdArr[1].Split('~')[0] != "" ? Convert.ToInt32(optionIdArr[1].Split('~')[0]) : 0;
                                //int columnId = optionIdArr[1].Split('~')[1] != "" ? Convert.ToInt32(optionIdArr[1].Split('~')[1]) : 0;
                                string optionId = optionIdArr[1].Split('~')[0] != "" ? Convert.ToString(optionIdArr[1].Split('~')[0]) : "0";
                                string columnId = optionIdArr[1].Split('~')[1] != "" ? Convert.ToString(optionIdArr[1].Split('~')[1]) : "0";
                                DataRow drCase = dtCase.NewRow();
                                drCase["Id"] = (i + 1);
                                drCase["QuestionId"] = questionId;
                                drCase["QuestionTypeId"] = questionTypeId;
                                drCase["OptionIdOrDisplayOrder"] = optionId;
                                drCase["ColumnIdOrDisplayOrder"] = columnId;
                                drCase["Expression"] = Expression;
                                drCase["NextQuestionAddWith"] = NextQuestionAddWith;
                                dtCase.Rows.Add(drCase);
                            }
                        }

                        projectBase.CaseTable = dtCase;
                        List<string> lstEmails = new List<string>();
                        List<string> lstResponders = new List<string>();
                        List<string> offlineResponders = new List<string>();
                        string Email = string.Empty;
                        string ExternalResponders = string.Empty;



                        actionResult = adminAction.Response_CaseSelectionReport(projectBase);

                        if (actionResult.IsSuccess)
                        {
                            if (Convert.ToString(actionResult.dtResult.Rows[0][0]) == "-1")
                            {

                            }
                            else
                            {
                                foreach (DataRow dr in actionResult.dtResult.Rows)
                                {
                                    ProjectId = dr["ProjectId"] != DBNull.Value ? Convert.ToInt32(dr["ProjectId"]) : 0;
                                    Email += dr["Email"] != DBNull.Value ? Convert.ToString(dr["Email"]) + "," : "";
                                    if (dr["Email"] != DBNull.Value && !String.IsNullOrEmpty(dr["Email"].ToString()))
                                        lstEmails.Add(dr["Email"].ToString());
                                    ExternalResponders += dr["ExternalResponderId"] != DBNull.Value ? Convert.ToString(dr["ExternalResponderId"]) + "," : "";
                                    if (dr["ExternalResponderId"] != DBNull.Value && !String.IsNullOrEmpty(dr["ExternalResponderId"].ToString()))
                                        lstResponders.Add(dr["ExternalResponderId"].ToString());

                                }



                            }
                        }

                        DataTable dt;
                        if (projectModel.ProjectType == "1" || projectModel.ProjectType == "2")
                        {
                            dt = GetReportDataTableForProduct(projectGUID, lstEmails.ToArray(), lstResponders.ToArray());
                        }
                        else
                        {
                            dt = GetReportDataTable1(projectGUID, lstEmails.ToArray(), lstResponders.ToArray());
                        }
                        actionResult = adminAction.Response_CaseSelectionReportOffline(projectBase);
                        if (actionResult.IsSuccess)
                        {
                            if (Convert.ToString(actionResult.dtResult.Rows[0][0]) == "-1")
                            {

                            }
                            else
                            {
                                ExternalResponders = "";
                                foreach (DataRow dr in actionResult.dtResult.Rows)
                                {
                                    ProjectId = dr["ProjectId"] != DBNull.Value ? Convert.ToInt32(dr["ProjectId"]) : 0;

                                    ExternalResponders += dr["RespId"] != DBNull.Value ? Convert.ToString(dr["RespId"]) + "," : "";
                                    if (dr["RespId"] != DBNull.Value && !String.IsNullOrEmpty(dr["RespId"].ToString()))
                                        offlineResponders.Add(dr["RespId"].ToString());

                                }
                            }
                        }
                        int RespCount = 0;
                        DataTable dt1;
                        if (projectModel.ProjectType == "1" || projectModel.ProjectType == "2")
                        {
                            dt1 = GetReportDataTableOfflineForProduct(projectGUID, (RespCount + 1), offlineResponders.ToArray());
                        }
                        else
                        {
                            dt1 = GetReportDataTableOffline1(projectGUID, (RespCount + 1), offlineResponders.ToArray());
                        }

                        DataRow CaseRow = CaseContainer1.NewRow();
                        DataTable dt2 = new DataTable();
                        CaseRow[0] = item.CaseName;
                        CaseRow[1] = dt;
                        CaseRow[2] = dt1;
                        dt2 = dt.Copy();
                        dt2.Merge(dt1, true);
                        CaseRow[3] = dt2;
                        CaseContainer1.Rows.Add(CaseRow);

                        string OnlineResponse = string.Empty;
                        if (dt.Columns.Count > 0)
                        {
                            OnlineResponse = ConvertDatatableToXML(dt);
                        }

                        string OfflineResponse = string.Empty;
                        if (dt1.Columns.Count > 0)
                        {
                            OfflineResponse = ConvertDatatableToXML(dt1);
                        }

                        string CombineResponse = string.Empty;
                        if (dt2.Columns.Count > 0)
                        {
                            CombineResponse = ConvertDatatableToXML(dt2);
                        }

                        actionResult = adminAction.ReportQueryBuilderCases_Add(projId, projectGUID, item.CaseName, OnlineResponse, OfflineResponse, CombineResponse);
                    }
                }

                DataTable dtSessionType = null;
                dtSessionType = CaseContainer1;

                int index = 0;
                string ReportDataHtml = "";
                string type = projectModel.ProjectType;
                if (type == "2")
                {
                    foreach (System.Data.DataRow dr in CaseContainer1.Rows)
                    {
                        index++;
                        ReportDataHtml += "<div class='caseRep" + index + "' style='display:none;'>";
                        ReportDataHtml += "<div class='panel-heading " + index + "' style=''>";
                        ReportDataHtml += "<h4 class='panel-tilte'><span data-name='ReportOf'>Report of </span><span style='color: #3498db'> " + dr["CaseName"] + "</span></h4></div>";
                        ReportDataHtml += "<div class='panel-body " + index + "' style='' data-id='" + index + "' data-name='" + dr["CaseName"] + "'>";
                        ReportDataHtml += "<table class='table table-striped table-bordered reportProductTable'><thead><tr>";
                        ReportDataHtml += "<th id='prodDescHeader' class='prodDescHeader' data-name='ProductDesc'>Product Description --></th>";
                        ReportDataHtml += "<th id='AtrAvgVariance' data-name='AverageVariance'>Average Variance</th>";
                        ReportDataHtml += "<th id='AtrAnova' data-name='Anova'>Anova</th>";
                        ReportDataHtml += "<th id='AtrStndDeviation' data-name='StandardDeviation'>Standard Deviation</th>";
                        ReportDataHtml += "<th id='AtrStndError' data-name='StandardError'>Standard Error</th>";
                        ReportDataHtml += "<th id='AtrSignificant99' data-name='SignificantDiff99'>Significant Difference at 99%</th>";
                        ReportDataHtml += "<th id='AtrSignificant95' data-name='SignificantDiff95'>Significant Difference at 95%</th>";
                        ReportDataHtml += "<th id='AtrSignificant90' data-name='SignificantDiff90'>Significant Difference at 90%</th>";
                        ReportDataHtml += "<th id='AtrSignificant80' data-name='SignificantDiff80'>Significant Difference at 80%</th>";
                        ReportDataHtml += "<th id='AtrFRatio' data-name='FRatio'>F-Ratio</th>";
                        ReportDataHtml += "</tr><tr>";
                        ReportDataHtml += "<th data-name='ProductCode' class='prodLogIdHeader'>Product Code --></th>";
                        ReportDataHtml += "<th></th><th></th><th></th><th></th><th></th><th></th><th></th><th></th><th></th></tr>";
                        ReportDataHtml += "<tr><th data-name='JudgementPerProduct  (Base Size)' class='judgementsPerProd'>Judgments Per Product (Base Size) --></th>";
                        ReportDataHtml += "<th></th><th></th><th></th><th></th><th></th><th></th><th></th><th></th><th></th></tr>";
                        ReportDataHtml += "</thead><tbody></tbody></table></div></div>";
                    }
                }
                else if (type == "3")
                {
                    foreach (System.Data.DataRow dr in CaseContainer1.Rows)
                    {
                        index++;
                        ReportDataHtml += "<div class='caseRepKano" + index + "' style='display:none;'>";
                        ReportDataHtml += "<div class='panel-heading " + index + "' style=''>";
                        ReportDataHtml += "<h4 class='panel-tilte'><span data-name='ReportOf'>Report of </span><span style='color: #3498db'> " + dr["CaseName"] + "</span></h4></div>";
                        ReportDataHtml += "<div class='panel-body " + index + "' style='' data-id='" + index + "' data-name='" + dr["CaseName"] + "'>";
                        ReportDataHtml += "<table id='reportKanoTable" + index + "' class='table table-striped table-bordered'><thead><tr>";
                        ReportDataHtml += "<th  data-name='ResponderId' style='display: none;' class='noExport'>Responder Id</th>";
                        ReportDataHtml += "<th  data-name='tabbedQuestions'>Questions</th>";
                        ReportDataHtml += "<th  data-name='RecodeValue' style='display: none;'>Recode value</th>";
                        ReportDataHtml += "<th data-name='Import_Resp' style='display: none;'>Import Responder</th>";
                        ReportDataHtml += "</tr>";
                        ReportDataHtml += "</thead><tbody id='reportKanoTableTBody" + index + "'>";
                        ReportDataHtml += "<tr><td style='display: none;' class='noExport'></td><td></td><td style='display: none;'></td><td style='display: none;'></td></tr></tbody></table></div></div>";
                    }
                }
                string SessionType = ConvertDatatableToXML_QueryBuilder(dtSessionType);
                actionResult = adminAction.ReportQueryBuilder_Add(projId, projectGUID, SessionType, ViewBagType, ReportDataHtml);
                //   return ReportDataHtml;
            }
            catch (Exception ex)
            {

            }
        }
        #endregion

        #region GetProjectDetailById
        public CreateProjectModel GetProjectDetailById(int? id = 0)
        {
            CreateProjectModel model = new CreateProjectModel();
            List<ProductProjectModel> lstproductModel = new List<ProductProjectModel>();
            try
            {
                projectBase.Id = Convert.ToInt32(id);
                actionResult = adminAction.Project_LoadById(projectBase);
                if (actionResult.IsSuccess)
                {
                    DataRow dr = actionResult.dtResult.Rows[0];
                    model.Id = dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0;
                    model.SuperProjectId = dr["SuperProjectId"] != DBNull.Value ? Convert.ToInt32(dr["SuperProjectId"]) : 0;
                    model.ProjectName = dr["ProjectName"] != DBNull.Value ? Convert.ToString(dr["ProjectName"]) : "";
                    model.WelcomeMessage = dr["WelcomeMessage"] != DBNull.Value && !String.IsNullOrEmpty(dr["WelcomeMessage"].ToString()) ? Convert.ToString(dr["WelcomeMessage"]) : Email.ReadFile("Welcome.txt");
                    model.PageHeader = dr["PageHeader"] != DBNull.Value && !String.IsNullOrEmpty(dr["PageHeader"].ToString()) ? Convert.ToString(dr["PageHeader"]) : Email.ReadFile("Header.txt");
                    model.PageFooter = dr["PageFooter"] != DBNull.Value && !String.IsNullOrEmpty(dr["PageFooter"].ToString()) ? Convert.ToString(dr["PageFooter"]) : Email.ReadFile("Footer.txt");
                    model.ProjectCompletionMesssage = dr["ProjectCompletionMesssage"] != DBNull.Value && !String.IsNullOrEmpty(dr["ProjectCompletionMesssage"].ToString()) ? Convert.ToString(dr["ProjectCompletionMesssage"]) : Email.ReadFile("Complete.txt");
                    model.RedirectUrl = dr["RedirectUrl"] != DBNull.Value ? Convert.ToString(dr["RedirectUrl"]) : "";
                    model.InvitationMessage = dr["InvitationMessage"] != DBNull.Value ? Convert.ToString(dr["InvitationMessage"]) : "";
                    model.OwnerId = dr["OwnerId"] != DBNull.Value ? Convert.ToInt32(dr["OwnerId"]) : 0;
                    model.CreatedBy = dr["CreatedBy"] != DBNull.Value ? Convert.ToInt32(dr["CreatedBy"]) : 0;
                    model.ProjectQuota = dr["ProjectQuota"] != DBNull.Value ? Convert.ToInt32(dr["ProjectQuota"]) : 0;
                    model.TerminationMessageUpper = dr["ProjectTerminationMessage"] != DBNull.Value && !String.IsNullOrEmpty(dr["ProjectTerminationMessage"].ToString()) ? Convert.ToString(dr["ProjectTerminationMessage"]) : Email.ReadFile("TerminateUpper.txt");
                    model.ProjectGuid = dr["ProjectGuid"] != DBNull.Value ? Convert.ToString(dr["ProjectGuid"]) : "";
                    model.IsIncludePageHeader = dr["IsIncludePageHeader"] != DBNull.Value ? Convert.ToBoolean(dr["IsIncludePageHeader"]) : false;
                    model.IsIncludePageFooter = dr["IsIncludePageFooter"] != DBNull.Value ? Convert.ToBoolean(dr["IsIncludePageFooter"]) : false;
                    model.IsIncludeProjectCompletionMesssage = dr["IsIncludeProjectCompletionMesssage"] != DBNull.Value ? Convert.ToBoolean(dr["IsIncludeProjectCompletionMesssage"]) : false;
                    model.IsIncludeTerminationMessage = dr["IsIncludeTerminationMessage"] != DBNull.Value ? Convert.ToBoolean(dr["IsIncludeTerminationMessage"]) : false;
                    model.IsIncludeWelcomeMessage = dr["IsIncludeWelcomeMessage"] != DBNull.Value ? Convert.ToBoolean(dr["IsIncludeWelcomeMessage"]) : false;
                    model.ProjectType = dr["ProjectType"] != DBNull.Value ? Convert.ToString(dr["ProjectType"]) : "";
                    //model.ProductMessage = dr["ProductMessage"] != DBNull.Value ? Convert.ToString(dr["ProductMessage"]) : "";
                }

                productProjectBase.ProjectId = Convert.ToInt32(id);
                actionResult = adminAction.ProductProject_LoadById(productProjectBase);
                if (actionResult.IsSuccess)
                {
                    foreach (DataRow dr in actionResult.dtResult.Rows)
                    {
                        lstproductModel.Add(new ProductProjectModel
                        {
                            Id = dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0,
                            ProjectId = dr["ProjectId"] != DBNull.Value ? Convert.ToInt32(dr["ProjectId"]) : 0,
                            ProductLogicalId = dr["ProductLogicalId"] != DBNull.Value ? Convert.ToInt32(dr["ProductLogicalId"]) : 0,
                            Description = dr["Description"] != DBNull.Value ? Convert.ToString(dr["Description"]) : string.Empty,
                            DisplayOrder = dr["DisplayOrder"] != DBNull.Value ? Convert.ToInt32(dr["DisplayOrder"]) : 0
                        });
                    }
                    lstproductModel = lstproductModel.OrderBy(l => l.DisplayOrder).ToList();
                    model.lstProductProjectModel = lstproductModel;
                    //  ViewBag.ProductCount = lstproductModel.Count;
                }

            }
            catch (Exception ex)
            {
                //ErrorReporting.WebApplicationError(ex);
            }
            return model;
        }
        #endregion

        #region GetReportDataTableForProduct
        private DataTable GetReportDataTableForProduct(string Id, string[] Emails, string[] ExternalResponders)
        {

            RespondentController respondent = new RespondentController();
            SurveyResponseModel model = new SurveyResponseModel();
            DataTable dtF = new DataTable();
            try
            {
                DataTable dt = new DataTable();

                string projectGuid = string.Empty;
                projectGuid = Id;
                dt.Columns.Add("Resp_Id", typeof(string));

                model = respondent.GetProjectPreview(projectGuid);

                //--ss
                if (model.projectModel.ProjectType == "2")
                {
                    DataRow drnew = dt.NewRow();
                    drnew[0] = "Product_LogicalId";
                    dt.Rows.Add(drnew);
                    DataRow drnew1 = dt.NewRow();
                    drnew1[0] = "Product_Name";
                    dt.Rows.Add(drnew1);
                }
                //--

                // Load all the questions of this project
                List<QuestionList> questionsList = new List<QuestionList>();
                questionBase.ProjectId = Convert.ToInt32(model.projectModel.Id);
                actionResult = adminAction.QuestionsLoadAll_ByProjectId(questionBase);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    foreach (DataRow dr in actionResult.dtResult.Rows)
                    {
                        questionsList.Add(new QuestionList
                        {
                            id = dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0,
                            questionTypeId = dr["QuestionTypeId"] != DBNull.Value ? Convert.ToInt32(dr["QuestionTypeId"]) : 0
                        });
                    }
                }

                string fileName = Guid.NewGuid().ToString().Substring(0, 5) + "_" + DateTime.Now.ToString("MM-dd-yyyy") + "_" + model.projectModel.ProjectName.Replace(" ", "");

                if (model.lstQuestionModel != null && model.lstQuestionModel.Count > 0)
                {
                    foreach (var item in model.lstQuestionModel)
                    {
                        if (item.PipedFrom > 0 && (!item.QuestionTitle.Contains("{{Q}}")))
                        {
                            if (item.QuestionTypeId == 2 || item.QuestionTypeId == 4 || item.QuestionTypeId == 5 || item.QuestionTypeId == 6)//Multi Punch || Constant Sum || Ranking || Matrix Type
                            {
                                if (model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel != null && model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel.Count > 0)
                                {
                                    foreach (var opt in model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel)
                                    {
                                        DataRow drnew = dt.NewRow();
                                        drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                        dt.Rows.Add(drnew);
                                    }
                                }
                            }

                            else if (item.QuestionTypeId == 1 || item.QuestionTypeId == 3 || item.QuestionTypeId == 7 || item.QuestionTypeId == 15)//Single Punch || Range || Simple Text
                            {
                                DataRow drnew = dt.NewRow();
                                drnew[0] = "Q" + item.QuestionId + "_S" + "-" + ReplaceHtml.ReplaceText(item.QuestionTitle);
                                dt.Rows.Add(drnew);
                            }
                            else if (item.QuestionTypeId == 8 || item.QuestionTypeId == 12)//Single punch matrix type  && Rank order type
                            {
                                //if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
                                //{
                                //foreach (var opt in item.lstOptionsModel)
                                //{
                                //    DataRow drnew = dt.NewRow();
                                //    //drnew[0] = "Q" + item.QuestionId + "_M" + opt.OptionId + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                //    drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                //    dt.Rows.Add(drnew);
                                //}
                                //}
                                if (item.lstMatrixTypeModel != null && item.lstMatrixTypeModel.Count > 0)
                                {
                                    foreach (var col in item.lstMatrixTypeModel)
                                    {
                                        DataRow drnew = dt.NewRow();
                                        //drnew[0] = "Q" + item.QuestionId + "_M" + opt.OptionId + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                        drnew[0] = "Q" + item.QuestionId + "_M" + col.DisplayOrder + "-" + ReplaceHtml.ReplaceText(col.ColumnName);
                                        dt.Rows.Add(drnew);
                                    }
                                }
                            }
                            else if (item.QuestionTypeId == 13)//Kano model matrix type 
                            {
                                if (model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel != null && model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel.Count > 0)
                                {
                                    foreach (var opt in model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel)
                                    {
                                        DataRow drnew = dt.NewRow();
                                        drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Present";
                                        dt.Rows.Add(drnew);
                                        DataRow drnew1 = dt.NewRow();
                                        drnew1[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Absent";
                                        dt.Rows.Add(drnew1);
                                    }
                                }
                            }
                            // Single Punch Matrix Type || Range Matrix Type Question ||
                            // Single punch type answer horizontal position || 
                            // Multiple punch type answer horizontal position || 
                            // Rank order type question || 
                            // KANO Model Matrix type question || Dropdown Matrix type question
                            else if (item.QuestionTypeId > 7 && item.QuestionTypeId != 15)
                            {
                                if (model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel != null && model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel.Count > 0)
                                {
                                    foreach (var opt in model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel)
                                    {
                                        foreach (var column in item.lstMatrixTypeModel)
                                        {
                                            DataRow drnew = dt.NewRow();
                                            drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "_C" + column.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + " " + ReplaceHtml.ReplaceText(column.ColumnName);
                                            dt.Rows.Add(drnew);
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (item.QuestionTypeId == 2 || item.QuestionTypeId == 4 || item.QuestionTypeId == 5 || item.QuestionTypeId == 6)//Multi Punch || Constant Sum || Ranking || Matrix Type
                            {
                                if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
                                {
                                    foreach (var opt in item.lstOptionsModel)
                                    {
                                        DataRow drnew = dt.NewRow();
                                        //drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                        drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                        dt.Rows.Add(drnew);
                                    }
                                }
                            }

                            else if (item.QuestionTypeId == 1 || item.QuestionTypeId == 3 || item.QuestionTypeId == 7 || item.QuestionTypeId == 15)//Single Punch || Range || Simple Text
                            {
                                DataRow drnew = dt.NewRow();
                                //drnew[0] = "Q" + item.DisplayOrder + "_S" + "-" + ReplaceHtml.ReplaceText(item.QuestionTitle);
                                drnew[0] = "Q" + item.QuestionId + "_S" + "-" + ReplaceHtml.ReplaceText(item.QuestionTitle);
                                dt.Rows.Add(drnew);
                            }
                            else if (item.QuestionTypeId == 8 || item.QuestionTypeId == 12)//Single punch matrix type  && Rank order type
                            {
                                //if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
                                //{
                                //foreach (var opt in item.lstOptionsModel)
                                //{
                                //    DataRow drnew = dt.NewRow();
                                //    //drnew[0] = "Q" + item.QuestionId + "_M" + opt.OptionId + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                //    drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                //    dt.Rows.Add(drnew);
                                //}
                                //}
                                if (item.lstMatrixTypeModel != null && item.lstMatrixTypeModel.Count > 0)
                                {
                                    foreach (var col in item.lstMatrixTypeModel)
                                    {
                                        DataRow drnew = dt.NewRow();
                                        //drnew[0] = "Q" + item.QuestionId + "_M" + opt.OptionId + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                        drnew[0] = "Q" + item.QuestionId + "_M" + col.DisplayOrder + "-" + ReplaceHtml.ReplaceText(col.ColumnName);
                                        dt.Rows.Add(drnew);
                                    }
                                }
                            }
                            else if (item.QuestionTypeId == 13)//Kano model matrix type 
                            {
                                if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
                                {
                                    foreach (var opt in item.lstOptionsModel)
                                    {
                                        DataRow drnew = dt.NewRow();
                                        //drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Present";
                                        drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Present";
                                        dt.Rows.Add(drnew);
                                        DataRow drnew1 = dt.NewRow();
                                        //drnew1[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Absent";
                                        drnew1[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Absent";
                                        dt.Rows.Add(drnew1);
                                    }
                                }
                            }
                            // Single Punch Matrix Type || Range Matrix Type Question ||
                            // Single punch type answer horizontal position || 
                            // Multiple punch type answer horizontal position || 
                            // Rank order type question || 
                            // KANO Model Matrix type question || Dropdown Matrix type question
                            else if (item.QuestionTypeId > 7)
                            {
                                if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
                                {
                                    foreach (var opt in item.lstOptionsModel)
                                    {
                                        foreach (var column in item.lstMatrixTypeModel)
                                        {
                                            DataRow drnew = dt.NewRow();
                                            //drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "_C" + column.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + " " + ReplaceHtml.ReplaceText(column.ColumnName);
                                            drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "_C" + column.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + " " + ReplaceHtml.ReplaceText(column.ColumnName);
                                            dt.Rows.Add(drnew);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                // Get the Excel Pattern of dt
                dtF = comMethods.GenerateTransposedTable(dt);

                // Get all the reponders
                projectBase.Id = model.projectModel.Id;
                actionResult = adminAction.CompleteRespondersLoad_ByProjectId(projectBase);
                List<RespondersModel> lstResponders = new List<RespondersModel>();
                foreach (DataRow dr in actionResult.dtResult.Rows)
                {
                    lstResponders.Add(new RespondersModel
                    {

                        InvitationGuid = dr["InvitationGuid"] != DBNull.Value ? Convert.ToString(dr["InvitationGuid"]) : "",
                        Email = dr["Email"] != DBNull.Value ? Convert.ToString(dr["Email"]) : "",

                    });
                }
                IEnumerable<RespondersModel> filteredList = lstResponders
                 .GroupBy(customer => customer.InvitationGuid).Select(group => group.First());
                // Get Responses
                SurveyLoadBase surveyLoadBase = new SurveyLoadBase();
                string json = string.Empty;
                SRV.BaseLayer.ActionResult actionResultResponses = new SRV.BaseLayer.ActionResult();
                int response_id = 1;            //ss
                foreach (var responder in filteredList)
                {
                    DataTable responsesDt = new DataTable();
                    surveyLoadBase.Id = model.projectModel.Id;
                    surveyLoadBase.Email = (!string.IsNullOrEmpty(responder.Email) ? Convert.ToString(responder.Email) : "");
                    surveyLoadBase.ExternalResponderId = (!string.IsNullOrEmpty(responder.InvitationGuid) ? Convert.ToString(responder.InvitationGuid) : "");

                    if (Array.IndexOf(Emails, surveyLoadBase.Email) > -1 || Array.IndexOf(ExternalResponders, surveyLoadBase.ExternalResponderId) > -1)
                    {
                        if (model.projectModel.lstProductProjectModel != null &&
                            model.projectModel.lstProductProjectModel.Count > 0)
                        {
                            foreach (var productModel in model.projectModel.lstProductProjectModel)
                            {
                                surveyLoadBase.ProductId = productModel.Id;
                                actionResultResponses = respondentAction.Response_LoadByProjectId(surveyLoadBase);
                                if (actionResultResponses.IsSuccess)
                                {
                                    if (responsesDt.Rows.Count == 0)
                                        responsesDt = actionResultResponses.dtResult.Copy();
                                    else
                                    {
                                        foreach (DataRow drThisRespRow in actionResultResponses.dtResult.Rows)
                                            responsesDt.ImportRow(drThisRespRow);
                                    }
                                }
                            }
                        }
                        else
                        {
                            actionResultResponses = respondentAction.Response_LoadByProjectId(surveyLoadBase);
                            if (actionResultResponses.IsSuccess)
                                responsesDt = actionResultResponses.dtResult.Copy();
                        }
                    }
                    // surveyLoadBase.OfflineResponderId = dr["RespId"] != DBNull.Value ? Convert.ToString(dr["RespId"]) : "";
                    //DataRow dr_dtF = dtF.NewRow();
                    // if (String.IsNullOrEmpty(surveyLoadBase.Email)) continue;

                    //actionResultResponses = respondentAction.Response_LoadByProjectId(surveyLoadBase);
                    if (responsesDt.Rows.Count > 0)//actionResultResponses.IsSuccess
                    {
                        //dr_dtF[0] = surveyLoadBase.Email;

                        int count = 1;
                        count = (model.projectModel.ProjectType == "2") ? count + 2 : count;        //ss
                        DataRow dr_dtF = dtF.NewRow();
                        DataRow drTemp = dtF.NewRow();
                        string product_desc = string.Empty;
                        string product_logiId = string.Empty;
                        int oldProductID = responsesDt.Rows[0]["ProductId"] != DBNull.Value ? Convert.ToInt32(responsesDt.Rows[0]["ProductId"]) : 0;
                        foreach (DataRow drResponse in responsesDt.Rows)// actionResultResponses.dtResult.Rows
                        {

                            int optionsCount = 0;

                            int productId = drResponse["ProductId"] != DBNull.Value ? Convert.ToInt32(drResponse["ProductId"]) : 0;
                            if (productId != oldProductID)
                            {
                                count = (model.projectModel.ProjectType == "2") ? 3 : 1;
                                dr_dtF = dtF.NewRow();
                            }

                            //--ss

                            product_desc = drResponse["ProductDescription"] != DBNull.Value ? Convert.ToString(drResponse["ProductDescription"]) : "";
                            product_logiId = drResponse["ProductLogicalId"] != DBNull.Value ? Convert.ToString(drResponse["ProductLogicalId"]) : "";

                            //--ss
                            int questionId = drResponse["QuestionId"] != DBNull.Value ? Convert.ToInt32(drResponse["QuestionId"]) : 0;
                            string answerExpression = drResponse["AnswerExpression"] != DBNull.Value ? Convert.ToString(drResponse["AnswerExpression"]) : "";
                            int questionTypeId = drResponse["QuestionTypeId"] != DBNull.Value ? Convert.ToInt32(drResponse["QuestionTypeId"]) : 1;
                            int PipedFrom = drResponse["PipedFrom"] != DBNull.Value ? Convert.ToInt32(drResponse["PipedFrom"]) : 0;
                            //dr_dtF[0] = drResponse;

                            // Get Total options count for this question
                            questionBase.Id = Convert.ToInt32(questionId);
                            SRV.BaseLayer.ActionResult actionResultAnswersTemp = new SRV.BaseLayer.ActionResult();
                            actionResultAnswersTemp = adminAction.Option_LoadAllById(questionBase);
                            if (actionResultAnswersTemp.dtResult != null && actionResultAnswersTemp.dtResult.Rows.Count > 0)
                            {
                                optionsCount = actionResultAnswersTemp.dtResult.Rows.Count;
                            }

                            if (questionTypeId == 2 || questionTypeId == 4 || questionTypeId == 5 || questionTypeId == 6)
                            {
                                string[] allAnswers = answerExpression.Split('|');
                                if (allAnswers.Length > 0)
                                {
                                    if (questionTypeId == 2)// Multi Punch
                                    {
                                        string allRec = string.Empty;
                                        //List<OptionsModel> matrixAns = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                        List<OptionsModel> matrixAns = new List<OptionsModel>();
                                        if (PipedFrom > 0 && !drResponse["QuestionTitle"].ToString().Contains("{{Q}}"))
                                        {
                                            matrixAns = model.lstQuestionModel.Where(v => v.QuestionId == PipedFrom).FirstOrDefault().lstOptionsModel;
                                        }
                                        else
                                        {
                                            matrixAns = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                        }

                                        foreach (var item in matrixAns)
                                        {
                                            string matchedId = string.Empty;
                                            foreach (string ans in allAnswers)
                                            {
                                                if (Convert.ToString(item.OptionId) == ans)
                                                {
                                                    matchedId = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            allRec += matchedId + "|";
                                        }

                                        string[] allAnswersMat = allRec.Substring(0, allRec.Length - 1).Split('|');
                                        foreach (string ans in allAnswersMat)
                                        {

                                            if (count <= dtF.Columns.Count - 1)
                                            {
                                                if ((Regex.Replace(dtF.Columns[count].ToString().Split('-')[0].Split('_')[1], @"[^\d]", String.Empty)) == ans)
                                                {
                                                    dr_dtF[count] = "1";// ans;
                                                    count++;
                                                    optionsCount--;
                                                }
                                                else
                                                {
                                                    dr_dtF[count] = "2";
                                                    count++;
                                                    optionsCount--;
                                                }
                                            }
                                        }
                                        //count = count + optionsCount;
                                    }
                                    else if (questionTypeId == 6) // matrix Type
                                    {
                                        foreach (string ans in allAnswers)
                                        {
                                            dr_dtF[count] = (ans.ToLower().IndexOf("yes") != -1) ? "1" : "2";
                                            count++;
                                            optionsCount--;
                                        }
                                        count = count + optionsCount;
                                    }
                                    else
                                    {
                                        foreach (string ans in allAnswers)
                                        {
                                            dr_dtF[count] = ans;
                                            count++;
                                            optionsCount--;
                                        }
                                        count = count + optionsCount;
                                    }
                                }
                            }
                            else if (questionTypeId == 1) // Single Punch Answers
                            {
                                //List<OptionsModel> SinglePunchAns = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                List<OptionsModel> SinglePunchAns = new List<OptionsModel>();
                                if (PipedFrom > 0 && !drResponse["QuestionTitle"].ToString().Contains("{{Q}}"))
                                {
                                    SinglePunchAns = model.lstQuestionModel.Where(v => v.QuestionId == PipedFrom).FirstOrDefault().lstOptionsModel;
                                }
                                else
                                {
                                    SinglePunchAns = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                }
                                int ansIndex = 0;
                                foreach (var item in SinglePunchAns)
                                {
                                    ansIndex++;
                                    if (Convert.ToString(item.OptionId) == answerExpression)
                                    {
                                        break;
                                    }
                                }

                                dr_dtF[count] = ansIndex;//answerExpression;
                                count++;
                                optionsCount--;
                            }
                            else if (questionTypeId == 8 || questionTypeId == 12) // single punch matrix answers
                            {
                                List<OptionsModel> optionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                List<MatrixTypeModel> columnDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstMatrixTypeModel;
                                int qustionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).Select(v => v.DisplayOrder).FirstOrDefault();
                                string[] allAnswers = answerExpression.Split('|');

                                List<string> lstColumnOrdinals = new List<string>();

                                string formattedAnswer = string.Empty;
                                string answerValue = string.Empty;
                                if (allAnswers.Length > 0)
                                {
                                    foreach (var ansExpression in allAnswers)
                                    {
                                        string displaycol = string.Empty;
                                        string displayopt = string.Empty;
                                        foreach (var item in optionDisplayOrder)
                                        {

                                            if (Convert.ToString(item.OptionId) == ansExpression.Split('~')[0])
                                            {
                                                displaycol = Convert.ToString(item.DisplayOrder);
                                                break;
                                            }
                                        }
                                        foreach (var item in columnDisplayOrder)
                                        {
                                            if (Convert.ToString(item.Id) == ansExpression.Split('~')[1])
                                            {
                                                displayopt = Convert.ToString(item.DisplayOrder);
                                                break;
                                            }
                                        }
                                        //  formattedAnswer = "Q" + questionId + "_M" + displaycol + "#" + displayopt;
                                        formattedAnswer = "Q" + questionId + "_M" + displayopt + "#" + displaycol;
                                        foreach (DataColumn column in dtF.Columns)
                                        {
                                            string[] columnHeader = column.ColumnName.Split('-');
                                            string[] answer = columnHeader[0].Split('_');
                                            if (columnHeader.Length > 1)
                                            {
                                                string answerInHeader = answer[0] + "_" + answer[1];
                                                string[] formattedAnswerArr = formattedAnswer.Split('#');
                                                if (formattedAnswerArr[0] == answerInHeader)
                                                {
                                                    int index = dr_dtF.Table.Columns[column.ColumnName].Ordinal;
                                                    lstColumnOrdinals.Add(index.ToString());
                                                    count++;
                                                    dr_dtF[index] = formattedAnswerArr[1];
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            else if (questionTypeId == 13) // Kano model matrix type
                            {
                                List<OptionsModel> optionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                List<MatrixTypeModel> columnDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstMatrixTypeModel;
                                int qustionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).Select(v => v.DisplayOrder).FirstOrDefault();
                                string[] allAnswers = answerExpression.Split('|');

                                List<string> lstColumnOrdinals = new List<string>();

                                string formattedAnswer = string.Empty;
                                string answerValue = string.Empty;
                                if (allAnswers.Length > 1)
                                {
                                    int repeatId = 0;
                                    foreach (var ansExpression in allAnswers)
                                    {
                                        string displaycol = string.Empty;
                                        string displayopt = string.Empty;
                                        string displayText = string.Empty;
                                        string text = string.Empty;
                                        if (repeatId == Convert.ToInt32(ansExpression.Split('~')[0]))
                                        {
                                            text = "_Absent";
                                            repeatId = 0;
                                        }
                                        else
                                        {
                                            text = "_Present";
                                        }
                                        foreach (var item in optionDisplayOrder)
                                        {
                                            if (Convert.ToString(item.OptionId) == ansExpression.Split('~')[0])
                                            {
                                                displayText = Convert.ToString(item.Option) + text;
                                                displaycol = Convert.ToString(item.DisplayOrder);
                                                repeatId = Convert.ToInt32(ansExpression.Split('~')[0]);
                                                break;
                                            }
                                        }
                                        foreach (var item in columnDisplayOrder)
                                        {
                                            if (Convert.ToString(item.Id) == ansExpression.Split('~')[1])
                                            {
                                                displayopt = Convert.ToString(item.DisplayOrder);
                                                break;
                                            }
                                        }
                                        //formattedAnswer = "Q" + qustionDisplayOrder + "_M" + displaycol + "-" + displayText + "#" + displayopt;
                                        formattedAnswer = "Q" + questionId + "_M" + displaycol + "-" + displayText + "#" + displayopt;
                                        foreach (DataColumn column in dtF.Columns)
                                        {

                                            string[] columnHeader = column.ColumnName.Split('-');
                                            string[] answer = columnHeader[0].Split('_');
                                            if (columnHeader.Length > 1)
                                            {
                                                string answerInHeader = answer[0] + "_" + answer[1] + "-" + columnHeader[1];
                                                string[] formattedAnswerArr = formattedAnswer.Split('#');
                                                if (Regex.Replace(formattedAnswerArr[0], @"\s+", "", RegexOptions.Multiline) == Regex.Replace(answerInHeader, @"\s+", "", RegexOptions.Multiline))
                                                {
                                                    int index = dr_dtF.Table.Columns[column.ColumnName].Ordinal;
                                                    lstColumnOrdinals.Add(index.ToString());
                                                    count++;
                                                    dr_dtF[index] = formattedAnswerArr[1];
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            else if (questionTypeId > 7 && questionTypeId != 15)
                            {
                                List<OptionsModel> optionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                List<MatrixTypeModel> columnDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstMatrixTypeModel;
                                List<DDLMatrixTypeModel> dropDownDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstDDlMatrixModel;
                                // count = count + (optionDisplayOrder.Count * columnDisplayOrder.Count);
                                int optDisplayOrder = 0;
                                if (optionDisplayOrder != null)
                                {
                                    optDisplayOrder = optionDisplayOrder.Count;
                                }
                                int colDisplayOrder = 0;
                                if (columnDisplayOrder != null)
                                {
                                    colDisplayOrder = columnDisplayOrder.Count;
                                }
                                int qustionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).Select(v => v.DisplayOrder).FirstOrDefault();
                                string[] allAnswers = answerExpression.Split('|');

                                List<string> lstColumnOrdinals = new List<string>();

                                string formattedAnswer = string.Empty;
                                string answerValue = string.Empty;
                                int DDlDisplayOrder = 0;
                                if (allAnswers.Length > 1)
                                {
                                    foreach (var ansExpression in allAnswers)
                                    {
                                        string displaycol = string.Empty;
                                        string displayopt = string.Empty;

                                        if (questionTypeId == 9 || questionTypeId == 14)
                                        {
                                            foreach (var item in optionDisplayOrder)
                                            {
                                                if (Convert.ToString(item.OptionId) == ansExpression.Split('~')[0].Split('*')[0])
                                                {
                                                    displaycol = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            foreach (var item in columnDisplayOrder)
                                            {
                                                if (Convert.ToString(item.Id) == ansExpression.Split('~')[1])
                                                {
                                                    displayopt = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            //formattedAnswer = "Q" + qustionDisplayOrder + "_M" + displaycol + "_C" + displayopt;
                                            formattedAnswer = "Q" + questionId + "_M" + displaycol + "_C" + displayopt;
                                            answerValue = ansExpression.Split('~')[0].Split('*')[1];
                                            if (questionTypeId == 14)
                                                DDlDisplayOrder = dropDownDisplayOrder.Where(l => l.Id == Convert.ToInt32(answerValue)).FirstOrDefault().DDLDisplayOrder;
                                        }
                                        else
                                        {
                                            foreach (var item in optionDisplayOrder)
                                            {
                                                if (Convert.ToString(item.OptionId) == ansExpression.Split('~')[0])
                                                {
                                                    displaycol = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            foreach (var item in columnDisplayOrder)
                                            {
                                                if (Convert.ToString(item.Id) == ansExpression.Split('~')[1])
                                                {
                                                    displayopt = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            //formattedAnswer = "Q" + qustionDisplayOrder + "_M" + displaycol + "_C" + displayopt;
                                            formattedAnswer = "Q" + questionId + "_M" + displaycol + "_C" + displayopt;
                                        }
                                        foreach (DataColumn column in dtF.Columns)
                                        {
                                            string[] columnHeader = column.ColumnName.Split('-');

                                            if (columnHeader.Length > 1)
                                            {
                                                string[] answer = columnHeader[0].Split('_');

                                                if (answer.Length > 2)
                                                {
                                                    string answerInHeader = answer[0] + "_" + answer[1] + "_" + answer[2];

                                                    if (formattedAnswer == answerInHeader)
                                                    {
                                                        int index = dr_dtF.Table.Columns[column.ColumnName].Ordinal;
                                                        lstColumnOrdinals.Add(index.ToString());
                                                        //count++;
                                                        if (questionTypeId == 9)
                                                        {
                                                            dr_dtF[index] = answerValue;
                                                        }
                                                        else if (questionTypeId == 14)
                                                        {
                                                            dr_dtF[index] = DDlDisplayOrder;
                                                        }
                                                        else
                                                        {
                                                            dr_dtF[index] = "1";
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }

                                // Fill All the Empty cells with 2

                                for (int i = 0; i <= dtF.Columns.Count - 1; i++)
                                    if (String.IsNullOrEmpty(Convert.ToString(dr_dtF[i])))
                                        dr_dtF[i] = "";
                            }

                            else
                            {
                                dr_dtF[count] = answerExpression;
                                count++;
                                optionsCount--;
                            }

                            if (productId != oldProductID)
                            {
                                dtF.Rows.Add(drTemp);
                                //  drTemp[0] = dtF.Rows.IndexOf(drTemp) + 1;
                                drTemp[0] = response_id;
                                oldProductID = productId;
                            }

                            drTemp = dr_dtF;
                            //---ss
                            if (model.projectModel.ProjectType == "2")
                            {
                                drTemp[2] = product_desc;
                                drTemp[1] = product_logiId;
                            }
                            //---ss

                        }

                        dtF.Rows.Add(dr_dtF);
                        //dr_dtF[0] = dtF.Rows.IndexOf(dr_dtF) + 1;
                        drTemp[0] = response_id;
                        //---ss
                        if (model.projectModel.ProjectType == "2")
                        {
                            dr_dtF[2] = product_desc;
                            dr_dtF[1] = product_logiId;
                        }
                        response_id++;
                        //---ss
                    }
                }
            }
            catch (Exception ex)
            {
                //ErrorReporting.WebApplicationError(ex);
            }
            return dtF;
        }
        #endregion

        #region GetReportDataTable1
        private DataTable GetReportDataTable1(string Id, string[] Emails, string[] ExternalResponders)
        {

            RespondentController respondent = new RespondentController();
            SurveyResponseModel model = new SurveyResponseModel();
            DataTable dtF = new DataTable();
            try
            {
                DataTable dt = new DataTable();

                string projectGuid = string.Empty;
                projectGuid = Id;
                dt.Columns.Add("Resp_Id", typeof(string));

                DataRow drnewRow = dt.NewRow();
                drnewRow[0] = "Import_Resp";
                dt.Rows.Add(drnewRow);

                model = respondent.GetProjectPreview(projectGuid);

                //--ss
                if (model.projectModel.ProjectType == "2")
                {
                    DataRow drnew = dt.NewRow();
                    drnew[0] = "Product_LogicalId";
                    dt.Rows.Add(drnew);
                    DataRow drnew1 = dt.NewRow();
                    drnew1[0] = "Product_Name";
                    dt.Rows.Add(drnew1);
                }
                //--

                // Load all the questions of this project
                List<QuestionList> questionsList = new List<QuestionList>();
                questionBase.ProjectId = Convert.ToInt32(model.projectModel.Id);
                actionResult = adminAction.QuestionsLoadAll_ByProjectId(questionBase);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    foreach (DataRow dr in actionResult.dtResult.Rows)
                    {
                        questionsList.Add(new QuestionList
                        {
                            id = dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0,
                            questionTypeId = dr["QuestionTypeId"] != DBNull.Value ? Convert.ToInt32(dr["QuestionTypeId"]) : 0
                        });
                    }
                }

                string fileName = Guid.NewGuid().ToString().Substring(0, 5) + "_" + DateTime.Now.ToString("MM-dd-yyyy") + "_" + model.projectModel.ProjectName.Replace(" ", "");

                if (model.lstQuestionModel != null && model.lstQuestionModel.Count > 0)
                {
                    foreach (var item in model.lstQuestionModel)
                    {
                        if (item.PipedFrom > 0 && (!item.QuestionTitle.Contains("{{Q}}")))
                        {
                            if (item.QuestionTypeId == 2 || item.QuestionTypeId == 4 || item.QuestionTypeId == 5 || item.QuestionTypeId == 6)//Multi Punch || Constant Sum || Ranking || Matrix Type
                            {
                                if (model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel != null && model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel.Count > 0)
                                {
                                    foreach (var opt in model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel)
                                    {
                                        DataRow drnew = dt.NewRow();
                                        drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                        dt.Rows.Add(drnew);
                                    }
                                }
                            }

                            else if (item.QuestionTypeId == 1 || item.QuestionTypeId == 3 || item.QuestionTypeId == 7 || item.QuestionTypeId == 15)//Single Punch || Range || Simple Text
                            {
                                DataRow drnew = dt.NewRow();
                                drnew[0] = "Q" + item.QuestionId + "_S" + "-" + ReplaceHtml.ReplaceText(item.QuestionTitle);
                                dt.Rows.Add(drnew);
                            }
                            else if (item.QuestionTypeId == 8 || item.QuestionTypeId == 12)//Single punch matrix type  && Rank order type
                            {
                                //if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
                                //{
                                //foreach (var opt in item.lstOptionsModel)
                                //{
                                //    DataRow drnew = dt.NewRow();
                                //    //drnew[0] = "Q" + item.QuestionId + "_M" + opt.OptionId + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                //    drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                //    dt.Rows.Add(drnew);
                                //}
                                //}
                                if (item.lstMatrixTypeModel != null && item.lstMatrixTypeModel.Count > 0)
                                {
                                    foreach (var col in item.lstMatrixTypeModel)
                                    {
                                        DataRow drnew = dt.NewRow();
                                        //drnew[0] = "Q" + item.QuestionId + "_M" + opt.OptionId + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                        drnew[0] = "Q" + item.QuestionId + "_M" + col.DisplayOrder + "-" + ReplaceHtml.ReplaceText(col.ColumnName);
                                        dt.Rows.Add(drnew);
                                    }
                                }
                            }
                            else if (item.QuestionTypeId == 13)//Kano model matrix type 
                            {
                                if (model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel != null && model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel.Count > 0)
                                {
                                    foreach (var opt in model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel)
                                    {
                                        DataRow drnew = dt.NewRow();
                                        drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Present";
                                        dt.Rows.Add(drnew);
                                        DataRow drnew1 = dt.NewRow();
                                        drnew1[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Absent";
                                        dt.Rows.Add(drnew1);
                                    }
                                }
                            }
                            // Single Punch Matrix Type || Range Matrix Type Question ||
                            // Single punch type answer horizontal position || 
                            // Multiple punch type answer horizontal position || 
                            // Rank order type question || 
                            // KANO Model Matrix type question || Dropdown Matrix type question
                            else if (item.QuestionTypeId > 7 && item.QuestionTypeId != 15)
                            {
                                if (model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel != null && model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel.Count > 0)
                                {
                                    foreach (var opt in model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel)
                                    {
                                        foreach (var column in item.lstMatrixTypeModel)
                                        {
                                            DataRow drnew = dt.NewRow();
                                            drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "_C" + column.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + " " + ReplaceHtml.ReplaceText(column.ColumnName);
                                            dt.Rows.Add(drnew);
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (item.QuestionTypeId == 2 || item.QuestionTypeId == 4 || item.QuestionTypeId == 5 || item.QuestionTypeId == 6)//Multi Punch || Constant Sum || Ranking || Matrix Type
                            {
                                if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
                                {
                                    foreach (var opt in item.lstOptionsModel)
                                    {
                                        DataRow drnew = dt.NewRow();
                                        //drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                        drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                        dt.Rows.Add(drnew);
                                    }
                                }
                            }

                            else if (item.QuestionTypeId == 1 || item.QuestionTypeId == 3 || item.QuestionTypeId == 7 || item.QuestionTypeId == 15)//Single Punch || Range || Simple Text
                            {
                                DataRow drnew = dt.NewRow();
                                //drnew[0] = "Q" + item.DisplayOrder + "_S" + "-" + ReplaceHtml.ReplaceText(item.QuestionTitle);
                                drnew[0] = "Q" + item.QuestionId + "_S" + "-" + ReplaceHtml.ReplaceText(item.QuestionTitle);
                                dt.Rows.Add(drnew);
                            }
                            else if (item.QuestionTypeId == 8 || item.QuestionTypeId == 12)//Single punch matrix type  && Rank order type
                            {
                                //if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
                                //{
                                //foreach (var opt in item.lstOptionsModel)
                                //{
                                //    DataRow drnew = dt.NewRow();
                                //    //drnew[0] = "Q" + item.QuestionId + "_M" + opt.OptionId + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                //    drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                //    dt.Rows.Add(drnew);
                                //}
                                //}
                                if (item.lstMatrixTypeModel != null && item.lstMatrixTypeModel.Count > 0)
                                {
                                    foreach (var col in item.lstMatrixTypeModel)
                                    {
                                        DataRow drnew = dt.NewRow();
                                        //drnew[0] = "Q" + item.QuestionId + "_M" + opt.OptionId + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                        drnew[0] = "Q" + item.QuestionId + "_M" + col.DisplayOrder + "-" + ReplaceHtml.ReplaceText(col.ColumnName);
                                        dt.Rows.Add(drnew);
                                    }
                                }
                            }
                            else if (item.QuestionTypeId == 13)//Kano model matrix type 
                            {
                                if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
                                {
                                    foreach (var opt in item.lstOptionsModel)
                                    {
                                        DataRow drnew = dt.NewRow();
                                        //drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Present";
                                        drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Present";
                                        dt.Rows.Add(drnew);
                                        DataRow drnew1 = dt.NewRow();
                                        //drnew1[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Absent";
                                        drnew1[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Absent";
                                        dt.Rows.Add(drnew1);
                                    }
                                }
                            }
                            // Single Punch Matrix Type || Range Matrix Type Question ||
                            // Single punch type answer horizontal position || 
                            // Multiple punch type answer horizontal position || 
                            // Rank order type question || 
                            // KANO Model Matrix type question || Dropdown Matrix type question
                            else if (item.QuestionTypeId > 7)
                            {
                                if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
                                {
                                    foreach (var opt in item.lstOptionsModel)
                                    {
                                        foreach (var column in item.lstMatrixTypeModel)
                                        {
                                            DataRow drnew = dt.NewRow();
                                            //drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "_C" + column.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + " " + ReplaceHtml.ReplaceText(column.ColumnName);
                                            drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "_C" + column.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + " " + ReplaceHtml.ReplaceText(column.ColumnName);
                                            dt.Rows.Add(drnew);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                // Get the Excel Pattern of dt
                dtF = comMethods.GenerateTransposedTable(dt);

                // Get all the reponders
                projectBase.Id = model.projectModel.Id;
                actionResult = adminAction.CompleteRespondersLoad_ByProjectId(projectBase);
                List<RespondersModel> lstResponders = new List<RespondersModel>();
                foreach (DataRow dr in actionResult.dtResult.Rows)
                {
                    lstResponders.Add(new RespondersModel
                    {

                        InvitationGuid = dr["InvitationGuid"] != DBNull.Value ? Convert.ToString(dr["InvitationGuid"]) : "",
                        Email = dr["Email"] != DBNull.Value ? Convert.ToString(dr["Email"]) : "",

                    });
                }
                IEnumerable<RespondersModel> filteredList = lstResponders
                 .GroupBy(customer => customer.InvitationGuid).Select(group => group.First());
                // Get Responses
                SurveyLoadBase surveyLoadBase = new SurveyLoadBase();
                string json = string.Empty;
                SRV.BaseLayer.ActionResult actionResultResponses = new SRV.BaseLayer.ActionResult();
                int response_id = 1;            //ss
                foreach (var responder in filteredList)
                {
                    DataTable responsesDt = new DataTable();
                    surveyLoadBase.Id = model.projectModel.Id;
                    surveyLoadBase.Email = (!string.IsNullOrEmpty(responder.Email) ? Convert.ToString(responder.Email) : "");
                    surveyLoadBase.ExternalResponderId = (!string.IsNullOrEmpty(responder.InvitationGuid) ? Convert.ToString(responder.InvitationGuid) : "");

                    if (Array.IndexOf(Emails, surveyLoadBase.Email) > -1 || Array.IndexOf(ExternalResponders, surveyLoadBase.ExternalResponderId) > -1)
                    {
                        if (model.projectModel.lstProductProjectModel != null &&
                            model.projectModel.lstProductProjectModel.Count > 0)
                        {
                            foreach (var productModel in model.projectModel.lstProductProjectModel)
                            {
                                surveyLoadBase.ProductId = productModel.Id;
                                actionResultResponses = respondentAction.Response_LoadByProjectId(surveyLoadBase);
                                if (actionResultResponses.IsSuccess)
                                {
                                    if (responsesDt.Rows.Count == 0)
                                        responsesDt = actionResultResponses.dtResult.Copy();
                                    else
                                    {
                                        foreach (DataRow drThisRespRow in actionResultResponses.dtResult.Rows)
                                            responsesDt.ImportRow(drThisRespRow);
                                    }
                                }
                            }
                        }
                        else
                        {
                            actionResultResponses = respondentAction.Response_LoadByProjectId(surveyLoadBase);
                            if (actionResultResponses.IsSuccess)
                                responsesDt = actionResultResponses.dtResult.Copy();
                        }
                    }
                    // surveyLoadBase.OfflineResponderId = dr["RespId"] != DBNull.Value ? Convert.ToString(dr["RespId"]) : "";
                    //DataRow dr_dtF = dtF.NewRow();
                    // if (String.IsNullOrEmpty(surveyLoadBase.Email)) continue;

                    //actionResultResponses = respondentAction.Response_LoadByProjectId(surveyLoadBase);
                    if (responsesDt.Rows.Count > 0)//actionResultResponses.IsSuccess
                    {

                        int count = 1;
                        count = (model.projectModel.ProjectType == "2") ? count + 2 : count;        //ss
                        DataRow dr_dtF = dtF.NewRow();
                        DataRow drTemp = dtF.NewRow();
                        string product_desc = string.Empty;
                        string product_logiId = string.Empty;
                        int oldProductID = responsesDt.Rows[0]["ProductId"] != DBNull.Value ? Convert.ToInt32(responsesDt.Rows[0]["ProductId"]) : 0;

                        foreach (DataRow drResponse in responsesDt.Rows)// actionResultResponses.dtResult.Rows
                        {

                            int optionsCount = 0;

                            int productId = drResponse["ProductId"] != DBNull.Value ? Convert.ToInt32(drResponse["ProductId"]) : 0;
                            if (productId != oldProductID)
                            {
                                count = (model.projectModel.ProjectType == "2") ? 3 : 1;
                                dr_dtF = dtF.NewRow();
                            }

                            //--ss

                            product_desc = drResponse["ProductDescription"] != DBNull.Value ? Convert.ToString(drResponse["ProductDescription"]) : "";
                            product_logiId = drResponse["ProductLogicalId"] != DBNull.Value ? Convert.ToString(drResponse["ProductLogicalId"]) : "";

                            //--ss
                            int questionId = drResponse["QuestionId"] != DBNull.Value ? Convert.ToInt32(drResponse["QuestionId"]) : 0;
                            string answerExpression = drResponse["AnswerExpression"] != DBNull.Value ? Convert.ToString(drResponse["AnswerExpression"]) : "";
                            int questionTypeId = drResponse["QuestionTypeId"] != DBNull.Value ? Convert.ToInt32(drResponse["QuestionTypeId"]) : 1;
                            int PipedFrom = drResponse["PipedFrom"] != DBNull.Value ? Convert.ToInt32(drResponse["PipedFrom"]) : 0;
                            //dr_dtF[0] = drResponse;

                            // Get Total options count for this question
                            questionBase.Id = Convert.ToInt32(questionId);
                            SRV.BaseLayer.ActionResult actionResultAnswersTemp = new SRV.BaseLayer.ActionResult();
                            actionResultAnswersTemp = adminAction.Option_LoadAllById(questionBase);
                            if (actionResultAnswersTemp.dtResult != null && actionResultAnswersTemp.dtResult.Rows.Count > 0)
                            {
                                optionsCount = actionResultAnswersTemp.dtResult.Rows.Count;
                            }

                            if (questionTypeId == 2 || questionTypeId == 4 || questionTypeId == 5 || questionTypeId == 6)
                            {
                                string[] allAnswers = answerExpression.Split('|');
                                if (allAnswers.Length > 0)
                                {
                                    if (questionTypeId == 2)// Multi Punch
                                    {
                                        string allRec = string.Empty;
                                        List<OptionsModel> matrixAns = new List<OptionsModel>();
                                        if (PipedFrom > 0 && !drResponse["QuestionTitle"].ToString().Contains("{{Q}}"))
                                        {
                                            matrixAns = model.lstQuestionModel.Where(v => v.QuestionId == PipedFrom).FirstOrDefault().lstOptionsModel;
                                        }
                                        else
                                        {
                                            matrixAns = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                        }

                                        foreach (var item in matrixAns)
                                        {
                                            string matchedId = string.Empty;
                                            foreach (string ans in allAnswers)
                                            {
                                                if (Convert.ToString(item.OptionId) == ans)
                                                {
                                                    matchedId = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            allRec += matchedId + "|";
                                        }

                                        string[] allAnswersMat = allRec.Substring(0, allRec.Length - 1).Split('|');
                                        foreach (string ans in allAnswersMat)
                                        {

                                            if (count <= dtF.Columns.Count - 1)
                                            {
                                                if ((Regex.Replace(dtF.Columns[count].ToString().Split('-')[0].Split('_')[1], @"[^\d]", String.Empty)) == ans)
                                                {
                                                    dr_dtF[count] = "1";// ans;
                                                    count++;
                                                    optionsCount--;
                                                }
                                                else
                                                {
                                                    dr_dtF[count] = "2";
                                                    count++;
                                                    optionsCount--;
                                                }
                                            }
                                        }
                                        //count = count + optionsCount;
                                    }
                                    else if (questionTypeId == 6) // matrix Type
                                    {
                                        foreach (string ans in allAnswers)
                                        {
                                            dr_dtF[count] = (ans.ToLower().IndexOf("yes") != -1) ? "1" : "2";
                                            count++;
                                            optionsCount--;
                                        }
                                        count = count + optionsCount;
                                    }
                                    else
                                    {
                                        foreach (string ans in allAnswers)
                                        {
                                            dr_dtF[count] = ans;
                                            count++;
                                            optionsCount--;
                                        }
                                        count = count + optionsCount;
                                    }
                                }
                            }
                            else if (questionTypeId == 1) // Single Punch Answers
                            {
                                //List<OptionsModel> SinglePunchAns = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                List<OptionsModel> SinglePunchAns = new List<OptionsModel>();
                                if (PipedFrom > 0 && !drResponse["QuestionTitle"].ToString().Contains("{{Q}}"))
                                {
                                    SinglePunchAns = model.lstQuestionModel.Where(v => v.QuestionId == PipedFrom).FirstOrDefault().lstOptionsModel;
                                }
                                else
                                {
                                    SinglePunchAns = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                }
                                int ansIndex = 0;
                                foreach (var item in SinglePunchAns)
                                {
                                    ansIndex++;
                                    if (Convert.ToString(item.OptionId) == answerExpression)
                                    {
                                        break;
                                    }
                                }

                                dr_dtF[count] = ansIndex;//answerExpression;
                                count++;
                                optionsCount--;
                            }
                            else if (questionTypeId == 8 || questionTypeId == 12) // single punch matrix answers
                            {
                                List<OptionsModel> optionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                List<MatrixTypeModel> columnDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstMatrixTypeModel;
                                int qustionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).Select(v => v.DisplayOrder).FirstOrDefault();
                                string[] allAnswers = answerExpression.Split('|');

                                List<string> lstColumnOrdinals = new List<string>();

                                string formattedAnswer = string.Empty;
                                string answerValue = string.Empty;
                                if (allAnswers.Length > 0)
                                {
                                    foreach (var ansExpression in allAnswers)
                                    {
                                        string displaycol = string.Empty;
                                        string displayopt = string.Empty;
                                        foreach (var item in optionDisplayOrder)
                                        {

                                            if (Convert.ToString(item.OptionId) == ansExpression.Split('~')[0])
                                            {
                                                displaycol = Convert.ToString(item.DisplayOrder);
                                                break;
                                            }
                                        }
                                        foreach (var item in columnDisplayOrder)
                                        {
                                            if (Convert.ToString(item.Id) == ansExpression.Split('~')[1])
                                            {
                                                displayopt = Convert.ToString(item.DisplayOrder);
                                                break;
                                            }
                                        }
                                        //  formattedAnswer = "Q" + questionId + "_M" + displaycol + "#" + displayopt;
                                        formattedAnswer = "Q" + questionId + "_M" + displayopt + "#" + displaycol;
                                        foreach (DataColumn column in dtF.Columns)
                                        {
                                            string[] columnHeader = column.ColumnName.Split('-');
                                            string[] answer = columnHeader[0].Split('_');
                                            if (columnHeader.Length > 1)
                                            {
                                                string answerInHeader = answer[0] + "_" + answer[1];
                                                string[] formattedAnswerArr = formattedAnswer.Split('#');
                                                if (formattedAnswerArr[0] == answerInHeader)
                                                {
                                                    int index = dr_dtF.Table.Columns[column.ColumnName].Ordinal;
                                                    lstColumnOrdinals.Add(index.ToString());
                                                    count++;
                                                    dr_dtF[index] = formattedAnswerArr[1];
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            else if (questionTypeId == 13) // Kano model matrix type
                            {
                                List<OptionsModel> optionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                List<MatrixTypeModel> columnDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstMatrixTypeModel;
                                int qustionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).Select(v => v.DisplayOrder).FirstOrDefault();
                                string[] allAnswers = answerExpression.Split('|');

                                List<string> lstColumnOrdinals = new List<string>();

                                string formattedAnswer = string.Empty;
                                string answerValue = string.Empty;
                                if (allAnswers.Length > 1)
                                {
                                    int repeatId = 0;
                                    foreach (var ansExpression in allAnswers)
                                    {
                                        string displaycol = string.Empty;
                                        string displayopt = string.Empty;
                                        string displayText = string.Empty;
                                        string text = string.Empty;
                                        if (repeatId == Convert.ToInt32(ansExpression.Split('~')[0]))
                                        {
                                            text = "_Absent";
                                            repeatId = 0;
                                        }
                                        else
                                        {
                                            text = "_Present";
                                        }
                                        foreach (var item in optionDisplayOrder)
                                        {
                                            if (Convert.ToString(item.OptionId) == ansExpression.Split('~')[0])
                                            {
                                                displayText = Convert.ToString(item.Option) + text;
                                                displaycol = Convert.ToString(item.DisplayOrder);
                                                repeatId = Convert.ToInt32(ansExpression.Split('~')[0]);
                                                break;
                                            }
                                        }
                                        foreach (var item in columnDisplayOrder)
                                        {
                                            if (Convert.ToString(item.Id) == ansExpression.Split('~')[1])
                                            {
                                                displayopt = Convert.ToString(item.DisplayOrder);
                                                break;
                                            }
                                        }
                                        //formattedAnswer = "Q" + qustionDisplayOrder + "_M" + displaycol + "-" + displayText + "#" + displayopt;
                                        formattedAnswer = "Q" + questionId + "_M" + displaycol + "-" + displayText + "#" + displayopt;
                                        foreach (DataColumn column in dtF.Columns)
                                        {

                                            string[] columnHeader = column.ColumnName.Split('-');
                                            string[] answer = columnHeader[0].Split('_');
                                            if (columnHeader.Length > 1)
                                            {
                                                string answerInHeader = answer[0] + "_" + answer[1] + "-" + columnHeader[1];
                                                string[] formattedAnswerArr = formattedAnswer.Split('#');
                                                if (Regex.Replace(formattedAnswerArr[0], @"\s+", "", RegexOptions.Multiline) == Regex.Replace(answerInHeader, @"\s+", "", RegexOptions.Multiline))
                                                {
                                                    int index = dr_dtF.Table.Columns[column.ColumnName].Ordinal;
                                                    lstColumnOrdinals.Add(index.ToString());
                                                    count++;
                                                    dr_dtF[index] = formattedAnswerArr[1];
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            else if (questionTypeId > 7 && questionTypeId != 15)
                            {
                                List<OptionsModel> optionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                List<MatrixTypeModel> columnDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstMatrixTypeModel;
                                List<DDLMatrixTypeModel> dropDownDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstDDlMatrixModel;
                                //count = count + (optionDisplayOrder.Count * columnDisplayOrder.Count);
                                int optDisplayOrder = 0;
                                if (optionDisplayOrder != null)
                                {
                                    optDisplayOrder = optionDisplayOrder.Count;
                                }
                                int colDisplayOrder = 0;
                                if (columnDisplayOrder != null)
                                {
                                    colDisplayOrder = columnDisplayOrder.Count;
                                }
                                int qustionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).Select(v => v.DisplayOrder).FirstOrDefault();
                                string[] allAnswers = answerExpression.Split('|');

                                List<string> lstColumnOrdinals = new List<string>();

                                string formattedAnswer = string.Empty;
                                string answerValue = string.Empty;
                                int DDlDisplayOrder = 0;
                                if (allAnswers.Length > 1)
                                {
                                    foreach (var ansExpression in allAnswers)
                                    {
                                        string displaycol = string.Empty;
                                        string displayopt = string.Empty;

                                        if (questionTypeId == 9 || questionTypeId == 14)
                                        {
                                            foreach (var item in optionDisplayOrder)
                                            {
                                                if (Convert.ToString(item.OptionId) == ansExpression.Split('~')[0].Split('*')[0])
                                                {
                                                    displaycol = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            foreach (var item in columnDisplayOrder)
                                            {
                                                if (Convert.ToString(item.Id) == ansExpression.Split('~')[1])
                                                {
                                                    displayopt = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            //formattedAnswer = "Q" + qustionDisplayOrder + "_M" + displaycol + "_C" + displayopt;
                                            formattedAnswer = "Q" + questionId + "_M" + displaycol + "_C" + displayopt;
                                            answerValue = ansExpression.Split('~')[0].Split('*')[1];
                                            if (questionTypeId == 14)
                                                DDlDisplayOrder = dropDownDisplayOrder.Where(l => l.Id == Convert.ToInt32(answerValue)).FirstOrDefault().DDLDisplayOrder;
                                        }
                                        else
                                        {
                                            foreach (var item in optionDisplayOrder)
                                            {
                                                if (Convert.ToString(item.OptionId) == ansExpression.Split('~')[0])
                                                {
                                                    displaycol = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            foreach (var item in columnDisplayOrder)
                                            {
                                                if (Convert.ToString(item.Id) == ansExpression.Split('~')[1])
                                                {
                                                    displayopt = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            //formattedAnswer = "Q" + qustionDisplayOrder + "_M" + displaycol + "_C" + displayopt;
                                            formattedAnswer = "Q" + questionId + "_M" + displaycol + "_C" + displayopt;
                                        }
                                        foreach (DataColumn column in dtF.Columns)
                                        {
                                            string[] columnHeader = column.ColumnName.Split('-');

                                            if (columnHeader.Length > 1)
                                            {
                                                string[] answer = columnHeader[0].Split('_');

                                                if (answer.Length > 2)
                                                {
                                                    string answerInHeader = answer[0] + "_" + answer[1] + "_" + answer[2];

                                                    if (formattedAnswer == answerInHeader)
                                                    {
                                                        int index = dr_dtF.Table.Columns[column.ColumnName].Ordinal;
                                                        lstColumnOrdinals.Add(index.ToString());
                                                        //count++;
                                                        if (questionTypeId == 9)
                                                        {
                                                            dr_dtF[index] = answerValue;
                                                        }
                                                        else if (questionTypeId == 14)
                                                        {
                                                            dr_dtF[index] = DDlDisplayOrder;
                                                        }
                                                        else
                                                        {
                                                            dr_dtF[index] = "1";
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }

                                // Fill All the Empty cells with 2

                                for (int i = 0; i <= dtF.Columns.Count - 1; i++)
                                    if (String.IsNullOrEmpty(Convert.ToString(dr_dtF[i])))
                                        dr_dtF[i] = "";
                            }

                            else
                            {
                                dr_dtF[count] = answerExpression;
                                count++;
                                optionsCount--;
                            }

                            if (productId != oldProductID)
                            {
                                dtF.Rows.Add(drTemp);
                                //  drTemp[0] = dtF.Rows.IndexOf(drTemp) + 1;
                                drTemp[0] = response_id;
                                oldProductID = productId;
                            }


                            dr_dtF[1] = (!string.IsNullOrEmpty(responder.InvitationGuid) ? Convert.ToString(responder.InvitationGuid) : "");

                            drTemp = dr_dtF;
                            //---ss
                            if (model.projectModel.ProjectType == "2")
                            {
                                drTemp[2] = product_desc;
                                drTemp[1] = product_logiId;
                            }
                            //---ss

                        }

                        dtF.Rows.Add(dr_dtF);
                        //dr_dtF[0] = dtF.Rows.IndexOf(dr_dtF) + 1;
                        drTemp[0] = response_id;
                        //---ss
                        if (model.projectModel.ProjectType == "2")
                        {
                            dr_dtF[2] = product_desc;
                            dr_dtF[1] = product_logiId;
                        }
                        response_id++;
                        //---ss
                    }
                }
            }
            catch (Exception ex)
            {
                //ErrorReporting.WebApplicationError(ex);
            }
            return dtF;
        }
        #endregion

        #region GetReportDataTableOfflineForProduct
        private DataTable GetReportDataTableOfflineForProduct(string Id, int ResponseCount, string[] ExternalResponders)
        {

            RespondentController respondent = new RespondentController();
            SurveyResponseModel model = new SurveyResponseModel();
            DataTable dtF = new DataTable();
            try
            {
                DataTable dt = new DataTable();

                string projectGuid = string.Empty;
                projectGuid = Id;
                dt.Columns.Add("Resp_Id", typeof(string));

                model = respondent.GetProjectPreview(projectGuid);

                //--ss
                if (model.projectModel.ProjectType == "2")
                {
                    DataRow drnew = dt.NewRow();
                    drnew[0] = "Product_LogicalId";
                    dt.Rows.Add(drnew);
                    DataRow drnew1 = dt.NewRow();
                    drnew1[0] = "Product_Name";
                    dt.Rows.Add(drnew1);
                }
                //--

                // Load all the questions of this project
                List<QuestionList> questionsList = new List<QuestionList>();
                questionBase.ProjectId = Convert.ToInt32(model.projectModel.Id);
                actionResult = adminAction.QuestionsLoadAll_ByProjectId(questionBase);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    foreach (DataRow dr in actionResult.dtResult.Rows)
                    {
                        questionsList.Add(new QuestionList
                        {
                            id = dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0,
                            questionTypeId = dr["QuestionTypeId"] != DBNull.Value ? Convert.ToInt32(dr["QuestionTypeId"]) : 0
                        });
                    }
                }

                string fileName = Guid.NewGuid().ToString().Substring(0, 5) + "_" + DateTime.Now.ToString("MM-dd-yyyy") + "_" + model.projectModel.ProjectName.Replace(" ", "");

                if (model.lstQuestionModel != null && model.lstQuestionModel.Count > 0)
                {
                    foreach (var item in model.lstQuestionModel)
                    {
                        if (item.QuestionTypeId == 2 || item.QuestionTypeId == 4 || item.QuestionTypeId == 5 || item.QuestionTypeId == 6)//Multi Punch || Constant Sum || Ranking || Matrix Type
                        {
                            if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
                            {
                                foreach (var opt in item.lstOptionsModel)
                                {
                                    DataRow drnew = dt.NewRow();
                                    //drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                    drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                    dt.Rows.Add(drnew);
                                }
                            }
                        }

                        else if (item.QuestionTypeId == 1 || item.QuestionTypeId == 3 || item.QuestionTypeId == 7 || item.QuestionTypeId == 15)//Single Punch || Range || Simple Text
                        {
                            DataRow drnew = dt.NewRow();
                            //drnew[0] = "Q" + item.DisplayOrder + "_S" + "-" + ReplaceHtml.ReplaceText(item.QuestionTitle);
                            drnew[0] = "Q" + item.QuestionId + "_S" + "-" + ReplaceHtml.ReplaceText(item.QuestionTitle);
                            dt.Rows.Add(drnew);
                        }
                        else if (item.QuestionTypeId == 8 || item.QuestionTypeId == 12)//Single punch matrix type  && Rank order type
                        {
                            //if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
                            //{
                            //foreach (var opt in item.lstOptionsModel)
                            //{
                            //    DataRow drnew = dt.NewRow();
                            //    //drnew[0] = "Q" + item.QuestionId + "_M" + opt.OptionId + "-" + ReplaceHtml.ReplaceText(opt.Option);
                            //    drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
                            //    dt.Rows.Add(drnew);
                            //}
                            //}
                            if (item.lstMatrixTypeModel != null && item.lstMatrixTypeModel.Count > 0)
                            {
                                foreach (var col in item.lstMatrixTypeModel)
                                {
                                    DataRow drnew = dt.NewRow();
                                    //drnew[0] = "Q" + item.QuestionId + "_M" + opt.OptionId + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                    drnew[0] = "Q" + item.QuestionId + "_M" + col.DisplayOrder + "-" + ReplaceHtml.ReplaceText(col.ColumnName);
                                    dt.Rows.Add(drnew);
                                }
                            }
                        }
                        else if (item.QuestionTypeId == 13)//Kano model matrix type 
                        {
                            if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
                            {
                                foreach (var opt in item.lstOptionsModel)
                                {
                                    DataRow drnew = dt.NewRow();
                                    //drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Present";
                                    drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Present";
                                    dt.Rows.Add(drnew);
                                    DataRow drnew1 = dt.NewRow();
                                    //drnew1[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Absent";
                                    drnew1[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Absent";
                                    dt.Rows.Add(drnew1);
                                }
                            }
                        }
                        // Single Punch Matrix Type || Range Matrix Type Question ||
                        // Single punch type answer horizontal position || 
                        // Multiple punch type answer horizontal position || 
                        // Rank order type question || 
                        // KANO Model Matrix type question || Dropdown Matrix type question
                        else if (item.QuestionTypeId > 7)
                        {
                            if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
                            {
                                foreach (var opt in item.lstOptionsModel)
                                {
                                    foreach (var column in item.lstMatrixTypeModel)
                                    {
                                        DataRow drnew = dt.NewRow();
                                        //drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "_C" + column.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + " " + ReplaceHtml.ReplaceText(column.ColumnName);
                                        drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "_C" + column.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + " " + ReplaceHtml.ReplaceText(column.ColumnName);
                                        dt.Rows.Add(drnew);
                                    }
                                }
                            }
                        }
                    }
                }
                // Get the Excel Pattern of dt
                dtF = comMethods.GenerateTransposedTable(dt);

                // Get all the reponders
                projectBase.Id = model.projectModel.Id;
                actionResult = adminAction.Responders_Offline_LoadBy_ProjectId(projectBase);

                // Get Responses
                SurveyLoadBase surveyLoadBase = new SurveyLoadBase();
                string json = string.Empty;
                SRV.BaseLayer.ActionResult actionResultResponses = new SRV.BaseLayer.ActionResult();
                int response_id = ResponseCount;            //ss
                foreach (DataRow dr in actionResult.dtResult.Rows)
                {
                    surveyLoadBase.Id = model.projectModel.Id;
                    surveyLoadBase.OfflineResponderId = dr["RespId"] != DBNull.Value ? Convert.ToString(dr["RespId"]) : "";
                    //DataRow dr_dtF = dtF.NewRow();

                    if (String.IsNullOrEmpty(surveyLoadBase.OfflineResponderId)) continue;


                    DataTable responsesDt = new DataTable();

                    if (Array.IndexOf(ExternalResponders, surveyLoadBase.OfflineResponderId) > -1)
                    {
                        if (model.projectModel.lstProductProjectModel != null && model.projectModel.lstProductProjectModel.Count > 0)
                        {
                            foreach (var productModel in model.projectModel.lstProductProjectModel)
                            {
                                surveyLoadBase.ProductId = productModel.Id;
                                actionResultResponses = respondentAction.Response_Offline_LoadBy_ProjectId_RespId(surveyLoadBase);
                                if (actionResultResponses.IsSuccess)
                                {
                                    if (responsesDt.Rows.Count == 0)
                                        responsesDt = actionResultResponses.dtResult.Copy();
                                    else
                                    {
                                        foreach (DataRow drThisRespRow in actionResultResponses.dtResult.Rows)
                                            responsesDt.ImportRow(drThisRespRow);

                                    }
                                }
                            }
                        }
                        else
                        {
                            actionResultResponses = respondentAction.Response_Offline_LoadBy_ProjectId_RespId(surveyLoadBase);
                            if (actionResultResponses.IsSuccess)
                                responsesDt = actionResultResponses.dtResult.Copy();

                        }
                    }


                    //actionResultResponses = respondentAction.Response_Offline_LoadBy_ProjectId_RespId(surveyLoadBase);
                    if (responsesDt.Rows.Count > 0)//actionResultResponses.IsSuccess
                    {
                        //dr_dtF[0] = surveyLoadBase.Email;

                        int count = 1;
                        count = (model.projectModel.ProjectType == "2") ? count + 2 : count;        //ss
                        DataRow dr_dtF = dtF.NewRow();
                        DataRow drTemp = dtF.NewRow();
                        string product_desc = string.Empty;
                        string product_logiId = string.Empty;
                        int oldProductID = responsesDt.Rows[0]["ProductId"] != DBNull.Value ? Convert.ToInt32(responsesDt.Rows[0]["ProductId"]) : 0;
                        foreach (DataRow drResponse in responsesDt.Rows)// actionResultResponses.dtResult.Rows
                        {

                            int optionsCount = 0;

                            int productId = drResponse["ProductId"] != DBNull.Value ? Convert.ToInt32(drResponse["ProductId"]) : 0;
                            if (productId != oldProductID)
                            {
                                count = (model.projectModel.ProjectType == "2") ? 3 : 1;
                                dr_dtF = dtF.NewRow();
                            }

                            //--ss

                            product_desc = drResponse["ProductDescription"] != DBNull.Value ? Convert.ToString(drResponse["ProductDescription"]) : "";
                            product_logiId = drResponse["ProductLogicalId"] != DBNull.Value ? Convert.ToString(drResponse["ProductLogicalId"]) : "";

                            //--ss
                            int questionId = drResponse["QuestionId"] != DBNull.Value ? Convert.ToInt32(drResponse["QuestionId"]) : 0;
                            string answerExpression = drResponse["AnswerExpression"] != DBNull.Value ? Convert.ToString(drResponse["AnswerExpression"]) : "";
                            int questionTypeId = drResponse["QuestionTypeId"] != DBNull.Value ? Convert.ToInt32(drResponse["QuestionTypeId"]) : 1;
                            int PipedFrom = drResponse["PipedFrom"] != DBNull.Value ? Convert.ToInt32(drResponse["PipedFrom"]) : 0;
                            //dr_dtF[0] = drResponse;

                            // Get Total options count for this question
                            questionBase.Id = Convert.ToInt32(questionId);
                            SRV.BaseLayer.ActionResult actionResultAnswersTemp = new SRV.BaseLayer.ActionResult();
                            actionResultAnswersTemp = adminAction.Option_LoadAllById(questionBase);
                            if (actionResultAnswersTemp.dtResult != null && actionResultAnswersTemp.dtResult.Rows.Count > 0)
                            {
                                optionsCount = actionResultAnswersTemp.dtResult.Rows.Count;
                            }

                            if (questionTypeId == 2 || questionTypeId == 4 || questionTypeId == 5 || questionTypeId == 6)
                            {
                                string[] allAnswers = answerExpression.Split('|');
                                if (allAnswers.Length > 0)
                                {
                                    if (questionTypeId == 2)// Multi Punch
                                    {
                                        string allRec = string.Empty;
                                        //List<OptionsModel> matrixAns = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;


                                        List<OptionsModel> matrixAns = new List<OptionsModel>();
                                        if (PipedFrom > 0 && !drResponse["QuestionTitle"].ToString().Contains("{{Q}}"))
                                        {
                                            matrixAns = model.lstQuestionModel.Where(v => v.QuestionId == PipedFrom).FirstOrDefault().lstOptionsModel;
                                        }
                                        else
                                        {
                                            matrixAns = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                        }

                                        foreach (var item in matrixAns)
                                        {
                                            string matchedId = string.Empty;
                                            foreach (string ans in allAnswers)
                                            {
                                                if (Convert.ToString(item.OptionId) == ans)
                                                {
                                                    matchedId = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            allRec += matchedId + "|";
                                        }

                                        string[] allAnswersMat = allRec.Substring(0, allRec.Length - 1).Split('|');
                                        foreach (string ans in allAnswersMat)
                                        {

                                            if (count <= dtF.Columns.Count - 1)
                                            {
                                                if ((Regex.Replace(dtF.Columns[count].ToString().Split('-')[0].Split('_')[1], @"[^\d]", String.Empty)) == ans)
                                                {
                                                    dr_dtF[count] = "1";// ans;
                                                    count++;
                                                    optionsCount--;
                                                }
                                                else
                                                {
                                                    dr_dtF[count] = "2";
                                                    count++;
                                                    optionsCount--;
                                                }
                                            }
                                        }
                                        //count = count + optionsCount;
                                    }
                                    else if (questionTypeId == 6) // matrix Type
                                    {
                                        foreach (string ans in allAnswers)
                                        {
                                            dr_dtF[count] = (ans.ToLower().IndexOf("yes") != -1) ? "1" : "2";
                                            count++;
                                            optionsCount--;
                                        }
                                        count = count + optionsCount;
                                    }
                                    else
                                    {
                                        foreach (string ans in allAnswers)
                                        {
                                            dr_dtF[count] = ans;
                                            count++;
                                            optionsCount--;
                                        }
                                        count = count + optionsCount;
                                    }
                                }
                            }
                            else if (questionTypeId == 1) // Single Punch Answers
                            {
                                //List<OptionsModel> SinglePunchAns = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                List<OptionsModel> SinglePunchAns = new List<OptionsModel>();
                                if (PipedFrom > 0 && !drResponse["QuestionTitle"].ToString().Contains("{{Q}}"))
                                {
                                    SinglePunchAns = model.lstQuestionModel.Where(v => v.QuestionId == PipedFrom).FirstOrDefault().lstOptionsModel;
                                }
                                else
                                {
                                    SinglePunchAns = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                }
                                int ansIndex = 0;
                                foreach (var item in SinglePunchAns)
                                {
                                    ansIndex++;
                                    if (Convert.ToString(item.OptionId) == answerExpression)
                                    {
                                        break;
                                    }
                                }

                                dr_dtF[count] = ansIndex;//answerExpression;
                                count++;
                                optionsCount--;
                            }
                            else if (questionTypeId == 8 || questionTypeId == 12) // single punch matrix answers
                            {
                                List<OptionsModel> optionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                List<MatrixTypeModel> columnDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstMatrixTypeModel;
                                int qustionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).Select(v => v.DisplayOrder).FirstOrDefault();
                                string[] allAnswers = answerExpression.Split('|');

                                List<string> lstColumnOrdinals = new List<string>();

                                string formattedAnswer = string.Empty;
                                string answerValue = string.Empty;
                                if (allAnswers.Length > 0)
                                {
                                    foreach (var ansExpression in allAnswers)
                                    {
                                        if (!string.IsNullOrEmpty(ansExpression))
                                        {
                                            string displaycol = string.Empty;
                                            string displayopt = string.Empty;
                                            foreach (var item in optionDisplayOrder)
                                            {

                                                if (Convert.ToString(item.OptionId) == ansExpression.Split('~')[0])
                                                {
                                                    displaycol = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            foreach (var item in columnDisplayOrder)
                                            {
                                                if (Convert.ToString(item.Id) == ansExpression.Split('~')[1])
                                                {
                                                    displayopt = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            //  formattedAnswer = "Q" + questionId + "_M" + displaycol + "#" + displayopt;
                                            formattedAnswer = "Q" + questionId + "_M" + displayopt + "#" + displaycol;
                                            foreach (DataColumn column in dtF.Columns)
                                            {
                                                string[] columnHeader = column.ColumnName.Split('-');
                                                string[] answer = columnHeader[0].Split('_');
                                                if (columnHeader.Length > 1)
                                                {
                                                    string answerInHeader = answer[0] + "_" + answer[1];
                                                    string[] formattedAnswerArr = formattedAnswer.Split('#');
                                                    if (formattedAnswerArr[0] == answerInHeader)
                                                    {
                                                        int index = dr_dtF.Table.Columns[column.ColumnName].Ordinal;
                                                        lstColumnOrdinals.Add(index.ToString());
                                                        count++;
                                                        dr_dtF[index] = formattedAnswerArr[1];
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            else if (questionTypeId == 13) // Kano model matrix type
                            {
                                List<OptionsModel> optionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                List<MatrixTypeModel> columnDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstMatrixTypeModel;
                                int qustionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).Select(v => v.DisplayOrder).FirstOrDefault();
                                string[] allAnswers = answerExpression.Split('|');

                                List<string> lstColumnOrdinals = new List<string>();

                                string formattedAnswer = string.Empty;
                                string answerValue = string.Empty;
                                if (allAnswers.Length > 1)
                                {
                                    int repeatId = 0;
                                    foreach (var ansExpression in allAnswers)
                                    {
                                        string displaycol = string.Empty;
                                        string displayopt = string.Empty;
                                        string displayText = string.Empty;
                                        string text = string.Empty;
                                        if (repeatId == Convert.ToInt32(ansExpression.Split('~')[0]))
                                        {
                                            text = "_Absent";
                                            repeatId = 0;
                                        }
                                        else
                                        {
                                            text = "_Present";
                                        }
                                        foreach (var item in optionDisplayOrder)
                                        {
                                            if (Convert.ToString(item.OptionId) == ansExpression.Split('~')[0])
                                            {
                                                displayText = Convert.ToString(item.Option) + text;
                                                displaycol = Convert.ToString(item.DisplayOrder);
                                                repeatId = Convert.ToInt32(ansExpression.Split('~')[0]);
                                                break;
                                            }
                                        }
                                        foreach (var item in columnDisplayOrder)
                                        {
                                            if (Convert.ToString(item.Id) == ansExpression.Split('~')[1])
                                            {
                                                displayopt = Convert.ToString(item.DisplayOrder);
                                                break;
                                            }
                                        }
                                        //formattedAnswer = "Q" + qustionDisplayOrder + "_M" + displaycol + "-" + displayText + "#" + displayopt;
                                        formattedAnswer = "Q" + questionId + "_M" + displaycol + "-" + displayText + "#" + displayopt;
                                        foreach (DataColumn column in dtF.Columns)
                                        {

                                            string[] columnHeader = column.ColumnName.Split('-');
                                            string[] answer = columnHeader[0].Split('_');
                                            if (columnHeader.Length > 1)
                                            {
                                                string answerInHeader = answer[0] + "_" + answer[1] + "-" + columnHeader[1];
                                                string[] formattedAnswerArr = formattedAnswer.Split('#');
                                                if (Regex.Replace(formattedAnswerArr[0], @"\s+", "", RegexOptions.Multiline) == Regex.Replace(answerInHeader, @"\s+", "", RegexOptions.Multiline))
                                                {
                                                    int index = dr_dtF.Table.Columns[column.ColumnName].Ordinal;
                                                    lstColumnOrdinals.Add(index.ToString());
                                                    count++;
                                                    dr_dtF[index] = formattedAnswerArr[1];
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            else if (questionTypeId > 7 && questionTypeId != 15)
                            {
                                List<OptionsModel> optionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                List<MatrixTypeModel> columnDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstMatrixTypeModel;
                                List<DDLMatrixTypeModel> dropDownDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstDDlMatrixModel;
                                // count = count + (optionDisplayOrder.Count * columnDisplayOrder.Count);
                                int optDisplayOrder = 0;
                                if (optionDisplayOrder != null)
                                {
                                    optDisplayOrder = optionDisplayOrder.Count;
                                }
                                int colDisplayOrder = 0;
                                if (columnDisplayOrder != null)
                                {
                                    colDisplayOrder = columnDisplayOrder.Count;
                                }
                                int qustionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).Select(v => v.DisplayOrder).FirstOrDefault();
                                string[] allAnswers = answerExpression.Split('|');

                                List<string> lstColumnOrdinals = new List<string>();

                                string formattedAnswer = string.Empty;
                                string answerValue = string.Empty;
                                int DDlDisplayOrder = 0;
                                if (allAnswers.Length > 1)
                                {
                                    foreach (var ansExpression in allAnswers)
                                    {
                                        string displaycol = string.Empty;
                                        string displayopt = string.Empty;

                                        if (questionTypeId == 9 || questionTypeId == 14)
                                        {
                                            foreach (var item in optionDisplayOrder)
                                            {
                                                if (Convert.ToString(item.OptionId) == ansExpression.Split('~')[0].Split('*')[0])
                                                {
                                                    displaycol = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            foreach (var item in columnDisplayOrder)
                                            {
                                                if (Convert.ToString(item.Id) == ansExpression.Split('~')[1])
                                                {
                                                    displayopt = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            //formattedAnswer = "Q" + qustionDisplayOrder + "_M" + displaycol + "_C" + displayopt;
                                            formattedAnswer = "Q" + questionId + "_M" + displaycol + "_C" + displayopt;
                                            answerValue = ansExpression.Split('~')[0].Split('*')[1];
                                            if (questionTypeId == 14)
                                                DDlDisplayOrder = dropDownDisplayOrder.Where(l => l.Id == Convert.ToInt32(answerValue)).FirstOrDefault().DDLDisplayOrder;
                                        }
                                        else
                                        {
                                            foreach (var item in optionDisplayOrder)
                                            {
                                                if (Convert.ToString(item.OptionId) == ansExpression.Split('~')[0])
                                                {
                                                    displaycol = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            foreach (var item in columnDisplayOrder)
                                            {
                                                if (Convert.ToString(item.Id) == ansExpression.Split('~')[1])
                                                {
                                                    displayopt = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            //formattedAnswer = "Q" + qustionDisplayOrder + "_M" + displaycol + "_C" + displayopt;
                                            formattedAnswer = "Q" + questionId + "_M" + displaycol + "_C" + displayopt;
                                        }
                                        foreach (DataColumn column in dtF.Columns)
                                        {
                                            string[] columnHeader = column.ColumnName.Split('-');

                                            if (columnHeader.Length > 1)
                                            {
                                                string[] answer = columnHeader[0].Split('_');

                                                if (answer.Length > 2)
                                                {
                                                    string answerInHeader = answer[0] + "_" + answer[1] + "_" + answer[2];

                                                    if (formattedAnswer == answerInHeader)
                                                    {
                                                        int index = dr_dtF.Table.Columns[column.ColumnName].Ordinal;
                                                        lstColumnOrdinals.Add(index.ToString());
                                                        //count++;
                                                        if (questionTypeId == 9)
                                                        {
                                                            dr_dtF[index] = answerValue;
                                                        }
                                                        else if (questionTypeId == 14)
                                                        {
                                                            dr_dtF[index] = DDlDisplayOrder;
                                                        }
                                                        else
                                                        {
                                                            dr_dtF[index] = "1";
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }

                                // Fill All the Empty cells with 2

                                for (int i = 0; i <= dtF.Columns.Count - 1; i++)
                                    if (String.IsNullOrEmpty(Convert.ToString(dr_dtF[i])))
                                        dr_dtF[i] = "";
                            }

                            else
                            {
                                dr_dtF[count] = answerExpression;
                                count++;
                                optionsCount--;
                            }



                            if (productId != oldProductID)
                            {
                                dtF.Rows.Add(drTemp);
                                drTemp[0] = response_id;
                                oldProductID = productId;
                            }

                            drTemp = dr_dtF;
                            //---ss
                            if (model.projectModel.ProjectType == "2")
                            {
                                drTemp[2] = product_desc;
                                drTemp[1] = product_logiId;
                            }
                            //---ss


                        }


                        dtF.Rows.Add(dr_dtF);
                        drTemp[0] = response_id;
                        //---ss
                        if (model.projectModel.ProjectType == "2")
                        {
                            dr_dtF[2] = product_desc;
                            dr_dtF[1] = product_logiId;
                        }
                        response_id++;
                        //---ss
                    }
                }
            }
            catch (Exception ex)
            {
                //ErrorReporting.WebApplicationError(ex);
            }
            return dtF;
        }
        #endregion

        #region GetReportDataTableOffline1
        private DataTable GetReportDataTableOffline1(string Id, int ResponseCount, string[] ExternalResponders)
        {

            RespondentController respondent = new RespondentController();
            SurveyResponseModel model = new SurveyResponseModel();
            DataTable dtF = new DataTable();
            try
            {
                DataTable dt = new DataTable();

                string projectGuid = string.Empty;
                projectGuid = Id;
                dt.Columns.Add("Resp_Id", typeof(string));

                DataRow drnewRow = dt.NewRow();
                drnewRow[0] = "Import_Resp";
                dt.Rows.Add(drnewRow);

                model = respondent.GetProjectPreview(projectGuid);

                //--ss
                if (model.projectModel.ProjectType == "2")
                {
                    DataRow drnew = dt.NewRow();
                    drnew[0] = "Product_LogicalId";
                    dt.Rows.Add(drnew);
                    DataRow drnew1 = dt.NewRow();
                    drnew1[0] = "Product_Name";
                    dt.Rows.Add(drnew1);
                }
                //--

                // Load all the questions of this project
                List<QuestionList> questionsList = new List<QuestionList>();
                questionBase.ProjectId = Convert.ToInt32(model.projectModel.Id);
                actionResult = adminAction.QuestionsLoadAll_ByProjectId(questionBase);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    foreach (DataRow dr in actionResult.dtResult.Rows)
                    {
                        questionsList.Add(new QuestionList
                        {
                            id = dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0,
                            questionTypeId = dr["QuestionTypeId"] != DBNull.Value ? Convert.ToInt32(dr["QuestionTypeId"]) : 0
                        });
                    }
                }

                string fileName = Guid.NewGuid().ToString().Substring(0, 5) + "_" + DateTime.Now.ToString("MM-dd-yyyy") + "_" + model.projectModel.ProjectName.Replace(" ", "");

                if (model.lstQuestionModel != null && model.lstQuestionModel.Count > 0)
                {
                    foreach (var item in model.lstQuestionModel)
                    {
                        if (item.PipedFrom > 0 && (!item.QuestionTitle.Contains("{{Q}}")))
                        {
                            if (item.QuestionTypeId == 2 || item.QuestionTypeId == 4 || item.QuestionTypeId == 5 || item.QuestionTypeId == 6)//Multi Punch || Constant Sum || Ranking || Matrix Type
                            {
                                if (model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel != null && model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel.Count > 0)
                                {
                                    foreach (var opt in model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel)
                                    {
                                        DataRow drnew = dt.NewRow();
                                        drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                        dt.Rows.Add(drnew);
                                    }
                                }
                            }

                            else if (item.QuestionTypeId == 1 || item.QuestionTypeId == 3 || item.QuestionTypeId == 7 || item.QuestionTypeId == 15)//Single Punch || Range || Simple Text
                            {
                                DataRow drnew = dt.NewRow();
                                drnew[0] = "Q" + item.QuestionId + "_S" + "-" + ReplaceHtml.ReplaceText(item.QuestionTitle);
                                dt.Rows.Add(drnew);
                            }
                            else if (item.QuestionTypeId == 8 || item.QuestionTypeId == 12)//Single punch matrix type  && Rank order type
                            {
                                //if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
                                //{
                                //foreach (var opt in item.lstOptionsModel)
                                //{
                                //    DataRow drnew = dt.NewRow();
                                //    //drnew[0] = "Q" + item.QuestionId + "_M" + opt.OptionId + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                //    drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                //    dt.Rows.Add(drnew);
                                //}
                                //}
                                if (item.lstMatrixTypeModel != null && item.lstMatrixTypeModel.Count > 0)
                                {
                                    foreach (var col in item.lstMatrixTypeModel)
                                    {
                                        DataRow drnew = dt.NewRow();
                                        //drnew[0] = "Q" + item.QuestionId + "_M" + opt.OptionId + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                        drnew[0] = "Q" + item.QuestionId + "_M" + col.DisplayOrder + "-" + ReplaceHtml.ReplaceText(col.ColumnName);
                                        dt.Rows.Add(drnew);
                                    }
                                }
                            }
                            else if (item.QuestionTypeId == 13)//Kano model matrix type 
                            {
                                if (model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel != null && model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel.Count > 0)
                                {
                                    foreach (var opt in model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel)
                                    {
                                        DataRow drnew = dt.NewRow();
                                        drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Present";
                                        dt.Rows.Add(drnew);
                                        DataRow drnew1 = dt.NewRow();
                                        drnew1[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Absent";
                                        dt.Rows.Add(drnew1);
                                    }
                                }
                            }
                            // Single Punch Matrix Type || Range Matrix Type Question ||
                            // Single punch type answer horizontal position || 
                            // Multiple punch type answer horizontal position || 
                            // Rank order type question || 
                            // KANO Model Matrix type question || Dropdown Matrix type question
                            else if (item.QuestionTypeId > 7 && item.QuestionTypeId != 15)
                            {
                                if (model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel != null && model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel.Count > 0)
                                {
                                    foreach (var opt in model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel)
                                    {
                                        foreach (var column in item.lstMatrixTypeModel)
                                        {
                                            DataRow drnew = dt.NewRow();
                                            drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "_C" + column.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + " " + ReplaceHtml.ReplaceText(column.ColumnName);
                                            dt.Rows.Add(drnew);
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (item.QuestionTypeId == 2 || item.QuestionTypeId == 4 || item.QuestionTypeId == 5 || item.QuestionTypeId == 6)//Multi Punch || Constant Sum || Ranking || Matrix Type
                            {
                                if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
                                {
                                    foreach (var opt in item.lstOptionsModel)
                                    {
                                        DataRow drnew = dt.NewRow();
                                        //drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                        drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                        dt.Rows.Add(drnew);
                                    }
                                }
                            }

                            else if (item.QuestionTypeId == 1 || item.QuestionTypeId == 3 || item.QuestionTypeId == 7 || item.QuestionTypeId == 15)//Single Punch || Range || Simple Text
                            {
                                DataRow drnew = dt.NewRow();
                                //drnew[0] = "Q" + item.DisplayOrder + "_S" + "-" + ReplaceHtml.ReplaceText(item.QuestionTitle);
                                drnew[0] = "Q" + item.QuestionId + "_S" + "-" + ReplaceHtml.ReplaceText(item.QuestionTitle);
                                dt.Rows.Add(drnew);
                            }
                            else if (item.QuestionTypeId == 8 || item.QuestionTypeId == 12)//Single punch matrix type  && Rank order type
                            {
                                //if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
                                //{
                                //foreach (var opt in item.lstOptionsModel)
                                //{
                                //    DataRow drnew = dt.NewRow();
                                //    //drnew[0] = "Q" + item.QuestionId + "_M" + opt.OptionId + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                //    drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                //    dt.Rows.Add(drnew);
                                //}
                                //}
                                if (item.lstMatrixTypeModel != null && item.lstMatrixTypeModel.Count > 0)
                                {
                                    foreach (var col in item.lstMatrixTypeModel)
                                    {
                                        DataRow drnew = dt.NewRow();
                                        //drnew[0] = "Q" + item.QuestionId + "_M" + opt.OptionId + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                        drnew[0] = "Q" + item.QuestionId + "_M" + col.DisplayOrder + "-" + ReplaceHtml.ReplaceText(col.ColumnName);
                                        dt.Rows.Add(drnew);
                                    }
                                }
                            }
                            else if (item.QuestionTypeId == 13)//Kano model matrix type 
                            {
                                if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
                                {
                                    foreach (var opt in item.lstOptionsModel)
                                    {
                                        DataRow drnew = dt.NewRow();
                                        //drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Present";
                                        drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Present";
                                        dt.Rows.Add(drnew);
                                        DataRow drnew1 = dt.NewRow();
                                        //drnew1[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Absent";
                                        drnew1[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Absent";
                                        dt.Rows.Add(drnew1);
                                    }
                                }
                            }
                            // Single Punch Matrix Type || Range Matrix Type Question ||
                            // Single punch type answer horizontal position || 
                            // Multiple punch type answer horizontal position || 
                            // Rank order type question || 
                            // KANO Model Matrix type question || Dropdown Matrix type question
                            else if (item.QuestionTypeId > 7)
                            {
                                if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
                                {
                                    foreach (var opt in item.lstOptionsModel)
                                    {
                                        foreach (var column in item.lstMatrixTypeModel)
                                        {
                                            DataRow drnew = dt.NewRow();
                                            //drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "_C" + column.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + " " + ReplaceHtml.ReplaceText(column.ColumnName);
                                            drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "_C" + column.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + " " + ReplaceHtml.ReplaceText(column.ColumnName);
                                            dt.Rows.Add(drnew);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }


                // Get the Excel Pattern of dt
                dtF = comMethods.GenerateTransposedTable(dt);

                // Get all the reponders
                projectBase.Id = model.projectModel.Id;
                actionResult = adminAction.Responders_Offline_LoadBy_ProjectId(projectBase);

                // Get Responses
                SurveyLoadBase surveyLoadBase = new SurveyLoadBase();
                string json = string.Empty;
                SRV.BaseLayer.ActionResult actionResultResponses = new SRV.BaseLayer.ActionResult();
                int response_id = ResponseCount;            //ss
                foreach (DataRow dr in actionResult.dtResult.Rows)
                {
                    surveyLoadBase.Id = model.projectModel.Id;
                    surveyLoadBase.OfflineResponderId = dr["RespId"] != DBNull.Value ? Convert.ToString(dr["RespId"]) : "";
                    //DataRow dr_dtF = dtF.NewRow();

                    if (String.IsNullOrEmpty(surveyLoadBase.OfflineResponderId)) continue;


                    DataTable responsesDt = new DataTable();

                    if (Array.IndexOf(ExternalResponders, surveyLoadBase.OfflineResponderId) > -1)
                    {
                        if (model.projectModel.lstProductProjectModel != null && model.projectModel.lstProductProjectModel.Count > 0)
                        {
                            foreach (var productModel in model.projectModel.lstProductProjectModel)
                            {
                                surveyLoadBase.ProductId = productModel.Id;
                                actionResultResponses = respondentAction.Response_Offline_LoadBy_ProjectId_RespId(surveyLoadBase);
                                if (actionResultResponses.IsSuccess)
                                {
                                    if (responsesDt.Rows.Count == 0)
                                        responsesDt = actionResultResponses.dtResult.Copy();
                                    else
                                    {
                                        foreach (DataRow drThisRespRow in actionResultResponses.dtResult.Rows)
                                            responsesDt.ImportRow(drThisRespRow);

                                    }
                                }
                            }
                        }
                        else
                        {
                            actionResultResponses = respondentAction.Response_Offline_LoadBy_ProjectId_RespId(surveyLoadBase);
                            if (actionResultResponses.IsSuccess)
                                responsesDt = actionResultResponses.dtResult.Copy();

                        }
                    }


                    //actionResultResponses = respondentAction.Response_Offline_LoadBy_ProjectId_RespId(surveyLoadBase);
                    if (responsesDt.Rows.Count > 0)//actionResultResponses.IsSuccess
                    {
                        //dr_dtF[0] = surveyLoadBase.Email;

                        int count = 1;
                        count = (model.projectModel.ProjectType == "2") ? count + 2 : count;        //ss
                        DataRow dr_dtF = dtF.NewRow();
                        DataRow drTemp = dtF.NewRow();
                        string product_desc = string.Empty;
                        string product_logiId = string.Empty;
                        int oldProductID = responsesDt.Rows[0]["ProductId"] != DBNull.Value ? Convert.ToInt32(responsesDt.Rows[0]["ProductId"]) : 0;
                        foreach (DataRow drResponse in responsesDt.Rows)// actionResultResponses.dtResult.Rows
                        {

                            int optionsCount = 0;

                            int productId = drResponse["ProductId"] != DBNull.Value ? Convert.ToInt32(drResponse["ProductId"]) : 0;
                            if (productId != oldProductID)
                            {
                                count = (model.projectModel.ProjectType == "2") ? 3 : 1;
                                dr_dtF = dtF.NewRow();
                            }

                            //--ss

                            product_desc = drResponse["ProductDescription"] != DBNull.Value ? Convert.ToString(drResponse["ProductDescription"]) : "";
                            product_logiId = drResponse["ProductLogicalId"] != DBNull.Value ? Convert.ToString(drResponse["ProductLogicalId"]) : "";

                            //--ss
                            int questionId = drResponse["QuestionId"] != DBNull.Value ? Convert.ToInt32(drResponse["QuestionId"]) : 0;
                            string answerExpression = drResponse["AnswerExpression"] != DBNull.Value ? Convert.ToString(drResponse["AnswerExpression"]) : "";
                            int questionTypeId = drResponse["QuestionTypeId"] != DBNull.Value ? Convert.ToInt32(drResponse["QuestionTypeId"]) : 1;
                            int PipedFrom = drResponse["PipedFrom"] != DBNull.Value ? Convert.ToInt32(drResponse["PipedFrom"]) : 0;
                            //dr_dtF[0] = drResponse;

                            // Get Total options count for this question
                            questionBase.Id = Convert.ToInt32(questionId);
                            SRV.BaseLayer.ActionResult actionResultAnswersTemp = new SRV.BaseLayer.ActionResult();
                            actionResultAnswersTemp = adminAction.Option_LoadAllById(questionBase);
                            if (actionResultAnswersTemp.dtResult != null && actionResultAnswersTemp.dtResult.Rows.Count > 0)
                            {
                                optionsCount = actionResultAnswersTemp.dtResult.Rows.Count;
                            }

                            if (questionTypeId == 2 || questionTypeId == 4 || questionTypeId == 5 || questionTypeId == 6)
                            {
                                string[] allAnswers = answerExpression.Split('|');
                                if (allAnswers.Length > 0)
                                {
                                    if (questionTypeId == 2)// Multi Punch
                                    {
                                        string allRec = string.Empty;
                                        //List<OptionsModel> matrixAns = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;


                                        List<OptionsModel> matrixAns = new List<OptionsModel>();
                                        if (PipedFrom > 0 && !drResponse["QuestionTitle"].ToString().Contains("{{Q}}"))
                                        {
                                            matrixAns = model.lstQuestionModel.Where(v => v.QuestionId == PipedFrom).FirstOrDefault().lstOptionsModel;
                                        }
                                        else
                                        {
                                            matrixAns = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                        }

                                        foreach (var item in matrixAns)
                                        {
                                            string matchedId = string.Empty;
                                            foreach (string ans in allAnswers)
                                            {
                                                if (Convert.ToString(item.OptionId) == ans)
                                                {
                                                    matchedId = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            allRec += matchedId + "|";
                                        }

                                        string[] allAnswersMat = allRec.Substring(0, allRec.Length - 1).Split('|');
                                        foreach (string ans in allAnswersMat)
                                        {

                                            if (count <= dtF.Columns.Count - 1)
                                            {
                                                if ((Regex.Replace(dtF.Columns[count].ToString().Split('-')[0].Split('_')[1], @"[^\d]", String.Empty)) == ans)
                                                {
                                                    dr_dtF[count] = "1";// ans;
                                                    count++;
                                                    optionsCount--;
                                                }
                                                else
                                                {
                                                    dr_dtF[count] = "2";
                                                    count++;
                                                    optionsCount--;
                                                }
                                            }
                                        }
                                        //count = count + optionsCount;
                                    }
                                    else if (questionTypeId == 6) // matrix Type
                                    {
                                        foreach (string ans in allAnswers)
                                        {
                                            dr_dtF[count] = (ans.ToLower().IndexOf("yes") != -1) ? "1" : "2";
                                            count++;
                                            optionsCount--;
                                        }
                                        count = count + optionsCount;
                                    }
                                    else
                                    {
                                        foreach (string ans in allAnswers)
                                        {
                                            dr_dtF[count] = ans;
                                            count++;
                                            optionsCount--;
                                        }
                                        count = count + optionsCount;
                                    }
                                }
                            }
                            else if (questionTypeId == 1) // Single Punch Answers
                            {
                                //List<OptionsModel> SinglePunchAns = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;

                                List<OptionsModel> SinglePunchAns = new List<OptionsModel>();
                                if (PipedFrom > 0 && !drResponse["QuestionTitle"].ToString().Contains("{{Q}}"))
                                {
                                    SinglePunchAns = model.lstQuestionModel.Where(v => v.QuestionId == PipedFrom).FirstOrDefault().lstOptionsModel;
                                }
                                else
                                {
                                    SinglePunchAns = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                }
                                int ansIndex = 0;
                                foreach (var item in SinglePunchAns)
                                {
                                    ansIndex++;
                                    if (Convert.ToString(item.OptionId) == answerExpression)
                                    {
                                        break;
                                    }
                                }

                                dr_dtF[count] = ansIndex;//answerExpression;
                                count++;
                                optionsCount--;
                            }
                            else if (questionTypeId == 8 || questionTypeId == 12) // single punch matrix answers
                            {
                                List<OptionsModel> optionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                List<MatrixTypeModel> columnDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstMatrixTypeModel;
                                int qustionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).Select(v => v.DisplayOrder).FirstOrDefault();
                                string[] allAnswers = answerExpression.Split('|');

                                List<string> lstColumnOrdinals = new List<string>();

                                string formattedAnswer = string.Empty;
                                string answerValue = string.Empty;
                                if (allAnswers.Length > 0)
                                {
                                    foreach (var ansExpression in allAnswers)
                                    {
                                        if (!string.IsNullOrEmpty(ansExpression))
                                        {
                                            string displaycol = string.Empty;
                                            string displayopt = string.Empty;
                                            foreach (var item in optionDisplayOrder)
                                            {

                                                if (Convert.ToString(item.OptionId) == ansExpression.Split('~')[0])
                                                {
                                                    displaycol = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            foreach (var item in columnDisplayOrder)
                                            {
                                                if (Convert.ToString(item.Id) == ansExpression.Split('~')[1])
                                                {
                                                    displayopt = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            //  formattedAnswer = "Q" + questionId + "_M" + displaycol + "#" + displayopt;
                                            formattedAnswer = "Q" + questionId + "_M" + displayopt + "#" + displaycol;
                                            foreach (DataColumn column in dtF.Columns)
                                            {
                                                string[] columnHeader = column.ColumnName.Split('-');
                                                string[] answer = columnHeader[0].Split('_');
                                                if (columnHeader.Length > 1)
                                                {
                                                    string answerInHeader = answer[0] + "_" + answer[1];
                                                    string[] formattedAnswerArr = formattedAnswer.Split('#');
                                                    if (formattedAnswerArr[0] == answerInHeader)
                                                    {
                                                        int index = dr_dtF.Table.Columns[column.ColumnName].Ordinal;
                                                        lstColumnOrdinals.Add(index.ToString());
                                                        count++;
                                                        dr_dtF[index] = formattedAnswerArr[1];
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            else if (questionTypeId == 13) // Kano model matrix type
                            {
                                List<OptionsModel> optionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                List<MatrixTypeModel> columnDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstMatrixTypeModel;
                                int qustionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).Select(v => v.DisplayOrder).FirstOrDefault();
                                string[] allAnswers = answerExpression.Split('|');

                                List<string> lstColumnOrdinals = new List<string>();

                                string formattedAnswer = string.Empty;
                                string answerValue = string.Empty;
                                if (allAnswers.Length > 1)
                                {
                                    int repeatId = 0;
                                    foreach (var ansExpression in allAnswers)
                                    {
                                        string displaycol = string.Empty;
                                        string displayopt = string.Empty;
                                        string displayText = string.Empty;
                                        string text = string.Empty;
                                        if (repeatId == Convert.ToInt32(ansExpression.Split('~')[0]))
                                        {
                                            text = "_Absent";
                                            repeatId = 0;
                                        }
                                        else
                                        {
                                            text = "_Present";
                                        }
                                        foreach (var item in optionDisplayOrder)
                                        {
                                            if (Convert.ToString(item.OptionId) == ansExpression.Split('~')[0])
                                            {
                                                displayText = Convert.ToString(item.Option) + text;
                                                displaycol = Convert.ToString(item.DisplayOrder);
                                                repeatId = Convert.ToInt32(ansExpression.Split('~')[0]);
                                                break;
                                            }
                                        }
                                        foreach (var item in columnDisplayOrder)
                                        {
                                            if (Convert.ToString(item.Id) == ansExpression.Split('~')[1])
                                            {
                                                displayopt = Convert.ToString(item.DisplayOrder);
                                                break;
                                            }
                                        }
                                        //formattedAnswer = "Q" + qustionDisplayOrder + "_M" + displaycol + "-" + displayText + "#" + displayopt;
                                        formattedAnswer = "Q" + questionId + "_M" + displaycol + "-" + displayText + "#" + displayopt;
                                        foreach (DataColumn column in dtF.Columns)
                                        {

                                            string[] columnHeader = column.ColumnName.Split('-');
                                            string[] answer = columnHeader[0].Split('_');
                                            if (columnHeader.Length > 1)
                                            {
                                                string answerInHeader = answer[0] + "_" + answer[1] + "-" + columnHeader[1];
                                                string[] formattedAnswerArr = formattedAnswer.Split('#');
                                                if (Regex.Replace(formattedAnswerArr[0], @"\s+", "", RegexOptions.Multiline) == Regex.Replace(answerInHeader, @"\s+", "", RegexOptions.Multiline))
                                                {
                                                    int index = dr_dtF.Table.Columns[column.ColumnName].Ordinal;
                                                    lstColumnOrdinals.Add(index.ToString());
                                                    count++;
                                                    dr_dtF[index] = formattedAnswerArr[1];
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            else if (questionTypeId > 7 && questionTypeId != 15)
                            {
                                List<OptionsModel> optionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                List<MatrixTypeModel> columnDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstMatrixTypeModel;
                                List<DDLMatrixTypeModel> dropDownDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstDDlMatrixModel;
                                //count = count + (optionDisplayOrder.Count * columnDisplayOrder.Count);
                                int optDisplayOrder = 0;
                                if (optionDisplayOrder != null)
                                {
                                    optDisplayOrder = optionDisplayOrder.Count;
                                }
                                int colDisplayOrder = 0;
                                if (columnDisplayOrder != null)
                                {
                                    colDisplayOrder = columnDisplayOrder.Count;
                                }
                                int qustionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).Select(v => v.DisplayOrder).FirstOrDefault();
                                string[] allAnswers = answerExpression.Split('|');

                                List<string> lstColumnOrdinals = new List<string>();

                                string formattedAnswer = string.Empty;
                                string answerValue = string.Empty;
                                int DDlDisplayOrder = 0;
                                if (allAnswers.Length > 1)
                                {
                                    foreach (var ansExpression in allAnswers)
                                    {
                                        string displaycol = string.Empty;
                                        string displayopt = string.Empty;

                                        if (questionTypeId == 9 || questionTypeId == 14)
                                        {
                                            foreach (var item in optionDisplayOrder)
                                            {
                                                if (Convert.ToString(item.OptionId) == ansExpression.Split('~')[0].Split('*')[0])
                                                {
                                                    displaycol = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            foreach (var item in columnDisplayOrder)
                                            {
                                                if (Convert.ToString(item.Id) == ansExpression.Split('~')[1])
                                                {
                                                    displayopt = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            //formattedAnswer = "Q" + qustionDisplayOrder + "_M" + displaycol + "_C" + displayopt;
                                            formattedAnswer = "Q" + questionId + "_M" + displaycol + "_C" + displayopt;
                                            answerValue = ansExpression.Split('~')[0].Split('*')[1];
                                            if (questionTypeId == 14)
                                                DDlDisplayOrder = dropDownDisplayOrder.Where(l => l.Id == Convert.ToInt32(answerValue)).FirstOrDefault().DDLDisplayOrder;
                                        }
                                        else
                                        {
                                            foreach (var item in optionDisplayOrder)
                                            {
                                                if (Convert.ToString(item.OptionId) == ansExpression.Split('~')[0])
                                                {
                                                    displaycol = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            foreach (var item in columnDisplayOrder)
                                            {
                                                if (Convert.ToString(item.Id) == ansExpression.Split('~')[1])
                                                {
                                                    displayopt = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            //formattedAnswer = "Q" + qustionDisplayOrder + "_M" + displaycol + "_C" + displayopt;
                                            formattedAnswer = "Q" + questionId + "_M" + displaycol + "_C" + displayopt;
                                        }
                                        foreach (DataColumn column in dtF.Columns)
                                        {
                                            string[] columnHeader = column.ColumnName.Split('-');

                                            if (columnHeader.Length > 1)
                                            {
                                                string[] answer = columnHeader[0].Split('_');

                                                if (answer.Length > 2)
                                                {
                                                    string answerInHeader = answer[0] + "_" + answer[1] + "_" + answer[2];

                                                    if (formattedAnswer == answerInHeader)
                                                    {
                                                        int index = dr_dtF.Table.Columns[column.ColumnName].Ordinal;
                                                        lstColumnOrdinals.Add(index.ToString());
                                                        //count++;
                                                        if (questionTypeId == 9)
                                                        {
                                                            dr_dtF[index] = answerValue;
                                                        }
                                                        else if (questionTypeId == 14)
                                                        {
                                                            dr_dtF[index] = DDlDisplayOrder;
                                                        }
                                                        else
                                                        {
                                                            dr_dtF[index] = "1";
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }

                                // Fill All the Empty cells with 2

                                for (int i = 0; i <= dtF.Columns.Count - 1; i++)
                                    if (String.IsNullOrEmpty(Convert.ToString(dr_dtF[i])))
                                        dr_dtF[i] = "";
                            }

                            else
                            {
                                dr_dtF[count] = answerExpression;
                                count++;
                                optionsCount--;
                            }



                            if (productId != oldProductID)
                            {
                                dtF.Rows.Add(drTemp);
                                drTemp[0] = response_id;
                                oldProductID = productId;
                            }

                            drTemp = dr_dtF;
                            //---ss
                            if (model.projectModel.ProjectType == "2")
                            {
                                drTemp[2] = product_desc;
                                drTemp[1] = product_logiId;
                            }
                            //---ss




                        }


                        dr_dtF[1] = dr["RespId"] != DBNull.Value ? Convert.ToString(dr["RespId"]) : "";

                        dtF.Rows.Add(dr_dtF);
                        drTemp[0] = response_id;
                        //---ss
                        if (model.projectModel.ProjectType == "2")
                        {
                            dr_dtF[2] = product_desc;
                            dr_dtF[1] = product_logiId;
                        }
                        response_id++;
                        //---ss


                    }
                }
            }
            catch (Exception ex)
            {
                //ErrorReporting.WebApplicationError(ex);
            }
            return dtF;
        }
        #endregion

        // Segmentation

        #region GetReportDataTable

        [WebMethod]
        private void GetReportDataTableOnlineSegmentation(string Id, int proId)
        {

            RespondentController respondent = new RespondentController();
            SurveyResponseModel model = new SurveyResponseModel();
            DataTable dtF = new DataTable();
            DataTable newData = new DataTable();
            try
            {
                DataTable dt = new DataTable();

                string projectGuid = string.Empty;
                projectGuid = Id;
                dt.Columns.Add("Resp_Id", typeof(string));

                model = respondent.GetProjectPreview(projectGuid);

                //--ss
                if (model.projectModel.ProjectType == "2")
                {
                    DataRow drnew = dt.NewRow();
                    drnew[0] = "Product_LogicalId";
                    dt.Rows.Add(drnew);
                    DataRow drnew1 = dt.NewRow();
                    drnew1[0] = "Product_Name";
                    dt.Rows.Add(drnew1);
                }
                //--

                // Load all the questions of this project
                List<QuestionList> questionsList = new List<QuestionList>();
                questionBase.ProjectId = Convert.ToInt32(model.projectModel.Id);
                actionResult = adminAction.QuestionsLoadAll_ByProjectId(questionBase);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    foreach (DataRow dr in actionResult.dtResult.Rows)
                    {
                        questionsList.Add(new QuestionList
                        {
                            id = dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0,
                            questionTypeId = dr["QuestionTypeId"] != DBNull.Value ? Convert.ToInt32(dr["QuestionTypeId"]) : 0
                        });
                    }
                }

                string fileName = Guid.NewGuid().ToString().Substring(0, 5) + "_" + DateTime.Now.ToString("MM-dd-yyyy") + "_" + model.projectModel.ProjectName.Replace(" ", "");

                if (model.lstQuestionModel != null && model.lstQuestionModel.Count > 0)
                {
                    foreach (var item in model.lstQuestionModel)
                    {
                        if (item.PipedFrom > 0 && (!item.QuestionTitle.Contains("{{Q}}")))
                        {
                            if (item.QuestionTypeId == 2 || item.QuestionTypeId == 4 || item.QuestionTypeId == 5 || item.QuestionTypeId == 6)//Multi Punch || Constant Sum || Ranking || Matrix Type
                            {
                                if (model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel != null && model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel.Count > 0)
                                {
                                    foreach (var opt in model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel)
                                    {
                                        DataRow drnew = dt.NewRow();
                                        drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                        dt.Rows.Add(drnew);
                                    }
                                }
                            }

                            else if (item.QuestionTypeId == 1 || item.QuestionTypeId == 3 || item.QuestionTypeId == 7 || item.QuestionTypeId == 15)//Single Punch || Range || Simple Text
                            {
                                DataRow drnew = dt.NewRow();
                                drnew[0] = "Q" + item.QuestionId + "_S" + "-" + ReplaceHtml.ReplaceText(item.QuestionTitle);
                                dt.Rows.Add(drnew);
                            }
                            else if (item.QuestionTypeId == 8 || item.QuestionTypeId == 12)//Single punch matrix type  && Rank order type
                            {
                                //if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
                                //{
                                //foreach (var opt in item.lstOptionsModel)
                                //{
                                //    DataRow drnew = dt.NewRow();
                                //    //drnew[0] = "Q" + item.QuestionId + "_M" + opt.OptionId + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                //    drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                //    dt.Rows.Add(drnew);
                                //}
                                //}
                                if (item.lstMatrixTypeModel != null && item.lstMatrixTypeModel.Count > 0)
                                {
                                    foreach (var col in item.lstMatrixTypeModel)
                                    {
                                        DataRow drnew = dt.NewRow();
                                        //drnew[0] = "Q" + item.QuestionId + "_M" + opt.OptionId + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                        drnew[0] = "Q" + item.QuestionId + "_M" + col.DisplayOrder + "-" + ReplaceHtml.ReplaceText(col.ColumnName);
                                        dt.Rows.Add(drnew);
                                    }
                                }
                            }
                            else if (item.QuestionTypeId == 13)//Kano model matrix type 
                            {
                                if (model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel != null && model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel.Count > 0)
                                {
                                    foreach (var opt in model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel)
                                    {
                                        DataRow drnew = dt.NewRow();
                                        drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Present";
                                        dt.Rows.Add(drnew);
                                        DataRow drnew1 = dt.NewRow();
                                        drnew1[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Absent";
                                        dt.Rows.Add(drnew1);
                                    }
                                }
                            }
                            // Single Punch Matrix Type || Range Matrix Type Question ||
                            // Single punch type answer horizontal position || 
                            // Multiple punch type answer horizontal position || 
                            // Rank order type question || 
                            // KANO Model Matrix type question || Dropdown Matrix type question
                            else if (item.QuestionTypeId > 7 && item.QuestionTypeId != 15)
                            {
                                if (model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel != null && model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel.Count > 0)
                                {
                                    foreach (var opt in model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel)
                                    {
                                        foreach (var column in item.lstMatrixTypeModel)
                                        {
                                            DataRow drnew = dt.NewRow();
                                            drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "_C" + column.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + " " + ReplaceHtml.ReplaceText(column.ColumnName);
                                            dt.Rows.Add(drnew);
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (item.QuestionTypeId == 2 || item.QuestionTypeId == 4 || item.QuestionTypeId == 5 || item.QuestionTypeId == 6)//Multi Punch || Constant Sum || Ranking || Matrix Type
                            {
                                if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
                                {
                                    foreach (var opt in item.lstOptionsModel)
                                    {
                                        DataRow drnew = dt.NewRow();
                                        //drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                        drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                        dt.Rows.Add(drnew);
                                    }
                                }
                            }

                            else if (item.QuestionTypeId == 1 || item.QuestionTypeId == 3 || item.QuestionTypeId == 7 || item.QuestionTypeId == 15)//Single Punch || Range || Simple Text
                            {
                                DataRow drnew = dt.NewRow();
                                //drnew[0] = "Q" + item.DisplayOrder + "_S" + "-" + ReplaceHtml.ReplaceText(item.QuestionTitle);
                                drnew[0] = "Q" + item.QuestionId + "_S" + "-" + ReplaceHtml.ReplaceText(item.QuestionTitle);
                                dt.Rows.Add(drnew);
                            }
                            else if (item.QuestionTypeId == 8 || item.QuestionTypeId == 12)//Single punch matrix type  && Rank order type
                            {
                                //if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
                                //{
                                //foreach (var opt in item.lstOptionsModel)
                                //{
                                //    DataRow drnew = dt.NewRow();
                                //    //drnew[0] = "Q" + item.QuestionId + "_M" + opt.OptionId + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                //    drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                //    dt.Rows.Add(drnew);
                                //}
                                //}
                                if (item.lstMatrixTypeModel != null && item.lstMatrixTypeModel.Count > 0)
                                {
                                    foreach (var col in item.lstMatrixTypeModel)
                                    {
                                        DataRow drnew = dt.NewRow();
                                        //drnew[0] = "Q" + item.QuestionId + "_M" + opt.OptionId + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                        drnew[0] = "Q" + item.QuestionId + "_M" + col.DisplayOrder + "-" + ReplaceHtml.ReplaceText(col.ColumnName);
                                        dt.Rows.Add(drnew);
                                    }
                                }
                            }
                            else if (item.QuestionTypeId == 13)//Kano model matrix type 
                            {
                                if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
                                {
                                    foreach (var opt in item.lstOptionsModel)
                                    {
                                        DataRow drnew = dt.NewRow();
                                        //drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Present";
                                        drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Present";
                                        dt.Rows.Add(drnew);
                                        DataRow drnew1 = dt.NewRow();
                                        //drnew1[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Absent";
                                        drnew1[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Absent";
                                        dt.Rows.Add(drnew1);
                                    }
                                }
                            }
                            // Single Punch Matrix Type || Range Matrix Type Question ||
                            // Single punch type answer horizontal position || 
                            // Multiple punch type answer horizontal position || 
                            // Rank order type question || 
                            // KANO Model Matrix type question || Dropdown Matrix type question
                            else if (item.QuestionTypeId > 7)
                            {
                                if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
                                {
                                    foreach (var opt in item.lstOptionsModel)
                                    {
                                        foreach (var column in item.lstMatrixTypeModel)
                                        {
                                            DataRow drnew = dt.NewRow();
                                            //drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "_C" + column.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + " " + ReplaceHtml.ReplaceText(column.ColumnName);
                                            drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "_C" + column.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + " " + ReplaceHtml.ReplaceText(column.ColumnName);
                                            dt.Rows.Add(drnew);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                // Get the Excel Pattern of dt
                dtF = comMethods.GenerateTransposedTable(dt);

                // Get all the reponders
                projectBase.Id = model.projectModel.Id;
                actionResult = adminAction.CompleteRespondersLoad_ByProjectId(projectBase);
                List<RespondersModel> lstResponders = new List<RespondersModel>();
                foreach (DataRow dr in actionResult.dtResult.Rows)
                {
                    lstResponders.Add(new RespondersModel
                    {

                        InvitationGuid = dr["InvitationGuid"] != DBNull.Value ? Convert.ToString(dr["InvitationGuid"]) : "",
                        Email = dr["Email"] != DBNull.Value ? Convert.ToString(dr["Email"]) : "",

                    });
                }
                IEnumerable<RespondersModel> filteredList = lstResponders
                 .GroupBy(customer => customer.InvitationGuid).Select(group => group.First());
                // Get Responses
                SurveyLoadBase surveyLoadBase = new SurveyLoadBase();
                string json = string.Empty;
                SRV.BaseLayer.ActionResult actionResultResponses = new SRV.BaseLayer.ActionResult();
                int response_id = 1;            //ss
                foreach (var responder in filteredList)
                {
                    DataTable responsesDt = new DataTable();
                    surveyLoadBase.Id = model.projectModel.Id;
                    surveyLoadBase.Email = (!string.IsNullOrEmpty(responder.Email) ? Convert.ToString(responder.Email) : "");
                    surveyLoadBase.ExternalResponderId = (!string.IsNullOrEmpty(responder.InvitationGuid) ? Convert.ToString(responder.InvitationGuid) : "");
                    string tempOnlineResponderId = (!string.IsNullOrEmpty(responder.InvitationGuid) ? Convert.ToString(responder.InvitationGuid) : "");
                    if (model.projectModel.lstProductProjectModel != null && model.projectModel.lstProductProjectModel.Count > 0)
                    {
                        foreach (var productModel in model.projectModel.lstProductProjectModel)
                        {
                            surveyLoadBase.ProductId = productModel.Id;
                            actionResultResponses = respondentAction.Response_LoadByProjectId(surveyLoadBase);
                            if (actionResultResponses.IsSuccess)
                            {
                                if (responsesDt.Rows.Count == 0)
                                    responsesDt = actionResultResponses.dtResult.Copy();
                                else
                                {
                                    foreach (DataRow drThisRespRow in actionResultResponses.dtResult.Rows)
                                        responsesDt.ImportRow(drThisRespRow);
                                }
                            }
                        }
                    }
                    else
                    {
                        actionResultResponses = respondentAction.Response_LoadByProjectId(surveyLoadBase);
                        if (actionResultResponses.IsSuccess)
                            responsesDt = actionResultResponses.dtResult.Copy();

                    }

                    // surveyLoadBase.OfflineResponderId = dr["RespId"] != DBNull.Value ? Convert.ToString(dr["RespId"]) : "";
                    //DataRow dr_dtF = dtF.NewRow();
                    // if (String.IsNullOrEmpty(surveyLoadBase.Email)) continue;

                    //actionResultResponses = respondentAction.Response_LoadByProjectId(surveyLoadBase);
                    if (responsesDt.Rows.Count > 0)//actionResultResponses.IsSuccess
                    {
                        //dr_dtF[0] = surveyLoadBase.Email;

                        int count = 1;
                        count = (model.projectModel.ProjectType == "2") ? count + 2 : count;        //ss
                        DataRow dr_dtF = dtF.NewRow();
                        DataRow drTemp = dtF.NewRow();
                        string product_desc = string.Empty;
                        string product_logiId = string.Empty;
                        int oldProductID = responsesDt.Rows[0]["ProductId"] != DBNull.Value ? Convert.ToInt32(responsesDt.Rows[0]["ProductId"]) : 0;
                        foreach (DataRow drResponse in responsesDt.Rows)// actionResultResponses.dtResult.Rows
                        {

                            int optionsCount = 0;
                            // response_id = drResponse["ReponderId"] != DBNull.Value ? (responsesDt.Rows[0]["ReponderId"]).ToString() : "";
                            int productId = drResponse["ProductId"] != DBNull.Value ? Convert.ToInt32(drResponse["ProductId"]) : 0;
                            if (productId != oldProductID)
                            {
                                count = (model.projectModel.ProjectType == "2") ? 3 : 1;
                                dr_dtF = dtF.NewRow();
                            }

                            //--ss

                            product_desc = drResponse["ProductDescription"] != DBNull.Value ? Convert.ToString(drResponse["ProductDescription"]) : "";
                            product_logiId = drResponse["ProductLogicalId"] != DBNull.Value ? Convert.ToString(drResponse["ProductLogicalId"]) : "";

                            //--ss
                            int questionId = drResponse["QuestionId"] != DBNull.Value ? Convert.ToInt32(drResponse["QuestionId"]) : 0;
                            string answerExpression = drResponse["AnswerExpression"] != DBNull.Value ? Convert.ToString(drResponse["AnswerExpression"]) : "";
                            int questionTypeId = drResponse["QuestionTypeId"] != DBNull.Value ? Convert.ToInt32(drResponse["QuestionTypeId"]) : 1;
                            int PipedFrom = drResponse["PipedFrom"] != DBNull.Value ? Convert.ToInt32(drResponse["PipedFrom"]) : 0;
                            //dr_dtF[0] = drResponse;

                            // Get Total options count for this question
                            questionBase.Id = Convert.ToInt32(questionId);
                            SRV.BaseLayer.ActionResult actionResultAnswersTemp = new SRV.BaseLayer.ActionResult();
                            actionResultAnswersTemp = adminAction.Option_LoadAllById(questionBase);
                            if (actionResultAnswersTemp.dtResult != null && actionResultAnswersTemp.dtResult.Rows.Count > 0)
                            {
                                optionsCount = actionResultAnswersTemp.dtResult.Rows.Count;
                            }

                            if (questionTypeId == 2 || questionTypeId == 4 || questionTypeId == 5 || questionTypeId == 6)
                            {
                                string[] allAnswers = answerExpression.Split('|');
                                if (allAnswers.Length > 0)
                                {
                                    if (questionTypeId == 2)// Multi Punch
                                    {
                                        string allRec = string.Empty;
                                        List<OptionsModel> matrixAns = new List<OptionsModel>();
                                        if (PipedFrom > 0 && !drResponse["QuestionTitle"].ToString().Contains("{{Q}}"))
                                        {
                                            matrixAns = model.lstQuestionModel.Where(v => v.QuestionId == PipedFrom).FirstOrDefault().lstOptionsModel;
                                        }
                                        else
                                        {
                                            matrixAns = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                        }

                                        foreach (var item in matrixAns)
                                        {
                                            string matchedId = string.Empty;
                                            foreach (string ans in allAnswers)
                                            {
                                                if (Convert.ToString(item.OptionId) == ans)
                                                {
                                                    matchedId = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            allRec += matchedId + "|";
                                        }

                                        string[] allAnswersMat = allRec.Substring(0, allRec.Length - 1).Split('|');
                                        foreach (string ans in allAnswersMat)
                                        {

                                            if (count <= dtF.Columns.Count - 1)
                                            {
                                                if ((Regex.Replace(dtF.Columns[count].ToString().Split('-')[0].Split('_')[1], @"[^\d]", String.Empty)) == ans)
                                                {
                                                    dr_dtF[count] = "1";// ans;
                                                    count++;
                                                    optionsCount--;
                                                }
                                                else
                                                {
                                                    dr_dtF[count] = "2";
                                                    count++;
                                                    optionsCount--;
                                                }
                                            }
                                        }
                                        //count = count + optionsCount;
                                    }
                                    else if (questionTypeId == 6) // matrix Type
                                    {
                                        foreach (string ans in allAnswers)
                                        {
                                            dr_dtF[count] = (ans.ToLower().IndexOf("yes") != -1) ? "1" : "2";
                                            count++;
                                            optionsCount--;
                                        }
                                        count = count + optionsCount;
                                    }
                                    else
                                    {
                                        foreach (string ans in allAnswers)
                                        {
                                            dr_dtF[count] = ans;
                                            count++;
                                            optionsCount--;
                                        }
                                        count = count + optionsCount;
                                    }
                                }
                            }
                            else if (questionTypeId == 1) // Single Punch Answers
                            {
                                //List<OptionsModel> SinglePunchAns = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                List<OptionsModel> SinglePunchAns = new List<OptionsModel>();
                                if (PipedFrom > 0 && !drResponse["QuestionTitle"].ToString().Contains("{{Q}}"))
                                {
                                    SinglePunchAns = model.lstQuestionModel.Where(v => v.QuestionId == PipedFrom).FirstOrDefault().lstOptionsModel;
                                }
                                else
                                {
                                    SinglePunchAns = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                }
                                int ansIndex = 0;
                                foreach (var item in SinglePunchAns)
                                {
                                    ansIndex++;
                                    if (Convert.ToString(item.OptionId) == answerExpression)
                                    {
                                        break;
                                    }
                                }

                                dr_dtF[count] = ansIndex;//answerExpression;
                                count++;
                                optionsCount--;
                            }
                            else if (questionTypeId == 8 || questionTypeId == 12) // single punch matrix answers
                            {
                                List<OptionsModel> optionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                List<MatrixTypeModel> columnDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstMatrixTypeModel;
                                int qustionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).Select(v => v.DisplayOrder).FirstOrDefault();
                                string[] allAnswers = answerExpression.Split('|');

                                List<string> lstColumnOrdinals = new List<string>();

                                string formattedAnswer = string.Empty;
                                string answerValue = string.Empty;
                                if (allAnswers.Length > 0)
                                {
                                    foreach (var ansExpression in allAnswers)
                                    {
                                        string displaycol = string.Empty;
                                        string displayopt = string.Empty;
                                        foreach (var item in optionDisplayOrder)
                                        {

                                            if (Convert.ToString(item.OptionId) == ansExpression.Split('~')[0])
                                            {
                                                displaycol = Convert.ToString(item.DisplayOrder);
                                                break;
                                            }
                                        }
                                        foreach (var item in columnDisplayOrder)
                                        {
                                            if (Convert.ToString(item.Id) == ansExpression.Split('~')[1])
                                            {
                                                displayopt = Convert.ToString(item.DisplayOrder);
                                                break;
                                            }
                                        }
                                        //  formattedAnswer = "Q" + questionId + "_M" + displaycol + "#" + displayopt;
                                        formattedAnswer = "Q" + questionId + "_M" + displayopt + "#" + displaycol;
                                        foreach (DataColumn column in dtF.Columns)
                                        {
                                            string[] columnHeader = column.ColumnName.Split('-');
                                            string[] answer = columnHeader[0].Split('_');
                                            if (columnHeader.Length > 1)
                                            {
                                                string answerInHeader = answer[0] + "_" + answer[1];
                                                string[] formattedAnswerArr = formattedAnswer.Split('#');
                                                if (formattedAnswerArr[0] == answerInHeader)
                                                {
                                                    int index = dr_dtF.Table.Columns[column.ColumnName].Ordinal;
                                                    lstColumnOrdinals.Add(index.ToString());
                                                    count++;
                                                    dr_dtF[index] = formattedAnswerArr[1];
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            else if (questionTypeId == 13) // Kano model matrix type
                            {
                                List<OptionsModel> optionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                List<MatrixTypeModel> columnDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstMatrixTypeModel;
                                int qustionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).Select(v => v.DisplayOrder).FirstOrDefault();
                                string[] allAnswers = answerExpression.Split('|');

                                List<string> lstColumnOrdinals = new List<string>();

                                string formattedAnswer = string.Empty;
                                string answerValue = string.Empty;
                                if (allAnswers.Length > 1)
                                {
                                    int repeatId = 0;
                                    foreach (var ansExpression in allAnswers)
                                    {
                                        string displaycol = string.Empty;
                                        string displayopt = string.Empty;
                                        string displayText = string.Empty;
                                        string text = string.Empty;
                                        if (repeatId == Convert.ToInt32(ansExpression.Split('~')[0]))
                                        {
                                            text = "_Absent";
                                            repeatId = 0;
                                        }
                                        else
                                        {
                                            text = "_Present";
                                        }
                                        foreach (var item in optionDisplayOrder)
                                        {
                                            if (Convert.ToString(item.OptionId) == ansExpression.Split('~')[0])
                                            {
                                                displayText = Convert.ToString(item.Option) + text;
                                                displaycol = Convert.ToString(item.DisplayOrder);
                                                repeatId = Convert.ToInt32(ansExpression.Split('~')[0]);
                                                break;
                                            }
                                        }
                                        foreach (var item in columnDisplayOrder)
                                        {
                                            if (Convert.ToString(item.Id) == ansExpression.Split('~')[1])
                                            {
                                                displayopt = Convert.ToString(item.DisplayOrder);
                                                break;
                                            }
                                        }
                                        //formattedAnswer = "Q" + qustionDisplayOrder + "_M" + displaycol + "-" + displayText + "#" + displayopt;
                                        formattedAnswer = "Q" + questionId + "_M" + displaycol + "-" + displayText + "#" + displayopt;
                                        foreach (DataColumn column in dtF.Columns)
                                        {

                                            string[] columnHeader = column.ColumnName.Split('-');
                                            string[] answer = columnHeader[0].Split('_');
                                            if (columnHeader.Length > 1)
                                            {
                                                string answerInHeader = answer[0] + "_" + answer[1] + "-" + columnHeader[1];
                                                string[] formattedAnswerArr = formattedAnswer.Split('#');
                                                if (Regex.Replace(formattedAnswerArr[0], @"\s+", "", RegexOptions.Multiline) == Regex.Replace(answerInHeader, @"\s+", "", RegexOptions.Multiline))
                                                {
                                                    int index = dr_dtF.Table.Columns[column.ColumnName].Ordinal;
                                                    lstColumnOrdinals.Add(index.ToString());
                                                    count++;
                                                    dr_dtF[index] = formattedAnswerArr[1];
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            else if (questionTypeId > 7 && questionTypeId != 15)
                            {
                                List<OptionsModel> optionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                List<MatrixTypeModel> columnDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstMatrixTypeModel;
                                List<DDLMatrixTypeModel> dropDownDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstDDlMatrixModel;
                                // count = count + (optionDisplayOrder.Count * columnDisplayOrder.Count);

                                int optDisplayOrder = 0;
                                if (optionDisplayOrder != null)
                                {
                                    optDisplayOrder = optionDisplayOrder.Count;
                                }
                                int colDisplayOrder = 0;
                                if (columnDisplayOrder != null)
                                {
                                    colDisplayOrder = columnDisplayOrder.Count;
                                }
                                count = count + (optDisplayOrder * colDisplayOrder);

                                int qustionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).Select(v => v.DisplayOrder).FirstOrDefault();
                                string[] allAnswers = answerExpression.Split('|');

                                List<string> lstColumnOrdinals = new List<string>();

                                string formattedAnswer = string.Empty;
                                string answerValue = string.Empty;
                                int DDlDisplayOrder = 0;
                                if (allAnswers.Length > 1)
                                {
                                    foreach (var ansExpression in allAnswers)
                                    {
                                        string displaycol = string.Empty;
                                        string displayopt = string.Empty;

                                        if (questionTypeId == 9 || questionTypeId == 14)
                                        {
                                            foreach (var item in optionDisplayOrder)
                                            {
                                                if (Convert.ToString(item.OptionId) == ansExpression.Split('~')[0].Split('*')[0])
                                                {
                                                    displaycol = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            foreach (var item in columnDisplayOrder)
                                            {
                                                if (Convert.ToString(item.Id) == ansExpression.Split('~')[1])
                                                {
                                                    displayopt = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            //formattedAnswer = "Q" + qustionDisplayOrder + "_M" + displaycol + "_C" + displayopt;
                                            formattedAnswer = "Q" + questionId + "_M" + displaycol + "_C" + displayopt;
                                            answerValue = ansExpression.Split('~')[0].Split('*')[1];
                                            if (questionTypeId == 14)
                                                DDlDisplayOrder = dropDownDisplayOrder.Where(l => l.Id == Convert.ToInt32(answerValue)).FirstOrDefault().DDLDisplayOrder;
                                        }
                                        else
                                        {
                                            foreach (var item in optionDisplayOrder)
                                            {
                                                if (Convert.ToString(item.OptionId) == ansExpression.Split('~')[0])
                                                {
                                                    displaycol = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            foreach (var item in columnDisplayOrder)
                                            {
                                                if (Convert.ToString(item.Id) == ansExpression.Split('~')[1])
                                                {
                                                    displayopt = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            //formattedAnswer = "Q" + qustionDisplayOrder + "_M" + displaycol + "_C" + displayopt;
                                            formattedAnswer = "Q" + questionId + "_M" + displaycol + "_C" + displayopt;
                                        }
                                        foreach (DataColumn column in dtF.Columns)
                                        {
                                            string[] columnHeader = column.ColumnName.Split('-');

                                            if (columnHeader.Length > 1)
                                            {
                                                string[] answer = columnHeader[0].Split('_');

                                                if (answer.Length > 2)
                                                {
                                                    string answerInHeader = answer[0] + "_" + answer[1] + "_" + answer[2];

                                                    if (formattedAnswer == answerInHeader)
                                                    {
                                                        int index = dr_dtF.Table.Columns[column.ColumnName].Ordinal;
                                                        lstColumnOrdinals.Add(index.ToString());
                                                        //count++;
                                                        if (questionTypeId == 9)
                                                        {
                                                            dr_dtF[index] = answerValue;
                                                        }
                                                        else if (questionTypeId == 14)
                                                        {
                                                            dr_dtF[index] = DDlDisplayOrder;
                                                        }
                                                        else
                                                        {
                                                            dr_dtF[index] = "1";
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }

                                // Fill All the Empty cells with 2

                                for (int i = 0; i <= dtF.Columns.Count - 1; i++)
                                    if (String.IsNullOrEmpty(Convert.ToString(dr_dtF[i])))
                                        dr_dtF[i] = "";
                            }

                            else
                            {
                                dr_dtF[count] = answerExpression;
                                count++;
                                optionsCount--;
                            }

                            if (productId != oldProductID)
                            {
                                dtF.Rows.Add(drTemp);
                                //  drTemp[0] = dtF.Rows.IndexOf(drTemp) + 1;
                                //   drTemp[0] = response_id;
                                drTemp[0] = tempOnlineResponderId;
                                oldProductID = productId;
                            }

                            drTemp = dr_dtF;
                            //---ss
                            if (model.projectModel.ProjectType == "2")
                            {
                                drTemp[2] = product_desc;
                                drTemp[1] = product_logiId;
                            }
                            //---ss

                        }

                        dtF.Rows.Add(dr_dtF);
                        //dr_dtF[0] = dtF.Rows.IndexOf(dr_dtF) + 1;
                        //drTemp[0] = response_id;
                        drTemp[0] = tempOnlineResponderId;
                        //---ss
                        if (model.projectModel.ProjectType == "2")
                        {
                            dr_dtF[2] = product_desc;
                            dr_dtF[1] = product_logiId;
                        }
                        // response_id++;
                        //---ss
                    }
                }

                if (dtF != null && dtF.Rows.Count > 0)
                {
                    //DataTable dtF = comMethods.GenerateTransposedTable(dt);
                    newData = comMethods.GenerateTransposedTable(dt);
                    foreach (DataRow dr in dtF.Rows)
                    {
                        if (model.projectModel.ProjectType == "2")
                        {
                            DataRow rw = newData.AsEnumerable().FirstOrDefault(tt => tt.Field<string>("Resp_Id") == dr["Resp_Id"].ToString() && tt.Field<string>("Product_Name") == dr["Product_Name"].ToString());
                            if (rw == null)
                            {
                                DataRow dr_dtF = newData.NewRow();

                                newData.Rows.Add(dr_dtF);
                                foreach (DataColumn dcTempKano in dtF.Columns)
                                {

                                    dr_dtF[dcTempKano.ToString()] = dr[dcTempKano.ToString()];

                                }
                                // row exists
                            }
                        }
                        else
                        {
                            DataRow rw = newData.AsEnumerable().FirstOrDefault(tt => tt.Field<string>("Resp_Id") == dr["Resp_Id"].ToString());
                            if (rw == null)
                            {
                                DataRow dr_dtF = newData.NewRow();

                                newData.Rows.Add(dr_dtF);
                                foreach (DataColumn dcTempKano in dtF.Columns)
                                {

                                    dr_dtF[dcTempKano.ToString()] = dr[dcTempKano.ToString()];

                                }
                                // row exists
                            }
                        }


                    }
                }
            }
            catch (Exception ex)
            {
                //ErrorReporting.WebApplicationError(ex);
            }
            if (newData.Columns.Count > 0)
            {
                string ReportDataHtml = "<table id='hdnReportHtml'>";
                DataTable dtReport = newData;
                if (dtReport != null)
                {
                    string[] cols = (from dc in dtReport.Columns.Cast<DataColumn>()
                                     select dc.ColumnName)
                                   .ToArray();
                    if (cols != null)
                    {
                        ReportDataHtml += "<tr class='thRow'>";
                        foreach (var item in cols)
                        {
                            ReportDataHtml += "<th>" + item + "</th>";
                        }
                        ReportDataHtml += "</tr>";
                    }

                    foreach (DataRow row in dtReport.Rows)
                    {
                        ReportDataHtml += "<tr class='tdRow'>";
                        foreach (var item in row.ItemArray)
                        {
                            ReportDataHtml += "<td>" + item + "</td>";
                        }
                        ReportDataHtml += "</tr>";
                    }
                }
                ReportDataHtml += "</table>";
                string onlineResponseXML = ConvertDatatableToXML(newData);
                actionResult = adminAction.OnlineSegmentationRespose_Add(proId, Id, onlineResponseXML, ReportDataHtml);
            }
        }
        #endregion

        #region GetReportDataTableOffline

        [WebMethod]
        private void GetReportDataTableOfflineSegmentation(string Id, int ResponseCount, int proId)
        {

            RespondentController respondent = new RespondentController();
            SurveyResponseModel model = new SurveyResponseModel();
            DataTable dtF = new DataTable();
            try
            {
                DataTable dt = new DataTable();

                string projectGuid = string.Empty;
                projectGuid = Id;
                dt.Columns.Add("Resp_Id", typeof(string));

                model = respondent.GetProjectPreview(projectGuid);

                //--ss
                if (model.projectModel.ProjectType == "2")
                {
                    DataRow drnew = dt.NewRow();
                    drnew[0] = "Product_LogicalId";
                    dt.Rows.Add(drnew);
                    DataRow drnew1 = dt.NewRow();
                    drnew1[0] = "Product_Name";
                    dt.Rows.Add(drnew1);
                }
                //--

                // Load all the questions of this project
                List<QuestionList> questionsList = new List<QuestionList>();
                questionBase.ProjectId = Convert.ToInt32(model.projectModel.Id);
                actionResult = adminAction.QuestionsLoadAll_ByProjectId(questionBase);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    foreach (DataRow dr in actionResult.dtResult.Rows)
                    {
                        questionsList.Add(new QuestionList
                        {
                            id = dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0,
                            questionTypeId = dr["QuestionTypeId"] != DBNull.Value ? Convert.ToInt32(dr["QuestionTypeId"]) : 0
                        });
                    }
                }

                string fileName = Guid.NewGuid().ToString().Substring(0, 5) + "_" + DateTime.Now.ToString("MM-dd-yyyy") + "_" + model.projectModel.ProjectName.Replace(" ", "");

                if (model.lstQuestionModel != null && model.lstQuestionModel.Count > 0)
                {
                    foreach (var item in model.lstQuestionModel)
                    {
                        if (item.PipedFrom > 0 && (!item.QuestionTitle.Contains("{{Q}}")))
                        {
                            if (item.QuestionTypeId == 2 || item.QuestionTypeId == 4 || item.QuestionTypeId == 5 || item.QuestionTypeId == 6)//Multi Punch || Constant Sum || Ranking || Matrix Type
                            {
                                if (model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel != null && model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel.Count > 0)
                                {
                                    foreach (var opt in model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel)
                                    {
                                        DataRow drnew = dt.NewRow();
                                        drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                        dt.Rows.Add(drnew);
                                    }
                                }
                            }

                            else if (item.QuestionTypeId == 1 || item.QuestionTypeId == 3 || item.QuestionTypeId == 7 || item.QuestionTypeId == 15)//Single Punch || Range || Simple Text
                            {
                                DataRow drnew = dt.NewRow();
                                drnew[0] = "Q" + item.QuestionId + "_S" + "-" + ReplaceHtml.ReplaceText(item.QuestionTitle);
                                dt.Rows.Add(drnew);
                            }
                            else if (item.QuestionTypeId == 8 || item.QuestionTypeId == 12)//Single punch matrix type  && Rank order type
                            {
                                //if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
                                //{
                                //foreach (var opt in item.lstOptionsModel)
                                //{
                                //    DataRow drnew = dt.NewRow();
                                //    //drnew[0] = "Q" + item.QuestionId + "_M" + opt.OptionId + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                //    drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                //    dt.Rows.Add(drnew);
                                //}
                                //}
                                if (item.lstMatrixTypeModel != null && item.lstMatrixTypeModel.Count > 0)
                                {
                                    foreach (var col in item.lstMatrixTypeModel)
                                    {
                                        DataRow drnew = dt.NewRow();
                                        //drnew[0] = "Q" + item.QuestionId + "_M" + opt.OptionId + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                        drnew[0] = "Q" + item.QuestionId + "_M" + col.DisplayOrder + "-" + ReplaceHtml.ReplaceText(col.ColumnName);
                                        dt.Rows.Add(drnew);
                                    }
                                }
                            }
                            else if (item.QuestionTypeId == 13)//Kano model matrix type 
                            {
                                if (model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel != null && model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel.Count > 0)
                                {
                                    foreach (var opt in model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel)
                                    {
                                        DataRow drnew = dt.NewRow();
                                        drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Present";
                                        dt.Rows.Add(drnew);
                                        DataRow drnew1 = dt.NewRow();
                                        drnew1[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Absent";
                                        dt.Rows.Add(drnew1);
                                    }
                                }
                            }
                            // Single Punch Matrix Type || Range Matrix Type Question ||
                            // Single punch type answer horizontal position || 
                            // Multiple punch type answer horizontal position || 
                            // Rank order type question || 
                            // KANO Model Matrix type question || Dropdown Matrix type question
                            else if (item.QuestionTypeId > 7 && item.QuestionTypeId != 15)
                            {
                                if (model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel != null && model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel.Count > 0)
                                {
                                    foreach (var opt in model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel)
                                    {
                                        foreach (var column in item.lstMatrixTypeModel)
                                        {
                                            DataRow drnew = dt.NewRow();
                                            drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "_C" + column.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + " " + ReplaceHtml.ReplaceText(column.ColumnName);
                                            dt.Rows.Add(drnew);
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (item.QuestionTypeId == 2 || item.QuestionTypeId == 4 || item.QuestionTypeId == 5 || item.QuestionTypeId == 6)//Multi Punch || Constant Sum || Ranking || Matrix Type
                            {
                                if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
                                {
                                    foreach (var opt in item.lstOptionsModel)
                                    {
                                        DataRow drnew = dt.NewRow();
                                        //drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                        drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                        dt.Rows.Add(drnew);
                                    }
                                }
                            }

                            else if (item.QuestionTypeId == 1 || item.QuestionTypeId == 3 || item.QuestionTypeId == 7 || item.QuestionTypeId == 15)//Single Punch || Range || Simple Text
                            {
                                DataRow drnew = dt.NewRow();
                                //drnew[0] = "Q" + item.DisplayOrder + "_S" + "-" + ReplaceHtml.ReplaceText(item.QuestionTitle);
                                drnew[0] = "Q" + item.QuestionId + "_S" + "-" + ReplaceHtml.ReplaceText(item.QuestionTitle);
                                dt.Rows.Add(drnew);
                            }
                            else if (item.QuestionTypeId == 8 || item.QuestionTypeId == 12)//Single punch matrix type  && Rank order type
                            {
                                //if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
                                //{
                                //foreach (var opt in item.lstOptionsModel)
                                //{
                                //    DataRow drnew = dt.NewRow();
                                //    //drnew[0] = "Q" + item.QuestionId + "_M" + opt.OptionId + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                //    drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                //    dt.Rows.Add(drnew);
                                //}
                                //}
                                if (item.lstMatrixTypeModel != null && item.lstMatrixTypeModel.Count > 0)
                                {
                                    foreach (var col in item.lstMatrixTypeModel)
                                    {
                                        DataRow drnew = dt.NewRow();
                                        //drnew[0] = "Q" + item.QuestionId + "_M" + opt.OptionId + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                        drnew[0] = "Q" + item.QuestionId + "_M" + col.DisplayOrder + "-" + ReplaceHtml.ReplaceText(col.ColumnName);
                                        dt.Rows.Add(drnew);
                                    }
                                }
                            }
                            else if (item.QuestionTypeId == 13)//Kano model matrix type 
                            {
                                if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
                                {
                                    foreach (var opt in item.lstOptionsModel)
                                    {
                                        DataRow drnew = dt.NewRow();
                                        //drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Present";
                                        drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Present";
                                        dt.Rows.Add(drnew);
                                        DataRow drnew1 = dt.NewRow();
                                        //drnew1[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Absent";
                                        drnew1[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Absent";
                                        dt.Rows.Add(drnew1);
                                    }
                                }
                            }
                            // Single Punch Matrix Type || Range Matrix Type Question ||
                            // Single punch type answer horizontal position || 
                            // Multiple punch type answer horizontal position || 
                            // Rank order type question || 
                            // KANO Model Matrix type question || Dropdown Matrix type question
                            else if (item.QuestionTypeId > 7)
                            {
                                if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
                                {
                                    foreach (var opt in item.lstOptionsModel)
                                    {
                                        foreach (var column in item.lstMatrixTypeModel)
                                        {
                                            DataRow drnew = dt.NewRow();
                                            //drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "_C" + column.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + " " + ReplaceHtml.ReplaceText(column.ColumnName);
                                            drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "_C" + column.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + " " + ReplaceHtml.ReplaceText(column.ColumnName);
                                            dt.Rows.Add(drnew);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                // Get the Excel Pattern of dt
                dtF = comMethods.GenerateTransposedTable(dt);

                // Get all the reponders
                projectBase.Id = model.projectModel.Id;
                actionResult = adminAction.Responders_Offline_LoadBy_ProjectId(projectBase);

                // Get Responses
                SurveyLoadBase surveyLoadBase = new SurveyLoadBase();
                string json = string.Empty;
                SRV.BaseLayer.ActionResult actionResultResponses = new SRV.BaseLayer.ActionResult();
                int response_id = ResponseCount;            //ss
                foreach (DataRow dr in actionResult.dtResult.Rows)
                {
                    surveyLoadBase.Id = model.projectModel.Id;
                    surveyLoadBase.OfflineResponderId = dr["RespId"] != DBNull.Value ? Convert.ToString(dr["RespId"]) : "";
                    string tempOfflineResponderId = dr["RespId"] != DBNull.Value ? Convert.ToString(dr["RespId"]) : "";
                    //DataRow dr_dtF = dtF.NewRow();

                    if (String.IsNullOrEmpty(surveyLoadBase.OfflineResponderId)) continue;


                    DataTable responsesDt = new DataTable();


                    if (model.projectModel.lstProductProjectModel != null && model.projectModel.lstProductProjectModel.Count > 0)
                    {
                        foreach (var productModel in model.projectModel.lstProductProjectModel)
                        {
                            surveyLoadBase.ProductId = productModel.Id;
                            actionResultResponses = respondentAction.Response_Offline_LoadBy_ProjectId_RespId(surveyLoadBase);
                            if (actionResultResponses.IsSuccess)
                            {
                                if (responsesDt.Rows.Count == 0)
                                    responsesDt = actionResultResponses.dtResult.Copy();
                                else
                                {
                                    foreach (DataRow drThisRespRow in actionResultResponses.dtResult.Rows)
                                        responsesDt.ImportRow(drThisRespRow);

                                }
                            }
                        }
                    }
                    else
                    {
                        actionResultResponses = respondentAction.Response_Offline_LoadBy_ProjectId_RespId(surveyLoadBase);
                        if (actionResultResponses.IsSuccess)
                            responsesDt = actionResultResponses.dtResult.Copy();

                    }


                    //actionResultResponses = respondentAction.Response_Offline_LoadBy_ProjectId_RespId(surveyLoadBase);
                    if (responsesDt.Rows.Count > 0)//actionResultResponses.IsSuccess
                    {
                        //dr_dtF[0] = surveyLoadBase.Email;

                        int count = 1;
                        count = (model.projectModel.ProjectType == "2") ? count + 2 : count;        //ss
                        DataRow dr_dtF = dtF.NewRow();
                        DataRow drTemp = dtF.NewRow();
                        string product_desc = string.Empty;
                        string product_logiId = string.Empty;
                        int oldProductID = responsesDt.Rows[0]["ProductId"] != DBNull.Value ? Convert.ToInt32(responsesDt.Rows[0]["ProductId"]) : 0;
                        foreach (DataRow drResponse in responsesDt.Rows)// actionResultResponses.dtResult.Rows
                        {

                            int optionsCount = 0;

                            int productId = drResponse["ProductId"] != DBNull.Value ? Convert.ToInt32(drResponse["ProductId"]) : 0;
                            if (productId != oldProductID)
                            {
                                count = (model.projectModel.ProjectType == "2") ? 3 : 1;
                                dr_dtF = dtF.NewRow();
                            }

                            //--ss

                            product_desc = drResponse["ProductDescription"] != DBNull.Value ? Convert.ToString(drResponse["ProductDescription"]) : "";
                            product_logiId = drResponse["ProductLogicalId"] != DBNull.Value ? Convert.ToString(drResponse["ProductLogicalId"]) : "";

                            //--ss
                            int questionId = drResponse["QuestionId"] != DBNull.Value ? Convert.ToInt32(drResponse["QuestionId"]) : 0;
                            string answerExpression = drResponse["AnswerExpression"] != DBNull.Value ? Convert.ToString(drResponse["AnswerExpression"]) : "";
                            int questionTypeId = drResponse["QuestionTypeId"] != DBNull.Value ? Convert.ToInt32(drResponse["QuestionTypeId"]) : 1;
                            int PipedFrom = drResponse["PipedFrom"] != DBNull.Value ? Convert.ToInt32(drResponse["PipedFrom"]) : 0;
                            //dr_dtF[0] = drResponse;

                            // Get Total options count for this question
                            questionBase.Id = Convert.ToInt32(questionId);
                            SRV.BaseLayer.ActionResult actionResultAnswersTemp = new SRV.BaseLayer.ActionResult();
                            actionResultAnswersTemp = adminAction.Option_LoadAllById(questionBase);
                            if (actionResultAnswersTemp.dtResult != null && actionResultAnswersTemp.dtResult.Rows.Count > 0)
                            {
                                optionsCount = actionResultAnswersTemp.dtResult.Rows.Count;
                            }

                            if (questionTypeId == 2 || questionTypeId == 4 || questionTypeId == 5 || questionTypeId == 6)
                            {
                                string[] allAnswers = answerExpression.Split('|');
                                if (allAnswers.Length > 0)
                                {
                                    if (questionTypeId == 2)// Multi Punch
                                    {
                                        string allRec = string.Empty;
                                        //List<OptionsModel> matrixAns = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                        List<OptionsModel> matrixAns = new List<OptionsModel>();
                                        if (PipedFrom > 0 && !drResponse["QuestionTitle"].ToString().Contains("{{Q}}"))
                                        {
                                            matrixAns = model.lstQuestionModel.Where(v => v.QuestionId == PipedFrom).FirstOrDefault().lstOptionsModel;
                                        }
                                        else
                                        {
                                            matrixAns = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                        }
                                        foreach (var item in matrixAns)
                                        {
                                            string matchedId = string.Empty;
                                            foreach (string ans in allAnswers)
                                            {
                                                if (Convert.ToString(item.OptionId) == ans)
                                                {
                                                    matchedId = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            allRec += matchedId + "|";
                                        }

                                        string[] allAnswersMat = allRec.Substring(0, allRec.Length - 1).Split('|');
                                        foreach (string ans in allAnswersMat)
                                        {

                                            if (count <= dtF.Columns.Count - 1)
                                            {
                                                if ((Regex.Replace(dtF.Columns[count].ToString().Split('-')[0].Split('_')[1], @"[^\d]", String.Empty)) == ans)
                                                {
                                                    dr_dtF[count] = "1";// ans;
                                                    count++;
                                                    optionsCount--;
                                                }
                                                else
                                                {
                                                    dr_dtF[count] = "2";
                                                    count++;
                                                    optionsCount--;
                                                }
                                            }
                                        }
                                        //count = count + optionsCount;
                                    }
                                    else if (questionTypeId == 6) // matrix Type
                                    {
                                        foreach (string ans in allAnswers)
                                        {
                                            dr_dtF[count] = (ans.ToLower().IndexOf("yes") != -1) ? "1" : "2";
                                            count++;
                                            optionsCount--;
                                        }
                                        count = count + optionsCount;
                                    }
                                    else
                                    {
                                        foreach (string ans in allAnswers)
                                        {
                                            dr_dtF[count] = ans;
                                            count++;
                                            optionsCount--;
                                        }
                                        count = count + optionsCount;
                                    }
                                }
                            }
                            else if (questionTypeId == 1) // Single Punch Answers
                            {
                                //List<OptionsModel> SinglePunchAns = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                List<OptionsModel> SinglePunchAns = new List<OptionsModel>();
                                if (PipedFrom > 0 && !drResponse["QuestionTitle"].ToString().Contains("{{Q}}"))
                                {
                                    SinglePunchAns = model.lstQuestionModel.Where(v => v.QuestionId == PipedFrom).FirstOrDefault().lstOptionsModel;
                                }
                                else
                                {
                                    SinglePunchAns = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                }
                                int ansIndex = 0;
                                foreach (var item in SinglePunchAns)
                                {
                                    ansIndex++;
                                    if (Convert.ToString(item.OptionId) == answerExpression)
                                    {
                                        break;
                                    }
                                }

                                dr_dtF[count] = ansIndex;//answerExpression;
                                count++;
                                optionsCount--;
                            }
                            else if (questionTypeId == 8 || questionTypeId == 12) // single punch matrix answers
                            {
                                List<OptionsModel> optionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                List<MatrixTypeModel> columnDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstMatrixTypeModel;
                                int qustionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).Select(v => v.DisplayOrder).FirstOrDefault();
                                string[] allAnswers = answerExpression.Split('|');

                                List<string> lstColumnOrdinals = new List<string>();

                                string formattedAnswer = string.Empty;
                                string answerValue = string.Empty;
                                if (allAnswers.Length > 0)
                                {
                                    foreach (var ansExpression in allAnswers)
                                    {
                                        if (!string.IsNullOrEmpty(ansExpression))
                                        {
                                            string displaycol = string.Empty;
                                            string displayopt = string.Empty;
                                            foreach (var item in optionDisplayOrder)
                                            {

                                                if (Convert.ToString(item.OptionId) == ansExpression.Split('~')[0])
                                                {
                                                    displaycol = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            foreach (var item in columnDisplayOrder)
                                            {
                                                if (Convert.ToString(item.Id) == ansExpression.Split('~')[1])
                                                {
                                                    displayopt = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            //  formattedAnswer = "Q" + questionId + "_M" + displaycol + "#" + displayopt;
                                            formattedAnswer = "Q" + questionId + "_M" + displayopt + "#" + displaycol;
                                            foreach (DataColumn column in dtF.Columns)
                                            {
                                                string[] columnHeader = column.ColumnName.Split('-');
                                                string[] answer = columnHeader[0].Split('_');
                                                if (columnHeader.Length > 1)
                                                {
                                                    string answerInHeader = answer[0] + "_" + answer[1];
                                                    string[] formattedAnswerArr = formattedAnswer.Split('#');
                                                    if (formattedAnswerArr[0] == answerInHeader)
                                                    {
                                                        int index = dr_dtF.Table.Columns[column.ColumnName].Ordinal;
                                                        lstColumnOrdinals.Add(index.ToString());
                                                        count++;
                                                        dr_dtF[index] = formattedAnswerArr[1];
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            else if (questionTypeId == 13) // Kano model matrix type
                            {
                                List<OptionsModel> optionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                List<MatrixTypeModel> columnDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstMatrixTypeModel;
                                int qustionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).Select(v => v.DisplayOrder).FirstOrDefault();
                                string[] allAnswers = answerExpression.Split('|');

                                List<string> lstColumnOrdinals = new List<string>();

                                string formattedAnswer = string.Empty;
                                string answerValue = string.Empty;
                                if (allAnswers.Length > 1)
                                {
                                    int repeatId = 0;
                                    foreach (var ansExpression in allAnswers)
                                    {
                                        string displaycol = string.Empty;
                                        string displayopt = string.Empty;
                                        string displayText = string.Empty;
                                        string text = string.Empty;
                                        if (repeatId == Convert.ToInt32(ansExpression.Split('~')[0]))
                                        {
                                            text = "_Absent";
                                            repeatId = 0;
                                        }
                                        else
                                        {
                                            text = "_Present";
                                        }
                                        foreach (var item in optionDisplayOrder)
                                        {
                                            if (Convert.ToString(item.OptionId) == ansExpression.Split('~')[0])
                                            {
                                                displayText = Convert.ToString(item.Option) + text;
                                                displaycol = Convert.ToString(item.DisplayOrder);
                                                repeatId = Convert.ToInt32(ansExpression.Split('~')[0]);
                                                break;
                                            }
                                        }
                                        foreach (var item in columnDisplayOrder)
                                        {
                                            if (Convert.ToString(item.Id) == ansExpression.Split('~')[1])
                                            {
                                                displayopt = Convert.ToString(item.DisplayOrder);
                                                break;
                                            }
                                        }
                                        //formattedAnswer = "Q" + qustionDisplayOrder + "_M" + displaycol + "-" + displayText + "#" + displayopt;
                                        formattedAnswer = "Q" + questionId + "_M" + displaycol + "-" + displayText + "#" + displayopt;
                                        foreach (DataColumn column in dtF.Columns)
                                        {

                                            string[] columnHeader = column.ColumnName.Split('-');
                                            string[] answer = columnHeader[0].Split('_');
                                            if (columnHeader.Length > 1)
                                            {
                                                string answerInHeader = answer[0] + "_" + answer[1] + "-" + columnHeader[1];
                                                string[] formattedAnswerArr = formattedAnswer.Split('#');
                                                if (Regex.Replace(formattedAnswerArr[0], @"\s+", "", RegexOptions.Multiline) == Regex.Replace(answerInHeader, @"\s+", "", RegexOptions.Multiline))
                                                {
                                                    int index = dr_dtF.Table.Columns[column.ColumnName].Ordinal;
                                                    lstColumnOrdinals.Add(index.ToString());
                                                    count++;
                                                    dr_dtF[index] = formattedAnswerArr[1];
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            else if (questionTypeId > 7 && questionTypeId != 15)
                            {
                                List<OptionsModel> optionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                List<MatrixTypeModel> columnDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstMatrixTypeModel;
                                List<DDLMatrixTypeModel> dropDownDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstDDlMatrixModel;
                                //count = count + (optionDisplayOrder.Count * columnDisplayOrder.Count);
                                int optDisplayOrder = 0;
                                if (optionDisplayOrder != null)
                                {
                                    optDisplayOrder = optionDisplayOrder.Count;
                                }
                                int colDisplayOrder = 0;
                                if (columnDisplayOrder != null)
                                {
                                    colDisplayOrder = columnDisplayOrder.Count;
                                }
                                int qustionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).Select(v => v.DisplayOrder).FirstOrDefault();
                                string[] allAnswers = answerExpression.Split('|');

                                List<string> lstColumnOrdinals = new List<string>();

                                string formattedAnswer = string.Empty;
                                string answerValue = string.Empty;
                                int DDlDisplayOrder = 0;
                                if (allAnswers.Length > 1)
                                {
                                    foreach (var ansExpression in allAnswers)
                                    {
                                        string displaycol = string.Empty;
                                        string displayopt = string.Empty;

                                        if (questionTypeId == 9 || questionTypeId == 14)
                                        {
                                            foreach (var item in optionDisplayOrder)
                                            {
                                                if (Convert.ToString(item.OptionId) == ansExpression.Split('~')[0].Split('*')[0])
                                                {
                                                    displaycol = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            foreach (var item in columnDisplayOrder)
                                            {
                                                if (Convert.ToString(item.Id) == ansExpression.Split('~')[1])
                                                {
                                                    displayopt = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            //formattedAnswer = "Q" + qustionDisplayOrder + "_M" + displaycol + "_C" + displayopt;
                                            formattedAnswer = "Q" + questionId + "_M" + displaycol + "_C" + displayopt;
                                            answerValue = ansExpression.Split('~')[0].Split('*')[1];
                                            if (questionTypeId == 14)
                                                DDlDisplayOrder = dropDownDisplayOrder.Where(l => l.Id == Convert.ToInt32(answerValue)).FirstOrDefault().DDLDisplayOrder;
                                        }
                                        else
                                        {
                                            foreach (var item in optionDisplayOrder)
                                            {
                                                if (Convert.ToString(item.OptionId) == ansExpression.Split('~')[0])
                                                {
                                                    displaycol = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            foreach (var item in columnDisplayOrder)
                                            {
                                                if (Convert.ToString(item.Id) == ansExpression.Split('~')[1])
                                                {
                                                    displayopt = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            //formattedAnswer = "Q" + qustionDisplayOrder + "_M" + displaycol + "_C" + displayopt;
                                            formattedAnswer = "Q" + questionId + "_M" + displaycol + "_C" + displayopt;
                                        }
                                        foreach (DataColumn column in dtF.Columns)
                                        {
                                            string[] columnHeader = column.ColumnName.Split('-');

                                            if (columnHeader.Length > 1)
                                            {
                                                string[] answer = columnHeader[0].Split('_');

                                                if (answer.Length > 2)
                                                {
                                                    string answerInHeader = answer[0] + "_" + answer[1] + "_" + answer[2];

                                                    if (formattedAnswer == answerInHeader)
                                                    {
                                                        int index = dr_dtF.Table.Columns[column.ColumnName].Ordinal;
                                                        lstColumnOrdinals.Add(index.ToString());
                                                        //count++;
                                                        if (questionTypeId == 9)
                                                        {
                                                            dr_dtF[index] = answerValue;
                                                        }
                                                        else if (questionTypeId == 14)
                                                        {
                                                            dr_dtF[index] = DDlDisplayOrder;
                                                        }
                                                        else
                                                        {
                                                            dr_dtF[index] = "1";
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }

                                // Fill All the Empty cells with 2

                                for (int i = 0; i <= dtF.Columns.Count - 1; i++)
                                    if (String.IsNullOrEmpty(Convert.ToString(dr_dtF[i])))
                                        dr_dtF[i] = "";
                            }

                            else
                            {
                                dr_dtF[count] = answerExpression;
                                count++;
                                optionsCount--;
                            }



                            if (productId != oldProductID)
                            {
                                dtF.Rows.Add(drTemp);
                                drTemp[0] = tempOfflineResponderId;
                                oldProductID = productId;
                            }

                            drTemp = dr_dtF;
                            //---ss
                            if (model.projectModel.ProjectType == "2")
                            {
                                drTemp[2] = product_desc;
                                drTemp[1] = product_logiId;
                            }
                            //---ss


                        }


                        dtF.Rows.Add(dr_dtF);
                        drTemp[0] = tempOfflineResponderId;
                        //---ss
                        if (model.projectModel.ProjectType == "2")
                        {
                            dr_dtF[2] = product_desc;
                            dr_dtF[1] = product_logiId;
                        }
                        //  response_id++;
                        //---ss
                    }
                }
            }
            catch (Exception ex)
            {
               // ErrorReporting.WebApplicationError(ex);
            }
            if (dtF.Columns.Count > 0)
            {
                string ReportDataHtml = "<table id='hdnReportHtml'>";
                DataTable dtReport = dtF;
                if (dtReport != null)
                {
                    string[] cols = (from dc in dtReport.Columns.Cast<DataColumn>()
                                     select dc.ColumnName)
                                   .ToArray();
                    if (cols != null)
                    {
                        ReportDataHtml += "<tr class='thRow'>";
                        foreach (var item in cols)
                        {
                            ReportDataHtml += "<th>" + item + "</th>";
                        }
                        ReportDataHtml += "</tr>";
                    }

                    foreach (DataRow row in dtReport.Rows)
                    {
                        ReportDataHtml += "<tr class='tdRow'>";
                        foreach (var item in row.ItemArray)
                        {
                            ReportDataHtml += "<td>" + item + "</td>";
                        }
                        ReportDataHtml += "</tr>";
                    }
                }
                ReportDataHtml += "</table>";
                string responseXML = ConvertDatatableToXML(dtF);
                actionResult = adminAction.OfflineSegmentationRespose_Add(proId, Id, responseXML, ReportDataHtml);
            }
        }
        #endregion

        #region GetReportDataTableCombined
        [WebMethod]
        public void GetSegmentationReportCombined(string projectGUID, int projectId)
        {
            int RespCount = 0;
            DataTable dtOnline = new DataTable();
            DataTable dtOffline = new DataTable();
            dtOnline = GetReportOnlineSegmentationCombine(projectGUID, projectId);
            RespCount = (from DataRow dRow in dtOnline.Rows select dRow["Resp_Id"]).Distinct().Count();
            dtOffline = GetReportOfflineSegmentationCombine(projectGUID, (RespCount + 1), projectId);
            dtOnline.Merge(dtOffline);

            if (dtOnline.Columns.Count > 0)
            {
                string ReportDataHtml = "<table id='hdnReportHtml'>";
                DataTable dtReport = dtOnline;
                if (dtReport != null)
                {
                    string[] cols = (from dc in dtReport.Columns.Cast<DataColumn>()
                                     select dc.ColumnName)
                                   .ToArray();
                    if (cols != null)
                    {
                        ReportDataHtml += "<tr class='thRow'>";
                        foreach (var item in cols)
                        {
                            ReportDataHtml += "<th>" + item + "</th>";
                        }
                        ReportDataHtml += "</tr>";
                    }

                    foreach (DataRow row in dtReport.Rows)
                    {
                        ReportDataHtml += "<tr class='tdRow'>";
                        foreach (var item in row.ItemArray)
                        {
                            ReportDataHtml += "<td>" + item + "</td>";
                        }
                        ReportDataHtml += "</tr>";
                    }
                }
                ReportDataHtml += "</table>";

                string responseXML = ConvertDatatableToXML(dtOnline);
                actionResult = adminAction.CombineSegmentationRespose_Add(projectId, projectGUID, responseXML, ReportDataHtml);
            }
        }
        #endregion

        #region GetReportDataTable for Combine

        [WebMethod]
        private DataTable GetReportOnlineSegmentationCombine(string Id, int proId)
        {

            RespondentController respondent = new RespondentController();
            SurveyResponseModel model = new SurveyResponseModel();
            DataTable dtF = new DataTable();
            DataTable newData = new DataTable();
            try
            {
                DataTable dt = new DataTable();

                string projectGuid = string.Empty;
                projectGuid = Id;
                dt.Columns.Add("Resp_Id", typeof(string));

                model = respondent.GetProjectPreview(projectGuid);

                //--ss
                if (model.projectModel.ProjectType == "2")
                {
                    DataRow drnew = dt.NewRow();
                    drnew[0] = "Product_LogicalId";
                    dt.Rows.Add(drnew);
                    DataRow drnew1 = dt.NewRow();
                    drnew1[0] = "Product_Name";
                    dt.Rows.Add(drnew1);
                }
                //--

                // Load all the questions of this project
                List<QuestionList> questionsList = new List<QuestionList>();
                questionBase.ProjectId = Convert.ToInt32(model.projectModel.Id);
                actionResult = adminAction.QuestionsLoadAll_ByProjectId(questionBase);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    foreach (DataRow dr in actionResult.dtResult.Rows)
                    {
                        questionsList.Add(new QuestionList
                        {
                            id = dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0,
                            questionTypeId = dr["QuestionTypeId"] != DBNull.Value ? Convert.ToInt32(dr["QuestionTypeId"]) : 0
                        });
                    }
                }

                string fileName = Guid.NewGuid().ToString().Substring(0, 5) + "_" + DateTime.Now.ToString("MM-dd-yyyy") + "_" + model.projectModel.ProjectName.Replace(" ", "");

                if (model.lstQuestionModel != null && model.lstQuestionModel.Count > 0)
                {
                    foreach (var item in model.lstQuestionModel)
                    {
                        if (item.PipedFrom > 0 && (!item.QuestionTitle.Contains("{{Q}}")))
                        {
                            if (item.QuestionTypeId == 2 || item.QuestionTypeId == 4 || item.QuestionTypeId == 5 || item.QuestionTypeId == 6)//Multi Punch || Constant Sum || Ranking || Matrix Type
                            {
                                if (model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel != null && model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel.Count > 0)
                                {
                                    foreach (var opt in model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel)
                                    {
                                        DataRow drnew = dt.NewRow();
                                        drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                        dt.Rows.Add(drnew);
                                    }
                                }
                            }

                            else if (item.QuestionTypeId == 1 || item.QuestionTypeId == 3 || item.QuestionTypeId == 7 || item.QuestionTypeId == 15)//Single Punch || Range || Simple Text
                            {
                                DataRow drnew = dt.NewRow();
                                drnew[0] = "Q" + item.QuestionId + "_S" + "-" + ReplaceHtml.ReplaceText(item.QuestionTitle);
                                dt.Rows.Add(drnew);
                            }
                            else if (item.QuestionTypeId == 8 || item.QuestionTypeId == 12)//Single punch matrix type  && Rank order type
                            {
                                //if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
                                //{
                                //foreach (var opt in item.lstOptionsModel)
                                //{
                                //    DataRow drnew = dt.NewRow();
                                //    //drnew[0] = "Q" + item.QuestionId + "_M" + opt.OptionId + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                //    drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                //    dt.Rows.Add(drnew);
                                //}
                                //}
                                if (item.lstMatrixTypeModel != null && item.lstMatrixTypeModel.Count > 0)
                                {
                                    foreach (var col in item.lstMatrixTypeModel)
                                    {
                                        DataRow drnew = dt.NewRow();
                                        //drnew[0] = "Q" + item.QuestionId + "_M" + opt.OptionId + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                        drnew[0] = "Q" + item.QuestionId + "_M" + col.DisplayOrder + "-" + ReplaceHtml.ReplaceText(col.ColumnName);
                                        dt.Rows.Add(drnew);
                                    }
                                }
                            }
                            else if (item.QuestionTypeId == 13)//Kano model matrix type 
                            {
                                if (model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel != null && model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel.Count > 0)
                                {
                                    foreach (var opt in model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel)
                                    {
                                        DataRow drnew = dt.NewRow();
                                        drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Present";
                                        dt.Rows.Add(drnew);
                                        DataRow drnew1 = dt.NewRow();
                                        drnew1[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Absent";
                                        dt.Rows.Add(drnew1);
                                    }
                                }
                            }
                            // Single Punch Matrix Type || Range Matrix Type Question ||
                            // Single punch type answer horizontal position || 
                            // Multiple punch type answer horizontal position || 
                            // Rank order type question || 
                            // KANO Model Matrix type question || Dropdown Matrix type question
                            else if (item.QuestionTypeId > 7 && item.QuestionTypeId != 15)
                            {
                                if (model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel != null && model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel.Count > 0)
                                {
                                    foreach (var opt in model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel)
                                    {
                                        foreach (var column in item.lstMatrixTypeModel)
                                        {
                                            DataRow drnew = dt.NewRow();
                                            drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "_C" + column.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + " " + ReplaceHtml.ReplaceText(column.ColumnName);
                                            dt.Rows.Add(drnew);
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (item.QuestionTypeId == 2 || item.QuestionTypeId == 4 || item.QuestionTypeId == 5 || item.QuestionTypeId == 6)//Multi Punch || Constant Sum || Ranking || Matrix Type
                            {
                                if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
                                {
                                    foreach (var opt in item.lstOptionsModel)
                                    {
                                        DataRow drnew = dt.NewRow();
                                        //drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                        drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                        dt.Rows.Add(drnew);
                                    }
                                }
                            }

                            else if (item.QuestionTypeId == 1 || item.QuestionTypeId == 3 || item.QuestionTypeId == 7 || item.QuestionTypeId == 15)//Single Punch || Range || Simple Text
                            {
                                DataRow drnew = dt.NewRow();
                                //drnew[0] = "Q" + item.DisplayOrder + "_S" + "-" + ReplaceHtml.ReplaceText(item.QuestionTitle);
                                drnew[0] = "Q" + item.QuestionId + "_S" + "-" + ReplaceHtml.ReplaceText(item.QuestionTitle);
                                dt.Rows.Add(drnew);
                            }
                            else if (item.QuestionTypeId == 8 || item.QuestionTypeId == 12)//Single punch matrix type  && Rank order type
                            {
                                //if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
                                //{
                                //foreach (var opt in item.lstOptionsModel)
                                //{
                                //    DataRow drnew = dt.NewRow();
                                //    //drnew[0] = "Q" + item.QuestionId + "_M" + opt.OptionId + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                //    drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                //    dt.Rows.Add(drnew);
                                //}
                                //}
                                if (item.lstMatrixTypeModel != null && item.lstMatrixTypeModel.Count > 0)
                                {
                                    foreach (var col in item.lstMatrixTypeModel)
                                    {
                                        DataRow drnew = dt.NewRow();
                                        //drnew[0] = "Q" + item.QuestionId + "_M" + opt.OptionId + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                        drnew[0] = "Q" + item.QuestionId + "_M" + col.DisplayOrder + "-" + ReplaceHtml.ReplaceText(col.ColumnName);
                                        dt.Rows.Add(drnew);
                                    }
                                }
                            }
                            else if (item.QuestionTypeId == 13)//Kano model matrix type 
                            {
                                if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
                                {
                                    foreach (var opt in item.lstOptionsModel)
                                    {
                                        DataRow drnew = dt.NewRow();
                                        //drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Present";
                                        drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Present";
                                        dt.Rows.Add(drnew);
                                        DataRow drnew1 = dt.NewRow();
                                        //drnew1[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Absent";
                                        drnew1[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Absent";
                                        dt.Rows.Add(drnew1);
                                    }
                                }
                            }
                            // Single Punch Matrix Type || Range Matrix Type Question ||
                            // Single punch type answer horizontal position || 
                            // Multiple punch type answer horizontal position || 
                            // Rank order type question || 
                            // KANO Model Matrix type question || Dropdown Matrix type question
                            else if (item.QuestionTypeId > 7)
                            {
                                if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
                                {
                                    foreach (var opt in item.lstOptionsModel)
                                    {
                                        foreach (var column in item.lstMatrixTypeModel)
                                        {
                                            DataRow drnew = dt.NewRow();
                                            //drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "_C" + column.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + " " + ReplaceHtml.ReplaceText(column.ColumnName);
                                            drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "_C" + column.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + " " + ReplaceHtml.ReplaceText(column.ColumnName);
                                            dt.Rows.Add(drnew);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                // Get the Excel Pattern of dt
                dtF = comMethods.GenerateTransposedTable(dt);

                // Get all the reponders
                projectBase.Id = model.projectModel.Id;
                actionResult = adminAction.CompleteRespondersLoad_ByProjectId(projectBase);
                List<RespondersModel> lstResponders = new List<RespondersModel>();
                foreach (DataRow dr in actionResult.dtResult.Rows)
                {
                    lstResponders.Add(new RespondersModel
                    {

                        InvitationGuid = dr["InvitationGuid"] != DBNull.Value ? Convert.ToString(dr["InvitationGuid"]) : "",
                        Email = dr["Email"] != DBNull.Value ? Convert.ToString(dr["Email"]) : "",

                    });
                }
                IEnumerable<RespondersModel> filteredList = lstResponders
                 .GroupBy(customer => customer.InvitationGuid).Select(group => group.First());
                // Get Responses
                SurveyLoadBase surveyLoadBase = new SurveyLoadBase();
                string json = string.Empty;
                SRV.BaseLayer.ActionResult actionResultResponses = new SRV.BaseLayer.ActionResult();
                int response_id = 1;            //ss
                foreach (var responder in filteredList)
                {
                    DataTable responsesDt = new DataTable();
                    surveyLoadBase.Id = model.projectModel.Id;
                    surveyLoadBase.Email = (!string.IsNullOrEmpty(responder.Email) ? Convert.ToString(responder.Email) : "");
                    surveyLoadBase.ExternalResponderId = (!string.IsNullOrEmpty(responder.InvitationGuid) ? Convert.ToString(responder.InvitationGuid) : "");
                    string tempOnlineResponderId = (!string.IsNullOrEmpty(responder.InvitationGuid) ? Convert.ToString(responder.InvitationGuid) : "");
                    if (model.projectModel.lstProductProjectModel != null && model.projectModel.lstProductProjectModel.Count > 0)
                    {
                        foreach (var productModel in model.projectModel.lstProductProjectModel)
                        {
                            surveyLoadBase.ProductId = productModel.Id;
                            actionResultResponses = respondentAction.Response_LoadByProjectId(surveyLoadBase);
                            if (actionResultResponses.IsSuccess)
                            {
                                if (responsesDt.Rows.Count == 0)
                                    responsesDt = actionResultResponses.dtResult.Copy();
                                else
                                {
                                    foreach (DataRow drThisRespRow in actionResultResponses.dtResult.Rows)
                                        responsesDt.ImportRow(drThisRespRow);
                                }
                            }
                        }
                    }
                    else
                    {
                        actionResultResponses = respondentAction.Response_LoadByProjectId(surveyLoadBase);
                        if (actionResultResponses.IsSuccess)
                            responsesDt = actionResultResponses.dtResult.Copy();

                    }

                    // surveyLoadBase.OfflineResponderId = dr["RespId"] != DBNull.Value ? Convert.ToString(dr["RespId"]) : "";
                    //DataRow dr_dtF = dtF.NewRow();
                    // if (String.IsNullOrEmpty(surveyLoadBase.Email)) continue;

                    //actionResultResponses = respondentAction.Response_LoadByProjectId(surveyLoadBase);
                    if (responsesDt.Rows.Count > 0)//actionResultResponses.IsSuccess
                    {
                        //dr_dtF[0] = surveyLoadBase.Email;

                        int count = 1;
                        count = (model.projectModel.ProjectType == "2") ? count + 2 : count;        //ss
                        DataRow dr_dtF = dtF.NewRow();
                        DataRow drTemp = dtF.NewRow();
                        string product_desc = string.Empty;
                        string product_logiId = string.Empty;
                        int oldProductID = responsesDt.Rows[0]["ProductId"] != DBNull.Value ? Convert.ToInt32(responsesDt.Rows[0]["ProductId"]) : 0;
                        foreach (DataRow drResponse in responsesDt.Rows)// actionResultResponses.dtResult.Rows
                        {

                            int optionsCount = 0;
                            // response_id = drResponse["ReponderId"] != DBNull.Value ? (responsesDt.Rows[0]["ReponderId"]).ToString() : "";
                            int productId = drResponse["ProductId"] != DBNull.Value ? Convert.ToInt32(drResponse["ProductId"]) : 0;
                            if (productId != oldProductID)
                            {
                                count = (model.projectModel.ProjectType == "2") ? 3 : 1;
                                dr_dtF = dtF.NewRow();
                            }

                            //--ss

                            product_desc = drResponse["ProductDescription"] != DBNull.Value ? Convert.ToString(drResponse["ProductDescription"]) : "";
                            product_logiId = drResponse["ProductLogicalId"] != DBNull.Value ? Convert.ToString(drResponse["ProductLogicalId"]) : "";

                            //--ss
                            int questionId = drResponse["QuestionId"] != DBNull.Value ? Convert.ToInt32(drResponse["QuestionId"]) : 0;
                            string answerExpression = drResponse["AnswerExpression"] != DBNull.Value ? Convert.ToString(drResponse["AnswerExpression"]) : "";
                            int questionTypeId = drResponse["QuestionTypeId"] != DBNull.Value ? Convert.ToInt32(drResponse["QuestionTypeId"]) : 1;
                            int PipedFrom = drResponse["PipedFrom"] != DBNull.Value ? Convert.ToInt32(drResponse["PipedFrom"]) : 0;
                            //dr_dtF[0] = drResponse;

                            // Get Total options count for this question
                            questionBase.Id = Convert.ToInt32(questionId);
                            SRV.BaseLayer.ActionResult actionResultAnswersTemp = new SRV.BaseLayer.ActionResult();
                            actionResultAnswersTemp = adminAction.Option_LoadAllById(questionBase);
                            if (actionResultAnswersTemp.dtResult != null && actionResultAnswersTemp.dtResult.Rows.Count > 0)
                            {
                                optionsCount = actionResultAnswersTemp.dtResult.Rows.Count;
                            }

                            if (questionTypeId == 2 || questionTypeId == 4 || questionTypeId == 5 || questionTypeId == 6)
                            {
                                string[] allAnswers = answerExpression.Split('|');
                                if (allAnswers.Length > 0)
                                {
                                    if (questionTypeId == 2)// Multi Punch
                                    {
                                        string allRec = string.Empty;
                                        List<OptionsModel> matrixAns = new List<OptionsModel>();
                                        if (PipedFrom > 0 && !drResponse["QuestionTitle"].ToString().Contains("{{Q}}"))
                                        {
                                            matrixAns = model.lstQuestionModel.Where(v => v.QuestionId == PipedFrom).FirstOrDefault().lstOptionsModel;
                                        }
                                        else
                                        {
                                            matrixAns = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                        }

                                        foreach (var item in matrixAns)
                                        {
                                            string matchedId = string.Empty;
                                            foreach (string ans in allAnswers)
                                            {
                                                if (Convert.ToString(item.OptionId) == ans)
                                                {
                                                    matchedId = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            allRec += matchedId + "|";
                                        }

                                        string[] allAnswersMat = allRec.Substring(0, allRec.Length - 1).Split('|');
                                        foreach (string ans in allAnswersMat)
                                        {

                                            if (count <= dtF.Columns.Count - 1)
                                            {
                                                if ((Regex.Replace(dtF.Columns[count].ToString().Split('-')[0].Split('_')[1], @"[^\d]", String.Empty)) == ans)
                                                {
                                                    dr_dtF[count] = "1";// ans;
                                                    count++;
                                                    optionsCount--;
                                                }
                                                else
                                                {
                                                    dr_dtF[count] = "2";
                                                    count++;
                                                    optionsCount--;
                                                }
                                            }
                                        }
                                        //count = count + optionsCount;
                                    }
                                    else if (questionTypeId == 6) // matrix Type
                                    {
                                        foreach (string ans in allAnswers)
                                        {
                                            dr_dtF[count] = (ans.ToLower().IndexOf("yes") != -1) ? "1" : "2";
                                            count++;
                                            optionsCount--;
                                        }
                                        count = count + optionsCount;
                                    }
                                    else
                                    {
                                        foreach (string ans in allAnswers)
                                        {
                                            dr_dtF[count] = ans;
                                            count++;
                                            optionsCount--;
                                        }
                                        count = count + optionsCount;
                                    }
                                }
                            }
                            else if (questionTypeId == 1) // Single Punch Answers
                            {
                                //List<OptionsModel> SinglePunchAns = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                List<OptionsModel> SinglePunchAns = new List<OptionsModel>();
                                if (PipedFrom > 0 && !drResponse["QuestionTitle"].ToString().Contains("{{Q}}"))
                                {
                                    SinglePunchAns = model.lstQuestionModel.Where(v => v.QuestionId == PipedFrom).FirstOrDefault().lstOptionsModel;
                                }
                                else
                                {
                                    SinglePunchAns = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                }
                                int ansIndex = 0;
                                foreach (var item in SinglePunchAns)
                                {
                                    ansIndex++;
                                    if (Convert.ToString(item.OptionId) == answerExpression)
                                    {
                                        break;
                                    }
                                }

                                dr_dtF[count] = ansIndex;//answerExpression;
                                count++;
                                optionsCount--;
                            }
                            else if (questionTypeId == 8 || questionTypeId == 12) // single punch matrix answers
                            {
                                List<OptionsModel> optionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                List<MatrixTypeModel> columnDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstMatrixTypeModel;
                                int qustionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).Select(v => v.DisplayOrder).FirstOrDefault();
                                string[] allAnswers = answerExpression.Split('|');

                                List<string> lstColumnOrdinals = new List<string>();

                                string formattedAnswer = string.Empty;
                                string answerValue = string.Empty;
                                if (allAnswers.Length > 0)
                                {
                                    foreach (var ansExpression in allAnswers)
                                    {
                                        string displaycol = string.Empty;
                                        string displayopt = string.Empty;
                                        foreach (var item in optionDisplayOrder)
                                        {

                                            if (Convert.ToString(item.OptionId) == ansExpression.Split('~')[0])
                                            {
                                                displaycol = Convert.ToString(item.DisplayOrder);
                                                break;
                                            }
                                        }
                                        foreach (var item in columnDisplayOrder)
                                        {
                                            if (Convert.ToString(item.Id) == ansExpression.Split('~')[1])
                                            {
                                                displayopt = Convert.ToString(item.DisplayOrder);
                                                break;
                                            }
                                        }
                                        //  formattedAnswer = "Q" + questionId + "_M" + displaycol + "#" + displayopt;
                                        formattedAnswer = "Q" + questionId + "_M" + displayopt + "#" + displaycol;
                                        foreach (DataColumn column in dtF.Columns)
                                        {
                                            string[] columnHeader = column.ColumnName.Split('-');
                                            string[] answer = columnHeader[0].Split('_');
                                            if (columnHeader.Length > 1)
                                            {
                                                string answerInHeader = answer[0] + "_" + answer[1];
                                                string[] formattedAnswerArr = formattedAnswer.Split('#');
                                                if (formattedAnswerArr[0] == answerInHeader)
                                                {
                                                    int index = dr_dtF.Table.Columns[column.ColumnName].Ordinal;
                                                    lstColumnOrdinals.Add(index.ToString());
                                                    count++;
                                                    dr_dtF[index] = formattedAnswerArr[1];
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            else if (questionTypeId == 13) // Kano model matrix type
                            {
                                List<OptionsModel> optionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                List<MatrixTypeModel> columnDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstMatrixTypeModel;
                                int qustionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).Select(v => v.DisplayOrder).FirstOrDefault();
                                string[] allAnswers = answerExpression.Split('|');

                                List<string> lstColumnOrdinals = new List<string>();

                                string formattedAnswer = string.Empty;
                                string answerValue = string.Empty;
                                if (allAnswers.Length > 1)
                                {
                                    int repeatId = 0;
                                    foreach (var ansExpression in allAnswers)
                                    {
                                        string displaycol = string.Empty;
                                        string displayopt = string.Empty;
                                        string displayText = string.Empty;
                                        string text = string.Empty;
                                        if (repeatId == Convert.ToInt32(ansExpression.Split('~')[0]))
                                        {
                                            text = "_Absent";
                                            repeatId = 0;
                                        }
                                        else
                                        {
                                            text = "_Present";
                                        }
                                        foreach (var item in optionDisplayOrder)
                                        {
                                            if (Convert.ToString(item.OptionId) == ansExpression.Split('~')[0])
                                            {
                                                displayText = Convert.ToString(item.Option) + text;
                                                displaycol = Convert.ToString(item.DisplayOrder);
                                                repeatId = Convert.ToInt32(ansExpression.Split('~')[0]);
                                                break;
                                            }
                                        }
                                        foreach (var item in columnDisplayOrder)
                                        {
                                            if (Convert.ToString(item.Id) == ansExpression.Split('~')[1])
                                            {
                                                displayopt = Convert.ToString(item.DisplayOrder);
                                                break;
                                            }
                                        }
                                        //formattedAnswer = "Q" + qustionDisplayOrder + "_M" + displaycol + "-" + displayText + "#" + displayopt;
                                        formattedAnswer = "Q" + questionId + "_M" + displaycol + "-" + displayText + "#" + displayopt;
                                        foreach (DataColumn column in dtF.Columns)
                                        {

                                            string[] columnHeader = column.ColumnName.Split('-');
                                            string[] answer = columnHeader[0].Split('_');
                                            if (columnHeader.Length > 1)
                                            {
                                                string answerInHeader = answer[0] + "_" + answer[1] + "-" + columnHeader[1];
                                                string[] formattedAnswerArr = formattedAnswer.Split('#');
                                                if (Regex.Replace(formattedAnswerArr[0], @"\s+", "", RegexOptions.Multiline) == Regex.Replace(answerInHeader, @"\s+", "", RegexOptions.Multiline))
                                                {
                                                    int index = dr_dtF.Table.Columns[column.ColumnName].Ordinal;
                                                    lstColumnOrdinals.Add(index.ToString());
                                                    count++;
                                                    dr_dtF[index] = formattedAnswerArr[1];
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            else if (questionTypeId > 7 && questionTypeId != 15)
                            {
                                List<OptionsModel> optionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                List<MatrixTypeModel> columnDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstMatrixTypeModel;
                                List<DDLMatrixTypeModel> dropDownDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstDDlMatrixModel;
                                //count = count + (optionDisplayOrder.Count * columnDisplayOrder.Count);
                                int optDisplayOrder = 0;
                                if (optionDisplayOrder != null)
                                {
                                    optDisplayOrder = optionDisplayOrder.Count;
                                }
                                int colDisplayOrder = 0;
                                if (columnDisplayOrder != null)
                                {
                                    colDisplayOrder = columnDisplayOrder.Count;
                                }
                                int qustionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).Select(v => v.DisplayOrder).FirstOrDefault();
                                string[] allAnswers = answerExpression.Split('|');

                                List<string> lstColumnOrdinals = new List<string>();

                                string formattedAnswer = string.Empty;
                                string answerValue = string.Empty;
                                int DDlDisplayOrder = 0;
                                if (allAnswers.Length > 1)
                                {
                                    foreach (var ansExpression in allAnswers)
                                    {
                                        string displaycol = string.Empty;
                                        string displayopt = string.Empty;

                                        if (questionTypeId == 9 || questionTypeId == 14)
                                        {
                                            foreach (var item in optionDisplayOrder)
                                            {
                                                if (Convert.ToString(item.OptionId) == ansExpression.Split('~')[0].Split('*')[0])
                                                {
                                                    displaycol = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            foreach (var item in columnDisplayOrder)
                                            {
                                                if (Convert.ToString(item.Id) == ansExpression.Split('~')[1])
                                                {
                                                    displayopt = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            //formattedAnswer = "Q" + qustionDisplayOrder + "_M" + displaycol + "_C" + displayopt;
                                            formattedAnswer = "Q" + questionId + "_M" + displaycol + "_C" + displayopt;
                                            answerValue = ansExpression.Split('~')[0].Split('*')[1];
                                            if (questionTypeId == 14)
                                                DDlDisplayOrder = dropDownDisplayOrder.Where(l => l.Id == Convert.ToInt32(answerValue)).FirstOrDefault().DDLDisplayOrder;
                                        }
                                        else
                                        {
                                            foreach (var item in optionDisplayOrder)
                                            {
                                                if (Convert.ToString(item.OptionId) == ansExpression.Split('~')[0])
                                                {
                                                    displaycol = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            foreach (var item in columnDisplayOrder)
                                            {
                                                if (Convert.ToString(item.Id) == ansExpression.Split('~')[1])
                                                {
                                                    displayopt = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            //formattedAnswer = "Q" + qustionDisplayOrder + "_M" + displaycol + "_C" + displayopt;
                                            formattedAnswer = "Q" + questionId + "_M" + displaycol + "_C" + displayopt;
                                        }
                                        foreach (DataColumn column in dtF.Columns)
                                        {
                                            string[] columnHeader = column.ColumnName.Split('-');

                                            if (columnHeader.Length > 1)
                                            {
                                                string[] answer = columnHeader[0].Split('_');

                                                if (answer.Length > 2)
                                                {
                                                    string answerInHeader = answer[0] + "_" + answer[1] + "_" + answer[2];

                                                    if (formattedAnswer == answerInHeader)
                                                    {
                                                        int index = dr_dtF.Table.Columns[column.ColumnName].Ordinal;
                                                        lstColumnOrdinals.Add(index.ToString());
                                                        //count++;
                                                        if (questionTypeId == 9)
                                                        {
                                                            dr_dtF[index] = answerValue;
                                                        }
                                                        else if (questionTypeId == 14)
                                                        {
                                                            dr_dtF[index] = DDlDisplayOrder;
                                                        }
                                                        else
                                                        {
                                                            dr_dtF[index] = "1";
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }

                                // Fill All the Empty cells with 2

                                for (int i = 0; i <= dtF.Columns.Count - 1; i++)
                                    if (String.IsNullOrEmpty(Convert.ToString(dr_dtF[i])))
                                        dr_dtF[i] = "";
                            }

                            else
                            {
                                dr_dtF[count] = answerExpression;
                                count++;
                                optionsCount--;
                            }

                            if (productId != oldProductID)
                            {
                                dtF.Rows.Add(drTemp);
                                //  drTemp[0] = dtF.Rows.IndexOf(drTemp) + 1;
                                //   drTemp[0] = response_id;
                                drTemp[0] = tempOnlineResponderId;
                                oldProductID = productId;
                            }

                            drTemp = dr_dtF;
                            //---ss
                            if (model.projectModel.ProjectType == "2")
                            {
                                drTemp[2] = product_desc;
                                drTemp[1] = product_logiId;
                            }
                            //---ss

                        }

                        dtF.Rows.Add(dr_dtF);
                        //dr_dtF[0] = dtF.Rows.IndexOf(dr_dtF) + 1;
                        //drTemp[0] = response_id;
                        drTemp[0] = tempOnlineResponderId;
                        //---ss
                        if (model.projectModel.ProjectType == "2")
                        {
                            dr_dtF[2] = product_desc;
                            dr_dtF[1] = product_logiId;
                        }
                        // response_id++;
                        //---ss
                    }
                }

                if (dtF != null && dtF.Rows.Count > 0)
                {
                    //DataTable dtF = comMethods.GenerateTransposedTable(dt);
                    newData = comMethods.GenerateTransposedTable(dt);
                    foreach (DataRow dr in dtF.Rows)
                    {
                        if (model.projectModel.ProjectType == "2")
                        {
                            DataRow rw = newData.AsEnumerable().FirstOrDefault(tt => tt.Field<string>("Resp_Id") == dr["Resp_Id"].ToString() && tt.Field<string>("Product_Name") == dr["Product_Name"].ToString());
                            if (rw == null)
                            {
                                DataRow dr_dtF = newData.NewRow();

                                newData.Rows.Add(dr_dtF);
                                foreach (DataColumn dcTempKano in dtF.Columns)
                                {

                                    dr_dtF[dcTempKano.ToString()] = dr[dcTempKano.ToString()];

                                }
                                // row exists
                            }
                        }
                        else
                        {
                            DataRow rw = newData.AsEnumerable().FirstOrDefault(tt => tt.Field<string>("Resp_Id") == dr["Resp_Id"].ToString());
                            if (rw == null)
                            {
                                DataRow dr_dtF = newData.NewRow();

                                newData.Rows.Add(dr_dtF);
                                foreach (DataColumn dcTempKano in dtF.Columns)
                                {

                                    dr_dtF[dcTempKano.ToString()] = dr[dcTempKano.ToString()];

                                }
                                // row exists
                            }
                        }


                    }
                }
            }
            catch (Exception ex)
            {
              //  ErrorReporting.WebApplicationError(ex);
            }
            return newData;
        }
        #endregion

        #region GetReportDataTableOffline for combine

        [WebMethod]
        private DataTable GetReportOfflineSegmentationCombine(string Id, int ResponseCount, int proId)
        {

            RespondentController respondent = new RespondentController();
            SurveyResponseModel model = new SurveyResponseModel();
            DataTable dtF = new DataTable();
            try
            {
                DataTable dt = new DataTable();

                string projectGuid = string.Empty;
                projectGuid = Id;
                dt.Columns.Add("Resp_Id", typeof(string));

                model = respondent.GetProjectPreview(projectGuid);

                //--ss
                if (model.projectModel.ProjectType == "2")
                {
                    DataRow drnew = dt.NewRow();
                    drnew[0] = "Product_LogicalId";
                    dt.Rows.Add(drnew);
                    DataRow drnew1 = dt.NewRow();
                    drnew1[0] = "Product_Name";
                    dt.Rows.Add(drnew1);
                }
                //--

                // Load all the questions of this project
                List<QuestionList> questionsList = new List<QuestionList>();
                questionBase.ProjectId = Convert.ToInt32(model.projectModel.Id);
                actionResult = adminAction.QuestionsLoadAll_ByProjectId(questionBase);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    foreach (DataRow dr in actionResult.dtResult.Rows)
                    {
                        questionsList.Add(new QuestionList
                        {
                            id = dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0,
                            questionTypeId = dr["QuestionTypeId"] != DBNull.Value ? Convert.ToInt32(dr["QuestionTypeId"]) : 0
                        });
                    }
                }

                string fileName = Guid.NewGuid().ToString().Substring(0, 5) + "_" + DateTime.Now.ToString("MM-dd-yyyy") + "_" + model.projectModel.ProjectName.Replace(" ", "");

                if (model.lstQuestionModel != null && model.lstQuestionModel.Count > 0)
                {
                    foreach (var item in model.lstQuestionModel)
                    {
                        if (item.PipedFrom > 0 && (!item.QuestionTitle.Contains("{{Q}}")))
                        {
                            if (item.QuestionTypeId == 2 || item.QuestionTypeId == 4 || item.QuestionTypeId == 5 || item.QuestionTypeId == 6)//Multi Punch || Constant Sum || Ranking || Matrix Type
                            {
                                if (model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel != null && model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel.Count > 0)
                                {
                                    foreach (var opt in model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel)
                                    {
                                        DataRow drnew = dt.NewRow();
                                        drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                        dt.Rows.Add(drnew);
                                    }
                                }
                            }

                            else if (item.QuestionTypeId == 1 || item.QuestionTypeId == 3 || item.QuestionTypeId == 7 || item.QuestionTypeId == 15)//Single Punch || Range || Simple Text
                            {
                                DataRow drnew = dt.NewRow();
                                drnew[0] = "Q" + item.QuestionId + "_S" + "-" + ReplaceHtml.ReplaceText(item.QuestionTitle);
                                dt.Rows.Add(drnew);
                            }
                            else if (item.QuestionTypeId == 8 || item.QuestionTypeId == 12)//Single punch matrix type  && Rank order type
                            {
                                //if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
                                //{
                                //foreach (var opt in item.lstOptionsModel)
                                //{
                                //    DataRow drnew = dt.NewRow();
                                //    //drnew[0] = "Q" + item.QuestionId + "_M" + opt.OptionId + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                //    drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                //    dt.Rows.Add(drnew);
                                //}
                                //}
                                if (item.lstMatrixTypeModel != null && item.lstMatrixTypeModel.Count > 0)
                                {
                                    foreach (var col in item.lstMatrixTypeModel)
                                    {
                                        DataRow drnew = dt.NewRow();
                                        //drnew[0] = "Q" + item.QuestionId + "_M" + opt.OptionId + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                        drnew[0] = "Q" + item.QuestionId + "_M" + col.DisplayOrder + "-" + ReplaceHtml.ReplaceText(col.ColumnName);
                                        dt.Rows.Add(drnew);
                                    }
                                }
                            }
                            else if (item.QuestionTypeId == 13)//Kano model matrix type 
                            {
                                if (model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel != null && model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel.Count > 0)
                                {
                                    foreach (var opt in model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel)
                                    {
                                        DataRow drnew = dt.NewRow();
                                        drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Present";
                                        dt.Rows.Add(drnew);
                                        DataRow drnew1 = dt.NewRow();
                                        drnew1[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Absent";
                                        dt.Rows.Add(drnew1);
                                    }
                                }
                            }
                            // Single Punch Matrix Type || Range Matrix Type Question ||
                            // Single punch type answer horizontal position || 
                            // Multiple punch type answer horizontal position || 
                            // Rank order type question || 
                            // KANO Model Matrix type question || Dropdown Matrix type question
                            else if (item.QuestionTypeId > 7 && item.QuestionTypeId != 15)
                            {
                                if (model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel != null && model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel.Count > 0)
                                {
                                    foreach (var opt in model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel)
                                    {
                                        foreach (var column in item.lstMatrixTypeModel)
                                        {
                                            DataRow drnew = dt.NewRow();
                                            drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "_C" + column.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + " " + ReplaceHtml.ReplaceText(column.ColumnName);
                                            dt.Rows.Add(drnew);
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (item.QuestionTypeId == 2 || item.QuestionTypeId == 4 || item.QuestionTypeId == 5 || item.QuestionTypeId == 6)//Multi Punch || Constant Sum || Ranking || Matrix Type
                            {
                                if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
                                {
                                    foreach (var opt in item.lstOptionsModel)
                                    {
                                        DataRow drnew = dt.NewRow();
                                        //drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                        drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                        dt.Rows.Add(drnew);
                                    }
                                }
                            }

                            else if (item.QuestionTypeId == 1 || item.QuestionTypeId == 3 || item.QuestionTypeId == 7 || item.QuestionTypeId == 15)//Single Punch || Range || Simple Text
                            {
                                DataRow drnew = dt.NewRow();
                                //drnew[0] = "Q" + item.DisplayOrder + "_S" + "-" + ReplaceHtml.ReplaceText(item.QuestionTitle);
                                drnew[0] = "Q" + item.QuestionId + "_S" + "-" + ReplaceHtml.ReplaceText(item.QuestionTitle);
                                dt.Rows.Add(drnew);
                            }
                            else if (item.QuestionTypeId == 8 || item.QuestionTypeId == 12)//Single punch matrix type  && Rank order type
                            {
                                //if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
                                //{
                                //foreach (var opt in item.lstOptionsModel)
                                //{
                                //    DataRow drnew = dt.NewRow();
                                //    //drnew[0] = "Q" + item.QuestionId + "_M" + opt.OptionId + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                //    drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                //    dt.Rows.Add(drnew);
                                //}
                                //}
                                if (item.lstMatrixTypeModel != null && item.lstMatrixTypeModel.Count > 0)
                                {
                                    foreach (var col in item.lstMatrixTypeModel)
                                    {
                                        DataRow drnew = dt.NewRow();
                                        //drnew[0] = "Q" + item.QuestionId + "_M" + opt.OptionId + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                        drnew[0] = "Q" + item.QuestionId + "_M" + col.DisplayOrder + "-" + ReplaceHtml.ReplaceText(col.ColumnName);
                                        dt.Rows.Add(drnew);
                                    }
                                }
                            }
                            else if (item.QuestionTypeId == 13)//Kano model matrix type 
                            {
                                if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
                                {
                                    foreach (var opt in item.lstOptionsModel)
                                    {
                                        DataRow drnew = dt.NewRow();
                                        //drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Present";
                                        drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Present";
                                        dt.Rows.Add(drnew);
                                        DataRow drnew1 = dt.NewRow();
                                        //drnew1[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Absent";
                                        drnew1[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Absent";
                                        dt.Rows.Add(drnew1);
                                    }
                                }
                            }
                            // Single Punch Matrix Type || Range Matrix Type Question ||
                            // Single punch type answer horizontal position || 
                            // Multiple punch type answer horizontal position || 
                            // Rank order type question || 
                            // KANO Model Matrix type question || Dropdown Matrix type question
                            else if (item.QuestionTypeId > 7)
                            {
                                if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
                                {
                                    foreach (var opt in item.lstOptionsModel)
                                    {
                                        foreach (var column in item.lstMatrixTypeModel)
                                        {
                                            DataRow drnew = dt.NewRow();
                                            //drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "_C" + column.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + " " + ReplaceHtml.ReplaceText(column.ColumnName);
                                            drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "_C" + column.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + " " + ReplaceHtml.ReplaceText(column.ColumnName);
                                            dt.Rows.Add(drnew);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                // Get the Excel Pattern of dt
                dtF = comMethods.GenerateTransposedTable(dt);

                // Get all the reponders
                projectBase.Id = model.projectModel.Id;
                actionResult = adminAction.Responders_Offline_LoadBy_ProjectId(projectBase);

                // Get Responses
                SurveyLoadBase surveyLoadBase = new SurveyLoadBase();
                string json = string.Empty;
                SRV.BaseLayer.ActionResult actionResultResponses = new SRV.BaseLayer.ActionResult();
                int response_id = ResponseCount;            //ss
                foreach (DataRow dr in actionResult.dtResult.Rows)
                {
                    surveyLoadBase.Id = model.projectModel.Id;
                    surveyLoadBase.OfflineResponderId = dr["RespId"] != DBNull.Value ? Convert.ToString(dr["RespId"]) : "";
                    string tempOfflineResponderId = dr["RespId"] != DBNull.Value ? Convert.ToString(dr["RespId"]) : "";
                    //DataRow dr_dtF = dtF.NewRow();

                    if (String.IsNullOrEmpty(surveyLoadBase.OfflineResponderId)) continue;


                    DataTable responsesDt = new DataTable();


                    if (model.projectModel.lstProductProjectModel != null && model.projectModel.lstProductProjectModel.Count > 0)
                    {
                        foreach (var productModel in model.projectModel.lstProductProjectModel)
                        {
                            surveyLoadBase.ProductId = productModel.Id;
                            actionResultResponses = respondentAction.Response_Offline_LoadBy_ProjectId_RespId(surveyLoadBase);
                            if (actionResultResponses.IsSuccess)
                            {
                                if (responsesDt.Rows.Count == 0)
                                    responsesDt = actionResultResponses.dtResult.Copy();
                                else
                                {
                                    foreach (DataRow drThisRespRow in actionResultResponses.dtResult.Rows)
                                        responsesDt.ImportRow(drThisRespRow);

                                }
                            }
                        }
                    }
                    else
                    {
                        actionResultResponses = respondentAction.Response_Offline_LoadBy_ProjectId_RespId(surveyLoadBase);
                        if (actionResultResponses.IsSuccess)
                            responsesDt = actionResultResponses.dtResult.Copy();

                    }


                    //actionResultResponses = respondentAction.Response_Offline_LoadBy_ProjectId_RespId(surveyLoadBase);
                    if (responsesDt.Rows.Count > 0)//actionResultResponses.IsSuccess
                    {
                        //dr_dtF[0] = surveyLoadBase.Email;

                        int count = 1;
                        count = (model.projectModel.ProjectType == "2") ? count + 2 : count;        //ss
                        DataRow dr_dtF = dtF.NewRow();
                        DataRow drTemp = dtF.NewRow();
                        string product_desc = string.Empty;
                        string product_logiId = string.Empty;
                        int oldProductID = responsesDt.Rows[0]["ProductId"] != DBNull.Value ? Convert.ToInt32(responsesDt.Rows[0]["ProductId"]) : 0;
                        foreach (DataRow drResponse in responsesDt.Rows)// actionResultResponses.dtResult.Rows
                        {

                            int optionsCount = 0;

                            int productId = drResponse["ProductId"] != DBNull.Value ? Convert.ToInt32(drResponse["ProductId"]) : 0;
                            if (productId != oldProductID)
                            {
                                count = (model.projectModel.ProjectType == "2") ? 3 : 1;
                                dr_dtF = dtF.NewRow();
                            }

                            //--ss

                            product_desc = drResponse["ProductDescription"] != DBNull.Value ? Convert.ToString(drResponse["ProductDescription"]) : "";
                            product_logiId = drResponse["ProductLogicalId"] != DBNull.Value ? Convert.ToString(drResponse["ProductLogicalId"]) : "";

                            //--ss
                            int questionId = drResponse["QuestionId"] != DBNull.Value ? Convert.ToInt32(drResponse["QuestionId"]) : 0;
                            string answerExpression = drResponse["AnswerExpression"] != DBNull.Value ? Convert.ToString(drResponse["AnswerExpression"]) : "";
                            int questionTypeId = drResponse["QuestionTypeId"] != DBNull.Value ? Convert.ToInt32(drResponse["QuestionTypeId"]) : 1;
                            int PipedFrom = drResponse["PipedFrom"] != DBNull.Value ? Convert.ToInt32(drResponse["PipedFrom"]) : 0;
                            //dr_dtF[0] = drResponse;

                            // Get Total options count for this question
                            questionBase.Id = Convert.ToInt32(questionId);
                            SRV.BaseLayer.ActionResult actionResultAnswersTemp = new SRV.BaseLayer.ActionResult();
                            actionResultAnswersTemp = adminAction.Option_LoadAllById(questionBase);
                            if (actionResultAnswersTemp.dtResult != null && actionResultAnswersTemp.dtResult.Rows.Count > 0)
                            {
                                optionsCount = actionResultAnswersTemp.dtResult.Rows.Count;
                            }

                            if (questionTypeId == 2 || questionTypeId == 4 || questionTypeId == 5 || questionTypeId == 6)
                            {
                                string[] allAnswers = answerExpression.Split('|');
                                if (allAnswers.Length > 0)
                                {
                                    if (questionTypeId == 2)// Multi Punch
                                    {
                                        string allRec = string.Empty;
                                        //List<OptionsModel> matrixAns = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                        List<OptionsModel> matrixAns = new List<OptionsModel>();
                                        if (PipedFrom > 0 && !drResponse["QuestionTitle"].ToString().Contains("{{Q}}"))
                                        {
                                            matrixAns = model.lstQuestionModel.Where(v => v.QuestionId == PipedFrom).FirstOrDefault().lstOptionsModel;
                                        }
                                        else
                                        {
                                            matrixAns = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                        }
                                        foreach (var item in matrixAns)
                                        {
                                            string matchedId = string.Empty;
                                            foreach (string ans in allAnswers)
                                            {
                                                if (Convert.ToString(item.OptionId) == ans)
                                                {
                                                    matchedId = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            allRec += matchedId + "|";
                                        }

                                        string[] allAnswersMat = allRec.Substring(0, allRec.Length - 1).Split('|');
                                        foreach (string ans in allAnswersMat)
                                        {

                                            if (count <= dtF.Columns.Count - 1)
                                            {
                                                if ((Regex.Replace(dtF.Columns[count].ToString().Split('-')[0].Split('_')[1], @"[^\d]", String.Empty)) == ans)
                                                {
                                                    dr_dtF[count] = "1";// ans;
                                                    count++;
                                                    optionsCount--;
                                                }
                                                else
                                                {
                                                    dr_dtF[count] = "2";
                                                    count++;
                                                    optionsCount--;
                                                }
                                            }
                                        }
                                        //count = count + optionsCount;
                                    }
                                    else if (questionTypeId == 6) // matrix Type
                                    {
                                        foreach (string ans in allAnswers)
                                        {
                                            dr_dtF[count] = (ans.ToLower().IndexOf("yes") != -1) ? "1" : "2";
                                            count++;
                                            optionsCount--;
                                        }
                                        count = count + optionsCount;
                                    }
                                    else
                                    {
                                        foreach (string ans in allAnswers)
                                        {
                                            dr_dtF[count] = ans;
                                            count++;
                                            optionsCount--;
                                        }
                                        count = count + optionsCount;
                                    }
                                }
                            }
                            else if (questionTypeId == 1) // Single Punch Answers
                            {
                                //List<OptionsModel> SinglePunchAns = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                List<OptionsModel> SinglePunchAns = new List<OptionsModel>();
                                if (PipedFrom > 0 && !drResponse["QuestionTitle"].ToString().Contains("{{Q}}"))
                                {
                                    SinglePunchAns = model.lstQuestionModel.Where(v => v.QuestionId == PipedFrom).FirstOrDefault().lstOptionsModel;
                                }
                                else
                                {
                                    SinglePunchAns = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                }
                                int ansIndex = 0;
                                foreach (var item in SinglePunchAns)
                                {
                                    ansIndex++;
                                    if (Convert.ToString(item.OptionId) == answerExpression)
                                    {
                                        break;
                                    }
                                }

                                dr_dtF[count] = ansIndex;//answerExpression;
                                count++;
                                optionsCount--;
                            }
                            else if (questionTypeId == 8 || questionTypeId == 12) // single punch matrix answers
                            {
                                List<OptionsModel> optionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                List<MatrixTypeModel> columnDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstMatrixTypeModel;
                                int qustionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).Select(v => v.DisplayOrder).FirstOrDefault();
                                string[] allAnswers = answerExpression.Split('|');

                                List<string> lstColumnOrdinals = new List<string>();

                                string formattedAnswer = string.Empty;
                                string answerValue = string.Empty;
                                if (allAnswers.Length > 0)
                                {
                                    foreach (var ansExpression in allAnswers)
                                    {
                                        if (!string.IsNullOrEmpty(ansExpression))
                                        {
                                            string displaycol = string.Empty;
                                            string displayopt = string.Empty;
                                            foreach (var item in optionDisplayOrder)
                                            {

                                                if (Convert.ToString(item.OptionId) == ansExpression.Split('~')[0])
                                                {
                                                    displaycol = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            foreach (var item in columnDisplayOrder)
                                            {
                                                if (Convert.ToString(item.Id) == ansExpression.Split('~')[1])
                                                {
                                                    displayopt = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            //  formattedAnswer = "Q" + questionId + "_M" + displaycol + "#" + displayopt;
                                            formattedAnswer = "Q" + questionId + "_M" + displayopt + "#" + displaycol;
                                            foreach (DataColumn column in dtF.Columns)
                                            {
                                                string[] columnHeader = column.ColumnName.Split('-');
                                                string[] answer = columnHeader[0].Split('_');
                                                if (columnHeader.Length > 1)
                                                {
                                                    string answerInHeader = answer[0] + "_" + answer[1];
                                                    string[] formattedAnswerArr = formattedAnswer.Split('#');
                                                    if (formattedAnswerArr[0] == answerInHeader)
                                                    {
                                                        int index = dr_dtF.Table.Columns[column.ColumnName].Ordinal;
                                                        lstColumnOrdinals.Add(index.ToString());
                                                        count++;
                                                        dr_dtF[index] = formattedAnswerArr[1];
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            else if (questionTypeId == 13) // Kano model matrix type
                            {
                                List<OptionsModel> optionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                List<MatrixTypeModel> columnDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstMatrixTypeModel;
                                int qustionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).Select(v => v.DisplayOrder).FirstOrDefault();
                                string[] allAnswers = answerExpression.Split('|');

                                List<string> lstColumnOrdinals = new List<string>();

                                string formattedAnswer = string.Empty;
                                string answerValue = string.Empty;
                                if (allAnswers.Length > 1)
                                {
                                    int repeatId = 0;
                                    foreach (var ansExpression in allAnswers)
                                    {
                                        string displaycol = string.Empty;
                                        string displayopt = string.Empty;
                                        string displayText = string.Empty;
                                        string text = string.Empty;
                                        if (repeatId == Convert.ToInt32(ansExpression.Split('~')[0]))
                                        {
                                            text = "_Absent";
                                            repeatId = 0;
                                        }
                                        else
                                        {
                                            text = "_Present";
                                        }
                                        foreach (var item in optionDisplayOrder)
                                        {
                                            if (Convert.ToString(item.OptionId) == ansExpression.Split('~')[0])
                                            {
                                                displayText = Convert.ToString(item.Option) + text;
                                                displaycol = Convert.ToString(item.DisplayOrder);
                                                repeatId = Convert.ToInt32(ansExpression.Split('~')[0]);
                                                break;
                                            }
                                        }
                                        foreach (var item in columnDisplayOrder)
                                        {
                                            if (Convert.ToString(item.Id) == ansExpression.Split('~')[1])
                                            {
                                                displayopt = Convert.ToString(item.DisplayOrder);
                                                break;
                                            }
                                        }
                                        //formattedAnswer = "Q" + qustionDisplayOrder + "_M" + displaycol + "-" + displayText + "#" + displayopt;
                                        formattedAnswer = "Q" + questionId + "_M" + displaycol + "-" + displayText + "#" + displayopt;
                                        foreach (DataColumn column in dtF.Columns)
                                        {

                                            string[] columnHeader = column.ColumnName.Split('-');
                                            string[] answer = columnHeader[0].Split('_');
                                            if (columnHeader.Length > 1)
                                            {
                                                string answerInHeader = answer[0] + "_" + answer[1] + "-" + columnHeader[1];
                                                string[] formattedAnswerArr = formattedAnswer.Split('#');
                                                if (Regex.Replace(formattedAnswerArr[0], @"\s+", "", RegexOptions.Multiline) == Regex.Replace(answerInHeader, @"\s+", "", RegexOptions.Multiline))
                                                {
                                                    int index = dr_dtF.Table.Columns[column.ColumnName].Ordinal;
                                                    lstColumnOrdinals.Add(index.ToString());
                                                    count++;
                                                    dr_dtF[index] = formattedAnswerArr[1];
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            else if (questionTypeId > 7 && questionTypeId != 15)
                            {
                                List<OptionsModel> optionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                List<MatrixTypeModel> columnDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstMatrixTypeModel;
                                List<DDLMatrixTypeModel> dropDownDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstDDlMatrixModel;
                                //count = count + (optionDisplayOrder.Count * columnDisplayOrder.Count);
                                int optDisplayOrder = 0;
                                if (optionDisplayOrder != null)
                                {
                                    optDisplayOrder = optionDisplayOrder.Count;
                                }
                                int colDisplayOrder = 0;
                                if (columnDisplayOrder != null)
                                {
                                    colDisplayOrder = columnDisplayOrder.Count;
                                }

                                int qustionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).Select(v => v.DisplayOrder).FirstOrDefault();
                                string[] allAnswers = answerExpression.Split('|');

                                List<string> lstColumnOrdinals = new List<string>();

                                string formattedAnswer = string.Empty;
                                string answerValue = string.Empty;
                                int DDlDisplayOrder = 0;
                                if (allAnswers.Length > 1)
                                {
                                    foreach (var ansExpression in allAnswers)
                                    {
                                        string displaycol = string.Empty;
                                        string displayopt = string.Empty;

                                        if (questionTypeId == 9 || questionTypeId == 14)
                                        {
                                            foreach (var item in optionDisplayOrder)
                                            {
                                                if (Convert.ToString(item.OptionId) == ansExpression.Split('~')[0].Split('*')[0])
                                                {
                                                    displaycol = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            foreach (var item in columnDisplayOrder)
                                            {
                                                if (Convert.ToString(item.Id) == ansExpression.Split('~')[1])
                                                {
                                                    displayopt = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            //formattedAnswer = "Q" + qustionDisplayOrder + "_M" + displaycol + "_C" + displayopt;
                                            formattedAnswer = "Q" + questionId + "_M" + displaycol + "_C" + displayopt;
                                            answerValue = ansExpression.Split('~')[0].Split('*')[1];
                                            if (questionTypeId == 14)
                                                DDlDisplayOrder = dropDownDisplayOrder.Where(l => l.Id == Convert.ToInt32(answerValue)).FirstOrDefault().DDLDisplayOrder;
                                        }
                                        else
                                        {
                                            foreach (var item in optionDisplayOrder)
                                            {
                                                if (Convert.ToString(item.OptionId) == ansExpression.Split('~')[0])
                                                {
                                                    displaycol = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            foreach (var item in columnDisplayOrder)
                                            {
                                                if (Convert.ToString(item.Id) == ansExpression.Split('~')[1])
                                                {
                                                    displayopt = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            //formattedAnswer = "Q" + qustionDisplayOrder + "_M" + displaycol + "_C" + displayopt;
                                            formattedAnswer = "Q" + questionId + "_M" + displaycol + "_C" + displayopt;
                                        }
                                        foreach (DataColumn column in dtF.Columns)
                                        {
                                            string[] columnHeader = column.ColumnName.Split('-');

                                            if (columnHeader.Length > 1)
                                            {
                                                string[] answer = columnHeader[0].Split('_');

                                                if (answer.Length > 2)
                                                {
                                                    string answerInHeader = answer[0] + "_" + answer[1] + "_" + answer[2];

                                                    if (formattedAnswer == answerInHeader)
                                                    {
                                                        int index = dr_dtF.Table.Columns[column.ColumnName].Ordinal;
                                                        lstColumnOrdinals.Add(index.ToString());
                                                        //count++;
                                                        if (questionTypeId == 9)
                                                        {
                                                            dr_dtF[index] = answerValue;
                                                        }
                                                        else if (questionTypeId == 14)
                                                        {
                                                            dr_dtF[index] = DDlDisplayOrder;
                                                        }
                                                        else
                                                        {
                                                            dr_dtF[index] = "1";
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }

                                // Fill All the Empty cells with 2

                                for (int i = 0; i <= dtF.Columns.Count - 1; i++)
                                    if (String.IsNullOrEmpty(Convert.ToString(dr_dtF[i])))
                                        dr_dtF[i] = "";
                            }

                            else
                            {
                                dr_dtF[count] = answerExpression;
                                count++;
                                optionsCount--;
                            }



                            if (productId != oldProductID)
                            {
                                dtF.Rows.Add(drTemp);
                                drTemp[0] = tempOfflineResponderId;
                                oldProductID = productId;
                            }

                            drTemp = dr_dtF;
                            //---ss
                            if (model.projectModel.ProjectType == "2")
                            {
                                drTemp[2] = product_desc;
                                drTemp[1] = product_logiId;
                            }
                            //---ss


                        }


                        dtF.Rows.Add(dr_dtF);
                        drTemp[0] = tempOfflineResponderId;
                        //---ss
                        if (model.projectModel.ProjectType == "2")
                        {
                            dr_dtF[2] = product_desc;
                            dr_dtF[1] = product_logiId;
                        }
                        //  response_id++;
                        //---ss
                    }
                }
            }
            catch (Exception ex)
            {
              //  ErrorReporting.WebApplicationError(ex);
            }
            return dtF;
        }
        #endregion

        // Convert to XML
        #region ConvertDatatableToXML
        public string ConvertDatatableToXML(DataTable dt)
        {
            string xmlstr;
            if (dt.Rows.Count > 0)
            {
                MemoryStream str = new MemoryStream();
                dt.TableName = "myTable";
                dt.WriteXml(str, true);
                str.Seek(0, SeekOrigin.Begin);
                StreamReader sr = new StreamReader(str);
                xmlstr = sr.ReadToEnd();
            }
            else
            {
                if (dt.Rows.Count == 0)
                {
                    dt.Rows.Add("1Delete1");
                }
                for (int i = 0; i < dt.Columns.Count; i++)
                {
                    dt.Rows[0][i] = "1Delete1";
                }

                MemoryStream str = new MemoryStream();
                dt.TableName = "myTable";
                dt.WriteXml(str, true);
                str.Seek(0, SeekOrigin.Begin);
                StreamReader sr = new StreamReader(str);
                xmlstr = sr.ReadToEnd().Replace("1Delete1", "");
            }
            return (xmlstr);
        }
        #endregion        

        #region ConvertDatatableToXML_QueryBuilder
        public string ConvertDatatableToXML_QueryBuilder(DataTable dt)
        {
            string xmlstr;

            XmlDocument doc = new XmlDocument();
            //XmlDeclaration xmlDeclaration = doc.CreateXmlDeclaration("1.0", "UTF-8", null);
            XmlElement root = doc.DocumentElement;
            //  doc.InsertBefore(xmlDeclaration, root);

            XmlElement DocumentElement = doc.CreateElement(string.Empty, "DocumentElement", string.Empty);
            doc.AppendChild(DocumentElement);

            if (dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    XmlElement Contant = doc.CreateElement(string.Empty, "myTable", string.Empty);
                    DocumentElement.AppendChild(Contant);
                    foreach (DataColumn column in dt.Columns)
                    {
                        string rowValue = row[column.ColumnName].ToString();
                        var Heading = column.ColumnName;
                        XmlElement Title = doc.CreateElement(string.Empty, Heading, string.Empty);
                        XmlText textHeading = doc.CreateTextNode(rowValue);
                        Title.AppendChild(textHeading);
                        Contant.AppendChild(Title);
                    }
                    DocumentElement.AppendChild(Contant);
                }
            }
            else
            {
                XmlElement Contant = doc.CreateElement(string.Empty, "myTable", string.Empty);
                DocumentElement.AppendChild(Contant);
                foreach (DataColumn column in dt.Columns)
                {
                    var Heading = column.ColumnName;
                    XmlElement Title = doc.CreateElement(string.Empty, Heading, string.Empty);
                    XmlText textHeading = doc.CreateTextNode(null);
                    Title.AppendChild(textHeading);
                    Contant.AppendChild(Title);
                }
                DocumentElement.AppendChild(Contant);
            }

            xmlstr = doc.InnerXml;

            return (xmlstr);
        }
        #endregion        
    }
}
