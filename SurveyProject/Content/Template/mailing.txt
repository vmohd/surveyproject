﻿<div>
	<span style="color:#0066cc;"><span style="font-size: 16px;">Dear Sir/Madam,</span></span></div>
<div>
	<p>
		<span style="color:#0066cc;"><span style="font-size: 14px;">Participating in Research with xDesignLab (applied to respondents)</span></span></p>
	<p>
		<span style="color:#0066cc;"><span style="font-size: 14px;">You are participating in this research project by $CLIENT$ in an effort to understand the preferences of vacation ownership owners. If you agree to participate in this research, you will be asked to provide your responses on a short questionnaire. </span></span></p>
	<p>
		<span style="color:#0066cc;"><span style="font-size: 14px;">The purpose of this research is to study $PROJECT$. </span></span></p>
	<p>
		<span style="color:#0066cc;"><span style="font-size: 14px;">There are no right or wrong answers – only your opinion. We are simply interested in your thoughts on each question. Please note that you are ineligible to participate in this research if you are less than 18 years old. </span></span></p>
	<p>
		<span style="color:#0066cc;"><span style="font-size: 14px;">Your responses are completely confidential. Information collected from this project will be used solely for research purposes.  </span></span></p>	
		<ul>
            <li style="font-size: 14px;padding: 5px;"><span style="color:#0066cc;">You are at least 18 years of age</span></li>
            <li style="font-size: 14px;padding: 5px;"><span style="color:#0066cc;">Your participation in this research is completely voluntary.</span> </li>
            <li style="font-size: 14px;padding: 5px;"><span style="color:#0066cc;">You do not have to answer any question or questions that you do not wish to answer.</span> </li>
        </ul>   
    <p>
        <span style="color:#0066cc;"><span style="font-size: 14px;">Please be advised that you do not have to participate in this research and you may withdraw from it at any time without consequence. </span></span>
    </p>
    <p>
        <span style="color:#0066cc;"><span style="font-size: 14px;">It will take you approximately 15 minutes to complete this survey. </span></span>
    </p>
    <p>
        <span style="color:#0066cc;"><span style="font-size: 14px;">There are no anticipated risks associated with participation.  </span></span>
    </p>
    <p>
        <span style="color:#0066cc;"><span style="font-size: 14px;">Incentive statement if any goes here! </span></span>
    </p>
    <p>
        <span style="color:#0066cc;"><span style="font-size: 14px;">ELECTRONIC CONSENT: Please select your choice below.</span></span>
    </p>
    	<p>
		&nbsp;</p>
</div>
<div>
    <p>
        <span style="color:#0066cc;"><span style="font-size: 14px;">If you do not wish to participate in the research study, please decline participation by clicking on the "disagree" button.</span></span>
    </p>
</div>
<div>
	<span style="color:#003366;"><strong><span style="font-size: 16px;">Please open the following link in your webbrowser:</span></strong></span></div>
<div>
	&nbsp;</div>