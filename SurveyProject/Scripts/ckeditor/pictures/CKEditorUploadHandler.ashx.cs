﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace GoodHeart.Generic_Handlers
{
    /// <summary>
    /// Summary description for CKEditorUploadHandler
    /// </summary>
    public class CKEditorUploadHandler : IHttpHandler, System.Web.SessionState.IRequiresSessionState
    {


        public void ProcessRequest(HttpContext context)
        {
            try
            {
                //string userID = context.Session["userID"].ToString();
                string ImageFilePath = "~/Scripts/ckeditor/pictures";

                if (!Directory.Exists(context.Server.MapPath(ImageFilePath)))
                {
                    Directory.CreateDirectory(context.Server.MapPath(ImageFilePath));
                }

                //string filepref = Guid.NewGuid().ToString();
                HttpPostedFile uploads = context.Request.Files["upload"];
                string CKEditorFuncNum = context.Request["CKEditorFuncNum"];
                string file = System.IO.Path.GetFileName(uploads.FileName);
                string fileName = Guid.NewGuid().ToString().Substring(0, 5) + "_" + file;
                string tempImageFilePath = ImageFilePath + "/" + fileName;

                // uploads.SaveAs(context.Server.MapPath(".") + "\\BannerImage\\" + filepref + "_" + file);
                uploads.SaveAs(context.Server.MapPath(tempImageFilePath));
                //provide direct URL here
                string url = "../../Scripts/ckeditor/pictures/" + fileName;

                context.Response.Write("<script>window.parent.CKEDITOR.tools.callFunction( " + CKEditorFuncNum + ", \"" + url + "\");</script>");             
                context.ApplicationInstance.CompleteRequest();
              
            }
            catch(Exception ex)
            {

            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}