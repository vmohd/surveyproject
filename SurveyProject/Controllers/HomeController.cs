﻿using SRV.ActionLayer.Admin;
using SRV.ActionLayer.Respondent;
using SRV.BaseLayer.Admin;
using SRV.BaseLayer.Respondent;
using SRV.Utility;
using SurveyProject.Helper;
using SurveyProject.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;

namespace SurveyProject.Controllers
{
    [UnHandledExceptionFilter]
    public class HomeController : Controller
    {
        #region Declaration 
        ProjectBase projectBase = new ProjectBase();
        AdminAction adminAction = new AdminAction();
        SRV.BaseLayer.ActionResult actionResult = new SRV.BaseLayer.ActionResult();
        RespondentAction respondentAction = new RespondentAction();
        CommonMethods comMethods = new CommonMethods();
        #endregion

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Terms()
        {
            return View();
        }

        public ActionResult About()
        {
            return View();
        }

        public ActionResult Contact()
        {
            return View();
        }

        public ActionResult FAQ()
        {
            return View();
        }

        public ActionResult PrivacyPolicy()
        {
            return View();
        }

        public ActionResult QuickSurvey()
        {
            return View();
        }

        public ActionResult Language()
        {
            return View();
        }

        public ActionResult uploadPartial()
        {
            AdminRegisterModel model = new AdminRegisterModel();
            CommonMethods comMethods = new CommonMethods();
            string path = string.Empty;

            // Get User Guid 
            model = comMethods.profileDetails();

            string userId = Convert.ToString(Session["UserId"]).PadLeft(5, '0');
            string folderName = String.Format("{0}{1}{2}{3}", "User_", model.UserGuid.Substring(0, 5), "_", userId);

            // Check and create directory
            path = Server.MapPath("~/Content/Uploads/Images/" + folderName);
            if (!System.IO.Directory.Exists(path))
            {
                System.IO.Directory.CreateDirectory(path);
            }
            var appData = path;
            var images = Directory.GetFiles(appData).Select(x => new ImagesViewModel
            {
                Url = Url.Content(String.Format("{0}{1}{2}{3}", "~/Content/Uploads/Images/", folderName, "/", Path.GetFileName(x)))
            });
            return View(images);
        }

        public void uploadNow(HttpPostedFileWrapper upload)
        {
            try
            {
                AdminRegisterModel model = new AdminRegisterModel();
                CommonMethods comMethods = new CommonMethods();
                string path = string.Empty;

                // Get User Guid 
                model = comMethods.profileDetails();

                string userId = Convert.ToString(Session["UserId"]).PadLeft(5, '0');
                string folderName = String.Format("{0}{1}{2}{3}", "User_", model.UserGuid.Substring(0, 5), "_", userId);

                // Check and create directory
                path = Server.MapPath("~/Content/Uploads/Images/" + folderName);
                if (!System.IO.Directory.Exists(path))
                {
                    System.IO.Directory.CreateDirectory(path);
                }
                if (upload != null)
                {
                    string ImageName = String.Format("{0}{1}{2}", Guid.NewGuid().ToString().Substring(0, 5), "_", upload.FileName.Replace(" ", ""));
                    string uploadPath = System.IO.Path.Combine(path, ImageName);
                    upload.SaveAs(uploadPath);
                }

            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
        }

        public JsonResult GetCountries()
        {
            string json = string.Empty;
            json = Email.ReadFile("Countries.txt");
            return Json(json, JsonRequestBehavior.AllowGet);
        }

        #region UploadGeneralMessageOtherFiles
        public JsonResult UploadGeneralMessageOtherFiles()
        {
            string json = string.Empty;
            string fullpath = string.Empty;
            if (Request.Files.Count > 0)
            {
                string directory = Server.MapPath("~/Content/Uploads/GeneralMessages/OtherFiles/");
                if (!Directory.Exists(directory))
                {
                    Directory.CreateDirectory(directory);
                }
                for (int i = 0; i < Request.Files.Count; i++)
                {
                    var pfb = Request.Files[i];
                    if (pfb != null && pfb.ContentLength > 0)
                    {
                        string fileName = Convert.ToString(Guid.NewGuid()).Substring(0, 5) + Path.GetFileName(pfb.FileName);
                        fullpath += "/Content/Uploads/GeneralMessages/OtherFiles/" + fileName + ",";
                        string path = Path.Combine(directory, fileName);
                        pfb.SaveAs(path);
                    }
                }
            }
            json = "{\"success\":\"true\",\"path\":\"fgfgfg\",\"imsg\":\"sdsds\"}";
            return Json(json, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region TestSurvey Get
        [HttpGet]
        public ActionResult TestSurvey(string id)
        {
            SurveyResponseModel model = new SurveyResponseModel();
            try
            {
                projectBase.Id = Convert.ToInt32(id);
                ViewBag.RedirectProjectId = 0;
                string productMsg = "";
                RespondentController respondent = new RespondentController();
                actionResult = adminAction.Project_LoadById(projectBase);
                if (actionResult.IsSuccess)
                {
                    DataRow dr = actionResult.dtResult.Rows[0];
                    string Guid = dr["ProjectGUID"] != null ? dr["ProjectGUID"].ToString() : "";
                    model = respondent.GetProjectPreview(Guid);
                    string products = string.Empty;
                    if (model.projectModel.lstProductProjectModel != null && model.projectModel.lstProductProjectModel.Count > 0)
                    {
                        foreach (var prod in model.projectModel.lstProductProjectModel)
                        {
                            productMsg += prod.ProductMessage + "~";
                            // products += "{\"ProductLogicalId\":\"" + prod.ProductLogicalId + "\",\"Description\":\"" + prod.Description + "\"},";
                        }
                        productMsg = productMsg.TrimEnd('~');
                        ViewBag.productsList = productMsg;
                        // ViewBag.productsList = "{\"data\":[" + products.Trim(',') + "]}";
                    }
                    ViewBag.ProjectId = model.projectModel.Id;

                    if (model.projectModel.RedirectUrl.Contains("UID"))
                    {
                        SurveyLoadBase surveyLoadBase = new SurveyLoadBase();
                        string TotalRedirectUrl = model.projectModel.RedirectUrl;
                        int endPos = TotalRedirectUrl.IndexOf("&&");
                        int strPos = TotalRedirectUrl.IndexOf("surveyId=");
                        int end = endPos - (strPos + 9);
                        string SurveyGuid = TotalRedirectUrl.Substring(strPos + 9, end);
                        surveyLoadBase.GUID = SurveyGuid;
                        actionResult = new SRV.BaseLayer.ActionResult();
                        actionResult = respondentAction.Survey_LoadByGUID(surveyLoadBase);
                        if (actionResult.IsSuccess)
                        {
                            DataRow drRedirectSurvey = actionResult.dtResult.Rows[0];
                            ViewBag.RedirectProjectId = drRedirectSurvey["Id"] != DBNull.Value ? Convert.ToInt32(drRedirectSurvey["Id"]) : 0;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return View(model);
        }
        #endregion
        
    }
}
