﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SurveyProject.Models;
using SRV.BaseLayer.Admin;
using SRV.ActionLayer.Admin;
using WebMatrix.WebData;
using SurveyProject.Helper;
using SRV.DataLayer.Admin;
using System.Configuration;
using System.Data;
using SRV.BaseLayer.SuperAdmin;
using SRV.ActionLayer.SuperAdmin;
using SRV.BaseLayer.Respondent;
using SRV.ActionLayer.Respondent;
using System.Xml.Linq;
using System.Text;
using SRV.Utility;
using System.Text.RegularExpressions;
using System.IO;
using ClosedXML.Excel;
using System.Web.Hosting;

namespace SurveyProject.Controllers
{
    [CheckLogin]
    [VerifyRoleAccess]
    [UnHandledExceptionFilter]
    public class AdminController : Controller
    {
        #region Declaration
        AdminBase adminBase = new AdminBase();
        ProjectBase projectBase = new ProjectBase();
        ProjecQuotaBase projectQuotaBase = new ProjecQuotaBase();
        ProjectSettingsBase projectSettingsBase = new ProjectSettingsBase();
        QuestionBase questionBase = new QuestionBase();
        ProjectInvitationBase projectInvitationBase = new ProjectInvitationBase();
        ProductProjectBase productProjectBase = new ProductProjectBase();
        SRV.BaseLayer.ActionResult actionResult = new SRV.BaseLayer.ActionResult();
        AdminAction adminAction = new AdminAction();
        UserInfoBase userInfoBase = new UserInfoBase();
        SuperAdminAc superAction = new SuperAdminAc();
        CreateRoleBase role = new CreateRoleBase();
        RespondentAction respondentAction = new RespondentAction();
        string SiteUrl = ConfigurationManager.AppSettings["site"].ToString();
        MatrixTypeBase matrixTypeBase = new MatrixTypeBase();
        CommonMethods comMethods = new CommonMethods();
        string url = ConfigurationManager.AppSettings["site"].ToString();
        GroupEmailMasterBase groupBase = new GroupEmailMasterBase();
        UserDirectoriesBase userDirectoriesBase = new UserDirectoriesBase();
        ProjectDirectoriesBase projectDirectoriesBase = new ProjectDirectoriesBase();

        UrlParameterBase urlParameterBase = new UrlParameterBase();
        UrlParameterAction urlParameterAction = new UrlParameterAction();
        #endregion

        #region BindState
        public JsonResult BindState(int cId = 0)
        {
            List<SelectListItem> stateLst = new List<SelectListItem>();
            try
            {
                adminBase.CountryId = cId;
                actionResult = adminAction.State_LoadAll(adminBase);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    foreach (DataRow dr in actionResult.dtResult.Rows)
                    {
                        stateLst.Add(new SelectListItem { Text = dr["StateName"].ToString(), Value = dr["ID"].ToString() });
                    }
                }

            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return Json(stateLst, JsonRequestBehavior.AllowGet);
        }
        #endregion


        #region CreateProject Get
        [HttpGet]
        public ActionResult CreateProject(int? SPId = 0, int? projId = 0, int? ST = 1)
        {
            var LanguageList = new List<SelectListItem>();
            var ProjectTypeList = new List<SelectListItem>();
            var model = new CreateProjectModel();

            List<ProductProjectModel> lstProductProjectModel = new List<ProductProjectModel>();
            model.lstProductProjectModel = lstProductProjectModel;


            ProjectTypeList.Add(new SelectListItem { Text = "Basic Survey", Value = "1" });
            ProjectTypeList.Add(new SelectListItem { Text = "Product Survey", Value = "2" });
            //    if (SPId == 0)
            //  {
            ProjectTypeList.Add(new SelectListItem { Text = "Kano type Survey", Value = "3" });
            //  }


            try
            {
                actionResult = adminAction.Language_LoadAll();
                if (actionResult.IsSuccess)
                {
                    foreach (DataRow dr in actionResult.dtResult.Rows)
                    {
                        LanguageList.Add(new SelectListItem { Text = dr["Name"].ToString(), Value = dr["Id"].ToString() });
                    }
                }
                if (Convert.ToInt32(projId) > 0)
                    model = GetProjectDetailById(projId);
                model.SurveyType = ST.GetValueOrDefault();
                model.SuperProjectId = SPId.GetValueOrDefault();
                projectBase.Id = projId.GetValueOrDefault();
                if (model.SurveyType == 1)
                {

                    actionResult = adminAction.Project_LoadById(projectBase);
                    if (actionResult.IsSuccess)
                    {
                        DataRow dr = actionResult.dtResult.Rows[0];
                        model.Id = Convert.ToInt32(dr["Id"]);
                        model.SuperProjectId = dr["SuperProjectId"] != DBNull.Value ? Convert.ToInt32(dr["SuperProjectId"]) : 0;
                        model.ProjectType = dr["ProjectType"] != DBNull.Value ? dr["ProjectType"].ToString() : "";
                        model.ProjectName = dr["ProjectName"] != DBNull.Value ? dr["ProjectName"].ToString() : string.Empty;
                        model.LanguageId = dr["LanguageId"] != DBNull.Value ? Convert.ToInt32(dr["LanguageId"]) : 0;
                        model.QuotaQues = dr["QuotaQues"] != DBNull.Value ? dr["QuotaQues"].ToString() : string.Empty;
                    }

                    productProjectBase.ProjectId = model.Id;
                    actionResult = adminAction.ProductProject_LoadById(productProjectBase);
                    if (actionResult.IsSuccess)
                    {
                        foreach (DataRow dr in actionResult.dtResult.Rows)
                        {
                            lstProductProjectModel.Add(new ProductProjectModel
                            {
                                Id = dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0,
                                ProjectId = dr["ProjectId"] != DBNull.Value ? Convert.ToInt32(dr["ProjectId"]) : 0,
                                ProductLogicalId = dr["ProductLogicalId"] != DBNull.Value ? Convert.ToInt32(dr["ProductLogicalId"]) : 0,
                                Description = dr["Description"] != DBNull.Value ? Convert.ToString(dr["Description"]) : string.Empty,
                                DisplayOrder = dr["DisplayOrder"] != DBNull.Value ? Convert.ToInt32(dr["DisplayOrder"]) : 0
                            });
                        }
                        model.lstProductProjectModel = lstProductProjectModel;
                        model.lstProductProjectModel = model.lstProductProjectModel.OrderBy(l => l.DisplayOrder).ToList();
                    }
                }
                else
                {
                    actionResult = adminAction.SuperProject_LoadById(projectBase);
                    if (actionResult.IsSuccess)
                    {
                        DataRow dr = actionResult.dtResult.Rows[0];
                        model.Id = Convert.ToInt32(dr["Id"]);
                        model.SuperProjectName = dr["SuperProjectName"] != DBNull.Value ? dr["SuperProjectName"].ToString() : string.Empty;
                        model.LanguageId = dr["LanguageId"] != DBNull.Value ? Convert.ToInt32(dr["LanguageId"]) : 0;
                        model.SuperProjectOpeningDate = dr["OpeningDate"] != DBNull.Value ? Convert.ToDateTime(dr["OpeningDate"]).ToString("MM/dd/yyyy") : string.Empty;
                        model.SuperProjectClosingDate = dr["ClosingDate"] != DBNull.Value ? Convert.ToDateTime(dr["ClosingDate"]).ToString("MM/dd/yyyy") : string.Empty;
                        model.SuperProjectIsRandomizeSurveys = dr["IsRandomizeSurveys"] != DBNull.Value ? Convert.ToBoolean(dr["IsRandomizeSurveys"].ToString()) : false;
                    }
                }

            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            ViewBag.LanguageList = LanguageList;
            ViewBag.ProjectTypeList = ProjectTypeList;
            return View(model);
        }
        #endregion

        #region SuperProjectSettings
        public ActionResult SuperProjectSettings(int? SPId = 0)
        {
            var model = new CreateProjectModel();
            try
            {
                projectBase.Id = Convert.ToInt32(SPId);
                actionResult = adminAction.SuperProject_LoadById(projectBase);
                if (actionResult.IsSuccess)
                {
                    DataRow dr = actionResult.dtResult.Rows[0];
                    model.Id = Convert.ToInt32(dr["Id"]);
                    model.SuperProjectName = dr["SuperProjectName"] != DBNull.Value ? dr["SuperProjectName"].ToString() : string.Empty;
                    model.LanguageId = dr["LanguageId"] != DBNull.Value ? Convert.ToInt32(dr["LanguageId"]) : 0;
                    model.SuperProjectOpeningDate = dr["OpeningDate"] != DBNull.Value ? Convert.ToDateTime(dr["OpeningDate"]).ToString("MM/dd/yyyy") : string.Empty;
                    model.SuperProjectClosingDate = dr["ClosingDate"] != DBNull.Value ? Convert.ToDateTime(dr["ClosingDate"]).ToString("MM/dd/yyyy") : string.Empty;
                    model.SuperProjectIsRandomizeSurveys = dr["IsRandomizeSurveys"] != DBNull.Value ? Convert.ToBoolean(dr["IsRandomizeSurveys"].ToString()) : false;
                }

                List<CreateProjectModel> lstProjectModel = new List<CreateProjectModel>();
                int UserId = Convert.ToInt32(Session["UserId"]);
                adminBase.Id = UserId;
                SRV.BaseLayer.ActionResult actionResultSubProj = new SRV.BaseLayer.ActionResult();
                actionResultSubProj = adminAction.Poject_LoadAll(adminBase);
                if (actionResultSubProj.dsResult != null && actionResultSubProj.dsResult.Tables[0].Rows.Count > 0)
                {
                    DataTable dtResult = actionResultSubProj.dsResult.Tables[0].Select("SuperProjectId is Not Null and SuperProjectId = '" + SPId + "'").CopyToDataTable();
                    foreach (DataRow dr in dtResult.Rows)
                    {
                        lstProjectModel.Add(new CreateProjectModel
                        {
                            Id = dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0,
                            ProjectName = dr["ProjectName"] != DBNull.Value ? Convert.ToString(dr["ProjectName"]) : "",
                            SuperProjectId = dr["SuperProjectId"] != DBNull.Value ? Convert.ToInt32(dr["SuperProjectId"]) : 0,
                            DisplayOrder = dr["DisplayOrder"] != DBNull.Value ? Convert.ToInt32(dr["DisplayOrder"]) : 0
                        });
                    }
                    lstProjectModel = lstProjectModel.OrderBy(l => l.DisplayOrder).ToList();
                    model.ProjectList = lstProjectModel;
                }
                TempData["SubSurveyIds"] = GetSuperSurveyPreviewData(Convert.ToInt32(SPId));
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return View(model);
        }
        #endregion

        #region CreateProject Post
        [ValidateInput(false)]
        [HttpPost]
        public ActionResult CreateProject(Models.CreateProjectModel model, FormCollection fc)
        {
            int ProjectId = 0;
            int projectType = 0;
            if (model != null)
            {
                projectType = Convert.ToInt32(model.ProjectType);
            }
            try
            {
                if (Session["UserId"] != null)
                {
                    DataTable dt = new DataTable();
                    string status = "created";
                    projectBase.LanguageId = model.LanguageId;
                    projectBase.SuperProjectId = model.SuperProjectId;
                    projectBase.ProjectName = model.ProjectName != null ? model.ProjectName : "";
                    //projectBase.WelcomeMessage = model.WelcomeMessage;
                    projectBase.OwnerId = Convert.ToInt32(Session["UserId"]);
                    projectBase.CreatedBy = Convert.ToInt32(Session["UserId"]);
                    projectBase.ProjectQuota = Convert.ToInt32(model.ProjectQuota);
                    projectBase.ProjectType = Convert.ToInt32(model.ProjectType);
                    projectBase.CloseButtonText = model.CloseButtonText;
                    projectBase.ProductProjectXML = fc["hdnProductProjectXML"] != null && fc["hdnProductProjectXML"] != "" ? Convert.ToString(fc["hdnProductProjectXML"]) : string.Empty;
                    if (model.Id > 0)
                    {
                        projectBase.ModifiedBy = Convert.ToInt32(Session["UserId"]);
                        projectBase.Id = model.Id;
                        status = "updated";
                    }
                    string Data = fc["hdnItems"] != null ? fc["hdnItems"] : "";
                    string[] allData = Data.Trim('~').Split('~');
                    //------ All Fields Related to Project
                    if (model.Id > 0)
                    {
                        projectBase.WelcomeMessage = String.IsNullOrEmpty(model.WelcomeMessage) ? "&nbsp;" : model.WelcomeMessage;
                        projectBase.PageHeader = String.IsNullOrEmpty(model.PageHeader) ? "&nbsp;" : model.PageHeader;
                        projectBase.PageFooter = String.IsNullOrEmpty(model.PageFooter) ? "&nbsp;" : model.PageFooter;
                        projectBase.ProjectCompletionMesssage = String.IsNullOrEmpty(model.ProjectCompletionMesssage) ? "&nbsp;" : model.ProjectCompletionMesssage;
                        projectBase.RedirectUrl = model.RedirectUrl;
                        projectBase.InvitationMessage = model.InvitationMessage;
                        projectBase.ProjectTerminationMessage = String.IsNullOrEmpty(model.TerminationMessageUpper) ? "&nbsp;" : model.TerminationMessageUpper;
                        //projectBase.isActive = model.IsActive;
                        projectBase.IsIncludePageHeader = model.IsIncludePageHeader;
                        projectBase.IsIncludePageFooter = model.IsIncludePageFooter;
                        projectBase.IsIncludeWelcomeMessage = model.IsIncludeWelcomeMessage;
                        projectBase.IsIncludeProjectCompletionMesssage = model.IsIncludeProjectCompletionMesssage;
                        projectBase.IsIncludeTerminationMessage = model.IsIncludeTerminationMessage;
                        projectBase.RedirectParameters = model.RedirectParameters;
                        projectBase.IsRedirectParameter = model.IsRedirectParameter;
                        projectBase.IsIncludeFinishImage = model.IsIncludeFinishImage;
                    }
                    //-----------------
                    actionResult = adminAction.Project_Insert_Update(projectBase);
                    if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                    {
                        ProjectId = Convert.ToInt32(actionResult.dtResult.Rows[0][0]);
                        TempData["ProjId"] = ProjectId;
                        TempData["ProjectType"] = Convert.ToInt32(model.ProjectType);
                        TempData["SuccessMessage"] = "Project " + status + " successfully.";
                    }
                    else
                    {
                        TempData["ErrorMessage"] = "Error in " + status + " the project";
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            //if (projectBase.ProjectType == 2)
            //{
            //    return RedirectToAction("LinkProduct", new { id = ProjectId });
            //}
            //  return RedirectToAction("CreateProject", new { SPId = model.SuperProjectId, projId = model.Id, ST = (model.SuperProjectId > 0 ? 2 : 1) });
            return RedirectToAction("CreateProject", new { SPId = model.SuperProjectId, projId = ProjectId, ST = projectType });
        }
        #endregion

        #region ProjectList Get
        [HttpGet]
        public ActionResult ProjectList()
        {
            CreateProjectModel model = new CreateProjectModel();
            List<CreateProjectModel> ProjectLst = new List<CreateProjectModel>();
            model.ProjectList = ProjectLst;
            try
            {
                int UserId = Convert.ToInt32(Session["UserId"]);
                if (UserId == 0) return View(model);

                adminBase.Id = UserId;
                SRV.BaseLayer.ActionResult actionResultProjects = new SRV.BaseLayer.ActionResult();
                actionResultProjects = adminAction.Poject_LoadAll(adminBase);

                if (actionResultProjects.dsResult.Tables[0] != null && actionResultProjects.dsResult.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in actionResultProjects.dsResult.Tables[0].Rows)
                    {
                        model = new CreateProjectModel();
                        model.Id = dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0;
                        model.ProjectName = dr["ProjectName"] != DBNull.Value ? Convert.ToString(dr["ProjectName"]) : "";
                        model.SuperProjectId = dr["SuperProjectId"] != DBNull.Value ? Convert.ToInt32(dr["SuperProjectId"]) : 0;
                        model.SuperProjectName = dr["SuperProjectName"] != DBNull.Value ? Convert.ToString(dr["SuperProjectName"]) : "N/A";
                        model.CreatedOn = dr["CreatedOn"] != DBNull.Value ? Convert.ToDateTime(dr["CreatedOn"]).ToString("MM/dd/yyyy") : "N/A";
                        model.ModifiedOn = dr["ModifiedOn"] != DBNull.Value ? Convert.ToDateTime(dr["ModifiedOn"]).ToString("MM/dd/yyyy") : "N/A";
                        model.CreatedByName = dr["CreatedBy"] != DBNull.Value ? dr["CreatedBy"].ToString() : "N/A";
                        model.ModifiedByName = dr["ModifiedBy"] != DBNull.Value ? dr["ModifiedBy"].ToString() : "N/A";
                        model.CreatedByName = dr["CreatedBy"] != DBNull.Value ? dr["CreatedBy"].ToString() : "N/A";
                        model.ProjectGuid = dr["ProjectGuid"] != DBNull.Value ? dr["ProjectGuid"].ToString() : "N/A";
                        model.Owner = dr["Owner"] != DBNull.Value ? dr["Owner"].ToString() : "N/A";
                        model.DisplayOrder = dr["DisplayOrder"] != DBNull.Value ? Convert.ToInt32(dr["DisplayOrder"]) : 0;
                        model.ProjectType = dr["ProjectType"] != DBNull.Value ? Convert.ToString(dr["ProjectType"]) : "0";
                        model.DirectoryId = dr["DirectoryId"] != DBNull.Value ? Convert.ToInt32(dr["DirectoryId"]) : 0;
                        model.DirectoryIdCol = dr["DirectoryIdCol"] != DBNull.Value ? Convert.ToInt32(dr["DirectoryIdCol"]) : 0;
                        ProjectLst.Add(model);
                    }
                    ProjectLst = ProjectLst.OrderBy(l => l.DisplayOrder).ToList();
                    model.ProjectList = ProjectLst;
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return View(model);
        }
        #endregion

        #region DisplayProjectList
        public ActionResult DisplayProjectList(int? dir = 0)
        {
            int TotalProject = 0;
            ManageProjectModel model = new ManageProjectModel();
            List<CreateProjectModel> tempLst = new List<CreateProjectModel>();

            List<SuperProjectModel> lstSuperProjectModel = new List<Models.SuperProjectModel>();
            List<CreateProjectModel> lstProjectModel = new List<CreateProjectModel>();
            model.lstSuperProjectModel = lstSuperProjectModel;
            model.lstProjectModel = lstProjectModel;

            try
            {
                int UserId = Convert.ToInt32(Session["UserId"]);
                adminBase.Id = UserId;
                actionResult = adminAction.Poject_LoadAll(adminBase);
                if (actionResult.dsResult != null && actionResult.dsResult.Tables[0].Rows.Count > 0)
                {
                    //DataTable dtResult = actionResult.dsResult.Tables[0].Select("SuperProjectId is Not Null and SuperProjectId <> '0'").CopyToDataTable();
                    DataTable dtResult1 = actionResult.dsResult.Tables[0].Select("SuperProjectId is Null or SuperProjectId = '0'").CopyToDataTable();

                    if (actionResult.dsResult.Tables[0] != null && actionResult.dsResult.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow dr in actionResult.dsResult.Tables[0].Rows)
                        {
                            if (dr["SuperProjectId"] != DBNull.Value && Convert.ToInt32(dr["SuperProjectId"]) != 0)
                            {
                                tempLst.Add(new CreateProjectModel
                                {
                                    Id = dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0,
                                    ProjectName = dr["ProjectName"] != DBNull.Value ? Convert.ToString(dr["ProjectName"]) : "",
                                    ProjectQuota = dr["ProjectQuota"] != DBNull.Value ? Convert.ToInt32(dr["ProjectQuota"]) : 0,
                                    SuperProjectId = dr["SuperProjectId"] != DBNull.Value ? Convert.ToInt32(dr["SuperProjectId"]) : 0,
                                    ProjectType = dr["ProjectType"] != DBNull.Value ? Convert.ToString(dr["ProjectType"]) : "",
                                    ProjectGuid = dr["ProjectGuid"] != DBNull.Value ? Convert.ToString(dr["ProjectGuid"]) : "",
                                    ProductsCount = dr["ProductsCount"] != DBNull.Value ? Convert.ToInt32(dr["ProductsCount"]) : 0,
                                    DisplayOrder = dr["DisplayOrder"] != DBNull.Value ? Convert.ToInt32(dr["DisplayOrder"]) : 0,
                                    CreatedOn = dr["CreatedOn"] != DBNull.Value ? Convert.ToDateTime(dr["CreatedOn"]).ToShortDateString() : "",
                                    ModifiedOn = dr["ModifiedOn"] != DBNull.Value ? Convert.ToDateTime(dr["ModifiedOn"]).ToShortDateString() : "",
                                    DirectoryId = dr["DirectoryId"] != DBNull.Value ? Convert.ToInt32(dr["DirectoryId"]) : 0,
                                    DirectoryIdCol = dr["DirectoryIdCol"] != DBNull.Value ? Convert.ToInt32(dr["DirectoryIdCol"]) : 0
                                });
                            }
                        }
                    }

                    foreach (DataRow dr in dtResult1.Rows)
                    {
                        lstProjectModel.Add(new CreateProjectModel
                        {
                            Id = dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0,
                            ProjectName = dr["ProjectName"] != DBNull.Value ? Convert.ToString(dr["ProjectName"]) : "",
                            ProjectQuota = dr["ProjectQuota"] != DBNull.Value ? Convert.ToInt32(dr["ProjectQuota"]) : 0,
                            SuperProjectId = dr["SuperProjectId"] != DBNull.Value ? Convert.ToInt32(dr["SuperProjectId"]) : 0,
                            ProjectType = dr["ProjectType"] != DBNull.Value ? Convert.ToString(dr["ProjectType"]) : "",
                            ProjectGuid = dr["ProjectGuid"] != DBNull.Value ? Convert.ToString(dr["ProjectGuid"]) : "",
                            ProductsCount = dr["ProductsCount"] != DBNull.Value ? Convert.ToInt32(dr["ProductsCount"]) : 0,
                            DisplayOrder = dr["DisplayOrder"] != DBNull.Value ? Convert.ToInt32(dr["DisplayOrder"]) : 0,
                            CreatedOn = dr["CreatedOn"] != DBNull.Value ? Convert.ToDateTime(dr["CreatedOn"]).ToShortDateString() : "",
                            ModifiedOn = dr["ModifiedOn"] != DBNull.Value ? Convert.ToDateTime(dr["ModifiedOn"]).ToShortDateString() : "",
                            DirectoryId = dr["DirectoryId"] != DBNull.Value ? Convert.ToInt32(dr["DirectoryId"]) : 0,
                            DirectoryIdCol = dr["DirectoryIdCol"] != DBNull.Value ? Convert.ToInt32(dr["DirectoryIdCol"]) : 0
                        });
                    }
                    TotalProject = (lstProjectModel.Count);
                    if (Convert.ToInt32(dir) > 0)
                        lstProjectModel = lstProjectModel.Where(l => l.DirectoryId == Convert.ToInt32(dir)).ToList();
                    model.lstProjectModel = lstProjectModel;
                }

                if (actionResult.dsResult != null && actionResult.dsResult.Tables[1].Rows.Count > 0)
                {
                    foreach (DataRow dr in actionResult.dsResult.Tables[1].Rows)
                    {
                        SuperProjectModel superProjectModel = new SuperProjectModel();
                        List<CreateProjectModel> projectListModel = new List<CreateProjectModel>();
                        superProjectModel.ProjectListModel = projectListModel;

                        superProjectModel.Id = dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0;
                        superProjectModel.ProjectName = Convert.ToString(dr["SuperProjectName"]);
                        superProjectModel.OpeningDate = dr["OpeningDate"] != DBNull.Value ? Convert.ToDateTime(dr["OpeningDate"]).ToShortDateString() : "";
                        superProjectModel.DirectoryId = dr["DirectoryId"] != DBNull.Value ? Convert.ToInt32(dr["DirectoryId"]) : 0;
                        superProjectModel.DirectoryIdCol = dr["DirectoryIdCol"] != DBNull.Value ? Convert.ToInt32(dr["DirectoryIdCol"]) : 0;
                        superProjectModel.CreatedOn = dr["CreatedOn"] != DBNull.Value ? Convert.ToString(dr["CreatedOn"]) : "";
                        //model.lstSuperProjectModel


                        if (tempLst != null && tempLst.Count > 0)
                        {
                            var result = tempLst.Where(m => m.SuperProjectId == superProjectModel.Id).ToList();
                            if (result != null && result.Count > 0)
                            {
                                foreach (var res in result)
                                {
                                    projectListModel.Add(new CreateProjectModel
                                    {
                                        Id = res.Id,
                                        ProjectName = res.ProjectName,
                                        ProjectQuota = res.ProjectQuota,
                                        SuperProjectId = res.SuperProjectId,
                                        ProjectType = res.ProjectType,
                                        ProjectGuid = res.ProjectGuid,
                                        ProductsCount = res.ProductsCount,
                                        DisplayOrder = res.DisplayOrder,
                                        DirectoryId = res.DirectoryId,
                                        DirectoryIdCol = res.DirectoryIdCol,
                                        CreatedOn = res.CreatedOn

                                    });
                                }
                            }
                        }
                        projectListModel = projectListModel.OrderBy(l => l.DisplayOrder).ToList();
                        superProjectModel.ProjectListModel = projectListModel;
                        model.lstSuperProjectModel.Add(superProjectModel);
                    }
                    TotalProject += model.lstSuperProjectModel.Count;
                    if (Convert.ToInt32(dir) > 0)
                        model.lstSuperProjectModel = model.lstSuperProjectModel.Where(l => l.DirectoryId == Convert.ToInt32(dir)).ToList();
                    // model.ProjectList = ProjectLst;
                }

                ViewBag.TotalProjectCount = TotalProject;
                //-----------Ekta 21 April 2016
                //List<UserDirectoriesModel> lstUserDirectoryModel = new List<UserDirectoriesModel>();
                //List<ProjectDirectoriesModel> lstprojectDirectoryModel = new List<ProjectDirectoriesModel>();
                //model.lstUserDirectoryModel = lstUserDirectoryModel;
                //userDirectoriesBase.UserId = Convert.ToInt32(Session["UserId"]);
                //actionResult = adminAction.UserDirectories_LoadByUserId(userDirectoriesBase);
                //if (actionResult.IsSuccess)
                //{
                //    lstUserDirectoryModel = Helper.CommHelper.ConvertTo<UserDirectoriesModel>(actionResult.dtResult);
                //}
                //model.lstUserDirectoryModel = lstUserDirectoryModel;

                //foreach (var item in lstUserDirectoryModel)
                //{
                //    projectDirectoriesBase.DirectoryId = Convert.ToInt32(item.Id);
                //    actionResult = adminAction.ProjectDirectories_LoadById(projectDirectoriesBase);
                //    if (actionResult.IsSuccess)
                //        lstprojectDirectoryModel = Helper.CommHelper.ConvertTo<ProjectDirectoriesModel>(actionResult.dtResult);
                //    item.lstProjectDirectoryModel = lstprojectDirectoryModel;
                //}
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return View("_PartialProjectList", model);
        }
        #endregion

        #region Create Question Get
        [HttpGet]
        public ActionResult CreateQuestion(int? Pid = 0, int? id = 0, int? PType = 0)
        {
            CreateQuestionModel model = new CreateQuestionModel();
            MatrixTypeModel matrixModel = new MatrixTypeModel();
            List<MatrixTypeModel> lstMatxModel = new List<MatrixTypeModel>();
            List<DDLMatrixTypeModel> lstDDLModel = new List<DDLMatrixTypeModel>();
            model.lstMatrixModel = lstMatxModel;
            model.lstDDlMatrixModel = lstDDLModel;
            List<QuotaListModel> QuotaLst = new List<QuotaListModel>();
            var QuestionTypeList = new List<SelectListItem>();
            var GroupTypeList = new List<SelectListItem>();
            List<CreateQuestionModel> QuestionLst = new List<CreateQuestionModel>();
            try
            {
                ViewBag.PId = Pid;
                ViewBag.QuestionId = id;
                ViewBag.ProjectType = PType;
                questionBase.ProjectId = Pid.GetValueOrDefault();

                if (id > 0)
                {
                    questionBase.Id = id.GetValueOrDefault();
                    actionResult = adminAction.Question_LoadById(questionBase);
                    if (actionResult.IsSuccess)
                    {
                        DataRow drFrst = actionResult.dsResult.Tables[0].Rows[0];
                        model.ProjectId = drFrst["ProjectId"] != DBNull.Value ? Convert.ToInt32(drFrst["ProjectId"]) : 0;
                        model.QuestionId = drFrst["Id"] != DBNull.Value ? Convert.ToInt32(drFrst["Id"]) : 0;
                        model.QuestionTitle = drFrst["QuestionTitle"] != DBNull.Value ? drFrst["QuestionTitle"].ToString() : "";
                        model.QuestionTypeId = drFrst["QuestionTypeId"] != DBNull.Value ? Convert.ToInt32(drFrst["QuestionTypeId"]) : 0;
                        model.RedirectUrl = drFrst["RedirectUrl"] != DBNull.Value ? Convert.ToString(drFrst["RedirectUrl"]) : "";
                        model.OptionsCaption = drFrst["OptionsCaption"] != DBNull.Value ? Convert.ToString(drFrst["OptionsCaption"]) : "";
                        model.isAnswersExplicating = drFrst["isAnswersExplicating"] != DBNull.Value ? Convert.ToBoolean(drFrst["isAnswersExplicating"]) : false;
                        model.isAnswersPiped = drFrst["isAnswersPiped"] != DBNull.Value ? Convert.ToBoolean(drFrst["isAnswersPiped"]) : false;
                        model.PipedFrom = drFrst["PipedFrom"] != DBNull.Value ? Convert.ToInt32(drFrst["PipedFrom"]) : 0;
                        model.ExplicatingFrom = drFrst["ExplicatingFrom"] != DBNull.Value ? Convert.ToInt32(drFrst["ExplicatingFrom"]) : 0;
                        model.QuestionTag = drFrst["QuestionTag"] != DBNull.Value ? Convert.ToString(drFrst["QuestionTag"]) : "";
                        model.RedirectQuesParameters = drFrst["RedirectQuesParameters"] != DBNull.Value ? Convert.ToString(drFrst["RedirectQuesParameters"]) : "";
                        ViewBag.PId = drFrst["ProjectId"] != DBNull.Value ? Convert.ToInt32(drFrst["ProjectId"]) : 0;
                        model.RedirectPassCode = drFrst["RedirectPassCode"] != DBNull.Value ? Convert.ToString(drFrst["RedirectPassCode"]) : "";
                        model.IsRedirectParameter = drFrst["IsRedirectParameter"] != DBNull.Value ? Convert.ToBoolean(drFrst["IsRedirectParameter"]) : false;
                        foreach (DataRow drScnd in actionResult.dsResult.Tables[1].Rows)
                        {
                            lstMatxModel.Add(new MatrixTypeModel
                            {
                                Id = drScnd["Id"] != DBNull.Value ? Convert.ToInt32(drScnd["Id"]) : 0,
                                QuestionId = drScnd["QuestionId"] != DBNull.Value ? Convert.ToInt32(drScnd["QuestionId"]) : 0,
                                ColumnName = drScnd["ColumnName"] != DBNull.Value ? Convert.ToString(drScnd["ColumnName"]) : "",
                                GroupId = drScnd["GroupId"] != DBNull.Value ? Convert.ToInt32(drScnd["GroupId"]) : 0,
                                DisplayOrder = drScnd["MatrixDisplayOrder"] != DBNull.Value ? Convert.ToInt32(drScnd["MatrixDisplayOrder"]) : 0
                            });
                        }
                        if (model.QuestionTypeId == 14)
                        {
                            if (actionResult.dsResult.Tables[2] != null && actionResult.dsResult.Tables[2].Rows.Count > 0)
                            {
                                foreach (DataRow dr in actionResult.dsResult.Tables[2].Rows)
                                {
                                    lstDDLModel.Add(new DDLMatrixTypeModel
                                    {
                                        Id = dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0,
                                        QuestionId = dr["QuestionId"] != DBNull.Value ? Convert.ToInt32(dr["QuestionId"]) : 0,
                                        OptionName = dr["OptionName"] != DBNull.Value ? Convert.ToString(dr["OptionName"]) : "",
                                        DDLDisplayOrder = dr["DisplayOrder"] != DBNull.Value ? Convert.ToInt32(dr["DisplayOrder"]) : 0
                                    });
                                }
                                lstDDLModel = lstDDLModel.OrderBy(l => l.DDLDisplayOrder).ToList();
                            }
                        }
                    }
                    model.lstMatrixModel = lstMatxModel;
                    model.lstDDlMatrixModel = lstDDLModel;
                }

                actionResult = adminAction.QuestionType_LoadAll();
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    foreach (DataRow dr in actionResult.dtResult.Rows)
                        QuestionTypeList.Add(new SelectListItem { Text = dr["QuestionType"].ToString(), Value = dr["Id"].ToString() });
                    if (PType == 3)
                        QuestionTypeList = QuestionTypeList.Where(l => l.Value == "13").ToList();
                    //else if (PType == 2)
                    //    QuestionTypeList = QuestionTypeList.Where(l => l.Value != "13").ToList();
                    else
                        QuestionTypeList = QuestionTypeList.Where(l => l.Value != "13").ToList();
                }
                ViewBag.QuestionTypes = QuestionTypeList;
                questionBase.ProjectId = ViewBag.PId;
                SRV.BaseLayer.ActionResult acRes = adminAction.Qusetion_LoadAllByProjectId(questionBase);
                if (acRes.IsSuccess)
                {
                    foreach (DataRow dr in acRes.dtResult.Rows)
                    {
                        QuestionLst.Add(new Models.CreateQuestionModel
                        {
                            ProjectId = dr["ProjectId"] != DBNull.Value ? Convert.ToInt32(dr["ProjectId"]) : 0,
                            QuestionId = dr["questionId"] != DBNull.Value ? Convert.ToInt32(dr["questionId"]) : 0,
                            QuestionType = dr["QuestionType"] != DBNull.Value ? dr["QuestionType"].ToString() : "",
                            QuestionTitle = dr["QuestionTitle"] != DBNull.Value ? Convert.ToString(dr["QuestionTitle"]) : "",
                            QuestionDisplayOrder = dr["DisplayOrder"] != DBNull.Value ? Convert.ToInt32(dr["DisplayOrder"]) : 0
                        });

                    }
                    model.QuestionList = QuestionLst;
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return View(model);
        }
        #endregion

        #region Create Question Post
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult CreateQuestion(CreateQuestionModel model, FormCollection fc)
        {
            int id = fc["hdnQuestionId"] != null ? Convert.ToInt32(fc["hdnQuestionId"]) : 0;
            string columnVal = fc["hdnColumnValues"] != null ? Convert.ToString(fc["hdnColumnValues"]) : "";
            string tableType = fc["hdnTableType"] != null ? Convert.ToString(fc["hdnTableType"]) : "";
            string DDlOptions = fc["hdnDDlValues"] != null ? Convert.ToString(fc["hdnDDlValues"]) : "";
            string DispOrder = fc["hdnDispOrder"] != null ? Convert.ToString(fc["hdnDispOrder"]) : "";
            string ansPipedExplicating = fc["hdnPipedExplicating"] != null ? Convert.ToString(fc["hdnPipedExplicating"]) : "";
            int ProjectType = fc["hdnProjectType"] != null ? Convert.ToInt32(fc["hdnProjectType"]) : 0;
            model.RedirectQuesParameters = fc["hdnQuesParameters"] != null ? Convert.ToString(fc["hdnQuesParameters"]).TrimEnd(',') : "";
            string xmlFile = string.Empty;
            string xmlDDlFile = string.Empty;
            StringBuilder questionFile = new StringBuilder();
            StringBuilder questionDDlFile = new StringBuilder();
            string colVal = string.Empty;
            string colId = string.Empty;
            string groupId = string.Empty;
            string Val = string.Empty;
            string ddlVal = string.Empty;
            string ddlId = string.Empty;
            try
            {
                if (Session["UserId"] != null)
                {
                    if (columnVal != "")
                    {
                        string[] columnValArray = columnVal.TrimEnd(',').Split(',');
                        for (int i = 0; i <= columnValArray.Length - 1; i++)
                        {
                            if (tableType.ToLower() == "kano")
                            {
                                colVal = columnValArray[i].Split('|')[0];
                                Val = columnValArray[i].Split('|')[1];
                                colId = Val.Split('#')[1];
                                groupId = Val.Split('#')[0];
                                questionFile.Append(String.Format("{0}{1}{2}{3}{4}{5}{6}", "<Question><GroupId>", groupId, "</GroupId><ColumnId>", colId, "</ColumnId><ColumnName>", colVal.Trim(), "</ColumnName><Type>radio</Type><DisplayOrder>" + (i + 1) + "</DisplayOrder><IsActive>1</IsActive></Question>"));
                                //xmlFile += "<Question><GroupId>" + groupId + "</GroupId><ColumnId>" + colId + "</ColumnId><ColumnName>" + colVal + "</ColumnName><Type>radio</Type><DisplayOrder>0</DisplayOrder><IsActive>1</IsActive></Question>";

                            }
                            else if (model.QuestionTypeId == 8 || model.QuestionTypeId == 9 || model.QuestionTypeId == 10 || model.QuestionTypeId == 12 || model.QuestionTypeId == 14)
                            {
                                colVal = columnValArray[i].Split('|')[0];
                                colId = columnValArray[i].Split('|')[1];
                                questionFile.Append(String.Format("{0}{1}{2}{3}{4}", "<Question><GroupId>0</GroupId><ColumnId>", colId, "</ColumnId><ColumnName>", colVal.Trim(), "</ColumnName><Type>radio</Type><DisplayOrder>" + (i + 1) + "</DisplayOrder><IsActive>1</IsActive></Question>"));
                                //xmlFile += "<Question><GroupId>0</GroupId><ColumnId>" + colId + "</ColumnId><ColumnName>" + colVal + "</ColumnName><Type>radio</Type><DisplayOrder>0</DisplayOrder><IsActive>1</IsActive></Question>";
                            }
                            else if (model.QuestionTypeId == 11)
                            {
                                colVal = columnValArray[i].Split('|')[0];
                                colId = columnValArray[i].Split('|')[1];
                                questionFile.Append(String.Format("{0}{1}{2}{3}{4}", "<Question><GroupId>0</GroupId><ColumnId>", colId, "</ColumnId><ColumnName>", colVal.Trim(), "</ColumnName><Type>checkbox</Type><DisplayOrder>" + (i + 1) + "</DisplayOrder><IsActive>1</IsActive></Question>"));
                                // xmlFile += "<Question><GroupId>0</GroupId><ColumnId>" + colId + "</ColumnId><ColumnName>" + colVal + "</ColumnName><Type>checkbox</Type><DisplayOrder>0</DisplayOrder><IsActive>1</IsActive></Question>";
                            }
                        }
                        xmlFile = String.Format("{0}{1}{2}", "<QuestionRoot>", questionFile, "</QuestionRoot>");
                        // xmlFile = "<QuestionRoot>" + xmlFile + "</QuestionRoot>";
                    }
                    if (DDlOptions != "" && model.QuestionTypeId == 14)
                    {
                        string[] DDlValArray = DDlOptions.TrimEnd(',').Split(',');
                        for (int i = 0; i <= DDlValArray.Length - 1; i++)
                        {
                            ddlVal = DDlValArray[i].Split('|')[0];
                            string[] DDlvalIdArray = DDlValArray[i].Split('|')[1].Split('#');
                            ddlId = DDlvalIdArray[0];
                            questionDDlFile.Append(String.Format("{0}{1}{2}{3}{4}{5}{6}", "<DDL><DDlId>", ddlId, "</DDlId><DDlName>", ddlVal.Trim(), "</DDlName><DDlCount>", DDlvalIdArray[1], "</DDlCount></DDL>"));
                        }
                        xmlDDlFile = String.Format("{0}{1}{2}", "<DDlRoot>", questionDDlFile, "</DDlRoot>");
                        questionBase.DDlXml = xmlDDlFile;
                    }
                    questionBase.QuestionTitle = model.QuestionTitle;
                    questionBase.QuestionTypeId = model.QuestionTypeId;
                    questionBase.RedirectUrl = model.RedirectUrl;
                    questionBase.OptionsCaption = model.OptionsCaption;
                    questionBase.RedirectPassCode = model.RedirectPassCode;
                    questionBase.RedirectQuesParameters = model.RedirectQuesParameters;
                    questionBase.IsRedirectParameter = model.IsRedirectParameter;
                    questionBase.ProjectId = fc["hdnProjectId"] != "0" && !String.IsNullOrEmpty(fc["hdnProjectId"]) ? Convert.ToInt32(fc["hdnProjectId"]) : 0;
                    questionBase.QusetionId = id;
                    if (ansPipedExplicating == "piped")
                        questionBase.isAnswersPiped = true;
                    else if (ansPipedExplicating == "explicating")
                        questionBase.isAnswersExplicating = true;
                    if (ansPipedExplicating == "piped" || ansPipedExplicating == "explicating")
                    {
                        if (ansPipedExplicating == "piped")
                        {
                            questionBase.PipedFrom = model.PipedFrom;
                        }
                        if (ansPipedExplicating == "explicating")
                        {
                            questionBase.ExplicatingFrom = model.PipedFrom;
                            questionBase.PipedFrom = model.PipedFrom;
                        }
                        questionBase.DispOrderXml = DispOrder;
                    }
                    //if (model.isAnswersExplicating)
                    //    questionBase.ExplicatingFrom = model.ExplicatingFrom;
                    questionBase.QuestionTag = model.QuestionTag;
                    questionBase.IsOptRandomize = model.IsOptRandomize;
                    questionBase.ColumnXml = xmlFile;
                    if (questionBase.ProjectId > 0 && questionBase.QuestionTypeId > 0)
                    {
                        if (id > 0)
                        {
                            questionBase.ModifiedBy = Convert.ToInt32(Session["UserId"]);
                            actionResult = adminAction.Question_Update(questionBase);
                            if (actionResult.IsSuccess)
                            {
                                DataRow dr = actionResult.dtResult.Rows[0];
                                int updateRes = dr["UpdateQuestion"] != DBNull.Value ? Convert.ToInt32(dr["UpdateQuestion"]) : 0;
                                int deleteOptionsRes = dr["DeleteOptions"] != DBNull.Value ? Convert.ToInt32(dr["DeleteOptions"]) : 0;
                                if (updateRes > 0 && (deleteOptionsRes == 1 || deleteOptionsRes == 2))
                                {
                                    TempData["SuccessMessage"] = "Question updated Successfully with deletion of all Answers and conditons.Please add answers again.";
                                }
                                else if (updateRes > 0 && deleteOptionsRes == 0)
                                {
                                    TempData["SuccessMessage"] = "Question updated Successfully.";
                                }
                                else if (updateRes == -1)
                                {
                                    TempData["ErrorMessage"] = "Error in updating the question.";
                                }
                            }
                        }
                        else
                        {
                            questionBase.CreatedBy = Convert.ToInt32(Session["UserId"]);
                            actionResult = adminAction.Question_Insert(questionBase);
                            if (actionResult.IsSuccess)
                            {
                                int questionId = Convert.ToInt32(actionResult.dtResult.Rows[0][0]);
                                TempData["QuestionId"] = questionId;
                                TempData["SuccessMessage"] = "Question created Successfully.";
                            }
                            else
                            {
                                TempData["ErrorMessage"] = "Error in saving the question.";
                            }
                            TempData["QuesTypeId"] = model.QuestionTypeId;

                            return RedirectToAction("InsertOptions", new { Pid = questionBase.ProjectId });
                        }
                    }
                    else
                        TempData["ErrorMessage"] = "Request error!!";
                }
            }
            catch (Exception ex)
            {
                // ErrorReporting.WebApplicationError(ex);
            }
            // return RedirectToAction("QuestionList", new { Id = questionBase.ProjectId });
            return RedirectToAction("QuestionList", new { Id = questionBase.ProjectId, projectType = ProjectType });
        }
        #endregion

        #region QuestionList Get
        public ActionResult QuestionList(int? id = 0, int? projectType = 0, string searchTag = "")
        {
            ViewBag.ProjectId = id;
            ViewBag.ProjectType = projectType;
            //Session["ProjectId"] = id;
            CreateQuestionModel model = new CreateQuestionModel();
            List<CreateQuestionModel> QuestionLst = new List<CreateQuestionModel>();
            // model.QuestionList = QuestionLst;
            try
            {
                if (Session["UserId"] != null)
                {
                    questionBase.ProjectId = id.GetValueOrDefault();
                    actionResult = adminAction.Qusetion_LoadAllByProjectId(questionBase);
                    if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                    {
                        foreach (DataRow dr in actionResult.dtResult.Rows)
                        {
                            model = new CreateQuestionModel();
                            model.ProjectId = dr["ProjectId"] != DBNull.Value ? Convert.ToInt32(dr["ProjectId"]) : 0;
                            model.QuestionId = dr["questionId"] != DBNull.Value ? Convert.ToInt32(dr["questionId"]) : 0;
                            model.QuestionType = dr["QuestionType"] != DBNull.Value ? dr["QuestionType"].ToString() : "";
                            model.QuestionTitle = dr["QuestionTitle"] != DBNull.Value ? Convert.ToString(dr["QuestionTitle"]) : "";
                            model.QuestionTag = dr["QuestionTag"] != DBNull.Value ? Convert.ToString(dr["QuestionTag"]) : "N/A";
                            // model.Option = Convert.ToString(dr["QuestionType"]);
                            QuestionLst.Add(model);
                        }
                        if (!String.IsNullOrEmpty(searchTag))
                            model.QuestionList = QuestionLst.Where(v => v.QuestionTag.ToLower().Contains(searchTag.ToLower())).ToList();
                        else
                            model.QuestionList = QuestionLst;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return View(model);
        }
        #endregion

        #region _PartialViewQueList
        public PartialViewResult _PartialViewQueList(int? id = 0, int? projectType = 0, string searchTag = "")
        {
            ViewBag.ProjectId = id;
            ViewBag.ProjectType = projectType;
            //Session["ProjectId"] = id;
            CreateQuestionModel model = new CreateQuestionModel();
            List<CreateQuestionModel> QuestionLst = new List<CreateQuestionModel>();
            // model.QuestionList = QuestionLst;
            try
            {
                if (Session["UserId"] != null)
                {
                    questionBase.ProjectId = id.GetValueOrDefault();
                    actionResult = adminAction.Qusetion_LoadAllByProjectId(questionBase);
                    if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                    {
                        foreach (DataRow dr in actionResult.dtResult.Rows)
                        {
                            model = new CreateQuestionModel();
                            model.ProjectId = dr["ProjectId"] != DBNull.Value ? Convert.ToInt32(dr["ProjectId"]) : 0;
                            model.QuestionId = dr["questionId"] != DBNull.Value ? Convert.ToInt32(dr["questionId"]) : 0;
                            model.QuestionType = dr["QuestionType"] != DBNull.Value ? dr["QuestionType"].ToString() : "";
                            model.QuestionTitle = dr["QuestionTitle"] != DBNull.Value ? Convert.ToString(dr["QuestionTitle"]) : "";
                            model.QuestionTag = dr["QuestionTag"] != DBNull.Value ? Convert.ToString(dr["QuestionTag"]) : "N/A";
                            // model.Option = Convert.ToString(dr["QuestionType"]);
                            QuestionLst.Add(model);
                        }
                        if (!String.IsNullOrEmpty(searchTag))
                            model.QuestionList = QuestionLst.Where(v => v.QuestionTag.ToLower().Contains(searchTag.ToLower())).ToList();
                        else
                            model.QuestionList = QuestionLst;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return PartialView(model);
        }
        #endregion

        #region DeleteQuestion
        public ActionResult DeleteQuestion(int QuesId, int Pid, int PType)
        {
            try
            {
                questionBase.Id = Convert.ToInt32(QuesId);
                actionResult = adminAction.DeleteQuestion_ById(questionBase);
                if (actionResult.IsSuccess)
                    TempData["SuccessMessage"] = "Question deleted Successfully.";
                else
                    TempData["ErrorMessage"] = "Error in deleting the question.";
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return RedirectToAction("QuestionList", new { Id = Pid, projectType = PType });
        }
        #endregion

        #region Bind Option
        public ActionResult BindOption(string Id)
        {
            List<OptionListModel> optionList = new List<OptionListModel>();

            if (!string.IsNullOrEmpty(Id))
            {
                questionBase.Id = Convert.ToInt32(Id);
                actionResult = adminAction.Option_LoadAllById(questionBase);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    optionList = CommHelper.ConvertTo<OptionListModel>(actionResult.dtResult);
                }
            }
            return Json(Newtonsoft.Json.JsonConvert.SerializeObject(optionList), JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region InsertOptions Get
        public ActionResult InsertOptions(int? Pid = 0)
        {
            CreateOptionsModel model = new CreateOptionsModel();
            ViewBag.ProjectId = Pid;
            var questionList = new List<SelectListItem>();
            List<QuotaListModel> QuotaLst = new List<QuotaListModel>();
            var QuestionTypeList = new List<SelectListItem>();
            try
            {
                if (Session["UserId"] != null)
                {
                    // Get Project's Total Quota
                    projectQuotaBase.ProjectId = Convert.ToInt32(Pid);
                    actionResult = adminAction.ProjectQuota_LoadById(projectQuotaBase);

                    if (actionResult.IsSuccess)
                    {
                        DataRow dr = actionResult.dtResult.Rows[0];
                        ViewBag.ProjectQuota = dr["ProjectQuota"] != DBNull.Value && !String.IsNullOrEmpty(Convert.ToString(dr["ProjectQuota"])) && Convert.ToInt32(dr["ProjectQuota"]) != 0 ? Convert.ToString(dr["ProjectQuota"]) : string.Empty;
                    }

                    questionBase.ProjectId = Pid.GetValueOrDefault();
                    actionResult = adminAction.QuestionsLoadAll_ByProjectId(questionBase);
                    if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                    {
                        foreach (DataRow dr in actionResult.dtResult.Rows)
                        {
                            questionList.Add(new SelectListItem { Text = (dr["DisplayOrder"] != DBNull.Value ? Convert.ToString(dr["DisplayOrder"]) : "") + " . " + ReplaceHtml.ReplaceText(Server.HtmlDecode(dr["QuestionTitle"].ToString())), Value = dr["Id"].ToString() });
                        }
                    }

                    model.QuotaList = QuotaLst;

                    questionBase.ProjectId = Pid.GetValueOrDefault();
                    actionResult = adminAction.QuotaId_LoadByProjectId(questionBase);
                    if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                    {
                        foreach (DataRow dr in actionResult.dtResult.Rows)
                        {
                            QuotaListModel modelquota = new QuotaListModel();
                            modelquota.QuotaId = dr["id"] != DBNull.Value ? Convert.ToInt32(dr["id"]) : 0;
                            modelquota.QutotaType = dr["QuotaType"] != DBNull.Value ? Convert.ToString(dr["QuotaType"]) : "";
                            QuotaLst.Add(modelquota);
                        }
                    }
                    model.QuotaList = QuotaLst;
                }
                ViewBag.Questions = questionList;
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return View(model);
        }
        #endregion

        #region InsertOptions Post
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult InsertOptions(CreateOptionsModel model, FormCollection fc)
        {
            bool IsMatrix = false;
            string addEditCase = fc["hdnAddEdit"] != null ? fc["hdnAddEdit"] : "";
            string TableData = fc["hdnTableData"] != null ? fc["hdnTableData"] : "";
            string SkipData = fc["hdnSkipData"] != null ? fc["hdnSkipData"] : "";
            int ProjectId = fc["hdnProjectId"] != null ? Convert.ToInt32(fc["hdnProjectId"]) : 0;
            int quesTypeId = Convert.ToInt32(fc["hdnQuesType"]) != 0 ? Convert.ToInt32(fc["hdnQuesType"]) : Convert.ToInt32(fc["hdnQuestypeIdD"]);
            string columnData = fc["hdnColumnData"] != null ? fc["hdnColumnData"] : "";

            string msg = "saved";
            var quesId = "<QuestionId></QuestionId>";
            StringBuilder options = new StringBuilder();//string.Empty;
            //string columns = string.Empty;
            //string optionsNo = string.Empty;
            try
            {
                if (Session["UserId"] != null)
                {

                    questionBase.QuestionTypeId = quesTypeId;
                    questionBase.QusetionId = model.QuestionId;
                    questionBase.ModifiedBy = Convert.ToInt32(Session["UserId"]);
                    //Delete All options and related data in case of Update.
                    if (addEditCase == "Edit")
                    {
                        actionResult = adminAction.DeleteOptionsBeforeUpdate_ByQuestionId(questionBase);
                        if (actionResult.IsSuccess)
                        {
                            msg = "updated";
                        }
                    }
                    if (quesTypeId == 3 || quesTypeId == 9 || quesTypeId == 15)
                    {
                        questionBase.RangeMin = !String.IsNullOrEmpty(model.StartRange) ? Convert.ToDecimal(model.StartRange) : 0;
                        questionBase.RangeMax = !String.IsNullOrEmpty(model.EndRange) ? Convert.ToDecimal(model.EndRange) : 0;
                    }
                    if (quesTypeId == 4)
                    {
                        questionBase.ConstantSum = !String.IsNullOrEmpty(model.TotalValue) ? Convert.ToDecimal(model.TotalValue) : 0;
                    }
                    actionResult = adminAction.QuestionUpdate_ByQuestionId(questionBase);
                    if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                    {
                        int Result = Convert.ToInt32(actionResult.dtResult.Rows[0][0]);
                        if (quesTypeId == 7 || quesTypeId == 3 || quesTypeId == 15)
                        {
                            if (Result > 0)
                            {
                                TempData["SuccessMessage"] = "Your Answer " + msg + " Successfully.";
                            }
                            else
                            {
                                TempData["ErrorMessage"] = "Your Answer not " + msg + ".";
                            }
                        }
                        else if (String.IsNullOrEmpty(TableData))
                        {
                            if (Result > 0)
                            {
                                TempData["SuccessMessage"] = "Your Answer " + msg + " Successfully.";
                            }
                            else
                            {
                                TempData["ErrorMessage"] = "Your Answer not " + msg + ".";
                            }
                        }
                        else
                        {
                            quesId = "<QuestionId>" + questionBase.QusetionId + "</QuestionId>";
                        }
                    }

                    if (quesTypeId != 7 || quesTypeId != 3)
                    {
                        if (!String.IsNullOrEmpty(TableData))
                        {

                            //Single Punch or Double Punch
                            if ((quesTypeId == 1 || quesTypeId == 2))
                            {
                                string[] tblArray = TableData.TrimEnd(']').Split(']');
                                string[] skipArray = SkipData.TrimEnd(',').Split(',');
                                if (tblArray != null && tblArray.Length > 0)
                                {
                                    for (int i = 0; i <= tblArray.Length - 1; i++)
                                    {
                                        string[] splitSkipData = skipArray[i].Split('~');
                                        string[] splitOption = tblArray[i].TrimEnd('~').Split('~');
                                        if (splitOption != null || splitOption.ToString() != "" && splitOption.Length > 0)
                                        {
                                            options.Append(String.Format("{0}{1}{2}{3}{4}", "<optionInfo> ", quesId, "<optionTitle>", splitOption[0].Split('*')[0].Trim(), "</optionTitle>"));

                                            if (splitOption[1] != null || splitOption[1].ToString() != "" && splitOption[1].Length > 0)
                                            {
                                                string[] splitIsTerminate = splitOption[1].ToString().Split('|');
                                                if (splitIsTerminate[0] != "false")
                                                {
                                                    options.Append(String.Format("{0}{1}{2}", "<IsValuedConditional>", splitIsTerminate[1], "</IsValuedConditional>"));
                                                    string[] splitQuota = splitIsTerminate[2].TrimEnd('#').Split('#');
                                                    options.Append(String.Format("{0}{1}{2}", "<IsValueWithQuotaConditional>", splitQuota[0], "</IsValueWithQuotaConditional>"));
                                                    if (splitQuota[0] != "false")
                                                    {
                                                        if (splitQuota.Length > 1)
                                                        {
                                                            if (splitQuota[1] != null && splitQuota[1].Length > 0)
                                                            {
                                                                options.Append(String.Format("{0}{1}{2}", "<QuotaId>", splitQuota[1].Trim(','), "</QuotaId>"));
                                                            }
                                                        }
                                                        else
                                                        {
                                                            options.Append("<QuotaId></QuotaId>");
                                                        }
                                                    }
                                                    else
                                                    {
                                                        options.Append("<QuotaId></QuotaId>");
                                                    }
                                                }
                                                else
                                                {
                                                    options.Append(String.Format("{0}{1}{2}", "<IsValuedConditional>", splitIsTerminate[0], "</IsValuedConditional>"));
                                                    options.Append(String.Format("{0}{1}{2}", "<IsValueWithQuotaConditional>", splitIsTerminate[0], "</IsValueWithQuotaConditional>"));
                                                    options.Append("<QuotaId></QuotaId>");
                                                }
                                                string[] splitSkip = splitSkipData[1].Split('-');
                                                options.Append(String.Format("{0}{1}{2}{3}{4}{5}{6}{7}{8}{9}{10}", "<MaxResponses>", splitSkipData[0], "</MaxResponses><IsSkip>", splitSkip[0], "</IsSkip><SkipQuestionId>", splitSkip[1], "</SkipQuestionId><DisplayOrder>", (i + 1), "</DisplayOrder><OptionRedirectUrl>", splitOption[0].Split('*')[1].Trim().Replace("&", "&amp;"), "</OptionRedirectUrl></optionInfo>"));
                                            }
                                        }
                                    }
                                }
                            }
                            //Range  or Matrix Type
                            else if (quesTypeId == 6)
                            {
                                string[] tblArray = columnData.TrimEnd(']').Split(']');
                                //string[] tblArrayNo=
                                string[] skipArray = SkipData.TrimEnd(',').Split(',');
                                if (tblArray != null && tblArray.Length > 0)
                                {
                                    for (int i = 0; i <= tblArray.Length - 1; i++)
                                    {
                                        string[] splitSkipData = skipArray[i].Split('~');
                                        string[] splitOption = tblArray[i].TrimEnd('~').Split('~');

                                        if (splitOption != null || splitOption.ToString() != "" && splitOption.Length > 0)
                                        {
                                            options.Append(String.Format("{0}{1}{2}{3}{4}", "<optionInfo> ", quesId, "<optionTitle>", splitOption[0].Split('*')[0].Trim(), "</optionTitle>"));

                                            if (splitOption[1] != null || splitOption[1].ToString() != "" && splitOption[1].Length > 0)
                                            {
                                                string[] splitIsTerminate = splitOption[1].ToString().Split('|');
                                                if (splitIsTerminate[0] != "false")
                                                {
                                                    options.Append(String.Format("{0}{1}{2}", "<IsValuedConditional>", splitIsTerminate[1], "</IsValuedConditional>"));
                                                    string[] splitQuota = splitIsTerminate[2].TrimEnd('#').Split('#');
                                                    options.Append(String.Format("{0}{1}{2}", "<IsValueWithQuotaConditional>", splitQuota[0], "</IsValueWithQuotaConditional>"));
                                                    if (splitQuota[0] != "false")
                                                    {
                                                        if (splitQuota.Length > 1)
                                                        {
                                                            if (splitQuota[1] != null && splitQuota[1].Length > 0)
                                                            {
                                                                options.Append(String.Format("{0}{1}{2}", "<QuotaId>", splitQuota[1].TrimEnd(','), "</QuotaId>"));
                                                            }
                                                        }
                                                        else
                                                        {
                                                            options.Append("<QuotaId></QuotaId>");
                                                        }
                                                    }
                                                    else
                                                    {
                                                        options.Append("<QuotaId></QuotaId>");
                                                    }
                                                }
                                                else
                                                {
                                                    options.Append(String.Format("{0}{1}{2}", "<IsValuedConditional>", splitIsTerminate[0], "</IsValuedConditional>"));
                                                    options.Append(String.Format("{0}{1}{2}", "<IsValueWithQuotaConditional>", splitIsTerminate[0], "</IsValueWithQuotaConditional>"));
                                                    options.Append("<QuotaId></QuotaId>");
                                                }
                                                string[] splitSkip = splitSkipData[1].Split('-');
                                                options.Append(String.Format("{0}{1}{2}{3}{4}{5}{6}{7}{8}{9}{10}", "<MaxResponses>", splitSkipData[0], "</MaxResponses><IsSkip>", splitSkip[0], "</IsSkip><SkipQuestionId>", splitSkip[1], "</SkipQuestionId><DisplayOrder>", (i + 1), "</DisplayOrder><OptionRedirectUrl>", splitOption[0].Split('*')[1].Trim().Replace("&", "&amp;"), "</OptionRedirectUrl></optionInfo>"));
                                            }
                                        }
                                    }
                                }
                                IsMatrix = true;
                                //string[] skipArray = SkipData.TrimEnd(',').Split(',');
                                //string[] tblArray = TableData.TrimEnd('~').Split('~');
                                //if (tblArray != null && tblArray.Length > 0)
                                //{
                                //    for (int i = 0; i <= tblArray.Length - 1; i++)
                                //    {
                                //        string[] splitSkipData = skipArray[i].Split('~');
                                //        string[] splitSkip = splitSkipData[1].Split('-');
                                //        options += "<optionInfo>" + quesId + "<optionTitle>" + tblArray[i] + "</optionTitle>" + "";
                                //        options += "<MaxResponses>" + splitSkipData[0] + "</MaxResponses><IsSkip>" + splitSkip[0] + "</IsSkip><SkipQuestionId>" + splitSkip[1] + "</SkipQuestionId></optionInfo>";
                                //    }
                                //}
                                //if (quesTypeId == 6)
                                //{
                                //    //string columnData = fc["hdnColumnData"] != null ? fc["hdnColumnData"] : "";
                                //    string[] tblColumn = columnData.TrimEnd('~').Split('~');
                                //    if (tblColumn != null && tblColumn.Length > 0)
                                //    {
                                //        for (int i = 0; i <= tblColumn.Length - 1; i++)
                                //        {
                                //            columns += "<columnInfo>" + quesId + "<columnName>" + tblColumn[i] + "</columnName>" + "</columnInfo>";
                                //        }
                                //    }
                                //    string columnXml = "<columnsList>" + columns + "</columnsList>";
                                //    questionBase.ColumnXml = columnXml;
                                //    questionBase.Type = "radio";
                                //    actionResult = adminAction.InsertMatrix_Column(questionBase);
                                //}
                            }
                            else  // Constant Sum or Ranking
                            {
                                string[] skipArray = SkipData.TrimEnd(',').Split(',');
                                //string[] tblArray = TableData.TrimEnd('~').Split('~');
                                string[] tblArray = TableData.TrimEnd('*').Split('*');
                                if (tblArray != null && tblArray.Length > 0)
                                {
                                    for (int i = 0; i <= tblArray.Length - 1; i++)
                                    {
                                        string[] splitSkipData = skipArray[i].Split('~');
                                        string[] splitSkip = splitSkipData[1].Split('-');
                                        //options.Append(String.Format("{0}{1}{2}{3}{4}", "<optionInfo>", quesId, "<optionTitle>", tblArray[i].Trim(), "</optionTitle>"));
                                        options.Append(String.Format("{0}{1}{2}{3}{4}", "<optionInfo>", quesId, "<optionTitle>", tblArray[i].Split('~')[0].Trim(), "</optionTitle>"));
                                        options.Append(String.Format("{0}{1}{2}{3}{4}{5}{6}{7}{8}{9}{10}", "<MaxResponses>", splitSkipData[0], "</MaxResponses><IsSkip>", splitSkip[0], "</IsSkip><SkipQuestionId>", splitSkip[1], "</SkipQuestionId><DisplayOrder>", (i + 1), "</DisplayOrder><OptionRedirectUrl>", tblArray[i].Split('~')[1].Trim().Replace("&", "&amp;"), "</OptionRedirectUrl></optionInfo>"));
                                    }
                                }
                            }

                            string xmlFile = String.Format("{0}{1}{2}", "<optionsList>", options.ToString(), "</optionsList>");

                            questionBase.Xml = xmlFile;
                            questionBase.ProjectId = ProjectId;
                            questionBase.IsMatrix = IsMatrix;
                            actionResult = adminAction.Option_Insert(questionBase);
                            if (actionResult.IsSuccess)
                            {
                                TempData["SuccessMessage"] = "Your Answer " + msg + " Successfully.";
                            }
                            else
                            {
                                TempData["ErrorMessage"] = "Your Answer not " + msg + ".";
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }

            return RedirectToAction("InsertOptions", new { Pid = ProjectId });
        }
        #endregion

        #region QuestionTypeJson
        public JsonResult QuestionTypeJson(int? Id = 0)
        {
            int questionTypeId = 0;
            try
            {
                questionBase.QusetionId = Id.GetValueOrDefault();
                actionResult = adminAction.QuestionsType_ByQuestionId(questionBase);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    questionTypeId = Convert.ToInt32(actionResult.dtResult.Rows[0]["QuestionTypeId"]);
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return Json(questionTypeId, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region AnswersGet
        public JsonResult AnswersGet(int Id, int questionTypeId)
        {
            string json = string.Empty;
            try
            {
                questionBase.QusetionId = Id;
                questionBase.QuestionTypeId = questionTypeId;
                actionResult = adminAction.Answers_LoadAllByQuestionId(questionBase);
                if (actionResult.IsSuccess)
                {
                    json = Newtonsoft.Json.JsonConvert.SerializeObject(actionResult.dsResult);
                    if (actionResult.dsResult != null)
                    {
                        if (questionTypeId == 1 || questionTypeId == 2)
                        {
                            if (actionResult.dsResult.Tables[1] != null && actionResult.dsResult.Tables[1].Rows.Count > 0)
                            {
                                string jsonRes = ManageAnswers(actionResult.dsResult.Tables[1]);

                                if (!String.IsNullOrEmpty(jsonRes))
                                {
                                    json = json.Substring(0, json.Length - 1) + ",";
                                    jsonRes = jsonRes.Substring(1);
                                    json += jsonRes;
                                }
                            }
                        }
                        else if (questionTypeId != 7)
                        {
                            string jsonRes = string.Empty;
                            SRV.BaseLayer.ActionResult ProjectAction = new SRV.BaseLayer.ActionResult();
                            ProjectAction = adminAction.ProjectQuestionDetails_LoadByQuestionid(questionBase);
                            if (ProjectAction.IsSuccess)
                            {
                                DataRow dr = ProjectAction.dtResult.Rows[0];
                                jsonRes = String.Format("{0}{1}{2}{3}{4}{5}{6}", "\"QuestionDetail\":[{\"ConstantSum\":\"", (dr["ConstantSum"] != DBNull.Value ? dr["ConstantSum"].ToString() : "0"), "\",\"RangeMin\":\"", (dr["RangeMin"] != DBNull.Value ? dr["RangeMin"].ToString() : "0"), "\",\"RangeMax\":\"", (dr["RangeMax"] != DBNull.Value ? dr["RangeMax"].ToString() : "0"), "\"}]}");
                                //jsonRes = "\"QuestionDetail\":[{\"ConstantSum\":\"" + (dr["ConstantSum"] != DBNull.Value ? dr["ConstantSum"].ToString() : "0") + "\",\"RangeMin\":\"" + (dr["RangeMin"] != DBNull.Value ? dr["RangeMin"].ToString() : "0") + "\",\"RangeMax\":\"" + (dr["RangeMax"] != DBNull.Value ? dr["RangeMax"].ToString() : "0") + "\"}]}";
                                json = json.Substring(0, json.Length - 1) + ",";
                                json += jsonRes;
                            }
                        }
                        if (questionTypeId == 6)
                        {
                            if (actionResult.dsResult.Tables[1] != null && actionResult.dsResult.Tables[1].Rows.Count > 0)
                            {
                                string jsonRes = ManageAnswersMatrix(actionResult.dsResult.Tables[1]);

                                if (!String.IsNullOrEmpty(jsonRes))
                                {
                                    json = json.Substring(0, json.Length - 1) + ",";
                                    jsonRes = jsonRes.Substring(1);
                                    json += jsonRes;
                                }
                            }
                        }
                    }
                }
                if (questionTypeId == 3 || questionTypeId == 15)
                {
                    SRV.BaseLayer.ActionResult ProjectAction = new SRV.BaseLayer.ActionResult();
                    ProjectAction = adminAction.ProjectQuestionDetails_LoadByQuestionid(questionBase);
                    if (ProjectAction.IsSuccess)
                    {
                        DataRow dr = ProjectAction.dtResult.Rows[0];
                        json = String.Format("{0}{1}{2}{3}{4}{5}{6}", "{\"QuestionDetail\":[{\"ConstantSum\":\"", (dr["ConstantSum"] != DBNull.Value ? dr["ConstantSum"].ToString() : "0"), "\",\"RangeMin\":\"", (dr["RangeMin"] != DBNull.Value ? dr["RangeMin"].ToString() : "0"), "\",\"RangeMax\":\"", (dr["RangeMax"] != DBNull.Value ? dr["RangeMax"].ToString() : "0"), "\"}]}");
                        //json = "{\"QuestionDetail\":[{\"ConstantSum\":\"" + (dr["ConstantSum"] != DBNull.Value ? dr["ConstantSum"].ToString() : "0") + "\",\"RangeMin\":\"" + (dr["RangeMin"] != DBNull.Value ? dr["RangeMin"].ToString() : "0") + "\",\"RangeMax\":\"" + (dr["RangeMax"] != DBNull.Value ? dr["RangeMax"].ToString() : "0") + "\"}]}";
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return Json(json, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region ManageAnswers Get
        public string ManageAnswers(DataTable dt)
        {
            ManageAnswerValues manage = new ManageAnswerValues();
            string json = string.Empty;
            var answersValueLst = new List<AnswersValue>();
            var answersValueWithQuotaLst = new List<AnswersValueWithQuota>();

            try
            {
                string conditionExpression = dt.Rows[0]["ConditionExpression"].ToString();
                string[] conditionExpressionArray = conditionExpression.Split('[');
                if (conditionExpressionArray != null && conditionExpressionArray.Length > 0)
                {
                    manage.ProjectId = conditionExpressionArray[0] != null ? Convert.ToInt32(conditionExpressionArray[0]) : 0;
                    if (!String.IsNullOrEmpty(conditionExpressionArray[1]))
                    {
                        string[] values = conditionExpressionArray[1].Split('{');
                        if (values != null && values.Length > 0)
                        {
                            if (!String.IsNullOrEmpty(values[0]))
                            {
                                string[] onlyValues = values[0].TrimEnd('|').Split('|');
                                if (onlyValues != null && onlyValues.Length > 0)
                                {
                                    foreach (var item in onlyValues)
                                    {
                                        string QuestionId = item.Substring(1, item.Length - 2);
                                        answersValueLst.Add(new Models.AnswersValue
                                        {
                                            QuestionId = Convert.ToInt32(QuestionId)
                                        });
                                    }
                                }
                            }
                            if (!String.IsNullOrEmpty(values[1]))
                            {
                                string[] valueWithQuota = values[1].Substring(0, values[1].Length - 2).Split('(');

                                if (valueWithQuota != null && valueWithQuota.Length > 0)
                                {
                                    for (int i = 1; i <= valueWithQuota.Length - 1; i++)
                                    {
                                        valueWithQuota[i] = valueWithQuota[i].Substring(0, valueWithQuota[i].Length - 1);
                                        string[] quota = valueWithQuota[i].Split(',');
                                        if (quota != null && quota.Length > 0)
                                        {
                                            answersValueWithQuotaLst.Add(new Models.AnswersValueWithQuota
                                            {
                                                QuestionId = Convert.ToInt32(quota[0]),
                                                optionId = Convert.ToInt32(quota[1])
                                            });
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            //manage.answerValueList = answersValueLst;
            // manage.answerValueWithQuotaList = answersValueWithQuotaLst;
            json = Newtonsoft.Json.JsonConvert.SerializeObject(new { answersValueLst, answersValueWithQuotaLst });
            return json;
        }
        #endregion

        #region ManageAnswersMatrix Get
        public string ManageAnswersMatrix(DataTable dt)
        {
            ManageAnswerValuesMatrix manage = new ManageAnswerValuesMatrix();
            string json = string.Empty;
            var answersValueLst = new List<AnswersValueMatrix>();
            var answersValueWithQuotaLst = new List<AnswersValueWithQuotaMatrix>();

            try
            {
                string conditionExpression = dt.Rows[0]["ConditionExpressionMatrix"].ToString();
                string[] conditionExpressionArray = conditionExpression.Split('[');
                if (conditionExpressionArray != null && conditionExpressionArray.Length > 0)
                {
                    manage.ProjectId = conditionExpressionArray[0] != null ? Convert.ToInt32(conditionExpressionArray[0]) : 0;
                    if (!String.IsNullOrEmpty(conditionExpressionArray[1]))
                    {
                        string[] values = conditionExpressionArray[1].Split('{');
                        if (values != null && values.Length > 0)
                        {
                            if (!String.IsNullOrEmpty(values[0]))
                            {
                                string[] onlyValues = values[0].TrimEnd('|').Split('|');
                                if (onlyValues != null && onlyValues.Length > 0)
                                {
                                    foreach (var item in onlyValues)
                                    {
                                        string QuestionId = item.Substring(1, item.Length - 2);
                                        answersValueLst.Add(new Models.AnswersValueMatrix
                                        {
                                            QuestionId = Convert.ToString(QuestionId)
                                        });
                                    }
                                }
                            }
                            if (!String.IsNullOrEmpty(values[1]))
                            {
                                string[] valueWithQuota = values[1].Substring(0, values[1].Length - 2).Split('(');

                                if (valueWithQuota != null && valueWithQuota.Length > 0)
                                {
                                    for (int i = 1; i <= valueWithQuota.Length - 1; i++)
                                    {
                                        valueWithQuota[i] = valueWithQuota[i].Substring(0, valueWithQuota[i].Length - 1);
                                        string[] quota = valueWithQuota[i].Split(',');
                                        if (quota != null && quota.Length > 0)
                                        {
                                            answersValueWithQuotaLst.Add(new Models.AnswersValueWithQuotaMatrix
                                            {
                                                QuestionId = Convert.ToString(quota[0]),
                                                optionId = Convert.ToString(quota[1])
                                            });
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            //manage.answerValueList = answersValueLst;
            // manage.answerValueWithQuotaList = answersValueWithQuotaLst;
            json = Newtonsoft.Json.JsonConvert.SerializeObject(new { answersValueLst, answersValueWithQuotaLst });
            return json;
        }
        #endregion

        #region EditProfile Get
        public ActionResult EditProfile()
        {
            AdminRegisterModel model = new AdminRegisterModel();
            try
            {
                model = comMethods.profileDetails();
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return View(model);
        }
        #endregion

        #region EditProfile Post
        [HttpPost]
        public ActionResult EditProfile(AdminRegisterModel model)
        {
            try
            {
                adminBase.Id = model.Id;
                adminBase.UserGuid = Guid.NewGuid().ToString();
                adminBase.FirstName = model.FirstName;
                adminBase.LastName = model.LastName;
                adminBase.Email = model.Email;
                adminBase.Gender = model.Gender;
                adminBase.Address = model.Address;
                adminBase.CountryId = Convert.ToInt32(model.CountryId);
                adminBase.StateId = Convert.ToInt32(model.StateId);
                adminBase.City = model.City;
                adminBase.ZipCode = model.ZipCode;
                adminBase.SecurityQuestion = model.SecurityQuestion;
                adminBase.AnswerToSecurityQuestion = model.AnswerToSecurityQuestion;
                adminBase.CountryOfResidenceId = model.CountryOfResidenceId;
                adminBase.LanguageId = model.LanguageId;
                adminBase.CompanyName = model.CompanyName;
                adminBase.PhoneNumber = model.PhoneNumber;
                adminBase.SendEmail = model.sendEmail;
                adminBase.OtherCommunications = model.OtherCommunications;

                actionResult = adminAction.Register_InsertUpdate(adminBase);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    int status = Convert.ToInt32(actionResult.dtResult.Rows[0][0]);
                    if (status > 0)
                    {
                        TempData["SuccessMessage"] = "Your profile is updated successfully.";
                    }
                    else
                    {
                        TempData["ErrorMessage"] = "Something went wrong.Please try again later";
                    }

                }
                else
                {
                    TempData["ErrorMessage"] = "Something went wrong.Please try again later";
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return RedirectToAction("EditProfile");
        }
        #endregion

        #region ChangePassword
        [HttpPost]
        public ActionResult ChangePassword(AdminRegisterModel model)
        {
            try
            {
                adminBase.Password = model.Password;
                adminBase.Id = Session["UserId"] != null ? Convert.ToInt32(Session["UserId"]) : 0;
                actionResult = adminAction.ChangePassword_ById(adminBase);
                if (actionResult.IsSuccess)
                {
                    int res = Convert.ToInt32(actionResult.dtResult.Rows[0][0]);
                    if (res > 0)
                    {
                        TempData["SuccessMessage"] = "Password changed successfully.";
                    }
                    else
                    {
                        TempData["ErrorMessage"] = "Something went wrong.Please try again later";
                    }
                }
                else
                {
                    TempData["ErrorMessage"] = "Something went wrong.Please try again later";
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            TempData["tab"] = "changePassword";
            return RedirectToAction("EditProfile");
        }
        #endregion

        #region GetProjectDetailById
        public CreateProjectModel GetProjectDetailById(int? id = 0)
        {
            CreateProjectModel model = new CreateProjectModel();
            List<ProductProjectModel> lstproductModel = new List<ProductProjectModel>();
            try
            {
                projectBase.Id = Convert.ToInt32(id);
                actionResult = adminAction.Project_LoadById(projectBase);
                if (actionResult.IsSuccess)
                {
                    DataRow dr = actionResult.dtResult.Rows[0];
                    model.Id = dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0;
                    model.SuperProjectId = dr["SuperProjectId"] != DBNull.Value ? Convert.ToInt32(dr["SuperProjectId"]) : 0;
                    model.ProjectName = dr["ProjectName"] != DBNull.Value ? Convert.ToString(dr["ProjectName"]) : "";
                    model.WelcomeMessage = dr["WelcomeMessage"] != DBNull.Value && !String.IsNullOrEmpty(dr["WelcomeMessage"].ToString()) ? Convert.ToString(dr["WelcomeMessage"]) : Email.ReadFile("Welcome.txt");
                    model.PageHeader = dr["PageHeader"] != DBNull.Value && !String.IsNullOrEmpty(dr["PageHeader"].ToString()) ? Convert.ToString(dr["PageHeader"]) : Email.ReadFile("Header.txt");
                    model.PageFooter = dr["PageFooter"] != DBNull.Value && !String.IsNullOrEmpty(dr["PageFooter"].ToString()) ? Convert.ToString(dr["PageFooter"]) : Email.ReadFile("Footer.txt");
                    model.ProjectCompletionMesssage = dr["ProjectCompletionMesssage"] != DBNull.Value && !String.IsNullOrEmpty(dr["ProjectCompletionMesssage"].ToString()) ? Convert.ToString(dr["ProjectCompletionMesssage"]) : Email.ReadFile("Complete.txt");
                    model.RedirectUrl = dr["RedirectUrl"] != DBNull.Value ? Convert.ToString(dr["RedirectUrl"]) : "";
                    model.InvitationMessage = dr["InvitationMessage"] != DBNull.Value ? Convert.ToString(dr["InvitationMessage"]) : "";
                    model.OwnerId = dr["OwnerId"] != DBNull.Value ? Convert.ToInt32(dr["OwnerId"]) : 0;
                    model.CreatedBy = dr["CreatedBy"] != DBNull.Value ? Convert.ToInt32(dr["CreatedBy"]) : 0;
                    model.ProjectQuota = dr["ProjectQuota"] != DBNull.Value ? Convert.ToInt32(dr["ProjectQuota"]) : 0;
                    model.TerminationMessageUpper = dr["ProjectTerminationMessage"] != DBNull.Value && !String.IsNullOrEmpty(dr["ProjectTerminationMessage"].ToString()) ? Convert.ToString(dr["ProjectTerminationMessage"]) : Email.ReadFile("TerminateUpper.txt");
                    model.ProjectGuid = dr["ProjectGuid"] != DBNull.Value ? Convert.ToString(dr["ProjectGuid"]) : "";
                    model.IsIncludePageHeader = dr["IsIncludePageHeader"] != DBNull.Value ? Convert.ToBoolean(dr["IsIncludePageHeader"]) : false;
                    model.IsIncludePageFooter = dr["IsIncludePageFooter"] != DBNull.Value ? Convert.ToBoolean(dr["IsIncludePageFooter"]) : false;
                    model.IsIncludeProjectCompletionMesssage = dr["IsIncludeProjectCompletionMesssage"] != DBNull.Value ? Convert.ToBoolean(dr["IsIncludeProjectCompletionMesssage"]) : false;
                    model.IsIncludeTerminationMessage = dr["IsIncludeTerminationMessage"] != DBNull.Value ? Convert.ToBoolean(dr["IsIncludeTerminationMessage"]) : false;
                    model.IsIncludeWelcomeMessage = dr["IsIncludeWelcomeMessage"] != DBNull.Value ? Convert.ToBoolean(dr["IsIncludeWelcomeMessage"]) : false;
                    model.ProjectType = dr["ProjectType"] != DBNull.Value ? Convert.ToString(dr["ProjectType"]) : "";
                    model.RedirectParameters = dr["RedirectParameters"] != DBNull.Value ? Convert.ToString(dr["RedirectParameters"]) : "";
                    model.IsRedirectParameter = dr["IsRedirectParameter"] != DBNull.Value ? Convert.ToBoolean(dr["IsRedirectParameter"]) : false;
                    model.IsIncludeFinishImage = dr["IsIncludeFinishImage"] != DBNull.Value ? Convert.ToBoolean(dr["IsIncludeFinishImage"]) : false;
                    model.LanguageId = dr["LanguageId"] != DBNull.Value ? Convert.ToInt32(dr["LanguageId"]) : 0;
                    model.CloseButtonText = dr["CloseButtonText"] != DBNull.Value ? Convert.ToString(dr["CloseButtonText"]) : "";
                    // model.ProductMessage = dr["ProductMessage"] != DBNull.Value ? Convert.ToString(dr["ProductMessage"]) : "";
                }

                productProjectBase.ProjectId = Convert.ToInt32(id);
                actionResult = adminAction.ProductProject_LoadById(productProjectBase);
                if (actionResult.IsSuccess)
                {
                    foreach (DataRow dr in actionResult.dtResult.Rows)
                    {
                        lstproductModel.Add(new ProductProjectModel
                        {
                            Id = dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0,
                            ProjectId = dr["ProjectId"] != DBNull.Value ? Convert.ToInt32(dr["ProjectId"]) : 0,
                            ProductLogicalId = dr["ProductLogicalId"] != DBNull.Value ? Convert.ToInt32(dr["ProductLogicalId"]) : 0,
                            Description = dr["Description"] != DBNull.Value ? Convert.ToString(dr["Description"]) : string.Empty,
                            DisplayOrder = dr["DisplayOrder"] != DBNull.Value ? Convert.ToInt32(dr["DisplayOrder"]) : 0,
                            ProductMessage = dr["ProductMessage"] != DBNull.Value ? Convert.ToString(dr["ProductMessage"]) : ""
                        });
                    }
                    lstproductModel = lstproductModel.OrderBy(l => l.DisplayOrder).ToList();
                    string productXml = "<ProductProjects>";
                    foreach (var prod in lstproductModel)
                    {
                        productXml += "<Project><Id>" + prod.Id + "</Id><ProductLogicalId>" + prod.ProductLogicalId +
                            "</ProductLogicalId><Description>" + prod.Description + "</Description><DisplayOrder>" + prod.DisplayOrder + "</DisplayOrder></Project>";
                    }
                    productXml += "</ProductProjects>";
                    model.ProductProjectXML = productXml;
                }

            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return model;
        }
        #endregion

        #region UpdateProjectDetails
        public bool UpdateProjectDetails(CreateProjectModel model)
        {
            actionResult = new SRV.BaseLayer.ActionResult();
            try
            {
                projectBase.Id = model.Id;
                projectBase.ProjectName = model.ProjectName;
                projectBase.WelcomeMessage = String.IsNullOrEmpty(model.WelcomeMessage) ? "&nbsp;" : model.WelcomeMessage;
                projectBase.PageHeader = String.IsNullOrEmpty(model.PageHeader) ? "&nbsp;" : model.PageHeader;
                projectBase.PageFooter = String.IsNullOrEmpty(model.PageFooter) ? "&nbsp;" : model.PageFooter;
                projectBase.ProjectCompletionMesssage = String.IsNullOrEmpty(model.ProjectCompletionMesssage) ? "&nbsp;" : model.ProjectCompletionMesssage;
                projectBase.RedirectUrl = model.RedirectUrl;
                projectBase.InvitationMessage = model.InvitationMessage;
                projectBase.OwnerId = model.OwnerId;
                projectBase.CreatedBy = model.CreatedBy;
                projectBase.ProjectQuota = Convert.ToInt32(model.ProjectQuota);
                projectBase.ProjectTerminationMessage = String.IsNullOrEmpty(model.TerminationMessageUpper) ? "&nbsp;" : model.TerminationMessageUpper;
                //projectBase.isActive = model.IsActive;
                projectBase.ModifiedBy = model.CreatedBy;
                projectBase.IsIncludePageHeader = model.IsIncludePageHeader;
                projectBase.IsIncludePageFooter = model.IsIncludePageFooter;
                projectBase.IsIncludeWelcomeMessage = model.IsIncludeWelcomeMessage;
                projectBase.IsIncludeProjectCompletionMesssage = model.IsIncludeProjectCompletionMesssage;
                projectBase.IsIncludeTerminationMessage = model.IsIncludeTerminationMessage;
                projectBase.SuperProjectId = model.SuperProjectId;
                projectBase.ProjectType = Convert.ToInt32(model.ProjectType);
                projectBase.ProductProjectXML = model.ProductProjectXML;
                projectBase.RedirectParameters = model.RedirectParameters;
                projectBase.IsRedirectParameter = model.IsRedirectParameter;
                projectBase.IsIncludeFinishImage = model.IsIncludeFinishImage;
                projectBase.LanguageId = model.LanguageId;
                projectBase.CloseButtonText = model.CloseButtonText;
                //     projectBase.ProductMessage = model.ProductMessage;
                actionResult = adminAction.Project_Insert_Update(projectBase);
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return actionResult.IsSuccess;
        }
        #endregion

        #region ProjectLayout Get
        [HttpGet]
        public ActionResult ProjectLayout(int? id = 0)
        {
            ViewBag.ProjectId = id;
            CreateProjectModel model = new CreateProjectModel();
            model = GetProjectDetailById(id);
            return View(model);
        }
        #endregion

        #region ProjectLayout Post
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult ProjectLayout(CreateProjectModel model)
        {
            if (UpdateProjectDetails(model))
            {
                TempData["SuccessMessage"] = "Project layout saved successfully";
            }
            else
            {
                TempData["ErrorMessage"] = "Some Error occurred!!";
            }
            return RedirectToAction("ProjectLayout", new { id = model.Id });
        }
        #endregion

        #region ProjectStartup Get
        [HttpGet]
        public ActionResult ProjectStartup(int? id = 0)
        {
            ViewBag.ProjectId = id;
            CreateProjectModel model = new CreateProjectModel();
            model = GetProjectDetailById(id);
            return View(model);
        }
        #endregion

        #region ProjectStartup Post
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult ProjectStartup(CreateProjectModel model)
        {
            if (UpdateProjectDetails(model))
            {
                TempData["SuccessMessage"] = "Project startup welcome message saved successfully";
            }
            else
            {
                TempData["ErrorMessage"] = "Some Error occurred!!";
            }
            return RedirectToAction("ProjectStartup", new { id = model.Id });
        }
        #endregion

        #region ProjectTransition Get
        [HttpGet]
        public ActionResult ProjectTransition(int? id = 0)
        {
            ViewBag.ProjectId = id;
            CreateProjectModel model = new CreateProjectModel();
            model = GetProjectDetailById(id);
            return View("ProjectCompletion", model);
        }
        #endregion

        #region ProjectCompletion Post
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult ProjectCompletion(CreateProjectModel model, FormCollection fc)
        {
            try
            {
                if (model.SuperProjectId == 0)
                {
                    model.RedirectParameters = fc["hdnParameters"] != null ? Convert.ToString(fc["hdnParameters"]).TrimEnd(',') : "";
                    if (UpdateProjectDetails(model))
                    {
                        TempData["SuccessMessage"] = "Project redirect url saved successfully";
                    }
                    else
                    {
                        TempData["ErrorMessage"] = "Some Error occurred!!";
                    }
                }
                else
                {
                    TempData["ErrorMessage"] = "Redirect url can't be set in Sub survey";
                }

            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return RedirectToAction("ProjectTransition", new { id = model.Id });
        }
        #endregion

        #region ProjectMailing Get
        public ActionResult ProjectMailing(int? id = 0, int? SPId = 0)
        {
            ViewBag.ProjectId = id;
            ProjectMailingModel model = new ProjectMailingModel();
            try
            {
                string projectGuid = string.Empty;
                string projectName = string.Empty;
                string url = string.Empty;
                userInfoBase.Id = Convert.ToInt32(Session["UserId"]);
                actionResult = adminAction.UserInfo_LoadById(userInfoBase);
                if (actionResult.IsSuccess)
                {
                    DataRow dr = actionResult.dtResult.Rows[0];
                    model.FromName = (dr["FirstName"] != DBNull.Value ? Convert.ToString(dr["FirstName"]) : "") + " " + (dr["LastName"] != DBNull.Value ? Convert.ToString(dr["LastName"]) : "");
                }
                model.subject = "Please take part in my survey";
                if (SPId == 0)
                {
                    projectBase.Id = Convert.ToInt32(id);
                }
                else
                {
                    List<CreateProjectModel> lstProjectModel = new List<CreateProjectModel>();
                    int UserId = Convert.ToInt32(Session["UserId"]);
                    adminBase.Id = UserId;
                    SRV.BaseLayer.ActionResult actionResultProj = new SRV.BaseLayer.ActionResult();
                    actionResultProj = adminAction.Poject_LoadAll(adminBase);
                    if (actionResultProj.dsResult != null && actionResultProj.dsResult.Tables[0].Rows.Count > 0)
                    {
                        DataTable dtResult = actionResultProj.dsResult.Tables[0].Select("SuperProjectId is Not Null and SuperProjectId = '" + SPId + "'").CopyToDataTable();
                        foreach (DataRow dr in dtResult.Rows)
                        {
                            lstProjectModel.Add(new CreateProjectModel
                            {
                                Id = dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0,
                                SuperProjectId = dr["SuperProjectId"] != DBNull.Value ? Convert.ToInt32(dr["SuperProjectId"]) : 0,
                            });
                        }
                        lstProjectModel = lstProjectModel.OrderByDescending(l => l.Id).ToList();
                    }
                    projectBase.Id = lstProjectModel.FirstOrDefault().Id;
                }
                actionResult = adminAction.Project_LoadById(projectBase);
                if (actionResult.IsSuccess)
                {
                    DataRow dr = actionResult.dtResult.Rows[0];
                    projectGuid = dr["ProjectGUID"] != DBNull.Value ? Convert.ToString(dr["ProjectGUID"]) : "";
                    projectName = dr["ProjectName"] != DBNull.Value ? Convert.ToString(dr["ProjectName"]) : "";
                    model.ProjectId = dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0;
                    model.SuperProjectId = dr["SuperProjectId"] != DBNull.Value ? Convert.ToInt32(dr["SuperProjectId"]) : 0;
                }
                //url = SiteUrl + "/Respondent/survey?surveyId=" + projectGuid + "&&uid=[-invitationid-]";
                url = SiteUrl + "/Respondent/survey?surveyId=[-surveyguid-]&&uid=[-invitationid-]&&_xS=[-superSurveyId-]";
                //  model.InvitationMessage = Email.ReadFile("mailing.txt") + "<div><span style='color:#3399FF;'>" + url + "</span></div><p>&nbsp;</p><div><span style='font-size:16px;'><span style='color: rgb(178, 34, 34);'>Kind regards,</span></span></div><div><span style='font-size:16px;'><span style='color: rgb(178, 34, 34);'>" + model.FromName + "</span></span></div>";
                string msg = Email.ReadFile("mailing.txt") + "<div><span style='color:#3399FF;'>" + url + "</span></div><p>&nbsp;</p><div><span style='font-size:16px;'><span style='color: rgb(178, 34, 34);'>Kind regards,</span></span></div><div><span style='font-size:16px;'><span style='color: rgb(178, 34, 34);'>" + model.FromName + "</span></span></div>";
                msg = msg.Replace("$CLIENT$", model.FromName);
                msg = msg.Replace("$PROJECT$", projectName);
                model.InvitationMessage = msg;
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return View(model);
        }
        #endregion

        #region ProjectMailing Post
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult ProjectMailing(ProjectMailingModel model, FormCollection fc)
        {
            try
            {
                List<GroupEmailsModel> GroupEmails = new List<GroupEmailsModel>();
                string mailingSubSurveyProdCount = fc["hdnSubSurveyEmailCount"] ?? fc["hdnSubSurveyEmailCount"];//   vijay@nextolive.com_149|6*148|2*,ekta@nextolive.com_149|6*148|2*,
                string[] tempArr = { };
                string[] mailingSubSurveyArray = mailingSubSurveyProdCount != "" ? mailingSubSurveyProdCount.TrimEnd(',').Split(',') : tempArr;
                string ArrSubsurveyEmail = string.Empty;
                string[] ArrSubSurveyIdCount;
                int ArrSubsurveyId = 0;
                int ArrSubsurveyCount = 0;
                //SRV.BaseLayer.ActionResult actionResultRedirection = new SRV.BaseLayer.ActionResult();
                //projectInvitationBase.EmailXml = fc["hdnRedirectUrls"] ?? fc["hdnRedirectUrls"];
                //actionResultRedirection = adminAction.SubProjectsRedirection_Update(projectInvitationBase);

                string xmlEmails = string.Empty;
                string returnedXML = string.Empty;
                List<CreateProjectModel> lstProjectModel = new List<CreateProjectModel>();
                string[] strarr = model.InvitationList.Trim().Split(',');

                bool isRandomizeSurveys = false;
                DataTable dtTempSurveys = new DataTable();

                if (model.SuperProjectId == 0)
                {
                    for (int i = 0; i <= strarr.Length - 1; i++)
                    {
                        //---------check group present in invitation or not
                        if (strarr[i].IndexOf("GRP_") != -1)
                        {
                            GroupEmails = GroupEmailCompleteList(strarr[i]);
                            foreach (var eml in GroupEmails)
                            {
                                xmlEmails += "<Invitation><Email>" + eml.Email + "</Email><ProjectId>" + model.ProjectId + "</ProjectId></Invitation>";
                            }
                        }
                        else
                            xmlEmails += "<Invitation><Email>" + strarr[i] + "</Email><ProjectId>" + model.ProjectId + "</ProjectId></Invitation>";
                    }
                    xmlEmails = "<Invitations>" + xmlEmails + "</Invitations>";
                    projectInvitationBase.EmailXml = xmlEmails;

                    //projectInvitationBase.ProjectId = model.ProjectId;
                    actionResult = adminAction.ProjectInvitation_Insert_Update(projectInvitationBase);

                    // Check if This is Product Type Survey then make an entry for this survey on Position Table
                    projectBase.Id = model.ProjectId;
                    SRV.BaseLayer.ActionResult actionResultProject = new SRV.BaseLayer.ActionResult();
                    actionResultProject = adminAction.Project_LoadById(projectBase);
                    if (actionResultProject.IsSuccess)
                    {
                        DataRow dr = actionResultProject.dtResult.Rows[0];
                        string projectType = dr["ProjectType"] != DBNull.Value ? Convert.ToString(dr["ProjectType"]) : "1";
                        if (projectType == "2")
                        {
                            for (int i = 0; i <= strarr.Length - 1; i++)
                            {
                                List<string> projectIds = new List<string>();
                                projectIds.Add(Convert.ToString(model.ProjectId));
                                string[] emailArr = { strarr[i] };
                                CreateRecordInPositionTable(emailArr, projectIds, model.SuperProjectId, ArrSubsurveyEmail, ArrSubsurveyId, ArrSubsurveyCount);
                            }
                        }
                    }

                }
                else
                {

                    xmlEmails = string.Empty;
                    int UserId = Convert.ToInt32(Session["UserId"]);
                    adminBase.Id = UserId;
                    SRV.BaseLayer.ActionResult actionResultProj = new SRV.BaseLayer.ActionResult();
                    actionResultProj = adminAction.Poject_LoadAll(adminBase);
                    if (actionResultProj.dsResult != null && actionResultProj.dsResult.Tables[0].Rows.Count > 0)
                    {

                        DataTable dtResult = actionResultProj.dsResult.Tables[0].Select("SuperProjectId is Not Null and SuperProjectId = '" + model.SuperProjectId + "'").CopyToDataTable();
                        Random rnd = new Random();

                        dtTempSurveys.Columns.Add("Id", typeof(int));
                        dtTempSurveys.Columns.Add("SuperProjectId", typeof(int));
                        dtTempSurveys.Columns.Add("DisplayOrder", typeof(int));
                        dtTempSurveys.Columns.Add("Random", typeof(string));

                        if (dtResult != null && dtResult.Rows.Count > 0)
                        {
                            isRandomizeSurveys = dtResult.Rows[0]["IsRandomizeSurveys"] != DBNull.Value ? Convert.ToBoolean(dtResult.Rows[0]["IsRandomizeSurveys"].ToString()) : false;

                            foreach (DataRow dr in dtResult.Rows)
                            {
                                DataRow drTempSurvey = dtTempSurveys.NewRow();
                                drTempSurvey["Id"] = dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0;
                                drTempSurvey["SuperProjectId"] = dr["SuperProjectId"] != DBNull.Value ? Convert.ToInt32(dr["SuperProjectId"]) : 0;
                                drTempSurvey["DisplayOrder"] = dr["DisplayOrder"] != DBNull.Value ? Convert.ToInt32(dr["DisplayOrder"]) : 0;
                                drTempSurvey["Random"] = rnd.Next(1, 10).ToString();
                                dtTempSurveys.Rows.Add(drTempSurvey);
                            }
                            if (isRandomizeSurveys)
                            {
                                dtTempSurveys.DefaultView.Sort = "Random asc";
                                dtTempSurveys = dtTempSurveys.DefaultView.ToTable();
                            }
                            else
                            {
                                dtTempSurveys.DefaultView.Sort = "DisplayOrder asc";
                                dtTempSurveys = dtTempSurveys.DefaultView.ToTable();
                            }
                            foreach (DataRow dr in dtTempSurveys.Rows)
                            {
                                lstProjectModel.Add(new CreateProjectModel
                                {
                                    Id = dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0,
                                    SuperProjectId = dr["SuperProjectId"] != DBNull.Value ? Convert.ToInt32(dr["SuperProjectId"]) : 0,
                                });
                            }
                        }
                        //lstProjectModel = lstProjectModel.OrderByDescending(l => l.Id).ToList();
                        lstProjectModel = lstProjectModel.ToList();

                        for (int i = 0; i <= strarr.Length - 1; i++)
                        {
                            foreach (DataRow dr in dtTempSurveys.Rows)
                            {
                                dr["Random"] = rnd.Next(1, 10).ToString();
                            }
                            if (isRandomizeSurveys)
                            {
                                dtTempSurveys.DefaultView.Sort = "Random asc";
                                dtTempSurveys = dtTempSurveys.DefaultView.ToTable();
                            }
                            else
                            {
                                dtTempSurveys.DefaultView.Sort = "DisplayOrder asc";
                                dtTempSurveys = dtTempSurveys.DefaultView.ToTable();
                            }
                            // Update the lstProjectModel
                            lstProjectModel = new List<CreateProjectModel>();
                            foreach (DataRow dr in dtTempSurveys.Rows)
                            {
                                lstProjectModel.Add(new CreateProjectModel
                                {
                                    Id = dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0,
                                    SuperProjectId = dr["SuperProjectId"] != DBNull.Value ? Convert.ToInt32(dr["SuperProjectId"]) : 0,
                                });
                            }
                            //--------check group exist
                            if (strarr[i].IndexOf("GRP_") != -1)
                            {
                                GroupEmails = GroupEmailCompleteList(strarr[i]);
                                foreach (var eml in GroupEmails)
                                {
                                    xmlEmails += "<Invitation><Email>" + eml.Email + "</Email><ProjectId>" + (dtTempSurveys.Rows[0]["Id"] != DBNull.Value ? Convert.ToInt32(dtTempSurveys.Rows[0]["Id"]) : 0) + "</ProjectId></Invitation>";
                                }
                            }
                            //-------------
                            else
                                xmlEmails += "<Invitation><Email>" + strarr[i] + "</Email><ProjectId>" + (dtTempSurveys.Rows[0]["Id"] != DBNull.Value ? Convert.ToInt32(dtTempSurveys.Rows[0]["Id"]) : 0) + "</ProjectId></Invitation>";

                            // Make this Entry To Position Table

                            if (model.SuperProjectId == 0)
                            {
                                List<string> projectIds = new List<string>();
                                projectIds.Add(Convert.ToString(projectInvitationBase.ProjectId));
                                string[] emailArr = { strarr[i] };
                                CreateRecordInPositionTable(emailArr, projectIds, model.SuperProjectId, ArrSubsurveyEmail, ArrSubsurveyId, ArrSubsurveyCount);
                            }
                            else
                            {

                                List<string> projectIds = new List<string>();
                                foreach (var item in lstProjectModel)
                                    projectIds.Add(Convert.ToString(item.Id));
                                string[] emailArr = { strarr[i] };

                                //--------------to check product count
                                if (mailingSubSurveyArray.Length > 0)
                                {
                                    for (int k = 0; k <= mailingSubSurveyArray.Length - 1; k++)   // vijay@nextolive.com_149|6*148|2,ekta@nextolive.com_149|6*148|2
                                    {
                                        if (String.IsNullOrEmpty(mailingSubSurveyArray[k])) continue;
                                        ArrSubsurveyEmail = mailingSubSurveyArray[k].TrimEnd('*').Split('#')[0];//vijay@nextolive.com  
                                        ArrSubSurveyIdCount = mailingSubSurveyArray[k].TrimEnd('*').Split('#')[1].Split('*');//149|6  148|2
                                        for (int m = 0; m <= ArrSubSurveyIdCount.Length - 1; m++)
                                        {
                                            ArrSubsurveyId = Convert.ToInt32(ArrSubSurveyIdCount[m].Split('|')[0]);
                                            ArrSubsurveyCount = Convert.ToInt32(ArrSubSurveyIdCount[m].Split('|')[1]);
                                            CreateRecordInPositionTable(emailArr, projectIds, model.SuperProjectId, ArrSubsurveyEmail, ArrSubsurveyId, ArrSubsurveyCount);
                                        }
                                    }
                                }
                                else
                                    CreateRecordInPositionTable(emailArr, projectIds, model.SuperProjectId, ArrSubsurveyEmail, ArrSubsurveyId, ArrSubsurveyCount);
                            }

                        }
                        xmlEmails = "<Invitations>" + xmlEmails + "</Invitations>";
                        projectInvitationBase.EmailXml = xmlEmails;
                    }
                    //projectInvitationBase.ProjectId = lstProjectModel.FirstOrDefault().Id;

                    actionResult = adminAction.ProjectInvitation_Insert_Update(projectInvitationBase);
                }

                if (actionResult.IsSuccess)
                {
                    //DataRow drXML = actionResult.dtResult.Rows[0];

                    //returnedXML = drXML[0] != DBNull.Value ? Convert.ToString(drXML[0]) : "";

                    //XDocument xDoc = XDocument.Parse(returnedXML);
                    List<InvitationList> lstInvitationList = Helper.CommHelper.ConvertTo<InvitationList>(actionResult.dtResult);

                    //lstInvitationList = (from xEle in xDoc.Descendants("Invitation")
                    //                     select new InvitationList
                    //                     {
                    //                         Email = xEle.Element("Email").Value,
                    //                         InvitationId = xEle.Element("InvitationGUID").Value,
                    //                         ProjectGuid = xEle.Element("ProjectGUID").Value
                    //                     }).ToList();

                    if (lstInvitationList != null && lstInvitationList.Count > 0)
                    {
                        System.Threading.Tasks.Parallel.ForEach(lstInvitationList, row =>
                        {
                            Email.ProjectMailingEmail(row.Email, row.ProjectGUID, row.InvitationGUID, model.subject, model.InvitationMessage, model.SuperProjectId);
                        });
                        TempData["SuccessMessage"] = "Mail sent successfully to " + model.InvitationList.TrimEnd(',') + "";
                    }
                    else
                    {
                        TempData["ErrorMessage"] = "Some error occurred in sending mail.";
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return View();
        }
        #endregion

        #region CreateRecordInPositionTable

        public void CreateRecordInPositionTable(string[] strarr, List<string> projectIds, int superProjectId, string ArrSubsurveyEmail, int ArrSubsurveyId, int ArrSubsurveyCount)
        {
            List<GroupEmailsModel> GroupEmails = new List<GroupEmailsModel>();
            int surveyCount = 1;
            for (int i = 0; i <= strarr.Length - 1; i++)
            {
                foreach (string projId in projectIds)
                {
                    List<ProductProjectModel> lstProductProjectModel = new List<ProductProjectModel>();
                    productProjectBase.ProjectId = Convert.ToInt32(projId);
                    SRV.BaseLayer.ActionResult actionResultProducts = new SRV.BaseLayer.ActionResult();
                    actionResultProducts = adminAction.ProductProject_LoadById(productProjectBase);
                    if (actionResultProducts.IsSuccess)
                    {
                        foreach (DataRow drProd in actionResultProducts.dtResult.Rows)
                        {
                            lstProductProjectModel.Add(new ProductProjectModel
                            {
                                Id = drProd["Id"] != DBNull.Value ? Convert.ToInt32(drProd["Id"]) : 0,
                                ProjectId = drProd["ProjectId"] != DBNull.Value ? Convert.ToInt32(drProd["ProjectId"]) : 0,
                                DisplayOrder = drProd["DisplayOrder"] != DBNull.Value ? Convert.ToInt32(drProd["DisplayOrder"]) : 0
                            });
                        }
                    }

                    DataTable dtProduct = new DataTable();
                    dtProduct.Columns.Add("ProductId", typeof(int));
                    dtProduct.Columns.Add("Random", typeof(string));
                    dtProduct.Columns.Add("DisplayOrder", typeof(string));
                    Random rndProd = new Random();

                    if (!String.IsNullOrEmpty(strarr[i]))
                    {
                        string productsXML = "<products>";
                        if (lstProductProjectModel != null && lstProductProjectModel.Count > 0)
                        {
                            foreach (var item in lstProductProjectModel)
                            {
                                DataRow drProduct = dtProduct.NewRow();
                                drProduct["ProductId"] = item.Id;
                                drProduct["Random"] = rndProd.Next(1, 10).ToString();
                                drProduct["DisplayOrder"] = item.DisplayOrder;
                                dtProduct.Rows.Add(drProduct);
                            }
                            bool IsProductRandomize = false;
                            projectSettingsBase.ProjectId = Convert.ToInt32(projId);// projId.Id;
                            SRV.BaseLayer.ActionResult actionResultSetting = new SRV.BaseLayer.ActionResult();
                            actionResultSetting = adminAction.ProjectSettings_LoadById(projectSettingsBase);
                            if (actionResultSetting.IsSuccess)
                            {
                                IsProductRandomize = actionResultSetting.dtResult.Rows[0]["IsProductRandomize"] != DBNull.Value ? Convert.ToBoolean(actionResultSetting.dtResult.Rows[0]["IsProductRandomize"]) : false;
                            }
                            if (IsProductRandomize == true)
                            {
                                dtProduct.DefaultView.Sort = "Random asc";
                                dtProduct = dtProduct.DefaultView.ToTable();
                            }
                            else
                            {
                                dtProduct.DefaultView.Sort = "DisplayOrder asc";
                                dtProduct = dtProduct.DefaultView.ToTable();
                            }
                            int prodDispOrder = 1;
                            if (!String.IsNullOrEmpty(ArrSubsurveyEmail))
                            {
                                if (ArrSubsurveyEmail == strarr[i])
                                {
                                    if (ArrSubsurveyId == Convert.ToInt32(projId))
                                    {
                                        for (int j = 0; j <= ArrSubsurveyCount - 1; j++)
                                        {
                                            productsXML += "<product><PId>" + Convert.ToString(dtProduct.Rows[j]["ProductId"]) + "</PId><PDispOrder>" + prodDispOrder + "</PDispOrder></product>";
                                            prodDispOrder++;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                foreach (DataRow drTblProd in dtProduct.Rows)
                                {
                                    productsXML += "<product><PId>" + Convert.ToString(drTblProd["ProductId"]) + "</PId><PDispOrder>" + prodDispOrder + "</PDispOrder></product>";
                                    prodDispOrder++;
                                }
                            }
                        }
                        productsXML += "</products>";
                        PositionTable positionTable = new PositionTable();
                        positionTable.SurveyId = Convert.ToInt32(projId);//projId.Id;
                        positionTable.ProductsXML = productsXML;
                        positionTable.ResponderId = strarr[i];
                        positionTable.SuperSurveyId = superProjectId;// model.SuperProjectId;
                        positionTable.SurveyDispOrder = surveyCount;
                        SRV.BaseLayer.ActionResult actionResultPosition = new SRV.BaseLayer.ActionResult();
                        if (positionTable.ResponderId.IndexOf("GRP_") != -1)
                        {
                            GroupEmails = GroupEmailCompleteList(strarr[i]);
                            foreach (var eml in GroupEmails)
                            {
                                positionTable.ResponderId = eml.Email;
                                actionResultPosition = adminAction.PositionTable_Insert(positionTable);
                            }
                        }
                        else
                            actionResultPosition = adminAction.PositionTable_Insert(positionTable);
                    }
                    surveyCount++;
                }

            }
        }

        #endregion

        #region GroupEmailCompleteList

        public List<GroupEmailsModel> GroupEmailCompleteList(string strarr)
        {
            string groupName = strarr;
            List<GroupEmailsModel> emailList = new List<GroupEmailsModel>();
            List<GroupEmailMasterModel> emailGroupList = new List<GroupEmailMasterModel>();

            string action = string.Empty;

            groupBase.UserId = Session["UserId"] != null ? Convert.ToInt32(Session["UserId"]) : 0;
            SRV.BaseLayer.ActionResult actionResultGroupMaster = new SRV.BaseLayer.ActionResult();
            actionResultGroupMaster = adminAction.EmailGroupList_byUserId(groupBase);
            if (actionResultGroupMaster.IsSuccess)
            {
                foreach (DataRow drGrpEmail in actionResultGroupMaster.dtResult.Rows)
                {
                    emailGroupList.Add(new Models.GroupEmailMasterModel
                    {
                        Id = drGrpEmail["Id"] != DBNull.Value ? Convert.ToInt32(drGrpEmail["Id"]) : 0,
                        GroupName = drGrpEmail["GroupName"] != DBNull.Value ? Convert.ToString(drGrpEmail["GroupName"]) : "",
                        UserId = drGrpEmail["Id"] != DBNull.Value ? Convert.ToInt32(drGrpEmail["Id"]) : 0,
                        CreatedOn = drGrpEmail["CreatedOn"] != DBNull.Value ? Convert.ToDateTime(drGrpEmail["CreatedOn"]).ToString("MM/dd/yyyy") : "N/A",
                        ModifiedOn = drGrpEmail["ModifiedOn"] != DBNull.Value ? Convert.ToDateTime(drGrpEmail["ModifiedOn"]).ToString("MM/dd/yyyy") : "N/A"
                    });
                }
                var hasGroup = (from grp in emailGroupList
                                where grp.GroupName == groupName
                                select grp.Id).FirstOrDefault();
                if (Convert.ToInt32(hasGroup) > 0)
                {
                    GroupEmailsBase emailBase = new GroupEmailsBase();
                    ManageGroupEmailModel grpEmailModel = new ManageGroupEmailModel();

                    emailBase.GroupId = Convert.ToInt32(hasGroup);
                    SRV.BaseLayer.ActionResult actionResultGroupEmail = new SRV.BaseLayer.ActionResult();
                    actionResultGroupEmail = adminAction.EmailsForGroup_LoadbyGroupId(emailBase);
                    if (actionResultGroupEmail.IsSuccess)
                    {
                        foreach (DataRow drEmailGrp in actionResultGroupEmail.dtResult.Rows)
                        {
                            emailList.Add(new GroupEmailsModel
                            {
                                Email = drEmailGrp["Email"] != DBNull.Value ? Convert.ToString(drEmailGrp["Email"]) : ""
                            });
                        }
                    }
                }
            }
            return emailList;
        }

        #endregion

        #region ProjectStatistics
        [CheckLoginAttribute]
        [HttpGet]
        public ActionResult ProjectStatistics(int? Id = 0)
        {
            ProjectStatsModel model = new ProjectStatsModel();
            string CreatedDate = string.Empty;
            try
            {
                int UserId = Convert.ToInt32(Session["UserId"]);
                adminBase.Id = UserId;
                adminBase.ProjectId = Id.GetValueOrDefault();
                actionResult = adminAction.ProjectStatistics_LoadById(adminBase);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    DataRow dr = actionResult.dtResult.Rows[0];
                    model.CreatedOn = dr["CreatedOn"] != DBNull.Value ? Convert.ToDateTime(dr["CreatedOn"]).ToString("MM/dd/yyyy") : "N/A";
                    model.ProjectName = dr["ProjectName"] != DBNull.Value ? dr["ProjectName"].ToString() : "N/A";
                    model.OpeningDate = dr["OpeningDate"] != DBNull.Value ? Convert.ToDateTime(dr["OpeningDate"]).ToString("MM/dd/yyyy") : "N/A";
                    model.ClosingDate = dr["ClosingDate"] != DBNull.Value ? Convert.ToDateTime(dr["ClosingDate"]).ToString("MM/dd/yyyy") : "N/A";
                    model.IsActive = dr["IsActive"] != DBNull.Value ? Convert.ToBoolean(dr["IsActive"]) : false;
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return View(model);
        }
        #endregion

        #region ProjectSetings Get
        [HttpGet]
        public ActionResult ProjectSettings(int? id = 0, int? SPId = 0)
        {
            ViewBag.PId = id;
            ProjectSettingsModel model = new ProjectSettingsModel();
            List<ProductProjectModel> lstproductModel = new List<ProductProjectModel>();
            actionResult = new SRV.BaseLayer.ActionResult();
            try
            {
                projectSettingsBase.ProjectId = Convert.ToInt32(id);
                actionResult = adminAction.ProjectSettings_LoadById(projectSettingsBase);
                model.ProjectId = Convert.ToInt32(id);
                if (actionResult.IsSuccess)
                {
                    DataRow dr = actionResult.dtResult.Rows[0];
                    model.Id = dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0;
                    model.ProjectId = dr["ProjectId"] != DBNull.Value ? Convert.ToInt32(dr["ProjectId"]) : 0;
                    model.ProjectGUID = dr["ProjectGUID"] != DBNull.Value ? Convert.ToString(dr["ProjectGUID"]) : "";
                    model.OpeningDate = dr["OpeningDate"] != DBNull.Value ? Convert.ToDateTime(dr["OpeningDate"]).ToString("MM/dd/yyyy") : "";
                    model.ClosingDate = dr["ClosingDate"] != DBNull.Value ? Convert.ToDateTime(dr["ClosingDate"]).ToString("MM/dd/yyyy") : "";
                    model.PrevNextPageNavigation = dr["PrevNextPageNavigation"] != DBNull.Value ? Convert.ToBoolean(dr["PrevNextPageNavigation"]) : false;
                    model.DisableQuestionNumbering = dr["DisableQuestionNumbering"] != DBNull.Value ? Convert.ToBoolean(dr["DisableQuestionNumbering"]) : false;
                    model.IsActive = dr["IsActive"] != DBNull.Value ? Convert.ToBoolean(dr["IsActive"]) : false;
                    model.Scored = dr["Scored"] != DBNull.Value ? Convert.ToBoolean(dr["Scored"]) : false;
                    model.ResumeOfProgress = dr["ResumeOfProgress"] != DBNull.Value ? Convert.ToBoolean(dr["ResumeOfProgress"]) : false;
                    model.TestMode = dr["TestMode"] != DBNull.Value ? Convert.ToBoolean(dr["TestMode"]) : false;
                    model.ProjectQuota = dr["ProjectQuota"] != DBNull.Value ? Convert.ToInt32(dr["ProjectQuota"]) : 0;
                    model.OnlineResponses = dr["OnlineResponses"] != DBNull.Value ? Convert.ToString(dr["OnlineResponses"]) : "0";
                    model.OfflineResponses = dr["OfflineResponses"] != DBNull.Value ? Convert.ToString(dr["OfflineResponses"]) : "0";
                    model.IsQuesRandomize = dr["IsQuesRandomize"] != DBNull.Value ? Convert.ToBoolean(dr["IsQuesRandomize"]) : false;
                    model.IsProductRandomize = dr["IsProductRandomize"] != DBNull.Value ? Convert.ToBoolean(dr["IsProductRandomize"]) : false;
                }
                model.SuperProjectId = Convert.ToInt32(SPId);
                CreateProjectModel detailModel = new CreateProjectModel();
                detailModel = GetProjectDetailById(id);
                if (String.IsNullOrEmpty(model.ProjectGUID))
                {
                    // Get Project GUID from separate Method
                    model.ProjectGUID = detailModel != null ? detailModel.ProjectGuid : "";
                }
                model.ProjectType = detailModel != null ? Convert.ToInt32(detailModel.ProjectType) : 0;
                model.lstProductProjectModel = lstproductModel;
                productProjectBase.ProjectId = Convert.ToInt32(id);
                actionResult = adminAction.ProductProject_LoadById(productProjectBase);
                if (actionResult.IsSuccess)
                {
                    foreach (DataRow dr in actionResult.dtResult.Rows)
                    {
                        lstproductModel.Add(new ProductProjectModel
                        {
                            Id = dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0,
                            ProjectId = dr["ProjectId"] != DBNull.Value ? Convert.ToInt32(dr["ProjectId"]) : 0,
                            ProductLogicalId = dr["ProductLogicalId"] != DBNull.Value ? Convert.ToInt32(dr["ProductLogicalId"]) : 0,
                            Description = dr["Description"] != DBNull.Value ? Convert.ToString(dr["Description"]) : string.Empty,
                            DisplayOrder = dr["DisplayOrder"] != DBNull.Value ? Convert.ToInt32(dr["DisplayOrder"]) : 0
                        });
                    }
                    lstproductModel = lstproductModel.OrderBy(l => l.DisplayOrder).ToList();
                }
                model.lstProductProjectModel = lstproductModel;
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return View(model);
        }
        #endregion

        #region ProjectSetting Post
        [HttpPost]
        public ActionResult ProjectSetting(ProjectSettingsModel model)
        {
            try
            {
                projectSettingsBase = new ProjectSettingsBase();
                projectSettingsBase.ProjectId = model.ProjectId;
                if (!String.IsNullOrEmpty(model.OpeningDate))
                    projectSettingsBase.OpeningDate = model.OpeningDate;
                if (!String.IsNullOrEmpty(model.ClosingDate))
                    projectSettingsBase.ClosingDate = model.ClosingDate;
                projectSettingsBase.PrevNextPageNavigation = model.PrevNextPageNavigation;
                projectSettingsBase.DisableQuestionNumbering = model.DisableQuestionNumbering;
                projectSettingsBase.IsActive = model.IsActive;
                projectSettingsBase.Scored = model.Scored;
                projectSettingsBase.ResumeOfProgress = model.ResumeOfProgress;
                projectSettingsBase.TestMode = model.TestMode;
                projectSettingsBase.IsQuesRandomize = model.IsQuesRandomize;
                projectSettingsBase.IsProductRandomize = model.IsProductRandomize;
                actionResult = new SRV.BaseLayer.ActionResult();
                actionResult = adminAction.ProjectSettings_InsertUpdate(projectSettingsBase);
                if (actionResult.IsSuccess)
                    TempData["SuccessMessage"] = "Project settings have changed succesfully";
                else
                    TempData["ErrorMessage"] = "Error in changing the settings.";
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return RedirectToAction("ProjectSettings", new { id = model.ProjectId, SPId = model.SuperProjectId });
        }
        #endregion

        #region ProjectSettingsPreview Post
        [HttpPost]
        public ActionResult ProjectSettingsPreview(ProjectSettingsModel model)
        {
            string json = string.Empty;
            try
            {
                projectSettingsBase = new ProjectSettingsBase();
                projectSettingsBase.ProjectId = model.ProjectId;
                if (!String.IsNullOrEmpty(model.OpeningDate))
                    projectSettingsBase.OpeningDate = model.OpeningDate;
                if (!String.IsNullOrEmpty(model.ClosingDate))
                    projectSettingsBase.ClosingDate = model.ClosingDate;
                projectSettingsBase.PrevNextPageNavigation = model.PrevNextPageNavigation;
                projectSettingsBase.DisableQuestionNumbering = model.DisableQuestionNumbering;
                projectSettingsBase.IsActive = model.IsActive == true ? false : true;
                projectSettingsBase.Scored = model.Scored;
                projectSettingsBase.ResumeOfProgress = model.ResumeOfProgress;
                projectSettingsBase.TestMode = model.TestMode;

                actionResult = new SRV.BaseLayer.ActionResult();
                actionResult = adminAction.ProjectSettings_InsertUpdate(projectSettingsBase);
                if (actionResult.IsSuccess)
                    json = "{\"ActiveStatus\":\"" + projectSettingsBase.IsActive + "\"}";
                else
                    json = "{\"ActiveStatus\":\"" + model.IsActive + "\"}";
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return Json(json, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region DeleteProject
        public ActionResult DeleteProject(int? id = 0)
        {
            try
            {
                projectBase.Id = Convert.ToInt32(id);
                actionResult = adminAction.ProjectDelete_ById(projectBase);
                if (actionResult.IsSuccess)
                {
                    TempData["SuccessMessage"] = "Project deleted successfully";
                }
                else
                {
                    TempData["ErrorMessage"] = "Project deletion is Ok but prevented while testing.";
                    return RedirectToAction("ProjectSettings", new { id = id });
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return RedirectToAction("ProjectList");
        }
        #endregion

        #region CloneProject Get
        public ActionResult CloneProject(int? id = 0)
        {
            try
            {
                adminBase.ProjectId = id.GetValueOrDefault();
                actionResult = adminAction.ProjectClone_ByProjectId(adminBase);
                if (actionResult.IsSuccess)
                {
                    TempData["SuccessMessage"] = "Project is Cloned succesfully.";
                }
                else
                {
                    TempData["ErrorMessage"] = "Error in cloning the project.";
                }
            }
            catch (Exception ex)
            {
                TempData["ErrorMessage"] = "Error in cloning the project.";
                ErrorReporting.WebApplicationError(ex);
            }
            return RedirectToAction("ProjectSettings", new { id = id });
        }
        #endregion

        #region ExportXML Get
        public ActionResult ExportXML(string id)
        {
            try
            {
                string xml = GetProjectXML(id);

                byte[] bytes = System.Text.Encoding.UTF8.GetBytes(xml);
                Response.OutputStream.Write(bytes, 0, bytes.Length);

                Response.Charset = "utf-8";
                Response.ContentType = "application/octet-stream";

                Response.ContentType = "text/xml";
                Response.AddHeader("Content-Disposition", "attachment; filename=\"SurveyExport.xml\"");

                // xml.WriteXml(Response.OutputStream, System.Data.XmlWriteMode.IgnoreSchema);

                Response.End();
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return RedirectToAction("Settings", new { id = id });
        }
        #endregion

        #region CheckRoles
        public JsonResult CheckRoles()
        {
            string json = string.Empty;
            int UserId = Session["UserId"] != null ? Convert.ToInt32(Session["UserId"]) : 0;
            try
            {
                role.UserId = UserId;
                actionResult = superAction.RoleForUser_ByUserId(role);
                if (actionResult.IsSuccess)
                {
                    foreach (DataRow dr in actionResult.dtResult.Rows)
                    {
                        json += "{\"RoleId\":\"" + dr["RoleId"] + "\"},";
                    }
                    json = "[" + json.TrimEnd(',') + "]";
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return Json(json, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region ProjectQuota Get
        public ActionResult ProjectQuota(int? Id = 0)
        {
            ViewBag.PId = Id;
            CreateProjectModel model = new CreateProjectModel();
            try
            {
                projectQuotaBase.ProjectId = Id.GetValueOrDefault();
                actionResult = adminAction.ProjectQuota_LoadById(projectQuotaBase);
                if (actionResult.IsSuccess)
                {
                    DataRow dr = actionResult.dtResult.Rows[0];
                    model.ProjectName = dr["ProjectName"] != DBNull.Value ? dr["ProjectName"].ToString() : "";
                    model.ProjectQuota = dr["ProjectQuota"] != DBNull.Value ? Convert.ToInt32(dr["ProjectQuota"]) : 0;
                    model.QuotaQues = dr["QuotaQues"] != DBNull.Value ? dr["QuotaQues"].ToString() : "";
                }

            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return View(model);
        }
        #endregion

        #region ProjectQuota Post
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult ProjectQuota(CreateProjectModel model, FormCollection fc)
        {
            string allQuotas = fc["hdnQuota"] != null ? fc["hdnQuota"] : "";
            int ProjectId = fc["hdnProjectId"] != null ? Convert.ToInt32(fc["hdnProjectId"]) : 0;
            int ProjectType = fc["hdnProjectType"] != null ? Convert.ToInt32(fc["hdnProjectType"]) : 0;
            string quotasXml = string.Empty;
            try
            {
                if (!String.IsNullOrEmpty(allQuotas))
                {
                    string[] allQuotaArray = allQuotas.TrimEnd('|').Split('|');
                    if (allQuotaArray != null && allQuotaArray.Length > 0)
                    {
                        for (int i = 0; i <= allQuotaArray.Length - 1; i++)
                        {
                            string[] quota = allQuotaArray[i].Split(',');
                            if (quota != null && quota.Length > 0)
                            {
                                quotasXml += "<Quota><Id>" + quota[0] + "</Id><ProjectId>" + ProjectId + "</ProjectId><QuotaType>" + quota[1] + "</QuotaType><QuotaPer>" + quota[2] + "</QuotaPer><QuotaJump>" + quota[3] + "</QuotaJump></Quota>";
                            }
                        }
                    }
                }
                else
                {
                    quotasXml = "<Quota><Id>0</Id><ProjectId>" + ProjectId + "</ProjectId><QuotaType>''</QuotaType><QuotaPer>''</QuotaPer><QuotaJump>''</QuotaJump></Quota>";
                }
                string QuotaXml = "<Project>" + quotasXml + "</Project>";
                projectQuotaBase.OpenXml = QuotaXml;
                projectQuotaBase.ProjectQuota = model.ProjectQuota;
                projectQuotaBase.ProjectId = ProjectId;
                projectQuotaBase.Id = Session["UserId"] != null ? Convert.ToInt32(Session["UserId"]) : 0;
                projectQuotaBase.QuotaQues = model.QuotaQues;
                actionResult = adminAction.ProjectQuotaOtherInsertUpdate_ByProjectId(projectQuotaBase);
                if (actionResult.IsSuccess)
                {
                    TempData["SuccessMessage"] = "Project quota is updated successfully.";
                }
                else
                {
                    TempData["ErrorMessage"] = "Error in updating project quota !";
                }
            }
            catch (Exception ex)
            {
                TempData["ErrorMessage"] = "Error in updating project quota !";
                ErrorReporting.WebApplicationError(ex);
            }
            //return RedirectToAction("ProjectQuota", new { Id = ProjectId });
            return RedirectToAction("QuestionList", new { id = ProjectId, projectType = ProjectType });
        }
        #endregion

        #region ProjectQuotaOther Json
        public JsonResult ProjectQuotaOther(int Id)
        {
            string json = string.Empty;
            try
            {
                projectQuotaBase.ProjectId = Id;
                actionResult = adminAction.ProjectQuotaOther_LoadById(projectQuotaBase);
                if (actionResult.IsSuccess)
                {
                    json = Newtonsoft.Json.JsonConvert.SerializeObject(actionResult.dtResult);
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return Json(json, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region GetProjectXML
        public string GetProjectXML(string guid)
        {
            SurveyLoadBase surveyLoadBase = new SurveyLoadBase();
            SurveyQuestionBase questionBase = new SurveyQuestionBase();
            SurveyResponseModel model = new SurveyResponseModel();
            CreateProjectModel projectModel = new CreateProjectModel();
            ProjectSettingsModel projectSettingsModel = new ProjectSettingsModel();
            List<QuestionModel> lstQuestionModel = new List<QuestionModel>();
            List<ProjectQuotaOther> lstProjectQuotaOther = new List<Models.ProjectQuotaOther>();
            model.projectModel = projectModel;
            model.projectSettingsModel = projectSettingsModel;
            model.lstQuestionModel = lstQuestionModel;
            string xmlToWrite = string.Empty;
            try
            {
                surveyLoadBase.GUID = guid;
                actionResult = new SRV.BaseLayer.ActionResult();
                actionResult = respondentAction.Survey_LoadByGUID(surveyLoadBase);
                if (actionResult.IsSuccess)
                {
                    DataRow dr = actionResult.dtResult.Rows[0];

                    // Assign project related values
                    projectModel.Id = dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0;
                    projectModel.ProjectName = dr["ProjectName"] != DBNull.Value ? Convert.ToString(dr["ProjectName"]) : "";
                    projectModel.WelcomeMessage = dr["WelcomeMessage"] != DBNull.Value ? Convert.ToString(dr["WelcomeMessage"]) : "";
                    projectModel.PageHeader = dr["PageHeader"] != DBNull.Value ? Convert.ToString(dr["PageHeader"]) : "";
                    projectModel.PageFooter = dr["PageFooter"] != DBNull.Value ? Convert.ToString(dr["PageFooter"]) : "";
                    projectModel.ProjectCompletionMesssage = dr["ProjectCompletionMesssage"] != DBNull.Value ? Convert.ToString(dr["ProjectCompletionMesssage"]) : "";
                    projectModel.RedirectUrl = dr["RedirectUrl"] != DBNull.Value ? Convert.ToString(dr["RedirectUrl"]) : "";
                    //projectModel.ProductMessage = dr["ProductMessage"] != DBNull.Value ? Convert.ToString(dr["ProductMessage"]) : "";

                    // Assign project settings related values
                    projectSettingsModel.OpeningDate = dr["OpeningDate"] != DBNull.Value ? Convert.ToDateTime(dr["OpeningDate"]).ToString("MM/dd/yyyy") : "";
                    projectSettingsModel.ClosingDate = dr["ClosingDate"] != DBNull.Value ? Convert.ToDateTime(dr["ClosingDate"]).ToString("MM/dd/yyyy") : "";
                    projectSettingsModel.DisableQuestionNumbering = dr["DisableQuestionNumbering"] != DBNull.Value ? Convert.ToBoolean(dr["DisableQuestionNumbering"]) : false;
                    projectSettingsModel.IsActive = dr["IsActive"] != DBNull.Value ? Convert.ToBoolean(dr["IsActive"]) : false;
                    projectSettingsModel.Scored = dr["Scored"] != DBNull.Value ? Convert.ToBoolean(dr["Scored"]) : false;
                    projectSettingsModel.PrevNextPageNavigation = dr["PrevNextPageNavigation"] != DBNull.Value ? Convert.ToBoolean(dr["PrevNextPageNavigation"]) : false;
                    projectSettingsModel.ResumeOfProgress = dr["ResumeOfProgress"] != DBNull.Value ? Convert.ToBoolean(dr["ResumeOfProgress"]) : false;

                    // Load Project Quota Values

                    projectQuotaBase.ProjectId = projectModel.Id;
                    SRV.BaseLayer.ActionResult actionResultProjectQuota = new SRV.BaseLayer.ActionResult();
                    actionResultProjectQuota = adminAction.ProjectQuota_LoadById(projectQuotaBase);
                    if (actionResultProjectQuota.IsSuccess)
                    {
                        DataRow drQuota = actionResultProjectQuota.dtResult.Rows[0];
                        projectModel.ProjectQuota = drQuota["ProjectQuota"] != DBNull.Value ? Convert.ToInt32(drQuota["ProjectQuota"]) : 0;
                    }

                    // Load Project Quota Others Values
                    SRV.BaseLayer.ActionResult actionResultProjectQuotaOther = new SRV.BaseLayer.ActionResult();
                    projectQuotaBase.ProjectId = projectModel.Id;
                    actionResultProjectQuotaOther = adminAction.ProjectQuotaOther_LoadById(projectQuotaBase);
                    if (actionResultProjectQuotaOther.IsSuccess)
                    {
                        foreach (DataRow drPQOther in actionResultProjectQuotaOther.dtResult.Rows)
                        {
                            lstProjectQuotaOther.Add(new ProjectQuotaOther
                            {
                                QuotaType = drPQOther["QuotaType"] != DBNull.Value ? Convert.ToString(drPQOther["QuotaType"]) : "",
                                QuotaPerc = drPQOther["QuotaPer"] != DBNull.Value ? Convert.ToDecimal(drPQOther["QuotaPer"]) : 0
                            });
                        }
                    }

                    // Load All Questions of this project
                    surveyLoadBase.Id = projectModel.Id;
                    SRV.BaseLayer.ActionResult actionResultQuestions = new SRV.BaseLayer.ActionResult();
                    actionResultQuestions = respondentAction.ProjQues_LoadByProjectId(surveyLoadBase);
                    if (actionResultQuestions.IsSuccess)
                    {
                        foreach (DataRow drQuestion in actionResultQuestions.dtResult.Rows)
                        {
                            QuestionModel question = new QuestionModel();
                            question.QuestionTypeId = drQuestion["QuestionTypeId"] != DBNull.Value ? Convert.ToInt32(drQuestion["QuestionTypeId"]) : 0;
                            question.QuestionId = drQuestion["QuestionId"] != DBNull.Value ? Convert.ToInt32(drQuestion["QuestionId"]) : 0;
                            question.QuestionTitle = drQuestion["QuestionTitle"] != DBNull.Value ? Convert.ToString(drQuestion["QuestionTitle"]) : "";
                            question.ConditionalExpression = drQuestion["ConditionalExpression"] != DBNull.Value ? Convert.ToString(drQuestion["ConditionalExpression"]) : "";
                            question.RangeMin = drQuestion["RangeMin"] != DBNull.Value ? Convert.ToInt32(drQuestion["RangeMin"]) : 0;
                            question.RangeMax = drQuestion["RangeMax"] != DBNull.Value ? Convert.ToInt32(drQuestion["RangeMax"]) : 0;
                            question.ConstantSum = drQuestion["ConstantSum"] != DBNull.Value ? Convert.ToInt32(drQuestion["ConstantSum"]) : 0;

                            // Load all Options of this question
                            SRV.BaseLayer.ActionResult actionResultOptions = new SRV.BaseLayer.ActionResult();
                            questionBase.Id = question.QuestionId;
                            actionResultOptions = respondentAction.Options_LoadAllByQuesId(questionBase);
                            List<OptionsModel> lstOptions = new List<OptionsModel>();
                            if (actionResultOptions.IsSuccess)
                            {
                                foreach (DataRow drOption in actionResultOptions.dtResult.Rows)
                                {
                                    lstOptions.Add(new OptionsModel
                                    {
                                        OptionId = drOption["Id"] != DBNull.Value ? Convert.ToInt32(drOption["Id"]) : 0,
                                        QuestionId = drOption["QuestionId"] != DBNull.Value ? Convert.ToInt32(drOption["QuestionId"]) : 0,
                                        Option = drOption["OptionTitle"] != DBNull.Value ? Convert.ToString(drOption["OptionTitle"]) : ""
                                    });
                                }
                                question.lstOptionsModel = lstOptions;
                            }
                            lstQuestionModel.Add(question);
                        }
                    }
                }

                // Make the XML

                xmlToWrite += "<ProjectDetail><Project><Id>" + projectModel.Id + "</Id><ProjectName>" + projectModel.ProjectName + "</ProjectName>";
                xmlToWrite += "<WelcomeMessage>" + projectModel.WelcomeMessage + "</WelcomeMessage>";
                xmlToWrite += "<PageHeader>" + projectModel.PageHeader + "</PageHeader>" + "<PageFooter>" + projectModel.PageFooter + "</PageFooter>";
                xmlToWrite += "<ProjectCompletionMessage>" + projectModel.ProjectCompletionMesssage + "</ProjectCompletionMessage>";
                xmlToWrite += "<RedirectUrl>" + projectModel.RedirectUrl + "</RedirectUrl>";
                //xmlToWrite += "<ProductMessage>" + projectModel.ProductMessage + "</ProductMessage>";
                xmlToWrite += "<InvitationMessage>Null</InvitationMessage>";
                xmlToWrite += "<OwnerId>" + projectModel.OwnerId + "</OwnerId>";
                xmlToWrite += "<CreatedBy>" + projectModel.CreatedBy + "</CreatedBy></Project><ProjectQuota><ProjectQuota>" + projectModel.ProjectQuota + "</ProjectQuota>";
                xmlToWrite += "<CreatedBy>" + projectModel.CreatedBy + "</CreatedBy><ModifiedBy>" + projectModel.ModifiedBy + "</ModifiedBy></ProjectQuota>";
                xmlToWrite += "<ProjectQuotaOther>";
                // Project Quota Others List
                foreach (var item in lstProjectQuotaOther)
                {
                    xmlToWrite += "<Others><QuotaType>" + item.QuotaType + "</QuotaType><QuotaPercent>" + item.QuotaPerc + "</QuotaPercent><IsDeleted>false</IsDeleted><CreatedBy>" + projectModel.CreatedBy + "</CreatedBy><ModifiedBy>" + projectModel.ModifiedBy + "</ModifiedBy></Others>";
                }
                xmlToWrite += "</ProjectQuotaOther>";
                xmlToWrite += "<ProjectSettings><OpeningDate>" + projectSettingsModel.OpeningDate + "</OpeningDate><ClosingDate>" + projectSettingsModel.ClosingDate + "</ClosingDate>";
                xmlToWrite += "<DisableQuestionNumbering>" + projectSettingsModel.DisableQuestionNumbering + "</DisableQuestionNumbering>";
                xmlToWrite += "<IsActive>" + projectSettingsModel.IsActive + "</IsActive><Scored>" + projectSettingsModel.Scored + "</Scored><PrevNextPageNavigation>" + projectSettingsModel.PrevNextPageNavigation + "</PrevNextPageNavigation><ResumeOfProgress>" + projectSettingsModel.ResumeOfProgress + "</ResumeOfProgress></ProjectSettings>";

                // Project Questions
                xmlToWrite += "<ProjectQuestions>";
                foreach (var question in lstQuestionModel)
                {
                    xmlToWrite += "<Questions><QuestionTitle>" + question.QuestionTitle + "</QuestionTitle><QuestionTypeId>" + question.QuestionTypeId + "</QuestionTypeId><IsActive>True</IsActive>";
                    xmlToWrite += "<GroupId>Null</GroupId><PageNumber>Null</PageNumber><CreatedBy>" + question.CreatedBy + "</CreatedBy><ModifiedBy>" + question.ModifiedBy + "</ModifiedBy>";
                    xmlToWrite += "<ConstantSum>" + question.ConstantSum + "</ConstantSum><RangeMin>" + question.RangeMin + "</RangeMin><RangeMax>" + question.RangeMax + "</RangeMax><DisplayOrder>" + question.DisplayOrder + "</DisplayOrder>";
                    xmlToWrite += "<ConditionExpression>" + question.ConditionalExpression + "</ConditionExpression>";
                    if (question.lstOptionsModel != null)
                    {
                        foreach (var opts in question.lstOptionsModel)
                        {
                            xmlToWrite += "<Options><OptionTitle>" + opts.Option + "</OptionTitle></Options>";
                        }
                    }
                    xmlToWrite += "</Questions>";
                }
                xmlToWrite += "</ProjectQuestions></ProjectDetail>";

            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return xmlToWrite;
        }
        #endregion

        #region ManageQuestions Get
        public ActionResult ManageQuestions(int? id = 0)
        {
            ViewBag.ProjectId = id;
            CreateQuestionModel model = new CreateQuestionModel();
            List<CreateQuestionModel> QuestionLst = new List<CreateQuestionModel>();
            // model.QuestionList = QuestionLst;
            try
            {

                if (Session["UserId"] != null)
                {
                    questionBase.ProjectId = id.GetValueOrDefault();
                    actionResult = adminAction.Qusetion_LoadAllByProjectId(questionBase);
                    if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                    {
                        foreach (DataRow dr in actionResult.dtResult.Rows)
                        {
                            model = new CreateQuestionModel();
                            model.ProjectId = Convert.ToInt32(dr["ProjectId"]);
                            model.QuestionId = dr["questionId"] != DBNull.Value ? Convert.ToInt32(dr["questionId"]) : 0;
                            model.QuestionType = dr["QuestionType"] != DBNull.Value ? dr["QuestionType"].ToString() : "";
                            model.QuestionTitle = Convert.ToString(dr["QuestionTitle"]);
                            // model.Option = Convert.ToString(dr["QuestionType"]);
                            QuestionLst.Add(model);
                        }
                        model.QuestionList = QuestionLst;

                    }
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return View(model);
        }
        #endregion

        #region ManageQuestions Post
        [ValidateInput(false)]
        [HttpPost]
        public ActionResult ManageQuestions(string QuesOrder)
        {
            string json = string.Empty;
            string Questions = string.Empty;
            try
            {
                if (!String.IsNullOrEmpty(QuesOrder))
                {
                    string[] allQuesOrder = QuesOrder.TrimEnd(',').Split(',');
                    if (allQuesOrder != null && allQuesOrder.Length > 0)
                    {
                        for (int i = 0; i <= allQuesOrder.Length - 1; i++)
                        {
                            string[] order = allQuesOrder[i].Split('-');
                            if (order != null && order.Length > 0)
                            {
                                Questions += "<ManageQuestions><QuestionId>" + order[0] + "</QuestionId><DisplayOrder>" + order[1] + "</DisplayOrder></ManageQuestions>";
                            }
                        }
                    }
                }
                else
                {
                    Questions = "<ManageQuestions><QuestionId>0</QuestionId><DisplayOrder>''</DisplayOrder></ManageQuestions>";
                }
                string OrderXml = "<ProjectQuestion>" + Questions + "</ProjectQuestion>";
                questionBase.ColumnXml = OrderXml;
                questionBase.Action = "questionDisplayOrder";
                questionBase.ModifiedBy = Session["UserId"] != null ? Convert.ToInt32(Session["UserId"]) : 0;
                actionResult = adminAction.ProjectQuestion_UpdateDisplayOrder(questionBase);
                if (actionResult.IsSuccess)
                {
                    json = "{\"Status\":\"1\",\"Message\":\"Project Question Order is updated successfully\"}";
                }
                else
                {
                    json = "{\"Status\":\"-1\",\"Message\":\"Error in updating Project Question Order\"}";
                }
            }
            catch (Exception ex)
            {
                json = "{\"Status\":\"-1\",\"Message\":\"Error in updating Project Question Order\"}";
                ErrorReporting.WebApplicationError(ex);
            }
            return Json(json, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region ManageOptions
        public ActionResult ManageOptions(int? id = 0)
        {
            ViewBag.ProjectId = id;
            CreateQuestionModel model = new CreateQuestionModel();
            var questionList = new List<SelectListItem>();
            ViewBag.QuesList = null;
            try
            {

                if (Session["UserId"] != null)
                {
                    questionBase.ProjectId = id.GetValueOrDefault();
                    actionResult = adminAction.QuestionsLoadAll_ByProjectId(questionBase);
                    if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                    {
                        foreach (DataRow dr in actionResult.dtResult.Rows)
                        {
                            questionList.Add(new SelectListItem { Text = (dr["DisplayOrder"] != DBNull.Value ? Convert.ToString(dr["DisplayOrder"]) : "") + " . " + ReplaceHtml.ReplaceText(Server.HtmlDecode(dr["QuestionTitle"].ToString())), Value = dr["Id"].ToString() });
                        }
                    }
                }
                ViewBag.QuesList = questionList;
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return View(model);
        }
        #endregion

        #region ManageOptions Post
        [ValidateInput(false)]
        [HttpPost]
        public ActionResult ManageOptions(string OptnOrder)
        {
            string json = string.Empty;
            string Options = string.Empty;
            try
            {
                if (!String.IsNullOrEmpty(OptnOrder))
                {
                    string[] allOptnOrder = OptnOrder.TrimEnd(',').Split(',');
                    if (allOptnOrder != null && allOptnOrder.Length > 0)
                    {
                        for (int i = 0; i <= allOptnOrder.Length - 1; i++)
                        {
                            string[] order = allOptnOrder[i].Split('-');
                            if (order != null && order.Length > 0)
                            {
                                Options += "<ManageOptions><OptionId>" + order[0] + "</OptionId><DisplayOrder>" + order[1] + "</DisplayOrder></ManageOptions>";
                            }
                        }
                    }
                }
                else
                {
                    Options = "<ManageOptions><OptionId>0</OptionId><DisplayOrder>''</DisplayOrder></ManageOptions>";
                }
                string OrderXml = "<ProjectOptions>" + Options + "</ProjectOptions>";
                questionBase.ColumnXml = OrderXml;
                actionResult = adminAction.QuestionOptions_UpdateDisplayOrder(questionBase);
                if (actionResult.IsSuccess)
                {
                    json = "{\"Status\":\"1\",\"Message\":\"Options Order is updated successfully\"}";
                }
                else
                {
                    json = "{\"Status\":\"-1\",\"Message\":\"Error in updating Options Order\"}";
                }
            }
            catch (Exception ex)
            {
                json = "{\"Status\":\"-1\",\"Message\":\"Error in updating Options Order\"}";
                ErrorReporting.WebApplicationError(ex);
            }
            return Json(json, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region SurveyResponses
        public ActionResult SurveyResponses(int id)
        {
            ViewBag.ProjectId = id;

            ManageRespondersModel model = new ManageRespondersModel();
            List<RespondersModel> lstResponders = new List<RespondersModel>();
            model.lstRespondersModel = lstResponders;
            try
            {
                projectBase.Id = id;
                actionResult = adminAction.Project_LoadById(projectBase);
                if (actionResult.IsSuccess)
                {
                    DataRow dr = actionResult.dtResult.Rows[0];
                    projectBase.SuperProjectId = dr["SuperProjectId"] != DBNull.Value ? Convert.ToInt32(dr["SuperProjectId"]) : 0;
                }
                actionResult = adminAction.CompleteRespondersLoad_ByProjectId(projectBase);
                if (actionResult.IsSuccess)
                {

                    foreach (DataRow dr in actionResult.dtResult.Rows)
                    {
                        model.lstRespondersModel.Add(new RespondersModel
                        {
                            //Id = dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0,
                            Id = 0,
                            ProjectGuid = dr["ProjectGUID"] != DBNull.Value ? Convert.ToString(dr["ProjectGUID"]) : "",
                            InvitationGuid = dr["InvitationGuid"] != DBNull.Value ? Convert.ToString(dr["InvitationGuid"]) : "",
                            Email = dr["Email"] != DBNull.Value ? Convert.ToString(dr["Email"]) : "",
                            //CreatedOn = dr["CreatedOn"] != DBNull.Value ? dr["CreatedOn"].ToString() : "N/A",
                            CreatedOn = dr["CreatedOn"] != DBNull.Value ? DateTime.Parse(dr["CreatedOn"].ToString(), System.Globalization.CultureInfo.InvariantCulture).ToString() : "N/A",
                            // ModifiedOn = dr["ModifiedOn"] != DBNull.Value ? DateTime.Parse(dr["ModifiedOn"].ToString(), System.Globalization.CultureInfo.InvariantCulture).ToString("MM/dd/yyyy") : "N/A",
                            SuperProjectId = dr["SuperProjectId"] != DBNull.Value ? Convert.ToInt32(dr["SuperProjectId"]) : 0,
                            SuperProjectName = dr["SuperProjectName"] != DBNull.Value ? Convert.ToString(dr["SuperProjectName"]) : "",
                            Source = dr["Source"] != DBNull.Value ? Convert.ToString(dr["Source"]) : "",
                        });
                    }
                    IEnumerable<RespondersModel> filteredList = model.lstRespondersModel
                      .GroupBy(customer => customer.InvitationGuid).Select(group => group.First());

                    model.lstRespondersModel = filteredList.ToList();
                }

            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return View(model);
        }
        #endregion

        #region ViewSurvey Get
        [HttpGet]
        public ActionResult ViewSurvey(string surveyId, string uid = "", int _xS = 0)
        {
            SurveyResponseModel model = new SurveyResponseModel();
            string productIds = "";
            string productNames = "";
            try
            {
                ViewBag.SuperSurveyId = _xS;
                model = comMethods.GetSurvey(surveyId, uid, "admin", "", _xS);

                ViewBag.ProjectId = model.projectModel.Id;

                if (model.projectModel.lstProductProjectModel != null && model.projectModel.lstProductProjectModel.Count > 0)
                    foreach (var itm in model.projectModel.lstProductProjectModel)
                    {
                        productIds += Convert.ToString(itm.Id) + ",";
                        productNames += Convert.ToString(itm.ProductLogicalId) + ",";
                    }


                ViewBag.productList = productIds;
                ViewBag.ProductNameList = productNames;
                ViewBag.SurveyId = surveyId;
                ViewBag.uid = uid;

                //if (model.isError)
                //    TempData["ErrorMessage"] = model.ErrorMessage;
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return View(model);
        }
        #endregion

        #region _PartialViewSurvey
        public ActionResult _PartialViewSurvey(string surveyId, string uid = "", int _xS = 0)
        {
            SurveyResponseModel model = new SurveyResponseModel();
            try
            {
                model = comMethods.GetSurvey(surveyId, uid, "admin", "", _xS);
                ViewBag.ProjectId = model.projectModel.Id;

                productProjectBase.ProjectId = Convert.ToInt32(model.projectModel.Id);

                //if (model.isError)
                //    TempData["ErrorMessage"] = model.ErrorMessage;
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return View(model);
        }
        #endregion

        #region UpdateSurvey
        public ActionResult UpdateSurvey(string surveyResponse, string responderEmail, int projectId, int? productId = 0, string externalResponderId = null)
        {
            SurveyResponseBase surveyResponseBase = new SurveyResponseBase();
            string json = string.Empty;
            string xml = string.Empty;
            json = "{\"Status\":\"-1\"}";
            // Make proper xml for saving the response.
            try
            {
                string[] responses = surveyResponse.TrimEnd(',').Split(',');
                foreach (string str in responses)
                {
                    string[] abc = str.Split('-');
                    if (abc.Length > 1)
                        xml += "<QuestAnswers><QuestionId>" + abc[0] + "</QuestionId>" + "<AnswerExpression>" + abc[1] + "</AnswerExpression>" + "</QuestAnswers>";

                }
                xml = "<Responses>" + xml + "</Responses>";

                // Set Base layer                    
                surveyResponseBase.Xml = xml;
                surveyResponseBase.Email = responderEmail;
                surveyResponseBase.ProjectId = projectId;
                surveyResponseBase.ExternalResponderId = externalResponderId;
                surveyResponseBase.ProductId = productId.GetValueOrDefault();
                surveyResponseBase.PositionTblId = 0;
                // Execute Method
                actionResult = new SRV.BaseLayer.ActionResult();
                actionResult = respondentAction.SurveyResponse_Insert_Update(surveyResponseBase);
                if (actionResult.IsSuccess)
                    json = "{\"Status\":\"1\"}";

            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return Json(json, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region LanguageConversion
        public JsonResult LanguageConversion(int? Id = 0)
        {
            return Json(Helper.LanguageHelper.ConvertLanguage(Id), JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region ProjectPreview Get
        public ActionResult ProjectPreview(int? id = 0)
        {
            SurveyResponseModel model = new SurveyResponseModel();
            projectBase.Id = id.GetValueOrDefault();
            try
            {
                RespondentController respondent = new RespondentController();
                actionResult = adminAction.Project_LoadById(projectBase);
                if (actionResult.IsSuccess)
                {
                    DataRow dr = actionResult.dtResult.Rows[0];
                    string Guid = dr["ProjectGUID"] != null ? dr["ProjectGUID"].ToString() : "";
                    model = respondent.GetProjectPreview(Guid);
                    ViewBag.ProjectId = model.projectModel.Id;
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return View(model);

        }
        #endregion

        #region BindQuestionDdl
        public ActionResult BindQuestionDdl(int? Id = 0, int? QuestionId = 0)
        {
            string json = string.Empty;
            try
            {
                questionBase.ProjectId = Id.GetValueOrDefault();
                actionResult = adminAction.QuestionsLoadAll_ByProjectId(questionBase);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    int dispOrder = actionResult.dtResult.AsEnumerable().Where(v => v.Field<Int64>("Id") == QuestionId.GetValueOrDefault()).Select(v => v.Field<int>("DisplayOrder")).FirstOrDefault();
                    foreach (DataRow dr in actionResult.dtResult.Rows)
                    {
                        if (Convert.ToInt32(dr["DisplayOrder"]) > dispOrder)
                        {
                            //string question = dr["QuestionTitle"] != DBNull.Value ? dr["QuestionTitle"].ToString() : "";
                            string question = dr["DisplayOrder"] != DBNull.Value ? dr["DisplayOrder"].ToString() : "";
                            json += "{\"QuestionId\":\"" + dr["Id"].ToString() + "\",\"QuestionName\":\"" + "Ques No. " + question + "\"},";
                        }
                    }
                    json = "[" + json.TrimEnd(',') + "]";
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return Json(json, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region RoleError Get
        [HttpGet]
        public ActionResult RoleError()
        {
            ManageAdminModel model = new ManageAdminModel();
            List<Models.CreateRolesModel> rolesList = new List<CreateRolesModel>();
            rolesList = comMethods.AllRoles();
            model.roleList = rolesList;
            return View(model);
        }
        #endregion

        #region ProjectReminderEmail
        [SkipLogin]
        public void ProjectReminderEmail(string id)
        {
            if (id == "640435B2-F0B8-4E2F-8171-BDBD8016A933")
            {
                actionResult = adminAction.Project_LoadByClosingDate();
                if (actionResult.IsSuccess)
                {
                    System.Threading.Tasks.Parallel.ForEach(actionResult.dtResult.AsEnumerable(), row =>
                    {
                        Email.ProjectReminderEmail((row["Email"] != DBNull.Value ? Convert.ToString(row["Email"]) : ""), row["FirstName"] != DBNull.Value ? Convert.ToString(row["FirstName"]) : "<-No Name->",
                            row["ProjectName"] != DBNull.Value ? Convert.ToString(row["ProjectName"]) : "<-No Name->", row["ClosingDate"] != DBNull.Value ? Convert.ToDateTime(row["ClosingDate"]).ToString("MM/dd/yyyy") : "",
                           row["TotalResponsesYet"] != DBNull.Value ? Convert.ToString(row["TotalResponsesYet"]) : "0", row["ProjectQuota"] != DBNull.Value ? Convert.ToString(row["ProjectQuota"]) : "0");
                    });
                }

            }
            //return View();
        }
        #endregion

        #region TestSurvey Get
        [HttpGet]
        public ActionResult TestSurvey(int? id = 0)
        {
            SurveyResponseModel model = new SurveyResponseModel();
            try
            {
                projectBase.Id = id.GetValueOrDefault();
                ViewBag.RedirectProjectId = 0;
                string productMsg = "";
                RespondentController respondent = new RespondentController();
                actionResult = adminAction.Project_LoadById(projectBase);
                if (actionResult.IsSuccess)
                {
                    DataRow dr = actionResult.dtResult.Rows[0];
                    string Guid = dr["ProjectGUID"] != null ? dr["ProjectGUID"].ToString() : "";
                    model = respondent.GetProjectPreview(Guid);
                    string products = string.Empty;
                    if (model.projectModel.lstProductProjectModel != null && model.projectModel.lstProductProjectModel.Count > 0)
                    {
                        foreach (var prod in model.projectModel.lstProductProjectModel)
                        {
                            productMsg += prod.ProductMessage + "~";
                            // products += "{\"ProductLogicalId\":\"" + prod.ProductLogicalId + "\",\"Description\":\"" + prod.Description + "\"},";
                        }
                        productMsg = productMsg.TrimEnd('~');
                        ViewBag.productsList = productMsg;
                        // ViewBag.productsList = "{\"data\":[" + products.Trim(',') + "]}";
                    }
                    ViewBag.ProjectId = model.projectModel.Id;

                    if (model.projectModel.RedirectUrl.Contains("UID"))
                    {
                        SurveyLoadBase surveyLoadBase = new SurveyLoadBase();
                        string TotalRedirectUrl = model.projectModel.RedirectUrl;
                        int endPos = TotalRedirectUrl.IndexOf("&&");
                        int strPos = TotalRedirectUrl.IndexOf("surveyId=");
                        int end = endPos - (strPos + 9);
                        string SurveyGuid = TotalRedirectUrl.Substring(strPos + 9, end);
                        surveyLoadBase.GUID = SurveyGuid;
                        actionResult = new SRV.BaseLayer.ActionResult();
                        actionResult = respondentAction.Survey_LoadByGUID(surveyLoadBase);
                        if (actionResult.IsSuccess)
                        {
                            DataRow drRedirectSurvey = actionResult.dtResult.Rows[0];
                            ViewBag.RedirectProjectId = drRedirectSurvey["Id"] != DBNull.Value ? Convert.ToInt32(drRedirectSurvey["Id"]) : 0;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return View("_PartialTestSurvey", model);
        }
        #endregion

        #region ImportExcel Get
        [HttpGet]
        public ActionResult ImportExcel(int? id = 0)
        {
            CreateProjectModel model = new CreateProjectModel();
            model = GetProjectDetailById(id);
            ViewBag.ProjectId = model.Id;
            ViewBag.ProjectGUID = model.ProjectGuid;
            return View();
        }
        #endregion

        #region ImportExcel Post
        [HttpPost]
        public ActionResult ImportExcel(FormCollection fc)
        {
            RespondentController respondent = new RespondentController();
            SurveyResponseModel model = new SurveyResponseModel();
            string ProductId = string.Empty;
            string prodId = string.Empty;
            Int64 projectId = fc["hdnProjectId"] != null && fc["hdnProjectId"] != "" ? Convert.ToInt64(fc["hdnProjectId"]) : 0;
            string projectGuid = fc["hdnProjectGuid"] != null && fc["hdnProjectGuid"] != "" ? Convert.ToString(fc["hdnProjectGuid"]) : string.Empty;
            int kanoColCount = 0;
            try
            {
                string uploadedfilename = string.Empty;

                // Create Directory If Not Created.
                if (!System.IO.Directory.Exists(Server.MapPath("~/Content/Uploads/")))
                {
                    System.IO.Directory.CreateDirectory(Server.MapPath("~/Content/Uploads/"));
                }

                // Delete all the previously uploaded files 
                string[] allFiles = System.IO.Directory.GetFiles(Server.MapPath("~/Content/Uploads/"), "*.xls*");// Get all file names
                foreach (string fileName in allFiles)
                {
                    if (System.IO.File.Exists(fileName))
                    {
                        System.GC.Collect();
                        System.GC.WaitForPendingFinalizers();
                        System.IO.File.Delete(fileName);
                    }
                }

                // File Upload Code
                HttpPostedFileBase pfb = Request.Files["excelFile"];
                if (pfb != null && pfb.ContentLength > 0)
                {
                    uploadedfilename = String.Format("{0}{1}{2}{3}{4}", Guid.NewGuid().ToString().Substring(0, 5), "_", DateTime.Now.ToString("MM-dd-yyyy hh-mm-tt"), "_", System.IO.Path.GetFileName(pfb.FileName).Replace(' ', '_'));
                    string filePath = System.IO.Path.Combine(HttpContext.Server.MapPath("~/Content/Uploads"), uploadedfilename);
                    pfb.SaveAs(filePath);
                }


                model = respondent.GetProjectPreview(projectGuid);
                DataSet ds = new DataSet();
                DataTable dt = new DataTable();

                // Datatable for second type 
                DataTable dtTemp = new DataTable();
                dtTemp.Columns.Add("RespId", typeof(string));
                //if (model.projectModel.ProjectType == "2")
                dtTemp.Columns.Add("ProductId", typeof(string));
                dtTemp.Columns.Add("ProjectId", typeof(string));
                dtTemp.Columns.Add("QuestionId", typeof(string));
                dtTemp.Columns.Add("AnsId", typeof(string));
                dtTemp.Columns.Add("ColumnId", typeof(string));
                dtTemp.Columns.Add("AnsValue", typeof(string));

                dt = CommonMethods.ImportExcelXLS(uploadedfilename, true);
                //dt = ds.Tables[0];

                bool IsNullValue = false;
                if (dt != null && dt.Rows.Count > 0)
                {
                    IsNullValue = HasNull(dt);
                    if (IsNullValue)
                    {
                        TempData["ErrorMessage"] = "The responses were not saved, All fields are required, Please check the data in document !";
                        return RedirectToAction("ImportExcel", new { id = projectId });
                    }
                }

                //----------- Check duplicate Ids
                string respList = string.Empty;
                var specList = (from r in dt.AsEnumerable()
                                select r["Resp_Id"]).Distinct().ToList();

                foreach (var item in specList)
                {
                    if (!string.IsNullOrEmpty(item.ToString()))
                    {
                        respList += (Convert.ToInt32(item)) + ",";
                    }
                }
                respList = respList.TrimEnd(',');
                SurveyResponseBase SurveyResponseCheck = new SurveyResponseBase();
                SRV.ActionLayer.Admin.AdminAction adminActionChkResp = new SRV.ActionLayer.Admin.AdminAction();
                SRV.BaseLayer.ActionResult actionResultCheckResponder = new SRV.BaseLayer.ActionResult();
                SurveyResponseCheck.ProjectId = Convert.ToInt32(projectId);
                SurveyResponseCheck.RespList = respList;
                actionResultCheckResponder = adminActionChkResp.Responses_Offline_CheckDuplicateId(SurveyResponseCheck);
                if (actionResultCheckResponder.IsSuccess)
                {
                    string DuplicateIds = Convert.ToString(actionResultCheckResponder.dtResult.Rows[0][0]);
                    if (!string.IsNullOrEmpty(DuplicateIds))
                    {
                        TempData["ErrorMessage"] = DuplicateIds.TrimEnd(',') + " Responder Id are already exists";
                        return RedirectToAction("ImportExcel", new { id = projectId });
                    }
                }
                //---------------- Duplicate check ends here. 

                // Make new display orders for Kano type DataTable 

                foreach (DataRow drTemp in dt.Rows)
                {
                    foreach (DataColumn dcTemp in dt.Columns)
                    {
                        if (dcTemp.ToString().ToLower().Contains("absent"))
                        {
                            drTemp[dcTemp.ToString()] = Convert.ToInt32(drTemp[dcTemp.ToString()]) + 5;
                        }
                    }
                }
                // Make new display orders for Kano type DataTable  ENDS HERE

                string[] columnNames = dt.Columns.Cast<DataColumn>().Select(x => x.ColumnName).ToArray();


                SRV.BaseLayer.Admin.QuestionBase questionBase = new SRV.BaseLayer.Admin.QuestionBase();
                SRV.BaseLayer.ActionResult actionResult = new SRV.BaseLayer.ActionResult();
                SRV.ActionLayer.Admin.AdminAction adminAction = new SRV.ActionLayer.Admin.AdminAction();
                List<QuestionList> questionsList = new List<QuestionList>();
                questionBase.ProjectId = Convert.ToInt32(projectId);
                actionResult = adminAction.QuestionsLoadAll_ByProjectId(questionBase);

                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    foreach (DataRow dr in actionResult.dtResult.Rows)
                    {
                        questionsList.Add(new QuestionList
                        {
                            id = dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0,
                            questionTypeId = dr["QuestionTypeId"] != DBNull.Value ? Convert.ToInt32(dr["QuestionTypeId"]) : 0,
                            displayOrder = dr["DisplayOrder"] != DBNull.Value ? Convert.ToInt32(dr["DisplayOrder"]) : 0

                        });
                    }
                }


                foreach (DataRow dr in dt.Rows)
                {
                    string respId = Convert.ToString(dr[2]);
                    if (model.projectModel.ProjectType == "2")
                    {
                        prodId = Convert.ToString(dr[4]);
                        //  prodId = prodId.Replace("#", ""); // Client Change

                        //--------Get Product Id
                        List<ProductProjectModel> lstProductProjectModel = new List<ProductProjectModel>();
                        productProjectBase.ProjectId = Convert.ToInt32(projectId);
                        SRV.BaseLayer.ActionResult actionResultProducts = new SRV.BaseLayer.ActionResult();
                        actionResultProducts = adminAction.ProductProject_LoadById(productProjectBase);
                        if (actionResultProducts.IsSuccess)
                        {
                            foreach (DataRow drProd in actionResultProducts.dtResult.Rows)
                            {
                                lstProductProjectModel.Add(new ProductProjectModel
                                {
                                    Id = drProd["Id"] != DBNull.Value ? Convert.ToInt32(drProd["Id"]) : 0,
                                    ProjectId = drProd["ProjectId"] != DBNull.Value ? Convert.ToInt32(drProd["ProjectId"]) : 0,
                                    Description = drProd["Description"] != DBNull.Value ? Convert.ToString(drProd["Description"]) : string.Empty,
                                    ProductLogicalId = drProd["ProductLogicalId"] != DBNull.Value ? Convert.ToInt32(drProd["ProductLogicalId"]) : 0,
                                });
                            }
                            int a = lstProductProjectModel.Where(l => l.Description.Replace(' ', '_') == prodId.Replace(' ', '_')).FirstOrDefault().Id;
                            ProductId = Convert.ToString(a);
                        }
                    }
                    int count = (model.projectModel.ProjectType == "2") ? -2 : 0;
                    foreach (string columnName in columnNames)
                    {

                        // First Type RespId,QuesId,AnsId,Ansvalue
                        if (count > 2)//(count != 0 && count != -2 && count != -1)
                        {
                            string[] titleArr = columnName.IndexOf('-') > -1 ? columnName.Split('-') : columnName.Split(' ');
                            string[] quesAnsArr = titleArr[0].Split('_');
                            //string optionTitle = titleArr[1];
                            string questId = Regex.Replace(quesAnsArr[0], @"[^\d]", String.Empty);
                            string optionId = quesAnsArr.Length > 1 ? Regex.Replace(quesAnsArr[1], @"[^\d]", String.Empty) : "";
                            string columnId = quesAnsArr.Length > 2 ? Regex.Replace(quesAnsArr[2], @"[^\d]", String.Empty) : "";
                            string optionvalue = Convert.ToString(dr[columnName]);

                            var res = questionsList.Where(v => v.displayOrder == Convert.ToInt32(questId)).FirstOrDefault();
                            if (res == null) continue;
                            questId = Convert.ToString(res.id);
                            int questTypeId = questionsList.Where(v => v.id == res.id).FirstOrDefault().questionTypeId;
                            if (questTypeId == 8 || questTypeId == 12)
                            {
                                columnId = optionId;
                                if (optionvalue != "")
                                {
                                    var question = model.lstQuestionModel.Where(v => v.QuestionId == res.id).FirstOrDefault();
                                    var options = question.lstOptionsModel.Where(v => v.DisplayOrder == Convert.ToInt32(optionvalue) && v.QuestionId == res.id).FirstOrDefault();
                                    if (options != null)
                                    {
                                        optionId = Convert.ToString(options.OptionId);
                                    }
                                    else
                                    {
                                        optionId = "";
                                    }
                                }
                                else
                                {
                                    optionId = "";
                                }
                                if (columnId != "")
                                {
                                    var question = model.lstQuestionModel.Where(v => v.QuestionId == res.id).FirstOrDefault();
                                    var columns = question.lstMatrixTypeModel.Where(v => v.DisplayOrder == Convert.ToInt32(columnId) && v.QuestionId == res.id).FirstOrDefault();
                                    columnId = (columns != null ? Convert.ToString(columns.Id) : columnId);
                                }

                            }
                            else if (questTypeId == 16)
                            {
                                optionId = "";
                                columnId = "";
                            }
                            else if (questTypeId == 2 || questTypeId == 4 || questTypeId == 5 || questTypeId == 6 || (questTypeId > 7 && questTypeId != 15))//item.QuestionTypeId == 1 || // || item.QuestionTypeId == 11
                            {
                                if (optionvalue != "" && optionId != "")
                                {
                                    var question = model.lstQuestionModel.Where(v => v.QuestionId == res.id).FirstOrDefault();
                                    var options = question.lstOptionsModel.Where(v => v.DisplayOrder == Convert.ToInt32(optionId) && v.QuestionId == res.id).FirstOrDefault();
                                    optionId = Convert.ToString(options.OptionId);
                                }
                                else
                                {
                                    optionId = "";
                                }
                                if (columnId != "")
                                {
                                    var question = model.lstQuestionModel.Where(v => v.QuestionId == res.id).FirstOrDefault();
                                    var columns = question.lstMatrixTypeModel.Where(v => v.DisplayOrder == Convert.ToInt32(columnId) && v.QuestionId == res.id).FirstOrDefault();
                                    columnId = (columns != null ? Convert.ToString(columns.Id) : columnId);
                                }
                            }
                            if (questTypeId == 13) // Single punch matrix type
                            {
                                if (optionvalue != "")
                                {
                                    var question = model.lstQuestionModel.Where(v => v.QuestionId == res.id).FirstOrDefault();
                                    var columns = question.lstMatrixTypeModel.Where(v => v.DisplayOrder == Convert.ToInt32(optionvalue) && v.QuestionId == res.id).FirstOrDefault();
                                    columnId = (columns != null ? Convert.ToString(columns.Id) : columnId);
                                }

                            }
                            else if (questTypeId == 14)
                            {
                                if (optionvalue != "" && optionId != "")
                                {
                                    var question = model.lstQuestionModel.Where(v => v.QuestionId == res.id).FirstOrDefault();
                                    var DropDowns = question.lstDDlMatrixModel.Where(v => v.DDLDisplayOrder == Convert.ToInt32(optionvalue) && v.QuestionId == res.id).FirstOrDefault();
                                    optionvalue = Convert.ToString(DropDowns.Id);
                                }
                                if (columnId != "")
                                {
                                    var question = model.lstQuestionModel.Where(v => v.QuestionId == res.id).FirstOrDefault();
                                    var columns = question.lstMatrixTypeModel.Where(v => v.DisplayOrder == Convert.ToInt32(columnId) && v.QuestionId == res.id).FirstOrDefault();
                                    columnId = (columns != null ? Convert.ToString(columns.Id) : columnId);
                                }
                            }
                            // Add these value to a temp table
                            DataRow drNew = dtTemp.NewRow();
                            drNew["RespId"] = respId;
                            //if (model.projectModel.ProjectType == "2")
                            drNew["ProductId"] = ProductId;
                            drNew["ProjectId"] = projectId;
                            drNew["QuestionId"] = questId;
                            drNew["AnsId"] = optionId;
                            drNew["ColumnId"] = columnId;
                            drNew["AnsValue"] = optionvalue;
                            dtTemp.Rows.Add(drNew);
                        }

                        count++;
                    }
                }

                // Second Type QuestionId, AnswerExpresion

                // Datatable for second type 
                DataTable dtFinal = new DataTable();
                dtFinal.Columns.Add("ProjectId", typeof(long));
                dtFinal.Columns.Add("QuestionId", typeof(long));
                dtFinal.Columns.Add("AnsExpression", typeof(string));
                dtFinal.Columns.Add("RespondentIP", typeof(string));
                dtFinal.Columns.Add("RespId", typeof(string));
                dtFinal.Columns.Add("CreatedOn", typeof(string));
                dtFinal.Columns.Add("Status", typeof(bool));
                //if (model.projectModel.ProjectType == "2")
                dtFinal.Columns.Add("ProductId", typeof(string));

                string quesTempId = string.Empty;
                string tempRespId = string.Empty;
                string tempProductId = string.Empty;
                string ansExpression = string.Empty;
                int countTemp = 0;

                //foreach (DataRow dr in dtTemp.Rows)
                //{

                //    // Prevent the below code to run when first time control enters in the loop 
                //    if (countTemp > 0)
                //    {

                //        if (quesTempId != Convert.ToString(dr["QuestionId"]))
                //        {
                //            // Add record in final table
                //            DataRow drFinalNew = dtFinal.NewRow();
                //            drFinalNew["ProjectId"] = projectId;
                //            drFinalNew["QuestionId"] = Convert.ToInt64(quesTempId);
                //            drFinalNew["AnsExpression"] = ansExpression.Trim('|');
                //            drFinalNew["RespondentIP"] = "";
                //            drFinalNew["RespId"] = tempRespId;
                //            drFinalNew["CreatedOn"] = System.DateTime.Now.ToString();
                //            drFinalNew["Status"] = true;
                //            dtFinal.Rows.Add(drFinalNew);
                //            // Reset the ansExpresion
                //            ansExpression = "";
                //        }
                //    }
                //    // Get current Question's questionType
                //    int tempId = Convert.ToInt32(dr["QuestionId"]);
                //    int itsQuestType = questionsList.Where(v => v.id == tempId).FirstOrDefault().questionTypeId;

                //    if (itsQuestType == 6) // Multiple Punch Type Question
                //    {
                //        ansExpression += Convert.ToString(dr["AnsId"]) + "~" + (Convert.ToInt32(dr["AnsValue"]) == 1 ? "Yes" : "No") + "|";
                //    }
                //    else if (itsQuestType == 1) // Single Punch Type Question
                //    {
                //        SurveyQuestionBase surveyQuestionBase = new SurveyQuestionBase();
                //        surveyQuestionBase.Id = tempId;
                //        SRV.BaseLayer.ActionResult actionResultOptions = new SRV.BaseLayer.ActionResult();
                //        RespondentAction respondentAction = new RespondentAction();
                //        actionResultOptions = respondentAction.Options_LoadAllByQuesId(surveyQuestionBase);
                //        List<OptionsModel> lstOptions = new List<OptionsModel>();
                //        if (actionResultOptions.IsSuccess)
                //        {
                //            int count = 1;
                //            foreach (DataRow drOption in actionResultOptions.dtResult.Rows)
                //            {
                //                if (drOption["Id"] != DBNull.Value)
                //                {
                //                    if (dr["AnsValue"] != DBNull.Value && !String.IsNullOrEmpty(dr["AnsValue"].ToString()) && Convert.ToInt32(dr["AnsValue"]) == count) // Check for AnsValue with count. if its same assign optionId to ansExpression
                //                    {
                //                        ansExpression += Convert.ToString(drOption["Id"]);
                //                        break;
                //                    }
                //                }
                //                count++;
                //            }
                //        }
                //    }
                //    else if (itsQuestType == 2) // Multi Punch
                //    {
                //        if (Convert.ToInt32(dr["AnsValue"]) == 1)
                //            ansExpression += Convert.ToString(dr["AnsId"]) + "|";
                //    }
                //    else if (itsQuestType == 8 || itsQuestType == 10 || itsQuestType == 11 || itsQuestType == 12) //
                //    {
                //        if (Convert.ToInt32(dr["AnsValue"]) == 1)
                //            ansExpression += Convert.ToString(dr["AnsId"]) + "~" + Convert.ToString(dr["ColumnId"]) + "|";
                //    }
                //    //else if (itsQuestType == 9) //  Range Matrix Type Question
                //    //{
                //    //    ansExpression += Convert.ToString(dr["AnsValue"]) + "|";
                //    //}
                //    //else if (itsQuestType == 13) // KANO Model Matrix type question
                //    //{
                //    //    ansExpression += Convert.ToString(dr["AnsId"]) + "~" + Convert.ToString(dr["ColumnId"]) + "|";
                //    //}
                //    //else if (itsQuestType == 14) // KANO Model Matrix type question
                //    //{
                //    //    ansExpression += Convert.ToString(dr["AnsId"]) + "*" + Convert.ToString(dr["AnsValue"]) + "~" + Convert.ToString(dr["ColumnId"]) + "|";
                //    //}
                //    //else
                //    //{
                //    //    ansExpression += Convert.ToString(dr["AnsValue"]) + "|";
                //    //}

                //    countTemp++;
                //    quesTempId = Convert.ToString(dr["QuestionId"]);
                //    tempRespId = Convert.ToString(dr["RespId"]);
                //}

                //int RowLen = (model.projectModel.ProjectType == "2") ? dtTemp.Rows.Count - 1 : dtTemp.Rows.Count-1;


                for (int i = 0; i < dtTemp.Rows.Count; i++)
                {
                    DataRow dr = null;
                    if (dtTemp.Rows.Count == i)
                    {
                        dr = dtTemp.Rows[i - 1];
                        DataRow drFinalNew = dtFinal.NewRow();
                        drFinalNew["ProjectId"] = projectId;
                        drFinalNew["QuestionId"] = Convert.ToInt64(quesTempId);
                        drFinalNew["AnsExpression"] = ansExpression.Trim('|');
                        drFinalNew["RespondentIP"] = "";
                        drFinalNew["RespId"] = tempRespId;
                        //if (model.projectModel.ProjectType == "2")
                        drFinalNew["ProductId"] = (tempProductId != string.Empty) ? tempProductId : "0";
                        drFinalNew["CreatedOn"] = System.DateTime.Now.ToString();
                        drFinalNew["Status"] = true;
                        dtFinal.Rows.Add(drFinalNew);
                        // Reset the ansExpresion
                        ansExpression = "";
                    }
                    else
                    {
                        dr = dtTemp.Rows[i];
                    }

                    // Prevent the below code to run when first time control enters in the loop 
                    if (countTemp > 0)
                    {

                        if (quesTempId != Convert.ToString(dr["QuestionId"]) || tempRespId.ToLower() != Convert.ToString(dr["RespId"]).ToLower())
                        {
                            // Add record in final table
                            if (ansExpression != string.Empty && ansExpression != null)
                            {
                                DataRow drFinalNew = dtFinal.NewRow();
                                drFinalNew["ProjectId"] = projectId;
                                drFinalNew["QuestionId"] = Convert.ToInt64(quesTempId);
                                drFinalNew["AnsExpression"] = ansExpression.Trim('|');
                                drFinalNew["RespondentIP"] = "";
                                drFinalNew["RespId"] = tempRespId;
                                //if (model.projectModel.ProjectType == "2")
                                drFinalNew["ProductId"] = (tempProductId != string.Empty) ? tempProductId : "0";
                                drFinalNew["CreatedOn"] = System.DateTime.Now.ToString();
                                drFinalNew["Status"] = true;
                                dtFinal.Rows.Add(drFinalNew);
                            }
                            // Reset the ansExpresion
                            ansExpression = "";
                        }
                    }
                    if (dtTemp.Rows.Count != i)
                    {
                        // Get current Question's questionType
                        int tempId = Convert.ToInt32(dr["QuestionId"]);
                        int itsQuestType = questionsList.Where(v => v.id == tempId).FirstOrDefault().questionTypeId;

                        if (itsQuestType == 6) // Multiple Punch Type Question
                        {
                            if (dr["AnsId"].ToString() != string.Empty && dr["AnsValue"].ToString() != string.Empty)
                            {
                                ansExpression += Convert.ToString(dr["AnsId"]) + "~" + (!string.IsNullOrEmpty(dr["AnsValue"].ToString()) && Convert.ToInt32(dr["AnsValue"]) == 1 ? "Yes" : "No") + "|";
                            }
                        }
                        else if (itsQuestType == 1) // Single Punch Type Question
                        {
                            SurveyQuestionBase surveyQuestionBase = new SurveyQuestionBase();
                            surveyQuestionBase.Id = tempId;
                            SRV.BaseLayer.ActionResult actionResultOptions = new SRV.BaseLayer.ActionResult();
                            RespondentAction respondentAction = new RespondentAction();
                            actionResultOptions = respondentAction.Options_LoadAllByQuesId(surveyQuestionBase);
                            List<OptionsModel> lstOptions = new List<OptionsModel>();
                            if (actionResultOptions.IsSuccess)
                            {
                                int count = 1;
                                foreach (DataRow drOption in actionResultOptions.dtResult.Rows)
                                {
                                    if (drOption["Id"] != DBNull.Value)
                                    {
                                        if (dr["AnsValue"] != DBNull.Value && !String.IsNullOrEmpty(dr["AnsValue"].ToString()) && Convert.ToInt32(dr["AnsValue"]) == count) // Check for AnsValue with count. if its same assign optionId to ansExpression
                                        {
                                            ansExpression += Convert.ToString(drOption["Id"]);
                                            break;
                                        }
                                    }
                                    count++;
                                }
                            }
                        }
                        else if (itsQuestType == 2) // Multi Punch
                        {
                            if (!string.IsNullOrEmpty(dr["AnsValue"].ToString()) && Convert.ToInt32(dr["AnsValue"]) == 1)
                                ansExpression += Convert.ToString(dr["AnsId"]) + "|";
                        }
                        else if (itsQuestType == 8 || itsQuestType == 12) //Single Punch matrix type
                        {
                            if (dr["AnsId"].ToString() != string.Empty && dr["ColumnId"].ToString() != string.Empty)
                            {
                                ansExpression += Convert.ToString(dr["AnsId"]) + "~" + Convert.ToString(dr["ColumnId"]) + "|";
                            }
                        }
                        else if (itsQuestType == 10 || itsQuestType == 11) //
                        {
                            if (!string.IsNullOrEmpty(dr["AnsValue"].ToString()) && Convert.ToInt32(dr["AnsValue"]) == 1)
                                ansExpression += Convert.ToString(dr["AnsId"]) + "~" + Convert.ToString(dr["ColumnId"]) + "|";
                        }
                        else if (itsQuestType == 9) //  Range Matrix Type Question
                        {
                            if (dr["AnsId"].ToString() != string.Empty && dr["AnsValue"].ToString() != string.Empty && dr["ColumnId"].ToString() != string.Empty)
                            {
                                ansExpression += Convert.ToString(dr["AnsId"]) + "*" + Convert.ToString(dr["AnsValue"]) + "~" + Convert.ToString(dr["ColumnId"]) + "|";
                            }
                        }
                        else if (itsQuestType == 13) // KANO Model Matrix type question
                        {
                            //if (Convert.ToInt32(dr["AnsValue"]) == 1)
                            ansExpression += Convert.ToString(dr["AnsId"]) + "~" + Convert.ToString(dr["ColumnId"]) + "|";
                        }
                        else if (itsQuestType == 14) // Drop Down Matrix type question
                        {
                            if (dr["AnsId"].ToString() != string.Empty && dr["AnsValue"].ToString() != string.Empty && dr["ColumnId"].ToString() != string.Empty)
                            {
                                ansExpression += Convert.ToString(dr["AnsId"]) + "*" + Convert.ToString(dr["AnsValue"]) + "~" + Convert.ToString(dr["ColumnId"]) + "|";
                            }
                        }
                        else
                        {
                            if (dr["AnsValue"].ToString() != string.Empty)
                            {
                                ansExpression += Convert.ToString(dr["AnsValue"]) + "|";
                            }
                        }

                        countTemp++;
                        quesTempId = Convert.ToString(dr["QuestionId"]);
                        tempRespId = Convert.ToString(dr["RespId"]);
                        if (model.projectModel.ProjectType == "2")
                            tempProductId = Convert.ToString(dr["ProductId"]);
                    }
                }


                // Add the final record in table
                if (ansExpression != string.Empty && ansExpression != null)
                {
                    DataRow drFinalNew1 = dtFinal.NewRow();
                    drFinalNew1["ProjectId"] = projectId;
                    drFinalNew1["QuestionId"] = Convert.ToInt64(quesTempId);
                    drFinalNew1["AnsExpression"] = ansExpression.Trim('|');
                    drFinalNew1["RespondentIP"] = "";
                    drFinalNew1["RespId"] = tempRespId;
                    //if (model.projectModel.ProjectType == "2")
                    drFinalNew1["ProductId"] = (tempProductId != string.Empty) ? tempProductId : "0";
                    drFinalNew1["CreatedOn"] = System.DateTime.Now.ToString();
                    drFinalNew1["Status"] = true;
                    dtFinal.Rows.Add(drFinalNew1);
                }

                // Save records to database.
                if (comMethods.saveTodataBase(dtFinal))
                    TempData["SuccessMessage"] = "The provided responses has been saved to server.";
                else
                    TempData["ErrorMessage"] = "The responses were not saved, Please check the data format in document !";

            }
            catch (Exception ex)
            {
                TempData["ErrorMessage"] = "The responses were not saved, Please check the data format in document !";
                ErrorReporting.WebApplicationError(ex);
            }
            return RedirectToAction("ImportExcel", new { id = projectId });
        }
        #endregion

        public bool HasNull(DataTable table)
        {
            foreach (DataColumn column in table.Columns)
            {
                if (table.Rows.OfType<DataRow>().Any(r => r.IsNull(column)))
                    return true;
            }

            return false;
        }

        #region ExportSurveyToExcel Get
        [HttpGet]
        public ActionResult ExportSurveyToExcel(string id)
        {
            RespondentController respondent = new RespondentController();
            SurveyResponseModel model = new SurveyResponseModel();

            try
            {
                DataTable dt = new DataTable();

                string projectGuid = string.Empty;
                projectGuid = id;
                dt.Columns.Add("PID", typeof(string));
                DataRow drCol = dt.NewRow();
                drCol[0] = "LID";
                dt.Rows.Add(drCol);
                DataRow drCol1 = dt.NewRow();
                drCol1[0] = "Resp_Id";
                dt.Rows.Add(drCol1);


                model = respondent.GetProjectPreview(projectGuid);//"00B683D8-05D3-4AAC-9B52-D18F7E705DCD"

                Regex rgx = new Regex("[^a-zA-Z0-9 -]");

                string fileName = rgx.Replace(Guid.NewGuid().ToString().Substring(0, 5) + "_" + DateTime.Now.ToString("MM-dd-yyyy") + "_" + model.projectModel.ProjectName.Replace(" ", ""), "-");

                //----ss
                if (model.projectModel.ProjectType == "2")
                {
                    DataRow drnew = dt.NewRow();
                    drnew[0] = "Product_LogicalId";
                    dt.Rows.Add(drnew);
                    DataRow drnew1 = dt.NewRow();
                    drnew1[0] = "Product_Name";
                    dt.Rows.Add(drnew1);
                }
                //--ss


                if (model.lstQuestionModel != null && model.lstQuestionModel.Count > 0)
                {
                    foreach (var item in model.lstQuestionModel)
                    {
                        if (item.QuestionTypeId == 2 || item.QuestionTypeId == 4 || item.QuestionTypeId == 5 || item.QuestionTypeId == 6)//item.QuestionTypeId == 1 || // || item.QuestionTypeId == 11
                        {
                            if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
                            {
                                foreach (var opt in item.lstOptionsModel)
                                {
                                    DataRow drnew = dt.NewRow();
                                    //drnew[0] = "Q" + item.QuestionId + "_M" + opt.OptionId + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                    drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                    dt.Rows.Add(drnew);
                                }
                            }
                        }

                        //if (item.QuestionTypeId > 7)//item.QuestionTypeId == 1 ||
                        //{
                        //    if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
                        //    {
                        //        foreach (var opt in item.lstOptionsModel)
                        //        {
                        //            DataRow drnew = dt.NewRow();
                        //            drnew[0] = "Q" + item.QuestionId + "_S" + opt.OptionId + "-" + ReplaceHtml.ReplaceText(opt.Option);
                        //            dt.Rows.Add(drnew);
                        //        }
                        //    }
                        //}

                        else if (item.QuestionTypeId == 1 || item.QuestionTypeId == 3 || item.QuestionTypeId == 7 || item.QuestionTypeId == 15)
                        {
                            DataRow drnew = dt.NewRow();
                            //drnew[0] = "Q" + item.QuestionId + "_S" + "-" + ReplaceHtml.ReplaceText(item.QuestionTitle);
                            drnew[0] = "Q" + item.DisplayOrder + "_S" + "-" + ReplaceHtml.ReplaceText(item.QuestionTitle);
                            dt.Rows.Add(drnew);
                        }
                        else if (item.QuestionTypeId == 8 || item.QuestionTypeId == 12)//Single punch matrix type
                        {
                            //if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
                            //{
                            //    foreach (var opt in item.lstOptionsModel)
                            //    {
                            //        DataRow drnew = dt.NewRow();
                            //        //drnew[0] = "Q" + item.QuestionId + "_M" + opt.OptionId + "-" + ReplaceHtml.ReplaceText(opt.Option);
                            //        drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
                            //        dt.Rows.Add(drnew);
                            //    }
                            //}
                            if (item.lstMatrixTypeModel != null && item.lstMatrixTypeModel.Count > 0)
                            {
                                foreach (var col in item.lstMatrixTypeModel)
                                {
                                    DataRow drnew = dt.NewRow();
                                    //drnew[0] = "Q" + item.QuestionId + "_M" + opt.OptionId + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                    drnew[0] = "Q" + item.DisplayOrder + "_M" + col.DisplayOrder + "-" + ReplaceHtml.ReplaceText(col.ColumnName);
                                    dt.Rows.Add(drnew);
                                }
                            }
                        }
                        else if (item.QuestionTypeId == 13)//Kano model matrix type 
                        {
                            if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
                            {
                                foreach (var opt in item.lstOptionsModel)
                                {
                                    DataRow drnew = dt.NewRow();
                                    drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Present";
                                    dt.Rows.Add(drnew);
                                    DataRow drnew1 = dt.NewRow();
                                    drnew1[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Absent";
                                    dt.Rows.Add(drnew1);
                                }
                            }
                        }
                        else if (item.QuestionTypeId == 16)//Kano model matrix type 
                        {
                        }
                        else //item.QuestionTypeId >7
                        {
                            if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
                            {
                                foreach (var opt in item.lstOptionsModel)
                                {
                                    var filterMatrix = item.lstMatrixTypeModel.Where(v => v.QuestionId == item.QuestionId);

                                    foreach (var mat in filterMatrix)
                                    {
                                        DataRow drnew = dt.NewRow();
                                        //drnew[0] = "Q" + item.QuestionId + "_M" + opt.OptionId + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                        drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "_C" + mat.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                        dt.Rows.Add(drnew);
                                    }
                                }
                            }
                        }
                    }
                }

                // Create Directory If Not Created.
                if (!System.IO.Directory.Exists(Server.MapPath("~/Content/Uploads/")))
                {
                    System.IO.Directory.CreateDirectory(Server.MapPath("~/Content/Uploads/"));
                }
                // File Upload Code

                if (dt != null && dt.Rows.Count > 0)
                {
                    DataTable dtF = comMethods.GenerateTransposedTable(dt);

                    ExportToExcel.CreateExcelFile abcd = new ExportToExcel.CreateExcelFile();
                    ExportToExcel.CreateExcelFile.CreateExcelDocument(dtF, Server.MapPath("~/Content/Uploads/" + fileName + ".xlsx"));
                }
                string backupPath = "~/Content/Uploads/";
                //string filename = "AttendanceData.csv";
                if (fileName != "")
                {

                    string path = Server.MapPath(backupPath + fileName + ".xlsx");

                    System.IO.Stream stream = null;

                    if (!System.IO.File.Exists(path))
                    {

                        using (FileStream fs = System.IO.File.Create(path))
                        {
                            Byte[] info = new UTF8Encoding(true).GetBytes(path);
                            fs.Write(info, 0, info.Length);
                        }
                    }

                    stream = new FileStream(path, System.IO.FileMode.Open, System.IO.FileAccess.Read, System.IO.FileShare.Read);
                    long bytesToRead = stream.Length;
                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

                    byte[] bts = System.IO.File.ReadAllBytes(path);
                    Response.Clear();
                    Response.ClearHeaders();
                    Response.AddHeader("Content-Type", "Application/octet-stream");
                    Response.AddHeader("Content-Length", bts.Length.ToString());

                    Response.AddHeader("Content-Disposition", "attachment;   filename=" + fileName + ".xlsx");

                    Response.BinaryWrite(bts);

                    Response.Flush();

                    Response.End();

                    if (stream != null)
                        stream.Close();// Close the File.

                    return File(stream, "application/octet-stream", fileName + ".xlsx");
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return RedirectToAction("ProjectSettings", new { id = model.projectModel.Id });
        }
        #endregion

        #region SurveyOfflineResponses
        public ActionResult SurveyOfflineResponses(int id)
        {
            ViewBag.ProjectId = id;
            ManageRespondersModel model = new ManageRespondersModel();
            List<RespondersModel> lstResponders = new List<RespondersModel>();
            model.lstRespondersModel = lstResponders;
            try
            {
                projectBase.Id = id;
                actionResult = adminAction.Responders_Offline_LoadBy_ProjectId(projectBase);
                if (actionResult.IsSuccess)
                {
                    foreach (DataRow dr in actionResult.dtResult.Rows)
                    {
                        model.lstRespondersModel.Add(new RespondersModel
                        {
                            ProjectGuid = dr["ProjectGUID"] != DBNull.Value ? Convert.ToString(dr["ProjectGUID"]) : "",
                            OfflineResponderId = dr["RespId"] != DBNull.Value ? Convert.ToString(dr["RespId"]) : "",
                            //Email = dr["Email"] != DBNull.Value ? Convert.ToString(dr["Email"]) : "",
                            CreatedOn = dr["CreatedOn"] != DBNull.Value ? DateTime.Parse(dr["CreatedOn"].ToString(), System.Globalization.CultureInfo.InvariantCulture).ToString() : "",
                            ModifiedOn = dr["ModifiedOn"] != DBNull.Value ? DateTime.Parse(dr["CreatedOn"].ToString(), System.Globalization.CultureInfo.InvariantCulture).ToString() : ""
                        });
                    }
                    model.lstRespondersModel = model.lstRespondersModel.GroupBy(m => m.OfflineResponderId).Select(m => m.First()).ToList();
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return View(model);
        }
        #endregion

        #region ViewOfflineSurvey Get
        [HttpGet]
        public ActionResult ViewOfflineSurvey(string surveyId, string uid = "")
        {
            SurveyResponseModel model = new SurveyResponseModel();
            string productIds = "";
            string productNames = "";
            try
            {
                model = comMethods.GetSurvey(surveyId, uid, "admin");
                ViewBag.ProjectId = model.projectModel.Id;
                ViewBag.OfflineResponderId = uid;

                if (model.projectModel.lstProductProjectModel != null && model.projectModel.lstProductProjectModel.Count > 0)
                    foreach (var itm in model.projectModel.lstProductProjectModel)
                    {
                        productIds += Convert.ToString(itm.Id) + ",";
                        productNames += Convert.ToString(itm.ProductLogicalId) + ",";
                    }


                ViewBag.productList = productIds;
                ViewBag.ProductNameList = productNames;
                ViewBag.SurveyId = surveyId;
                ViewBag.uid = uid;


                //if (model.isError)
                //    TempData["ErrorMessage"] = model.ErrorMessage;
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return View(model);
        }
        #endregion

        #region ProjectSummary
        [HttpGet]
        public ActionResult ProjectSummary(int? id = 0)
        {
            ProjectSettingsModel model = new ProjectSettingsModel();
            actionResult = new SRV.BaseLayer.ActionResult();

            ManageRespondersModel manageResModel = new ManageRespondersModel();
            List<RespondersModel> lstResponders = new List<RespondersModel>();
            manageResModel.lstRespondersModel = lstResponders;

            try
            {
                projectBase.Id = Convert.ToInt32(id);

                #region For Online Responses

                manageResModel.lstRespondersModel.Clear();
                actionResult = new SRV.BaseLayer.ActionResult();
                actionResult = adminAction.Project_LoadById(projectBase);
                if (actionResult.IsSuccess)
                {
                    DataRow dr = actionResult.dtResult.Rows[0];
                    projectBase.SuperProjectId = dr["SuperProjectId"] != DBNull.Value ? Convert.ToInt32(dr["SuperProjectId"]) : 0;
                }
                actionResult = new SRV.BaseLayer.ActionResult();
                actionResult = adminAction.CompleteRespondersLoad_ByProjectId(projectBase);
                if (actionResult.IsSuccess)
                {

                    foreach (DataRow dr in actionResult.dtResult.Rows)
                    {
                        manageResModel.lstRespondersModel.Add(new RespondersModel
                        {
                            InvitationGuid = dr["InvitationGuid"] != DBNull.Value ? Convert.ToString(dr["InvitationGuid"]) : ""
                        });
                    }
                    IEnumerable<RespondersModel> filteredList = manageResModel.lstRespondersModel.GroupBy(customer => customer.InvitationGuid).Select(group => group.First());
                    model.OnlineResponses = filteredList.ToList().Count.ToString();
                }
                #endregion

                #region For Offline Responses

                manageResModel.lstRespondersModel.Clear();
                actionResult = new SRV.BaseLayer.ActionResult();
                actionResult = adminAction.Responders_Offline_LoadBy_ProjectId(projectBase);
                if (actionResult.IsSuccess)
                {
                    foreach (DataRow dr in actionResult.dtResult.Rows)
                    {
                        manageResModel.lstRespondersModel.Add(new RespondersModel
                        {
                            OfflineResponderId = dr["RespId"] != DBNull.Value ? Convert.ToString(dr["RespId"]) : ""
                        });
                    }
                    IEnumerable<RespondersModel> filteredList = manageResModel.lstRespondersModel.GroupBy(m => m.OfflineResponderId).Select(m => m.First()).ToList();
                    model.OfflineResponses = filteredList.ToList().Count.ToString();
                }
                #endregion

                #region For Incomplete Responses

                manageResModel.lstRespondersModel.Clear();
                actionResult = new SRV.BaseLayer.ActionResult();
                actionResult = adminAction.Project_LoadById(projectBase);
                if (actionResult.IsSuccess)
                {
                    DataRow dr = actionResult.dtResult.Rows[0];
                    projectBase.SuperProjectId = dr["SuperProjectId"] != DBNull.Value ? Convert.ToInt32(dr["SuperProjectId"]) : 0;
                }
                actionResult = new SRV.BaseLayer.ActionResult();
                actionResult = adminAction.IncompleteRespondersLoad_ByProjectId(projectBase);
                if (actionResult.IsSuccess)
                {
                    foreach (DataRow dr in actionResult.dtResult.Rows)
                    {
                        manageResModel.lstRespondersModel.Add(new RespondersModel
                        {
                            InvitationGuid = dr["InvitationGuid"] != DBNull.Value ? Convert.ToString(dr["InvitationGuid"]) : ""
                        });
                    }
                    IEnumerable<RespondersModel> filteredList = manageResModel.lstRespondersModel.GroupBy(c => c.InvitationGuid).Select(group => group.First());

                    model.IncompleteResponses = filteredList.ToList().Count.ToString();
                }
                #endregion

                actionResult = new SRV.BaseLayer.ActionResult();
                projectSettingsBase.ProjectId = Convert.ToInt32(id);
                actionResult = adminAction.ProjectSettings_LoadById(projectSettingsBase);
                model.ProjectId = Convert.ToInt32(id);
                if (actionResult.IsSuccess)
                {
                    DataRow dr = actionResult.dtResult.Rows[0];
                    model.ProjectQuota = dr["ProjectQuota"] != DBNull.Value ? Convert.ToInt32(dr["ProjectQuota"]) : 0;
                }

            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }

            #region
            //ProjectSettingsModel model = new ProjectSettingsModel();
            //actionResult = new SRV.BaseLayer.ActionResult();
            //try
            //{
            //    projectSettingsBase.ProjectId = Convert.ToInt32(id);
            //    actionResult = adminAction.ProjectSettings_LoadById(projectSettingsBase);
            //    model.ProjectId = Convert.ToInt32(id);
            //    if (actionResult.IsSuccess)
            //    {
            //        DataRow dr = actionResult.dtResult.Rows[0];
            //        //model.Id = dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0;
            //        //model.ProjectId = dr["ProjectId"] != DBNull.Value ? Convert.ToInt32(dr["ProjectId"]) : 0;
            //        //model.ProjectGUID = dr["ProjectGUID"] != DBNull.Value ? Convert.ToString(dr["ProjectGUID"]) : "";
            //        //model.OpeningDate = dr["OpeningDate"] != DBNull.Value ? Convert.ToDateTime(dr["OpeningDate"]).ToString("MM/dd/yyyy") : "";
            //        //model.ClosingDate = dr["ClosingDate"] != DBNull.Value ? Convert.ToDateTime(dr["ClosingDate"]).ToString("MM/dd/yyyy") : "";
            //        //model.PrevNextPageNavigation = dr["PrevNextPageNavigation"] != DBNull.Value ? Convert.ToBoolean(dr["PrevNextPageNavigation"]) : false;
            //        //model.DisableQuestionNumbering = dr["DisableQuestionNumbering"] != DBNull.Value ? Convert.ToBoolean(dr["DisableQuestionNumbering"]) : false;
            //        //model.IsActive = dr["IsActive"] != DBNull.Value ? Convert.ToBoolean(dr["IsActive"]) : false;
            //        //model.Scored = dr["Scored"] != DBNull.Value ? Convert.ToBoolean(dr["Scored"]) : false;
            //        //model.ResumeOfProgress = dr["ResumeOfProgress"] != DBNull.Value ? Convert.ToBoolean(dr["ResumeOfProgress"]) : false;
            //        //model.TestMode = dr["TestMode"] != DBNull.Value ? Convert.ToBoolean(dr["TestMode"]) : false;
            //        model.ProjectQuota = dr["ProjectQuota"] != DBNull.Value ? Convert.ToInt32(dr["ProjectQuota"]) : 0;
            //        model.OnlineResponses = dr["OnlineResponses"] != DBNull.Value ? Convert.ToString(dr["OnlineResponses"]) : "0";
            //        model.OfflineResponses = dr["OfflineResponses"] != DBNull.Value ? Convert.ToString(dr["OfflineResponses"]) : "0";
            //    }
            //}
            //catch (Exception ex)
            //{
            //    ErrorReporting.WebApplicationError(ex);
            //}
            #endregion

            return View(model);
        }
        #endregion

        #region DeleteResponse
        [HttpGet]
        public JsonResult DeleteResponse(int projectId, string mode, string responderId)
        {
            string json = string.Empty;
            SurveyResponseBase surveyResponseBase = new SurveyResponseBase();
            try
            {
                surveyResponseBase.ProjectId = projectId;
                surveyResponseBase.Mode = mode;
                surveyResponseBase.Email = responderId;
                surveyResponseBase.ResponderId = responderId;

                actionResult = adminAction.Response_DeleteByIds(surveyResponseBase);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                    json = "{\"Status\":\"1\"}";
                else
                    json = "{\"Status\":\"-1\"}";

            }
            catch (Exception ex)
            {
                json = "{\"Status\":\"-1\"}";
                ErrorReporting.WebApplicationError(ex);
            }
            return Json(json, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region ExportOfflineResponses
        public ActionResult ExportOfflineResponses(string Id)
        {
            RespondentController respondent = new RespondentController();
            SurveyResponseModel model = new SurveyResponseModel();
            List<RespondersModel> lstResponders = new List<RespondersModel>();
            try
            {
                DataTable dt = new DataTable();

                string projectGuid = string.Empty;
                projectGuid = Id;
                dt.Columns.Add("PID", typeof(string));
                DataRow drCol = dt.NewRow();
                drCol[0] = "LID";
                dt.Rows.Add(drCol);
                DataRow drCol1 = dt.NewRow();
                drCol1[0] = "Resp_Id";
                dt.Rows.Add(drCol1);
                // dt.Columns.Add("Resp_Id", typeof(string));

                model = respondent.GetProjectPreview(projectGuid);

                //--ss
                if (model.projectModel.ProjectType == "2")
                {
                    DataRow drnew = dt.NewRow();
                    drnew[0] = "Product_LogicalId";
                    dt.Rows.Add(drnew);
                    DataRow drnew1 = dt.NewRow();
                    drnew1[0] = "Product_Name";
                    dt.Rows.Add(drnew1);
                }
                //--

                // Load all the questions of this project
                List<QuestionList> questionsList = new List<QuestionList>();
                questionBase.ProjectId = Convert.ToInt32(model.projectModel.Id);
                actionResult = adminAction.QuestionsLoadAll_ByProjectId(questionBase);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    foreach (DataRow dr in actionResult.dtResult.Rows)
                    {
                        questionsList.Add(new QuestionList
                        {
                            id = dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0,
                            questionTypeId = dr["QuestionTypeId"] != DBNull.Value ? Convert.ToInt32(dr["QuestionTypeId"]) : 0
                        });
                    }
                }

                string fileName = Guid.NewGuid().ToString().Substring(0, 5) + "_" + DateTime.Now.ToString("MM-dd-yyyy") + "_" + model.projectModel.ProjectName.Replace(" ", "");

                if (model.lstQuestionModel != null && model.lstQuestionModel.Count > 0)
                {
                    foreach (var item in model.lstQuestionModel)
                    {
                        if (item.QuestionTypeId == 2 || item.QuestionTypeId == 4 || item.QuestionTypeId == 5 || item.QuestionTypeId == 6)//Multi Punch || Constant Sum || Ranking || Matrix Type
                        {
                            if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
                            {
                                foreach (var opt in item.lstOptionsModel)
                                {
                                    DataRow drnew = dt.NewRow();
                                    drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                    dt.Rows.Add(drnew);
                                }
                            }
                        }

                        else if (item.QuestionTypeId == 1 || item.QuestionTypeId == 3 || item.QuestionTypeId == 7 || item.QuestionTypeId == 15)//Single Punch || Range || Simple Text
                        {
                            DataRow drnew = dt.NewRow();
                            drnew[0] = "Q" + item.DisplayOrder + "_S" + "-" + ReplaceHtml.ReplaceText(item.QuestionTitle);
                            dt.Rows.Add(drnew);
                        }
                        else if (item.QuestionTypeId == 8 || item.QuestionTypeId == 12)//Single punch matrix type  && Rank order type
                        {
                            //if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
                            //{
                            //foreach (var opt in item.lstOptionsModel)
                            //{
                            //    DataRow drnew = dt.NewRow();
                            //    //drnew[0] = "Q" + item.QuestionId + "_M" + opt.OptionId + "-" + ReplaceHtml.ReplaceText(opt.Option);
                            //    drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
                            //    dt.Rows.Add(drnew);
                            //}
                            //}
                            if (item.lstMatrixTypeModel != null && item.lstMatrixTypeModel.Count > 0)
                            {
                                foreach (var col in item.lstMatrixTypeModel)
                                {
                                    DataRow drnew = dt.NewRow();
                                    //drnew[0] = "Q" + item.QuestionId + "_M" + opt.OptionId + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                    drnew[0] = "Q" + item.DisplayOrder + "_M" + col.DisplayOrder + "-" + ReplaceHtml.ReplaceText(col.ColumnName);
                                    dt.Rows.Add(drnew);
                                }
                            }
                        }
                        else if (item.QuestionTypeId == 13)//Kano model matrix type 
                        {
                            if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
                            {
                                foreach (var opt in item.lstOptionsModel)
                                {
                                    DataRow drnew = dt.NewRow();
                                    drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Present";
                                    dt.Rows.Add(drnew);
                                    DataRow drnew1 = dt.NewRow();
                                    drnew1[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Absent";
                                    dt.Rows.Add(drnew1);
                                }
                            }
                        }
                        else if (item.QuestionTypeId == 16)
                        {

                        }
                        // Single Punch Matrix Type || Range Matrix Type Question ||
                        // Single punch type answer horizontal position || 
                        // Multiple punch type answer horizontal position || 
                        // Rank order type question || 
                        // KANO Model Matrix type question || Dropdown Matrix type question
                        else if (item.QuestionTypeId > 7 && item.QuestionTypeId != 15)
                        {
                            if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
                            {
                                foreach (var opt in item.lstOptionsModel)
                                {
                                    foreach (var column in item.lstMatrixTypeModel)
                                    {
                                        DataRow drnew = dt.NewRow();
                                        drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "_C" + column.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + " " + ReplaceHtml.ReplaceText(column.ColumnName);
                                        dt.Rows.Add(drnew);
                                    }
                                }
                            }
                        }
                    }
                }
                // Get the Excel Pattern of dt
                DataTable dtF = comMethods.GenerateTransposedTable(dt);

                // Get all the reponders
                projectBase.Id = model.projectModel.Id;
                actionResult = adminAction.Responders_Offline_LoadBy_ProjectId(projectBase);

                // Get Responses
                SurveyLoadBase surveyLoadBase = new SurveyLoadBase();
                string json = string.Empty;
                SRV.BaseLayer.ActionResult actionResultResponses = new SRV.BaseLayer.ActionResult();
                string response_id = "";            //ss
                foreach (DataRow dr in actionResult.dtResult.Rows)
                {
                    lstResponders.Add(new RespondersModel
                    {

                        InvitationGuid = dr["RespId"] != DBNull.Value ? Convert.ToString(dr["RespId"]) : ""


                    });
                }
                IEnumerable<RespondersModel> filteredList = lstResponders
                  .GroupBy(customer => customer.InvitationGuid).Select(group => group.First());
                foreach (var responder in lstResponders)
                {
                    DataTable responsesDt = new DataTable();
                    surveyLoadBase.Id = model.projectModel.Id;
                    surveyLoadBase.OfflineResponderId = (!string.IsNullOrEmpty(responder.InvitationGuid) ? Convert.ToString(responder.InvitationGuid) : "");
                    if (model.projectModel.lstProductProjectModel != null && model.projectModel.lstProductProjectModel.Count > 0)
                    {
                        foreach (var productModel in model.projectModel.lstProductProjectModel)
                        {
                            surveyLoadBase.ProductId = productModel.Id;
                            actionResultResponses = respondentAction.Response_Offline_LoadBy_ProjectId_RespId(surveyLoadBase);
                            if (actionResultResponses.IsSuccess)
                            {
                                if (responsesDt.Rows.Count == 0)
                                    responsesDt = actionResultResponses.dtResult.Copy();
                                else
                                {
                                    foreach (DataRow drThisRespRow in actionResultResponses.dtResult.Rows)
                                        responsesDt.ImportRow(drThisRespRow);

                                }
                            }
                        }
                    }
                    else
                    {
                        actionResultResponses = respondentAction.Response_Offline_LoadBy_ProjectId_RespId(surveyLoadBase);
                        if (actionResultResponses.IsSuccess)
                            responsesDt = actionResultResponses.dtResult.Copy();

                    }

                    // surveyLoadBase.OfflineResponderId = dr["RespId"] != DBNull.Value ? Convert.ToString(dr["RespId"]) : "";
                    //DataRow dr_dtF = dtF.NewRow();
                    //if (String.IsNullOrEmpty(surveyLoadBase.Email)) continue;

                    //actionResultResponses = respondentAction.Response_LoadByProjectId(surveyLoadBase);
                    if (responsesDt.Rows.Count > 0)//actionResultResponses.IsSuccess
                    {
                        //dr_dtF[0] = surveyLoadBase.Email;

                        int count = 1;
                        count = (model.projectModel.ProjectType == "2") ? count + 4 : count + 2;        //ss
                        DataRow dr_dtF = dtF.NewRow();
                        DataRow drTemp = dtF.NewRow();
                        string product_desc = string.Empty;
                        string product_logiId = string.Empty;
                        int oldProductID = responsesDt.Rows[0]["ProductId"] != DBNull.Value ? Convert.ToInt32(responsesDt.Rows[0]["ProductId"]) : 0;
                        foreach (DataRow drResponse in responsesDt.Rows)// actionResultResponses.dtResult.Rows
                        {

                            int optionsCount = 0;
                            response_id = drResponse["RespId"] != DBNull.Value ? (responsesDt.Rows[0]["RespId"]).ToString() : "";
                            int productId = drResponse["ProductId"] != DBNull.Value ? Convert.ToInt32(drResponse["ProductId"]) : 0;
                            if (productId != oldProductID)
                            {
                                count = (model.projectModel.ProjectType == "2") ? 5 : 3;
                                dr_dtF = dtF.NewRow();
                            }

                            //--ss

                            product_desc = drResponse["ProductDescription"] != DBNull.Value ? Convert.ToString(drResponse["ProductDescription"]) : "";
                            product_logiId = drResponse["ProductLogicalId"] != DBNull.Value ? Convert.ToString(drResponse["ProductLogicalId"]) : "";

                            //--ss
                            int questionId = drResponse["QuestionId"] != DBNull.Value ? Convert.ToInt32(drResponse["QuestionId"]) : 0;
                            string answerExpression = drResponse["AnswerExpression"] != DBNull.Value ? Convert.ToString(drResponse["AnswerExpression"]) : "";
                            int questionTypeId = drResponse["QuestionTypeId"] != DBNull.Value ? Convert.ToInt32(drResponse["QuestionTypeId"]) : 1;
                            //dr_dtF[0] = drResponse;

                            // Get Total options count for this question
                            questionBase.Id = Convert.ToInt32(questionId);
                            SRV.BaseLayer.ActionResult actionResultAnswersTemp = new SRV.BaseLayer.ActionResult();
                            actionResultAnswersTemp = adminAction.Option_LoadAllById(questionBase);
                            if (actionResultAnswersTemp.dtResult != null && actionResultAnswersTemp.dtResult.Rows.Count > 0)
                            {
                                optionsCount = actionResultAnswersTemp.dtResult.Rows.Count;
                            }

                            if (questionTypeId == 2 || questionTypeId == 4 || questionTypeId == 5 || questionTypeId == 6)
                            {
                                string[] allAnswers = answerExpression.Split('|');
                                if (allAnswers.Length > 0)
                                {
                                    if (questionTypeId == 2)// Multi Punch
                                    {
                                        string allRec = string.Empty;
                                        List<OptionsModel> matrixAns = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                        if (matrixAns != null)
                                        {
                                            foreach (var item in matrixAns)
                                            {
                                                string matchedId = string.Empty;
                                                foreach (string ans in allAnswers)
                                                {
                                                    if (Convert.ToString(item.OptionId) == ans)
                                                    {
                                                        matchedId = Convert.ToString(item.DisplayOrder);
                                                        break;
                                                    }
                                                }
                                                allRec += matchedId + "|";
                                            }
                                        }
                                        string[] blank = { };
                                        string[] allAnswersMat = (allRec.Length > 0 ? allRec.Substring(0, allRec.Length - 1).Split('|') : blank);
                                        foreach (string ans in allAnswersMat)
                                        {

                                            if (count <= dtF.Columns.Count - 1)
                                            {
                                                if ((Regex.Replace(dtF.Columns[count].ToString().Split('-')[0].Split('_')[1], @"[^\d]", String.Empty)) == ans)
                                                {
                                                    dr_dtF[count] = "1";// ans;
                                                    count++;
                                                    optionsCount--;
                                                }
                                                else
                                                {
                                                    dr_dtF[count] = "2";
                                                    count++;
                                                    optionsCount--;
                                                }
                                            }
                                        }
                                        //count = count + optionsCount;
                                    }
                                    else if (questionTypeId == 6) // matrix Type
                                    {
                                        foreach (string ans in allAnswers)
                                        {
                                            dr_dtF[count] = (ans.ToLower().IndexOf("yes") != -1) ? "1" : "2";
                                            count++;
                                            optionsCount--;
                                        }
                                        count = count + optionsCount;
                                    }
                                    else
                                    {
                                        foreach (string ans in allAnswers)
                                        {
                                            dr_dtF[count] = ans;
                                            count++;
                                            optionsCount--;
                                        }
                                        count = count + optionsCount;
                                    }
                                }
                            }
                            else if (questionTypeId == 1) // Single Punch Answers
                            {
                                List<OptionsModel> SinglePunchAns = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                int ansIndex = 0;
                                if (SinglePunchAns != null)
                                {
                                    foreach (var item in SinglePunchAns)
                                    {
                                        ansIndex++;
                                        if (Convert.ToString(item.OptionId) == answerExpression)
                                        {
                                            break;
                                        }
                                    }

                                    dr_dtF[count] = ansIndex;//answerExpression;
                                    count++;
                                    optionsCount--;
                                }
                            }
                            else if (questionTypeId == 8 || questionTypeId == 12) // single punch matrix answers && rank order type question
                            {
                                List<OptionsModel> optionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                List<MatrixTypeModel> columnDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstMatrixTypeModel;
                                int qustionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).Select(v => v.DisplayOrder).FirstOrDefault();
                                string[] allAnswers = answerExpression.Split('|');

                                List<string> lstColumnOrdinals = new List<string>();

                                string formattedAnswer = string.Empty;
                                string answerValue = string.Empty;
                                if (allAnswers.Length > 0)
                                {
                                    foreach (var ansExpression in allAnswers)
                                    {
                                        string displaycol = string.Empty;
                                        string displayopt = string.Empty;
                                        foreach (var item in optionDisplayOrder)
                                        {

                                            if (Convert.ToString(item.OptionId) == ansExpression.Split('~')[0])
                                            {
                                                displaycol = Convert.ToString(item.DisplayOrder);
                                                break;
                                            }
                                        }
                                        foreach (var item in columnDisplayOrder)
                                        {

                                            if (Convert.ToString(item.Id) == ansExpression.Split('~')[1])
                                            {
                                                displayopt = Convert.ToString(item.DisplayOrder);
                                                break;
                                            }
                                        }
                                        //  formattedAnswer = "Q" + qustionDisplayOrder + "_M" + displaycol + "#" + displayopt;
                                        formattedAnswer = "Q" + qustionDisplayOrder + "_M" + displayopt + "#" + displaycol;
                                        foreach (DataColumn column in dtF.Columns)
                                        {
                                            string[] columnHeader = column.ColumnName.Split('-');
                                            string[] answer = columnHeader[0].Split('_');
                                            if (columnHeader.Length > 1)
                                            {
                                                //if (answer.Length > 2)
                                                //{
                                                string answerInHeader = answer[0] + "_" + answer[1];
                                                string[] formattedAnswerArr = formattedAnswer.Split('#');
                                                if (formattedAnswerArr[0].Replace(" ", String.Empty) == answerInHeader.Replace(" ", String.Empty))
                                                {
                                                    int index = dr_dtF.Table.Columns[column.ColumnName].Ordinal;
                                                    lstColumnOrdinals.Add(index.ToString());
                                                    count++;
                                                    dr_dtF[index] = formattedAnswerArr[1];
                                                }
                                                //}
                                            }
                                        }
                                    }
                                }
                            }
                            else if (questionTypeId == 13) // Kano model matrix type
                            {
                                List<OptionsModel> optionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                List<MatrixTypeModel> columnDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstMatrixTypeModel;
                                int qustionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).Select(v => v.DisplayOrder).FirstOrDefault();
                                string[] allAnswers = answerExpression.Split('|');

                                List<string> lstColumnOrdinals = new List<string>();

                                string formattedAnswer = string.Empty;
                                string answerValue = string.Empty;
                                if (allAnswers.Length > 1)
                                {
                                    int repeatId = 0;
                                    foreach (var ansExpression in allAnswers)
                                    {
                                        string displaycol = string.Empty;
                                        string displayopt = string.Empty;
                                        string displayText = string.Empty;
                                        string text = string.Empty;
                                        if (repeatId == Convert.ToInt32(ansExpression.Split('~')[0]))
                                        {
                                            text = "_Absent";
                                            repeatId = 0;
                                        }
                                        else
                                        {
                                            text = "_Present";
                                        }
                                        foreach (var item in optionDisplayOrder)
                                        {
                                            if (Convert.ToString(item.OptionId) == ansExpression.Split('~')[0])
                                            {
                                                displayText = Convert.ToString(item.Option) + text;
                                                displaycol = Convert.ToString(item.DisplayOrder);
                                                repeatId = Convert.ToInt32(ansExpression.Split('~')[0]);
                                                break;
                                            }
                                        }
                                        foreach (var item in columnDisplayOrder)
                                        {
                                            if (Convert.ToString(item.Id) == ansExpression.Split('~')[1])
                                            {
                                                displayopt = Convert.ToString(item.DisplayOrder);
                                                break;
                                            }
                                        }
                                        formattedAnswer = "Q" + qustionDisplayOrder + "_M" + displaycol + "-" + displayText + "#" + displayopt;
                                        foreach (DataColumn column in dtF.Columns)
                                        {
                                            string[] columnHeader = column.ColumnName.Split('-');
                                            string[] answer = columnHeader[0].Split('_');
                                            if (columnHeader.Length > 1)
                                            {
                                                //if (answer.Length > 2)
                                                //{
                                                string answerInHeader = answer[0] + "_" + answer[1] + "-" + columnHeader[1];
                                                string[] formattedAnswerArr = formattedAnswer.Split('#');
                                                if (formattedAnswerArr[0].Replace(" ", String.Empty) == answerInHeader.Replace(" ", String.Empty))
                                                {
                                                    int index = dr_dtF.Table.Columns[column.ColumnName].Ordinal;
                                                    lstColumnOrdinals.Add(index.ToString());
                                                    count++;
                                                    dr_dtF[index] = formattedAnswerArr[1];
                                                }
                                                //}
                                            }
                                        }
                                    }
                                }
                            }
                            else if (questionTypeId == 16)
                            {

                            }
                            else if (questionTypeId > 7 && questionTypeId != 15)
                            {
                                List<OptionsModel> optionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                List<MatrixTypeModel> columnDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstMatrixTypeModel;
                                List<DDLMatrixTypeModel> dropDownDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstDDlMatrixModel;
                                int qustionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).Select(v => v.DisplayOrder).FirstOrDefault();
                                string[] allAnswers = answerExpression.Split('|');

                                List<string> lstColumnOrdinals = new List<string>();

                                string formattedAnswer = string.Empty;
                                string answerValue = string.Empty;
                                int DDlDisplayOrder = 0;
                                if (allAnswers.Length > 1)
                                {
                                    foreach (var ansExpression in allAnswers)
                                    {
                                        string displaycol = string.Empty;
                                        string displayopt = string.Empty;

                                        if (questionTypeId == 9 || questionTypeId == 14)
                                        {
                                            foreach (var item in optionDisplayOrder)
                                            {
                                                if (Convert.ToString(item.OptionId) == ansExpression.Split('~')[0].Split('*')[0])
                                                {
                                                    displaycol = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            foreach (var item in columnDisplayOrder)
                                            {
                                                if (Convert.ToString(item.Id) == ansExpression.Split('~')[1])
                                                {
                                                    displayopt = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            formattedAnswer = "Q" + qustionDisplayOrder + "_M" + displaycol + "_C" + displayopt;
                                            answerValue = ansExpression.Split('~')[0].Split('*')[1];
                                            if (questionTypeId == 14)
                                                DDlDisplayOrder = dropDownDisplayOrder.Where(l => l.Id == Convert.ToInt32(answerValue)).FirstOrDefault().DDLDisplayOrder;
                                        }
                                        else
                                        {
                                            foreach (var item in optionDisplayOrder)
                                            {
                                                if (Convert.ToString(item.OptionId) == ansExpression.Split('~')[0])
                                                {
                                                    displaycol = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            foreach (var item in columnDisplayOrder)
                                            {
                                                if (Convert.ToString(item.Id) == ansExpression.Split('~')[1])
                                                {
                                                    displayopt = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            formattedAnswer = "Q" + qustionDisplayOrder + "_M" + displaycol + "_C" + displayopt;
                                        }
                                        foreach (DataColumn column in dtF.Columns)
                                        {
                                            string[] columnHeader = column.ColumnName.Split('-');

                                            if (columnHeader.Length > 1)
                                            {
                                                string[] answer = columnHeader[0].Split('_');

                                                if (answer.Length > 2)
                                                {
                                                    string answerInHeader = answer[0] + "_" + answer[1] + "_" + answer[2];

                                                    if (formattedAnswer.Replace(" ", String.Empty) == answerInHeader.Replace(" ", String.Empty))
                                                    {
                                                        int index = dr_dtF.Table.Columns[column.ColumnName].Ordinal;
                                                        lstColumnOrdinals.Add(index.ToString());
                                                        count++;
                                                        if (questionTypeId == 9)
                                                        {
                                                            dr_dtF[index] = answerValue;
                                                        }
                                                        else if (questionTypeId == 14)
                                                        {
                                                            dr_dtF[index] = DDlDisplayOrder;
                                                        }
                                                        else
                                                        {
                                                            dr_dtF[index] = "1";
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }

                                // Fill All the Empty cells with 2

                                for (int i = 0; i <= dtF.Columns.Count - 1; i++)
                                    if (String.IsNullOrEmpty(Convert.ToString(dr_dtF[i])))
                                        dr_dtF[i] = "";
                            }

                            else
                            {
                                dr_dtF[count] = answerExpression;
                                count++;
                                optionsCount--;
                            }

                            if (productId != oldProductID)
                            {
                                dtF.Rows.Add(drTemp);
                                //  drTemp[0] = dtF.Rows.IndexOf(drTemp) + 1;
                                drTemp[0] = model.projectModel.Id;
                                drTemp[1] = model.projectModel.LanguageId;
                                drTemp[2] = response_id;
                                //drTemp[0] = response_id;
                                oldProductID = productId;
                            }

                            drTemp = dr_dtF;
                            //---ss
                            if (model.projectModel.ProjectType == "2")
                            {
                                drTemp[4] = product_desc;
                                drTemp[3] = product_logiId;
                            }
                            //---ss

                        }

                        dtF.Rows.Add(dr_dtF);
                        //dr_dtF[0] = dtF.Rows.IndexOf(dr_dtF) + 1;
                        drTemp[0] = model.projectModel.Id;
                        drTemp[1] = model.projectModel.LanguageId;
                        drTemp[2] = response_id;
                        //---ss
                        if (model.projectModel.ProjectType == "2")
                        {
                            dr_dtF[4] = product_desc;
                            dr_dtF[3] = product_logiId;
                        }

                        //---ss
                    }
                }
                // Make new display orders for Kano type DataTable 

                foreach (DataRow drTempKano in dtF.Rows)
                {
                    foreach (DataColumn dcTempKano in dtF.Columns)
                    {
                        if (dcTempKano.ToString().ToLower().Contains("absent"))
                        {
                            drTempKano[dcTempKano.ToString()] = drTempKano[dcTempKano.ToString()] == "" ? 1 : Convert.ToInt32(drTempKano[dcTempKano.ToString()]) - 5;
                            //drTempKano[dcTempKano.ToString()] = Convert.ToInt32(drTempKano[dcTempKano.ToString()]) - 5;
                        }
                    }
                }
                // Make new display orders for Kano type DataTable  ENDS HERE


                // Create Directory If Not Created.
                if (!System.IO.Directory.Exists(Server.MapPath("~/Content/Uploads/")))
                {
                    System.IO.Directory.CreateDirectory(Server.MapPath("~/Content/Uploads/"));
                }
                // File Upload Code

                if (dtF != null && dtF.Rows.Count > 0)
                {
                    //DataTable dtF = comMethods.GenerateTransposedTable(dt);
                    DataTable newData = comMethods.GenerateTransposedTable(dt);
                    foreach (DataRow dr in dtF.Rows)
                    {
                        if (model.projectModel.ProjectType == "2")
                        {
                            DataRow rw = newData.AsEnumerable().FirstOrDefault(tt => tt.Field<string>("Resp_Id") == dr["Resp_Id"].ToString() && tt.Field<string>("Product_Name") == dr["Product_Name"].ToString());
                            if (rw == null)
                            {
                                DataRow dr_dtF = newData.NewRow();

                                newData.Rows.Add(dr_dtF);
                                foreach (DataColumn dcTempKano in dtF.Columns)
                                {

                                    dr_dtF[dcTempKano.ToString()] = dr[dcTempKano.ToString()];

                                }
                                // row exists
                            }
                        }
                        else
                        {
                            DataRow rw = newData.AsEnumerable().FirstOrDefault(tt => tt.Field<string>("Resp_Id") == dr["Resp_Id"].ToString());
                            if (rw == null)
                            {
                                DataRow dr_dtF = newData.NewRow();

                                newData.Rows.Add(dr_dtF);
                                foreach (DataColumn dcTempKano in dtF.Columns)
                                {

                                    dr_dtF[dcTempKano.ToString()] = dr[dcTempKano.ToString()];

                                }
                                // row exists
                            }
                        }


                    }
                    XLWorkbook workbook = new XLWorkbook();
                    newData.TableName = "Table1";
                    workbook.Worksheets.Add(newData);
                    workbook.SaveAs(Server.MapPath("~/Content/Uploads/" + fileName + ".xlsx"));
                    //ExportToExcel.CreateExcelFile abcd = new ExportToExcel.CreateExcelFile();
                    //ExportToExcel.CreateExcelFile.CreateExcelDocument(newData, Server.MapPath("~/Content/Uploads/" + fileName + ".xlsx"));
                }
                string backupPath = "~/Content/Uploads/";
                //string filename = "AttendanceData.csv";
                if (fileName != "")
                {

                    string path = Server.MapPath(backupPath + fileName + ".xlsx");

                    System.IO.Stream stream = null;

                    if (!System.IO.File.Exists(path))
                    {
                        using (FileStream fs = System.IO.File.Create(path))
                        {
                            Byte[] info = new UTF8Encoding(true).GetBytes(path);
                            fs.Write(info, 0, info.Length);
                        }
                    }

                    stream = new FileStream(path, System.IO.FileMode.Open, System.IO.FileAccess.Read, System.IO.FileShare.Read);
                    long bytesToRead = stream.Length;
                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

                    byte[] bts = System.IO.File.ReadAllBytes(path);
                    Response.Clear();
                    Response.ClearHeaders();
                    Response.AddHeader("Content-Type", "Application/octet-stream");
                    Response.AddHeader("Content-Length", bts.Length.ToString());

                    Response.AddHeader("Content-Disposition", "attachment;   filename=" + fileName + ".xlsx");

                    Response.BinaryWrite(bts);

                    Response.Flush();

                    Response.End();
                    return File(stream, "application/octet-stream", fileName + ".xlsx");
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return RedirectToAction("ProjectSettings", new { id = model.projectModel.Id });
        }
        #endregion

        #region ExportOnlineResponses
        public ActionResult ExportOnlineResponses(string Id)
        {
            RespondentController respondent = new RespondentController();
            SurveyResponseModel model = new SurveyResponseModel();
            List<RespondersModel> lstResponders = new List<RespondersModel>();
            try
            {
                DataTable dt = new DataTable();

                string projectGuid = string.Empty;
                projectGuid = Id;
                dt.Columns.Add("PID", typeof(string));
                DataRow drCol = dt.NewRow();
                drCol[0] = "LID";
                dt.Rows.Add(drCol);
                DataRow drCol1 = dt.NewRow();
                drCol1[0] = "Resp_Id";
                dt.Rows.Add(drCol1);
                // dt.Columns.Add("Resp_Id", typeof(string));

                model = respondent.GetProjectPreview(projectGuid);

                //--ss
                if (model.projectModel.ProjectType == "2")
                {
                    DataRow drnew = dt.NewRow();
                    drnew[0] = "Product_LogicalId";
                    dt.Rows.Add(drnew);
                    DataRow drnew1 = dt.NewRow();
                    drnew1[0] = "Product_Name";
                    dt.Rows.Add(drnew1);
                }
                //--

                // Load all the questions of this project
                List<QuestionList> questionsList = new List<QuestionList>();
                questionBase.ProjectId = Convert.ToInt32(model.projectModel.Id);
                actionResult = adminAction.QuestionsLoadAll_ByProjectId(questionBase);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    foreach (DataRow dr in actionResult.dtResult.Rows)
                    {
                        questionsList.Add(new QuestionList
                        {
                            id = dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0,
                            questionTypeId = dr["QuestionTypeId"] != DBNull.Value ? Convert.ToInt32(dr["QuestionTypeId"]) : 0
                        });
                    }
                }

                string fileName = Guid.NewGuid().ToString().Substring(0, 5) + "_" + DateTime.Now.ToString("MM-dd-yyyy") + "_" + model.projectModel.ProjectName.Replace(" ", "");

                if (model.lstQuestionModel != null && model.lstQuestionModel.Count > 0)
                {
                    foreach (var item in model.lstQuestionModel)
                    {
                        if (item.PipedFrom > 0 && (!item.QuestionTitle.Contains("{{Q}}")))
                        {
                            if (item.QuestionTypeId == 2 || item.QuestionTypeId == 4 || item.QuestionTypeId == 5 || item.QuestionTypeId == 6)//Multi Punch || Constant Sum || Ranking || Matrix Type
                            {
                                if (model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel != null && model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel.Count > 0)
                                {
                                    foreach (var opt in model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel)
                                    {
                                        DataRow drnew = dt.NewRow();
                                        drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                        dt.Rows.Add(drnew);
                                    }
                                }
                            }

                            else if (item.QuestionTypeId == 1 || item.QuestionTypeId == 3 || item.QuestionTypeId == 7 || item.QuestionTypeId == 15)//Single Punch || Range || Simple Text
                            {
                                DataRow drnew = dt.NewRow();
                                drnew[0] = "Q" + item.DisplayOrder + "_S" + "-" + ReplaceHtml.ReplaceText(item.QuestionTitle);
                                dt.Rows.Add(drnew);
                            }
                            else if (item.QuestionTypeId == 8 || item.QuestionTypeId == 12)//Single punch matrix type  && Rank order type
                            {
                                //if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
                                //{
                                //foreach (var opt in item.lstOptionsModel)
                                //{
                                //    DataRow drnew = dt.NewRow();
                                //    //drnew[0] = "Q" + item.QuestionId + "_M" + opt.OptionId + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                //    drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                //    dt.Rows.Add(drnew);
                                //}
                                //}
                                if (item.lstMatrixTypeModel != null && item.lstMatrixTypeModel.Count > 0)
                                {
                                    foreach (var col in item.lstMatrixTypeModel)
                                    {
                                        DataRow drnew = dt.NewRow();
                                        //drnew[0] = "Q" + item.QuestionId + "_M" + opt.OptionId + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                        drnew[0] = "Q" + item.DisplayOrder + "_M" + col.DisplayOrder + "-" + ReplaceHtml.ReplaceText(col.ColumnName);
                                        dt.Rows.Add(drnew);
                                    }
                                }
                            }
                            else if (item.QuestionTypeId == 13)//Kano model matrix type 
                            {
                                if (model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel != null && model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel.Count > 0)
                                {
                                    foreach (var opt in model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel)
                                    {
                                        DataRow drnew = dt.NewRow();
                                        drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Present";
                                        dt.Rows.Add(drnew);
                                        DataRow drnew1 = dt.NewRow();
                                        drnew1[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Absent";
                                        dt.Rows.Add(drnew1);
                                    }
                                }
                            }
                            else if (item.QuestionTypeId == 16) // Description Type Question
                            {

                            }
                            // Single Punch Matrix Type || Range Matrix Type Question ||
                            // Single punch type answer horizontal position || 
                            // Multiple punch type answer horizontal position || 
                            // Rank order type question || 
                            // KANO Model Matrix type question || Dropdown Matrix type question
                            else if (item.QuestionTypeId > 7 && item.QuestionTypeId != 15)
                            {
                                if (model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel != null && model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel.Count > 0)
                                {
                                    foreach (var opt in model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel)
                                    {
                                        foreach (var column in item.lstMatrixTypeModel)
                                        {
                                            DataRow drnew = dt.NewRow();
                                            drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "_C" + column.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + " " + ReplaceHtml.ReplaceText(column.ColumnName);
                                            dt.Rows.Add(drnew);
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {

                            if (item.QuestionTypeId == 2 || item.QuestionTypeId == 4 || item.QuestionTypeId == 5 || item.QuestionTypeId == 6)//Multi Punch || Constant Sum || Ranking || Matrix Type
                            {
                                if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
                                {
                                    foreach (var opt in item.lstOptionsModel)
                                    {
                                        DataRow drnew = dt.NewRow();
                                        drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                        dt.Rows.Add(drnew);
                                    }
                                }
                            }

                            else if (item.QuestionTypeId == 1 || item.QuestionTypeId == 3 || item.QuestionTypeId == 7 || item.QuestionTypeId == 15)//Single Punch || Range || Simple Text
                            {
                                DataRow drnew = dt.NewRow();
                                drnew[0] = "Q" + item.DisplayOrder + "_S" + "-" + ReplaceHtml.ReplaceText(item.QuestionTitle);
                                dt.Rows.Add(drnew);
                            }
                            else if (item.QuestionTypeId == 8 || item.QuestionTypeId == 12)//Single punch matrix type  && Rank order type
                            {
                                //if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
                                //{
                                //foreach (var opt in item.lstOptionsModel)
                                //{
                                //    DataRow drnew = dt.NewRow();
                                //    //drnew[0] = "Q" + item.QuestionId + "_M" + opt.OptionId + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                //    drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                //    dt.Rows.Add(drnew);
                                //}
                                //}
                                if (item.lstMatrixTypeModel != null && item.lstMatrixTypeModel.Count > 0)
                                {
                                    foreach (var col in item.lstMatrixTypeModel)
                                    {
                                        DataRow drnew = dt.NewRow();
                                        //drnew[0] = "Q" + item.QuestionId + "_M" + opt.OptionId + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                        drnew[0] = "Q" + item.DisplayOrder + "_M" + col.DisplayOrder + "-" + ReplaceHtml.ReplaceText(col.ColumnName);
                                        dt.Rows.Add(drnew);
                                    }
                                }
                            }
                            else if (item.QuestionTypeId == 13)//Kano model matrix type 
                            {
                                if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
                                {
                                    foreach (var opt in item.lstOptionsModel)
                                    {
                                        DataRow drnew = dt.NewRow();
                                        drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Present";
                                        dt.Rows.Add(drnew);
                                        DataRow drnew1 = dt.NewRow();
                                        drnew1[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Absent";
                                        dt.Rows.Add(drnew1);
                                    }
                                }
                            }
                            // Single Punch Matrix Type || Range Matrix Type Question ||
                            // Single punch type answer horizontal position || 
                            // Multiple punch type answer horizontal position || 
                            // Rank order type question || 
                            // KANO Model Matrix type question || Dropdown Matrix type question
                            else if (item.QuestionTypeId > 7 && item.QuestionTypeId != 15)
                            {
                                if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
                                {
                                    foreach (var opt in item.lstOptionsModel)
                                    {
                                        foreach (var column in item.lstMatrixTypeModel)
                                        {
                                            DataRow drnew = dt.NewRow();
                                            drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "_C" + column.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + " " + ReplaceHtml.ReplaceText(column.ColumnName);
                                            dt.Rows.Add(drnew);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                // Get the Excel Pattern of dt
                DataTable dtF = comMethods.GenerateTransposedTable(dt);

                // Get all the reponders
                projectBase.Id = model.projectModel.Id;
                actionResult = adminAction.RespondersLoad_ByProjectId(projectBase);

                // Get Responses
                SurveyLoadBase surveyLoadBase = new SurveyLoadBase();
                string json = string.Empty;
                SRV.BaseLayer.ActionResult actionResultResponses = new SRV.BaseLayer.ActionResult();
                string response_id = "";            //ss
                foreach (DataRow dr in actionResult.dtResult.Rows)
                {
                    lstResponders.Add(new RespondersModel
                    {

                        InvitationGuid = dr["InvitationGuid"] != DBNull.Value ? Convert.ToString(dr["InvitationGuid"]) : "",
                        Email = dr["Email"] != DBNull.Value ? Convert.ToString(dr["Email"]) : "",

                    });
                }
                IEnumerable<RespondersModel> filteredList = lstResponders
                  .GroupBy(customer => customer.InvitationGuid).Select(group => group.First());
                foreach (var responder in lstResponders)
                {
                    DataTable responsesDt = new DataTable();
                    surveyLoadBase.Id = model.projectModel.Id;
                    surveyLoadBase.Email = (!string.IsNullOrEmpty(responder.Email) ? Convert.ToString(responder.Email) : "");
                    surveyLoadBase.ExternalResponderId = (!string.IsNullOrEmpty(responder.InvitationGuid) ? Convert.ToString(responder.InvitationGuid) : "");
                    if (model.projectModel.lstProductProjectModel != null && model.projectModel.lstProductProjectModel.Count > 0)
                    {
                        foreach (var productModel in model.projectModel.lstProductProjectModel)
                        {
                            surveyLoadBase.ProductId = productModel.Id;
                            actionResultResponses = respondentAction.Response_LoadByProjectId(surveyLoadBase);
                            if (actionResultResponses.IsSuccess)
                            {
                                if (responsesDt.Rows.Count == 0)
                                    responsesDt = actionResultResponses.dtResult.Copy();
                                else
                                {
                                    foreach (DataRow drThisRespRow in actionResultResponses.dtResult.Rows)
                                        responsesDt.ImportRow(drThisRespRow);

                                }
                            }
                        }
                    }
                    else
                    {
                        actionResultResponses = respondentAction.Response_LoadByProjectId(surveyLoadBase);
                        if (actionResultResponses.IsSuccess)
                            responsesDt = actionResultResponses.dtResult.Copy();

                    }

                    // surveyLoadBase.OfflineResponderId = dr["RespId"] != DBNull.Value ? Convert.ToString(dr["RespId"]) : "";
                    //DataRow dr_dtF = dtF.NewRow();
                    //if (String.IsNullOrEmpty(surveyLoadBase.Email)) continue;

                    //actionResultResponses = respondentAction.Response_LoadByProjectId(surveyLoadBase);
                    if (responsesDt.Rows.Count > 0)//actionResultResponses.IsSuccess
                    {
                        //dr_dtF[0] = surveyLoadBase.Email;

                        int count = 1;
                        count = (model.projectModel.ProjectType == "2") ? count + 4 : count + 2;        //ss
                        DataRow dr_dtF = dtF.NewRow();
                        DataRow drTemp = dtF.NewRow();
                        string product_desc = string.Empty;
                        string product_logiId = string.Empty;
                        int oldProductID = responsesDt.Rows[0]["ProductId"] != DBNull.Value ? Convert.ToInt32(responsesDt.Rows[0]["ProductId"]) : 0;
                        foreach (DataRow drResponse in responsesDt.Rows)// actionResultResponses.dtResult.Rows
                        {

                            int optionsCount = 0;
                            response_id = drResponse["ReponderId"] != DBNull.Value ? (responsesDt.Rows[0]["ReponderId"]).ToString() : "";
                            int productId = drResponse["ProductId"] != DBNull.Value ? Convert.ToInt32(drResponse["ProductId"]) : 0;
                            if (productId != oldProductID)
                            {
                                count = (model.projectModel.ProjectType == "2") ? 5 : 3;
                                dr_dtF = dtF.NewRow();
                            }

                            //--ss

                            product_desc = drResponse["ProductDescription"] != DBNull.Value ? Convert.ToString(drResponse["ProductDescription"]) : "";
                            product_logiId = drResponse["ProductLogicalId"] != DBNull.Value ? Convert.ToString(drResponse["ProductLogicalId"]) : "";

                            //--ss
                            int questionId = drResponse["QuestionId"] != DBNull.Value ? Convert.ToInt32(drResponse["QuestionId"]) : 0;
                            string answerExpression = drResponse["AnswerExpression"] != DBNull.Value ? Convert.ToString(drResponse["AnswerExpression"]) : "";
                            int questionTypeId = drResponse["QuestionTypeId"] != DBNull.Value ? Convert.ToInt32(drResponse["QuestionTypeId"]) : 1;
                            //dr_dtF[0] = drResponse;

                            // Get Total options count for this question
                            questionBase.Id = Convert.ToInt32(questionId);
                            SRV.BaseLayer.ActionResult actionResultAnswersTemp = new SRV.BaseLayer.ActionResult();
                            actionResultAnswersTemp = adminAction.Option_LoadAllById(questionBase);
                            if (actionResultAnswersTemp.dtResult != null && actionResultAnswersTemp.dtResult.Rows.Count > 0)
                            {
                                optionsCount = actionResultAnswersTemp.dtResult.Rows.Count;
                            }

                            if (questionTypeId == 2 || questionTypeId == 4 || questionTypeId == 5 || questionTypeId == 6)
                            {
                                string[] allAnswers = answerExpression.Split('|');
                                if (allAnswers.Length > 0)
                                {
                                    if (questionTypeId == 2)// Multi Punch
                                    {
                                        string allRec = string.Empty;
                                        List<OptionsModel> matrixAns = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                        if (matrixAns != null)
                                        {
                                            foreach (var item in matrixAns)
                                            {
                                                string matchedId = string.Empty;
                                                foreach (string ans in allAnswers)
                                                {
                                                    if (Convert.ToString(item.OptionId) == ans)
                                                    {
                                                        matchedId = Convert.ToString(item.DisplayOrder);
                                                        break;
                                                    }
                                                }
                                                allRec += matchedId + "|";
                                            }
                                        }
                                        string[] blank = { };
                                        string[] allAnswersMat = (allRec.Length > 0 ? allRec.Substring(0, allRec.Length - 1).Split('|') : blank);
                                        foreach (string ans in allAnswersMat)
                                        {

                                            if (count <= dtF.Columns.Count - 1)
                                            {
                                                if ((Regex.Replace(dtF.Columns[count].ToString().Split('-')[0].Split('_')[1], @"[^\d]", String.Empty)) == ans)
                                                {
                                                    dr_dtF[count] = "1";// ans;
                                                    count++;
                                                    optionsCount--;
                                                }
                                                else
                                                {
                                                    dr_dtF[count] = "2";
                                                    count++;
                                                    optionsCount--;
                                                }
                                            }
                                        }
                                        //count = count + optionsCount;
                                    }
                                    else if (questionTypeId == 6) // matrix Type
                                    {
                                        foreach (string ans in allAnswers)
                                        {
                                            dr_dtF[count] = (ans.ToLower().IndexOf("yes") != -1) ? "1" : "2";
                                            count++;
                                            optionsCount--;
                                        }
                                        count = count + optionsCount;
                                    }
                                    else
                                    {
                                        foreach (string ans in allAnswers)
                                        {
                                            dr_dtF[count] = ans;
                                            count++;
                                            optionsCount--;
                                        }
                                        count = count + optionsCount;
                                    }
                                }
                            }
                            else if (questionTypeId == 1) // Single Punch Answers
                            {
                                List<OptionsModel> SinglePunchAns = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                int ansIndex = 0;
                                if (SinglePunchAns != null)
                                {
                                    foreach (var item in SinglePunchAns)
                                    {
                                        ansIndex++;
                                        if (Convert.ToString(item.OptionId) == answerExpression)
                                        {
                                            break;
                                        }
                                    }
                                    dr_dtF[count] = ansIndex;//answerExpression;
                                    count++;
                                    optionsCount--;
                                }


                            }
                            else if (questionTypeId == 8 || questionTypeId == 12) // single punch matrix answers && rank order type question
                            {
                                List<OptionsModel> optionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                List<MatrixTypeModel> columnDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstMatrixTypeModel;
                                int qustionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).Select(v => v.DisplayOrder).FirstOrDefault();
                                string[] allAnswers = answerExpression.Split('|');

                                List<string> lstColumnOrdinals = new List<string>();

                                string formattedAnswer = string.Empty;
                                string answerValue = string.Empty;
                                if (allAnswers.Length > 0)
                                {
                                    foreach (var ansExpression in allAnswers)
                                    {
                                        string displaycol = string.Empty;
                                        string displayopt = string.Empty;
                                        foreach (var item in optionDisplayOrder)
                                        {

                                            if (Convert.ToString(item.OptionId) == ansExpression.Split('~')[0])
                                            {
                                                displaycol = Convert.ToString(item.DisplayOrder);
                                                break;
                                            }
                                        }
                                        foreach (var item in columnDisplayOrder)
                                        {

                                            if (Convert.ToString(item.Id) == ansExpression.Split('~')[1])
                                            {
                                                displayopt = Convert.ToString(item.DisplayOrder);
                                                break;
                                            }
                                        }
                                        //  formattedAnswer = "Q" + qustionDisplayOrder + "_M" + displaycol + "#" + displayopt;
                                        formattedAnswer = "Q" + qustionDisplayOrder + "_M" + displayopt + "#" + displaycol;
                                        foreach (DataColumn column in dtF.Columns)
                                        {
                                            string[] columnHeader = column.ColumnName.Split('-');
                                            string[] answer = columnHeader[0].Split('_');
                                            if (columnHeader.Length > 1)
                                            {
                                                //if (answer.Length > 2)
                                                //{
                                                string answerInHeader = answer[0] + "_" + answer[1];
                                                string[] formattedAnswerArr = formattedAnswer.Split('#');
                                                if (formattedAnswerArr[0].Replace(" ", String.Empty) == answerInHeader.Replace(" ", String.Empty))
                                                {
                                                    int index = dr_dtF.Table.Columns[column.ColumnName].Ordinal;
                                                    lstColumnOrdinals.Add(index.ToString());
                                                    count++;
                                                    dr_dtF[index] = formattedAnswerArr[1];
                                                }
                                                //}
                                            }
                                        }
                                    }
                                }
                            }
                            else if (questionTypeId == 13) // Kano model matrix type
                            {
                                List<OptionsModel> optionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                List<MatrixTypeModel> columnDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstMatrixTypeModel;
                                int qustionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).Select(v => v.DisplayOrder).FirstOrDefault();
                                string[] allAnswers = answerExpression.Split('|');

                                List<string> lstColumnOrdinals = new List<string>();

                                string formattedAnswer = string.Empty;
                                string answerValue = string.Empty;
                                if (allAnswers.Length > 1)
                                {
                                    int repeatId = 0;
                                    foreach (var ansExpression in allAnswers)
                                    {
                                        string displaycol = string.Empty;
                                        string displayopt = string.Empty;
                                        string displayText = string.Empty;
                                        string text = string.Empty;
                                        if (repeatId == Convert.ToInt32(ansExpression.Split('~')[0]))
                                        {
                                            text = "_Absent";
                                            repeatId = 0;
                                        }
                                        else
                                        {
                                            text = "_Present";
                                        }
                                        foreach (var item in optionDisplayOrder)
                                        {
                                            if (Convert.ToString(item.OptionId) == ansExpression.Split('~')[0])
                                            {
                                                displayText = Convert.ToString(item.Option) + text;
                                                displaycol = Convert.ToString(item.DisplayOrder);
                                                repeatId = Convert.ToInt32(ansExpression.Split('~')[0]);
                                                break;
                                            }
                                        }
                                        foreach (var item in columnDisplayOrder)
                                        {
                                            if (Convert.ToString(item.Id) == ansExpression.Split('~')[1])
                                            {
                                                displayopt = Convert.ToString(item.DisplayOrder);
                                                break;
                                            }
                                        }
                                        formattedAnswer = "Q" + qustionDisplayOrder + "_M" + displaycol + "-" + displayText + "#" + displayopt;
                                        foreach (DataColumn column in dtF.Columns)
                                        {
                                            string[] columnHeader = column.ColumnName.Split('-');
                                            string[] answer = columnHeader[0].Split('_');
                                            if (columnHeader.Length > 1)
                                            {
                                                //if (answer.Length > 2)
                                                //{
                                                string answerInHeader = answer[0] + "_" + answer[1] + "-" + columnHeader[1];
                                                string[] formattedAnswerArr = formattedAnswer.Split('#');
                                                if (formattedAnswerArr[0].Replace(" ", String.Empty) == answerInHeader.Replace(" ", String.Empty))
                                                {
                                                    int index = dr_dtF.Table.Columns[column.ColumnName].Ordinal;
                                                    lstColumnOrdinals.Add(index.ToString());
                                                    count++;
                                                    dr_dtF[index] = formattedAnswerArr[1];
                                                }
                                                //}
                                            }
                                        }
                                    }
                                }
                            }
                            else if (questionTypeId == 16) // Description Type Question
                            {

                            }
                            else if (questionTypeId > 7 && questionTypeId != 15)
                            {
                                List<OptionsModel> optionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                List<MatrixTypeModel> columnDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstMatrixTypeModel;
                                List<DDLMatrixTypeModel> dropDownDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstDDlMatrixModel;
                                count = count + (optionDisplayOrder.Count * columnDisplayOrder.Count);
                                int qustionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).Select(v => v.DisplayOrder).FirstOrDefault();
                                string[] allAnswers = answerExpression.Split('|');

                                List<string> lstColumnOrdinals = new List<string>();
                                string answerData = "0";
                                string formattedAnswer = string.Empty;
                                string answerValue = string.Empty;
                                int DDlDisplayOrder = 0;
                                if (allAnswers.Length > 1)
                                {
                                    foreach (var ansExpression in allAnswers)
                                    {
                                        string displaycol = string.Empty;
                                        string displayopt = string.Empty;

                                        if (questionTypeId == 9 || questionTypeId == 14)
                                        {
                                            foreach (var item in optionDisplayOrder)
                                            {
                                                if (Convert.ToString(item.OptionId) == ansExpression.Split('~')[0].Split('*')[0])
                                                {
                                                    displaycol = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            foreach (var item in columnDisplayOrder)
                                            {
                                                if (Convert.ToString(item.Id) == ansExpression.Split('~')[1])
                                                {
                                                    displayopt = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            formattedAnswer = "Q" + qustionDisplayOrder + "_M" + displaycol + "_C" + displayopt;
                                            answerValue = ansExpression.Split('~')[0].Split('*')[1];
                                            if (questionTypeId == 14)
                                                DDlDisplayOrder = dropDownDisplayOrder.Where(l => l.Id == Convert.ToInt32(answerValue)).FirstOrDefault().DDLDisplayOrder;
                                        }
                                        else
                                        {

                                            foreach (var item in optionDisplayOrder)
                                            {
                                                if (Convert.ToString(item.OptionId) == ansExpression.Split('~')[0])
                                                {
                                                    displaycol = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            foreach (var item in columnDisplayOrder)
                                            {
                                                if (Convert.ToString(item.Id) == ansExpression.Split('~')[1])
                                                {
                                                    displayopt = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            formattedAnswer = "Q" + qustionDisplayOrder + "_M" + displaycol + "_C" + displayopt;
                                        }
                                        foreach (DataColumn column in dtF.Columns)
                                        {
                                            string[] columnHeader = column.ColumnName.Split('-');

                                            if (columnHeader.Length > 1)
                                            {
                                                string[] answer = columnHeader[0].Split('_');

                                                if (answer.Length > 2)
                                                {
                                                    string answerInHeader = answer[0] + "_" + answer[1] + "_" + answer[2];

                                                    if (formattedAnswer.Replace(" ", String.Empty) == answerInHeader.Replace(" ", String.Empty))
                                                    {
                                                        int index = dr_dtF.Table.Columns[column.ColumnName].Ordinal;
                                                        lstColumnOrdinals.Add(index.ToString());
                                                        //count++;
                                                        if (questionTypeId == 9)
                                                        {
                                                            dr_dtF[index] = answerValue;
                                                        }
                                                        else if (questionTypeId == 14)
                                                        {
                                                            dr_dtF[index] = DDlDisplayOrder;
                                                        }
                                                        else
                                                        {
                                                            dr_dtF[index] = "1";
                                                        }
                                                    }

                                                }
                                            }
                                        }
                                    }
                                }

                                // Fill All the Empty cells with 2

                                for (int i = 0; i <= dtF.Columns.Count - 1; i++)
                                    if (String.IsNullOrEmpty(Convert.ToString(dr_dtF[i])))
                                        dr_dtF[i] = "";
                            }

                            else
                            {
                                dr_dtF[count] = answerExpression;
                                count++;
                                optionsCount--;
                            }

                            if (productId != oldProductID)
                            {
                                dtF.Rows.Add(drTemp);
                                //  drTemp[0] = dtF.Rows.IndexOf(drTemp) + 1;
                                drTemp[0] = model.projectModel.Id;
                                drTemp[1] = model.projectModel.LanguageId;
                                drTemp[2] = response_id;
                                //drTemp[0] = response_id;
                                oldProductID = productId;
                            }

                            drTemp = dr_dtF;
                            //---ss
                            if (model.projectModel.ProjectType == "2")
                            {
                                drTemp[4] = product_desc;
                                drTemp[3] = product_logiId;
                            }
                            //---ss

                        }

                        dtF.Rows.Add(dr_dtF);
                        //dr_dtF[0] = dtF.Rows.IndexOf(dr_dtF) + 1;
                        drTemp[0] = model.projectModel.Id;
                        drTemp[1] = model.projectModel.LanguageId;
                        drTemp[2] = response_id;
                        //---ss
                        if (model.projectModel.ProjectType == "2")
                        {
                            dr_dtF[4] = product_desc;
                            dr_dtF[3] = product_logiId;
                        }

                        //---ss
                    }
                }
                // Make new display orders for Kano type DataTable 

                foreach (DataRow drTempKano in dtF.Rows)
                {
                    foreach (DataColumn dcTempKano in dtF.Columns)
                    {
                        if (dcTempKano.ToString().ToLower().Contains("absent"))
                        {
                            drTempKano[dcTempKano.ToString()] = Convert.ToInt32(drTempKano[dcTempKano.ToString()]) - 5;
                        }
                    }
                }
                // Make new display orders for Kano type DataTable  ENDS HERE


                // Create Directory If Not Created.
                if (!System.IO.Directory.Exists(Server.MapPath("~/Content/Uploads/")))
                {
                    System.IO.Directory.CreateDirectory(Server.MapPath("~/Content/Uploads/"));
                }
                // File Upload Code

                if (dtF != null && dtF.Rows.Count > 0)
                {
                    //DataTable dtF = comMethods.GenerateTransposedTable(dt);
                    DataTable newData = comMethods.GenerateTransposedTable(dt);
                    foreach (DataRow dr in dtF.Rows)
                    {
                        if (model.projectModel.ProjectType == "2")
                        {
                            DataRow rw = newData.AsEnumerable().FirstOrDefault(tt => tt.Field<string>("Resp_Id") == dr["Resp_Id"].ToString() && tt.Field<string>("Product_Name") == dr["Product_Name"].ToString());
                            if (rw == null)
                            {
                                DataRow dr_dtF = newData.NewRow();

                                newData.Rows.Add(dr_dtF);
                                foreach (DataColumn dcTempKano in dtF.Columns)
                                {

                                    dr_dtF[dcTempKano.ToString()] = dr[dcTempKano.ToString()];

                                }
                                // row exists
                            }
                        }
                        else
                        {
                            DataRow rw = newData.AsEnumerable().FirstOrDefault(tt => tt.Field<string>("Resp_Id") == dr["Resp_Id"].ToString());
                            if (rw == null)
                            {
                                DataRow dr_dtF = newData.NewRow();

                                newData.Rows.Add(dr_dtF);
                                foreach (DataColumn dcTempKano in dtF.Columns)
                                {

                                    dr_dtF[dcTempKano.ToString()] = dr[dcTempKano.ToString()];

                                }
                                // row exists
                            }
                        }


                    }
                    XLWorkbook workbook = new XLWorkbook();
                    newData.TableName = "Table1";
                    workbook.Worksheets.Add(newData);

                    workbook.SaveAs(Server.MapPath("~/Content/Uploads/" + fileName + ".xlsx"));
                    //ExportToExcel.CreateExcelFile abcd = new ExportToExcel.CreateExcelFile();
                    //ExportToExcel.CreateExcelFile.CreateExcelDocument(newData, Server.MapPath("~/Content/Uploads/" + fileName + ".xlsx"));
                }
                string backupPath = "~/Content/Uploads/";
                //string filename = "AttendanceData.csv";
                if (fileName != "")
                {

                    string path = Server.MapPath(backupPath + fileName + ".xlsx");

                    System.IO.Stream stream = null;

                    if (!System.IO.File.Exists(path))
                    {
                        using (FileStream fs = System.IO.File.Create(path))
                        {
                            Byte[] info = new UTF8Encoding(true).GetBytes(path);
                            fs.Write(info, 0, info.Length);
                        }
                    }

                    stream = new FileStream(path, System.IO.FileMode.Open, System.IO.FileAccess.Read, System.IO.FileShare.Read);
                    long bytesToRead = stream.Length;
                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

                    byte[] bts = System.IO.File.ReadAllBytes(path);
                    Response.Clear();
                    Response.ClearHeaders();
                    Response.AddHeader("Content-Type", "Application/octet-stream");
                    Response.AddHeader("Content-Length", bts.Length.ToString());

                    Response.AddHeader("Content-Disposition", "attachment;   filename=" + fileName + ".xlsx");

                    Response.BinaryWrite(bts);

                    Response.Flush();

                    Response.End();
                    return File(stream, "application/octet-stream", fileName + ".xlsx");
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return RedirectToAction("ProjectSettings", new { id = model.projectModel.Id });
        }
        #endregion

        #region GetSurveyList Get
        [HttpGet]
        public ActionResult GetSurveyList()
        {
            CreateProjectModel model = new CreateProjectModel();
            List<CreateProjectModel> ProjectLst = new List<CreateProjectModel>();
            model.ProjectList = ProjectLst;
            try
            {
                int UserId = Convert.ToInt32(Session["UserId"]);
                adminBase.Id = UserId;
                SRV.BaseLayer.ActionResult actionResultProjects = new SRV.BaseLayer.ActionResult();
                actionResultProjects = adminAction.Poject_LoadAll(adminBase);
                if (actionResultProjects.dsResult.Tables[0] != null && actionResultProjects.dsResult.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in actionResultProjects.dsResult.Tables[0].Rows)
                    {
                        model = new CreateProjectModel();
                        model.Id = dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0;
                        model.ProjectName = dr["ProjectName"] != DBNull.Value ? Convert.ToString(dr["ProjectName"]) : "";
                        model.SuperProjectId = dr["SuperProjectId"] != DBNull.Value ? Convert.ToInt32(dr["SuperProjectId"]) : 0;
                        model.ProjectGuid = dr["ProjectGuid"] != DBNull.Value ? dr["ProjectGuid"].ToString() : "N/A";
                        model.Owner = dr["Owner"] != DBNull.Value ? dr["Owner"].ToString() : "N/A";
                        model.DisplayOrder = dr["DisplayOrder"] != DBNull.Value ? Convert.ToInt32(dr["DisplayOrder"]) : 0;
                        model.RedirectUrl = url + "/Respondent/survey?surveyId=" + model.ProjectGuid + "&&uid=UID";
                        ProjectLst.Add(model);
                    }
                    ProjectLst = ProjectLst.Where(l => l.SuperProjectId == 0).ToList();
                    model.ProjectList = ProjectLst;
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return View("_PartialSurveys", model);
        }
        #endregion

        #region QuestionGroupList Get
        [HttpGet]
        public ActionResult QuestionGroupList(int? Id = 0)
        {
            ViewBag.ProjectId = Id ?? 0;
            List<ProjectQuestionGroupsModel> lstGroup = new List<ProjectQuestionGroupsModel>();
            ProjectQuestionGroupsModel model = new ProjectQuestionGroupsModel();
            ProjectQuestionGroupBase groupBase = new ProjectQuestionGroupBase();
            try
            {
                groupBase.ProjectId = Id ?? 0;
                actionResult = adminAction.QuestionGroupsLoadAll_ByProjectId(groupBase);
                if (actionResult.IsSuccess)
                {
                    foreach (DataRow dr in actionResult.dtResult.Rows)
                    {
                        lstGroup.Add(new Models.ProjectQuestionGroupsModel
                        {
                            Id = dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0,
                            GroupName = dr["GroupName"] != DBNull.Value ? dr["GroupName"].ToString() : "",
                            GroupDescription = dr["GroupDescription"] != DBNull.Value ? dr["GroupDescription"].ToString() : "",
                            GroupQuestions = dr["GroupQuestions"] != DBNull.Value ? dr["GroupQuestions"].ToString() + "," : "",
                            CreatedOn = dr["CreatedOn"] != DBNull.Value ? Convert.ToDateTime(dr["CreatedOn"]).ToString("MM/dd/yyyy") : "N/A",
                            ModifiedOn = dr["ModifiedOn"] != DBNull.Value ? Convert.ToDateTime(dr["ModifiedOn"]).ToString("MM/dd/yyyy") : "N/A"
                        });
                    }
                }
                model.groupList = lstGroup;

                // Load all the question sof this project
                questionBase.ProjectId = Id.GetValueOrDefault();
                SRV.BaseLayer.ActionResult actionResultQuestions = new SRV.BaseLayer.ActionResult();
                actionResultQuestions = adminAction.QuestionsLoadAll_ByProjectId(questionBase);
                //List<SelectListItem> questionList = new List<SelectListItem>();
                List<CreateQuestionModel> questionList = new List<CreateQuestionModel>();

                if (actionResultQuestions.dtResult != null && actionResultQuestions.dtResult.Rows.Count > 0)
                {
                    foreach (DataRow dr in actionResultQuestions.dtResult.Rows)
                    {
                        questionList.Add(new CreateQuestionModel
                        {
                            QuestionId = Convert.ToInt32(dr["Id"].ToString()),
                            QuestionTitle = String.Format("{0}{1}{2}", (dr["DisplayOrder"] != DBNull.Value ? Convert.ToString(dr["DisplayOrder"]) : ""), ". ", ReplaceHtml.ReplaceText(Server.HtmlDecode(dr["QuestionTitle"].ToString())))
                        });
                    }

                    // If We need in Dropdown List then uncomment the below code.
                    //var countrilesList = actionResultQuestions.dtResult.AsEnumerable().ToList();
                    //questionList = countrilesList.Select(dr => new SelectListItem
                    //{
                    //    Text = (dr["DisplayOrder"] != DBNull.Value ? Convert.ToString(dr["DisplayOrder"]) : "") + " . " + ReplaceHtml.ReplaceText(Server.HtmlDecode(dr["QuestionTitle"].ToString())),
                    //    Value = dr["Id"].ToString()
                    //}).ToList();
                }

                ViewBag.Questions = questionList;
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return View(model);
        }
        #endregion

        #region CreateQuestionGroup Get
        [HttpGet]
        public ActionResult CreateQuestionGroup(int? Id = 0, int? Pid = 0)
        {
            ProjectQuestionGroupsModel model = new ProjectQuestionGroupsModel();
            ProjectQuestionGroupBase groupBase = new ProjectQuestionGroupBase();
            ViewBag.ProjectId = Pid ?? 0;
            try
            {
                groupBase.Id = Id ?? 0;
                groupBase.ProjectId = Pid ?? 0;
                if (Id > 0)
                {
                    groupBase.Action = "Edit";
                    actionResult = adminAction.CommonMethodQuestionGroups(groupBase);
                    if (actionResult.IsSuccess)
                    {
                        DataRow dr = actionResult.dtResult.Rows[0];
                        model.Id = dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0;
                        model.GroupName = dr["GroupName"] != DBNull.Value ? dr["GroupName"].ToString() : "";
                        model.GroupDescription = dr["GroupDescription"] != DBNull.Value ? dr["GroupDescription"].ToString() : "";
                        ViewBag.ProjectId = dr["ProjectId"] != DBNull.Value ? Convert.ToInt32(dr["ProjectId"]) : 0;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return View(model);
        }
        #endregion

        #region CreateQuestionGroup Post
        [HttpPost]
        public ActionResult CreateQuestionGroup(ProjectQuestionGroupsModel model, FormCollection fc)
        {
            ProjectQuestionGroupBase groupBase = new ProjectQuestionGroupBase();
            int ProjectId = fc["hdnProjectId"] != null ? Convert.ToInt32(fc["hdnProjectId"]) : 0;
            try
            {
                groupBase.Id = model.Id;
                groupBase.GroupName = model.GroupName;
                groupBase.GroupDescription = model.GroupDescription;
                groupBase.Action = model.Id > 0 ? "Update" : "Create";
                groupBase.ProjectId = ProjectId;
                actionResult = adminAction.CommonMethodQuestionGroups(groupBase);
                if (actionResult.IsSuccess)
                {
                    TempData["SuccessMessage"] = "Question group is " + (model.Id > 0 ? "updated" : "saved") + " successfully.";
                }
                else
                {
                    TempData["ErrorMessage"] = "Error in " + (model.Id > 0 ? "updating" : "saving") + " Question group.";
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return RedirectToAction("QuestionGroupList", new { @Id = groupBase.ProjectId });
        }
        #endregion

        #region DeleteQuestionGroup
        public ActionResult DeleteQuestionGroup(int Id, int Pid)
        {
            ProjectQuestionGroupBase groupBase = new ProjectQuestionGroupBase();
            try
            {
                groupBase.Id = Convert.ToInt32(Id);
                actionResult = adminAction.DeleteQuestionGroup_ById(groupBase);
                if (actionResult.IsSuccess)
                    TempData["SuccessMessage"] = "Question group deleted Successfully.";
                else
                    TempData["ErrorMessage"] = "Error in deleting the question group.";
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return RedirectToAction("QuestionGroupList", new { @Id = Pid });
        }
        #endregion

        #region SetAllQuestions
        [HttpGet]
        public ActionResult SetAllQuestions(int? projectId = 0, int? questionTypeId = 0, int? questionId = 0)
        {
            ViewBag.ProjectId = projectId;
            //Session["ProjectId"] = id;
            CreateQuestionModel model = new CreateQuestionModel();
            try
            {
                model = comMethods.GetQuestionListByQuestionType(projectId, questionTypeId);
                if (model != null)
                {
                    model.QuestionList = model.QuestionList.Where(v => v.QuestionId != questionId.GetValueOrDefault()).ToList();
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return View("_PartialInsertOptions", model);
        }
        #endregion

        #region UpdateQuestionGroups Post
        [ValidateInput(false)]
        [HttpPost]
        public ActionResult UpdateQuestionGroups(FormCollection fc)
        {
            ProjectQuestionGroupBase groupBase = new ProjectQuestionGroupBase();
            string strGroup = fc["hdngroupQuestions"] != null ? fc["hdngroupQuestions"].ToString() : "";
            int projectId = fc["hdnProjectId"] != null ? Convert.ToInt32(fc["hdnProjectId"]) : 0;
            try
            {
                if (!String.IsNullOrEmpty(strGroup) && !String.IsNullOrWhiteSpace(strGroup))
                {
                    groupBase.GroupXml = strGroup;
                    groupBase.ProjectId = projectId;
                    actionResult = adminAction.UpdateQuestionGroups_byProjectId(groupBase);
                    if (actionResult.IsSuccess)
                        TempData["SuccessMessage"] = "Questions assigned to group successfully.";
                }
            }
            catch (Exception ex)
            {
                TempData["ErrorMessage"] = "Error in assigning group !";
                ErrorReporting.WebApplicationError(ex);
            }
            return RedirectToAction("QuestionGroupList", new { Id = projectId });
        }
        #endregion

        #region QuestionsRandomization Get
        public ActionResult QuestionsRandomization(int? Id = 0)
        {
            ViewBag.ProjectId = Id ?? 0;
            List<ProjectQuestionGroupsModel> lstGroup = new List<ProjectQuestionGroupsModel>();
            ManageGroupRandomizationModel model = new ManageGroupRandomizationModel();
            ProjectQuestionGroupBase groupBase = new ProjectQuestionGroupBase();
            try
            {
                groupBase.ProjectId = Id ?? 0;
                actionResult = adminAction.QuestionGroupsLoadAll_ByProjectId(groupBase);
                if (actionResult.IsSuccess)
                {
                    foreach (DataRow dr in actionResult.dtResult.Rows)
                    {
                        lstGroup.Add(new Models.ProjectQuestionGroupsModel
                        {
                            Id = dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0,
                            GroupName = dr["GroupName"] != DBNull.Value ? dr["GroupName"].ToString() : "",
                            GroupDescription = dr["GroupDescription"] != DBNull.Value ? dr["GroupDescription"].ToString() : "",
                            GroupQuestions = dr["GroupQuestions"] != DBNull.Value ? dr["GroupQuestions"].ToString() : "",
                            CreatedOn = dr["CreatedOn"] != DBNull.Value ? Convert.ToDateTime(dr["CreatedOn"]).ToString("MM/dd/yyyy") : "N/A",
                            ModifiedOn = dr["ModifiedOn"] != DBNull.Value ? Convert.ToDateTime(dr["ModifiedOn"]).ToString("MM/dd/yyyy") : "N/A"
                        });
                    }
                }
                model.groupList = lstGroup;

                // Load all the question of this project
                questionBase.ProjectId = Id.GetValueOrDefault();
                SRV.BaseLayer.ActionResult actionResultQuestions = new SRV.BaseLayer.ActionResult();
                actionResultQuestions = adminAction.QuestionsLoadAll_ByProjectId(questionBase);
                //List<SelectListItem> questionList = new List<SelectListItem>();
                List<CreateQuestionModel> questionList = new List<CreateQuestionModel>();

                if (actionResultQuestions.dtResult != null && actionResultQuestions.dtResult.Rows.Count > 0)
                {
                    foreach (DataRow dr in actionResultQuestions.dtResult.Rows)
                    {
                        questionList.Add(new CreateQuestionModel
                        {
                            QuestionId = Convert.ToInt32(dr["Id"].ToString()),
                            QuestionTitle = String.Format("{0}{1}{2}", (dr["DisplayOrder"] != DBNull.Value ? Convert.ToString(dr["DisplayOrder"]) : ""), ". ", ReplaceHtml.ReplaceText(Server.HtmlDecode(dr["QuestionTitle"].ToString()))),
                            QuestionGruopId = dr["GroupId"] != DBNull.Value ? Convert.ToInt32(dr["GroupId"]) : 0
                        });
                    }
                }
                model.questionList = questionList;
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }

            return View(model);
        }
        #endregion

        #region UpdateQuestionsGroupOrder Post
        [ValidateInput(false)]
        [HttpPost]
        public ActionResult UpdateQuestionsGroupOrder(string QuesOrder)
        {
            string json = string.Empty;
            string Groups = string.Empty;
            try
            {
                if (!String.IsNullOrEmpty(QuesOrder))
                {
                    string[] allQuesOrder = QuesOrder.TrimEnd(',').Split(',');
                    if (allQuesOrder != null && allQuesOrder.Length > 0)
                    {
                        for (int i = 0; i <= allQuesOrder.Length - 1; i++)
                        {
                            string[] order = allQuesOrder[i].Split('-');
                            if (order != null && order.Length > 0)
                            {
                                Groups += "<ManageQuestions><GroupId>" + order[0] + "</GroupId><QuestionId>" + order[1] + "</QuestionId><DisplayOrder>" + order[2] + "</DisplayOrder></ManageQuestions>";
                            }
                        }
                    }
                }
                else
                {
                    Groups = "<ManageQuestions><GroupId>0</GroupId><QuestionId>0</QuestionId><DisplayOrder>''</DisplayOrder></ManageQuestions>";
                }
                string OrderXml = "<ProjectQuestion>" + Groups + "</ProjectQuestion>";
                questionBase.ColumnXml = OrderXml;
                questionBase.Action = "groupsDisplayOrder";
                questionBase.ModifiedBy = Session["UserId"] != null ? Convert.ToInt32(Session["UserId"]) : 0;
                actionResult = adminAction.ProjectQuestion_UpdateDisplayOrder(questionBase);
                if (actionResult.IsSuccess)
                {
                    json = "{\"Status\":\"1\",\"Message\":\"Project Question Order is updated successfully\"}";
                }
                else
                {
                    json = "{\"Status\":\"-1\",\"Message\":\"Error in updating Project Question Order\"}";
                }
            }
            catch (Exception ex)
            {
                json = "{\"Status\":\"-1\",\"Message\":\"Error in updating Project Question Order\"}";
                ErrorReporting.WebApplicationError(ex);
            }
            return Json(json, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region PagesRandomization Get
        public ActionResult PagesRandomization(int? Id = 0)
        {
            ViewBag.ProjectId = Id ?? 0;
            List<ProjectQuestionGroupsModel> lstGroup = new List<ProjectQuestionGroupsModel>();
            ManageGroupRandomizationModel model = new ManageGroupRandomizationModel();
            ProjectQuestionGroupBase groupBase = new ProjectQuestionGroupBase();
            try
            {
                groupBase.ProjectId = Id ?? 0;
                actionResult = adminAction.QuestionGroupsLoadAll_ByProjectId(groupBase);
                if (actionResult.IsSuccess)
                {
                    foreach (DataRow dr in actionResult.dtResult.Rows)
                    {
                        lstGroup.Add(new Models.ProjectQuestionGroupsModel
                        {
                            Id = dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0,
                            GroupName = dr["GroupName"] != DBNull.Value ? dr["GroupName"].ToString() : "",
                            GroupDescription = dr["GroupDescription"] != DBNull.Value ? dr["GroupDescription"].ToString() : "",
                            GroupQuestions = dr["GroupQuestions"] != DBNull.Value ? dr["GroupQuestions"].ToString() : "",
                            CreatedOn = dr["CreatedOn"] != DBNull.Value ? Convert.ToDateTime(dr["CreatedOn"]).ToString("MM/dd/yyyy") : "N/A",
                            ModifiedOn = dr["ModifiedOn"] != DBNull.Value ? Convert.ToDateTime(dr["ModifiedOn"]).ToString("MM/dd/yyyy") : "N/A",
                            DisplayOrder = dr["DisplayOrder"] != DBNull.Value ? Convert.ToInt32(dr["DisplayOrder"]) : 0
                        });
                    }
                }
                lstGroup = lstGroup.OrderBy(l => l.DisplayOrder).ToList();
                model.groupList = lstGroup;

                // Load all the question of this project
                questionBase.ProjectId = Id.GetValueOrDefault();
                SRV.BaseLayer.ActionResult actionResultQuestions = new SRV.BaseLayer.ActionResult();
                actionResultQuestions = adminAction.QuestionsLoadAll_ByProjectId(questionBase);
                //List<SelectListItem> questionList = new List<SelectListItem>();
                List<CreateQuestionModel> questionList = new List<CreateQuestionModel>();

                if (actionResultQuestions.dtResult != null && actionResultQuestions.dtResult.Rows.Count > 0)
                {
                    foreach (DataRow dr in actionResultQuestions.dtResult.Rows)
                    {
                        questionList.Add(new CreateQuestionModel
                        {
                            QuestionId = Convert.ToInt32(dr["Id"].ToString()),
                            QuestionTitle = String.Format("{0}{1}{2}", (dr["DisplayOrder"] != DBNull.Value ? Convert.ToString(dr["DisplayOrder"]) : ""), ". ", ReplaceHtml.ReplaceText(Server.HtmlDecode(dr["QuestionTitle"].ToString()))),
                            QuestionGruopId = dr["GroupId"] != DBNull.Value ? Convert.ToInt32(dr["GroupId"]) : 0
                        });
                    }
                }
                model.questionList = questionList;
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }

            return View(model);
        }
        #endregion

        #region UpdateGroupOrder Post
        [ValidateInput(false)]
        [HttpPost]
        public ActionResult UpdateGroupOrder(string GroupOrder)
        {
            ProjectQuestionGroupBase groupBase = new ProjectQuestionGroupBase();
            string json = string.Empty;
            string Groups = string.Empty;
            try
            {
                if (!String.IsNullOrEmpty(GroupOrder))
                {
                    string[] allQuesOrder = GroupOrder.TrimEnd(',').Split(',');
                    if (allQuesOrder != null && allQuesOrder.Length > 0)
                    {
                        for (int i = 0; i <= allQuesOrder.Length - 1; i++)
                        {
                            string[] order = allQuesOrder[i].Split('-');
                            if (order != null && order.Length > 0)
                            {
                                Groups += "<ManageGroupOrder><GroupId>" + order[0] + "</GroupId><DisplayOrder>" + order[1] + "</DisplayOrder></ManageGroupOrder>";
                            }
                        }
                    }
                }
                else
                {
                    Groups = "<ManageGroupOrder><GroupId>0</GroupId><DisplayOrder>''</DisplayOrder></ManageGroupOrder>";
                }
                string OrderXml = "<ProjectQuestion>" + Groups + "</ProjectQuestion>";
                groupBase.GroupXml = OrderXml;
                actionResult = adminAction.ProjectQuestionGroups_UpdateDisplayOrder(groupBase);
                if (actionResult.IsSuccess)
                {
                    json = "{\"Status\":\"1\",\"Message\":\"Project Group Order is updated successfully\"}";
                }
                else
                {
                    json = "{\"Status\":\"-1\",\"Message\":\"Error in updating Project Group Order\"}";
                }
            }
            catch (Exception ex)
            {
                json = "{\"Status\":\"-1\",\"Message\":\"Error in updating Project Group Order\"}";
                ErrorReporting.WebApplicationError(ex);
            }
            return Json(json, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region GetQuestionListByQuestionType
        [HttpGet]
        public JsonResult GetQuestionListByQuestionType(int? projectId = 0, int? questionTypeId = 0, int? questionId = 0, bool? qType = false)
        {
            ViewBag.ProjectId = projectId;
            string json = string.Empty;
            CreateQuestionModel model = new CreateQuestionModel();
            try
            {

                model = comMethods.GetQuestionListByQuestionType(projectId, questionTypeId, qType);

                if (model != null && (questionTypeId == 1 || questionTypeId == 2 || questionTypeId == 7))
                {
                    model.QuestionList = model.QuestionList.Where(v => v.QuestionId != questionId.GetValueOrDefault()).ToList();

                    if (model.QuestionList != null && model.QuestionList.Count > 0)
                    {
                        foreach (var item in model.QuestionList)
                        {
                            json += "{\"QuestionId\":\"" + item.QuestionId + "\",\"QuestionTitle\":\"" + Regex.Replace(item.QuestionTitle.Replace("\r", "").Replace("\n", "").Replace("&nbsp;", ""), @"\s+", " ", RegexOptions.Multiline) + "\",\"isAnswerPiped\":\"" + item.isAnswersPiped.ToString().ToLower() + "\",\"isAnswerExplicating\":\"" + item.isAnswersExplicating.ToString().ToLower() + "\"},";
                        }
                        json = "[" + json.TrimEnd(',') + "]";
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return Json(json, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region LinkProduct
        [HttpGet]
        public ActionResult LinkProduct(int? id = 0)
        {
            ViewBag.PId = id;
            CreateProjectModel model = new CreateProjectModel();
            List<CreateProjectModel> ProjectLst = new List<CreateProjectModel>();
            model.ProjectList = ProjectLst;
            try
            {
                int UserId = Convert.ToInt32(Session["UserId"]);
                adminBase.Id = UserId;
                SRV.BaseLayer.ActionResult actionResultProjects = new SRV.BaseLayer.ActionResult();
                actionResultProjects = adminAction.LinkProduct_LoadAll(adminBase);
                if (actionResultProjects.dtResult != null && actionResultProjects.dtResult.Rows.Count > 0)
                {
                    foreach (DataRow dr in actionResultProjects.dtResult.Rows)
                    {
                        model = new CreateProjectModel();
                        model.Id = dr["projectId"] != DBNull.Value ? Convert.ToInt32(dr["projectId"]) : 0;
                        model.ProjectName = dr["projectName"] != DBNull.Value ? Convert.ToString(dr["projectName"]) : "";
                        model.SurveyLink = dr["linkId"] != DBNull.Value ? Convert.ToInt32(dr["linkId"]) : 0;
                        ProjectLst.Add(model);
                    }
                    model.ProjectList = ProjectLst;
                }


                List<SelectListItem> productList = new List<SelectListItem>();
                productProjectBase.ProjectId = Convert.ToInt32(id);
                actionResult = adminAction.ProductProject_LoadById(productProjectBase);
                if (actionResult.IsSuccess)
                {
                    foreach (DataRow dr in actionResult.dtResult.Rows)
                    {
                        productList.Add(new SelectListItem
                        {
                            Text = dr["ProductLogicalId"] != DBNull.Value ? Convert.ToString(dr["ProductLogicalId"]) : "",
                            Value = dr["Id"] != DBNull.Value ? Convert.ToString(dr["Id"]) : "0"

                        });
                    }
                    ViewBag.productList = productList;
                }


            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return View(model);
        }
        #endregion

        #region SaveLinkProduct
        [ValidateInput(false)]
        [HttpGet]
        public JsonResult SaveLinkProduct(string Xml)
        {
            string json = string.Empty;
            if (Xml != "<root></root>")
            {
                productProjectBase.Xml = Xml;
                actionResult = adminAction.SurveyLink_Insert(productProjectBase);
                if (actionResult.IsSuccess)
                {
                    json = "1";
                }
                else
                {
                    json = "0";
                }
            }
            return Json(json, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region LinkQuestionGroup
        public ActionResult LinkQuestionGoup(int? id = 0)
        {

            List<ProductProjectModel> productList = new List<ProductProjectModel>();
            try
            {

                if (Session["UserId"] != null)
                {
                    productProjectBase.ProjectId = Convert.ToInt32(id);
                    actionResult = adminAction.ProductProject_LoadById(productProjectBase);
                    if (actionResult.IsSuccess)
                    {
                        foreach (DataRow dr in actionResult.dtResult.Rows)
                        {
                            productList.Add(new ProductProjectModel
                            {
                                ProductLogicalId = dr["ProductLogicalId"] != DBNull.Value ? Convert.ToInt32(dr["ProductLogicalId"]) : 0,
                                Id = dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0
                            });
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }

            return View(productList);
        }
        #endregion

        #region BindQuestionByProductId
        public JsonResult BindQuestionByProductId(string Id)
        {
            string json = string.Empty;
            CreateQuestionModel model = new CreateQuestionModel();
            List<CreateQuestionModel> QuestionLst = new List<CreateQuestionModel>();
            try
            {
                questionBase.Id = Convert.ToInt32(Id);
                actionResult = adminAction.Qusetion_LoadAllByProductId(questionBase);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    foreach (DataRow dr in actionResult.dtResult.Rows)
                    {
                        model = new CreateQuestionModel();
                        model.ProjectId = dr["ProjectId"] != DBNull.Value ? Convert.ToInt32(dr["ProjectId"]) : 0;
                        model.QuestionGruopId = dr["LinkedId"] != DBNull.Value ? Convert.ToInt32(dr["LinkedId"]) : 0;
                        model.QuestionId = dr["questionId"] != DBNull.Value ? Convert.ToInt32(dr["questionId"]) : 0;
                        model.QuestionTitle = dr["QuestionTitle"] != DBNull.Value ? Convert.ToString(dr["QuestionTitle"]) : "";
                        QuestionLst.Add(model);
                    }
                    json = Newtonsoft.Json.JsonConvert.SerializeObject(QuestionLst);
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }

            return Json(json, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region SaveLinkQuestion
        [ValidateInput(false)]
        [HttpGet]
        public JsonResult SaveLinkQuestion(string Xml)
        {
            string json = string.Empty;
            if (Xml != "<root></root>")
            {
                productProjectBase.Xml = Xml;
                actionResult = adminAction.LinkQuestion_Insert(productProjectBase);
                if (actionResult.IsSuccess)
                {
                    json = "1";
                }
                else
                {
                    json = "0";
                }
            }
            return Json(json, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region CreateSuperProject Get
        [HttpGet]
        public ActionResult CreateSuperProject(int? id = 0)
        {
            var LanguageList = new List<SelectListItem>();
            var model = new CreateProjectModel();
            List<ProductProjectModel> lstProductProjectModel = new List<ProductProjectModel>();
            model.lstProductProjectModel = lstProductProjectModel;
            try
            {
                projectBase.Id = id.GetValueOrDefault();
                actionResult = adminAction.SuperProject_LoadById(projectBase);
                if (actionResult.IsSuccess)
                {
                    DataRow dr = actionResult.dtResult.Rows[0];
                    model.Id = Convert.ToInt32(dr["Id"]);
                    model.SuperProjectName = dr["SuperProjectName"] != DBNull.Value ? dr["SuperProjectName"].ToString() : string.Empty;
                    model.LanguageId = dr["LanguageId"] != DBNull.Value ? Convert.ToInt32(dr["LanguageId"]) : 0;
                }
                actionResult = adminAction.Language_LoadAll();
                if (actionResult.IsSuccess)
                {
                    foreach (DataRow dr in actionResult.dtResult.Rows)
                    {
                        LanguageList.Add(new SelectListItem { Text = dr["Name"].ToString(), Value = dr["Id"].ToString() });
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            ViewBag.LanguageList = LanguageList;
            return View(model);
        }
        #endregion

        #region CreateSuperProject Post
        [ValidateInput(false)]
        [HttpPost]
        public ActionResult CreateSuperProject(CreateProjectModel model, FormCollection fc)
        {
            int ProjectId = 0;
            try
            {
                if (Session["UserId"] != null)
                {
                    DataTable dt = new DataTable();
                    string status = "created";
                    projectBase.LanguageId = model.LanguageId;
                    projectBase.SuperProjectName = model.SuperProjectName != null ? model.SuperProjectName : "";
                    projectBase.SuperProjectOpeningDate = model.SuperProjectOpeningDate;
                    projectBase.SuperProjectClosingDate = model.SuperProjectClosingDate;
                    projectBase.SuperProjectIsRandomizeSurveys = model.SuperProjectIsRandomizeSurveys;
                    projectBase.OwnerId = Convert.ToInt32(Session["UserId"]);

                    if (model.Id > 0)
                    {
                        projectBase.Id = model.Id;
                        status = "updated";
                    }
                    actionResult = adminAction.SuperProject_Insert_Update(projectBase);
                    if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                    {
                        ProjectId = Convert.ToInt32(actionResult.dtResult.Rows[0][0]);
                        TempData["ProjId"] = ProjectId;
                        TempData["SuccessMessage"] = "Super Project " + status + " successfully.";
                    }
                    else
                    {
                        TempData["ErrorMessage"] = "Error in " + status + " the super project";
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            //return RedirectToAction("CreateProject", new { @ST = "2" });
            return RedirectToAction("CreateProject", new { SPId = ProjectId });
        }
        #endregion

        #region DeleteSuperProject
        public ActionResult DeleteSuperProject(int? id = 0)
        {
            try
            {
                // TempData["SuccessMessage"] = "Project deletion is Ok but prevented while testing.";

                projectBase.SuperProjectId = Convert.ToInt32(id);
                actionResult = adminAction.SuperProjectDelete_ById(projectBase);
                if (actionResult.IsSuccess)
                {
                    TempData["SuccessMessage"] = "Super Project deleted successfully";
                }
                else
                {
                    TempData["ErrorMessage"] = "Super Project deletion is Ok but prevented while testing.";
                    return RedirectToAction("ProjectSettings", new { id = id });
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return RedirectToAction("ProjectList");
        }
        #endregion

        #region ManageProductPosition Get
        public ActionResult ManageProductPosition(int? Id = 0)
        {
            ViewBag.SuperSurveyId = Id;
            PositionTable positionBase = new PositionTable();
            ManageGroupRandomizationModel model = new ManageGroupRandomizationModel();
            List<PositionTableModel> lstPosTable = new List<PositionTableModel>();
            model.positionList = lstPosTable;
            try
            {
                positionBase.SuperSurveyId = Id ?? 0;
                actionResult = adminAction.RespondersLoad_BySuperSurveyId(positionBase);
                if (actionResult.IsSuccess)
                {
                    foreach (DataRow dr in actionResult.dtResult.Rows)
                    {
                        lstPosTable.Add(new Models.PositionTableModel
                        {
                            //Id = dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0,
                            ResponderId = dr["ResponderId"] != DBNull.Value ? dr["ResponderId"].ToString() : "N/A",
                            // SuperSurveyId = dr["SuperSurveyId"] != DBNull.Value ? Convert.ToInt32(dr["SuperSurveyId"]) : 0,
                            // SurveyId = dr["SurveyId"] != DBNull.Value ? Convert.ToInt32(dr["SurveyId"]) : 0,
                            // ProductId = dr["ProductId"] != DBNull.Value ? Convert.ToInt32(dr["ProductId"]) : 0,
                            //ProjectName = dr["ProjectName"] != DBNull.Value ? dr["ProjectName"].ToString() : "N/A",
                            SuperSurveyName = dr["SuperSurveyName"] != DBNull.Value ? dr["SuperSurveyName"].ToString() : "N/A"
                        });
                    }
                    model.positionList = lstPosTable;
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return View(model);
        }
        #endregion

        #region SurveyRandomization Get
        public ActionResult SurveyRandomization(int? SId = 0, string RId = "")
        {
            PositionTable positionBase = new PositionTable();
            ManageGroupRandomizationModel model = new ManageGroupRandomizationModel();
            List<PositionTableModel> lstPosTable = new List<PositionTableModel>();
            model.positionList = lstPosTable;
            try
            {
                positionBase.SuperSurveyId = SId ?? 0;
                positionBase.ResponderId = RId;
                actionResult = adminAction.PositionTable_LoadBySuperSurveyId(positionBase);
                if (actionResult.IsSuccess)
                {
                    foreach (DataRow dr in actionResult.dtResult.Rows)
                    {
                        if (!lstPosTable.Exists(v => v.SurveyId == Convert.ToInt32(dr["SurveyId"])))
                        {
                            lstPosTable.Add(new Models.PositionTableModel
                            {
                                Id = dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0,
                                ResponderId = dr["ResponderId"] != DBNull.Value ? dr["ResponderId"].ToString() : "N/A",
                                SuperSurveyId = dr["SuperSurveyId"] != DBNull.Value ? Convert.ToInt32(dr["SuperSurveyId"]) : 0,
                                SurveyId = dr["SurveyId"] != DBNull.Value ? Convert.ToInt32(dr["SurveyId"]) : 0,
                                ProjectGuid = dr["ProjectGuid"] != DBNull.Value ? Convert.ToString(dr["ProjectGuid"]) : "N/A",
                                ProductId = dr["ProductId"] != DBNull.Value ? Convert.ToInt32(dr["ProductId"]) : 0,
                                ProjectName = dr["ProjectName"] != DBNull.Value ? dr["ProjectName"].ToString() : "N/A",
                                ProductName = dr["ProductName"] != DBNull.Value ? dr["ProductName"].ToString() : "N/A"
                            });
                        }
                    }
                    model.positionList = lstPosTable;
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return View(model);
        }
        #endregion

        #region UpdateSurveyOrder Post
        [ValidateInput(false)]
        [HttpPost]
        public ActionResult UpdateSurveyOrder(string SurveyOrder, string respId)
        {
            PositionTable positionBase = new PositionTable();
            string json = string.Empty;
            string Pos = string.Empty;
            try
            {
                if (!String.IsNullOrEmpty(SurveyOrder))
                {
                    string[] allSurveyOrder = SurveyOrder.TrimEnd(',').Split(',');
                    if (allSurveyOrder != null && allSurveyOrder.Length > 0)
                    {
                        for (int i = 0; i <= allSurveyOrder.Length - 1; i++)
                        {
                            string[] order = allSurveyOrder[i].Split('-');
                            if (order != null && order.Length > 0)
                            {
                                Pos += "<ManageSurveyOrder><SurveyId>" + order[0] + "</SurveyId><DisplayOrder>" + order[1] + "</DisplayOrder></ManageSurveyOrder>";
                            }
                        }
                    }
                }
                else
                {
                    Pos = "<ManageSurveyOrder><SurveyId>0</SurveyId><DisplayOrder>''</DisplayOrder></ManageSurveyOrder>";
                }
                string OrderXml = "<Surveys>" + Pos + "</Surveys>";
                positionBase.OrderXml = OrderXml;
                positionBase.ResponderId = respId;
                actionResult = adminAction.PositionTableSurvey_UpdateDisplayOrder(positionBase);
                if (actionResult.IsSuccess)
                {
                    json = "{\"Status\":\"1\",\"Message\":\"Survey Order has been updated successfully.\"}";
                }
                else
                {
                    json = "{\"Status\":\"-1\",\"Message\":\"Error in updating Survey Order.\"}";
                }
            }
            catch (Exception ex)
            {
                json = "{\"Status\":\"-1\",\"Message\":\"Error in updating Survey Order.\"}";
                ErrorReporting.WebApplicationError(ex);
            }
            return Json(json, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region ProductsRandomization Get
        public ActionResult ProductsRandomization(int? SId = 0, string RId = "")
        {
            PositionTable positionBase = new PositionTable();
            ManageGroupRandomizationModel model = new ManageGroupRandomizationModel();
            List<PositionTableModel> lstPosTable = new List<PositionTableModel>();
            List<ProductProjectModel> lstProductProject = new List<ProductProjectModel>();
            model.positionList = lstPosTable;
            model.productProjectList = lstProductProject;
            try
            {
                positionBase.SuperSurveyId = SId ?? 0;
                positionBase.ResponderId = RId;
                actionResult = adminAction.Products_LoadBySuperSurveyId(positionBase);
                if (actionResult.IsSuccess)
                {
                    foreach (DataRow dr in actionResult.dsResult.Tables[0].Rows)
                    {
                        if (!lstPosTable.Exists(v => v.SurveyId == Convert.ToInt32(dr["SurveyId"])))
                        {
                            lstPosTable.Add(new Models.PositionTableModel
                            {
                                Id = dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0,
                                ResponderId = dr["ResponderId"] != DBNull.Value ? dr["ResponderId"].ToString() : "N/A",
                                SuperSurveyId = dr["SuperSurveyId"] != DBNull.Value ? Convert.ToInt32(dr["SuperSurveyId"]) : 0,
                                SurveyId = dr["SurveyId"] != DBNull.Value ? Convert.ToInt32(dr["SurveyId"]) : 0,
                                ProductId = dr["ProductId"] != DBNull.Value ? Convert.ToInt32(dr["ProductId"]) : 0,
                                ProjectName = dr["ProjectName"] != DBNull.Value ? dr["ProjectName"].ToString() : "N/A",
                                ProductName = dr["ProductName"] != DBNull.Value ? dr["ProductName"].ToString() : "N/A"
                            });
                        }
                    }
                    if (actionResult.dsResult.Tables[1] != null && actionResult.dsResult.Tables[1].Rows.Count > 0)
                    {
                        foreach (DataRow drProduct in actionResult.dsResult.Tables[1].Rows)
                        {
                            lstProductProject.Add(new Models.ProductProjectModel
                            {
                                Id = drProduct["Id"] != DBNull.Value ? Convert.ToInt32(drProduct["Id"]) : 0,
                                ProjectId = drProduct["ProjectId"] != DBNull.Value ? Convert.ToInt32(drProduct["ProjectId"]) : 0,
                                ProductLogicalId = drProduct["ProductLogicalId"] != DBNull.Value ? Convert.ToInt32(drProduct["ProductLogicalId"]) : 0,
                                Description = drProduct["Description"] != DBNull.Value ? Convert.ToString(drProduct["Description"]) : ""
                            });
                        }
                    }
                    model.positionList = lstPosTable;
                    model.productProjectList = lstProductProject;
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return View(model);
        }
        #endregion

        #region UpdateProductOrder Post
        [ValidateInput(false)]
        [HttpPost]
        public ActionResult UpdateProductOrder(string ProductOrder, string respId)
        {
            string json = string.Empty;
            string prod = string.Empty;
            PositionTable positionBase = new PositionTable();
            try
            {
                if (!String.IsNullOrEmpty(ProductOrder))
                {
                    string[] allProdOrder = ProductOrder.TrimEnd(',').Split(',');
                    if (allProdOrder != null && allProdOrder.Length > 0)
                    {
                        for (int i = 0; i <= allProdOrder.Length - 1; i++)
                        {
                            string[] order = allProdOrder[i].Split('-');
                            if (order != null && order.Length > 0)
                            {
                                prod += "<ManageProducts><SurveyId>" + order[0] + "</SurveyId><ProductId>" + order[1] + "</ProductId><DisplayOrder>" + order[2] + "</DisplayOrder></ManageProducts>";
                            }
                        }
                    }
                }
                else
                {
                    prod = "<ManageProducts><SurveyId>0</SurveyId><ProductId>0</ProductId><DisplayOrder>''</DisplayOrder></ManageProducts>";
                }
                string OrderXml = "<Project>" + prod + "</Project>";
                positionBase.OrderXml = OrderXml;
                positionBase.ResponderId = respId;
                actionResult = adminAction.Product_UpdateDisplayOrder(positionBase);
                if (actionResult.IsSuccess)
                {
                    json = "{\"Status\":\"1\",\"Message\":\"Product Order is updated successfully\"}";
                }
                else
                {
                    json = "{\"Status\":\"-1\",\"Message\":\"Error in updating Product Order\"}";
                }
            }
            catch (Exception ex)
            {
                json = "{\"Status\":\"-1\",\"Message\":\"Error in updating Product Order\"}";
                ErrorReporting.WebApplicationError(ex);
            }
            return Json(json, JsonRequestBehavior.AllowGet);
        }
        #endregion

        //#region AddProductResponder
        //[HttpGet]
        //public ActionResult AddProductResponder(int? SPId = 0)
        //{
        //    PositionTable positionBase = new PositionTable();
        //    string json = string.Empty;
        //    try
        //    {
        //        positionBase.SuperSurveyId = Convert.ToInt32(SPId);
        //        actionResult = adminAction.RespondersLoad_BySuperSurveyId(positionBase);
        //        if (actionResult.IsSuccess)
        //        {
        //            foreach (DataRow dr in actionResult.dtResult.Rows)
        //            {
        //                json += "{\"ResponderId\":\"" + (dr["ResponderId"] != DBNull.Value ? dr["ResponderId"].ToString() : "N/A") + "\",\"SuperSurveyName\":\"" + (dr["SuperSurveyName"] != DBNull.Value ? dr["SuperSurveyName"].ToString() : "N/A") + "\"},";
        //            }
        //            json = "{\"data\":[" + json.TrimEnd(',') + "]}";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        ErrorReporting.WebApplicationError(ex);
        //    }
        //    ViewBag.SuperProjectId = SPId;
        //    ViewBag.RespondersList = json;
        //    return View();
        //}
        //#endregion

        //#region AddResponderToPositionTable
        //[HttpGet]
        //public JsonResult AddResponderToPositionTable(int superProjectId, string email)
        //{
        //    List<CreateProjectModel> lstProjectModel = new List<CreateProjectModel>();
        //    string json = json = "{\"Status\":\"1\"}";
        //    int UserId = Convert.ToInt32(Session["UserId"]);

        //    try
        //    {
        //        adminBase.Id = UserId;
        //        SRV.BaseLayer.ActionResult actionResultProj = new SRV.BaseLayer.ActionResult();
        //        actionResultProj = adminAction.Poject_LoadAll(adminBase);
        //        if (actionResultProj.dsResult != null && actionResultProj.dsResult.Tables[0].Rows.Count > 0)
        //        {
        //            DataTable dtResult = actionResultProj.dsResult.Tables[0].Select("SuperProjectId is Not Null and SuperProjectId = '" + superProjectId + "'").CopyToDataTable();
        //            foreach (DataRow dr in dtResult.Rows)
        //            {
        //                lstProjectModel.Add(new CreateProjectModel
        //                {
        //                    Id = dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0,
        //                    SuperProjectId = dr["SuperProjectId"] != DBNull.Value ? Convert.ToInt32(dr["SuperProjectId"]) : 0,
        //                });
        //            }
        //            lstProjectModel = lstProjectModel.OrderByDescending(l => l.Id).ToList();

        //            foreach (var projId in lstProjectModel)
        //            {
        //                List<ProductProjectModel> lstProductProjectModel = new List<ProductProjectModel>();
        //                productProjectBase.ProjectId = projId.Id;
        //                SRV.BaseLayer.ActionResult actionResultProducts = new SRV.BaseLayer.ActionResult();
        //                actionResultProducts = adminAction.ProductProject_LoadById(productProjectBase);
        //                if (actionResultProducts.IsSuccess)
        //                {
        //                    foreach (DataRow drProd in actionResultProducts.dtResult.Rows)
        //                    {
        //                        lstProductProjectModel.Add(new ProductProjectModel
        //                        {
        //                            Id = drProd["Id"] != DBNull.Value ? Convert.ToInt32(drProd["Id"]) : 0,
        //                            ProjectId = drProd["ProjectId"] != DBNull.Value ? Convert.ToInt32(drProd["ProjectId"]) : 0,
        //                        });
        //                    }
        //                }

        //                // Make this Entry to Position Table

        //                if (!String.IsNullOrEmpty(email))
        //                {
        //                    DataTable dtProduct = new DataTable();
        //                    dtProduct.Columns.Add("ProductId", typeof(int));
        //                    dtProduct.Columns.Add("Random", typeof(string));
        //                    Random rnd = new Random();

        //                    string productsXML = "<products>";
        //                    if (lstProductProjectModel != null && lstProductProjectModel.Count > 0)
        //                    {
        //                        foreach (var item in lstProductProjectModel)
        //                        {
        //                            DataRow drProduct = dtProduct.NewRow();
        //                            drProduct["ProductId"] = item.Id;
        //                            drProduct["Random"] = rnd.Next(1, 10).ToString();
        //                            dtProduct.Rows.Add(drProduct);
        //                        }
        //                        bool IsProductRandomize = false;
        //                        projectSettingsBase.ProjectId = projId.Id;
        //                        SRV.BaseLayer.ActionResult actionResultSetting = new SRV.BaseLayer.ActionResult();
        //                        actionResultSetting = adminAction.ProjectSettings_LoadById(projectSettingsBase);
        //                        if (actionResultSetting.IsSuccess)
        //                        {
        //                            IsProductRandomize = actionResultSetting.dtResult.Rows[0]["IsProductRandomize"] != DBNull.Value ? Convert.ToBoolean(actionResultSetting.dtResult.Rows[0]["IsProductRandomize"]) : false;
        //                        }
        //                        if (IsProductRandomize == true)
        //                        {
        //                            dtProduct.DefaultView.Sort = "Random asc";
        //                            dtProduct = dtProduct.DefaultView.ToTable();
        //                        }
        //                        foreach (DataRow drTblProd in dtProduct.Rows)
        //                        {
        //                            productsXML += "<product><PId>" + Convert.ToString(drTblProd["ProductId"]) + "</PId></product>";
        //                        }
        //                    }
        //                    productsXML += "</products>";
        //                    PositionTable positionTable = new PositionTable();
        //                    positionTable.SurveyId = projId.Id;
        //                    positionTable.ProductsXML = productsXML;
        //                    positionTable.ResponderId = email;
        //                    positionTable.SuperSurveyId = superProjectId;
        //                    SRV.BaseLayer.ActionResult actionResultPosition = new SRV.BaseLayer.ActionResult();
        //                    actionResultPosition = adminAction.PositionTable_Insert(positionTable);

        //                }
        //            }
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        json = "{\"Status\":\"-1\"}";
        //    }
        //    return Json(json, JsonRequestBehavior.AllowGet);
        //}
        //#endregion

        #region GroupEmails Get
        public ActionResult GroupEmails(int? Id = 0)
        {
            ManageGroupEmailModel model = new ManageGroupEmailModel();
            Models.GroupEmailMasterModel grp = new Models.GroupEmailMasterModel();
            model.groupEmailMasterModel = grp;
            //  Models.GroupEmailsModel grpEmails = new Models.GroupEmailsModel();            
            // model.groupEmailModel = grpEmails;
            GroupEmailMasterBase groupBase = new GroupEmailMasterBase();
            groupBase.Id = Id ?? 0;
            try
            {
                if (groupBase.Id > 0)
                {
                    actionResult = adminAction.EmailGroup_LoadbyId(groupBase);
                    if (actionResult.IsSuccess)
                    {
                        DataRow dr = actionResult.dtResult.Rows[0];
                        grp.Id = dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0;
                        grp.GroupName = dr["GroupName"] != DBNull.Value ? dr["GroupName"].ToString() : "";
                        grp.UserId = dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0;
                        grp.CreatedOn = dr["CreatedOn"] != DBNull.Value ? Convert.ToDateTime(dr["CreatedOn"]).ToString("MM/dd/yyyy") : "";
                        grp.ModifiedOn = dr["ModifiedOn"] != DBNull.Value ? Convert.ToDateTime(dr["ModifiedOn"]).ToString("MM/dd/yyyy") : "";
                    }
                    model.groupEmailMasterModel = grp;
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return View(model);
        }
        #endregion

        #region GroupEmails Post
        [HttpPost]
        public ActionResult GroupEmails(ManageGroupEmailModel model)
        {
            GroupEmailMasterBase groupBase = new GroupEmailMasterBase();
            string action = string.Empty;
            try
            {
                if (model.groupEmailMasterModel.Id > 0)
                {
                    groupBase.Id = model.groupEmailMasterModel.Id;
                    groupBase.Action = "Edit";
                }
                else
                {
                    groupBase.Action = "Add";
                }
                groupBase.GroupName = "GRP_" + model.groupEmailMasterModel.GroupName;
                groupBase.UserId = Session["UserId"] != null ? Convert.ToInt32(Session["UserId"]) : 0;
                actionResult = adminAction.InsertUpdateDeleteEmailGroup_byUserId(groupBase);
                if (actionResult.IsSuccess)
                {
                    TempData["SuccessMessage"] = "Email group has been " + (groupBase.Action == "Add" ? "created" : "updated") + " successfully.";
                }
                else
                    TempData["ErrorMessage"] = "Error in " + (groupBase.Action == "Add" ? "creating" : "updating") + " Email group.";
            }
            catch (Exception ex)
            {
                TempData["ErrorMessage"] = "Error in " + (groupBase.Action == "Add" ? "creating" : "updating") + " Email group.";
                ErrorReporting.WebApplicationError(ex);
            }
            return RedirectToAction("GroupEmailList");
        }
        #endregion

        #region GroupEmailList Get
        public ActionResult GroupEmailList()
        {
            ManageGroupEmailModel model = new ManageGroupEmailModel();
            List<GroupEmailMasterModel> emailGroupList = new List<GroupEmailMasterModel>();
            GroupEmailMasterBase groupBase = new GroupEmailMasterBase();
            string action = string.Empty;
            try
            {
                groupBase.UserId = Session["UserId"] != null ? Convert.ToInt32(Session["UserId"]) : 0;
                actionResult = adminAction.EmailGroupList_byUserId(groupBase);
                if (actionResult.IsSuccess)
                {
                    foreach (DataRow dr in actionResult.dtResult.Rows)
                    {
                        emailGroupList.Add(new Models.GroupEmailMasterModel
                        {
                            Id = dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0,
                            GroupName = dr["GroupName"] != DBNull.Value ? dr["GroupName"].ToString() : "",
                            UserId = dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0,
                            CreatedOn = dr["CreatedOn"] != DBNull.Value ? Convert.ToDateTime(dr["CreatedOn"]).ToString("MM/dd/yyyy") : "N/A",
                            ModifiedOn = dr["ModifiedOn"] != DBNull.Value ? Convert.ToDateTime(dr["ModifiedOn"]).ToString("MM/dd/yyyy") : "N/A"
                        });
                    }
                    model.EmailGroupsList = emailGroupList;
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return View(model);
        }
        #endregion

        #region DeleteGroupEmails Get
        public ActionResult DeleteGroupEmails(int? Id = 0)
        {
            GroupEmailMasterBase groupBase = new GroupEmailMasterBase();
            groupBase.Action = "Delete";
            groupBase.Id = Id ?? 0;
            try
            {
                actionResult = adminAction.InsertUpdateDeleteEmailGroup_byUserId(groupBase);
                if (actionResult.IsSuccess)
                {
                    TempData["SuccessMessage"] = "Email group has been deleted successfully.";
                }
                else
                    TempData["ErrorMessage"] = "Error in deleting Email group.";
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return RedirectToAction("GroupEmailList");
        }
        #endregion

        #region SaveEmailGrp
        public ActionResult SaveEmailGrp(FormCollection fc)
        {
            string AllEmails = fc["hdnAllEmails"] != null ? fc["hdnAllEmails"].ToString() : "";
            int GroupId = fc["hdnEmailGroupId"] != null ? Convert.ToInt32(fc["hdnEmailGroupId"]) : 0;
            string EmailGroupName = fc["hdnEmailGroupName"] != null ? fc["hdnEmailGroupName"].ToString() : "";
            string Emails = string.Empty;
            string EmailXml = string.Empty;
            GroupEmailsBase emailBase = new GroupEmailsBase();
            try
            {
                if (!String.IsNullOrEmpty(AllEmails))
                {
                    string[] splitAllEmails = AllEmails.TrimEnd(',').Split(',');
                    if (splitAllEmails != null && splitAllEmails.Length > 0)
                    {
                        foreach (var item in splitAllEmails)
                        {
                            string[] email = item.Split('-');
                            Emails += "<EmailDesc><Id>" + email[0] + "</Id><Email>" + email[1] + "</Email></EmailDesc>";
                        }
                    }
                    EmailXml = "<Groups>" + Emails + "</Groups>";
                    emailBase.GroupId = GroupId;
                    emailBase.Xml = EmailXml;
                    actionResult = adminAction.InsertUpdateGroupEmails_byGroupId(emailBase);
                    if (actionResult.IsSuccess)
                    {
                        TempData["SuccessMessage"] = "Emails has been saved successfully for the group " + EmailGroupName + ".";
                    }
                    else
                        TempData["ErrorMessage"] = "Error in saving the emails for the group " + EmailGroupName + ".";
                }
            }
            catch (Exception ex)
            {
                TempData["ErrorMessage"] = "Error in saving the emails for the group " + EmailGroupName + ".";
                ErrorReporting.WebApplicationError(ex);
            }
            return RedirectToAction("GroupEmailList");
        }
        #endregion

        #region EmailsForGroup
        public JsonResult EmailsForGroup(int? Id = 0)
        {
            GroupEmailsBase emailBase = new GroupEmailsBase();
            string json = string.Empty;
            try
            {
                emailBase.GroupId = Id ?? 0;
                actionResult = adminAction.EmailsForGroup_LoadbyGroupId(emailBase);
                if (actionResult.IsSuccess)
                {
                    foreach (DataRow dr in actionResult.dtResult.Rows)
                    {
                        json += "{\"Id\":\"" + dr["Id"] + "\",\"GroupId\":\"" + dr["GroupId"] + "\",\"Email\":\"" + dr["Email"] + "\"},";
                    }
                    json = "[" + json.TrimEnd(',') + "]";
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return Json(json, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region GroupEmailJsonList
        public JsonResult GroupEmailJsonList()
        {
            List<GroupEmailMasterModel> emailGroupList = new List<GroupEmailMasterModel>();
            GroupEmailMasterBase groupBase = new GroupEmailMasterBase();
            string json = string.Empty;
            try
            {
                groupBase.UserId = Session["UserId"] != null ? Convert.ToInt32(Session["UserId"]) : 0;
                actionResult = adminAction.EmailGroupList_byUserId(groupBase);
                if (actionResult.IsSuccess)
                {
                    foreach (DataRow dr in actionResult.dtResult.Rows)
                    {
                        json += "{\"Id\":\"" + dr["Id"] + "\",\"GroupName\":\"" + dr["GroupName"] + "\"},";
                    }
                    json = "[" + json.TrimEnd(',') + "]";
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return Json(json, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region UpdateProjectDisplayOrder
        [HttpPost]
        public ActionResult UpdateProjectDisplayOrder(string SurveyDispOrder)
        {
            string json = string.Empty;
            string Pos = string.Empty;
            try
            {
                if (!String.IsNullOrEmpty(SurveyDispOrder))
                {
                    string[] allSurveyOrder = SurveyDispOrder.TrimEnd(',').Split(',');
                    if (allSurveyOrder != null && allSurveyOrder.Length > 0)
                    {
                        for (int i = 0; i <= allSurveyOrder.Length - 1; i++)
                        {
                            string[] order = allSurveyOrder[i].Split('-');
                            if (order != null && order.Length > 0)
                            {
                                Pos += "<ManageSurveyOrder><SurveyId>" + order[0] + "</SurveyId><DisplayOrder>" + order[1] + "</DisplayOrder></ManageSurveyOrder>";
                            }
                        }
                    }
                }
                else
                {
                    Pos = "<ManageSurveyOrder><SurveyId>0</SurveyId><DisplayOrder>''</DisplayOrder></ManageSurveyOrder>";
                }
                string OrderXml = "<Surveys>" + Pos + "</Surveys>";
                projectBase.DisplayOrderXml = OrderXml;
                actionResult = adminAction.Projects_UpdateDisplayOrder(projectBase);
                if (actionResult.IsSuccess)
                {
                    json = "{\"Status\":\"1\",\"Message\":\"Subsurvey Order has been updated successfully.\"}";
                }
                else
                {
                    json = "{\"Status\":\"-1\",\"Message\":\"Error in updating Subsurvey Order.\"}";
                }
            }
            catch (Exception ex)
            {
                json = "{\"Status\":\"-1\",\"Message\":\"Error in updating Subsurvey Order.\"}";
                ErrorReporting.WebApplicationError(ex);
            }
            return Json(json, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region UpdateProjectProductDisplayOrder
        [HttpPost]
        public ActionResult UpdateProjectProductDisplayOrder(string ProductDispOrder, int? ProjectId = 0)
        {
            string json = string.Empty;
            string Pos = string.Empty;
            try
            {
                if (!String.IsNullOrEmpty(ProductDispOrder))
                {
                    string[] allSurveyProdOrder = ProductDispOrder.TrimEnd(',').Split(',');
                    if (allSurveyProdOrder != null && allSurveyProdOrder.Length > 0)
                    {
                        for (int i = 0; i <= allSurveyProdOrder.Length - 1; i++)
                        {
                            string[] order = allSurveyProdOrder[i].Split('-');
                            if (order != null && order.Length > 0)
                            {
                                Pos += "<ManageProductOrder><Id>" + order[0] + "</Id><ProjectId>" + ProjectId + "</ProjectId><DisplayOrder>" + order[1] + "</DisplayOrder></ManageProductOrder>";
                            }
                        }
                    }
                }
                else
                {
                    Pos = "<ManageProductOrder><Id>0</Id><ProjectId>0</ProjectId><DisplayOrder>''</DisplayOrder></ManageProductOrder>";
                }
                string OrderXml = "<Surveys>" + Pos + "</Surveys>";
                projectBase.DisplayOrderXml = OrderXml;
                actionResult = adminAction.ProductProject_UpdateDisplayOrder(projectBase);
                if (actionResult.IsSuccess)
                {
                    json = "{\"Status\":\"1\",\"Message\":\"Survey Product Order has been updated successfully.\"}";
                }
                else
                {
                    json = "{\"Status\":\"-1\",\"Message\":\"Error in updating Survey Product Order.\"}";
                }
            }
            catch (Exception ex)
            {
                json = "{\"Status\":\"-1\",\"Message\":\"Error in updating Survey Product Order.\"}";
                ErrorReporting.WebApplicationError(ex);
            }
            return Json(json, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region GetSuperSurveyPreviewData
        public string GetSuperSurveyPreviewData(int? id = 0)
        {
            //---------List of sub survey
            List<CreateProjectModel> lstCreateProjectModel = new List<CreateProjectModel>();
            int UserId = Convert.ToInt32(Session["UserId"]);
            string SubSurveyIds = string.Empty;
            adminBase.Id = UserId;
            actionResult = adminAction.Poject_LoadAll(adminBase);
            if (actionResult.dsResult != null && actionResult.dsResult.Tables[0].Rows.Count > 0)
            {
                DataTable dtResult = actionResult.dsResult.Tables[0].Select("SuperProjectId is Not Null and SuperProjectId = '" + Convert.ToInt32(id) + "'").CopyToDataTable();
                if (dtResult != null && dtResult.Rows.Count > 0)
                {
                    foreach (DataRow dr in dtResult.Rows)
                    {
                        if (dr["SuperProjectId"] != DBNull.Value && Convert.ToInt32(dr["SuperProjectId"]) != 0)
                        {
                            lstCreateProjectModel.Add(new CreateProjectModel
                            {
                                Id = dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0,
                                ProjectName = dr["ProjectName"] != DBNull.Value ? Convert.ToString(dr["ProjectName"]) : "",
                                ProjectQuota = dr["ProjectQuota"] != DBNull.Value ? Convert.ToInt32(dr["ProjectQuota"]) : 0,
                                SuperProjectId = dr["SuperProjectId"] != DBNull.Value ? Convert.ToInt32(dr["SuperProjectId"]) : 0,
                                ProjectType = dr["ProjectType"] != DBNull.Value ? Convert.ToString(dr["ProjectType"]) : "",
                                ProjectGuid = dr["ProjectGuid"] != DBNull.Value ? Convert.ToString(dr["ProjectGuid"]) : "",
                                ProductsCount = dr["ProductsCount"] != DBNull.Value ? Convert.ToInt32(dr["ProductsCount"]) : 0,
                                DisplayOrder = dr["DisplayOrder"] != DBNull.Value ? Convert.ToInt32(dr["DisplayOrder"]) : 0
                            });
                        }
                    }
                    lstCreateProjectModel = lstCreateProjectModel.OrderBy(l => l.DisplayOrder).ToList();
                }
                foreach (var projId in lstCreateProjectModel)
                {
                    List<ProductProjectModel> lstProductProjectModel = new List<ProductProjectModel>();
                    productProjectBase.ProjectId = Convert.ToInt32(projId.Id);
                    SRV.BaseLayer.ActionResult actionResultProducts = new SRV.BaseLayer.ActionResult();
                    actionResultProducts = adminAction.ProductProject_LoadById(productProjectBase);
                    if (actionResultProducts.IsSuccess)
                    {
                        foreach (DataRow drProd in actionResultProducts.dtResult.Rows)
                        {
                            lstProductProjectModel.Add(new ProductProjectModel
                            {
                                Id = drProd["Id"] != DBNull.Value ? Convert.ToInt32(drProd["Id"]) : 0,
                                ProjectId = drProd["ProjectId"] != DBNull.Value ? Convert.ToInt32(drProd["ProjectId"]) : 0,
                                DisplayOrder = drProd["DisplayOrder"] != DBNull.Value ? Convert.ToInt32(drProd["DisplayOrder"]) : 0,
                                Description = drProd["Description"] != DBNull.Value ? Convert.ToString(drProd["Description"]) : string.Empty,
                                ProductLogicalId = drProd["ProductLogicalId"] != DBNull.Value ? Convert.ToInt32(drProd["ProductLogicalId"]) : 0,
                            });
                        }
                        lstProductProjectModel = lstProductProjectModel.OrderBy(l => l.DisplayOrder).ToList();
                    }
                    if (lstProductProjectModel.Count > 0)
                    {
                        foreach (var item in lstProductProjectModel)
                        {
                            SubSurveyIds += "" + Convert.ToString(projId.Id) + "-" + Convert.ToString(item.ProductLogicalId) + " " + item.Description + ",";
                        }
                    }
                    else
                    {
                        SubSurveyIds += "" + Convert.ToString(projId.Id) + "-" + "" + ",";
                    }
                }
            }
            return SubSurveyIds.Trim(',');
        }
        #endregion

        #region ProductMessageLayout
        public ActionResult ProductMessageLayout(int? ProjectId = 0)
        {
            ProductProjectModel productProjectModel = new ProductProjectModel();
            List<SelectListItem> lstProductProjectModel = new List<SelectListItem>();
            productProjectBase.ProjectId = Convert.ToInt32(ProjectId);
            ViewBag.ProjectId = Convert.ToInt32(ProjectId);
            actionResult = adminAction.ProductProject_LoadById(productProjectBase);
            if (actionResult.IsSuccess)
            {
                foreach (DataRow dr in actionResult.dtResult.Rows)
                {
                    lstProductProjectModel.Add(new SelectListItem { Text = dr["Description"].ToString(), Value = dr["Id"].ToString() });
                }
            }
            ViewBag.ProductList = lstProductProjectModel;
            return View();
        }
        #endregion

        #region ProductMessageLayout Post
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult ProductMessageLayout(ProductProjectModel model, FormCollection fc)
        {
            ProductProjectBase productBase = new ProductProjectBase();
            int projectId = fc["hdnProjectId"] != null ? Convert.ToInt32(fc["hdnProjectId"]) : 0;
            productBase.Id = model.Id;
            productBase.ProductMessage = model.ProductMessage;
            actionResult = adminAction.ProductProject_UpdateMessageById(productBase);
            if (actionResult.IsSuccess)
                TempData["SuccessMessage"] = "Product message updated successfully";
            else
                TempData["ErrorMessage"] = "Error in updating product message";
            return RedirectToAction("ProductMessageLayout", new { ProjectId = projectId });
        }
        #endregion

        #region GetProductMessage
        [HttpGet]
        public ActionResult GetProductMessage(int? ProductId = 0, int? projectId = 0)
        {
            string json = string.Empty;
            ProductProjectModel model = new ProductProjectModel();
            List<SelectListItem> lstProductProjectModel = new List<SelectListItem>();
            productProjectBase.Id = Convert.ToInt32(ProductId);
            actionResult = adminAction.Product_LoadById(productProjectBase);
            if (actionResult.IsSuccess)
            {
                DataRow dr = actionResult.dtResult.Rows[0];
                model.Id = dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0;
                model.ProductMessage = dr["ProductMessage"] != DBNull.Value ? Convert.ToString(dr["ProductMessage"]) : "";
            }
            productProjectBase.ProjectId = Convert.ToInt32(projectId);
            ViewBag.ProjectId = Convert.ToInt32(projectId);
            actionResult = adminAction.ProductProject_LoadById(productProjectBase);
            if (actionResult.IsSuccess)
            {
                foreach (DataRow dr in actionResult.dtResult.Rows)
                {
                    lstProductProjectModel.Add(new SelectListItem { Text = dr["Description"].ToString(), Value = dr["Id"].ToString() });
                }
            }
            ViewBag.ProductList = lstProductProjectModel;
            return View("ProductMessageLayout", model);
        }
        #endregion

        #region DeleteAllOfflineResponses
        public ActionResult DeleteAllOfflineResponses(int? ProjectId = 0)
        {
            projectBase.Id = Convert.ToInt32(ProjectId);
            actionResult = adminAction.Responders_Offline_DeleteBy_ProjectId(projectBase);
            if (actionResult.IsSuccess)
                TempData["SuccessMessage"] = "All responses deleted successfully";
            else
                TempData["ErrorMessage"] = "Error occurred in deleteing responses";
            return RedirectToAction("SurveyOfflineResponses", new { id = Convert.ToInt32(ProjectId) });
        }
        #endregion

        #region DeleteAllOnlineResponses
        public ActionResult DeleteAllOnlineResponses(int? ProjectId = 0)
        {
            projectBase.Id = Convert.ToInt32(ProjectId);
            actionResult = adminAction.Response_DeleteBy_ProjectId(projectBase);
            if (actionResult.IsSuccess)
                TempData["SuccessMessage"] = "All responses deleted successfully";
            else
                TempData["ErrorMessage"] = "Error occurred in deleteing responses";
            return RedirectToAction("SurveyResponses", new { id = Convert.ToInt32(ProjectId) });
        }
        #endregion

        #region CreateUserDirectory
        [HttpPost]
        public ActionResult CreateUserDirectory(string dirName, int? id = 0)
        {
            string json = string.Empty;
            userDirectoriesBase.Id = Convert.ToInt32(id);
            userDirectoriesBase.DirName = dirName;
            userDirectoriesBase.UserId = Convert.ToInt32(Session["UserId"]);
            actionResult = adminAction.UserDirectories_InsertUpdate(userDirectoriesBase);
            if (actionResult.IsSuccess)
                json = "{\"Status\":\"1\"}";
            else
                json = "{\"Status\":\"-1\"}";
            return Json(json, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region DeleteDirectory
        public ActionResult DeleteDirectory(int? id = 0)
        {
            string json = string.Empty;
            userDirectoriesBase.Id = Convert.ToInt32(id);
            actionResult = adminAction.UserDirectories_DeleteByUserId(userDirectoriesBase);
            if (actionResult.IsSuccess)
                json = "{\"Status\":\"1\"}";
            else
                json = "{\"Status\":\"-1\"}";
            return Json(json, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region ProjectDirectories_InsertUpdate
        [ValidateInput(false)]
        public ActionResult ProjectDirectories_InsertUpdate(string directoryXml)
        {
            string json = string.Empty;
            projectDirectoriesBase.Xml = directoryXml;
            actionResult = adminAction.ProjectDirectories_IU(projectDirectoriesBase);
            if (actionResult.IsSuccess)
                json = "{\"Status\":\"1\"}";
            else
                json = "{\"Status\":\"-1\"}";
            return Json(json, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region UserDirectoryList
        public ActionResult UserDirectoryList()
        {
            string json = string.Empty;
            List<UserDirectoriesModel> lstUserDirectoryModel = new List<UserDirectoriesModel>();
            //List<ProjectDirectoriesModel> lstprojectDirectoryModel = new List<ProjectDirectoriesModel>();
            userDirectoriesBase.UserId = Convert.ToInt32(Session["UserId"]);
            actionResult = adminAction.UserDirectories_LoadByUserId(userDirectoriesBase);
            if (actionResult.IsSuccess)
            {
                lstUserDirectoryModel = Helper.CommHelper.ConvertTo<UserDirectoriesModel>(actionResult.dtResult);
                foreach (var item in lstUserDirectoryModel)
                {
                    List<ProjectDirectoriesModel> lstprojectDirectoryModel = new List<ProjectDirectoriesModel>();
                    projectDirectoriesBase.DirectoryId = Convert.ToInt32(item.Id);
                    actionResult = adminAction.ProjectDirectories_LoadById(projectDirectoriesBase);
                    if (actionResult.IsSuccess)
                        lstprojectDirectoryModel = Helper.CommHelper.ConvertTo<ProjectDirectoriesModel>(actionResult.dtResult);
                    item.lstProjectDirectoryModel = lstprojectDirectoryModel;
                    item.AssignCount = lstprojectDirectoryModel.Count;
                }
            }
            string ssd = Newtonsoft.Json.JsonConvert.SerializeObject(lstUserDirectoryModel);
            return Json(ssd, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region ProjectDirectories_LoadAll
        public ActionResult ProjectDirectories_LoadAll(int? dir = 0)
        {
            string json = string.Empty;
            ManageProjectModel model = new ManageProjectModel();
            List<CreateProjectModel> lstProjectModel = new List<CreateProjectModel>();
            List<SuperProjectModel> lstsuperProjectModel = new List<SuperProjectModel>();
            model.lstProjectModel = lstProjectModel;
            model.lstSuperProjectModel = lstsuperProjectModel;
            projectBase.OwnerId = Convert.ToInt32(Session["UserId"]);
            actionResult = adminAction.ProjectDirectories_LoadAll(projectBase);
            if (actionResult.IsSuccess)
            {
                if (actionResult.dsResult.Tables[0] != null && actionResult.dsResult.Tables[0].Rows.Count > 0)
                {
                    lstProjectModel = Helper.CommHelper.ConvertTo<CreateProjectModel>(actionResult.dsResult.Tables[0]);
                    lstProjectModel = lstProjectModel.Where(l => l.SuperProjectId == 0).ToList();
                }
                if (actionResult.dsResult.Tables[1] != null && actionResult.dsResult.Tables[1].Rows.Count > 0)
                    lstsuperProjectModel = Helper.CommHelper.ConvertTo<SuperProjectModel>(actionResult.dsResult.Tables[1]);
                if (dir > 0)
                {
                    lstProjectModel = lstProjectModel.Where(l => l.DirectoryId == dir).ToList();
                    lstsuperProjectModel = lstsuperProjectModel.Where(l => l.DirectoryId == dir).ToList();
                }
            }
            model.lstProjectModel = lstProjectModel;
            model.lstSuperProjectModel = lstsuperProjectModel;
            return Json(Newtonsoft.Json.JsonConvert.SerializeObject(model), JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region AddUrlParameter
        [HttpPost]
        public ActionResult AddUrlParameter(string parameterName)
        {
            string json = string.Empty;
            try
            {
                urlParameterBase.ParameterName = parameterName;
                actionResult = urlParameterAction.UrlParameter_Insert(urlParameterBase);
                if (actionResult.IsSuccess)
                    json = "{\"Status\":\"1\",\"InsertedId\":\"" + actionResult.dtResult.Rows[0][0].ToString() + "\"}";
                else
                    json = "{\"Status\":\"-1\"}";
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return Json(json, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region GetUrlParameters
        public ActionResult GetUrlParameters()
        {
            string json = string.Empty;
            try
            {
                actionResult = urlParameterAction.UrlParameter_LoadAll();
                if (actionResult.IsSuccess)
                    json = "{\"Status\":\"1\",\"data\":" + Newtonsoft.Json.JsonConvert.SerializeObject(actionResult.dtResult) + "}";
                else
                    json = "{\"Status\":\"-1\"}";
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return Json(json, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region DeleteUrlParameter
        [HttpPost]
        public ActionResult DeleteUrlParameter(int Id)
        {
            string json = string.Empty;
            try
            {
                urlParameterBase.Id = Id;
                actionResult = urlParameterAction.ProjectDelete_ById(urlParameterBase);
                if (actionResult.IsSuccess)
                    json = "{\"Status\":\"1\"}";
                else
                    json = "{\"Status\":\"-1\"}";
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return Json(json, JsonRequestBehavior.AllowGet);
        }
        #endregion



        #region AssignSurveyToPanel
        [HttpGet]
        public ActionResult AssignSurveyToPanel(int? Id = 0, string type = null)
        {
            ViewBag.Type = type;
            ManageAdminModel model = new ManageAdminModel();
            ViewBag.ProjectId = Convert.ToInt32(Id);
            ProjectBase projectBase = new ProjectBase();
            projectBase.Id = Convert.ToInt32(Id);
            string languageId = "";
            actionResult = adminAction.Project_LoadById(projectBase);
            if (actionResult.IsSuccess)
            {
                DataRow dr = actionResult.dtResult.Rows[0];
                languageId = dr["LanguageId"] != null ? dr["LanguageId"].ToString() : "";

            }
            ViewBag.Language = languageId;
            List<AdminModel> lstAdminModel = new List<AdminModel>();
            model.Adminlist = lstAdminModel;
            adminBase.RoleId = 4;
            actionResult = adminAction.PanelList_LoadAll(adminBase);
            if (actionResult.IsSuccess)
                lstAdminModel = Helper.CommHelper.ConvertTo<AdminModel>(actionResult.dtResult);
            model.Adminlist = lstAdminModel;

            if (type != null)
            {
                TempData["SubSurveyIds"] = GetSuperSurveyPreviewData(Convert.ToInt32(Id));
            }
            return View(model);

        }
        #endregion

        #region AssignSurveyToPanel
        [HttpPost]
        public ActionResult AssignSurveyToPanel(FormCollection fc, int? prjId = 0)
        {
            string PanelIds = fc["hdnPanelIds"] ?? string.Empty;
            string addEditCase = fc["hdnAddEdit"] ?? string.Empty;
            int ProjectId = fc["hdnProjectId"] != null ? Convert.ToInt32(fc["hdnProjectId"]) : 0;
            string type = fc["Type"];
            try
            {
                string UserId = String.Format("{0}{1}{2}", "<UserId>", Convert.ToInt32(Session["UserId"]), "</UserId>"); ;
                string projId = String.Format("{0}{1}{2}", "<ProjectId>", ProjectId, "</ProjectId>");
                string projectInPan = string.Empty;

                if (addEditCase == "Add")
                {
                    if (!String.IsNullOrEmpty(PanelIds))
                    {
                        string[] panArray = PanelIds.TrimEnd(',').Split(',');
                        if (panArray != null && panArray.Length > 0)
                        {
                            for (int i = 0; i <= panArray.Length - 1; i++)
                            {
                                if (!string.IsNullOrEmpty(type))
                                {
                                    projectInPan += String.Format("{0}{1}{2}{3}{4}{5}", "<ProjectInPanel><Id>0</Id>", UserId, "<ProjectId>" + prjId + "</ProjectId>", "<PanelId>", panArray[i], "</PanelId></ProjectInPanel>");
                                }
                                else
                                {
                                    projectInPan += String.Format("{0}{1}{2}{3}{4}{5}", "<ProjectInPanel><Id>0</Id>", UserId, projId, "<PanelId>", panArray[i], "</PanelId></ProjectInPanel>");
                                }

                            }
                        }
                        string xml = String.Format("{0}{1}{2}", "<Panels>", projectInPan, "</Panels>");
                        projectBase.PanelXml = xml;
                        if (!string.IsNullOrEmpty(type))
                        {
                            projectBase.SuperProjectId = ProjectId;
                        }
                        actionResult = adminAction.PanelProjects_InsertUpdate(projectBase);
                        if (actionResult.IsSuccess)
                        {
                            int res = Convert.ToInt32(actionResult.dtResult.Rows[0][0]);
                            if (res > 0)
                                TempData["SuccessMessage"] = String.Format("{0}", "panel assigned successfully.");
                            else
                                TempData["ErrorMessage"] = String.Format("{0}", "Error in assigning panel.");
                        }
                    }
                    else
                    {
                        TempData["ErrorMessage"] = String.Format("{0}{1}{2}", "Error in assigning panel.Please assign atleast one panel.");
                    }
                }
                else if (addEditCase == "Edit")
                {
                    ViewBag.Title = "Update Role";
                    string Ids = string.Empty;
                    if (!String.IsNullOrEmpty(PanelIds))
                    {
                        string[] rolesArray = PanelIds.TrimEnd(',').Split(',');
                        if (rolesArray != null && rolesArray.Length > 0)
                        {
                            for (int i = 0; i <= rolesArray.Length - 1; i++)
                            {
                                string[] seprateRoleIds = rolesArray[i].Split('-');
                                if (seprateRoleIds != null && seprateRoleIds.Length > 0)
                                {
                                    if (string.IsNullOrEmpty(type))
                                    {
                                        projectInPan += String.Format("{0}{1}{2}{3}{4}{5}{6}{7}", "<ProjectInPanel><Id>", seprateRoleIds[1], "</Id>", UserId, projId, "<PanelId>", seprateRoleIds[0], "</PanelId></ProjectInPanel>");
                                    }
                                    else
                                    {
                                        projectInPan += String.Format("{0}{1}{2}{3}{4}{5}{6}{7}", "<ProjectInPanel><Id>", seprateRoleIds[1], "</Id>", UserId, "<ProjectId>" + prjId + "</ProjectId>", "<PanelId>", seprateRoleIds[0], "</PanelId></ProjectInPanel>");
                                    }
                                }
                            }
                        }

                        string xml = String.Format("{0}{1}{2}", "<Panels>", projectInPan, "</Panels>");
                        projectBase.PanelXml = xml;
                        if (!string.IsNullOrEmpty(type))
                        {
                            projectBase.SuperProjectId = ProjectId;
                        }
                        actionResult = adminAction.PanelProjects_InsertUpdate(projectBase);
                        if (actionResult.IsSuccess)
                            TempData["SuccessMessage"] = String.Format("{0}", "panel updated successfully.");
                        else
                            TempData["ErrorMessage"] = String.Format("{0}", "Error in updating panel.");

                    }
                    else
                    {
                        TempData["ErrorMessage"] = String.Format("{0}", "Error in updating panel.Please assign atleast one panel.");
                    }
                }

            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return RedirectToAction("AssignSurveyToPanel", new { id = ProjectId, type = type });
        }
        #endregion

        #region ExistingPanel Json
        public JsonResult ExistingPanel(int ProjectId, string type = null)
        {
            string json = string.Empty;
            try
            {
                projectBase.Id = ProjectId;
                projectBase.SuperProjectId = (!string.IsNullOrEmpty(type) ? ProjectId : 0);
                actionResult = adminAction.PanelProjects_LoadByProjectId(projectBase);
                if (actionResult.IsSuccess)
                {

                    foreach (DataRow dr in actionResult.dtResult.Rows)
                    {
                        json += "{\"PanelId\":\"" + dr["PanelId"] + "\",\"Id\":\"" + dr["Id"] + "\"}" + ",";
                    }
                    json = "[" + json.TrimEnd(',') + "]";
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return Json(json, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region UnassignedProjectFromDirectory
        public ActionResult UnassignedProjectFromDirectory(string UnAssignedIds)
        {
            string json = string.Empty;
            try
            {
                if (UnAssignedIds != "")
                {
                    projectDirectoriesBase.DirectoryIdList = UnAssignedIds.TrimEnd(',');
                    actionResult = adminAction.ProjectDirectories_DeleteById(projectDirectoriesBase);
                    if (actionResult.IsSuccess)
                        json = "{\"Status\":\"1\"}";
                    else
                        json = "{\"Status\":\"-1\"}";
                }
                else
                    json = "{\"Status\":\"-1\"}";
            }
            catch (Exception ex)
            { }
            return Json(json, JsonRequestBehavior.AllowGet);
        }
        #endregion

        public ActionResult abcd(int projId, string ReportType)
        {
            ViewBag.ProjectId = projId;
            return View();
        }

        public ActionResult Reports(int projId, string ReportType)
        {
            ViewBag.ProjectId = projId;
            ViewBag.ReportType = ReportType;
            return View();
        }

        #region RecoverDeletedProject Get
        [HttpGet]
        public ActionResult RecoverDeletedProject()
        {
            CreateProjectModel model = new CreateProjectModel();
            List<CreateProjectModel> ProjectLst = new List<CreateProjectModel>();
            model.ProjectList = ProjectLst;
            try
            {
                int UserId = Convert.ToInt32(Session["UserId"]);
                if (UserId == 0) return View(model);

                adminBase.Id = UserId;
                SRV.BaseLayer.ActionResult actionResultProjects = new SRV.BaseLayer.ActionResult();
                actionResultProjects = adminAction.DeletePoject_LoadAll(adminBase);

                if (actionResultProjects.IsSuccess)
                {
                    foreach (DataRow dr in actionResultProjects.dtResult.Rows)
                    {
                        model = new CreateProjectModel();
                        model.Id = dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0;
                        model.ProjectName = dr["ProjectName"] != DBNull.Value ? Convert.ToString(dr["ProjectName"]) : "";
                        model.SuperProjectId = dr["SuperProjectId"] != DBNull.Value ? Convert.ToInt32(dr["SuperProjectId"]) : 0;
                        model.SuperProjectName = dr["SuperProjectName"] != DBNull.Value ? Convert.ToString(dr["SuperProjectName"]) : "N/A";
                        model.DeleteDate = dr["DeleteDate"] != DBNull.Value ? Convert.ToDateTime(dr["DeleteDate"]).ToString("MM/dd/yyyy") : "N/A";
                        model.CreatedByName = dr["CreatedBy"] != DBNull.Value ? dr["CreatedBy"].ToString() : "N/A";
                        model.Owner = dr["Owner"] != DBNull.Value ? dr["Owner"].ToString() : "N/A";
                        ProjectLst.Add(model);
                    }
                    ProjectLst = ProjectLst.OrderBy(l => l.DisplayOrder).ToList();
                    model.ProjectList = ProjectLst;
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return View(model);
        }
        #endregion

        #region Recover Deleted Project Post


        [HttpPost]
        public ActionResult recoverDeletedPro(int? recProId = 0)
        {
            string jsonResult = string.Empty;
            ProjectBase projectBase = new ProjectBase();
            try
            {
                projectBase.Id = Convert.ToInt32(recProId);
                actionResult = adminAction.RecoverDeleteProject_ById(projectBase);
                if (actionResult.IsSuccess)
                {
                    jsonResult = "success";
                }
                else {
                    jsonResult = "error";
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return Json(jsonResult, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Recover Deleted Project Post

        [HttpPost]
        public JsonResult SurveyNameCheck(string surveyName = null, int proId = 0)
        {
            string jsonResult = string.Empty;
            ProjectBase projectBase = new ProjectBase();
            try
            {
                projectBase.SuperProjectId = proId;
                projectBase.ProjectName = surveyName;
                actionResult = adminAction.SurveyNameCheck(projectBase);
                if (actionResult.IsSuccess)
                {
                    DataRow dr = actionResult.dtResult.Rows[0];
                    jsonResult = dr["Result"] != DBNull.Value ? dr["Result"].ToString() : "0";
                }
                else {
                    jsonResult = "error";
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return Json(jsonResult, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region SurveyResponses
        public ActionResult SurveyIncompleteResponses(int id)
        {
            ViewBag.ProjectId = id;

            ManageRespondersModel model = new ManageRespondersModel();
            List<RespondersModel> lstResponders = new List<RespondersModel>();
            model.lstRespondersModel = lstResponders;
            try
            {
                projectBase.Id = id;
                actionResult = adminAction.Project_LoadById(projectBase);
                if (actionResult.IsSuccess)
                {
                    DataRow dr = actionResult.dtResult.Rows[0];
                    projectBase.SuperProjectId = dr["SuperProjectId"] != DBNull.Value ? Convert.ToInt32(dr["SuperProjectId"]) : 0;
                }
                actionResult = adminAction.IncompleteRespondersLoad_ByProjectId(projectBase);
                if (actionResult.IsSuccess)
                {

                    foreach (DataRow dr in actionResult.dtResult.Rows)
                    {
                        model.lstRespondersModel.Add(new RespondersModel
                        {
                            Id = 0,
                            ProjectGuid = dr["ProjectGUID"] != DBNull.Value ? Convert.ToString(dr["ProjectGUID"]) : "",
                            InvitationGuid = dr["InvitationGuid"] != DBNull.Value ? Convert.ToString(dr["InvitationGuid"]) : "",
                            Email = dr["Email"] != DBNull.Value ? Convert.ToString(dr["Email"]) : "",
                            CreatedOn = dr["CreatedOn"] != DBNull.Value ? DateTime.Parse(dr["CreatedOn"].ToString(), System.Globalization.CultureInfo.InvariantCulture).ToString() : "N/A",
                            // ModifiedOn = dr["ModifiedOn"] != DBNull.Value ? DateTime.Parse(dr["ModifiedOn"].ToString(), System.Globalization.CultureInfo.InvariantCulture).ToString("MM/dd/yyyy") : "N/A",
                            SuperProjectId = dr["SuperProjectId"] != DBNull.Value ? Convert.ToInt32(dr["SuperProjectId"]) : 0,
                            SuperProjectName = dr["SuperProjectName"] != DBNull.Value ? Convert.ToString(dr["SuperProjectName"]) : "",
                            Source = dr["Source"] != DBNull.Value ? Convert.ToString(dr["Source"]) : "",
                        });
                    }
                    IEnumerable<RespondersModel> filteredList = model.lstRespondersModel
                      .GroupBy(customer => customer.InvitationGuid).Select(group => group.First());

                    model.lstRespondersModel = filteredList.ToList();
                }

            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return View(model);
        }
        #endregion

        [HttpGet]
        public ActionResult NotParticipateRespondent(int? Id = 0)
        {
            CreateProjectModel model = new CreateProjectModel();
            List<CreateProjectModel> ProjectLst = new List<CreateProjectModel>();
            model.ProjectList = ProjectLst;
            try
            {
                int UserId = Convert.ToInt32(Session["UserId"]);
                if (UserId == 0) return View(model);

                adminBase.ProjectId = Convert.ToInt32(Id);
                adminBase.Id = UserId;
                SRV.BaseLayer.ActionResult actionResultProjects = new SRV.BaseLayer.ActionResult();
                actionResultProjects = adminAction.NotParticipateResProject_LoadAll(adminBase);

                if (actionResultProjects.dtResult.Rows != null && actionResultProjects.dtResult.Rows.Count > 0)
                {
                    foreach (DataRow dr in actionResultProjects.dtResult.Rows)
                    {
                        model = new CreateProjectModel();
                        model.Id = dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0;
                        model.ProjectName = dr["ProjectName"] != DBNull.Value ? Convert.ToString(dr["ProjectName"]) : "";
                        model.SuperProjectId = dr["SuperProjectId"] != DBNull.Value ? Convert.ToInt32(dr["SuperProjectId"]) : 0;
                        model.SuperProjectName = dr["SuperProjectName"] != DBNull.Value ? Convert.ToString(dr["SuperProjectName"]) : "N/A";
                        model.CreatedOn = dr["CreatedOn"] != DBNull.Value ? Convert.ToDateTime(dr["CreatedOn"]).ToString("MM/dd/yyyy") : "N/A";
                        model.ModifiedOn = dr["ModifiedOn"] != DBNull.Value ? Convert.ToDateTime(dr["ModifiedOn"]).ToString("MM/dd/yyyy") : "N/A";
                        model.CreatedByName = dr["CreatedBy"] != DBNull.Value ? dr["CreatedBy"].ToString() : "N/A";
                        model.ModifiedByName = dr["ModifiedBy"] != DBNull.Value ? dr["ModifiedBy"].ToString() : "N/A";
                        model.CreatedByName = dr["CreatedBy"] != DBNull.Value ? dr["CreatedBy"].ToString() : "N/A";
                        model.ProjectGuid = dr["ProjectGuid"] != DBNull.Value ? dr["ProjectGuid"].ToString() : "N/A";
                        model.Owner = dr["Owner"] != DBNull.Value ? dr["Owner"].ToString() : "N/A";
                        model.DisplayOrder = dr["DisplayOrder"] != DBNull.Value ? Convert.ToInt32(dr["DisplayOrder"]) : 0;
                        model.ProjectType = dr["ProjectType"] != DBNull.Value ? Convert.ToString(dr["ProjectType"]) : "0";
                        model.DirectoryId = dr["DirectoryId"] != DBNull.Value ? Convert.ToInt32(dr["DirectoryId"]) : 0;
                        model.DirectoryIdCol = dr["DirectoryIdCol"] != DBNull.Value ? Convert.ToInt32(dr["DirectoryIdCol"]) : 0;
                        ProjectLst.Add(model);
                    }
                    ProjectLst = ProjectLst.OrderBy(l => l.DisplayOrder).ToList();
                    model.ProjectList = ProjectLst;
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return View(model);
        }

        public ActionResult NotParticipateRespList(int? proId = 0, int? projectType = 0)
        {
            CreateProjectModel model = new CreateProjectModel();
            try
            {
                if (Session["UserId"] != null)
                {
                    adminBase.ProjectId = Convert.ToInt32(proId);
                    SRV.BaseLayer.ActionResult actionResultProjects = new SRV.BaseLayer.ActionResult();
                    actionResultProjects = adminAction.NotParticipateRes_Bind(adminBase);
                    if (actionResultProjects.IsSuccess)
                    {
                        DataRow dr = actionResultProjects.dtResult.Rows[0];
                        ViewBag.NotParticipateRespondent = dr["NotParticipateRespondent"] != DBNull.Value ? dr["NotParticipateRespondent"].ToString() : "";
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return View(model);
        }

        #region Deleted No Peak

        [HttpPost]
        public ActionResult deletedNoPeak(int? surveyId = 0)
        {
            string jsonResult = string.Empty;
            ProjectBase projectBase = new ProjectBase();
            try
            {
                projectBase.Id = Convert.ToInt32(surveyId);
                actionResult = adminAction.DeleteNoPeak_ById(projectBase);
                if (actionResult.IsSuccess)
                {
                    jsonResult = "success";
                }
                else {
                    jsonResult = "error";
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return Json(jsonResult, JsonRequestBehavior.AllowGet);
        }
        #endregion

        public ActionResult uploadPartial()
        {

            string path = string.Empty;
            path = Server.MapPath("~/Scripts/ckeditor/pictures");
            if (!System.IO.Directory.Exists(path))
            {
                System.IO.Directory.CreateDirectory(path);
            }
            var appData = path;
            var images = Directory.GetFiles(appData).Select(x => new ImagesViewModel
            {
                Url = Url.Content(String.Format("{0}{1}", "~/Scripts/ckeditor/pictures/", Path.GetFileName(x)))
            });
            return View(images);
        }

    }



    public class QuestionList
    {
        public int id { get; set; }
        public int questionTypeId { get; set; }
        public int displayOrder { get; set; }
        public int answerId { get; set; }
        public string questionTitle { get; set; }
        public string answerTitle { get; set; }
    }
}

