﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using DotNetOpenAuth.AspNet;
using Microsoft.Web.WebPages.OAuth;
using WebMatrix.WebData;
using SurveyProject.Filters;
using SurveyProject.Models;
using SRV.BaseLayer.Account;
using SRV.ActionLayer.Account;
using SurveyProject.Helper;
using SRV.ActionLayer.Admin;
using SRV.BaseLayer.Admin;
using System.Data;
using SRV.Utility;
using Newtonsoft.Json;

namespace SurveyProject.Controllers
{
    [UnHandledExceptionFilter]
    public class AccountController : Controller
    {

        #region Declaration
        AccountBase accountBase = new AccountBase();
        SRV.BaseLayer.ActionResult actionResult = new SRV.BaseLayer.ActionResult();
        AccountAction accountAction = new AccountAction();
        AdminAction adminAction = new AdminAction();
        AdminBase adminBase = new AdminBase();
        #endregion

        #region Login Get
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            LoginModel model = new LoginModel();
            Response.Cache.SetExpires(DateTime.UtcNow.AddMinutes(-1));
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetNoStore();
            Response.Cache.AppendCacheExtension("no-cache");
            try
            {

            }
            catch (Exception ex)
            {

            }
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }
        #endregion

        #region Login Post
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginModel model, string returnUrl)
        {
            try
            {
                int result = 0;
                if (ModelState.IsValid)
                {
                    accountBase.Email = model.Email;
                    accountBase.Password = model.Password;
                    actionResult = accountAction.Login_Load(accountBase);
                    if (actionResult.IsSuccess)
                    {
                        DataRow dr = actionResult.dtResult.Rows[0];
                        result = Convert.ToInt32(dr[0]);
                        if (result < 0)
                        {
                            TempData["ErrorMessage"] = "Invalid email or Password.";
                            //ModelState.AddModelError("", "Invalid UserName or Password.");
                        }
                        else if (result == 0)
                        {
                            TempData["ErrorMessage"] = "Your account is not verfied yet,Please verify your account.";
                            //ModelState.AddModelError("", "Your Account is not verfied,Please verify your account.");
                        }
                        else
                        {
                            Session["UserId"] = Convert.ToInt32(dr["UserId"]);
                            Session["RoleId"] = dr["RoleId"] != DBNull.Value ? dr["RoleId"] : "0";
                            Session["UserName"] = dr["FirstName"] != DBNull.Value ? dr["FirstName"] : "";
                            if (Session["RoleId"].ToString() == "1")// Super Admin
                            {
                                //if (Url.IsLocalUrl(returnUrl))return Redirect(returnUrl);
                                return RedirectToAction("Dashboard", "SuperAdmin");
                            }
                            else if (Session["RoleId"].ToString() == "2")// Admin User
                            {
                                //if (Url.IsLocalUrl(returnUrl)) return Redirect(returnUrl);
                                return RedirectToAction("ProjectList", "Admin");
                            }
                            else if (Session["RoleId"].ToString() == "3")// Respondent User
                            {
                                //if (Url.IsLocalUrl(returnUrl)) return Redirect(returnUrl);
                                return RedirectToAction("Dashboard", "Respondent");
                            }
                            else if (Session["RoleId"].ToString() == "4") //Panel User
                            {
                                //if (Url.IsLocalUrl(returnUrl)) return Redirect(returnUrl);
                                return RedirectToAction("Surveys", "Panel");
                            }
                            else if (Session["RoleId"].ToString() == "5") //Field Service
                            {
                                //if (Url.IsLocalUrl(returnUrl)) return Redirect(returnUrl);
                                return RedirectToAction("Dashboard", "FieldService");
                            }
                        }
                    }
                    else
                    {
                        TempData["ErrorMessage"] = "The email or password provided is incorrect.";
                        //ModelState.AddModelError("", "The user name or password provided is incorrect.");
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return View(model);
        }
        #endregion

        #region LogOff
        [HttpGet]
        public ActionResult LogOff()
        {
            //WebSecurity.Logout();
            Session.Clear();
            Session.Abandon();
            return RedirectToAction("Index", "Home");
        }
        #endregion

        #region Register Get
        [AllowAnonymous]
        public ActionResult Register()
        {
            AdminRegisterModel model = new AdminRegisterModel();
            List<SelectListItem> countryLst = new List<SelectListItem>();
            List<SelectListItem> stateLst = new List<SelectListItem>();
            List<SelectListItem> languageLst = new List<SelectListItem>();
            model.appInfo = new List<appInfo>();
            actionResult = adminAction.Country_LoadAll();
            if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
            {
                foreach (DataRow dr in actionResult.dtResult.Rows)
                {
                    countryLst.Add(new SelectListItem { Text = dr["CountryName"].ToString(), Value = dr["ID"].ToString() });
                }
            }
            model.CountryList = countryLst;
            model.StateList = stateLst;
            SRV.BaseLayer.ActionResult LanguageAction = new SRV.BaseLayer.ActionResult();
            LanguageAction = adminAction.Language_LoadAll();
            if (LanguageAction.IsSuccess)
            {
                foreach (DataRow dr in LanguageAction.dtResult.Rows)
                {
                    languageLst.Add(new SelectListItem { Text = dr["Name"].ToString() + " (" + dr["Abbreviation"].ToString() + ")", Value = dr["Id"].ToString() });
                }
            }
            model.LanguageList = languageLst;
            SRV.BaseLayer.ActionResult appinfolist = new SRV.BaseLayer.ActionResult();
            appinfolist = accountAction.get_app_info();
            if (LanguageAction.IsSuccess)
            {
                foreach (DataRow dr in appinfolist.dtResult.Rows)
                {
                    model.appInfo.Add(new appInfo { appId = Convert.ToInt16(dr["appId"]), appName = dr["appName"].ToString(), appImage= dr["appImage"].ToString() });
                }
            }            
            return View(model);
        }
        #endregion

        #region Register Post
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Register(AdminRegisterModel model, FormCollection fc)
        {
            string role = fc["hdnRole"] != null ? fc["hdnRole"] : "";
            long UserId = 0;
            List<appInfoBase> appinfo = JsonConvert.DeserializeObject<List<appInfoBase>>(model.appid);
            try
            {
                adminBase.UserGuid = Guid.NewGuid().ToString();
                if (role == "admin")
                {
                    adminBase.RoleId = 2;
                }
                if (role == "user")
                {
                    adminBase.RoleId = 3;
                }
                if (role == "panel")
                {
                    adminBase.RoleId = 4;
                }
                if (role == "field")
                {
                    adminBase.RoleId = 5;
                }
                adminBase.FirstName = model.FirstName;
                adminBase.LastName = model.LastName;
                adminBase.Gender = model.Gender;
                adminBase.Email = model.Email;
                adminBase.Password = model.Password;
                adminBase.Address = model.Address;
                adminBase.CountryId = Convert.ToInt32(model.CountryId);
                adminBase.StateId = Convert.ToInt32(model.StateId);
                adminBase.City = model.City;
                adminBase.ZipCode = model.ZipCode;
                adminBase.SecurityQuestion = model.SecurityQuestion;
                adminBase.AnswerToSecurityQuestion = model.AnswerToSecurityQuestion;
                adminBase.CountryOfResidenceId = model.CountryOfResidenceId;
                adminBase.LanguageId = model.LanguageId;
                adminBase.CompanyName = model.CompanyName;
                adminBase.PhoneNumber = model.PhoneNumber;
                adminBase.SendEmail = model.sendEmail;
                adminBase.OtherCommunications = model.OtherCommunications;

                actionResult = adminAction.Register_InsertUpdate(adminBase);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    int status = Convert.ToInt32(actionResult.dtResult.Rows[0][0]);
                    if (status == -10)
                    {
                        TempData["RegErrorMessage"] = "Email address already exists, Please choose another email.";
                    }
                    else
                    {
                        Email.SendConfirmationEmail(adminBase.Email, adminBase.UserGuid, adminBase.FirstName);
                        UserId = Convert.ToInt32(status);
                        actionResult = accountAction.insert_user_app_Info(appinfo, UserId);
                        //if(actionResult.dtResult!=null && actionResult.dtResult.Rows.Count > 0)
                        //{

                        //}
                        TempData["RegSuccessMessage"] = "You have registered successfully, A verification email has been sent to your email : " + adminBase.Email + " Please verify your email and then login.";
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return RedirectToAction("Register");
        }

        #endregion

        #region ConfirmRegistration
        public ActionResult ConfirmRegistration(string id)
        {
            try
            {
                adminBase.UserGuid = id;
                int result = 0;
                actionResult = adminAction.AdminRegistration_Confirm(adminBase);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    result = Convert.ToInt32(actionResult.dtResult.Rows[0][0]);
                    if (result > 0)
                    {
                        TempData["SuccessMessage"] = "Your account is activated now. Please login to continue .";
                    }
                    else if (result == 0)
                    {
                        TempData["ErrorMessage"] = "Your account has been already activated.";
                    }
                    else
                    {
                        TempData["ErrorMessage"] = "Your token has been expired.";
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return RedirectToAction("Login");
        }
        #endregion

        #region Manage Get
        public ActionResult Manage(ManageMessageId? message)
        {
            ViewBag.StatusMessage =
                message == ManageMessageId.ChangePasswordSuccess ? "Your password has been changed."
                : message == ManageMessageId.SetPasswordSuccess ? "Your password has been set."
                : message == ManageMessageId.RemoveLoginSuccess ? "The external login was removed."
                : "";
            ViewBag.HasLocalPassword = OAuthWebSecurity.HasLocalAccount(WebSecurity.GetUserId(User.Identity.Name));
            ViewBag.ReturnUrl = Url.Action("Manage");
            return View();
        }
        #endregion

        #region Manage Post
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Manage(LocalPasswordModel model)
        {
            bool hasLocalAccount = OAuthWebSecurity.HasLocalAccount(WebSecurity.GetUserId(User.Identity.Name));
            ViewBag.HasLocalPassword = hasLocalAccount;
            ViewBag.ReturnUrl = Url.Action("Manage");
            if (hasLocalAccount)
            {
                if (ModelState.IsValid)
                {
                    // ChangePassword will throw an exception rather than return false in certain failure scenarios.
                    bool changePasswordSucceeded;
                    try
                    {
                        changePasswordSucceeded = WebSecurity.ChangePassword(User.Identity.Name, model.OldPassword, model.NewPassword);
                    }
                    catch (Exception)
                    {
                        changePasswordSucceeded = false;
                    }

                    if (changePasswordSucceeded)
                    {
                        return RedirectToAction("Manage", new { Message = ManageMessageId.ChangePasswordSuccess });
                    }
                    else
                    {
                        ModelState.AddModelError("", "The current password is incorrect or the new password is invalid.");
                    }
                }
            }
            else
            {
                // User does not have a local password so remove any validation errors caused by a missing
                // OldPassword field
                ModelState state = ModelState["OldPassword"];
                if (state != null)
                {
                    state.Errors.Clear();
                }

                if (ModelState.IsValid)
                {
                    try
                    {
                        WebSecurity.CreateAccount(User.Identity.Name, model.NewPassword);
                        return RedirectToAction("Manage", new { Message = ManageMessageId.SetPasswordSuccess });
                    }
                    catch (Exception e)
                    {
                        ModelState.AddModelError("", e);
                    }
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }
        #endregion

        #region ExternalLogin
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ExternalLogin(string provider, string returnUrl)
        {
            return new ExternalLoginResult(provider, Url.Action("ExternalLoginCallback", new { ReturnUrl = returnUrl }));
        }
        #endregion

        #region ExternalLoginCallback
        [AllowAnonymous]
        public ActionResult ExternalLoginCallback(string returnUrl)
        {
            AuthenticationResult result = OAuthWebSecurity.VerifyAuthentication(Url.Action("ExternalLoginCallback", new { ReturnUrl = returnUrl }));
            if (!result.IsSuccessful)
            {
                return RedirectToAction("ExternalLoginFailure");
            }

            if (OAuthWebSecurity.Login(result.Provider, result.ProviderUserId, createPersistentCookie: false))
            {
                return RedirectToLocal(returnUrl);
            }

            if (User.Identity.IsAuthenticated)
            {
                // If the current user is logged in add the new account
                OAuthWebSecurity.CreateOrUpdateAccount(result.Provider, result.ProviderUserId, User.Identity.Name);
                return RedirectToLocal(returnUrl);
            }
            else
            {
                // User is new, ask for their desired membership name
                string loginData = OAuthWebSecurity.SerializeProviderUserId(result.Provider, result.ProviderUserId);
                ViewBag.ProviderDisplayName = OAuthWebSecurity.GetOAuthClientData(result.Provider).DisplayName;
                ViewBag.ReturnUrl = returnUrl;
                return View("ExternalLoginConfirmation", new RegisterExternalLoginModel { UserName = result.UserName, ExternalLoginData = loginData });
            }
        }
        #endregion

        #region ExternalLoginConfirmation
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ExternalLoginConfirmation(RegisterExternalLoginModel model, string returnUrl)
        {
            string provider = null;
            string providerUserId = null;

            if (User.Identity.IsAuthenticated || !OAuthWebSecurity.TryDeserializeProviderUserId(model.ExternalLoginData, out provider, out providerUserId))
            {
                return RedirectToAction("Manage");
            }

            if (ModelState.IsValid)
            {
                // Insert a new user into the database
                using (UsersContext db = new UsersContext())
                {
                    UserProfile user = db.UserProfiles.FirstOrDefault(u => u.UserName.ToLower() == model.UserName.ToLower());
                    // Check if user already exists
                    if (user == null)
                    {
                        // Insert name into the profile table
                        db.UserProfiles.Add(new UserProfile { UserName = model.UserName });
                        db.SaveChanges();

                        OAuthWebSecurity.CreateOrUpdateAccount(provider, providerUserId, model.UserName);
                        OAuthWebSecurity.Login(provider, providerUserId, createPersistentCookie: false);

                        return RedirectToLocal(returnUrl);
                    }
                    else
                    {
                        ModelState.AddModelError("UserName", "User name already exists. Please enter a different user name.");
                    }
                }
            }

            ViewBag.ProviderDisplayName = OAuthWebSecurity.GetOAuthClientData(provider).DisplayName;
            ViewBag.ReturnUrl = returnUrl;
            return View(model);
        }
        #endregion

        #region ExternalLoginFailure
        [AllowAnonymous]
        public ActionResult ExternalLoginFailure()
        {
            return View();
        }
        #endregion

        #region ExternalLoginsList
        [AllowAnonymous]
        [ChildActionOnly]
        public ActionResult ExternalLoginsList(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return PartialView("_ExternalLoginsListPartial", OAuthWebSecurity.RegisteredClientData);
        }
        #endregion

        #region RemoveExternalLogins
        [ChildActionOnly]
        public ActionResult RemoveExternalLogins()
        {
            ICollection<OAuthAccount> accounts = OAuthWebSecurity.GetAccountsFromUserName(User.Identity.Name);
            List<ExternalLogin> externalLogins = new List<ExternalLogin>();
            foreach (OAuthAccount account in accounts)
            {
                AuthenticationClientData clientData = OAuthWebSecurity.GetOAuthClientData(account.Provider);

                externalLogins.Add(new ExternalLogin
                {
                    Provider = account.Provider,
                    ProviderDisplayName = clientData.DisplayName,
                    ProviderUserId = account.ProviderUserId,
                });
            }

            ViewBag.ShowRemoveButton = externalLogins.Count > 1 || OAuthWebSecurity.HasLocalAccount(WebSecurity.GetUserId(User.Identity.Name));
            return PartialView("_RemoveExternalLoginsPartial", externalLogins);
        }
        #endregion

        #region Forgot Password Get
        [AllowAnonymous]
        [HttpGet]
        public ActionResult ForgotPassword()
        {
            return View();
        }
        #endregion

        #region Forgot Password Post
        [AllowAnonymous]
        [HttpPost]
        public ActionResult ForgotPassword(LoginModel model)
        {
            int result = 0;
            accountBase.Email = model.Email;
            try
            {
                actionResult = accountAction.ForgotPassword(accountBase);
                if (actionResult.IsSuccess)
                {
                    if (actionResult.dtResult.Rows[0][0].ToString() == "-1")
                    {
                        result = -1;
                        TempData["ForgotErrorMessage"] = "Invalid Email Address, Please Choose Another.";
                    }
                    else
                    {
                        string guid = actionResult.dtResult.Rows[0][0].ToString();
                        Email.SendForgotPasswordEmail(accountBase.Email, guid);
                        result = 1;
                        TempData["ForgotSuccessMessage"] = "Password Expiration link has been sent to your Email address.";
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }

            return View();
        }
        #endregion

        #region Reset Password Get
        [AllowAnonymous]
        [HttpGet]
        public ActionResult ResetPassword(string id)
        {
            ViewBag.token = id;
            return View();
        }
        #endregion

        #region Reset Password Post
        [AllowAnonymous]
        [HttpPost]
        public ActionResult ResetPassword(FormCollection fc, RegisterModel model)
        {
            accountBase.UserGuid = fc["hdnToken"];
            accountBase.Password = model.Password;
            int result = 0;
            try
            {
                actionResult = accountAction.ResetPassword(accountBase);
                if (actionResult.IsSuccess)
                {
                    result = Convert.ToInt32(actionResult.dtResult.Rows[0][0]);
                    if (result > 0)
                    {
                        TempData["SuccessMessage"] = "Your password has been changed successfully.";
                        return RedirectToAction("Login");
                    }
                    else
                    {
                        TempData["ResetErrorMessage"] = "Your link has been expired.";
                        return View();
                    }
                }

            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return View();

        }
        #endregion

        #region Helpers
        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        public enum ManageMessageId
        {
            ChangePasswordSuccess,
            SetPasswordSuccess,
            RemoveLoginSuccess,
        }

        internal class ExternalLoginResult : ActionResult
        {
            public ExternalLoginResult(string provider, string returnUrl)
            {
                Provider = provider;
                ReturnUrl = returnUrl;
            }

            public string Provider { get; private set; }
            public string ReturnUrl { get; private set; }

            public override void ExecuteResult(ControllerContext context)
            {
                OAuthWebSecurity.RequestAuthentication(Provider, ReturnUrl);
            }
        }

        private static string ErrorCodeToString(MembershipCreateStatus createStatus)
        {
            // See http://go.microsoft.com/fwlink/?LinkID=177550 for
            // a full list of status codes.
            switch (createStatus)
            {
                case MembershipCreateStatus.DuplicateUserName:
                    return "User name already exists. Please enter a different user name.";

                case MembershipCreateStatus.DuplicateEmail:
                    return "A user name for that e-mail address already exists. Please enter a different e-mail address.";

                case MembershipCreateStatus.InvalidPassword:
                    return "The password provided is invalid. Please enter a valid password value.";

                case MembershipCreateStatus.InvalidEmail:
                    return "The e-mail address provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidAnswer:
                    return "The password retrieval answer provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidQuestion:
                    return "The password retrieval question provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidUserName:
                    return "The user name provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.ProviderError:
                    return "The authentication provider returned an error. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                case MembershipCreateStatus.UserRejected:
                    return "The user creation request has been canceled. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                default:
                    return "An unknown error occurred. Please verify your entry and try again. If the problem persists, please contact your system administrator.";
            }
        }
        #endregion

        #region BindState
        public JsonResult BindState(int cId = 0)
        {
            List<SelectListItem> stateLst = new List<SelectListItem>();
            adminBase.CountryId = cId;
            actionResult = adminAction.State_LoadAll(adminBase);
            if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
            {
                foreach (DataRow dr in actionResult.dtResult.Rows)
                {
                    stateLst.Add(new SelectListItem { Text = dr["StateName"].ToString(), Value = dr["ID"].ToString() });
                }
            }
            return Json(stateLst, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}
