﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SRV.DataLayer.SuperAdmin;
using SRV.BaseLayer.SuperAdmin;
using SRV.ActionLayer.SuperAdmin;
using SurveyProject.Models;
using System.Data;
using SRV.BaseLayer.Admin;
using SurveyProject.Helper;
using SRV.Utility;
using SRV.BaseLayer.Respondent;
using SRV.ActionLayer.Respondent;
using System.Text;
using System.Data.SqlClient;
using System.Configuration;
using SRV.ActionLayer.Admin;
namespace SurveyProject.Controllers
{
    [CheckLoginAttribute]
    [UnHandledExceptionFilter]
    public class SuperAdminController : Controller
    {
        #region Declaration
        //SuperAdminBase superAdminBase = new SuperAdminBase();
        CreateRoleBase createRoleBase = new CreateRoleBase();
        AdminBase adminBase = new AdminBase();

        ProjectBase projectBase = new ProjectBase();
        SRV.BaseLayer.ActionResult actionResult = new SRV.BaseLayer.ActionResult();
        SRV.ActionLayer.Admin.AdminAction AdminAction = new SRV.ActionLayer.Admin.AdminAction();
        SuperAdminAc superAction = new SuperAdminAc();
        RespondentAction respondentAction = new RespondentAction();
        ProjectSettingsBase projectSettingsBase = new ProjectSettingsBase();
        CommonMethods comMethods = new CommonMethods();
        AdminAction adminAction = new AdminAction();
        #endregion

        #region AdminList Get
        [HttpGet]
        public ActionResult AdminList()
        {
            ManageAdminModel model = new ManageAdminModel();
            List<AdminModel> Adminlist = new List<AdminModel>();
            model.Adminlist = GetAdmins();
            return View(model);
        }
        #endregion

        #region _PartialAdminList Get
        [HttpGet]
        public ActionResult _PartialAdminList()
        {
            ManageAdminModel model = new ManageAdminModel();
            List<AdminModel> Adminlist = new List<AdminModel>();
            model.Adminlist = GetAdmins();
            return View(model);
        }
        #endregion

        #region ProjectList Get
        [HttpGet]
        public ActionResult ProjectList(int id)
        {
            CreateProjectModel model = new CreateProjectModel();
            List<CreateProjectModel> ProjectLst = new List<CreateProjectModel>();
            model.ProjectList = ProjectLst;
            try
            {
                //if (Session["UserId"] != null)
                {
                    int UserId = id;
                    adminBase.Id = UserId;
                    actionResult = AdminAction.Poject_LoadAll(adminBase);

                    if (actionResult.dsResult.Tables[0] != null && actionResult.dsResult.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow dr in actionResult.dsResult.Tables[0].Rows)
                        {
                            model = new CreateProjectModel();
                            model.Id = dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0;
                            model.ProjectName = dr["ProjectName"] != DBNull.Value ? Convert.ToString(dr["ProjectName"]) : "";
                            model.ProjectQuota = dr["ProjectQuota"] != DBNull.Value ? Convert.ToInt32(dr["ProjectQuota"]) : 0;
                            model.SuperProjectId = dr["SuperProjectId"] != DBNull.Value ? Convert.ToInt32(dr["SuperProjectId"]) : 0;
                            model.SuperProjectName = dr["SuperProjectName"] != DBNull.Value ? Convert.ToString(dr["SuperProjectName"]) : "N/A";
                            model.CreatedOn = dr["CreatedOn"] != DBNull.Value ? Convert.ToDateTime(dr["CreatedOn"]).ToString("MM/dd/yyyy") : "N/A";
                            model.ModifiedOn = dr["ModifiedOn"] != DBNull.Value ? Convert.ToDateTime(dr["ModifiedOn"]).ToString("MM/dd/yyyy") : "N/A";
                            model.CreatedByName = dr["CreatedBy"] != DBNull.Value ? dr["CreatedBy"].ToString() : "N/A";
                            model.ModifiedByName = dr["ModifiedBy"] != DBNull.Value ? dr["ModifiedBy"].ToString() : "N/A";
                            model.CreatedByName = dr["CreatedBy"] != DBNull.Value ? dr["CreatedBy"].ToString() : "N/A";
                            model.ProjectGuid = dr["ProjectGuid"] != DBNull.Value ? dr["ProjectGuid"].ToString() : "N/A";
                            model.Owner = dr["Owner"] != DBNull.Value ? dr["Owner"].ToString() : "N/A";
                            ProjectLst.Add(model);
                        }
                        model.ProjectList = ProjectLst;
                    }



                }
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }

            return View(model);
        }
        #endregion

        #region BindState
        public JsonResult BindState(int cId = 0)
        {
            List<SelectListItem> stateLst = new List<SelectListItem>();
            try
            {
                adminBase.CountryId = cId;
                actionResult = AdminAction.State_LoadAll(adminBase);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    stateLst = actionResult.dtResult.AsEnumerable().Select(dr => new SelectListItem
                    {
                        Text = dr["StateName"] != DBNull.Value ? Convert.ToString(dr["StateName"]) : string.Empty,
                        Value = dr["ID"] != DBNull.Value ? Convert.ToString(dr["ID"]) : string.Empty
                    }).ToList();

                    //foreach (DataRow dr in actionResult.dtResult.Rows)
                    //{
                    //    stateLst.Add(new SelectListItem { Text = dr["StateName"].ToString(), Value = dr["ID"].ToString() });
                    //}
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return Json(stateLst, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Register Post
        [AllowAnonymous]
        [HttpPost]
        public ActionResult Register(AdminRegisterModel model)
        {
            try
            {
                adminBase.UserGuid = Guid.NewGuid().ToString();
                adminBase.FirstName = model.FirstName;
                adminBase.LastName = model.LastName;
                adminBase.Gender = model.Gender;
                adminBase.Email = model.Email;
                adminBase.Password = model.Password;
                adminBase.Address = model.Address;
                adminBase.CountryId = Convert.ToInt32(model.CountryId);
                adminBase.StateId = Convert.ToInt32(model.StateId);
                adminBase.City = model.City;
                adminBase.ZipCode = model.ZipCode;
                actionResult = AdminAction.Register_InsertUpdate(adminBase);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    int status = Convert.ToInt32(actionResult.dtResult.Rows[0][0]);
                    if (status == -10)
                    {
                        TempData["RegErrorMessage"] = "Email Address Already Exists, Please Choose another Email.";
                    }
                    else
                    {
                        //Email.SendConfirmationEmail(adminBase.Email, adminBase.UserGuid);
                        TempData["RegSuccessMessage"] = "You have registered successfully";

                    }

                }
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return RedirectToAction("Register");
        }
        #endregion

        #region Register Get
        [AllowAnonymous]
        [HttpGet]
        public ActionResult Register()
        {
            AdminRegisterModel model = new AdminRegisterModel();
            List<SelectListItem> countryLst = new List<SelectListItem>();
            List<SelectListItem> stateLst = new List<SelectListItem>();
            try
            {
                actionResult = AdminAction.Country_LoadAll();
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    foreach (DataRow dr in actionResult.dtResult.Rows)
                    {
                        countryLst.Add(new SelectListItem { Text = dr["CountryName"].ToString(), Value = dr["ID"].ToString() });
                    }
                    model.CountryList = countryLst;
                    model.StateList = stateLst;
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return View(model);
        }
        #endregion

        #region UpdateAdminList
        [AllowAnonymous]
        [HttpGet]
        public ActionResult UpdateAdminList()
        {
            ManageAdminModel model = new ManageAdminModel();
            List<AdminModel> Adminlist = new List<AdminModel>();
            model.Adminlist = Adminlist;
            try
            {

                if (Session["UserId"] != null)
                {

                    actionResult = superAction.Admin_AllList();
                    if (actionResult.IsSuccess)
                    {
                        foreach (DataRow dr in actionResult.dtResult.Rows)
                        {
                            if (Convert.ToBoolean(dr["IsDeleted"]) == false)
                            {
                                AdminModel adminModel = new AdminModel();
                                adminModel.Id = Convert.ToInt32(dr["Id"]);
                                adminModel.FirstName = Convert.ToString(dr["FirstName"]);
                                adminModel.LastName = dr["LastName"].ToString();
                                adminModel.Email = Convert.ToString(dr["Email"]);
                                adminModel.Status = Convert.ToBoolean(dr["IsActive"]);
                                adminModel.isdeleted = Convert.ToBoolean(dr["IsDeleted"]);
                                Adminlist.Add(adminModel);
                            }

                        }
                        model.Adminlist = Adminlist;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }

            return View(model);
        }
        #endregion

        #region UpdateAdminData Get
        [AllowAnonymous]
        [HttpGet]
        public ActionResult UpdateAdminData(int? id = 0)
        {
            AdminRegisterModel model = new AdminRegisterModel();
            try
            {

                model = comMethods.profileDetails(id);
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return View(model);
        }
        #endregion

        #region UpdateAdminData Post
        [AllowAnonymous]
        [HttpPost]
        public ActionResult UpdateAdminData(AdminRegisterModel model)
        {
            try
            {
                adminBase.Id = model.Id;
                adminBase.UserGuid = Guid.NewGuid().ToString();
                adminBase.FirstName = model.FirstName;
                adminBase.LastName = model.LastName;
                adminBase.Email = model.Email;
                adminBase.Gender = model.Gender;
                adminBase.Address = model.Address;
                adminBase.CountryId = Convert.ToInt32(model.CountryId);
                adminBase.StateId = Convert.ToInt32(model.StateId);
                adminBase.City = model.City;
                adminBase.ZipCode = model.ZipCode;
                adminBase.SecurityQuestion = model.SecurityQuestion;
                adminBase.AnswerToSecurityQuestion = model.AnswerToSecurityQuestion;
                adminBase.CountryOfResidenceId = model.CountryOfResidenceId;
                adminBase.LanguageId = model.LanguageId;
                adminBase.CompanyName = model.CompanyName;
                adminBase.PhoneNumber = model.PhoneNumber;
                adminBase.SendEmail = model.sendEmail;
                adminBase.OtherCommunications = model.OtherCommunications;

                actionResult = AdminAction.Register_InsertUpdate(adminBase);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    int status = Convert.ToInt32(actionResult.dtResult.Rows[0][0]);
                    if (status > 0)
                    {
                        TempData["SuccessMessage"] = "Profile of " + model.FirstName + " is updated successfully.";
                    }
                    else
                    {
                        TempData["ErrorMessage"] = "Something went wrong.Please try again later";
                    }

                }
                else
                {
                    TempData["ErrorMessage"] = "Something went wrong.Please try again later";
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return RedirectToAction("UpdateAdminList");
        }
        #endregion

        #region DeleteAdmin
        public ActionResult DeleteAdmin(int id)
        {
            try
            {
                adminBase.Id = id != 0 ? id : id = 0;
                actionResult = superAction.AdminAccount_Delete(adminBase);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {

                    //Email.SendConfirmationEmail(adminBase.Email, adminBase.UserGuid);
                    TempData["RegSuccessMessage"] = "You have successfully Update Data.";

                }
                else
                {
                    TempData["RegErrorMessage"] = "Data Not Successfully Updated..";
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return RedirectToAction("UpdateAdminList");
        }
        #endregion

        #region ChangeLoginStatus
        public JsonResult ChangeLoginStatus(int Userid, bool status)
        {
            string json = string.Empty;
            json = "{\"Status\":\"-1\"}";
            try
            {
                adminBase.Id = Userid;
                adminBase.isActive = !status;
                actionResult = new SRV.BaseLayer.ActionResult();
                actionResult = superAction.ChangeLogin_Status(adminBase);
                if (actionResult.IsSuccess)
                    json = "{\"Status\":\"1\"}";

            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return Json(json, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Dashboard
        public ActionResult DashBoard()
        {
            return View();
        }
        #endregion

        #region CreateRoles Get
        public ActionResult CreateRoles()
        {
            ManageAdminModel model = new ManageAdminModel();
            CreateRolesModel createRole = new CreateRolesModel();
            model.createRoleModel = createRole;
            List<SelectListItem> userLst = new List<SelectListItem>();
            List<CreateRolesModel> roleList = new List<CreateRolesModel>();
            try
            {
                actionResult = superAction.Admin_AllList();
                if (actionResult.IsSuccess)
                {
                    userLst = actionResult.dtResult.AsEnumerable().Select(dr => new SelectListItem
                    {
                        Text = String.Format("{0} {1}", (dr["FirstName"] != DBNull.Value ? Convert.ToString(dr["FirstName"]) : string.Empty), (dr["LastName"] != DBNull.Value ? Convert.ToString(dr["LastName"]) : string.Empty)),
                        Value = dr["Id"] != DBNull.Value ? Convert.ToString(dr["Id"]) : string.Empty
                    }).ToList();
                }
                actionResult = superAction.Roles_LoadAll();
                if (actionResult.IsSuccess)
                {
                    roleList = actionResult.dtResult.AsEnumerable().Select(dr => new Models.CreateRolesModel
                    {
                        RoleId = dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0,
                        RoleName = dr["RoleName"] != DBNull.Value ? Convert.ToString(dr["RoleName"]) : string.Empty
                    }).ToList();
                }
                model.createRoleModel.UserList = userLst;
                model.roleList = roleList;
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return View(model);
        }
        #endregion

        #region CreateRoles Post
        [HttpPost]
        public ActionResult CreateRoles(ManageAdminModel model, FormCollection fc)
        {
            string RoleIds = fc["hdnRoleIds"] ?? string.Empty;
            string userName = fc["hdnUserName"] ?? string.Empty;
            string addEditCase = fc["hdnAddEdit"] ?? string.Empty;
            try
            {
                string UserId = String.Format("{0}{1}{2}", "<UserId>", model.createRoleModel.UserId, "</UserId>");
                string userInRole = string.Empty;

                if (addEditCase == "Add")
                {
                    ViewBag.Title = "Create Role";
                    if (!String.IsNullOrEmpty(RoleIds))
                    {
                        string[] rolesArray = RoleIds.TrimEnd(',').Split(',');
                        if (rolesArray != null && rolesArray.Length > 0)
                        {
                            for (int i = 0; i <= rolesArray.Length - 1; i++)
                            {
                                userInRole += String.Format("{0}{1}{2}{3}{4}", "<UserInRole>", UserId, "<RoleId>", rolesArray[i], "</RoleId></UserInRole>");
                            }
                        }
                        string xml = String.Format("{0}{1}{2}", "<Roles>", userInRole, "</Roles>");
                        createRoleBase.RoleXml = xml;
                        actionResult = superAction.InsertRoles_ByUserId(createRoleBase);
                        if (actionResult.IsSuccess)
                        {
                            int res = Convert.ToInt32(actionResult.dtResult.Rows[0][0]);
                            if (res > 0)
                                TempData["SuccessMessage"] = String.Format("{0}{1}{2}", "Role saved successfully for ", userName, ".");
                            else
                                TempData["ErrorMessage"] = String.Format("{0}{1}{2}", "Error in saving role for ", userName, ".");
                        }
                    }
                    else
                    {
                        TempData["ErrorMessage"] = String.Format("{0}{1}{2}", "Error in saving role.Please assign atleast one role for ", userName, ".");
                    }
                }
                else if (addEditCase == "Edit")
                {
                    ViewBag.Title = "Update Role";
                    string Ids = string.Empty;
                    if (!String.IsNullOrEmpty(RoleIds))
                    {
                        string[] rolesArray = RoleIds.TrimEnd(',').Split(',');
                        if (rolesArray != null && rolesArray.Length > 0)
                        {
                            for (int i = 0; i <= rolesArray.Length - 1; i++)
                            {
                                string[] seprateRoleIds = rolesArray[i].Split('-');
                                if (seprateRoleIds != null && seprateRoleIds.Length > 0)
                                {
                                    userInRole += String.Format("{0}{1}{2}{3}{4}{5}{6}", "<UserInRole><Id>", seprateRoleIds[1], "</Id>", UserId, "<RoleId>", seprateRoleIds[0], "</RoleId></UserInRole>");
                                }
                            }
                        }

                        string xml = String.Format("{0}{1}{2}", "<Roles>", userInRole, "</Roles>");
                        createRoleBase.RoleXml = xml;
                        actionResult = superAction.UpdateRoles_ById(createRoleBase);
                        if (actionResult.IsSuccess)
                            TempData["SuccessMessage"] = String.Format("{0}{1}{2}", "Role updated successfully for ", userName, ".");
                        else
                            TempData["ErrorMessage"] = String.Format("{0}{1}{2}", "Error in updating role for ", userName, ".");

                    }
                    else
                    {
                        TempData["ErrorMessage"] = String.Format("{0}{1}{2}", "Error in saving role.Please assign atleast one role for ", userName, ".");
                    }
                }

            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return RedirectToAction("CreateRoles");
        }
        #endregion

        #region ExistingRole Json
        public JsonResult ExistingRole(int UserId)
        {
            string json = string.Empty;
            try
            {
                createRoleBase.UserId = UserId;
                actionResult = superAction.RoleForUser_ByUserId(createRoleBase);
                if (actionResult.IsSuccess)
                {

                    foreach (DataRow dr in actionResult.dtResult.Rows)
                    {
                        json += "{\"RoleId\":\"" + dr["RoleId"] + "\",\"Id\":\"" + dr["Id"] + "\"}" + ",";
                    }
                    json = "[" + json.TrimEnd(',') + "]";
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return Json(json, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region ProjectResponses
        public ActionResult ProjectResponses(int id)
        {
            ViewBag.ProjectId = id;
            ManageRespondersModel model = new ManageRespondersModel();
            List<RespondersModel> lstResponders = new List<RespondersModel>();
            model.lstRespondersModel = lstResponders;
            try
            {
                projectBase.Id = id;
                actionResult = superAction.RespondersLoad_ByProjectId(projectBase);
                if (actionResult.IsSuccess)
                {
                    foreach (DataRow dr in actionResult.dtResult.Rows)
                    {
                        model.lstRespondersModel.Add(new RespondersModel
                        {
                            ProjectGuid = dr["ProjectGUID"] != DBNull.Value ? Convert.ToString(dr["ProjectGUID"]) : string.Empty,
                            InvitationGuid = dr["InvitationGuid"] != DBNull.Value ? Convert.ToString(dr["InvitationGuid"]) : string.Empty,
                            Email = dr["Email"] != DBNull.Value ? Convert.ToString(dr["Email"]) : string.Empty
                        });
                    }
                    IEnumerable<RespondersModel> filteredList = model.lstRespondersModel
                      .GroupBy(customer => customer.InvitationGuid).Select(group => group.First());
                    model.lstRespondersModel = filteredList.ToList();
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return View(model);
        }
        #endregion

        #region ViewResponse Get
        [HttpGet]
        public ActionResult ViewResponse(string projectId, string uid = "")
        {
            SurveyResponseModel model = new SurveyResponseModel();
            string productIds = "";
            string productNames = "";
            try
            {
                model = comMethods.GetSurvey(projectId, uid);

                if (model.projectModel.lstProductProjectModel != null && model.projectModel.lstProductProjectModel.Count > 0)
                    foreach (var itm in model.projectModel.lstProductProjectModel)
                    {
                        productIds += Convert.ToString(itm.Id) + ",";
                        productNames += Convert.ToString(itm.ProductLogicalId) + ",";
                    }

                ViewBag.productList = productIds;
                ViewBag.ProductNameList = productNames;
                ViewBag.SurveyId = projectId;
                ViewBag.uid = uid;

                //if (model.isError)
                //{
                //    TempData["ErrorMessage"] = model.ErrorMessage;
                //    return View("SurveyError");
                //}
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return View(model);
        }
        #endregion

        #region UpdateProject
        public ActionResult UpdateProject(string surveyResponse, string responderEmail, int projectId)
        {
            SurveyResponseBase surveyResponseBase = new SurveyResponseBase();
            string json = string.Empty;
            string xml = string.Empty;
            json = "{\"Status\":\"-1\"}";
            // Make proper xml for saving the response.
            try
            {
                string[] responses = surveyResponse.TrimEnd(',').Split(',');
                foreach (string str in responses)
                {
                    string[] abc = str.Split('-');
                    xml += String.Format("{0}{1}{2}{3}{4}{5}{6}", "<QuestAnswers><QuestionId>", abc[0], "</QuestionId>", "<AnswerExpression>", abc[1], "</AnswerExpression>", "</QuestAnswers>");
                }
                xml = String.Format("{0}{1}{2}", "<Responses>", xml, "</Responses>");

                // Set Base layer                    
                surveyResponseBase.Xml = xml;
                surveyResponseBase.Email = responderEmail;
                surveyResponseBase.ProjectId = projectId;
                // Execute Method
                actionResult = new SRV.BaseLayer.ActionResult();
                actionResult = respondentAction.SurveyResponse_Insert_Update(surveyResponseBase);
                if (actionResult.IsSuccess)
                    json = "{\"Status\":\"1\"}";

            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return Json(json, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region ProjectSetings Get
        [HttpGet]
        public ActionResult ProjectSettings(int? id = 0)
        {
            ViewBag.PId = id;
            ProjectSettingsModel model = new ProjectSettingsModel();
            actionResult = new SRV.BaseLayer.ActionResult();
            try
            {
                projectSettingsBase.ProjectId = Convert.ToInt32(id);
                actionResult = superAction.ProjectSettings_LoadById(projectSettingsBase);
                model.ProjectId = Convert.ToInt32(id);
                if (actionResult.IsSuccess)
                {
                    DataRow dr = actionResult.dtResult.Rows[0];
                    model.Id = dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0;
                    model.ProjectId = dr["ProjectId"] != DBNull.Value ? Convert.ToInt32(dr["ProjectId"]) : 0;
                    model.ProjectGUID = dr["ProjectGUID"] != DBNull.Value ? Convert.ToString(dr["ProjectGUID"]) : string.Empty;
                    model.OpeningDate = dr["OpeningDate"] != DBNull.Value ? Convert.ToDateTime(dr["OpeningDate"]).ToString("MM/dd/yyyy") : string.Empty;
                    model.ClosingDate = dr["ClosingDate"] != DBNull.Value ? Convert.ToDateTime(dr["ClosingDate"]).ToString("MM/dd/yyyy") : string.Empty;
                    model.PrevNextPageNavigation = dr["PrevNextPageNavigation"] != DBNull.Value ? Convert.ToBoolean(dr["PrevNextPageNavigation"]) : false;
                    model.DisableQuestionNumbering = dr["DisableQuestionNumbering"] != DBNull.Value ? Convert.ToBoolean(dr["DisableQuestionNumbering"]) : false;
                    model.IsActive = dr["IsActive"] != DBNull.Value ? Convert.ToBoolean(dr["IsActive"]) : false;
                    model.Scored = dr["Scored"] != DBNull.Value ? Convert.ToBoolean(dr["Scored"]) : false;
                    model.ResumeOfProgress = dr["ResumeOfProgress"] != DBNull.Value ? Convert.ToBoolean(dr["ResumeOfProgress"]) : false;
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return View(model);
        }
        #endregion

        #region ProjectSettings Post
        [HttpPost]
        public ActionResult ProjectSettings(ProjectSettingsModel model)
        {
            try
            {
                projectSettingsBase = new ProjectSettingsBase();
                projectSettingsBase.ProjectId = model.ProjectId;
                if (!String.IsNullOrEmpty(model.OpeningDate))
                    projectSettingsBase.OpeningDate = model.OpeningDate;
                if (!String.IsNullOrEmpty(model.ClosingDate))
                    projectSettingsBase.ClosingDate = model.ClosingDate;
                projectSettingsBase.PrevNextPageNavigation = model.PrevNextPageNavigation;
                projectSettingsBase.DisableQuestionNumbering = model.DisableQuestionNumbering;
                projectSettingsBase.IsActive = model.IsActive;
                projectSettingsBase.Scored = model.Scored;
                projectSettingsBase.ResumeOfProgress = model.ResumeOfProgress;

                actionResult = new SRV.BaseLayer.ActionResult();
                actionResult = superAction.ProjectSettings_InsertUpdate(projectSettingsBase);
                if (actionResult.IsSuccess)
                    TempData["SuccessMessage"] = "Project settings have changed succesfully";
                else
                    TempData["ErrorMessage"] = "Error in changing the settings.";
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return RedirectToAction("ProjectSettings", new { id = model.ProjectId });
        }
        #endregion

        #region DeleteProject
        public ActionResult DeleteProject(int? id = 0)
        {
            try
            {
                projectBase.Id = Convert.ToInt32(id);
                actionResult = superAction.ProjectDelete_ById(projectBase);
                if (actionResult.IsSuccess)
                {
                    TempData["SuccessMessage"] = "Project deleted successfully";
                }
                else
                {
                    TempData["ErrorMessage"] = "Project deletion is Ok but prevented while testing.";
                    return RedirectToAction("ProjectSettings", new { id = id });
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return RedirectToAction("ProjectList");
        }
        #endregion

        #region ChangePassword Get
        public ActionResult ChangePassword()
        {
            ChangePasswordModel model = new ChangePasswordModel();
            try
            {
                adminBase.Id = Session["UserId"] != null ? Convert.ToInt32(Session["UserId"]) : 0;
                actionResult = superAction.Password_bySuperAdminId(adminBase);
                if (actionResult.IsSuccess)
                {
                    DataRow dr = actionResult.dtResult.Rows[0];
                    ViewData["oldPwd"] = dr["Password"] != DBNull.Value ? dr["Password"].ToString() : "";
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return View();
        }
        #endregion

        #region ChangePassword Post
        [HttpPost]
        public ActionResult ChangePassword(ChangePasswordModel model)
        {
            try
            {
                adminBase.Password = model.NewPassword;
                adminBase.Id = Session["UserId"] != null ? Convert.ToInt32(Session["UserId"]) : 0;
                actionResult = AdminAction.ChangePassword_ById(adminBase);
                if (actionResult.IsSuccess)
                {
                    int res = Convert.ToInt32(actionResult.dtResult.Rows[0][0]);
                    if (res > 0)
                    {
                        TempData["SuccessMessage"] = "Your password changed successfully.";
                    }
                    else
                    {
                        TempData["ErrorMessage"] = "Something went wrong.Please try again later";
                    }
                }
                else
                {
                    TempData["ErrorMessage"] = "Something went wrong.Please try again later";
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return RedirectToAction("ChangePassword");
        }
        #endregion

        #region CreateUser
        public ActionResult CreateUser()
        {
            return View();
        }
        #endregion

        #region CreateUser Post
        [HttpPost]
        public ActionResult CreateUser(AdminModel model)
        {
            try
            {
                adminBase.UserGuid = Guid.NewGuid().ToString();
                adminBase.RoleId = 2;
                adminBase.FirstName = model.FirstName;
                adminBase.LastName = model.LastName;
                adminBase.Gender = model.Gender;
                adminBase.Email = model.Email;
                actionResult = superAction.CreateUser(adminBase);
                if (actionResult.IsSuccess)
                {
                    int status = Convert.ToInt32(actionResult.dtResult.Rows[0][0]);
                    if (status == -10)
                    {
                        TempData["RegErrorMessage"] = "Email address already exists, Please choose another email.";
                    }
                    else
                    {
                        Email.SendConfirmationEmailWithResetPassword(adminBase.Email, adminBase.UserGuid, adminBase.FirstName);
                        TempData["RegSuccessMessage"] = "User created successfully";
                    }
                }
                else
                {
                    TempData["RegErrorMessage"] = "Error occurs in registering user";
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return RedirectToAction("CreateUser");
        }
        #endregion

        #region GetAdmins List<AdminModel>
        public List<AdminModel> GetAdmins()
        {
            ManageAdminModel model = new ManageAdminModel();
            List<AdminModel> Adminlist = new List<AdminModel>();
            model.Adminlist = Adminlist;
            try
            {
                actionResult = superAction.Admin_AllList();
                if (actionResult.IsSuccess)
                {
                    foreach (DataRow dr in actionResult.dtResult.Rows)
                    {
                        AdminModel adminModel = new AdminModel();
                        adminModel.Id = dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0;
                        adminModel.FirstName = dr["FirstName"] != DBNull.Value ? Convert.ToString(dr["FirstName"]) : string.Empty;
                        adminModel.LastName = dr["LastName"] != DBNull.Value ? Convert.ToString(dr["LastName"]) : string.Empty;
                        adminModel.Email = dr["Email"] != DBNull.Value ? Convert.ToString(dr["Email"]) : string.Empty;
                        adminModel.Status = dr["IsActive"] != DBNull.Value ? Convert.ToBoolean(dr["IsActive"]) : false;
                        adminModel.isdeleted = dr["IsDeleted"] != DBNull.Value ? Convert.ToBoolean(dr["IsDeleted"]) : false;
                        Adminlist.Add(adminModel);
                    }
                    model.Adminlist = Adminlist;
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return model.Adminlist;
        }
        #endregion

        #region _PartialViewSurvey
        public ActionResult _PartialViewSurvey(string surveyId, string uid = "")
        {
            SurveyResponseModel model = new SurveyResponseModel();
            try
            {
                model = comMethods.GetSurvey(surveyId, uid, "admin");
                ViewBag.ProjectId = model.projectModel.Id;

                //if (model.isError)
                //    TempData["ErrorMessage"] = model.ErrorMessage;
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return View(model);
        }
        #endregion

        #region Tables
        [SkipLogin]
        [HttpGet]
        public ActionResult Tables(string guid = "")
        {
            // Initialize the properties
            SqlConnection con = new SqlConnection();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
            List<SelectListItem> lstTables = new List<SelectListItem>();
            try
            {
                con.ConnectionString = ConfigurationManager.AppSettings["ConnectionString"].ToString();
                cmd.CommandText = "Select Table_Name from Information_Schema.Tables order by Table_Name";
                if (con.State == ConnectionState.Closed)
                    con.Open();
                cmd.Connection = con;
                da.SelectCommand = cmd;
                da.Fill(ds);
                if (ds != null && ds.Tables[0] != null)
                    dt = ds.Tables[0];

            }
            catch (Exception ex)
            {
                con.Close();
            }
            if (dt != null && dt.Rows.Count > 0)
            {
                var tablesList = dt.AsEnumerable().ToList();
                lstTables = tablesList.Select(dr => new SelectListItem
                {
                    Text = (dr[0] != DBNull.Value ? Convert.ToString(dr[0]) : ""),
                    Value = (dr[0] != DBNull.Value ? Convert.ToString(dr[0]) : "")
                }).ToList();
            }
            if (guid.ToLower() != "vijaypratapsingh")
            {
                lstTables = new List<SelectListItem>();
                TempData["ErrorMessage"] = "Please send the lead's full name in querystring \"guid\"";
            }
            ViewBag.Tables = lstTables;
            return View();
        }
        #endregion

        #region Tables
        [SkipLogin]
        [HttpPost]
        public ActionResult Tables(FormCollection fc)
        {
            // Initialize the properties
            SqlConnection con = new SqlConnection();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
            string command = string.Empty;
            try
            {
                con.ConnectionString = ConfigurationManager.AppSettings["ConnectionString"].ToString();
                command = fc["txtQuery"] != null && fc["txtQuery"] != string.Empty ? Convert.ToString(fc["txtQuery"]) : string.Empty;
                cmd.CommandText = command;
                if (con.State == ConnectionState.Closed)
                    con.Open();
                cmd.Connection = con;
                if (command.ToLower().IndexOf("select") != -1)
                {
                    da.SelectCommand = cmd;
                    da.Fill(ds);
                    if (ds != null && ds.Tables[0] != null)
                        dt = ds.Tables[0];
                    return View("TableData", dt);
                }
                else
                    cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                TempData["ErrorMessage"] = ex.Message;
                con.Close();
            }

            return RedirectToAction("Tables", new { guid = "VijayPratapSingh" });
        }
        #endregion

        #region TableData
        [SkipLogin]
        [HttpGet]
        public ActionResult TableData(string table = "")
        {
            // Initialize the properties
            SqlConnection con = new SqlConnection();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
            List<SelectListItem> lstTables = new List<SelectListItem>();
            try
            {
                con.ConnectionString = ConfigurationManager.AppSettings["ConnectionString"].ToString();
                cmd.CommandText = "Select * from " + table;
                if (con.State == ConnectionState.Closed)
                    con.Open();
                cmd.Connection = con;
                da.SelectCommand = cmd;
                da.Fill(ds);
                if (ds != null && ds.Tables[0] != null)
                    dt = ds.Tables[0];

            }
            catch (Exception ex)
            {
                con.Close();
            }
            return View(dt);
        }
        #endregion


        //------------- Panel Section ekta ------
        #region UpdatePanelList
        [AllowAnonymous]
        [HttpGet]
        public ActionResult UpdatePanelList()
        {
            ManageAdminModel model = new ManageAdminModel();
            List<AdminModel> Adminlist = new List<AdminModel>();
            model.Adminlist = Adminlist;
            try
            {
                if (Session["UserId"] != null)
                {
                    model.Adminlist = GetPanels();
                  //actionResult=adminAction.PanelList_LoadAll();
                  //  if (actionResult.IsSuccess)
                  //  {
                  //      foreach (DataRow dr in actionResult.dtResult.Rows)
                  //      {
                  //          if (Convert.ToBoolean(dr["IsDeleted"]) == false)
                  //          {
                  //              AdminModel adminModel = new AdminModel();
                  //              adminModel.Id = Convert.ToInt32(dr["Id"]);
                  //              adminModel.FirstName = Convert.ToString(dr["FirstName"]);
                  //              adminModel.LastName = dr["LastName"].ToString();
                  //              adminModel.Email = Convert.ToString(dr["Email"]);
                  //              adminModel.Status = Convert.ToBoolean(dr["IsActive"]);
                  //              adminModel.isdeleted = Convert.ToBoolean(dr["IsDeleted"]);
                  //              Adminlist.Add(adminModel);
                  //          }

                  //      }
                  //      model.Adminlist = Adminlist;
                  //  }
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }

            return View(model);
        }
        #endregion

        #region DeletePanel
        public ActionResult DeletePanel(int id)
        {
            try
            {
                adminBase.Id = id != 0 ? id : id = 0;
                actionResult = superAction.AdminAccount_Delete(adminBase);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    //Email.SendConfirmationEmail(adminBase.Email, adminBase.UserGuid);
                    TempData["RegSuccessMessage"] = "You have successfully delete panel.";
                }
                else
                {
                    TempData["RegErrorMessage"] = "Panel Not Deleted..";
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return RedirectToAction("UpdatePanelList");
        }
        #endregion

        #region UpdatePanelData Get
        [AllowAnonymous]
        [HttpGet]
        public ActionResult UpdatePanelData(int? id = 0)
        {
            AdminRegisterModel model = new AdminRegisterModel();
            try
            {

                model = comMethods.profileDetails(id);
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return View(model);
        }
        #endregion

        #region UpdatePanelData Post
        [AllowAnonymous]
        [HttpPost]
        public ActionResult UpdatePanelData(AdminRegisterModel model)
        {
            try
            {
                adminBase.Id = model.Id;
                adminBase.UserGuid = Guid.NewGuid().ToString();
                adminBase.FirstName = model.FirstName;
                adminBase.LastName = model.LastName;
                adminBase.Email = model.Email;
                adminBase.Gender = model.Gender;
                adminBase.Address = model.Address;
                adminBase.CountryId = Convert.ToInt32(model.CountryId);
                adminBase.StateId = Convert.ToInt32(model.StateId);
                adminBase.City = model.City;
                adminBase.ZipCode = model.ZipCode;
                adminBase.SecurityQuestion = model.SecurityQuestion;
                adminBase.AnswerToSecurityQuestion = model.AnswerToSecurityQuestion;
                adminBase.CountryOfResidenceId = model.CountryOfResidenceId;
                adminBase.LanguageId = model.LanguageId;
                adminBase.CompanyName = model.CompanyName;
                adminBase.PhoneNumber = model.PhoneNumber;
                adminBase.SendEmail = model.sendEmail;
                adminBase.OtherCommunications = model.OtherCommunications;

                actionResult = AdminAction.Register_InsertUpdate(adminBase);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    int status = Convert.ToInt32(actionResult.dtResult.Rows[0][0]);
                    if (status > 0)
                    {
                        TempData["SuccessMessage"] = "Profile of " + model.FirstName + " is updated successfully.";
                    }
                    else
                    {
                        TempData["ErrorMessage"] = "Something went wrong.Please try again later";
                    }

                }
                else
                {
                    TempData["ErrorMessage"] = "Something went wrong.Please try again later";
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return RedirectToAction("UpdatePanelList");
        }
        #endregion

        #region PanelList Get
        [HttpGet]
        public ActionResult PanelList()
        {
            ManageAdminModel model = new ManageAdminModel();
            List<AdminModel> Adminlist = new List<AdminModel>();
            model.Adminlist = GetPanels();
            return View(model);
        }
        #endregion

        #region GetPanels List<AdminModel>
        public List<AdminModel> GetPanels()
        {
            ManageAdminModel model = new ManageAdminModel();
            List<AdminModel> Adminlist = new List<AdminModel>();
            model.Adminlist = Adminlist;
            try
            {
                adminBase.RoleId = 4;
                actionResult = adminAction.PanelList_LoadAll(adminBase);
                if (actionResult.IsSuccess)
                {
                    foreach (DataRow dr in actionResult.dtResult.Rows)
                    {
                        AdminModel adminModel = new AdminModel();
                        adminModel.Id = dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0;
                        adminModel.FirstName = dr["FirstName"] != DBNull.Value ? Convert.ToString(dr["FirstName"]) : string.Empty;
                        adminModel.LastName = dr["LastName"] != DBNull.Value ? Convert.ToString(dr["LastName"]) : string.Empty;
                        adminModel.Email = dr["Email"] != DBNull.Value ? Convert.ToString(dr["Email"]) : string.Empty;
                        adminModel.Status = dr["IsActive"] != DBNull.Value ? Convert.ToBoolean(dr["IsActive"]) : false;
                        adminModel.isdeleted = dr["IsDeleted"] != DBNull.Value ? Convert.ToBoolean(dr["IsDeleted"]) : false;
                        Adminlist.Add(adminModel);
                    }
                    model.Adminlist = Adminlist;
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return model.Adminlist;
        }
        #endregion

        #region CreatePanelRoles Get
        public ActionResult CreatePanelRoles()
        {
            ManageAdminModel model = new ManageAdminModel();
            CreateRolesModel createRole = new CreateRolesModel();
            model.createRoleModel = createRole;
            List<SelectListItem> userLst = new List<SelectListItem>();
            List<CreateRolesModel> roleList = new List<CreateRolesModel>();
            try
            {
                adminBase.RoleId = 4;
                actionResult = adminAction.PanelList_LoadAll(adminBase);
                if (actionResult.IsSuccess)
                {
                    userLst = actionResult.dtResult.AsEnumerable().Select(dr => new SelectListItem
                    {
                        Text = String.Format("{0} {1}", (dr["FirstName"] != DBNull.Value ? Convert.ToString(dr["FirstName"]) : string.Empty), (dr["LastName"] != DBNull.Value ? Convert.ToString(dr["LastName"]) : string.Empty)),
                        Value = dr["Id"] != DBNull.Value ? Convert.ToString(dr["Id"]) : string.Empty
                    }).ToList();
                }
                actionResult = superAction.Roles_LoadAll();
                if (actionResult.IsSuccess)
                {
                    roleList = actionResult.dtResult.AsEnumerable().Select(dr => new Models.CreateRolesModel
                    {
                        RoleId = dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0,
                        RoleName = dr["RoleName"] != DBNull.Value ? Convert.ToString(dr["RoleName"]) : string.Empty
                    }).ToList();
                }
                model.createRoleModel.UserList = userLst;
                model.roleList = roleList;
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return View(model);
        }
        #endregion

        #region CreatePanelRoles Post
        [HttpPost]
        public ActionResult CreatePanelRoles(ManageAdminModel model, FormCollection fc)
        {
            string RoleIds = fc["hdnRoleIds"] ?? string.Empty;
            string userName = fc["hdnUserName"] ?? string.Empty;
            string addEditCase = fc["hdnAddEdit"] ?? string.Empty;
            try
            {
                string UserId = String.Format("{0}{1}{2}", "<UserId>", model.createRoleModel.UserId, "</UserId>");
                string userInRole = string.Empty;

                if (addEditCase == "Add")
                {
                    ViewBag.Title = "Create Role";
                    if (!String.IsNullOrEmpty(RoleIds))
                    {
                        string[] rolesArray = RoleIds.TrimEnd(',').Split(',');
                        if (rolesArray != null && rolesArray.Length > 0)
                        {
                            for (int i = 0; i <= rolesArray.Length - 1; i++)
                            {
                                userInRole += String.Format("{0}{1}{2}{3}{4}", "<UserInRole>", UserId, "<RoleId>", rolesArray[i], "</RoleId></UserInRole>");
                            }
                        }
                        string xml = String.Format("{0}{1}{2}", "<Roles>", userInRole, "</Roles>");
                        createRoleBase.RoleXml = xml;
                        actionResult = superAction.InsertRoles_ByUserId(createRoleBase);
                        if (actionResult.IsSuccess)
                        {
                            int res = Convert.ToInt32(actionResult.dtResult.Rows[0][0]);
                            if (res > 0)
                                TempData["SuccessMessage"] = String.Format("{0}{1}{2}", "Role saved successfully for ", userName, ".");
                            else
                                TempData["ErrorMessage"] = String.Format("{0}{1}{2}", "Error in saving role for ", userName, ".");
                        }
                    }
                    else
                    {
                        TempData["ErrorMessage"] = String.Format("{0}{1}{2}", "Error in saving role.Please assign atleast one role for ", userName, ".");
                    }
                }
                else if (addEditCase == "Edit")
                {
                    ViewBag.Title = "Update Role";
                    string Ids = string.Empty;
                    if (!String.IsNullOrEmpty(RoleIds))
                    {
                        string[] rolesArray = RoleIds.TrimEnd(',').Split(',');
                        if (rolesArray != null && rolesArray.Length > 0)
                        {
                            for (int i = 0; i <= rolesArray.Length - 1; i++)
                            {
                                string[] seprateRoleIds = rolesArray[i].Split('-');
                                if (seprateRoleIds != null && seprateRoleIds.Length > 0)
                                {
                                    userInRole += String.Format("{0}{1}{2}{3}{4}{5}{6}", "<UserInRole><Id>", seprateRoleIds[1], "</Id>", UserId, "<RoleId>", seprateRoleIds[0], "</RoleId></UserInRole>");
                                }
                            }
                        }

                        string xml = String.Format("{0}{1}{2}", "<Roles>", userInRole, "</Roles>");
                        createRoleBase.RoleXml = xml;
                        actionResult = superAction.UpdateRoles_ById(createRoleBase);
                        if (actionResult.IsSuccess)
                            TempData["SuccessMessage"] = String.Format("{0}{1}{2}", "Role updated successfully for ", userName, ".");
                        else
                            TempData["ErrorMessage"] = String.Format("{0}{1}{2}", "Error in updating role for ", userName, ".");

                    }
                    else
                    {
                        TempData["ErrorMessage"] = String.Format("{0}{1}{2}", "Error in saving role.Please assign atleast one role for ", userName, ".");
                    }
                }

            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return RedirectToAction("CreatePanelRoles");
        }
        #endregion

        #region CreatePanel
        public ActionResult CreatePanel()
        {
            return View();
        }
        #endregion

        #region CreatePanel Post
        [HttpPost]
        public ActionResult CreatePanel(AdminModel model)
        {
            try
            {
                adminBase.UserGuid = Guid.NewGuid().ToString();
                adminBase.RoleId = 4;
                adminBase.FirstName = model.FirstName;
                adminBase.LastName = model.LastName;
                adminBase.Gender = model.Gender;
                adminBase.Email = model.Email;
                actionResult = superAction.CreateUser(adminBase);
                if (actionResult.IsSuccess)
                {
                    int status = Convert.ToInt32(actionResult.dtResult.Rows[0][0]);
                    if (status == -10)
                    {
                        TempData["RegErrorMessage"] = "Email address already exists, Please choose another email.";
                    }
                    else
                    {
                        Email.SendConfirmationEmailWithResetPassword(adminBase.Email, adminBase.UserGuid, adminBase.FirstName);
                        TempData["RegSuccessMessage"] = "Panel created successfully";
                    }
                }
                else
                {
                    TempData["RegErrorMessage"] = "Error occurs in registering panel";
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return RedirectToAction("CreatePanel");
        }
        #endregion

        //-------------End Panel Section

        //------------- Field Service Section ekta 11 May 2016------
        #region UpdateFieldServiceList
        [AllowAnonymous]
        [HttpGet]
        public ActionResult UpdateFieldServiceList()
        {
            ManageAdminModel model = new ManageAdminModel();
            List<AdminModel> Adminlist = new List<AdminModel>();
            model.Adminlist = Adminlist;
            try
            {
                if (Session["UserId"] != null)
                {
                    model.Adminlist = GetFieldServices();
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }

            return View(model);
        }
        #endregion

        #region DeleteFieldService
        public ActionResult DeleteFieldService(int id)
        {
            try
            {
                adminBase.Id = id != 0 ? id : id = 0;
                actionResult = superAction.AdminAccount_Delete(adminBase);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    //Email.SendConfirmationEmail(adminBase.Email, adminBase.UserGuid);
                    TempData["RegSuccessMessage"] = "You have successfully delete Field Service.";
                }
                else
                {
                    TempData["RegErrorMessage"] = "Field Service not deleted..";
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return RedirectToAction("UpdateFieldServiceList");
        }
        #endregion

        #region UpdateFieldServiceData Get
        [AllowAnonymous]
        [HttpGet]
        public ActionResult UpdateFieldServiceData(int? id = 0)
        {
            AdminRegisterModel model = new AdminRegisterModel();
            try
            {

                model = comMethods.profileDetails(id);
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return View(model);
        }
        #endregion

        #region UpdateFieldServiceData Post
        [AllowAnonymous]
        [HttpPost]
        public ActionResult UpdateFieldServiceData(AdminRegisterModel model)
        {
            try
            {
                adminBase.Id = model.Id;
                adminBase.UserGuid = Guid.NewGuid().ToString();
                adminBase.FirstName = model.FirstName;
                adminBase.LastName = model.LastName;
                adminBase.Email = model.Email;
                adminBase.Gender = model.Gender;
                adminBase.Address = model.Address;
                adminBase.CountryId = Convert.ToInt32(model.CountryId);
                adminBase.StateId = Convert.ToInt32(model.StateId);
                adminBase.City = model.City;
                adminBase.ZipCode = model.ZipCode;
                adminBase.SecurityQuestion = model.SecurityQuestion;
                adminBase.AnswerToSecurityQuestion = model.AnswerToSecurityQuestion;
                adminBase.CountryOfResidenceId = model.CountryOfResidenceId;
                adminBase.LanguageId = model.LanguageId;
                adminBase.CompanyName = model.CompanyName;
                adminBase.PhoneNumber = model.PhoneNumber;
                adminBase.SendEmail = model.sendEmail;
                adminBase.OtherCommunications = model.OtherCommunications;

                actionResult = AdminAction.Register_InsertUpdate(adminBase);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    int status = Convert.ToInt32(actionResult.dtResult.Rows[0][0]);
                    if (status > 0)
                    {
                        TempData["SuccessMessage"] = "Profile of " + model.FirstName + " is updated successfully.";
                    }
                    else
                    {
                        TempData["ErrorMessage"] = "Something went wrong.Please try again later";
                    }

                }
                else
                {
                    TempData["ErrorMessage"] = "Something went wrong.Please try again later";
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return RedirectToAction("UpdateFieldServiceList");
        }
        #endregion

        #region FieldServiceList Get
        [HttpGet]
        public ActionResult FieldServiceList()
        {
            ManageAdminModel model = new ManageAdminModel();
            List<AdminModel> Adminlist = new List<AdminModel>();
            model.Adminlist = GetFieldServices();
            return View(model);
        }
        #endregion

        #region GetFieldServices List<AdminModel>
        public List<AdminModel> GetFieldServices()
        {
            ManageAdminModel model = new ManageAdminModel();
            List<AdminModel> Adminlist = new List<AdminModel>();
            model.Adminlist = Adminlist;
            try
            {
                adminBase.RoleId = 5;
                actionResult = adminAction.PanelList_LoadAll(adminBase);
                if (actionResult.IsSuccess)
                {
                    foreach (DataRow dr in actionResult.dtResult.Rows)
                    {
                        AdminModel adminModel = new AdminModel();
                        adminModel.Id = dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0;
                        adminModel.FirstName = dr["FirstName"] != DBNull.Value ? Convert.ToString(dr["FirstName"]) : string.Empty;
                        adminModel.LastName = dr["LastName"] != DBNull.Value ? Convert.ToString(dr["LastName"]) : string.Empty;
                        adminModel.Email = dr["Email"] != DBNull.Value ? Convert.ToString(dr["Email"]) : string.Empty;
                        adminModel.Status = dr["IsActive"] != DBNull.Value ? Convert.ToBoolean(dr["IsActive"]) : false;
                        adminModel.isdeleted = dr["IsDeleted"] != DBNull.Value ? Convert.ToBoolean(dr["IsDeleted"]) : false;
                        Adminlist.Add(adminModel);
                    }
                    model.Adminlist = Adminlist;
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return model.Adminlist;
        }
        #endregion

        #region CreateFieldServiceRoles Get
        public ActionResult CreateFieldServiceRoles()
        {
            ManageAdminModel model = new ManageAdminModel();
            CreateRolesModel createRole = new CreateRolesModel();
            model.createRoleModel = createRole;
            List<SelectListItem> userLst = new List<SelectListItem>();
            List<CreateRolesModel> roleList = new List<CreateRolesModel>();
            try
            {
                adminBase.RoleId = 4;
                actionResult = adminAction.PanelList_LoadAll(adminBase);
                if (actionResult.IsSuccess)
                {
                    userLst = actionResult.dtResult.AsEnumerable().Select(dr => new SelectListItem
                    {
                        Text = String.Format("{0} {1}", (dr["FirstName"] != DBNull.Value ? Convert.ToString(dr["FirstName"]) : string.Empty), (dr["LastName"] != DBNull.Value ? Convert.ToString(dr["LastName"]) : string.Empty)),
                        Value = dr["Id"] != DBNull.Value ? Convert.ToString(dr["Id"]) : string.Empty
                    }).ToList();
                }
                actionResult = superAction.Roles_LoadAll();
                if (actionResult.IsSuccess)
                {
                    roleList = actionResult.dtResult.AsEnumerable().Select(dr => new Models.CreateRolesModel
                    {
                        RoleId = dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0,
                        RoleName = dr["RoleName"] != DBNull.Value ? Convert.ToString(dr["RoleName"]) : string.Empty
                    }).ToList();
                }
                model.createRoleModel.UserList = userLst;
                model.roleList = roleList;
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return View(model);
        }
        #endregion

        #region CreateFieldServiceRoles Post
        [HttpPost]
        public ActionResult CreateFieldServiceRoles(ManageAdminModel model, FormCollection fc)
        {
            string RoleIds = fc["hdnRoleIds"] ?? string.Empty;
            string userName = fc["hdnUserName"] ?? string.Empty;
            string addEditCase = fc["hdnAddEdit"] ?? string.Empty;
            try
            {
                string UserId = String.Format("{0}{1}{2}", "<UserId>", model.createRoleModel.UserId, "</UserId>");
                string userInRole = string.Empty;

                if (addEditCase == "Add")
                {
                    ViewBag.Title = "Create Role";
                    if (!String.IsNullOrEmpty(RoleIds))
                    {
                        string[] rolesArray = RoleIds.TrimEnd(',').Split(',');
                        if (rolesArray != null && rolesArray.Length > 0)
                        {
                            for (int i = 0; i <= rolesArray.Length - 1; i++)
                            {
                                userInRole += String.Format("{0}{1}{2}{3}{4}", "<UserInRole>", UserId, "<RoleId>", rolesArray[i], "</RoleId></UserInRole>");
                            }
                        }
                        string xml = String.Format("{0}{1}{2}", "<Roles>", userInRole, "</Roles>");
                        createRoleBase.RoleXml = xml;
                        actionResult = superAction.InsertRoles_ByUserId(createRoleBase);
                        if (actionResult.IsSuccess)
                        {
                            int res = Convert.ToInt32(actionResult.dtResult.Rows[0][0]);
                            if (res > 0)
                                TempData["SuccessMessage"] = String.Format("{0}{1}{2}", "Role saved successfully for ", userName, ".");
                            else
                                TempData["ErrorMessage"] = String.Format("{0}{1}{2}", "Error in saving role for ", userName, ".");
                        }
                    }
                    else
                    {
                        TempData["ErrorMessage"] = String.Format("{0}{1}{2}", "Error in saving role.Please assign atleast one role for ", userName, ".");
                    }
                }
                else if (addEditCase == "Edit")
                {
                    ViewBag.Title = "Update Role";
                    string Ids = string.Empty;
                    if (!String.IsNullOrEmpty(RoleIds))
                    {
                        string[] rolesArray = RoleIds.TrimEnd(',').Split(',');
                        if (rolesArray != null && rolesArray.Length > 0)
                        {
                            for (int i = 0; i <= rolesArray.Length - 1; i++)
                            {
                                string[] seprateRoleIds = rolesArray[i].Split('-');
                                if (seprateRoleIds != null && seprateRoleIds.Length > 0)
                                {
                                    userInRole += String.Format("{0}{1}{2}{3}{4}{5}{6}", "<UserInRole><Id>", seprateRoleIds[1], "</Id>", UserId, "<RoleId>", seprateRoleIds[0], "</RoleId></UserInRole>");
                                }
                            }
                        }

                        string xml = String.Format("{0}{1}{2}", "<Roles>", userInRole, "</Roles>");
                        createRoleBase.RoleXml = xml;
                        actionResult = superAction.UpdateRoles_ById(createRoleBase);
                        if (actionResult.IsSuccess)
                            TempData["SuccessMessage"] = String.Format("{0}{1}{2}", "Role updated successfully for ", userName, ".");
                        else
                            TempData["ErrorMessage"] = String.Format("{0}{1}{2}", "Error in updating role for ", userName, ".");

                    }
                    else
                    {
                        TempData["ErrorMessage"] = String.Format("{0}{1}{2}", "Error in saving role.Please assign atleast one role for ", userName, ".");
                    }
                }

            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return RedirectToAction("CreateFieldServiceRoles");
        }
        #endregion

        #region CreateFieldService
        public ActionResult CreateFieldService()
        {
            return View();
        }
        #endregion

        #region CreateFieldService Post
        [HttpPost]
        public ActionResult CreateFieldService(AdminModel model)
        {
            try
            {
                adminBase.UserGuid = Guid.NewGuid().ToString();
                adminBase.RoleId = 5;
                adminBase.FirstName = model.FirstName;
                adminBase.LastName = model.LastName;
                adminBase.Gender = model.Gender;
                adminBase.Email = model.Email;
                actionResult = superAction.CreateUser(adminBase);
                if (actionResult.IsSuccess)
                {
                    int status = Convert.ToInt32(actionResult.dtResult.Rows[0][0]);
                    if (status == -10)
                    {
                        TempData["RegErrorMessage"] = "Email address already exists, Please choose another email.";
                    }
                    else
                    {
                        Email.SendConfirmationEmailWithResetPassword(adminBase.Email, adminBase.UserGuid, adminBase.FirstName);
                        TempData["RegSuccessMessage"] = "Field service created successfully";
                    }
                }
                else
                {
                    TempData["RegErrorMessage"] = "Error occurs in registering field service";
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return RedirectToAction("CreateFieldService");
        }
        #endregion

        //-------------End Field Service Section
    }
}
