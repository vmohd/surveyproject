﻿using SRV.ActionLayer.Admin;
using SRV.ActionLayer.Respondent;
using SRV.BaseLayer.Admin;
using SRV.BaseLayer.Respondent;
using SRV.Utility;
using SurveyProject.Helper;
using SurveyProject.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace SurveyProject.Controllers
{
    public class ParallelTaskController : Controller
    {
        #region Declaration
        QuestionBase questionBase = new QuestionBase();
        SRV.BaseLayer.ActionResult actionResult = new SRV.BaseLayer.ActionResult();
        AdminAction adminAction = new AdminAction();
        CommonMethods comMethods = new CommonMethods();
        RespondentAction respondentAction = new RespondentAction();
        ProjectBase projectBase = new ProjectBase();
        #endregion
        public ActionResult Index()
        {
            //ResponseAsync();
            return View();
        }



        //#region  ResponseAsync
       
        //public void ResponseAsync()
        //{
        //    int RespCount = 0;

        //    SRV.BaseLayer.ActionResult actionResultProjects = new SRV.BaseLayer.ActionResult();
        //    AdminAction adminAction = new AdminAction();

        //    actionResultProjects = adminAction.Project_Load();
        //    if (actionResultProjects.dtResult != null && actionResultProjects.dtResult.Rows.Count > 0)
        //    {
        //        Task[] offlineTask = new Task[actionResultProjects.dtResult.Rows.Count];
        //        Task[] onlineTask = new Task[actionResultProjects.dtResult.Rows.Count];
        //        Task[] combinedTask = new Task[actionResultProjects.dtResult.Rows.Count];
        //        int i = 0;

        //        foreach (DataRow dr in actionResultProjects.dtResult.Rows)
        //        {
        //            int projectId = dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0;
        //            string projectGUID = dr["ProjectGUID"] != DBNull.Value ? dr["ProjectGUID"].ToString() : "";

        //            offlineTask[i] = Task.Factory.StartNew(() =>
        //            {
        //                GetReportDataTableOffline(projectGUID, (RespCount + 1), projectId);
        //            });
        //            offlineTask[i].Wait();

        //            onlineTask[i] = Task.Factory.StartNew(() =>
        //            {
        //                GetReportDataTableOnline(projectGUID, projectId);
        //            });
        //            onlineTask[i].Wait();

        //            combinedTask[i] = Task.Factory.StartNew(() =>
        //            {
        //                GetReportDataTableCombined(projectGUID, projectId);
        //            });
        //            combinedTask[i].Wait();
        //            i++;
        //        }
        //    }
        //}
        //#endregion

        //#region GetReportDataTableCombined
       
        //public void GetReportDataTableCombined(string projectGUID, int projectId)
        //{
        //    int RespCount = 0;
        //    DataTable dtOnline = new DataTable();
        //    DataTable dtOffline = new DataTable();
        //    dtOnline = GetRepOnline(projectGUID, projectId);
        //    RespCount = (from DataRow dRow in dtOnline.Rows select dRow["Resp_Id"]).Distinct().Count();
        //    dtOffline = GetRepOffline(projectGUID, (RespCount + 1), projectId);
        //    dtOnline.Merge(dtOffline);

        //    if (dtOnline.Rows.Count > 0)
        //    {
        //        string responseXML = ConvertDatatableToXML(dtOnline);
        //        actionResult = adminAction.CombinedProjectRespose_Add(projectId, projectGUID, responseXML);
        //    }
        //}
        //#endregion

        //#region GetReportDataTableOnline
       
        //public void GetReportDataTableOnline(string Id, int proId)
        //{
        //    RespondentController respondent = new RespondentController();
        //    SurveyResponseModel model = new SurveyResponseModel();
        //    DataTable dtF = new DataTable();
        //    DataTable newData = new DataTable();
        //    try
        //    {
        //        DataTable dt = new DataTable();

        //        string projectGuid = string.Empty;
        //        projectGuid = Id;
        //        dt.Columns.Add("Resp_Id", typeof(string));

        //        model = respondent.GetProjectPreview(projectGuid);

        //        //--ss
        //        if (model.projectModel.ProjectType == "2")
        //        {
        //            DataRow drnew = dt.NewRow();
        //            drnew[0] = "Product_LogicalId";
        //            dt.Rows.Add(drnew);
        //            DataRow drnew1 = dt.NewRow();
        //            drnew1[0] = "Product_Name";
        //            dt.Rows.Add(drnew1);
        //        }
        //        //--

        //        // Load all the questions of this project
        //        List<QuestionList> questionsList = new List<QuestionList>();
        //        questionBase.ProjectId = Convert.ToInt32(model.projectModel.Id);
        //        actionResult = adminAction.QuestionsLoadAll_ByProjectId(questionBase);
        //        if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
        //        {
        //            foreach (DataRow dr in actionResult.dtResult.Rows)
        //            {
        //                questionsList.Add(new QuestionList
        //                {
        //                    id = dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0,
        //                    questionTypeId = dr["QuestionTypeId"] != DBNull.Value ? Convert.ToInt32(dr["QuestionTypeId"]) : 0
        //                });
        //            }
        //        }

        //        string fileName = Guid.NewGuid().ToString().Substring(0, 5) + "_" + DateTime.Now.ToString("MM-dd-yyyy") + "_" + model.projectModel.ProjectName.Replace(" ", "");

        //        if (model.lstQuestionModel != null && model.lstQuestionModel.Count > 0)
        //        {
        //            foreach (var item in model.lstQuestionModel)
        //            {
        //                if (item.PipedFrom > 0 && (!item.QuestionTitle.Contains("{{Q}}")))
        //                {
        //                    if (item.QuestionTypeId == 2 || item.QuestionTypeId == 4 || item.QuestionTypeId == 5 || item.QuestionTypeId == 6)//Multi Punch || Constant Sum || Ranking || Matrix Type
        //                    {
        //                        if (model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel != null && model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel.Count > 0)
        //                        {
        //                            foreach (var opt in model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel)
        //                            {
        //                                DataRow drnew = dt.NewRow();
        //                                drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
        //                                dt.Rows.Add(drnew);
        //                            }
        //                        }
        //                    }

        //                    else if (item.QuestionTypeId == 1 || item.QuestionTypeId == 3 || item.QuestionTypeId == 7 || item.QuestionTypeId == 15)//Single Punch || Range || Simple Text
        //                    {
        //                        DataRow drnew = dt.NewRow();
        //                        drnew[0] = "Q" + item.QuestionId + "_S" + "-" + ReplaceHtml.ReplaceText(item.QuestionTitle);
        //                        dt.Rows.Add(drnew);
        //                    }
        //                    else if (item.QuestionTypeId == 8 || item.QuestionTypeId == 12)//Single punch matrix type  && Rank order type
        //                    {
        //                        //if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
        //                        //{
        //                        //foreach (var opt in item.lstOptionsModel)
        //                        //{
        //                        //    DataRow drnew = dt.NewRow();
        //                        //    //drnew[0] = "Q" + item.QuestionId + "_M" + opt.OptionId + "-" + ReplaceHtml.ReplaceText(opt.Option);
        //                        //    drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
        //                        //    dt.Rows.Add(drnew);
        //                        //}
        //                        //}
        //                        if (item.lstMatrixTypeModel != null && item.lstMatrixTypeModel.Count > 0)
        //                        {
        //                            foreach (var col in item.lstMatrixTypeModel)
        //                            {
        //                                DataRow drnew = dt.NewRow();
        //                                //drnew[0] = "Q" + item.QuestionId + "_M" + opt.OptionId + "-" + ReplaceHtml.ReplaceText(opt.Option);
        //                                drnew[0] = "Q" + item.QuestionId + "_M" + col.DisplayOrder + "-" + ReplaceHtml.ReplaceText(col.ColumnName);
        //                                dt.Rows.Add(drnew);
        //                            }
        //                        }
        //                    }
        //                    else if (item.QuestionTypeId == 13)//Kano model matrix type 
        //                    {
        //                        if (model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel != null && model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel.Count > 0)
        //                        {
        //                            foreach (var opt in model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel)
        //                            {
        //                                DataRow drnew = dt.NewRow();
        //                                drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Present";
        //                                dt.Rows.Add(drnew);
        //                                DataRow drnew1 = dt.NewRow();
        //                                drnew1[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Absent";
        //                                dt.Rows.Add(drnew1);
        //                            }
        //                        }
        //                    }
        //                    // Single Punch Matrix Type || Range Matrix Type Question ||
        //                    // Single punch type answer horizontal position || 
        //                    // Multiple punch type answer horizontal position || 
        //                    // Rank order type question || 
        //                    // KANO Model Matrix type question || Dropdown Matrix type question
        //                    else if (item.QuestionTypeId > 7 && item.QuestionTypeId != 15)
        //                    {
        //                        if (model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel != null && model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel.Count > 0)
        //                        {
        //                            foreach (var opt in model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel)
        //                            {
        //                                foreach (var column in item.lstMatrixTypeModel)
        //                                {
        //                                    DataRow drnew = dt.NewRow();
        //                                    drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "_C" + column.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + " " + ReplaceHtml.ReplaceText(column.ColumnName);
        //                                    dt.Rows.Add(drnew);
        //                                }
        //                            }
        //                        }
        //                    }
        //                }
        //                else
        //                {
        //                    if (item.QuestionTypeId == 2 || item.QuestionTypeId == 4 || item.QuestionTypeId == 5 || item.QuestionTypeId == 6)//Multi Punch || Constant Sum || Ranking || Matrix Type
        //                    {
        //                        if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
        //                        {
        //                            foreach (var opt in item.lstOptionsModel)
        //                            {
        //                                DataRow drnew = dt.NewRow();
        //                                //drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
        //                                drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
        //                                dt.Rows.Add(drnew);
        //                            }
        //                        }
        //                    }

        //                    else if (item.QuestionTypeId == 1 || item.QuestionTypeId == 3 || item.QuestionTypeId == 7 || item.QuestionTypeId == 15)//Single Punch || Range || Simple Text
        //                    {
        //                        DataRow drnew = dt.NewRow();
        //                        //drnew[0] = "Q" + item.DisplayOrder + "_S" + "-" + ReplaceHtml.ReplaceText(item.QuestionTitle);
        //                        drnew[0] = "Q" + item.QuestionId + "_S" + "-" + ReplaceHtml.ReplaceText(item.QuestionTitle);
        //                        dt.Rows.Add(drnew);
        //                    }
        //                    else if (item.QuestionTypeId == 8 || item.QuestionTypeId == 12)//Single punch matrix type  && Rank order type
        //                    {
        //                        //if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
        //                        //{
        //                        //foreach (var opt in item.lstOptionsModel)
        //                        //{
        //                        //    DataRow drnew = dt.NewRow();
        //                        //    //drnew[0] = "Q" + item.QuestionId + "_M" + opt.OptionId + "-" + ReplaceHtml.ReplaceText(opt.Option);
        //                        //    drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
        //                        //    dt.Rows.Add(drnew);
        //                        //}
        //                        //}
        //                        if (item.lstMatrixTypeModel != null && item.lstMatrixTypeModel.Count > 0)
        //                        {
        //                            foreach (var col in item.lstMatrixTypeModel)
        //                            {
        //                                DataRow drnew = dt.NewRow();
        //                                //drnew[0] = "Q" + item.QuestionId + "_M" + opt.OptionId + "-" + ReplaceHtml.ReplaceText(opt.Option);
        //                                drnew[0] = "Q" + item.QuestionId + "_M" + col.DisplayOrder + "-" + ReplaceHtml.ReplaceText(col.ColumnName);
        //                                dt.Rows.Add(drnew);
        //                            }
        //                        }
        //                    }
        //                    else if (item.QuestionTypeId == 13)//Kano model matrix type 
        //                    {
        //                        if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
        //                        {
        //                            foreach (var opt in item.lstOptionsModel)
        //                            {
        //                                DataRow drnew = dt.NewRow();
        //                                //drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Present";
        //                                drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Present";
        //                                dt.Rows.Add(drnew);
        //                                DataRow drnew1 = dt.NewRow();
        //                                //drnew1[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Absent";
        //                                drnew1[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Absent";
        //                                dt.Rows.Add(drnew1);
        //                            }
        //                        }
        //                    }
        //                    // Single Punch Matrix Type || Range Matrix Type Question ||
        //                    // Single punch type answer horizontal position || 
        //                    // Multiple punch type answer horizontal position || 
        //                    // Rank order type question || 
        //                    // KANO Model Matrix type question || Dropdown Matrix type question
        //                    else if (item.QuestionTypeId > 7)
        //                    {
        //                        if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
        //                        {
        //                            foreach (var opt in item.lstOptionsModel)
        //                            {
        //                                foreach (var column in item.lstMatrixTypeModel)
        //                                {
        //                                    DataRow drnew = dt.NewRow();
        //                                    //drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "_C" + column.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + " " + ReplaceHtml.ReplaceText(column.ColumnName);
        //                                    drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "_C" + column.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + " " + ReplaceHtml.ReplaceText(column.ColumnName);
        //                                    dt.Rows.Add(drnew);
        //                                }
        //                            }
        //                        }
        //                    }
        //                }
        //            }
        //        }
        //        // Get the Excel Pattern of dt
        //        dtF = comMethods.GenerateTransposedTable(dt);

        //        // Get all the reponders
        //        projectBase.Id = model.projectModel.Id;
        //        actionResult = adminAction.CompleteRespondersLoad_ByProjectId(projectBase);
        //        List<RespondersModel> lstResponders = new List<RespondersModel>();
        //        foreach (DataRow dr in actionResult.dtResult.Rows)
        //        {
        //            lstResponders.Add(new RespondersModel
        //            {

        //                InvitationGuid = dr["InvitationGuid"] != DBNull.Value ? Convert.ToString(dr["InvitationGuid"]) : "",
        //                Email = dr["Email"] != DBNull.Value ? Convert.ToString(dr["Email"]) : "",

        //            });
        //        }
        //        IEnumerable<RespondersModel> filteredList = lstResponders
        //         .GroupBy(customer => customer.InvitationGuid).Select(group => group.First());
        //        // Get Responses
        //        SurveyLoadBase surveyLoadBase = new SurveyLoadBase();
        //        string json = string.Empty;
        //        SRV.BaseLayer.ActionResult actionResultResponses = new SRV.BaseLayer.ActionResult();
        //        int response_id = 1;            //ss
        //        foreach (var responder in filteredList)
        //        {
        //            DataTable responsesDt = new DataTable();
        //            surveyLoadBase.Id = model.projectModel.Id;
        //            surveyLoadBase.Email = (!string.IsNullOrEmpty(responder.Email) ? Convert.ToString(responder.Email) : "");
        //            surveyLoadBase.ExternalResponderId = (!string.IsNullOrEmpty(responder.InvitationGuid) ? Convert.ToString(responder.InvitationGuid) : "");
        //            if (model.projectModel.lstProductProjectModel != null && model.projectModel.lstProductProjectModel.Count > 0)
        //            {
        //                foreach (var productModel in model.projectModel.lstProductProjectModel)
        //                {
        //                    surveyLoadBase.ProductId = productModel.Id;
        //                    actionResultResponses = respondentAction.Response_LoadByProjectId(surveyLoadBase);
        //                    if (actionResultResponses.IsSuccess)
        //                    {
        //                        if (responsesDt.Rows.Count == 0)
        //                            responsesDt = actionResultResponses.dtResult.Copy();
        //                        else
        //                        {
        //                            foreach (DataRow drThisRespRow in actionResultResponses.dtResult.Rows)
        //                                responsesDt.ImportRow(drThisRespRow);
        //                        }
        //                    }
        //                }
        //            }
        //            else
        //            {
        //                actionResultResponses = respondentAction.Response_LoadByProjectId(surveyLoadBase);
        //                if (actionResultResponses.IsSuccess)
        //                    responsesDt = actionResultResponses.dtResult.Copy();

        //            }

        //            // surveyLoadBase.OfflineResponderId = dr["RespId"] != DBNull.Value ? Convert.ToString(dr["RespId"]) : "";
        //            //DataRow dr_dtF = dtF.NewRow();
        //            // if (String.IsNullOrEmpty(surveyLoadBase.Email)) continue;

        //            //actionResultResponses = respondentAction.Response_LoadByProjectId(surveyLoadBase);
        //            if (responsesDt.Rows.Count > 0)//actionResultResponses.IsSuccess
        //            {
        //                //dr_dtF[0] = surveyLoadBase.Email;

        //                int count = 1;
        //                count = (model.projectModel.ProjectType == "2") ? count + 2 : count;        //ss
        //                DataRow dr_dtF = dtF.NewRow();
        //                DataRow drTemp = dtF.NewRow();
        //                string product_desc = string.Empty;
        //                string product_logiId = string.Empty;
        //                int oldProductID = responsesDt.Rows[0]["ProductId"] != DBNull.Value ? Convert.ToInt32(responsesDt.Rows[0]["ProductId"]) : 0;
        //                foreach (DataRow drResponse in responsesDt.Rows)// actionResultResponses.dtResult.Rows
        //                {

        //                    int optionsCount = 0;
        //                    // response_id = drResponse["ReponderId"] != DBNull.Value ? (responsesDt.Rows[0]["ReponderId"]).ToString() : "";
        //                    int productId = drResponse["ProductId"] != DBNull.Value ? Convert.ToInt32(drResponse["ProductId"]) : 0;
        //                    if (productId != oldProductID)
        //                    {
        //                        count = (model.projectModel.ProjectType == "2") ? 3 : 1;
        //                        dr_dtF = dtF.NewRow();
        //                    }

        //                    //--ss

        //                    product_desc = drResponse["ProductDescription"] != DBNull.Value ? Convert.ToString(drResponse["ProductDescription"]) : "";
        //                    product_logiId = drResponse["ProductLogicalId"] != DBNull.Value ? Convert.ToString(drResponse["ProductLogicalId"]) : "";

        //                    //--ss
        //                    int questionId = drResponse["QuestionId"] != DBNull.Value ? Convert.ToInt32(drResponse["QuestionId"]) : 0;
        //                    string answerExpression = drResponse["AnswerExpression"] != DBNull.Value ? Convert.ToString(drResponse["AnswerExpression"]) : "";
        //                    int questionTypeId = drResponse["QuestionTypeId"] != DBNull.Value ? Convert.ToInt32(drResponse["QuestionTypeId"]) : 1;
        //                    int PipedFrom = drResponse["PipedFrom"] != DBNull.Value ? Convert.ToInt32(drResponse["PipedFrom"]) : 0;
        //                    //dr_dtF[0] = drResponse;

        //                    // Get Total options count for this question
        //                    questionBase.Id = Convert.ToInt32(questionId);
        //                    SRV.BaseLayer.ActionResult actionResultAnswersTemp = new SRV.BaseLayer.ActionResult();
        //                    actionResultAnswersTemp = adminAction.Option_LoadAllById(questionBase);
        //                    if (actionResultAnswersTemp.dtResult != null && actionResultAnswersTemp.dtResult.Rows.Count > 0)
        //                    {
        //                        optionsCount = actionResultAnswersTemp.dtResult.Rows.Count;
        //                    }

        //                    if (questionTypeId == 2 || questionTypeId == 4 || questionTypeId == 5 || questionTypeId == 6)
        //                    {
        //                        string[] allAnswers = answerExpression.Split('|');
        //                        if (allAnswers.Length > 0)
        //                        {
        //                            if (questionTypeId == 2)// Multi Punch
        //                            {
        //                                string allRec = string.Empty;
        //                                List<OptionsModel> matrixAns = new List<OptionsModel>();
        //                                if (PipedFrom > 0 && !drResponse["QuestionTitle"].ToString().Contains("{{Q}}"))
        //                                {
        //                                    matrixAns = model.lstQuestionModel.Where(v => v.QuestionId == PipedFrom).FirstOrDefault().lstOptionsModel;
        //                                }
        //                                else
        //                                {
        //                                    matrixAns = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
        //                                }

        //                                foreach (var item in matrixAns)
        //                                {
        //                                    string matchedId = string.Empty;
        //                                    foreach (string ans in allAnswers)
        //                                    {
        //                                        if (Convert.ToString(item.OptionId) == ans)
        //                                        {
        //                                            matchedId = Convert.ToString(item.DisplayOrder);
        //                                            break;
        //                                        }
        //                                    }
        //                                    allRec += matchedId + "|";
        //                                }

        //                                string[] allAnswersMat = allRec.Substring(0, allRec.Length - 1).Split('|');
        //                                foreach (string ans in allAnswersMat)
        //                                {

        //                                    if (count <= dtF.Columns.Count - 1)
        //                                    {
        //                                        if ((Regex.Replace(dtF.Columns[count].ToString().Split('-')[0].Split('_')[1], @"[^\d]", String.Empty)) == ans)
        //                                        {
        //                                            dr_dtF[count] = "1";// ans;
        //                                            count++;
        //                                            optionsCount--;
        //                                        }
        //                                        else
        //                                        {
        //                                            dr_dtF[count] = "2";
        //                                            count++;
        //                                            optionsCount--;
        //                                        }
        //                                    }
        //                                }
        //                                //count = count + optionsCount;
        //                            }
        //                            else if (questionTypeId == 6) // matrix Type
        //                            {
        //                                foreach (string ans in allAnswers)
        //                                {
        //                                    dr_dtF[count] = (ans.ToLower().IndexOf("yes") != -1) ? "1" : "2";
        //                                    count++;
        //                                    optionsCount--;
        //                                }
        //                                count = count + optionsCount;
        //                            }
        //                            else
        //                            {
        //                                foreach (string ans in allAnswers)
        //                                {
        //                                    dr_dtF[count] = ans;
        //                                    count++;
        //                                    optionsCount--;
        //                                }
        //                                count = count + optionsCount;
        //                            }
        //                        }
        //                    }
        //                    else if (questionTypeId == 1) // Single Punch Answers
        //                    {
        //                        //List<OptionsModel> SinglePunchAns = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
        //                        List<OptionsModel> SinglePunchAns = new List<OptionsModel>();
        //                        if (PipedFrom > 0 && !drResponse["QuestionTitle"].ToString().Contains("{{Q}}"))
        //                        {
        //                            SinglePunchAns = model.lstQuestionModel.Where(v => v.QuestionId == PipedFrom).FirstOrDefault().lstOptionsModel;
        //                        }
        //                        else
        //                        {
        //                            SinglePunchAns = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
        //                        }
        //                        int ansIndex = 0;
        //                        foreach (var item in SinglePunchAns)
        //                        {
        //                            ansIndex++;
        //                            if (Convert.ToString(item.OptionId) == answerExpression)
        //                            {
        //                                break;
        //                            }
        //                        }

        //                        dr_dtF[count] = ansIndex;//answerExpression;
        //                        count++;
        //                        optionsCount--;
        //                    }
        //                    else if (questionTypeId == 8 || questionTypeId == 12) // single punch matrix answers
        //                    {
        //                        List<OptionsModel> optionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
        //                        List<MatrixTypeModel> columnDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstMatrixTypeModel;
        //                        int qustionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).Select(v => v.DisplayOrder).FirstOrDefault();
        //                        string[] allAnswers = answerExpression.Split('|');

        //                        List<string> lstColumnOrdinals = new List<string>();

        //                        string formattedAnswer = string.Empty;
        //                        string answerValue = string.Empty;
        //                        if (allAnswers.Length > 0)
        //                        {
        //                            foreach (var ansExpression in allAnswers)
        //                            {
        //                                string displaycol = string.Empty;
        //                                string displayopt = string.Empty;
        //                                foreach (var item in optionDisplayOrder)
        //                                {

        //                                    if (Convert.ToString(item.OptionId) == ansExpression.Split('~')[0])
        //                                    {
        //                                        displaycol = Convert.ToString(item.DisplayOrder);
        //                                        break;
        //                                    }
        //                                }
        //                                foreach (var item in columnDisplayOrder)
        //                                {
        //                                    if (Convert.ToString(item.Id) == ansExpression.Split('~')[1])
        //                                    {
        //                                        displayopt = Convert.ToString(item.DisplayOrder);
        //                                        break;
        //                                    }
        //                                }
        //                                //  formattedAnswer = "Q" + questionId + "_M" + displaycol + "#" + displayopt;
        //                                formattedAnswer = "Q" + questionId + "_M" + displayopt + "#" + displaycol;
        //                                foreach (DataColumn column in dtF.Columns)
        //                                {
        //                                    string[] columnHeader = column.ColumnName.Split('-');
        //                                    string[] answer = columnHeader[0].Split('_');
        //                                    if (columnHeader.Length > 1)
        //                                    {
        //                                        string answerInHeader = answer[0] + "_" + answer[1];
        //                                        string[] formattedAnswerArr = formattedAnswer.Split('#');
        //                                        if (formattedAnswerArr[0] == answerInHeader)
        //                                        {
        //                                            int index = dr_dtF.Table.Columns[column.ColumnName].Ordinal;
        //                                            lstColumnOrdinals.Add(index.ToString());
        //                                            count++;
        //                                            dr_dtF[index] = formattedAnswerArr[1];
        //                                        }
        //                                    }
        //                                }
        //                            }
        //                        }
        //                    }
        //                    else if (questionTypeId == 13) // Kano model matrix type
        //                    {
        //                        List<OptionsModel> optionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
        //                        List<MatrixTypeModel> columnDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstMatrixTypeModel;
        //                        int qustionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).Select(v => v.DisplayOrder).FirstOrDefault();
        //                        string[] allAnswers = answerExpression.Split('|');

        //                        List<string> lstColumnOrdinals = new List<string>();

        //                        string formattedAnswer = string.Empty;
        //                        string answerValue = string.Empty;
        //                        if (allAnswers.Length > 1)
        //                        {
        //                            int repeatId = 0;
        //                            foreach (var ansExpression in allAnswers)
        //                            {
        //                                string displaycol = string.Empty;
        //                                string displayopt = string.Empty;
        //                                string displayText = string.Empty;
        //                                string text = string.Empty;
        //                                if (repeatId == Convert.ToInt32(ansExpression.Split('~')[0]))
        //                                {
        //                                    text = "_Absent";
        //                                    repeatId = 0;
        //                                }
        //                                else
        //                                {
        //                                    text = "_Present";
        //                                }
        //                                foreach (var item in optionDisplayOrder)
        //                                {
        //                                    if (Convert.ToString(item.OptionId) == ansExpression.Split('~')[0])
        //                                    {
        //                                        displayText = Convert.ToString(item.Option) + text;
        //                                        displaycol = Convert.ToString(item.DisplayOrder);
        //                                        repeatId = Convert.ToInt32(ansExpression.Split('~')[0]);
        //                                        break;
        //                                    }
        //                                }
        //                                foreach (var item in columnDisplayOrder)
        //                                {
        //                                    if (Convert.ToString(item.Id) == ansExpression.Split('~')[1])
        //                                    {
        //                                        displayopt = Convert.ToString(item.DisplayOrder);
        //                                        break;
        //                                    }
        //                                }
        //                                //formattedAnswer = "Q" + qustionDisplayOrder + "_M" + displaycol + "-" + displayText + "#" + displayopt;
        //                                formattedAnswer = "Q" + questionId + "_M" + displaycol + "-" + displayText + "#" + displayopt;
        //                                foreach (DataColumn column in dtF.Columns)
        //                                {

        //                                    string[] columnHeader = column.ColumnName.Split('-');
        //                                    string[] answer = columnHeader[0].Split('_');
        //                                    if (columnHeader.Length > 1)
        //                                    {
        //                                        string answerInHeader = answer[0] + "_" + answer[1] + "-" + columnHeader[1];
        //                                        string[] formattedAnswerArr = formattedAnswer.Split('#');
        //                                        if (Regex.Replace(formattedAnswerArr[0], @"\s+", "", RegexOptions.Multiline) == Regex.Replace(answerInHeader, @"\s+", "", RegexOptions.Multiline))
        //                                        {
        //                                            int index = dr_dtF.Table.Columns[column.ColumnName].Ordinal;
        //                                            lstColumnOrdinals.Add(index.ToString());
        //                                            count++;
        //                                            dr_dtF[index] = formattedAnswerArr[1];
        //                                        }
        //                                    }
        //                                }
        //                            }
        //                        }
        //                    }
        //                    else if (questionTypeId > 7 && questionTypeId != 15)
        //                    {
        //                        List<OptionsModel> optionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
        //                        List<MatrixTypeModel> columnDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstMatrixTypeModel;
        //                        List<DDLMatrixTypeModel> dropDownDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstDDlMatrixModel;
        //                        count = count + (optionDisplayOrder.Count * columnDisplayOrder.Count);
        //                        int qustionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).Select(v => v.DisplayOrder).FirstOrDefault();
        //                        string[] allAnswers = answerExpression.Split('|');

        //                        List<string> lstColumnOrdinals = new List<string>();

        //                        string formattedAnswer = string.Empty;
        //                        string answerValue = string.Empty;
        //                        int DDlDisplayOrder = 0;
        //                        if (allAnswers.Length > 1)
        //                        {
        //                            foreach (var ansExpression in allAnswers)
        //                            {
        //                                string displaycol = string.Empty;
        //                                string displayopt = string.Empty;

        //                                if (questionTypeId == 9 || questionTypeId == 14)
        //                                {
        //                                    foreach (var item in optionDisplayOrder)
        //                                    {
        //                                        if (Convert.ToString(item.OptionId) == ansExpression.Split('~')[0].Split('*')[0])
        //                                        {
        //                                            displaycol = Convert.ToString(item.DisplayOrder);
        //                                            break;
        //                                        }
        //                                    }
        //                                    foreach (var item in columnDisplayOrder)
        //                                    {
        //                                        if (Convert.ToString(item.Id) == ansExpression.Split('~')[1])
        //                                        {
        //                                            displayopt = Convert.ToString(item.DisplayOrder);
        //                                            break;
        //                                        }
        //                                    }
        //                                    //formattedAnswer = "Q" + qustionDisplayOrder + "_M" + displaycol + "_C" + displayopt;
        //                                    formattedAnswer = "Q" + questionId + "_M" + displaycol + "_C" + displayopt;
        //                                    answerValue = ansExpression.Split('~')[0].Split('*')[1];
        //                                    if (questionTypeId == 14)
        //                                        DDlDisplayOrder = dropDownDisplayOrder.Where(l => l.Id == Convert.ToInt32(answerValue)).FirstOrDefault().DDLDisplayOrder;
        //                                }
        //                                else
        //                                {
        //                                    foreach (var item in optionDisplayOrder)
        //                                    {
        //                                        if (Convert.ToString(item.OptionId) == ansExpression.Split('~')[0])
        //                                        {
        //                                            displaycol = Convert.ToString(item.DisplayOrder);
        //                                            break;
        //                                        }
        //                                    }
        //                                    foreach (var item in columnDisplayOrder)
        //                                    {
        //                                        if (Convert.ToString(item.Id) == ansExpression.Split('~')[1])
        //                                        {
        //                                            displayopt = Convert.ToString(item.DisplayOrder);
        //                                            break;
        //                                        }
        //                                    }
        //                                    //formattedAnswer = "Q" + qustionDisplayOrder + "_M" + displaycol + "_C" + displayopt;
        //                                    formattedAnswer = "Q" + questionId + "_M" + displaycol + "_C" + displayopt;
        //                                }
        //                                foreach (DataColumn column in dtF.Columns)
        //                                {
        //                                    string[] columnHeader = column.ColumnName.Split('-');

        //                                    if (columnHeader.Length > 1)
        //                                    {
        //                                        string[] answer = columnHeader[0].Split('_');

        //                                        if (answer.Length > 2)
        //                                        {
        //                                            string answerInHeader = answer[0] + "_" + answer[1] + "_" + answer[2];

        //                                            if (formattedAnswer == answerInHeader)
        //                                            {
        //                                                int index = dr_dtF.Table.Columns[column.ColumnName].Ordinal;
        //                                                lstColumnOrdinals.Add(index.ToString());
        //                                                //count++;
        //                                                if (questionTypeId == 9)
        //                                                {
        //                                                    dr_dtF[index] = answerValue;
        //                                                }
        //                                                else if (questionTypeId == 14)
        //                                                {
        //                                                    dr_dtF[index] = DDlDisplayOrder;
        //                                                }
        //                                                else
        //                                                {
        //                                                    dr_dtF[index] = "1";
        //                                                }
        //                                            }
        //                                        }
        //                                    }
        //                                }
        //                            }
        //                        }

        //                        // Fill All the Empty cells with 2

        //                        for (int i = 0; i <= dtF.Columns.Count - 1; i++)
        //                            if (String.IsNullOrEmpty(Convert.ToString(dr_dtF[i])))
        //                                dr_dtF[i] = "";
        //                    }

        //                    else
        //                    {
        //                        dr_dtF[count] = answerExpression;
        //                        count++;
        //                        optionsCount--;
        //                    }

        //                    if (productId != oldProductID)
        //                    {
        //                        dtF.Rows.Add(drTemp);
        //                        //  drTemp[0] = dtF.Rows.IndexOf(drTemp) + 1;
        //                        drTemp[0] = response_id;
        //                        oldProductID = productId;
        //                    }

        //                    drTemp = dr_dtF;
        //                    //---ss
        //                    if (model.projectModel.ProjectType == "2")
        //                    {
        //                        drTemp[2] = product_desc;
        //                        drTemp[1] = product_logiId;
        //                    }
        //                    //---ss

        //                }

        //                dtF.Rows.Add(dr_dtF);
        //                //dr_dtF[0] = dtF.Rows.IndexOf(dr_dtF) + 1;
        //                drTemp[0] = response_id;
        //                //---ss
        //                if (model.projectModel.ProjectType == "2")
        //                {
        //                    dr_dtF[2] = product_desc;
        //                    dr_dtF[1] = product_logiId;
        //                }
        //                response_id++;
        //                //---ss
        //            }
        //        }

        //        if (dtF != null && dtF.Rows.Count > 0)
        //        {
        //            //DataTable dtF = comMethods.GenerateTransposedTable(dt);
        //            newData = comMethods.GenerateTransposedTable(dt);
        //            foreach (DataRow dr in dtF.Rows)
        //            {
        //                if (model.projectModel.ProjectType == "2")
        //                {
        //                    DataRow rw = newData.AsEnumerable().FirstOrDefault(tt => tt.Field<string>("Resp_Id") == dr["Resp_Id"].ToString() && tt.Field<string>("Product_Name") == dr["Product_Name"].ToString());
        //                    if (rw == null)
        //                    {
        //                        DataRow dr_dtF = newData.NewRow();

        //                        newData.Rows.Add(dr_dtF);
        //                        foreach (DataColumn dcTempKano in dtF.Columns)
        //                        {

        //                            dr_dtF[dcTempKano.ToString()] = dr[dcTempKano.ToString()];

        //                        }
        //                        // row exists
        //                    }
        //                }
        //                else
        //                {
        //                    DataRow rw = newData.AsEnumerable().FirstOrDefault(tt => tt.Field<string>("Resp_Id") == dr["Resp_Id"].ToString());
        //                    if (rw == null)
        //                    {
        //                        DataRow dr_dtF = newData.NewRow();

        //                        newData.Rows.Add(dr_dtF);
        //                        foreach (DataColumn dcTempKano in dtF.Columns)
        //                        {

        //                            dr_dtF[dcTempKano.ToString()] = dr[dcTempKano.ToString()];

        //                        }
        //                        // row exists
        //                    }
        //                }


        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {

        //    }
        //    if (newData.Rows.Count > 0)
        //    {
        //        string onlineResponseXML = ConvertDatatableToXML(newData);
        //        actionResult = adminAction.OnlineProjectRespose_Add(proId, Id, onlineResponseXML);
        //    }
        //}
        //#endregion

        //#region GetReportDataTableOffline
       
        //public void GetReportDataTableOffline(string Id, int ResponseCount, int proId)
        //{
        //    RespondentController respondent = new RespondentController();
        //    SurveyResponseModel model = new SurveyResponseModel();
        //    DataTable dtF = new DataTable();
        //    try
        //    {
        //        DataTable dt = new DataTable();

        //        string projectGuid = string.Empty;
        //        projectGuid = Id;
        //        dt.Columns.Add("Resp_Id", typeof(string));

        //        model = respondent.GetProjectPreview(projectGuid);

        //        //--ss
        //        if (model.projectModel.ProjectType == "2")
        //        {
        //            DataRow drnew = dt.NewRow();
        //            drnew[0] = "Product_LogicalId";
        //            dt.Rows.Add(drnew);
        //            DataRow drnew1 = dt.NewRow();
        //            drnew1[0] = "Product_Name";
        //            dt.Rows.Add(drnew1);
        //        }
        //        //--

        //        // Load all the questions of this project
        //        List<QuestionList> questionsList = new List<QuestionList>();
        //        questionBase.ProjectId = Convert.ToInt32(model.projectModel.Id);
        //        actionResult = adminAction.QuestionsLoadAll_ByProjectId(questionBase);
        //        if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
        //        {
        //            foreach (DataRow dr in actionResult.dtResult.Rows)
        //            {
        //                questionsList.Add(new QuestionList
        //                {
        //                    id = dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0,
        //                    questionTypeId = dr["QuestionTypeId"] != DBNull.Value ? Convert.ToInt32(dr["QuestionTypeId"]) : 0
        //                });
        //            }
        //        }

        //        string fileName = Guid.NewGuid().ToString().Substring(0, 5) + "_" + DateTime.Now.ToString("MM-dd-yyyy") + "_" + model.projectModel.ProjectName.Replace(" ", "");

        //        if (model.lstQuestionModel != null && model.lstQuestionModel.Count > 0)
        //        {
        //            foreach (var item in model.lstQuestionModel)
        //            {
        //                if (item.PipedFrom > 0 && (!item.QuestionTitle.Contains("{{Q}}")))
        //                {
        //                    if (item.QuestionTypeId == 2 || item.QuestionTypeId == 4 || item.QuestionTypeId == 5 || item.QuestionTypeId == 6)//Multi Punch || Constant Sum || Ranking || Matrix Type
        //                    {
        //                        if (model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel != null && model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel.Count > 0)
        //                        {
        //                            foreach (var opt in model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel)
        //                            {
        //                                DataRow drnew = dt.NewRow();
        //                                drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
        //                                dt.Rows.Add(drnew);
        //                            }
        //                        }
        //                    }

        //                    else if (item.QuestionTypeId == 1 || item.QuestionTypeId == 3 || item.QuestionTypeId == 7 || item.QuestionTypeId == 15)//Single Punch || Range || Simple Text
        //                    {
        //                        DataRow drnew = dt.NewRow();
        //                        drnew[0] = "Q" + item.QuestionId + "_S" + "-" + ReplaceHtml.ReplaceText(item.QuestionTitle);
        //                        dt.Rows.Add(drnew);
        //                    }
        //                    else if (item.QuestionTypeId == 8 || item.QuestionTypeId == 12)//Single punch matrix type  && Rank order type
        //                    {
        //                        //if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
        //                        //{
        //                        //foreach (var opt in item.lstOptionsModel)
        //                        //{
        //                        //    DataRow drnew = dt.NewRow();
        //                        //    //drnew[0] = "Q" + item.QuestionId + "_M" + opt.OptionId + "-" + ReplaceHtml.ReplaceText(opt.Option);
        //                        //    drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
        //                        //    dt.Rows.Add(drnew);
        //                        //}
        //                        //}
        //                        if (item.lstMatrixTypeModel != null && item.lstMatrixTypeModel.Count > 0)
        //                        {
        //                            foreach (var col in item.lstMatrixTypeModel)
        //                            {
        //                                DataRow drnew = dt.NewRow();
        //                                //drnew[0] = "Q" + item.QuestionId + "_M" + opt.OptionId + "-" + ReplaceHtml.ReplaceText(opt.Option);
        //                                drnew[0] = "Q" + item.QuestionId + "_M" + col.DisplayOrder + "-" + ReplaceHtml.ReplaceText(col.ColumnName);
        //                                dt.Rows.Add(drnew);
        //                            }
        //                        }
        //                    }
        //                    else if (item.QuestionTypeId == 13)//Kano model matrix type 
        //                    {
        //                        if (model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel != null && model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel.Count > 0)
        //                        {
        //                            foreach (var opt in model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel)
        //                            {
        //                                DataRow drnew = dt.NewRow();
        //                                drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Present";
        //                                dt.Rows.Add(drnew);
        //                                DataRow drnew1 = dt.NewRow();
        //                                drnew1[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Absent";
        //                                dt.Rows.Add(drnew1);
        //                            }
        //                        }
        //                    }
        //                    // Single Punch Matrix Type || Range Matrix Type Question ||
        //                    // Single punch type answer horizontal position || 
        //                    // Multiple punch type answer horizontal position || 
        //                    // Rank order type question || 
        //                    // KANO Model Matrix type question || Dropdown Matrix type question
        //                    else if (item.QuestionTypeId > 7 && item.QuestionTypeId != 15)
        //                    {
        //                        if (model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel != null && model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel.Count > 0)
        //                        {
        //                            foreach (var opt in model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel)
        //                            {
        //                                foreach (var column in item.lstMatrixTypeModel)
        //                                {
        //                                    DataRow drnew = dt.NewRow();
        //                                    drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "_C" + column.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + " " + ReplaceHtml.ReplaceText(column.ColumnName);
        //                                    dt.Rows.Add(drnew);
        //                                }
        //                            }
        //                        }
        //                    }
        //                }
        //                else
        //                {
        //                    if (item.QuestionTypeId == 2 || item.QuestionTypeId == 4 || item.QuestionTypeId == 5 || item.QuestionTypeId == 6)//Multi Punch || Constant Sum || Ranking || Matrix Type
        //                    {
        //                        if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
        //                        {
        //                            foreach (var opt in item.lstOptionsModel)
        //                            {
        //                                DataRow drnew = dt.NewRow();
        //                                //drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
        //                                drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
        //                                dt.Rows.Add(drnew);
        //                            }
        //                        }
        //                    }

        //                    else if (item.QuestionTypeId == 1 || item.QuestionTypeId == 3 || item.QuestionTypeId == 7 || item.QuestionTypeId == 15)//Single Punch || Range || Simple Text
        //                    {
        //                        DataRow drnew = dt.NewRow();
        //                        //drnew[0] = "Q" + item.DisplayOrder + "_S" + "-" + ReplaceHtml.ReplaceText(item.QuestionTitle);
        //                        drnew[0] = "Q" + item.QuestionId + "_S" + "-" + ReplaceHtml.ReplaceText(item.QuestionTitle);
        //                        dt.Rows.Add(drnew);
        //                    }
        //                    else if (item.QuestionTypeId == 8 || item.QuestionTypeId == 12)//Single punch matrix type  && Rank order type
        //                    {
        //                        //if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
        //                        //{
        //                        //foreach (var opt in item.lstOptionsModel)
        //                        //{
        //                        //    DataRow drnew = dt.NewRow();
        //                        //    //drnew[0] = "Q" + item.QuestionId + "_M" + opt.OptionId + "-" + ReplaceHtml.ReplaceText(opt.Option);
        //                        //    drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
        //                        //    dt.Rows.Add(drnew);
        //                        //}
        //                        //}
        //                        if (item.lstMatrixTypeModel != null && item.lstMatrixTypeModel.Count > 0)
        //                        {
        //                            foreach (var col in item.lstMatrixTypeModel)
        //                            {
        //                                DataRow drnew = dt.NewRow();
        //                                //drnew[0] = "Q" + item.QuestionId + "_M" + opt.OptionId + "-" + ReplaceHtml.ReplaceText(opt.Option);
        //                                drnew[0] = "Q" + item.QuestionId + "_M" + col.DisplayOrder + "-" + ReplaceHtml.ReplaceText(col.ColumnName);
        //                                dt.Rows.Add(drnew);
        //                            }
        //                        }
        //                    }
        //                    else if (item.QuestionTypeId == 13)//Kano model matrix type 
        //                    {
        //                        if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
        //                        {
        //                            foreach (var opt in item.lstOptionsModel)
        //                            {
        //                                DataRow drnew = dt.NewRow();
        //                                //drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Present";
        //                                drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Present";
        //                                dt.Rows.Add(drnew);
        //                                DataRow drnew1 = dt.NewRow();
        //                                //drnew1[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Absent";
        //                                drnew1[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Absent";
        //                                dt.Rows.Add(drnew1);
        //                            }
        //                        }
        //                    }
        //                    // Single Punch Matrix Type || Range Matrix Type Question ||
        //                    // Single punch type answer horizontal position || 
        //                    // Multiple punch type answer horizontal position || 
        //                    // Rank order type question || 
        //                    // KANO Model Matrix type question || Dropdown Matrix type question
        //                    else if (item.QuestionTypeId > 7)
        //                    {
        //                        if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
        //                        {
        //                            foreach (var opt in item.lstOptionsModel)
        //                            {
        //                                foreach (var column in item.lstMatrixTypeModel)
        //                                {
        //                                    DataRow drnew = dt.NewRow();
        //                                    //drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "_C" + column.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + " " + ReplaceHtml.ReplaceText(column.ColumnName);
        //                                    drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "_C" + column.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + " " + ReplaceHtml.ReplaceText(column.ColumnName);
        //                                    dt.Rows.Add(drnew);
        //                                }
        //                            }
        //                        }
        //                    }
        //                }
        //            }
        //        }
        //        // Get the Excel Pattern of dt
        //        dtF = comMethods.GenerateTransposedTable(dt);

        //        // Get all the reponders
        //        projectBase.Id = model.projectModel.Id;
        //        actionResult = adminAction.Responders_Offline_LoadBy_ProjectId(projectBase);

        //        // Get Responses
        //        SurveyLoadBase surveyLoadBase = new SurveyLoadBase();
        //        string json = string.Empty;
        //        SRV.BaseLayer.ActionResult actionResultResponses = new SRV.BaseLayer.ActionResult();
        //        int response_id = ResponseCount;            //ss
        //        foreach (DataRow dr in actionResult.dtResult.Rows)
        //        {
        //            surveyLoadBase.Id = model.projectModel.Id;
        //            surveyLoadBase.OfflineResponderId = dr["RespId"] != DBNull.Value ? Convert.ToString(dr["RespId"]) : "";
        //            //DataRow dr_dtF = dtF.NewRow();

        //            if (String.IsNullOrEmpty(surveyLoadBase.OfflineResponderId)) continue;


        //            DataTable responsesDt = new DataTable();


        //            if (model.projectModel.lstProductProjectModel != null && model.projectModel.lstProductProjectModel.Count > 0)
        //            {
        //                foreach (var productModel in model.projectModel.lstProductProjectModel)
        //                {
        //                    surveyLoadBase.ProductId = productModel.Id;
        //                    actionResultResponses = respondentAction.Response_Offline_LoadBy_ProjectId_RespId(surveyLoadBase);
        //                    if (actionResultResponses.IsSuccess)
        //                    {
        //                        if (responsesDt.Rows.Count == 0)
        //                            responsesDt = actionResultResponses.dtResult.Copy();
        //                        else
        //                        {
        //                            foreach (DataRow drThisRespRow in actionResultResponses.dtResult.Rows)
        //                                responsesDt.ImportRow(drThisRespRow);

        //                        }
        //                    }
        //                }
        //            }
        //            else
        //            {
        //                actionResultResponses = respondentAction.Response_Offline_LoadBy_ProjectId_RespId(surveyLoadBase);
        //                if (actionResultResponses.IsSuccess)
        //                    responsesDt = actionResultResponses.dtResult.Copy();

        //            }


        //            //actionResultResponses = respondentAction.Response_Offline_LoadBy_ProjectId_RespId(surveyLoadBase);
        //            if (responsesDt.Rows.Count > 0)//actionResultResponses.IsSuccess
        //            {
        //                //dr_dtF[0] = surveyLoadBase.Email;

        //                int count = 1;
        //                count = (model.projectModel.ProjectType == "2") ? count + 2 : count;        //ss
        //                DataRow dr_dtF = dtF.NewRow();
        //                DataRow drTemp = dtF.NewRow();
        //                string product_desc = string.Empty;
        //                string product_logiId = string.Empty;
        //                int oldProductID = responsesDt.Rows[0]["ProductId"] != DBNull.Value ? Convert.ToInt32(responsesDt.Rows[0]["ProductId"]) : 0;
        //                foreach (DataRow drResponse in responsesDt.Rows)// actionResultResponses.dtResult.Rows
        //                {

        //                    int optionsCount = 0;

        //                    int productId = drResponse["ProductId"] != DBNull.Value ? Convert.ToInt32(drResponse["ProductId"]) : 0;
        //                    if (productId != oldProductID)
        //                    {
        //                        count = (model.projectModel.ProjectType == "2") ? 3 : 1;
        //                        dr_dtF = dtF.NewRow();
        //                    }

        //                    //--ss

        //                    product_desc = drResponse["ProductDescription"] != DBNull.Value ? Convert.ToString(drResponse["ProductDescription"]) : "";
        //                    product_logiId = drResponse["ProductLogicalId"] != DBNull.Value ? Convert.ToString(drResponse["ProductLogicalId"]) : "";

        //                    //--ss
        //                    int questionId = drResponse["QuestionId"] != DBNull.Value ? Convert.ToInt32(drResponse["QuestionId"]) : 0;
        //                    string answerExpression = drResponse["AnswerExpression"] != DBNull.Value ? Convert.ToString(drResponse["AnswerExpression"]) : "";
        //                    int questionTypeId = drResponse["QuestionTypeId"] != DBNull.Value ? Convert.ToInt32(drResponse["QuestionTypeId"]) : 1;
        //                    int PipedFrom = drResponse["PipedFrom"] != DBNull.Value ? Convert.ToInt32(drResponse["PipedFrom"]) : 0;
        //                    //dr_dtF[0] = drResponse;

        //                    // Get Total options count for this question
        //                    questionBase.Id = Convert.ToInt32(questionId);
        //                    SRV.BaseLayer.ActionResult actionResultAnswersTemp = new SRV.BaseLayer.ActionResult();
        //                    actionResultAnswersTemp = adminAction.Option_LoadAllById(questionBase);
        //                    if (actionResultAnswersTemp.dtResult != null && actionResultAnswersTemp.dtResult.Rows.Count > 0)
        //                    {
        //                        optionsCount = actionResultAnswersTemp.dtResult.Rows.Count;
        //                    }

        //                    if (questionTypeId == 2 || questionTypeId == 4 || questionTypeId == 5 || questionTypeId == 6)
        //                    {
        //                        string[] allAnswers = answerExpression.Split('|');
        //                        if (allAnswers.Length > 0)
        //                        {
        //                            if (questionTypeId == 2)// Multi Punch
        //                            {
        //                                string allRec = string.Empty;
        //                                //List<OptionsModel> matrixAns = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
        //                                List<OptionsModel> matrixAns = new List<OptionsModel>();
        //                                if (PipedFrom > 0 && !drResponse["QuestionTitle"].ToString().Contains("{{Q}}"))
        //                                {
        //                                    matrixAns = model.lstQuestionModel.Where(v => v.QuestionId == PipedFrom).FirstOrDefault().lstOptionsModel;
        //                                }
        //                                else
        //                                {
        //                                    matrixAns = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
        //                                }
        //                                foreach (var item in matrixAns)
        //                                {
        //                                    string matchedId = string.Empty;
        //                                    foreach (string ans in allAnswers)
        //                                    {
        //                                        if (Convert.ToString(item.OptionId) == ans)
        //                                        {
        //                                            matchedId = Convert.ToString(item.DisplayOrder);
        //                                            break;
        //                                        }
        //                                    }
        //                                    allRec += matchedId + "|";
        //                                }

        //                                string[] allAnswersMat = allRec.Substring(0, allRec.Length - 1).Split('|');
        //                                foreach (string ans in allAnswersMat)
        //                                {

        //                                    if (count <= dtF.Columns.Count - 1)
        //                                    {
        //                                        if ((Regex.Replace(dtF.Columns[count].ToString().Split('-')[0].Split('_')[1], @"[^\d]", String.Empty)) == ans)
        //                                        {
        //                                            dr_dtF[count] = "1";// ans;
        //                                            count++;
        //                                            optionsCount--;
        //                                        }
        //                                        else
        //                                        {
        //                                            dr_dtF[count] = "2";
        //                                            count++;
        //                                            optionsCount--;
        //                                        }
        //                                    }
        //                                }
        //                                //count = count + optionsCount;
        //                            }
        //                            else if (questionTypeId == 6) // matrix Type
        //                            {
        //                                foreach (string ans in allAnswers)
        //                                {
        //                                    dr_dtF[count] = (ans.ToLower().IndexOf("yes") != -1) ? "1" : "2";
        //                                    count++;
        //                                    optionsCount--;
        //                                }
        //                                count = count + optionsCount;
        //                            }
        //                            else
        //                            {
        //                                foreach (string ans in allAnswers)
        //                                {
        //                                    dr_dtF[count] = ans;
        //                                    count++;
        //                                    optionsCount--;
        //                                }
        //                                count = count + optionsCount;
        //                            }
        //                        }
        //                    }
        //                    else if (questionTypeId == 1) // Single Punch Answers
        //                    {
        //                        //List<OptionsModel> SinglePunchAns = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
        //                        List<OptionsModel> SinglePunchAns = new List<OptionsModel>();
        //                        if (PipedFrom > 0 && !drResponse["QuestionTitle"].ToString().Contains("{{Q}}"))
        //                        {
        //                            SinglePunchAns = model.lstQuestionModel.Where(v => v.QuestionId == PipedFrom).FirstOrDefault().lstOptionsModel;
        //                        }
        //                        else
        //                        {
        //                            SinglePunchAns = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
        //                        }
        //                        int ansIndex = 0;
        //                        foreach (var item in SinglePunchAns)
        //                        {
        //                            ansIndex++;
        //                            if (Convert.ToString(item.OptionId) == answerExpression)
        //                            {
        //                                break;
        //                            }
        //                        }

        //                        dr_dtF[count] = ansIndex;//answerExpression;
        //                        count++;
        //                        optionsCount--;
        //                    }
        //                    else if (questionTypeId == 8 || questionTypeId == 12) // single punch matrix answers
        //                    {
        //                        List<OptionsModel> optionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
        //                        List<MatrixTypeModel> columnDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstMatrixTypeModel;
        //                        int qustionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).Select(v => v.DisplayOrder).FirstOrDefault();
        //                        string[] allAnswers = answerExpression.Split('|');

        //                        List<string> lstColumnOrdinals = new List<string>();

        //                        string formattedAnswer = string.Empty;
        //                        string answerValue = string.Empty;
        //                        if (allAnswers.Length > 0)
        //                        {
        //                            foreach (var ansExpression in allAnswers)
        //                            {
        //                                if (!string.IsNullOrEmpty(ansExpression))
        //                                {
        //                                    string displaycol = string.Empty;
        //                                    string displayopt = string.Empty;
        //                                    foreach (var item in optionDisplayOrder)
        //                                    {

        //                                        if (Convert.ToString(item.OptionId) == ansExpression.Split('~')[0])
        //                                        {
        //                                            displaycol = Convert.ToString(item.DisplayOrder);
        //                                            break;
        //                                        }
        //                                    }
        //                                    foreach (var item in columnDisplayOrder)
        //                                    {
        //                                        if (Convert.ToString(item.Id) == ansExpression.Split('~')[1])
        //                                        {
        //                                            displayopt = Convert.ToString(item.DisplayOrder);
        //                                            break;
        //                                        }
        //                                    }
        //                                    //  formattedAnswer = "Q" + questionId + "_M" + displaycol + "#" + displayopt;
        //                                    formattedAnswer = "Q" + questionId + "_M" + displayopt + "#" + displaycol;
        //                                    foreach (DataColumn column in dtF.Columns)
        //                                    {
        //                                        string[] columnHeader = column.ColumnName.Split('-');
        //                                        string[] answer = columnHeader[0].Split('_');
        //                                        if (columnHeader.Length > 1)
        //                                        {
        //                                            string answerInHeader = answer[0] + "_" + answer[1];
        //                                            string[] formattedAnswerArr = formattedAnswer.Split('#');
        //                                            if (formattedAnswerArr[0] == answerInHeader)
        //                                            {
        //                                                int index = dr_dtF.Table.Columns[column.ColumnName].Ordinal;
        //                                                lstColumnOrdinals.Add(index.ToString());
        //                                                count++;
        //                                                dr_dtF[index] = formattedAnswerArr[1];
        //                                            }
        //                                        }
        //                                    }
        //                                }
        //                            }
        //                        }
        //                    }
        //                    else if (questionTypeId == 13) // Kano model matrix type
        //                    {
        //                        List<OptionsModel> optionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
        //                        List<MatrixTypeModel> columnDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstMatrixTypeModel;
        //                        int qustionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).Select(v => v.DisplayOrder).FirstOrDefault();
        //                        string[] allAnswers = answerExpression.Split('|');

        //                        List<string> lstColumnOrdinals = new List<string>();

        //                        string formattedAnswer = string.Empty;
        //                        string answerValue = string.Empty;
        //                        if (allAnswers.Length > 1)
        //                        {
        //                            int repeatId = 0;
        //                            foreach (var ansExpression in allAnswers)
        //                            {
        //                                string displaycol = string.Empty;
        //                                string displayopt = string.Empty;
        //                                string displayText = string.Empty;
        //                                string text = string.Empty;
        //                                if (repeatId == Convert.ToInt32(ansExpression.Split('~')[0]))
        //                                {
        //                                    text = "_Absent";
        //                                    repeatId = 0;
        //                                }
        //                                else
        //                                {
        //                                    text = "_Present";
        //                                }
        //                                foreach (var item in optionDisplayOrder)
        //                                {
        //                                    if (Convert.ToString(item.OptionId) == ansExpression.Split('~')[0])
        //                                    {
        //                                        displayText = Convert.ToString(item.Option) + text;
        //                                        displaycol = Convert.ToString(item.DisplayOrder);
        //                                        repeatId = Convert.ToInt32(ansExpression.Split('~')[0]);
        //                                        break;
        //                                    }
        //                                }
        //                                foreach (var item in columnDisplayOrder)
        //                                {
        //                                    if (Convert.ToString(item.Id) == ansExpression.Split('~')[1])
        //                                    {
        //                                        displayopt = Convert.ToString(item.DisplayOrder);
        //                                        break;
        //                                    }
        //                                }
        //                                //formattedAnswer = "Q" + qustionDisplayOrder + "_M" + displaycol + "-" + displayText + "#" + displayopt;
        //                                formattedAnswer = "Q" + questionId + "_M" + displaycol + "-" + displayText + "#" + displayopt;
        //                                foreach (DataColumn column in dtF.Columns)
        //                                {

        //                                    string[] columnHeader = column.ColumnName.Split('-');
        //                                    string[] answer = columnHeader[0].Split('_');
        //                                    if (columnHeader.Length > 1)
        //                                    {
        //                                        string answerInHeader = answer[0] + "_" + answer[1] + "-" + columnHeader[1];
        //                                        string[] formattedAnswerArr = formattedAnswer.Split('#');
        //                                        if (Regex.Replace(formattedAnswerArr[0], @"\s+", "", RegexOptions.Multiline) == Regex.Replace(answerInHeader, @"\s+", "", RegexOptions.Multiline))
        //                                        {
        //                                            int index = dr_dtF.Table.Columns[column.ColumnName].Ordinal;
        //                                            lstColumnOrdinals.Add(index.ToString());
        //                                            count++;
        //                                            dr_dtF[index] = formattedAnswerArr[1];
        //                                        }
        //                                    }
        //                                }
        //                            }
        //                        }
        //                    }
        //                    else if (questionTypeId > 7 && questionTypeId != 15)
        //                    {
        //                        List<OptionsModel> optionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
        //                        List<MatrixTypeModel> columnDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstMatrixTypeModel;
        //                        List<DDLMatrixTypeModel> dropDownDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstDDlMatrixModel;
        //                        count = count + (optionDisplayOrder.Count * columnDisplayOrder.Count);
        //                        int qustionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).Select(v => v.DisplayOrder).FirstOrDefault();
        //                        string[] allAnswers = answerExpression.Split('|');

        //                        List<string> lstColumnOrdinals = new List<string>();

        //                        string formattedAnswer = string.Empty;
        //                        string answerValue = string.Empty;
        //                        int DDlDisplayOrder = 0;
        //                        if (allAnswers.Length > 1)
        //                        {
        //                            foreach (var ansExpression in allAnswers)
        //                            {
        //                                string displaycol = string.Empty;
        //                                string displayopt = string.Empty;

        //                                if (questionTypeId == 9 || questionTypeId == 14)
        //                                {
        //                                    foreach (var item in optionDisplayOrder)
        //                                    {
        //                                        if (Convert.ToString(item.OptionId) == ansExpression.Split('~')[0].Split('*')[0])
        //                                        {
        //                                            displaycol = Convert.ToString(item.DisplayOrder);
        //                                            break;
        //                                        }
        //                                    }
        //                                    foreach (var item in columnDisplayOrder)
        //                                    {
        //                                        if (Convert.ToString(item.Id) == ansExpression.Split('~')[1])
        //                                        {
        //                                            displayopt = Convert.ToString(item.DisplayOrder);
        //                                            break;
        //                                        }
        //                                    }
        //                                    //formattedAnswer = "Q" + qustionDisplayOrder + "_M" + displaycol + "_C" + displayopt;
        //                                    formattedAnswer = "Q" + questionId + "_M" + displaycol + "_C" + displayopt;
        //                                    answerValue = ansExpression.Split('~')[0].Split('*')[1];
        //                                    if (questionTypeId == 14)
        //                                        DDlDisplayOrder = dropDownDisplayOrder.Where(l => l.Id == Convert.ToInt32(answerValue)).FirstOrDefault().DDLDisplayOrder;
        //                                }
        //                                else
        //                                {
        //                                    foreach (var item in optionDisplayOrder)
        //                                    {
        //                                        if (Convert.ToString(item.OptionId) == ansExpression.Split('~')[0])
        //                                        {
        //                                            displaycol = Convert.ToString(item.DisplayOrder);
        //                                            break;
        //                                        }
        //                                    }
        //                                    foreach (var item in columnDisplayOrder)
        //                                    {
        //                                        if (Convert.ToString(item.Id) == ansExpression.Split('~')[1])
        //                                        {
        //                                            displayopt = Convert.ToString(item.DisplayOrder);
        //                                            break;
        //                                        }
        //                                    }
        //                                    //formattedAnswer = "Q" + qustionDisplayOrder + "_M" + displaycol + "_C" + displayopt;
        //                                    formattedAnswer = "Q" + questionId + "_M" + displaycol + "_C" + displayopt;
        //                                }
        //                                foreach (DataColumn column in dtF.Columns)
        //                                {
        //                                    string[] columnHeader = column.ColumnName.Split('-');

        //                                    if (columnHeader.Length > 1)
        //                                    {
        //                                        string[] answer = columnHeader[0].Split('_');

        //                                        if (answer.Length > 2)
        //                                        {
        //                                            string answerInHeader = answer[0] + "_" + answer[1] + "_" + answer[2];

        //                                            if (formattedAnswer == answerInHeader)
        //                                            {
        //                                                int index = dr_dtF.Table.Columns[column.ColumnName].Ordinal;
        //                                                lstColumnOrdinals.Add(index.ToString());
        //                                                //count++;
        //                                                if (questionTypeId == 9)
        //                                                {
        //                                                    dr_dtF[index] = answerValue;
        //                                                }
        //                                                else if (questionTypeId == 14)
        //                                                {
        //                                                    dr_dtF[index] = DDlDisplayOrder;
        //                                                }
        //                                                else
        //                                                {
        //                                                    dr_dtF[index] = "1";
        //                                                }
        //                                            }
        //                                        }
        //                                    }
        //                                }
        //                            }
        //                        }

        //                        // Fill All the Empty cells with 2

        //                        for (int i = 0; i <= dtF.Columns.Count - 1; i++)
        //                            if (String.IsNullOrEmpty(Convert.ToString(dr_dtF[i])))
        //                                dr_dtF[i] = "";
        //                    }

        //                    else
        //                    {
        //                        dr_dtF[count] = answerExpression;
        //                        count++;
        //                        optionsCount--;
        //                    }



        //                    if (productId != oldProductID)
        //                    {
        //                        dtF.Rows.Add(drTemp);
        //                        drTemp[0] = response_id;
        //                        oldProductID = productId;
        //                    }

        //                    drTemp = dr_dtF;
        //                    //---ss
        //                    if (model.projectModel.ProjectType == "2")
        //                    {
        //                        drTemp[2] = product_desc;
        //                        drTemp[1] = product_logiId;
        //                    }
        //                    //---ss


        //                }


        //                dtF.Rows.Add(dr_dtF);
        //                drTemp[0] = response_id;
        //                //---ss
        //                if (model.projectModel.ProjectType == "2")
        //                {
        //                    dr_dtF[2] = product_desc;
        //                    dr_dtF[1] = product_logiId;
        //                }
        //                response_id++;
        //                //---ss
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {

        //    }
        //    if (dtF.Rows.Count > 0)
        //    {
        //        string responseXML = ConvertDatatableToXML(dtF);
        //        actionResult = adminAction.ProjectRespose_Add(proId, Id, responseXML);
        //    }
        //}
        //#endregion

        //#region GetReportDataTableOnline for Combined
       
        //public DataTable GetRepOnline(string Id, int proId)
        //{
        //    RespondentController respondent = new RespondentController();
        //    SurveyResponseModel model = new SurveyResponseModel();
        //    DataTable dtF = new DataTable();
        //    DataTable newData = new DataTable();
        //    try
        //    {
        //        DataTable dt = new DataTable();

        //        string projectGuid = string.Empty;
        //        projectGuid = Id;
        //        dt.Columns.Add("Resp_Id", typeof(string));

        //        model = respondent.GetProjectPreview(projectGuid);

        //        //--ss
        //        if (model.projectModel.ProjectType == "2")
        //        {
        //            DataRow drnew = dt.NewRow();
        //            drnew[0] = "Product_LogicalId";
        //            dt.Rows.Add(drnew);
        //            DataRow drnew1 = dt.NewRow();
        //            drnew1[0] = "Product_Name";
        //            dt.Rows.Add(drnew1);
        //        }
        //        //--

        //        // Load all the questions of this project
        //        List<QuestionList> questionsList = new List<QuestionList>();
        //        questionBase.ProjectId = Convert.ToInt32(model.projectModel.Id);
        //        actionResult = adminAction.QuestionsLoadAll_ByProjectId(questionBase);
        //        if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
        //        {
        //            foreach (DataRow dr in actionResult.dtResult.Rows)
        //            {
        //                questionsList.Add(new QuestionList
        //                {
        //                    id = dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0,
        //                    questionTypeId = dr["QuestionTypeId"] != DBNull.Value ? Convert.ToInt32(dr["QuestionTypeId"]) : 0
        //                });
        //            }
        //        }

        //        string fileName = Guid.NewGuid().ToString().Substring(0, 5) + "_" + DateTime.Now.ToString("MM-dd-yyyy") + "_" + model.projectModel.ProjectName.Replace(" ", "");

        //        if (model.lstQuestionModel != null && model.lstQuestionModel.Count > 0)
        //        {
        //            foreach (var item in model.lstQuestionModel)
        //            {
        //                if (item.PipedFrom > 0 && (!item.QuestionTitle.Contains("{{Q}}")))
        //                {
        //                    if (item.QuestionTypeId == 2 || item.QuestionTypeId == 4 || item.QuestionTypeId == 5 || item.QuestionTypeId == 6)//Multi Punch || Constant Sum || Ranking || Matrix Type
        //                    {
        //                        if (model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel != null && model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel.Count > 0)
        //                        {
        //                            foreach (var opt in model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel)
        //                            {
        //                                DataRow drnew = dt.NewRow();
        //                                drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
        //                                dt.Rows.Add(drnew);
        //                            }
        //                        }
        //                    }

        //                    else if (item.QuestionTypeId == 1 || item.QuestionTypeId == 3 || item.QuestionTypeId == 7 || item.QuestionTypeId == 15)//Single Punch || Range || Simple Text
        //                    {
        //                        DataRow drnew = dt.NewRow();
        //                        drnew[0] = "Q" + item.QuestionId + "_S" + "-" + ReplaceHtml.ReplaceText(item.QuestionTitle);
        //                        dt.Rows.Add(drnew);
        //                    }
        //                    else if (item.QuestionTypeId == 8 || item.QuestionTypeId == 12)//Single punch matrix type  && Rank order type
        //                    {
        //                        //if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
        //                        //{
        //                        //foreach (var opt in item.lstOptionsModel)
        //                        //{
        //                        //    DataRow drnew = dt.NewRow();
        //                        //    //drnew[0] = "Q" + item.QuestionId + "_M" + opt.OptionId + "-" + ReplaceHtml.ReplaceText(opt.Option);
        //                        //    drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
        //                        //    dt.Rows.Add(drnew);
        //                        //}
        //                        //}
        //                        if (item.lstMatrixTypeModel != null && item.lstMatrixTypeModel.Count > 0)
        //                        {
        //                            foreach (var col in item.lstMatrixTypeModel)
        //                            {
        //                                DataRow drnew = dt.NewRow();
        //                                //drnew[0] = "Q" + item.QuestionId + "_M" + opt.OptionId + "-" + ReplaceHtml.ReplaceText(opt.Option);
        //                                drnew[0] = "Q" + item.QuestionId + "_M" + col.DisplayOrder + "-" + ReplaceHtml.ReplaceText(col.ColumnName);
        //                                dt.Rows.Add(drnew);
        //                            }
        //                        }
        //                    }
        //                    else if (item.QuestionTypeId == 13)//Kano model matrix type 
        //                    {
        //                        if (model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel != null && model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel.Count > 0)
        //                        {
        //                            foreach (var opt in model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel)
        //                            {
        //                                DataRow drnew = dt.NewRow();
        //                                drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Present";
        //                                dt.Rows.Add(drnew);
        //                                DataRow drnew1 = dt.NewRow();
        //                                drnew1[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Absent";
        //                                dt.Rows.Add(drnew1);
        //                            }
        //                        }
        //                    }
        //                    // Single Punch Matrix Type || Range Matrix Type Question ||
        //                    // Single punch type answer horizontal position || 
        //                    // Multiple punch type answer horizontal position || 
        //                    // Rank order type question || 
        //                    // KANO Model Matrix type question || Dropdown Matrix type question
        //                    else if (item.QuestionTypeId > 7 && item.QuestionTypeId != 15)
        //                    {
        //                        if (model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel != null && model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel.Count > 0)
        //                        {
        //                            foreach (var opt in model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel)
        //                            {
        //                                foreach (var column in item.lstMatrixTypeModel)
        //                                {
        //                                    DataRow drnew = dt.NewRow();
        //                                    drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "_C" + column.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + " " + ReplaceHtml.ReplaceText(column.ColumnName);
        //                                    dt.Rows.Add(drnew);
        //                                }
        //                            }
        //                        }
        //                    }
        //                }
        //                else
        //                {
        //                    if (item.QuestionTypeId == 2 || item.QuestionTypeId == 4 || item.QuestionTypeId == 5 || item.QuestionTypeId == 6)//Multi Punch || Constant Sum || Ranking || Matrix Type
        //                    {
        //                        if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
        //                        {
        //                            foreach (var opt in item.lstOptionsModel)
        //                            {
        //                                DataRow drnew = dt.NewRow();
        //                                //drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
        //                                drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
        //                                dt.Rows.Add(drnew);
        //                            }
        //                        }
        //                    }

        //                    else if (item.QuestionTypeId == 1 || item.QuestionTypeId == 3 || item.QuestionTypeId == 7 || item.QuestionTypeId == 15)//Single Punch || Range || Simple Text
        //                    {
        //                        DataRow drnew = dt.NewRow();
        //                        //drnew[0] = "Q" + item.DisplayOrder + "_S" + "-" + ReplaceHtml.ReplaceText(item.QuestionTitle);
        //                        drnew[0] = "Q" + item.QuestionId + "_S" + "-" + ReplaceHtml.ReplaceText(item.QuestionTitle);
        //                        dt.Rows.Add(drnew);
        //                    }
        //                    else if (item.QuestionTypeId == 8 || item.QuestionTypeId == 12)//Single punch matrix type  && Rank order type
        //                    {
        //                        //if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
        //                        //{
        //                        //foreach (var opt in item.lstOptionsModel)
        //                        //{
        //                        //    DataRow drnew = dt.NewRow();
        //                        //    //drnew[0] = "Q" + item.QuestionId + "_M" + opt.OptionId + "-" + ReplaceHtml.ReplaceText(opt.Option);
        //                        //    drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
        //                        //    dt.Rows.Add(drnew);
        //                        //}
        //                        //}
        //                        if (item.lstMatrixTypeModel != null && item.lstMatrixTypeModel.Count > 0)
        //                        {
        //                            foreach (var col in item.lstMatrixTypeModel)
        //                            {
        //                                DataRow drnew = dt.NewRow();
        //                                //drnew[0] = "Q" + item.QuestionId + "_M" + opt.OptionId + "-" + ReplaceHtml.ReplaceText(opt.Option);
        //                                drnew[0] = "Q" + item.QuestionId + "_M" + col.DisplayOrder + "-" + ReplaceHtml.ReplaceText(col.ColumnName);
        //                                dt.Rows.Add(drnew);
        //                            }
        //                        }
        //                    }
        //                    else if (item.QuestionTypeId == 13)//Kano model matrix type 
        //                    {
        //                        if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
        //                        {
        //                            foreach (var opt in item.lstOptionsModel)
        //                            {
        //                                DataRow drnew = dt.NewRow();
        //                                //drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Present";
        //                                drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Present";
        //                                dt.Rows.Add(drnew);
        //                                DataRow drnew1 = dt.NewRow();
        //                                //drnew1[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Absent";
        //                                drnew1[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Absent";
        //                                dt.Rows.Add(drnew1);
        //                            }
        //                        }
        //                    }
        //                    // Single Punch Matrix Type || Range Matrix Type Question ||
        //                    // Single punch type answer horizontal position || 
        //                    // Multiple punch type answer horizontal position || 
        //                    // Rank order type question || 
        //                    // KANO Model Matrix type question || Dropdown Matrix type question
        //                    else if (item.QuestionTypeId > 7)
        //                    {
        //                        if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
        //                        {
        //                            foreach (var opt in item.lstOptionsModel)
        //                            {
        //                                foreach (var column in item.lstMatrixTypeModel)
        //                                {
        //                                    DataRow drnew = dt.NewRow();
        //                                    //drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "_C" + column.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + " " + ReplaceHtml.ReplaceText(column.ColumnName);
        //                                    drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "_C" + column.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + " " + ReplaceHtml.ReplaceText(column.ColumnName);
        //                                    dt.Rows.Add(drnew);
        //                                }
        //                            }
        //                        }
        //                    }
        //                }
        //            }
        //        }
        //        // Get the Excel Pattern of dt
        //        dtF = comMethods.GenerateTransposedTable(dt);

        //        // Get all the reponders
        //        projectBase.Id = model.projectModel.Id;
        //        actionResult = adminAction.CompleteRespondersLoad_ByProjectId(projectBase);
        //        List<RespondersModel> lstResponders = new List<RespondersModel>();
        //        foreach (DataRow dr in actionResult.dtResult.Rows)
        //        {
        //            lstResponders.Add(new RespondersModel
        //            {

        //                InvitationGuid = dr["InvitationGuid"] != DBNull.Value ? Convert.ToString(dr["InvitationGuid"]) : "",
        //                Email = dr["Email"] != DBNull.Value ? Convert.ToString(dr["Email"]) : "",

        //            });
        //        }
        //        IEnumerable<RespondersModel> filteredList = lstResponders
        //         .GroupBy(customer => customer.InvitationGuid).Select(group => group.First());
        //        // Get Responses
        //        SurveyLoadBase surveyLoadBase = new SurveyLoadBase();
        //        string json = string.Empty;
        //        SRV.BaseLayer.ActionResult actionResultResponses = new SRV.BaseLayer.ActionResult();
        //        int response_id = 1;            //ss
        //        foreach (var responder in filteredList)
        //        {
        //            DataTable responsesDt = new DataTable();
        //            surveyLoadBase.Id = model.projectModel.Id;
        //            surveyLoadBase.Email = (!string.IsNullOrEmpty(responder.Email) ? Convert.ToString(responder.Email) : "");
        //            surveyLoadBase.ExternalResponderId = (!string.IsNullOrEmpty(responder.InvitationGuid) ? Convert.ToString(responder.InvitationGuid) : "");
        //            if (model.projectModel.lstProductProjectModel != null && model.projectModel.lstProductProjectModel.Count > 0)
        //            {
        //                foreach (var productModel in model.projectModel.lstProductProjectModel)
        //                {
        //                    surveyLoadBase.ProductId = productModel.Id;
        //                    actionResultResponses = respondentAction.Response_LoadByProjectId(surveyLoadBase);
        //                    if (actionResultResponses.IsSuccess)
        //                    {
        //                        if (responsesDt.Rows.Count == 0)
        //                            responsesDt = actionResultResponses.dtResult.Copy();
        //                        else
        //                        {
        //                            foreach (DataRow drThisRespRow in actionResultResponses.dtResult.Rows)
        //                                responsesDt.ImportRow(drThisRespRow);
        //                        }
        //                    }
        //                }
        //            }
        //            else
        //            {
        //                actionResultResponses = respondentAction.Response_LoadByProjectId(surveyLoadBase);
        //                if (actionResultResponses.IsSuccess)
        //                    responsesDt = actionResultResponses.dtResult.Copy();

        //            }

        //            // surveyLoadBase.OfflineResponderId = dr["RespId"] != DBNull.Value ? Convert.ToString(dr["RespId"]) : "";
        //            //DataRow dr_dtF = dtF.NewRow();
        //            // if (String.IsNullOrEmpty(surveyLoadBase.Email)) continue;

        //            //actionResultResponses = respondentAction.Response_LoadByProjectId(surveyLoadBase);
        //            if (responsesDt.Rows.Count > 0)//actionResultResponses.IsSuccess
        //            {
        //                //dr_dtF[0] = surveyLoadBase.Email;

        //                int count = 1;
        //                count = (model.projectModel.ProjectType == "2") ? count + 2 : count;        //ss
        //                DataRow dr_dtF = dtF.NewRow();
        //                DataRow drTemp = dtF.NewRow();
        //                string product_desc = string.Empty;
        //                string product_logiId = string.Empty;
        //                int oldProductID = responsesDt.Rows[0]["ProductId"] != DBNull.Value ? Convert.ToInt32(responsesDt.Rows[0]["ProductId"]) : 0;
        //                foreach (DataRow drResponse in responsesDt.Rows)// actionResultResponses.dtResult.Rows
        //                {

        //                    int optionsCount = 0;
        //                    // response_id = drResponse["ReponderId"] != DBNull.Value ? (responsesDt.Rows[0]["ReponderId"]).ToString() : "";
        //                    int productId = drResponse["ProductId"] != DBNull.Value ? Convert.ToInt32(drResponse["ProductId"]) : 0;
        //                    if (productId != oldProductID)
        //                    {
        //                        count = (model.projectModel.ProjectType == "2") ? 3 : 1;
        //                        dr_dtF = dtF.NewRow();
        //                    }

        //                    //--ss

        //                    product_desc = drResponse["ProductDescription"] != DBNull.Value ? Convert.ToString(drResponse["ProductDescription"]) : "";
        //                    product_logiId = drResponse["ProductLogicalId"] != DBNull.Value ? Convert.ToString(drResponse["ProductLogicalId"]) : "";

        //                    //--ss
        //                    int questionId = drResponse["QuestionId"] != DBNull.Value ? Convert.ToInt32(drResponse["QuestionId"]) : 0;
        //                    string answerExpression = drResponse["AnswerExpression"] != DBNull.Value ? Convert.ToString(drResponse["AnswerExpression"]) : "";
        //                    int questionTypeId = drResponse["QuestionTypeId"] != DBNull.Value ? Convert.ToInt32(drResponse["QuestionTypeId"]) : 1;
        //                    int PipedFrom = drResponse["PipedFrom"] != DBNull.Value ? Convert.ToInt32(drResponse["PipedFrom"]) : 0;
        //                    //dr_dtF[0] = drResponse;

        //                    // Get Total options count for this question
        //                    questionBase.Id = Convert.ToInt32(questionId);
        //                    SRV.BaseLayer.ActionResult actionResultAnswersTemp = new SRV.BaseLayer.ActionResult();
        //                    actionResultAnswersTemp = adminAction.Option_LoadAllById(questionBase);
        //                    if (actionResultAnswersTemp.dtResult != null && actionResultAnswersTemp.dtResult.Rows.Count > 0)
        //                    {
        //                        optionsCount = actionResultAnswersTemp.dtResult.Rows.Count;
        //                    }

        //                    if (questionTypeId == 2 || questionTypeId == 4 || questionTypeId == 5 || questionTypeId == 6)
        //                    {
        //                        string[] allAnswers = answerExpression.Split('|');
        //                        if (allAnswers.Length > 0)
        //                        {
        //                            if (questionTypeId == 2)// Multi Punch
        //                            {
        //                                string allRec = string.Empty;
        //                                List<OptionsModel> matrixAns = new List<OptionsModel>();
        //                                if (PipedFrom > 0 && !drResponse["QuestionTitle"].ToString().Contains("{{Q}}"))
        //                                {
        //                                    matrixAns = model.lstQuestionModel.Where(v => v.QuestionId == PipedFrom).FirstOrDefault().lstOptionsModel;
        //                                }
        //                                else
        //                                {
        //                                    matrixAns = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
        //                                }

        //                                foreach (var item in matrixAns)
        //                                {
        //                                    string matchedId = string.Empty;
        //                                    foreach (string ans in allAnswers)
        //                                    {
        //                                        if (Convert.ToString(item.OptionId) == ans)
        //                                        {
        //                                            matchedId = Convert.ToString(item.DisplayOrder);
        //                                            break;
        //                                        }
        //                                    }
        //                                    allRec += matchedId + "|";
        //                                }

        //                                string[] allAnswersMat = allRec.Substring(0, allRec.Length - 1).Split('|');
        //                                foreach (string ans in allAnswersMat)
        //                                {

        //                                    if (count <= dtF.Columns.Count - 1)
        //                                    {
        //                                        if ((Regex.Replace(dtF.Columns[count].ToString().Split('-')[0].Split('_')[1], @"[^\d]", String.Empty)) == ans)
        //                                        {
        //                                            dr_dtF[count] = "1";// ans;
        //                                            count++;
        //                                            optionsCount--;
        //                                        }
        //                                        else
        //                                        {
        //                                            dr_dtF[count] = "2";
        //                                            count++;
        //                                            optionsCount--;
        //                                        }
        //                                    }
        //                                }
        //                                //count = count + optionsCount;
        //                            }
        //                            else if (questionTypeId == 6) // matrix Type
        //                            {
        //                                foreach (string ans in allAnswers)
        //                                {
        //                                    dr_dtF[count] = (ans.ToLower().IndexOf("yes") != -1) ? "1" : "2";
        //                                    count++;
        //                                    optionsCount--;
        //                                }
        //                                count = count + optionsCount;
        //                            }
        //                            else
        //                            {
        //                                foreach (string ans in allAnswers)
        //                                {
        //                                    dr_dtF[count] = ans;
        //                                    count++;
        //                                    optionsCount--;
        //                                }
        //                                count = count + optionsCount;
        //                            }
        //                        }
        //                    }
        //                    else if (questionTypeId == 1) // Single Punch Answers
        //                    {
        //                        //List<OptionsModel> SinglePunchAns = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
        //                        List<OptionsModel> SinglePunchAns = new List<OptionsModel>();
        //                        if (PipedFrom > 0 && !drResponse["QuestionTitle"].ToString().Contains("{{Q}}"))
        //                        {
        //                            SinglePunchAns = model.lstQuestionModel.Where(v => v.QuestionId == PipedFrom).FirstOrDefault().lstOptionsModel;
        //                        }
        //                        else
        //                        {
        //                            SinglePunchAns = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
        //                        }
        //                        int ansIndex = 0;
        //                        foreach (var item in SinglePunchAns)
        //                        {
        //                            ansIndex++;
        //                            if (Convert.ToString(item.OptionId) == answerExpression)
        //                            {
        //                                break;
        //                            }
        //                        }

        //                        dr_dtF[count] = ansIndex;//answerExpression;
        //                        count++;
        //                        optionsCount--;
        //                    }
        //                    else if (questionTypeId == 8 || questionTypeId == 12) // single punch matrix answers
        //                    {
        //                        List<OptionsModel> optionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
        //                        List<MatrixTypeModel> columnDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstMatrixTypeModel;
        //                        int qustionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).Select(v => v.DisplayOrder).FirstOrDefault();
        //                        string[] allAnswers = answerExpression.Split('|');

        //                        List<string> lstColumnOrdinals = new List<string>();

        //                        string formattedAnswer = string.Empty;
        //                        string answerValue = string.Empty;
        //                        if (allAnswers.Length > 0)
        //                        {
        //                            foreach (var ansExpression in allAnswers)
        //                            {
        //                                string displaycol = string.Empty;
        //                                string displayopt = string.Empty;
        //                                foreach (var item in optionDisplayOrder)
        //                                {

        //                                    if (Convert.ToString(item.OptionId) == ansExpression.Split('~')[0])
        //                                    {
        //                                        displaycol = Convert.ToString(item.DisplayOrder);
        //                                        break;
        //                                    }
        //                                }
        //                                foreach (var item in columnDisplayOrder)
        //                                {
        //                                    if (Convert.ToString(item.Id) == ansExpression.Split('~')[1])
        //                                    {
        //                                        displayopt = Convert.ToString(item.DisplayOrder);
        //                                        break;
        //                                    }
        //                                }
        //                                //  formattedAnswer = "Q" + questionId + "_M" + displaycol + "#" + displayopt;
        //                                formattedAnswer = "Q" + questionId + "_M" + displayopt + "#" + displaycol;
        //                                foreach (DataColumn column in dtF.Columns)
        //                                {
        //                                    string[] columnHeader = column.ColumnName.Split('-');
        //                                    string[] answer = columnHeader[0].Split('_');
        //                                    if (columnHeader.Length > 1)
        //                                    {
        //                                        string answerInHeader = answer[0] + "_" + answer[1];
        //                                        string[] formattedAnswerArr = formattedAnswer.Split('#');
        //                                        if (formattedAnswerArr[0] == answerInHeader)
        //                                        {
        //                                            int index = dr_dtF.Table.Columns[column.ColumnName].Ordinal;
        //                                            lstColumnOrdinals.Add(index.ToString());
        //                                            count++;
        //                                            dr_dtF[index] = formattedAnswerArr[1];
        //                                        }
        //                                    }
        //                                }
        //                            }
        //                        }
        //                    }
        //                    else if (questionTypeId == 13) // Kano model matrix type
        //                    {
        //                        List<OptionsModel> optionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
        //                        List<MatrixTypeModel> columnDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstMatrixTypeModel;
        //                        int qustionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).Select(v => v.DisplayOrder).FirstOrDefault();
        //                        string[] allAnswers = answerExpression.Split('|');

        //                        List<string> lstColumnOrdinals = new List<string>();

        //                        string formattedAnswer = string.Empty;
        //                        string answerValue = string.Empty;
        //                        if (allAnswers.Length > 1)
        //                        {
        //                            int repeatId = 0;
        //                            foreach (var ansExpression in allAnswers)
        //                            {
        //                                string displaycol = string.Empty;
        //                                string displayopt = string.Empty;
        //                                string displayText = string.Empty;
        //                                string text = string.Empty;
        //                                if (repeatId == Convert.ToInt32(ansExpression.Split('~')[0]))
        //                                {
        //                                    text = "_Absent";
        //                                    repeatId = 0;
        //                                }
        //                                else
        //                                {
        //                                    text = "_Present";
        //                                }
        //                                foreach (var item in optionDisplayOrder)
        //                                {
        //                                    if (Convert.ToString(item.OptionId) == ansExpression.Split('~')[0])
        //                                    {
        //                                        displayText = Convert.ToString(item.Option) + text;
        //                                        displaycol = Convert.ToString(item.DisplayOrder);
        //                                        repeatId = Convert.ToInt32(ansExpression.Split('~')[0]);
        //                                        break;
        //                                    }
        //                                }
        //                                foreach (var item in columnDisplayOrder)
        //                                {
        //                                    if (Convert.ToString(item.Id) == ansExpression.Split('~')[1])
        //                                    {
        //                                        displayopt = Convert.ToString(item.DisplayOrder);
        //                                        break;
        //                                    }
        //                                }
        //                                //formattedAnswer = "Q" + qustionDisplayOrder + "_M" + displaycol + "-" + displayText + "#" + displayopt;
        //                                formattedAnswer = "Q" + questionId + "_M" + displaycol + "-" + displayText + "#" + displayopt;
        //                                foreach (DataColumn column in dtF.Columns)
        //                                {

        //                                    string[] columnHeader = column.ColumnName.Split('-');
        //                                    string[] answer = columnHeader[0].Split('_');
        //                                    if (columnHeader.Length > 1)
        //                                    {
        //                                        string answerInHeader = answer[0] + "_" + answer[1] + "-" + columnHeader[1];
        //                                        string[] formattedAnswerArr = formattedAnswer.Split('#');
        //                                        if (Regex.Replace(formattedAnswerArr[0], @"\s+", "", RegexOptions.Multiline) == Regex.Replace(answerInHeader, @"\s+", "", RegexOptions.Multiline))
        //                                        {
        //                                            int index = dr_dtF.Table.Columns[column.ColumnName].Ordinal;
        //                                            lstColumnOrdinals.Add(index.ToString());
        //                                            count++;
        //                                            dr_dtF[index] = formattedAnswerArr[1];
        //                                        }
        //                                    }
        //                                }
        //                            }
        //                        }
        //                    }
        //                    else if (questionTypeId > 7 && questionTypeId != 15)
        //                    {
        //                        List<OptionsModel> optionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
        //                        List<MatrixTypeModel> columnDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstMatrixTypeModel;
        //                        List<DDLMatrixTypeModel> dropDownDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstDDlMatrixModel;
        //                        count = count + (optionDisplayOrder.Count * columnDisplayOrder.Count);
        //                        int qustionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).Select(v => v.DisplayOrder).FirstOrDefault();
        //                        string[] allAnswers = answerExpression.Split('|');

        //                        List<string> lstColumnOrdinals = new List<string>();

        //                        string formattedAnswer = string.Empty;
        //                        string answerValue = string.Empty;
        //                        int DDlDisplayOrder = 0;
        //                        if (allAnswers.Length > 1)
        //                        {
        //                            foreach (var ansExpression in allAnswers)
        //                            {
        //                                string displaycol = string.Empty;
        //                                string displayopt = string.Empty;

        //                                if (questionTypeId == 9 || questionTypeId == 14)
        //                                {
        //                                    foreach (var item in optionDisplayOrder)
        //                                    {
        //                                        if (Convert.ToString(item.OptionId) == ansExpression.Split('~')[0].Split('*')[0])
        //                                        {
        //                                            displaycol = Convert.ToString(item.DisplayOrder);
        //                                            break;
        //                                        }
        //                                    }
        //                                    foreach (var item in columnDisplayOrder)
        //                                    {
        //                                        if (Convert.ToString(item.Id) == ansExpression.Split('~')[1])
        //                                        {
        //                                            displayopt = Convert.ToString(item.DisplayOrder);
        //                                            break;
        //                                        }
        //                                    }
        //                                    //formattedAnswer = "Q" + qustionDisplayOrder + "_M" + displaycol + "_C" + displayopt;
        //                                    formattedAnswer = "Q" + questionId + "_M" + displaycol + "_C" + displayopt;
        //                                    answerValue = ansExpression.Split('~')[0].Split('*')[1];
        //                                    if (questionTypeId == 14)
        //                                        DDlDisplayOrder = dropDownDisplayOrder.Where(l => l.Id == Convert.ToInt32(answerValue)).FirstOrDefault().DDLDisplayOrder;
        //                                }
        //                                else
        //                                {
        //                                    foreach (var item in optionDisplayOrder)
        //                                    {
        //                                        if (Convert.ToString(item.OptionId) == ansExpression.Split('~')[0])
        //                                        {
        //                                            displaycol = Convert.ToString(item.DisplayOrder);
        //                                            break;
        //                                        }
        //                                    }
        //                                    foreach (var item in columnDisplayOrder)
        //                                    {
        //                                        if (Convert.ToString(item.Id) == ansExpression.Split('~')[1])
        //                                        {
        //                                            displayopt = Convert.ToString(item.DisplayOrder);
        //                                            break;
        //                                        }
        //                                    }
        //                                    //formattedAnswer = "Q" + qustionDisplayOrder + "_M" + displaycol + "_C" + displayopt;
        //                                    formattedAnswer = "Q" + questionId + "_M" + displaycol + "_C" + displayopt;
        //                                }
        //                                foreach (DataColumn column in dtF.Columns)
        //                                {
        //                                    string[] columnHeader = column.ColumnName.Split('-');

        //                                    if (columnHeader.Length > 1)
        //                                    {
        //                                        string[] answer = columnHeader[0].Split('_');

        //                                        if (answer.Length > 2)
        //                                        {
        //                                            string answerInHeader = answer[0] + "_" + answer[1] + "_" + answer[2];

        //                                            if (formattedAnswer == answerInHeader)
        //                                            {
        //                                                int index = dr_dtF.Table.Columns[column.ColumnName].Ordinal;
        //                                                lstColumnOrdinals.Add(index.ToString());
        //                                                //count++;
        //                                                if (questionTypeId == 9)
        //                                                {
        //                                                    dr_dtF[index] = answerValue;
        //                                                }
        //                                                else if (questionTypeId == 14)
        //                                                {
        //                                                    dr_dtF[index] = DDlDisplayOrder;
        //                                                }
        //                                                else
        //                                                {
        //                                                    dr_dtF[index] = "1";
        //                                                }
        //                                            }
        //                                        }
        //                                    }
        //                                }
        //                            }
        //                        }

        //                        // Fill All the Empty cells with 2

        //                        for (int i = 0; i <= dtF.Columns.Count - 1; i++)
        //                            if (String.IsNullOrEmpty(Convert.ToString(dr_dtF[i])))
        //                                dr_dtF[i] = "";
        //                    }

        //                    else
        //                    {
        //                        dr_dtF[count] = answerExpression;
        //                        count++;
        //                        optionsCount--;
        //                    }

        //                    if (productId != oldProductID)
        //                    {
        //                        dtF.Rows.Add(drTemp);
        //                        //  drTemp[0] = dtF.Rows.IndexOf(drTemp) + 1;
        //                        drTemp[0] = response_id;
        //                        oldProductID = productId;
        //                    }

        //                    drTemp = dr_dtF;
        //                    //---ss
        //                    if (model.projectModel.ProjectType == "2")
        //                    {
        //                        drTemp[2] = product_desc;
        //                        drTemp[1] = product_logiId;
        //                    }
        //                    //---ss

        //                }

        //                dtF.Rows.Add(dr_dtF);
        //                //dr_dtF[0] = dtF.Rows.IndexOf(dr_dtF) + 1;
        //                drTemp[0] = response_id;
        //                //---ss
        //                if (model.projectModel.ProjectType == "2")
        //                {
        //                    dr_dtF[2] = product_desc;
        //                    dr_dtF[1] = product_logiId;
        //                }
        //                response_id++;
        //                //---ss
        //            }
        //        }

        //        if (dtF != null && dtF.Rows.Count > 0)
        //        {
        //            //DataTable dtF = comMethods.GenerateTransposedTable(dt);
        //            newData = comMethods.GenerateTransposedTable(dt);
        //            foreach (DataRow dr in dtF.Rows)
        //            {
        //                if (model.projectModel.ProjectType == "2")
        //                {
        //                    DataRow rw = newData.AsEnumerable().FirstOrDefault(tt => tt.Field<string>("Resp_Id") == dr["Resp_Id"].ToString() && tt.Field<string>("Product_Name") == dr["Product_Name"].ToString());
        //                    if (rw == null)
        //                    {
        //                        DataRow dr_dtF = newData.NewRow();

        //                        newData.Rows.Add(dr_dtF);
        //                        foreach (DataColumn dcTempKano in dtF.Columns)
        //                        {

        //                            dr_dtF[dcTempKano.ToString()] = dr[dcTempKano.ToString()];

        //                        }
        //                        // row exists
        //                    }
        //                }
        //                else
        //                {
        //                    DataRow rw = newData.AsEnumerable().FirstOrDefault(tt => tt.Field<string>("Resp_Id") == dr["Resp_Id"].ToString());
        //                    if (rw == null)
        //                    {
        //                        DataRow dr_dtF = newData.NewRow();

        //                        newData.Rows.Add(dr_dtF);
        //                        foreach (DataColumn dcTempKano in dtF.Columns)
        //                        {

        //                            dr_dtF[dcTempKano.ToString()] = dr[dcTempKano.ToString()];

        //                        }
        //                        // row exists
        //                    }
        //                }


        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {

        //    }
        //    return newData;
        //}
        //#endregion

        //#region GetReportOffline for Combined
       
        //public DataTable GetRepOffline(string Id, int ResponseCount, int proId)
        //{
        //    RespondentController respondent = new RespondentController();
        //    SurveyResponseModel model = new SurveyResponseModel();
        //    DataTable dtF = new DataTable();
        //    try
        //    {
        //        DataTable dt = new DataTable();

        //        string projectGuid = string.Empty;
        //        projectGuid = Id;
        //        dt.Columns.Add("Resp_Id", typeof(string));

        //        model = respondent.GetProjectPreview(projectGuid);

        //        //--ss
        //        if (model.projectModel.ProjectType == "2")
        //        {
        //            DataRow drnew = dt.NewRow();
        //            drnew[0] = "Product_LogicalId";
        //            dt.Rows.Add(drnew);
        //            DataRow drnew1 = dt.NewRow();
        //            drnew1[0] = "Product_Name";
        //            dt.Rows.Add(drnew1);
        //        }
        //        //--

        //        // Load all the questions of this project
        //        List<QuestionList> questionsList = new List<QuestionList>();
        //        questionBase.ProjectId = Convert.ToInt32(model.projectModel.Id);
        //        actionResult = adminAction.QuestionsLoadAll_ByProjectId(questionBase);
        //        if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
        //        {
        //            foreach (DataRow dr in actionResult.dtResult.Rows)
        //            {
        //                questionsList.Add(new QuestionList
        //                {
        //                    id = dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0,
        //                    questionTypeId = dr["QuestionTypeId"] != DBNull.Value ? Convert.ToInt32(dr["QuestionTypeId"]) : 0
        //                });
        //            }
        //        }

        //        string fileName = Guid.NewGuid().ToString().Substring(0, 5) + "_" + DateTime.Now.ToString("MM-dd-yyyy") + "_" + model.projectModel.ProjectName.Replace(" ", "");

        //        if (model.lstQuestionModel != null && model.lstQuestionModel.Count > 0)
        //        {
        //            foreach (var item in model.lstQuestionModel)
        //            {
        //                if (item.PipedFrom > 0 && (!item.QuestionTitle.Contains("{{Q}}")))
        //                {
        //                    if (item.QuestionTypeId == 2 || item.QuestionTypeId == 4 || item.QuestionTypeId == 5 || item.QuestionTypeId == 6)//Multi Punch || Constant Sum || Ranking || Matrix Type
        //                    {
        //                        if (model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel != null && model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel.Count > 0)
        //                        {
        //                            foreach (var opt in model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel)
        //                            {
        //                                DataRow drnew = dt.NewRow();
        //                                drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
        //                                dt.Rows.Add(drnew);
        //                            }
        //                        }
        //                    }

        //                    else if (item.QuestionTypeId == 1 || item.QuestionTypeId == 3 || item.QuestionTypeId == 7 || item.QuestionTypeId == 15)//Single Punch || Range || Simple Text
        //                    {
        //                        DataRow drnew = dt.NewRow();
        //                        drnew[0] = "Q" + item.QuestionId + "_S" + "-" + ReplaceHtml.ReplaceText(item.QuestionTitle);
        //                        dt.Rows.Add(drnew);
        //                    }
        //                    else if (item.QuestionTypeId == 8 || item.QuestionTypeId == 12)//Single punch matrix type  && Rank order type
        //                    {
        //                        //if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
        //                        //{
        //                        //foreach (var opt in item.lstOptionsModel)
        //                        //{
        //                        //    DataRow drnew = dt.NewRow();
        //                        //    //drnew[0] = "Q" + item.QuestionId + "_M" + opt.OptionId + "-" + ReplaceHtml.ReplaceText(opt.Option);
        //                        //    drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
        //                        //    dt.Rows.Add(drnew);
        //                        //}
        //                        //}
        //                        if (item.lstMatrixTypeModel != null && item.lstMatrixTypeModel.Count > 0)
        //                        {
        //                            foreach (var col in item.lstMatrixTypeModel)
        //                            {
        //                                DataRow drnew = dt.NewRow();
        //                                //drnew[0] = "Q" + item.QuestionId + "_M" + opt.OptionId + "-" + ReplaceHtml.ReplaceText(opt.Option);
        //                                drnew[0] = "Q" + item.QuestionId + "_M" + col.DisplayOrder + "-" + ReplaceHtml.ReplaceText(col.ColumnName);
        //                                dt.Rows.Add(drnew);
        //                            }
        //                        }
        //                    }
        //                    else if (item.QuestionTypeId == 13)//Kano model matrix type 
        //                    {
        //                        if (model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel != null && model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel.Count > 0)
        //                        {
        //                            foreach (var opt in model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel)
        //                            {
        //                                DataRow drnew = dt.NewRow();
        //                                drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Present";
        //                                dt.Rows.Add(drnew);
        //                                DataRow drnew1 = dt.NewRow();
        //                                drnew1[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Absent";
        //                                dt.Rows.Add(drnew1);
        //                            }
        //                        }
        //                    }
        //                    // Single Punch Matrix Type || Range Matrix Type Question ||
        //                    // Single punch type answer horizontal position || 
        //                    // Multiple punch type answer horizontal position || 
        //                    // Rank order type question || 
        //                    // KANO Model Matrix type question || Dropdown Matrix type question
        //                    else if (item.QuestionTypeId > 7 && item.QuestionTypeId != 15)
        //                    {
        //                        if (model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel != null && model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel.Count > 0)
        //                        {
        //                            foreach (var opt in model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel)
        //                            {
        //                                foreach (var column in item.lstMatrixTypeModel)
        //                                {
        //                                    DataRow drnew = dt.NewRow();
        //                                    drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "_C" + column.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + " " + ReplaceHtml.ReplaceText(column.ColumnName);
        //                                    dt.Rows.Add(drnew);
        //                                }
        //                            }
        //                        }
        //                    }
        //                }
        //                else
        //                {
        //                    if (item.QuestionTypeId == 2 || item.QuestionTypeId == 4 || item.QuestionTypeId == 5 || item.QuestionTypeId == 6)//Multi Punch || Constant Sum || Ranking || Matrix Type
        //                    {
        //                        if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
        //                        {
        //                            foreach (var opt in item.lstOptionsModel)
        //                            {
        //                                DataRow drnew = dt.NewRow();
        //                                //drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
        //                                drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
        //                                dt.Rows.Add(drnew);
        //                            }
        //                        }
        //                    }

        //                    else if (item.QuestionTypeId == 1 || item.QuestionTypeId == 3 || item.QuestionTypeId == 7 || item.QuestionTypeId == 15)//Single Punch || Range || Simple Text
        //                    {
        //                        DataRow drnew = dt.NewRow();
        //                        //drnew[0] = "Q" + item.DisplayOrder + "_S" + "-" + ReplaceHtml.ReplaceText(item.QuestionTitle);
        //                        drnew[0] = "Q" + item.QuestionId + "_S" + "-" + ReplaceHtml.ReplaceText(item.QuestionTitle);
        //                        dt.Rows.Add(drnew);
        //                    }
        //                    else if (item.QuestionTypeId == 8 || item.QuestionTypeId == 12)//Single punch matrix type  && Rank order type
        //                    {
        //                        //if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
        //                        //{
        //                        //foreach (var opt in item.lstOptionsModel)
        //                        //{
        //                        //    DataRow drnew = dt.NewRow();
        //                        //    //drnew[0] = "Q" + item.QuestionId + "_M" + opt.OptionId + "-" + ReplaceHtml.ReplaceText(opt.Option);
        //                        //    drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
        //                        //    dt.Rows.Add(drnew);
        //                        //}
        //                        //}
        //                        if (item.lstMatrixTypeModel != null && item.lstMatrixTypeModel.Count > 0)
        //                        {
        //                            foreach (var col in item.lstMatrixTypeModel)
        //                            {
        //                                DataRow drnew = dt.NewRow();
        //                                //drnew[0] = "Q" + item.QuestionId + "_M" + opt.OptionId + "-" + ReplaceHtml.ReplaceText(opt.Option);
        //                                drnew[0] = "Q" + item.QuestionId + "_M" + col.DisplayOrder + "-" + ReplaceHtml.ReplaceText(col.ColumnName);
        //                                dt.Rows.Add(drnew);
        //                            }
        //                        }
        //                    }
        //                    else if (item.QuestionTypeId == 13)//Kano model matrix type 
        //                    {
        //                        if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
        //                        {
        //                            foreach (var opt in item.lstOptionsModel)
        //                            {
        //                                DataRow drnew = dt.NewRow();
        //                                //drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Present";
        //                                drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Present";
        //                                dt.Rows.Add(drnew);
        //                                DataRow drnew1 = dt.NewRow();
        //                                //drnew1[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Absent";
        //                                drnew1[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Absent";
        //                                dt.Rows.Add(drnew1);
        //                            }
        //                        }
        //                    }
        //                    // Single Punch Matrix Type || Range Matrix Type Question ||
        //                    // Single punch type answer horizontal position || 
        //                    // Multiple punch type answer horizontal position || 
        //                    // Rank order type question || 
        //                    // KANO Model Matrix type question || Dropdown Matrix type question
        //                    else if (item.QuestionTypeId > 7)
        //                    {
        //                        if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
        //                        {
        //                            foreach (var opt in item.lstOptionsModel)
        //                            {
        //                                foreach (var column in item.lstMatrixTypeModel)
        //                                {
        //                                    DataRow drnew = dt.NewRow();
        //                                    //drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "_C" + column.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + " " + ReplaceHtml.ReplaceText(column.ColumnName);
        //                                    drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "_C" + column.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + " " + ReplaceHtml.ReplaceText(column.ColumnName);
        //                                    dt.Rows.Add(drnew);
        //                                }
        //                            }
        //                        }
        //                    }
        //                }
        //            }
        //        }
        //        // Get the Excel Pattern of dt
        //        dtF = comMethods.GenerateTransposedTable(dt);

        //        // Get all the reponders
        //        projectBase.Id = model.projectModel.Id;
        //        actionResult = adminAction.Responders_Offline_LoadBy_ProjectId(projectBase);

        //        // Get Responses
        //        SurveyLoadBase surveyLoadBase = new SurveyLoadBase();
        //        string json = string.Empty;
        //        SRV.BaseLayer.ActionResult actionResultResponses = new SRV.BaseLayer.ActionResult();
        //        int response_id = ResponseCount;            //ss
        //        foreach (DataRow dr in actionResult.dtResult.Rows)
        //        {
        //            surveyLoadBase.Id = model.projectModel.Id;
        //            surveyLoadBase.OfflineResponderId = dr["RespId"] != DBNull.Value ? Convert.ToString(dr["RespId"]) : "";
        //            //DataRow dr_dtF = dtF.NewRow();

        //            if (String.IsNullOrEmpty(surveyLoadBase.OfflineResponderId)) continue;


        //            DataTable responsesDt = new DataTable();


        //            if (model.projectModel.lstProductProjectModel != null && model.projectModel.lstProductProjectModel.Count > 0)
        //            {
        //                foreach (var productModel in model.projectModel.lstProductProjectModel)
        //                {
        //                    surveyLoadBase.ProductId = productModel.Id;
        //                    actionResultResponses = respondentAction.Response_Offline_LoadBy_ProjectId_RespId(surveyLoadBase);
        //                    if (actionResultResponses.IsSuccess)
        //                    {
        //                        if (responsesDt.Rows.Count == 0)
        //                            responsesDt = actionResultResponses.dtResult.Copy();
        //                        else
        //                        {
        //                            foreach (DataRow drThisRespRow in actionResultResponses.dtResult.Rows)
        //                                responsesDt.ImportRow(drThisRespRow);

        //                        }
        //                    }
        //                }
        //            }
        //            else
        //            {
        //                actionResultResponses = respondentAction.Response_Offline_LoadBy_ProjectId_RespId(surveyLoadBase);
        //                if (actionResultResponses.IsSuccess)
        //                    responsesDt = actionResultResponses.dtResult.Copy();

        //            }


        //            //actionResultResponses = respondentAction.Response_Offline_LoadBy_ProjectId_RespId(surveyLoadBase);
        //            if (responsesDt.Rows.Count > 0)//actionResultResponses.IsSuccess
        //            {
        //                //dr_dtF[0] = surveyLoadBase.Email;

        //                int count = 1;
        //                count = (model.projectModel.ProjectType == "2") ? count + 2 : count;        //ss
        //                DataRow dr_dtF = dtF.NewRow();
        //                DataRow drTemp = dtF.NewRow();
        //                string product_desc = string.Empty;
        //                string product_logiId = string.Empty;
        //                int oldProductID = responsesDt.Rows[0]["ProductId"] != DBNull.Value ? Convert.ToInt32(responsesDt.Rows[0]["ProductId"]) : 0;
        //                foreach (DataRow drResponse in responsesDt.Rows)// actionResultResponses.dtResult.Rows
        //                {

        //                    int optionsCount = 0;

        //                    int productId = drResponse["ProductId"] != DBNull.Value ? Convert.ToInt32(drResponse["ProductId"]) : 0;
        //                    if (productId != oldProductID)
        //                    {
        //                        count = (model.projectModel.ProjectType == "2") ? 3 : 1;
        //                        dr_dtF = dtF.NewRow();
        //                    }

        //                    //--ss

        //                    product_desc = drResponse["ProductDescription"] != DBNull.Value ? Convert.ToString(drResponse["ProductDescription"]) : "";
        //                    product_logiId = drResponse["ProductLogicalId"] != DBNull.Value ? Convert.ToString(drResponse["ProductLogicalId"]) : "";

        //                    //--ss
        //                    int questionId = drResponse["QuestionId"] != DBNull.Value ? Convert.ToInt32(drResponse["QuestionId"]) : 0;
        //                    string answerExpression = drResponse["AnswerExpression"] != DBNull.Value ? Convert.ToString(drResponse["AnswerExpression"]) : "";
        //                    int questionTypeId = drResponse["QuestionTypeId"] != DBNull.Value ? Convert.ToInt32(drResponse["QuestionTypeId"]) : 1;
        //                    int PipedFrom = drResponse["PipedFrom"] != DBNull.Value ? Convert.ToInt32(drResponse["PipedFrom"]) : 0;
        //                    //dr_dtF[0] = drResponse;

        //                    // Get Total options count for this question
        //                    questionBase.Id = Convert.ToInt32(questionId);
        //                    SRV.BaseLayer.ActionResult actionResultAnswersTemp = new SRV.BaseLayer.ActionResult();
        //                    actionResultAnswersTemp = adminAction.Option_LoadAllById(questionBase);
        //                    if (actionResultAnswersTemp.dtResult != null && actionResultAnswersTemp.dtResult.Rows.Count > 0)
        //                    {
        //                        optionsCount = actionResultAnswersTemp.dtResult.Rows.Count;
        //                    }

        //                    if (questionTypeId == 2 || questionTypeId == 4 || questionTypeId == 5 || questionTypeId == 6)
        //                    {
        //                        string[] allAnswers = answerExpression.Split('|');
        //                        if (allAnswers.Length > 0)
        //                        {
        //                            if (questionTypeId == 2)// Multi Punch
        //                            {
        //                                string allRec = string.Empty;
        //                                //List<OptionsModel> matrixAns = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
        //                                List<OptionsModel> matrixAns = new List<OptionsModel>();
        //                                if (PipedFrom > 0 && !drResponse["QuestionTitle"].ToString().Contains("{{Q}}"))
        //                                {
        //                                    matrixAns = model.lstQuestionModel.Where(v => v.QuestionId == PipedFrom).FirstOrDefault().lstOptionsModel;
        //                                }
        //                                else
        //                                {
        //                                    matrixAns = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
        //                                }
        //                                foreach (var item in matrixAns)
        //                                {
        //                                    string matchedId = string.Empty;
        //                                    foreach (string ans in allAnswers)
        //                                    {
        //                                        if (Convert.ToString(item.OptionId) == ans)
        //                                        {
        //                                            matchedId = Convert.ToString(item.DisplayOrder);
        //                                            break;
        //                                        }
        //                                    }
        //                                    allRec += matchedId + "|";
        //                                }

        //                                string[] allAnswersMat = allRec.Substring(0, allRec.Length - 1).Split('|');
        //                                foreach (string ans in allAnswersMat)
        //                                {

        //                                    if (count <= dtF.Columns.Count - 1)
        //                                    {
        //                                        if ((Regex.Replace(dtF.Columns[count].ToString().Split('-')[0].Split('_')[1], @"[^\d]", String.Empty)) == ans)
        //                                        {
        //                                            dr_dtF[count] = "1";// ans;
        //                                            count++;
        //                                            optionsCount--;
        //                                        }
        //                                        else
        //                                        {
        //                                            dr_dtF[count] = "2";
        //                                            count++;
        //                                            optionsCount--;
        //                                        }
        //                                    }
        //                                }
        //                                //count = count + optionsCount;
        //                            }
        //                            else if (questionTypeId == 6) // matrix Type
        //                            {
        //                                foreach (string ans in allAnswers)
        //                                {
        //                                    dr_dtF[count] = (ans.ToLower().IndexOf("yes") != -1) ? "1" : "2";
        //                                    count++;
        //                                    optionsCount--;
        //                                }
        //                                count = count + optionsCount;
        //                            }
        //                            else
        //                            {
        //                                foreach (string ans in allAnswers)
        //                                {
        //                                    dr_dtF[count] = ans;
        //                                    count++;
        //                                    optionsCount--;
        //                                }
        //                                count = count + optionsCount;
        //                            }
        //                        }
        //                    }
        //                    else if (questionTypeId == 1) // Single Punch Answers
        //                    {
        //                        //List<OptionsModel> SinglePunchAns = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
        //                        List<OptionsModel> SinglePunchAns = new List<OptionsModel>();
        //                        if (PipedFrom > 0 && !drResponse["QuestionTitle"].ToString().Contains("{{Q}}"))
        //                        {
        //                            SinglePunchAns = model.lstQuestionModel.Where(v => v.QuestionId == PipedFrom).FirstOrDefault().lstOptionsModel;
        //                        }
        //                        else
        //                        {
        //                            SinglePunchAns = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
        //                        }
        //                        int ansIndex = 0;
        //                        foreach (var item in SinglePunchAns)
        //                        {
        //                            ansIndex++;
        //                            if (Convert.ToString(item.OptionId) == answerExpression)
        //                            {
        //                                break;
        //                            }
        //                        }

        //                        dr_dtF[count] = ansIndex;//answerExpression;
        //                        count++;
        //                        optionsCount--;
        //                    }
        //                    else if (questionTypeId == 8 || questionTypeId == 12) // single punch matrix answers
        //                    {
        //                        List<OptionsModel> optionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
        //                        List<MatrixTypeModel> columnDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstMatrixTypeModel;
        //                        int qustionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).Select(v => v.DisplayOrder).FirstOrDefault();
        //                        string[] allAnswers = answerExpression.Split('|');

        //                        List<string> lstColumnOrdinals = new List<string>();

        //                        string formattedAnswer = string.Empty;
        //                        string answerValue = string.Empty;
        //                        if (allAnswers.Length > 0)
        //                        {
        //                            foreach (var ansExpression in allAnswers)
        //                            {
        //                                if (!string.IsNullOrEmpty(ansExpression))
        //                                {
        //                                    string displaycol = string.Empty;
        //                                    string displayopt = string.Empty;
        //                                    foreach (var item in optionDisplayOrder)
        //                                    {

        //                                        if (Convert.ToString(item.OptionId) == ansExpression.Split('~')[0])
        //                                        {
        //                                            displaycol = Convert.ToString(item.DisplayOrder);
        //                                            break;
        //                                        }
        //                                    }
        //                                    foreach (var item in columnDisplayOrder)
        //                                    {
        //                                        if (Convert.ToString(item.Id) == ansExpression.Split('~')[1])
        //                                        {
        //                                            displayopt = Convert.ToString(item.DisplayOrder);
        //                                            break;
        //                                        }
        //                                    }
        //                                    //  formattedAnswer = "Q" + questionId + "_M" + displaycol + "#" + displayopt;
        //                                    formattedAnswer = "Q" + questionId + "_M" + displayopt + "#" + displaycol;
        //                                    foreach (DataColumn column in dtF.Columns)
        //                                    {
        //                                        string[] columnHeader = column.ColumnName.Split('-');
        //                                        string[] answer = columnHeader[0].Split('_');
        //                                        if (columnHeader.Length > 1)
        //                                        {
        //                                            string answerInHeader = answer[0] + "_" + answer[1];
        //                                            string[] formattedAnswerArr = formattedAnswer.Split('#');
        //                                            if (formattedAnswerArr[0] == answerInHeader)
        //                                            {
        //                                                int index = dr_dtF.Table.Columns[column.ColumnName].Ordinal;
        //                                                lstColumnOrdinals.Add(index.ToString());
        //                                                count++;
        //                                                dr_dtF[index] = formattedAnswerArr[1];
        //                                            }
        //                                        }
        //                                    }
        //                                }
        //                            }
        //                        }
        //                    }
        //                    else if (questionTypeId == 13) // Kano model matrix type
        //                    {
        //                        List<OptionsModel> optionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
        //                        List<MatrixTypeModel> columnDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstMatrixTypeModel;
        //                        int qustionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).Select(v => v.DisplayOrder).FirstOrDefault();
        //                        string[] allAnswers = answerExpression.Split('|');

        //                        List<string> lstColumnOrdinals = new List<string>();

        //                        string formattedAnswer = string.Empty;
        //                        string answerValue = string.Empty;
        //                        if (allAnswers.Length > 1)
        //                        {
        //                            int repeatId = 0;
        //                            foreach (var ansExpression in allAnswers)
        //                            {
        //                                string displaycol = string.Empty;
        //                                string displayopt = string.Empty;
        //                                string displayText = string.Empty;
        //                                string text = string.Empty;
        //                                if (repeatId == Convert.ToInt32(ansExpression.Split('~')[0]))
        //                                {
        //                                    text = "_Absent";
        //                                    repeatId = 0;
        //                                }
        //                                else
        //                                {
        //                                    text = "_Present";
        //                                }
        //                                foreach (var item in optionDisplayOrder)
        //                                {
        //                                    if (Convert.ToString(item.OptionId) == ansExpression.Split('~')[0])
        //                                    {
        //                                        displayText = Convert.ToString(item.Option) + text;
        //                                        displaycol = Convert.ToString(item.DisplayOrder);
        //                                        repeatId = Convert.ToInt32(ansExpression.Split('~')[0]);
        //                                        break;
        //                                    }
        //                                }
        //                                foreach (var item in columnDisplayOrder)
        //                                {
        //                                    if (Convert.ToString(item.Id) == ansExpression.Split('~')[1])
        //                                    {
        //                                        displayopt = Convert.ToString(item.DisplayOrder);
        //                                        break;
        //                                    }
        //                                }
        //                                //formattedAnswer = "Q" + qustionDisplayOrder + "_M" + displaycol + "-" + displayText + "#" + displayopt;
        //                                formattedAnswer = "Q" + questionId + "_M" + displaycol + "-" + displayText + "#" + displayopt;
        //                                foreach (DataColumn column in dtF.Columns)
        //                                {

        //                                    string[] columnHeader = column.ColumnName.Split('-');
        //                                    string[] answer = columnHeader[0].Split('_');
        //                                    if (columnHeader.Length > 1)
        //                                    {
        //                                        string answerInHeader = answer[0] + "_" + answer[1] + "-" + columnHeader[1];
        //                                        string[] formattedAnswerArr = formattedAnswer.Split('#');
        //                                        if (Regex.Replace(formattedAnswerArr[0], @"\s+", "", RegexOptions.Multiline) == Regex.Replace(answerInHeader, @"\s+", "", RegexOptions.Multiline))
        //                                        {
        //                                            int index = dr_dtF.Table.Columns[column.ColumnName].Ordinal;
        //                                            lstColumnOrdinals.Add(index.ToString());
        //                                            count++;
        //                                            dr_dtF[index] = formattedAnswerArr[1];
        //                                        }
        //                                    }
        //                                }
        //                            }
        //                        }
        //                    }
        //                    else if (questionTypeId > 7 && questionTypeId != 15)
        //                    {
        //                        List<OptionsModel> optionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
        //                        List<MatrixTypeModel> columnDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstMatrixTypeModel;
        //                        List<DDLMatrixTypeModel> dropDownDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstDDlMatrixModel;
        //                        count = count + (optionDisplayOrder.Count * columnDisplayOrder.Count);
        //                        int qustionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).Select(v => v.DisplayOrder).FirstOrDefault();
        //                        string[] allAnswers = answerExpression.Split('|');

        //                        List<string> lstColumnOrdinals = new List<string>();

        //                        string formattedAnswer = string.Empty;
        //                        string answerValue = string.Empty;
        //                        int DDlDisplayOrder = 0;
        //                        if (allAnswers.Length > 1)
        //                        {
        //                            foreach (var ansExpression in allAnswers)
        //                            {
        //                                string displaycol = string.Empty;
        //                                string displayopt = string.Empty;

        //                                if (questionTypeId == 9 || questionTypeId == 14)
        //                                {
        //                                    foreach (var item in optionDisplayOrder)
        //                                    {
        //                                        if (Convert.ToString(item.OptionId) == ansExpression.Split('~')[0].Split('*')[0])
        //                                        {
        //                                            displaycol = Convert.ToString(item.DisplayOrder);
        //                                            break;
        //                                        }
        //                                    }
        //                                    foreach (var item in columnDisplayOrder)
        //                                    {
        //                                        if (Convert.ToString(item.Id) == ansExpression.Split('~')[1])
        //                                        {
        //                                            displayopt = Convert.ToString(item.DisplayOrder);
        //                                            break;
        //                                        }
        //                                    }
        //                                    //formattedAnswer = "Q" + qustionDisplayOrder + "_M" + displaycol + "_C" + displayopt;
        //                                    formattedAnswer = "Q" + questionId + "_M" + displaycol + "_C" + displayopt;
        //                                    answerValue = ansExpression.Split('~')[0].Split('*')[1];
        //                                    if (questionTypeId == 14)
        //                                        DDlDisplayOrder = dropDownDisplayOrder.Where(l => l.Id == Convert.ToInt32(answerValue)).FirstOrDefault().DDLDisplayOrder;
        //                                }
        //                                else
        //                                {
        //                                    foreach (var item in optionDisplayOrder)
        //                                    {
        //                                        if (Convert.ToString(item.OptionId) == ansExpression.Split('~')[0])
        //                                        {
        //                                            displaycol = Convert.ToString(item.DisplayOrder);
        //                                            break;
        //                                        }
        //                                    }
        //                                    foreach (var item in columnDisplayOrder)
        //                                    {
        //                                        if (Convert.ToString(item.Id) == ansExpression.Split('~')[1])
        //                                        {
        //                                            displayopt = Convert.ToString(item.DisplayOrder);
        //                                            break;
        //                                        }
        //                                    }
        //                                    //formattedAnswer = "Q" + qustionDisplayOrder + "_M" + displaycol + "_C" + displayopt;
        //                                    formattedAnswer = "Q" + questionId + "_M" + displaycol + "_C" + displayopt;
        //                                }
        //                                foreach (DataColumn column in dtF.Columns)
        //                                {
        //                                    string[] columnHeader = column.ColumnName.Split('-');

        //                                    if (columnHeader.Length > 1)
        //                                    {
        //                                        string[] answer = columnHeader[0].Split('_');

        //                                        if (answer.Length > 2)
        //                                        {
        //                                            string answerInHeader = answer[0] + "_" + answer[1] + "_" + answer[2];

        //                                            if (formattedAnswer == answerInHeader)
        //                                            {
        //                                                int index = dr_dtF.Table.Columns[column.ColumnName].Ordinal;
        //                                                lstColumnOrdinals.Add(index.ToString());
        //                                                //count++;
        //                                                if (questionTypeId == 9)
        //                                                {
        //                                                    dr_dtF[index] = answerValue;
        //                                                }
        //                                                else if (questionTypeId == 14)
        //                                                {
        //                                                    dr_dtF[index] = DDlDisplayOrder;
        //                                                }
        //                                                else
        //                                                {
        //                                                    dr_dtF[index] = "1";
        //                                                }
        //                                            }
        //                                        }
        //                                    }
        //                                }
        //                            }
        //                        }

        //                        // Fill All the Empty cells with 2

        //                        for (int i = 0; i <= dtF.Columns.Count - 1; i++)
        //                            if (String.IsNullOrEmpty(Convert.ToString(dr_dtF[i])))
        //                                dr_dtF[i] = "";
        //                    }

        //                    else
        //                    {
        //                        dr_dtF[count] = answerExpression;
        //                        count++;
        //                        optionsCount--;
        //                    }



        //                    if (productId != oldProductID)
        //                    {
        //                        dtF.Rows.Add(drTemp);
        //                        drTemp[0] = response_id;
        //                        oldProductID = productId;
        //                    }

        //                    drTemp = dr_dtF;
        //                    //---ss
        //                    if (model.projectModel.ProjectType == "2")
        //                    {
        //                        drTemp[2] = product_desc;
        //                        drTemp[1] = product_logiId;
        //                    }
        //                    //---ss


        //                }


        //                dtF.Rows.Add(dr_dtF);
        //                drTemp[0] = response_id;
        //                //---ss
        //                if (model.projectModel.ProjectType == "2")
        //                {
        //                    dr_dtF[2] = product_desc;
        //                    dr_dtF[1] = product_logiId;
        //                }
        //                response_id++;
        //                //---ss
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {

        //    }
        //    return dtF;
        //}
        //#endregion

        //#region ConvertDatatableToXML
        //public string ConvertDatatableToXML(DataTable dt)
        //{
        //    MemoryStream str = new MemoryStream();
        //    dt.TableName = "myTable";
        //    dt.WriteXml(str, true);
        //    str.Seek(0, SeekOrigin.Begin);
        //    StreamReader sr = new StreamReader(str);
        //    string xmlstr;
        //    xmlstr = sr.ReadToEnd();
        //    return (xmlstr);
        //}
        //#endregion
    }
}
