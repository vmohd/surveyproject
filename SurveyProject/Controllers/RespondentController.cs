﻿using SRV.ActionLayer.Respondent;
using SRV.BaseLayer.Respondent;
using SurveyProject.Helper;
using SurveyProject.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SRV.BaseLayer.Admin;
using SRV.ActionLayer.Admin;
using System.Xml;
using System.Web.Helpers;
using SRV.Utility;
using System.Runtime.Caching;
using System.Configuration;
using System.Net;
using System.Globalization;

namespace SurveyProject.Controllers
{
    [UnHandledExceptionFilter]
    public class RespondentController : Controller
    {
        #region Declaration
        AdminBase adminBase = new AdminBase();
        AdminAction adminAction = new AdminAction();
        SurveyLoadBase surveyLoadBase = new SurveyLoadBase();
        SurveyQuestionBase questionBase = new SurveyQuestionBase();
        ProductProjectBase productProjectBase = new ProductProjectBase();
        RespondentBase respondent = new RespondentBase();
        SRV.BaseLayer.ActionResult actionResult = new SRV.BaseLayer.ActionResult();
        RespondentAction respondentAction = new RespondentAction();
        CommonMethods comMethods = new CommonMethods();
        ProjectBase projectBase = new ProjectBase();

        #endregion

        #region Survey Get
        [HttpGet]
        public ActionResult Survey(string surveyId, string uid = "", string _pnlId = "", string _xP = "", string _xR = "", string _xL = "", string _xQId = "", bool product = false, int? _xCount = 0, string _xS = "")
        {
            ViewBag.Count = _xCount;
            int superSurveyId = 0;
            if (!string.IsNullOrEmpty(_xS))
            {
                ViewBag.SuperProject = _xS;
                TempData["SubSurveyIds"] = GetSuperSurveyPreviewData(Convert.ToInt32(_xS));
            }
            if ((surveyId == null && uid == "") && (_xP != "" && _xR != ""))
            {
                ProjectBase projectBase = new ProjectBase();
                projectBase.Id = Convert.ToInt32(_xP);
                actionResult = adminAction.Project_LoadById(projectBase);
                if (actionResult.IsSuccess)
                {
                    DataRow dr = actionResult.dtResult.Rows[0];
                    surveyId = dr["ProjectGUID"] != DBNull.Value ? Convert.ToString(dr["ProjectGUID"]) : "";
                    superSurveyId = dr["SuperProjectId"] != DBNull.Value ? Convert.ToInt32(dr["SuperProjectId"]) : 0;
                }
                ProjectInvitationBase projectInvitationBase = new ProjectInvitationBase();
                projectInvitationBase.Id = Convert.ToInt32(_xR);
                SRV.BaseLayer.ActionResult actionResultInvi = new SRV.BaseLayer.ActionResult();
                actionResultInvi = respondentAction.ProjectInvitation_LoadByID(projectInvitationBase);
                if (actionResultInvi.IsSuccess)
                {
                    DataRow dr = actionResultInvi.dtResult.Rows[0];
                    uid = dr["InvitationGuid"] != DBNull.Value ? Convert.ToString(dr["InvitationGuid"]) : "";
                }
                ViewBag.questionId = _xQId;
                ViewBag.ExternalResponderId = _xR;
            }
            ViewBag.SuperSurvey = superSurveyId;
            SurveyResponseModel model = new SurveyResponseModel();

            model = comMethods.GetSurvey(surveyId, uid, "resp", _pnlId, superSurveyId);
            if (model.isError)
            {
                TempData["ErrorMessage"] = model.ErrorMessage;
                return View("SurveyError");
            }
            return View(model);
        }
        #endregion

        #region SurveyExtUser
        [HttpGet]
        public ActionResult SurveyExtUser(string _pnlId = "", string _xP = "", string _xR = "", string _xL = "", bool product = false, int? _xCount = 0, string _xS = "")
        {
            SurveyResponseModel model = new SurveyResponseModel();
            ViewBag.Count = _xCount;
            int responseCount = 0;
            int projectQuota = 0;
            try
            {
                if (!string.IsNullOrEmpty(_xS))
                {
                    ViewBag.SuperProject = _xS;
                    TempData["SubSurveyIds"] = GetSuperSurveyPreviewData(Convert.ToInt32(_xS));
                }
                string ref1 = Request.ServerVariables["REMOTE_HOST"];
                string hostName = Dns.GetHostName();
                string myIP = Dns.GetHostEntry(hostName).AddressList[0].ToString();
                if ((_xR.IndexOf("Resp") != -1 || _xR == ""))
                    //_xR = Guid.NewGuid().ToString().Substring(0, 5);
                    _xR = GetRandomNumbers();
                if (_xR.IndexOf("Resp") == -1 && _xR != "")
                {
                    projectBase.Id = Convert.ToInt32(_xP);
                    ViewBag.RedirectProjectId = 0;
                    string productMsg = "";
                    RespondentController respondent = new RespondentController();
                    actionResult = adminAction.Project_LoadById(projectBase);
                    if (actionResult.IsSuccess)
                    {
                        DataRow dr = actionResult.dtResult.Rows[0];

                        if (dr["ProjectQuota"] != DBNull.Value)
                        {
                            responseCount = dr["TotalResponses"] != DBNull.Value ? Convert.ToInt32(dr["TotalResponses"]) : 0;
                            bool _isValidQuota = Convert.ToInt32(dr["ProjectQuota"]) > 0;
                            projectQuota = Convert.ToInt32(dr["ProjectQuota"]);
                            if ((responseCount >= projectQuota) && _isValidQuota)      // If ProjectQuota is set then check for responses count
                            {
                                TempData["ErrorMessage"] = "We already have collected all the responses for this survey."; ;
                                return View("SurveyError");
                            }
                        }

                        string projGuid = dr["ProjectGUID"] != null ? dr["ProjectGUID"].ToString() : "";
                        model = GetProjectPreview(projGuid);
                        if (model.isError)
                        {
                            TempData["ErrorMessage"] = model.ErrorMessage;
                            return View("SurveyError");
                        }
                        string products = string.Empty;
                        if (model.projectModel.lstProductProjectModel != null && model.projectModel.lstProductProjectModel.Count > 0)
                        {
                            foreach (var prod in model.projectModel.lstProductProjectModel)
                            {
                                productMsg += prod.ProductMessage + "~";
                                // products += "{\"ProductLogicalId\":\"" + prod.ProductLogicalId + "\",\"Description\":\"" + prod.Description + "\"},";
                            }
                            productMsg = productMsg.TrimEnd('~');
                            ViewBag.productsList = productMsg;
                            // ViewBag.productsList = "{\"data\":[" + products.Trim(',') + "]}";
                        }
                        ViewBag.ProjectId = model.projectModel.Id;

                        if (model.projectModel.RedirectUrl.Contains("UID"))
                        {
                            SurveyLoadBase surveyLoadBase = new SurveyLoadBase();
                            string TotalRedirectUrl = model.projectModel.RedirectUrl;
                            int endPos = TotalRedirectUrl.IndexOf("&&");
                            int strPos = TotalRedirectUrl.IndexOf("surveyId=");
                            int end = endPos - (strPos + 9);
                            string SurveyGuid = TotalRedirectUrl.Substring(strPos + 9, end);
                            surveyLoadBase.GUID = SurveyGuid;
                            actionResult = new SRV.BaseLayer.ActionResult();
                            actionResult = respondentAction.Survey_LoadByGUID(surveyLoadBase);
                            if (actionResult.IsSuccess)
                            {
                                DataRow drRedirectSurvey = actionResult.dtResult.Rows[0];
                                ViewBag.RedirectProjectId = drRedirectSurvey["Id"] != DBNull.Value ? Convert.ToInt32(drRedirectSurvey["Id"]) : 0;
                            }
                        }
                        else if (model.projectModel.RedirectUrl != "")
                        {
                            if (model.projectModel.RedirectParameters != "" || model.projectModel.IsRedirectParameter != false)
                            {
                                string[] redirectParamArr = model.projectModel.RedirectParameters.Split(',');
                                if (redirectParamArr.Length > 0)
                                {
                                    for (int i = 0; i <= redirectParamArr.Length - 1; i++)
                                    {
                                        if (redirectParamArr[i] == "_xP")
                                        {
                                            model.projectModel.RedirectUrl = (model.projectModel.RedirectUrl) + "&_xP=" + model.projectModel.Id;
                                        }
                                        if (redirectParamArr[i] == "_xR")
                                        {
                                            model.projectModel.RedirectUrl = (model.projectModel.RedirectUrl) + "&_xR=" + _xR;
                                        }
                                        if (redirectParamArr[i] == "_xL")
                                        {
                                            model.projectModel.RedirectUrl = (model.projectModel.RedirectUrl) + "&_xL=" + model.projectModel.LanguageId;
                                        }
                                    }
                                }
                                if (model.projectModel.IsRedirectParameter == true)
                                {
                                    if (model.projectModel.RedirectUrl.IndexOf("_xP") == -1)
                                    {
                                        model.projectModel.RedirectUrl = (model.projectModel.RedirectUrl) + "&_xP=" + model.projectModel.Id;
                                    }
                                    if (model.projectModel.RedirectUrl.IndexOf("_xR") == -1)
                                    {
                                        model.projectModel.RedirectUrl = (model.projectModel.RedirectUrl) + "&_xR=" + _xR;
                                    }
                                }
                            }
                            if (model.projectModel.RedirectUrl.IndexOf("_xP") == -1)
                            {
                                if (model.projectModel.RedirectUrl.IndexOf('?') == -1)
                                    model.projectModel.RedirectUrl = (model.projectModel.RedirectUrl) + "?_xP=" + model.projectModel.Id;
                                else
                                    model.projectModel.RedirectUrl = (model.projectModel.RedirectUrl) + "&_xP=" + model.projectModel.Id;
                            }
                            if (model.projectModel.RedirectUrl.IndexOf("_xR") == -1)
                            {
                                model.projectModel.RedirectUrl = (model.projectModel.RedirectUrl) + "&_xR=" + _xR;
                            }
                            if (model.projectModel.RedirectUrl.IndexOf("_xL") == -1)
                            {
                                model.projectModel.RedirectUrl = (model.projectModel.RedirectUrl) + "&_xL=" + model.projectModel.LanguageId;
                            }
                        }
                    }
                    ViewBag.ExternalResponderId = _xR;
                    model.pnlId = _pnlId;
                }
                else
                {
                    TempData["ErrorMessage"] = "You are not a requested user !!";
                    return View("SurveyError");
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return View(model);
        }
        #endregion

        #region GetSuperSurveyPreviewData
        public string GetSuperSurveyPreviewData(int? id = 0)
        {
            //---------List of sub survey
            List<CreateProjectModel> lstCreateProjectModel = new List<CreateProjectModel>();
            int UserId = Convert.ToInt32(Session["UserId"]);
            string SubSurveyIds = string.Empty;
            adminBase.Id = UserId;
            actionResult = adminAction.Poject_LoadAll(adminBase);
            if (actionResult.dsResult != null && actionResult.dsResult.Tables[0].Rows.Count > 0)
            {
                DataTable dtResult = actionResult.dsResult.Tables[0].Select("SuperProjectId is Not Null and SuperProjectId = '" + Convert.ToInt32(id) + "'").CopyToDataTable();
                if (dtResult != null && dtResult.Rows.Count > 0)
                {
                    foreach (DataRow dr in dtResult.Rows)
                    {
                        if (dr["SuperProjectId"] != DBNull.Value && Convert.ToInt32(dr["SuperProjectId"]) != 0)
                        {
                            lstCreateProjectModel.Add(new CreateProjectModel
                            {
                                Id = dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0,
                                ProjectName = dr["ProjectName"] != DBNull.Value ? Convert.ToString(dr["ProjectName"]) : "",
                                ProjectQuota = dr["ProjectQuota"] != DBNull.Value ? Convert.ToInt32(dr["ProjectQuota"]) : 0,
                                SuperProjectId = dr["SuperProjectId"] != DBNull.Value ? Convert.ToInt32(dr["SuperProjectId"]) : 0,
                                ProjectType = dr["ProjectType"] != DBNull.Value ? Convert.ToString(dr["ProjectType"]) : "",
                                ProjectGuid = dr["ProjectGuid"] != DBNull.Value ? Convert.ToString(dr["ProjectGuid"]) : "",
                                ProductsCount = dr["ProductsCount"] != DBNull.Value ? Convert.ToInt32(dr["ProductsCount"]) : 0,
                                DisplayOrder = dr["DisplayOrder"] != DBNull.Value ? Convert.ToInt32(dr["DisplayOrder"]) : 0
                            });
                        }
                    }
                    lstCreateProjectModel = lstCreateProjectModel.OrderBy(l => l.DisplayOrder).ToList();
                }
                foreach (var projId in lstCreateProjectModel)
                {
                    List<ProductProjectModel> lstProductProjectModel = new List<ProductProjectModel>();
                    productProjectBase.ProjectId = Convert.ToInt32(projId.Id);
                    SRV.BaseLayer.ActionResult actionResultProducts = new SRV.BaseLayer.ActionResult();
                    actionResultProducts = adminAction.ProductProject_LoadById(productProjectBase);
                    if (actionResultProducts.IsSuccess)
                    {
                        foreach (DataRow drProd in actionResultProducts.dtResult.Rows)
                        {
                            lstProductProjectModel.Add(new ProductProjectModel
                            {
                                Id = drProd["Id"] != DBNull.Value ? Convert.ToInt32(drProd["Id"]) : 0,
                                ProjectId = drProd["ProjectId"] != DBNull.Value ? Convert.ToInt32(drProd["ProjectId"]) : 0,
                                DisplayOrder = drProd["DisplayOrder"] != DBNull.Value ? Convert.ToInt32(drProd["DisplayOrder"]) : 0,
                                Description = drProd["Description"] != DBNull.Value ? Convert.ToString(drProd["Description"]) : string.Empty,
                                ProductLogicalId = drProd["ProductLogicalId"] != DBNull.Value ? Convert.ToInt32(drProd["ProductLogicalId"]) : 0,
                            });
                        }
                        lstProductProjectModel = lstProductProjectModel.OrderBy(l => l.DisplayOrder).ToList();
                    }
                    if (lstProductProjectModel.Count > 0)
                    {
                        foreach (var item in lstProductProjectModel)
                        {
                            SubSurveyIds += "" + Convert.ToString(projId.Id) + "-" + Convert.ToString(item.ProductLogicalId) + " " + item.Description + ",";
                        }
                    }
                    else
                    {
                        SubSurveyIds += "" + Convert.ToString(projId.Id) + "-" + "" + ",";
                    }
                }
            }
            return SubSurveyIds.Trim(',');
        }
        #endregion


        public static string GetRandomNumbers()
        {
            Random random = new Random();

            Random r = new Random();
            int randNum = r.Next(1000000);
            string sixDigitNumber = randNum.ToString("D6");

            return sixDigitNumber;
        }

        #region SaveSurvey Post
        [HttpPost]
        public JsonResult SaveSurvey(string surveyResponse, string responderEmail, int Id, bool isAnswer, int projectId, string MaxResponsesCount, int? SurveyProductId = 0, int? PositionTblId = 0, string pnlId = "", bool? IsExternal = false, string extRespId = "", string terminate = "")
        {
            if (extRespId != "" && (responderEmail == null || responderEmail == string.Empty))
                IsExternal = true;
            else
                IsExternal = false;
            SurveyResponseBase surveyResponseBase = new SurveyResponseBase();
            string json = string.Empty;
            string xml = string.Empty;
            json = "{\"Status\":\"-1\"}";
            // Make proper xml for saving the response.
            try
            {
                if (responderEmail != "")
                {
                    surveyResponseBase.QuotaId = Id;
                    surveyResponseBase.Email = responderEmail;
                    actionResult = respondentAction.ProjectQuotaOther_Update(surveyResponseBase);
                    if (isAnswer)
                    {
                        string[] responses = surveyResponse.TrimEnd(',').Split(',');
                        if (responses.Length > 0)
                        {
                            foreach (string str in responses)
                            {
                                string[] quesAns = str.Split('-');
                                if (str.IndexOf('-') == -1)
                                {
                                    quesAns = quesAns.Concat(new string[] { "" }).ToArray();
                                }
                                if (!String.IsNullOrEmpty(quesAns[0]))
                                    xml += "<QuestAnswers><QuestionId>" + quesAns[0] + "</QuestionId>" + "<AnswerExpression>" + (!String.IsNullOrEmpty(quesAns[1]) ? quesAns[1] : "") + "</AnswerExpression>" + "</QuestAnswers>";
                            }
                        }
                        xml = "<Responses>" + xml + "</Responses>";

                        // Set Base layer                    
                        surveyResponseBase.Xml = xml;
                        surveyResponseBase.ResponsesCountXml = MaxResponsesCount;
                        surveyResponseBase.ProjectId = projectId;
                        surveyResponseBase.ProductId = Convert.ToInt32(SurveyProductId);
                        surveyResponseBase.PositionTblId = Convert.ToInt32(PositionTblId);
                        surveyResponseBase.PnlId = Convert.ToString(pnlId);
                        surveyResponseBase.IsExternal = Convert.ToBoolean(IsExternal);
                        if (surveyResponseBase.IsExternal)
                            surveyResponseBase.ExternalResponderId = Convert.ToString(extRespId);
                        surveyResponseBase.Terminate = Convert.ToString(terminate);
                        // Execute Method
                        actionResult = new SRV.BaseLayer.ActionResult();
                        actionResult = respondentAction.SurveyResponse_Insert_Update(surveyResponseBase);
                    }
                    if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                    {
                        DataRow dr = actionResult.dtResult.Rows[0];
                        if (dr[0] != DBNull.Value && dr[0].ToString() == "1")
                        {
                            json = "{\"Status\":\"1\"}";
                        }
                        else if (dr[0] != DBNull.Value && dr[0].ToString() == "-10")
                        {
                            json = "{\"Status\":\"-1\",\"ErrorMessage\":\"You have already been responded to this survey.\"}";
                        }
                    }
                }
                else //if (extRespId != "")
                {
                    surveyResponseBase.QuotaId = Id;
                    surveyResponseBase.Email = responderEmail;
                    surveyResponseBase.ExternalResponderId = extRespId;
                    if (Convert.ToBoolean(IsExternal))
                        actionResult = respondentAction.ProjectQuotaOther_UpdateByExUser(surveyResponseBase);
                    else if (!Convert.ToBoolean(IsExternal))
                        actionResult = respondentAction.ProjectQuotaOther_Update(surveyResponseBase);

                    if (isAnswer)
                    {
                        string[] responses = surveyResponse.TrimEnd(',').Split(',');
                        if (responses.Length > 0)
                        {
                            foreach (string str in responses)
                            {
                                string[] quesAns = str.Split('-');
                                if (str.IndexOf('-') == -1)
                                {
                                    quesAns = quesAns.Concat(new string[] { "" }).ToArray();
                                }
                                if (!String.IsNullOrEmpty(quesAns[0]))
                                    xml += "<QuestAnswers><QuestionId>" + quesAns[0] + "</QuestionId>" + "<AnswerExpression>" + (!String.IsNullOrEmpty(quesAns[1]) ? quesAns[1] : "") + "</AnswerExpression>" + "</QuestAnswers>";
                            }
                        }
                        xml = "<Responses>" + xml + "</Responses>";

                        // Set Base layer                    
                        surveyResponseBase.Xml = xml;
                        surveyResponseBase.ResponsesCountXml = MaxResponsesCount;
                        surveyResponseBase.ProjectId = projectId;
                        surveyResponseBase.ProductId = Convert.ToInt32(SurveyProductId);
                        surveyResponseBase.PositionTblId = Convert.ToInt32(PositionTblId);
                        surveyResponseBase.PnlId = Convert.ToString(pnlId);
                        surveyResponseBase.IsExternal = Convert.ToBoolean(IsExternal);
                        surveyResponseBase.ExternalResponderId = Convert.ToString(extRespId);
                        surveyResponseBase.Terminate = Convert.ToString(terminate);
                        // Execute Method
                        actionResult = new SRV.BaseLayer.ActionResult();
                        actionResult = respondentAction.SurveyResponse_Insert_Update(surveyResponseBase);
                    }
                    if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                    {
                        DataRow dr = actionResult.dtResult.Rows[0];
                        if (dr[0] != DBNull.Value && dr[0].ToString() == "1")
                        {
                            json = "{\"Status\":\"1\"}";
                        }
                        else if (dr[0] != DBNull.Value && dr[0].ToString() == "-10")
                        {
                            json = "{\"Status\":\"-1\",\"ErrorMessage\":\"You have already been responded to this survey.\"}";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return Json(json, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Dashboard Get
        public ActionResult Dashboard()
        {
            return View();
        }
        #endregion

        #region ConditionExpression Get
        public JsonResult ConditionExpression(int? Id = 0)
        {
            ManageAnswerValues manage = new ManageAnswerValues();
            ManageAnswerValuesMatrix manageMatrix = new ManageAnswerValuesMatrix();
            string json = string.Empty;
            var answersValueLst = new List<AnswersValue>();
            var answersValueWithQuotaLst = new List<AnswersValueWithQuota>();
            var answersValueMatrixLst = new List<AnswersValueMatrix>();
            var answersValueWithQuotaMatrixLst = new List<AnswersValueWithQuotaMatrix>();

            ProjecQuotaBase quotaBase = new ProjecQuotaBase();
            try
            {
                quotaBase.ProjectId = Id.GetValueOrDefault();
                actionResult = respondentAction.ConditionExpression_LoadByProjectId(quotaBase);
                if (actionResult.IsSuccess)
                {
                    foreach (DataRow dr in actionResult.dtResult.Rows)
                    {
                        string conditionExpression = dr["ConditionExpression"] != DBNull.Value ? dr["ConditionExpression"].ToString() : "";
                        if (conditionExpression != "")
                        {
                            string[] conditionExpressionArray = conditionExpression.Split('[');
                            if (conditionExpressionArray != null && conditionExpressionArray.Length > 0)
                            {
                                manage.ProjectId = conditionExpressionArray[0] != null ? Convert.ToInt32(conditionExpressionArray[0]) : 0;
                                if (!String.IsNullOrEmpty(conditionExpressionArray[1]))
                                {
                                    string[] values = conditionExpressionArray[1].Split('{');
                                    if (values != null && values.Length > 0)
                                    {
                                        if (!String.IsNullOrEmpty(values[0]))
                                        {
                                            string[] onlyValues = values[0].TrimEnd('|').Split('|');
                                            if (onlyValues != null && onlyValues.Length > 0)
                                            {
                                                foreach (var item in onlyValues)
                                                {
                                                    string QuestionId = item.Substring(1, item.Length - 2);
                                                    answersValueLst.Add(new Models.AnswersValue
                                                    {
                                                        QuestionId = Convert.ToInt32(QuestionId)
                                                    });
                                                }
                                            }
                                        }
                                        if (!String.IsNullOrEmpty(values[1]))
                                        {
                                            string[] valueWithQuota = values[1].Substring(0, values[1].Length - 2).Split('(');

                                            if (valueWithQuota != null && valueWithQuota.Length > 0)
                                            {
                                                for (int i = 1; i <= valueWithQuota.Length - 1; i++)
                                                {
                                                    valueWithQuota[i] = valueWithQuota[i].Substring(0, valueWithQuota[i].Length - 1);
                                                    string[] quota = valueWithQuota[i].Split(',');
                                                    if (quota != null && quota.Length > 0)
                                                    {
                                                        answersValueWithQuotaLst.Add(new Models.AnswersValueWithQuota
                                                        {
                                                            QuestionId = Convert.ToInt32(quota[0]),
                                                            optionId = Convert.ToInt32(quota[1])
                                                        });
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        string conditionExpressionMatrix = dr["ConditionExpressionMatrix"] != DBNull.Value ? dr["ConditionExpressionMatrix"].ToString() : "";
                        if (conditionExpressionMatrix != "")
                        {
                            string[] conditionExpressionMatrixArray = conditionExpressionMatrix.Split('[');
                            if (conditionExpressionMatrixArray != null && conditionExpressionMatrixArray.Length > 0)
                            {
                                manageMatrix.ProjectId = conditionExpressionMatrixArray[0] != null && !String.IsNullOrEmpty(conditionExpressionMatrixArray[0]) ? Convert.ToInt32(conditionExpressionMatrixArray[0]) : 0;
                                if (!String.IsNullOrEmpty(conditionExpressionMatrixArray[1]))
                                {
                                    string[] values = conditionExpressionMatrixArray[1].Split('{');
                                    if (values != null && values.Length > 0)
                                    {
                                        if (!String.IsNullOrEmpty(values[0]))
                                        {
                                            string[] onlyValues = values[0].TrimEnd('|').Split('|');
                                            if (onlyValues != null && onlyValues.Length > 0)
                                            {
                                                foreach (var item in onlyValues)
                                                {
                                                    string QuestionId = item.Substring(1, item.Length - 2);
                                                    answersValueMatrixLst.Add(new Models.AnswersValueMatrix
                                                    {
                                                        QuestionId = Convert.ToString(QuestionId)
                                                    });
                                                }
                                            }
                                        }
                                        if (!String.IsNullOrEmpty(values[1]))
                                        {
                                            string[] valueWithQuota = values[1].Substring(0, values[1].Length - 2).Split('(');

                                            if (valueWithQuota != null && valueWithQuota.Length > 0)
                                            {
                                                for (int i = 1; i <= valueWithQuota.Length - 1; i++)
                                                {
                                                    valueWithQuota[i] = valueWithQuota[i].Substring(0, valueWithQuota[i].Length - 1);
                                                    string[] quota = valueWithQuota[i].Split(',');
                                                    if (quota != null && quota.Length > 0)
                                                    {
                                                        answersValueWithQuotaMatrixLst.Add(new Models.AnswersValueWithQuotaMatrix
                                                        {
                                                            QuestionId = Convert.ToString(quota[0]),
                                                            optionId = Convert.ToString(quota[1])
                                                        });
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    json = Newtonsoft.Json.JsonConvert.SerializeObject(new { answersValueLst, answersValueWithQuotaLst, answersValueMatrixLst, answersValueWithQuotaMatrixLst });
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return Json(json, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Surveys Get
        [HttpGet]
        public ActionResult Surveys()
        {
            CreateProjectModel model = new CreateProjectModel();
            List<CreateProjectModel> ProjectLst = new List<CreateProjectModel>();
            model.ProjectList = ProjectLst;
            try
            {
                {
                    int UserId = Convert.ToInt32(Session["UserId"]);
                    respondent.Id = UserId;
                    actionResult = respondentAction.Projects_LoadByResponderId(respondent);
                    if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                    {
                        foreach (DataRow dr in actionResult.dtResult.Rows)
                        {
                            model = new CreateProjectModel();
                            model.Id = Convert.ToInt32(dr["Id"]);
                            model.ProjectName = dr["ProjectName"] != DBNull.Value ? Convert.ToString(dr["ProjectName"]) : "";
                            model.ProjectGuid = dr["ProjectGuid"] != DBNull.Value ? Convert.ToString(dr["ProjectGuid"]) : "";
                            model.InvitationGuid = dr["InvitationGuid"] != DBNull.Value ? Convert.ToString(dr["InvitationGuid"]) : "";
                            ProjectLst.Add(model);
                        }
                        model.ProjectList = ProjectLst;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return View(model);
        }
        #endregion

        #region ViewSurvey Get
        [HttpGet]
        public ActionResult ViewSurvey(string surveyId, string uid = "")
        {
            SurveyResponseModel model = new SurveyResponseModel();
            string productIds = "";
            string productNames = "";
            try
            {
                model = comMethods.GetSurvey(surveyId, uid);

                if (model.projectModel.lstProductProjectModel != null && model.projectModel.lstProductProjectModel.Count > 0)
                    foreach (var itm in model.projectModel.lstProductProjectModel)
                    {
                        productIds += Convert.ToString(itm.Id) + ",";
                        productNames += Convert.ToString(itm.ProductLogicalId) + ",";
                    }

                ViewBag.productList = productIds;
                ViewBag.ProductNameList = productNames;
                ViewBag.SurveyId = surveyId;
                ViewBag.uid = uid;

                //if (model.isError)
                //{
                //    TempData["ErrorMessage"] = model.ErrorMessage;
                //    return View("SurveyError");
                //}
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return View(model);
        }
        #endregion

        #region GetAnswers JsonResult
        public JsonResult GetAnswers(int projectId, string email, int? productId = 0, string ExternalResponderId = null)
        {
            surveyLoadBase.Id = projectId;
            surveyLoadBase.Email = email;
            surveyLoadBase.ProductId = productId.GetValueOrDefault();
            surveyLoadBase.ExternalResponderId = ExternalResponderId;
            string json = string.Empty;

            actionResult = respondentAction.Response_LoadByProjectId(surveyLoadBase);
            if (actionResult.IsSuccess)
            {
                foreach (DataRow dr in actionResult.dtResult.Rows)
                {
                    string questionId = dr["QuestionId"] != DBNull.Value ? Convert.ToString(dr["QuestionId"]) : "0";
                    string answerExpression = dr["AnswerExpression"] != DBNull.Value ? Convert.ToString(dr["AnswerExpression"]) : "0";
                    json = json + "{\"QuestionId\":\"" + questionId + "\",\"AnswerExpression\":\"" + answerExpression + "\"},";
                }
            }
            json = "{\"data\":[" + json.TrimEnd(',') + "]}";
            return Json(json, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region GetOfflineAnswers JsonResult
        public JsonResult GetOfflineAnswers(int projectId, string respId, int? productId = 0)
        {
            surveyLoadBase.Id = projectId;
            surveyLoadBase.OfflineResponderId = respId;
            surveyLoadBase.ProductId = productId.GetValueOrDefault();
            string json = string.Empty;
            actionResult = respondentAction.Response_Offline_LoadBy_ProjectId_RespId(surveyLoadBase);
            if (actionResult.IsSuccess)
            {
                foreach (DataRow dr in actionResult.dtResult.Rows)
                {
                    string questionId = dr["QuestionId"] != DBNull.Value ? Convert.ToString(dr["QuestionId"]) : "0";
                    string answerExpression = dr["AnswerExpression"] != DBNull.Value ? Convert.ToString(dr["AnswerExpression"]) : "0";
                    json = json + "{\"QuestionId\":\"" + questionId + "\",\"AnswerExpression\":\"" + answerExpression + "\"},";
                }
            }
            json = "{\"data\":[" + json.TrimEnd(',') + "]}";
            return Json(json, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region LanguageConversion
        public JsonResult LanguageConversion(int? Id = 0)
        {
            return Json(Helper.LanguageHelper.ConvertLanguage(Id), JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region ChangeUILang
        public JsonResult ChangeUILang(string lang)
        {
            string languageAbbr = string.Empty;
            try
            {
                if (String.IsNullOrEmpty(lang))
                    languageAbbr = "en";
                else
                    languageAbbr = Convert.ToString(lang);
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }

            return Json(Helper.LanguageHelper.ApplyLanguage(languageAbbr), JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region GetProjectPreview
        public SurveyResponseModel GetProjectPreview(string surveyId)
        {
            SurveyResponseModel model = new SurveyResponseModel();
            CreateProjectModel projectModel = new CreateProjectModel();
            ProjectSettingsModel projectSettingsModel = new ProjectSettingsModel();
            Responder responder = new Responder();
            List<QuestionModel> lstQuestionModel = new List<QuestionModel>();
            List<ProjectQuotaOther> lstProjectQuotaOtherModel = new List<ProjectQuotaOther>();
            List<MatrixTypeModel> lstMatxModel = new List<MatrixTypeModel>();
            List<MatrixTypeGroupModel> lstMatxTypeGroupModel = new List<MatrixTypeGroupModel>();
            QuestionBase questionBaseQ = new QuestionBase();
            List<ProductProjectModel> lstProductProjectModel = new List<ProductProjectModel>();

            projectModel.lstProductProjectModel = lstProductProjectModel;
            model.projectModel = projectModel;
            model.projectSettingsModel = projectSettingsModel;
            model.responder = responder;
            model.lstQuestionModel = lstQuestionModel;
            model.lstProjectQuotaOtherModel = lstProjectQuotaOtherModel;

            try
            {
                surveyLoadBase.GUID = surveyId;
                actionResult = new SRV.BaseLayer.ActionResult();
                actionResult = respondentAction.Survey_LoadByGUID(surveyLoadBase);
                if (actionResult.IsSuccess)
                {
                    DataRow dr = actionResult.dtResult.Rows[0];

                    //if (dr["OpeningDate"] != DBNull.Value && !String.IsNullOrEmpty(Convert.ToString(dr["OpeningDate"])))
                    //{
                    //    if (Convert.ToDateTime(dr["OpeningDate"]) > DateTime.Now.Date)
                    //    {
                    //        model.isError = true;
                    //        TempData["ErrorMessage"] = "The survey is not started yet.";
                    //        return model;
                    //        //return View("SurveyError");
                    //    }
                    //}
                    //if (dr["ClosingDate"] != DBNull.Value && !String.IsNullOrEmpty(Convert.ToString(dr["ClosingDate"])))
                    //{
                    //    if (Convert.ToDateTime(dr["ClosingDate"]) <= DateTime.Now.Date)
                    //    {
                    //        model.isError = true;
                    //        TempData["ErrorMessage"] = "The survey has been closed.";
                    //        return model;
                    //        //return View("SurveyError");
                    //    }
                    //}
                    //if (!Convert.ToBoolean(dr["IsActive"]))
                    //{
                    //    model.isError = true;
                    //    TempData["ErrorMessage"] = "The survey is in-active.";
                    //    return model;
                    //    //return View("SurveyError");
                    //}

                    // Assign project related values
                    projectModel.Id = dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0;
                    projectModel.ProjectGuid = dr["ProjectGUID"] != DBNull.Value ? Convert.ToString(dr["ProjectGUID"]) : "";
                    projectModel.ProjectName = dr["ProjectName"] != DBNull.Value ? Convert.ToString(dr["ProjectName"]) : "";
                    projectModel.WelcomeMessage = (dr["WelcomeMessage"] != DBNull.Value && !String.IsNullOrEmpty(dr["WelcomeMessage"].ToString())) ? Convert.ToString(dr["WelcomeMessage"]) : Email.ReadFile("Welcome.txt");
                    projectModel.PageHeader = (dr["PageHeader"] != DBNull.Value && !String.IsNullOrEmpty(dr["PageHeader"].ToString())) ? Convert.ToString(dr["PageHeader"]) : Email.ReadFile("Header.txt");
                    projectModel.PageFooter = (dr["PageFooter"] != DBNull.Value && !String.IsNullOrEmpty(dr["PageFooter"].ToString())) ? Convert.ToString(dr["PageFooter"]) : Email.ReadFile("Footer.txt");
                    projectModel.ProjectCompletionMesssage = (dr["ProjectCompletionMesssage"] != DBNull.Value && !String.IsNullOrEmpty(dr["ProjectCompletionMesssage"].ToString())) ? Convert.ToString(dr["ProjectCompletionMesssage"]) : Email.ReadFile("Complete.txt");
                    projectModel.TerminationMessageUpper = Email.ReadFile("TerminateUpper.txt");
                    projectModel.TerminationMessageLower = Email.ReadFile("TerminateLower.txt");
                    projectModel.RedirectUrl = dr["RedirectUrl"] != DBNull.Value ? Convert.ToString(dr["RedirectUrl"]) : "";
                    projectModel.ProjectType = dr["ProjectType"] != DBNull.Value ? Convert.ToString(dr["ProjectType"]) : "1";
                    projectModel.IsIncludePageHeader = dr["IsIncludePageHeader"] != DBNull.Value ? Convert.ToBoolean(dr["IsIncludePageHeader"]) : false;
                    projectModel.IsIncludePageFooter = dr["IsIncludePageFooter"] != DBNull.Value ? Convert.ToBoolean(dr["IsIncludePageFooter"]) : false;
                    projectModel.IsIncludeProjectCompletionMesssage = dr["IsIncludeProjectCompletionMesssage"] != DBNull.Value ? Convert.ToBoolean(dr["IsIncludeProjectCompletionMesssage"]) : false;
                    projectModel.IsIncludeTerminationMessage = dr["IsIncludeTerminationMessage"] != DBNull.Value ? Convert.ToBoolean(dr["IsIncludeTerminationMessage"]) : false;
                    projectModel.IsIncludeWelcomeMessage = dr["IsIncludeWelcomeMessage"] != DBNull.Value ? Convert.ToBoolean(dr["IsIncludeWelcomeMessage"]) : false;
                    projectModel.LanguageId = dr["LanguageId"] != DBNull.Value ? Convert.ToInt32(dr["LanguageId"]) : 1;
                    projectModel.RedirectParameters = dr["RedirectParameters"] != DBNull.Value ? Convert.ToString(dr["RedirectParameters"]) : "";
                    projectModel.IsRedirectParameter = dr["IsRedirectParameter"] != DBNull.Value ? Convert.ToBoolean(dr["IsRedirectParameter"]) : false;
                    projectModel.IsIncludeFinishImage = dr["IsIncludeFinishImage"] != DBNull.Value ? Convert.ToBoolean(dr["IsIncludeFinishImage"]) : false;
                    projectModel.CloseButtonText = dr["CloseButtonText"] != DBNull.Value ? Convert.ToString(dr["CloseButtonText"]) : "";
                    projectModel.QuotaQues = dr["QuotaQues"] != DBNull.Value ? Convert.ToString(dr["QuotaQues"]) : "Please choose one of it and continue..";
                    // projectModel.ProductMessage = (dr["ProductMessage"] != DBNull.Value && !String.IsNullOrEmpty(dr["ProductMessage"].ToString())) ? Convert.ToString(dr["ProductMessage"]) : "";
                    model.projectModel = projectModel;

                    // Assign project settings related values
                    projectSettingsModel.OpeningDate = dr["OpeningDate"] != DBNull.Value ? Convert.ToDateTime(dr["OpeningDate"]).ToString("MM/dd/yyyy") : "";
                    projectSettingsModel.ClosingDate = dr["ClosingDate"] != DBNull.Value ? Convert.ToDateTime(dr["ClosingDate"]).ToString("MM/dd/yyyy") : "";
                    projectSettingsModel.DisableQuestionNumbering = dr["DisableQuestionNumbering"] != DBNull.Value ? Convert.ToBoolean(dr["DisableQuestionNumbering"]) : false;
                    projectSettingsModel.IsActive = dr["IsActive"] != DBNull.Value ? Convert.ToBoolean(dr["IsActive"]) : false;
                    projectSettingsModel.Scored = dr["Scored"] != DBNull.Value ? Convert.ToBoolean(dr["Scored"]) : false;
                    projectSettingsModel.PrevNextPageNavigation = dr["PrevNextPageNavigation"] != DBNull.Value ? Convert.ToBoolean(dr["PrevNextPageNavigation"]) : false;
                    projectSettingsModel.ResumeOfProgress = dr["ResumeOfProgress"] != DBNull.Value ? Convert.ToBoolean(dr["ResumeOfProgress"]) : false;
                    projectSettingsModel.TestMode = dr["TestMode"] != DBNull.Value ? Convert.ToBoolean(dr["TestMode"]) : false;
                    model.projectSettingsModel = projectSettingsModel;

                    if (!String.IsNullOrEmpty(Convert.ToString(projectSettingsModel.OpeningDate))) // Check if survey has not started yet.
                    {
                        if (DateTime.ParseExact(projectSettingsModel.OpeningDate, "MM/dd/yyyy", null) > DateTime.Now.Date)
                        {
                            model.isError = true;
                            model.ErrorMessage = "The survey is not started yet.";
                        }
                    }
                    if (!String.IsNullOrEmpty(Convert.ToString(projectSettingsModel.ClosingDate))) // Check if survey has been closed.
                    {
                        if (DateTime.ParseExact(projectSettingsModel.ClosingDate, "MM/dd/yyyy", null) <= DateTime.Now.Date)
                        {
                            model.isError = true;
                            model.ErrorMessage = "The survey has been closed.";
                        }
                    }
                    if (!projectSettingsModel.IsActive) // Check if survey is inactive.
                    {
                        model.isError = true;
                        model.ErrorMessage = "The survey is in-active.";
                    }
                    // Load all of the Products of This Survey
                    if (projectModel.ProjectType == "2")
                    {
                        SRV.BaseLayer.ActionResult actionResultProjectProducts = new SRV.BaseLayer.ActionResult();
                        ProductProjectBase productProjectBase = new ProductProjectBase();
                        productProjectBase.ProjectId = projectModel.Id;
                        actionResultProjectProducts = adminAction.ProductProject_LoadById(productProjectBase);
                        if (actionResultProjectProducts.IsSuccess)
                        {
                            foreach (DataRow drProduct in actionResultProjectProducts.dtResult.Rows)
                            {
                                projectModel.lstProductProjectModel.Add(new ProductProjectModel
                                {
                                    Id = drProduct["Id"] != DBNull.Value ? Convert.ToInt32(drProduct["Id"]) : 0,
                                    ProductLogicalId = drProduct["ProductLogicalId"] != DBNull.Value ? Convert.ToInt32(drProduct["ProductLogicalId"]) : 0,
                                    Description = drProduct["Description"] != DBNull.Value ? Convert.ToString(drProduct["Description"]) : string.Empty,
                                    DisplayOrder = drProduct["DisplayOrder"] != DBNull.Value ? Convert.ToInt32(drProduct["DisplayOrder"]) : 0,
                                    ProductMessage = drProduct["ProductMessage"] != DBNull.Value ? Convert.ToString(drProduct["ProductMessage"]) : ""
                                });
                            }
                            projectModel.lstProductProjectModel = projectModel.lstProductProjectModel.OrderBy(l => l.DisplayOrder).ToList();
                        }
                    }


                    surveyLoadBase.Id = projectModel.Id;
                    SRV.BaseLayer.ActionResult actionResultQuota = new SRV.BaseLayer.ActionResult();

                    //Load All Quota Other of this project
                    actionResultQuota = respondentAction.ProjQuotaOther_LoadByProjectId(surveyLoadBase);
                    if (actionResultQuota.IsSuccess)
                    {
                        foreach (DataRow drQuota in actionResultQuota.dtResult.Rows)
                        {
                            lstProjectQuotaOtherModel.Add(new ProjectQuotaOther
                            {
                                Id = Convert.ToInt32(drQuota["Id"]),
                                QuotaType = drQuota["QuotaType"] != DBNull.Value ? drQuota["QuotaType"].ToString() : "",
                                QuotaPerc = drQuota["QuotaPer"] != DBNull.Value ? Convert.ToDecimal(drQuota["QuotaPer"]) : 0,
                                MaxCount = drQuota["MaxCount"] != DBNull.Value ? Convert.ToInt32(drQuota["MaxCount"]) : 0,
                                QuotaJump = drQuota["QuotaJump"] != DBNull.Value ? Convert.ToInt32(drQuota["QuotaJump"]) : 0
                            });
                        }
                        model.lstProjectQuotaOtherModel = lstProjectQuotaOtherModel;
                    }

                    SRV.BaseLayer.ActionResult actionResultQuestions = new SRV.BaseLayer.ActionResult();
                    // Load All Questions of this project                   
                    actionResultQuestions = respondentAction.ProjQues_LoadByProjectId(surveyLoadBase);
                    if (actionResultQuestions.IsSuccess)
                    {
                        foreach (DataRow drQuestion in actionResultQuestions.dtResult.Rows)
                        {
                            QuestionModel question = new QuestionModel();
                            question.QuestionTypeId = drQuestion["QuestionTypeId"] != DBNull.Value ? Convert.ToInt32(drQuestion["QuestionTypeId"]) : 0;
                            question.QuestionId = drQuestion["QuestionId"] != DBNull.Value ? Convert.ToInt32(drQuestion["QuestionId"]) : 0;
                            question.QuestionTitle = drQuestion["QuestionTitle"] != DBNull.Value ? Convert.ToString(drQuestion["QuestionTitle"]) : "";
                            question.ConditionalExpression = drQuestion["ConditionalExpression"] != DBNull.Value ? Convert.ToString(drQuestion["ConditionalExpression"]) : "";
                            question.RangeMin = drQuestion["RangeMin"] != DBNull.Value ? Convert.ToInt32(drQuestion["RangeMin"]) : 0;
                            question.RangeMax = drQuestion["RangeMax"] != DBNull.Value ? Convert.ToInt32(drQuestion["RangeMax"]) : 0;
                            question.ConstantSum = drQuestion["ConstantSum"] != DBNull.Value ? Convert.ToInt32(drQuestion["ConstantSum"]) : 0;
                            question.RedirectUrl = drQuestion["RedirectUrl"] != DBNull.Value ? Convert.ToString(drQuestion["RedirectUrl"]) : "";
                            question.OptionsCaption = drQuestion["OptionsCaption"] != DBNull.Value ? Convert.ToString(drQuestion["OptionsCaption"]) : "";
                            question.DisplayOrder = drQuestion["DisplayOrder"] != DBNull.Value ? Convert.ToInt32(drQuestion["DisplayOrder"]) : 0;
                            question.isAnswersPiped = drQuestion["isAnswersPiped"] != DBNull.Value ? Convert.ToBoolean(drQuestion["isAnswersPiped"]) : false;
                            question.isAnswersExplicating = drQuestion["isAnswersExplicating"] != DBNull.Value ? Convert.ToBoolean(drQuestion["isAnswersExplicating"]) : false;
                            question.PipedFrom = drQuestion["PipedFrom"] != DBNull.Value ? Convert.ToInt32(drQuestion["PipedFrom"]) : 0;
                            question.ExplicatingFrom = drQuestion["ExplicatingFrom"] != DBNull.Value ? Convert.ToInt32(drQuestion["ExplicatingFrom"]) : 0;

                            // Load All of the Column Names
                            if (question.QuestionTypeId > 7 && question.QuestionTypeId != 15)
                            {
                                questionBaseQ.Id = question.QuestionId;
                                SRV.BaseLayer.ActionResult actionResultColumns = new SRV.BaseLayer.ActionResult();
                                actionResultColumns = adminAction.Question_LoadById(questionBaseQ);
                                if (actionResult.IsSuccess)
                                {
                                    foreach (DataRow drScnd in actionResultColumns.dsResult.Tables[1].Rows)
                                    {
                                        lstMatxModel.Add(new MatrixTypeModel
                                        {
                                            Id = drScnd["Id"] != DBNull.Value ? Convert.ToInt32(drScnd["Id"]) : 0,
                                            QuestionId = drScnd["QuestionId"] != DBNull.Value ? Convert.ToInt32(drScnd["QuestionId"]) : 0,
                                            DisplayOrder = drScnd["MatrixDisplayOrder"] != DBNull.Value ? Convert.ToInt32(drScnd["MatrixDisplayOrder"]) : 0,
                                            ColumnName = drScnd["ColumnName"] != DBNull.Value ? Convert.ToString(drScnd["ColumnName"]) : "",
                                            GroupId = drScnd["GroupId"] != DBNull.Value ? Convert.ToInt32(drScnd["GroupId"]) : 0
                                        });
                                        if (!lstMatxTypeGroupModel.Any(v => v.Id == Convert.ToInt32(drScnd["GroupId"])) && Convert.ToInt32(drScnd["GroupId"]) != 0)
                                        {
                                            lstMatxTypeGroupModel.Add(new MatrixTypeGroupModel
                                            {
                                                Id = drScnd["GroupId"] != DBNull.Value ? Convert.ToInt32(drScnd["GroupId"]) : 0,
                                                GroupCaptionDisplayOrder = drScnd["GroupCaptionDisplayOrder"] != DBNull.Value ? Convert.ToInt32(drScnd["GroupCaptionDisplayOrder"]) : 0,
                                                GroupCaption = drScnd["GroupCaption"] != DBNull.Value ? Convert.ToString(drScnd["GroupCaption"]) : ""
                                            });
                                        }
                                    }
                                    question.lstMatrixTypeModel = lstMatxModel;
                                    question.lstMatrixTypeGroup = lstMatxTypeGroupModel;
                                }
                            }


                            // Load all Options of this question
                            SRV.BaseLayer.ActionResult actionResultOptions = new SRV.BaseLayer.ActionResult();
                            questionBase.Id = question.QuestionId;
                            actionResultOptions = respondentAction.Options_LoadAllByQuesId(questionBase);
                            List<OptionsModel> lstOptions = new List<OptionsModel>();
                            if (actionResultOptions.IsSuccess)
                            {
                                foreach (DataRow drOption in actionResultOptions.dtResult.Rows)
                                {
                                    lstOptions.Add(new OptionsModel
                                    {
                                        OptionId = drOption["Id"] != DBNull.Value ? Convert.ToInt32(drOption["Id"]) : 0,
                                        DisplayOrder = drOption["DisplayOrder"] != DBNull.Value ? Convert.ToInt32(drOption["DisplayOrder"]) : 0,
                                        QuestionId = drOption["QuestId"] != DBNull.Value ? Convert.ToInt32(drOption["QuestId"]) : 0,
                                        Option = drOption["OptionTitle"] != DBNull.Value ? Convert.ToString(drOption["OptionTitle"]) : "",
                                        IsSkip = drOption["IsSkip"] != DBNull.Value ? Convert.ToBoolean(drOption["IsSkip"]) : false,
                                        SkipQuestionId = drOption["SkipQuestionId"] != DBNull.Value ? Convert.ToInt32(drOption["SkipQuestionId"]) : 0,
                                        MaxResponsesCount = drOption["MaxResponsesCount"] != DBNull.Value ? drOption["MaxResponsesCount"].ToString() : "unlimited",
                                        OptionRedirectUrl = drOption["OptionRedirectUrl"] != DBNull.Value ? (Convert.ToString(drOption["OptionRedirectUrl"]).IndexOf('?') == -1 ? (Convert.ToString(drOption["OptionRedirectUrl"]) +
                                        "?_xP=" + projectModel.Id + "&_xL=" + projectModel.LanguageId) : (Convert.ToString(drOption["OptionRedirectUrl"]) +
                                        "&_xP=" + projectModel.Id + "&_xL=" + projectModel.LanguageId)) : "javascript:;"
                                    });
                                }
                                question.lstOptionsModel = lstOptions;

                                //Load Drop Down Options
                                SRV.BaseLayer.ActionResult actionResultDropDownOption = new SRV.BaseLayer.ActionResult();
                                questionBase.Id = question.QuestionId;
                                actionResultDropDownOption = respondentAction.DDLMatrixTypeOptions_LoadByQuestionId(questionBase);
                                List<DDLMatrixTypeModel> lstDDLModel = new List<DDLMatrixTypeModel>();
                                if (actionResultDropDownOption.IsSuccess)
                                {
                                    foreach (DataRow drDropDown in actionResultDropDownOption.dtResult.Rows)
                                    {
                                        lstDDLModel.Add(new DDLMatrixTypeModel
                                        {
                                            Id = drDropDown["Id"] != DBNull.Value ? Convert.ToInt32(drDropDown["Id"]) : 0,
                                            QuestionId = drDropDown["QuestionId"] != DBNull.Value ? Convert.ToInt32(drDropDown["QuestionId"]) : 0,
                                            OptionName = drDropDown["OptionName"] != DBNull.Value ? Convert.ToString(drDropDown["OptionName"]) : "",
                                            DDLDisplayOrder = drDropDown["DisplayOrder"] != DBNull.Value ? Convert.ToInt32(drDropDown["DisplayOrder"]) : 0
                                        });
                                    }
                                    lstDDLModel = lstDDLModel.OrderBy(l => l.DDLDisplayOrder).ToList();
                                }
                                question.lstDDlMatrixModel = lstDDLModel;
                            }
                            lstQuestionModel.Add(question);
                            lstMatxModel = new List<MatrixTypeModel>();
                        }
                        model.lstQuestionModel = lstQuestionModel;
                        //model.lstMatrixTypeModel = lstMatxModel; 
                    }
                }
                else
                {
                    model.isError = true;
                    TempData["ErrorMessage"] = "Invalid link";
                    return model;
                }
            }
            catch (Exception ex)
            {
                // ErrorReporting.WebApplicationError(ex);
            }
            return model;
        }

        #endregion

        #region EditProfile Get
        public ActionResult EditProfile()
        {
            AdminRegisterModel model = new AdminRegisterModel();
            try
            {
                CommonMethods comMethods = new CommonMethods();
                model = comMethods.profileDetails();
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return View(model);
        }
        #endregion

        #region EditProfile Post
        [HttpPost]
        public ActionResult EditProfile(AdminRegisterModel model)
        {
            try
            {
                adminBase.Id = model.Id;
                adminBase.UserGuid = Guid.NewGuid().ToString();
                adminBase.FirstName = model.FirstName;
                adminBase.LastName = model.LastName;
                adminBase.Email = model.Email;
                adminBase.Gender = model.Gender;
                adminBase.Address = model.Address;
                adminBase.CountryId = Convert.ToInt32(model.CountryId);
                adminBase.StateId = Convert.ToInt32(model.StateId);
                adminBase.City = model.City;
                adminBase.ZipCode = model.ZipCode;
                adminBase.SecurityQuestion = model.SecurityQuestion;
                adminBase.AnswerToSecurityQuestion = model.AnswerToSecurityQuestion;
                adminBase.CountryOfResidenceId = model.CountryOfResidenceId;
                adminBase.LanguageId = model.LanguageId;
                adminBase.CompanyName = model.CompanyName;
                adminBase.PhoneNumber = model.PhoneNumber;
                adminBase.SendEmail = model.sendEmail;
                adminBase.OtherCommunications = model.OtherCommunications;

                actionResult = adminAction.Register_InsertUpdate(adminBase);
                if (actionResult.IsSuccess)
                {
                    int status = Convert.ToInt32(actionResult.dtResult.Rows[0][0]);
                    if (status > 0)
                    {
                        TempData["SuccessMessage"] = "Your profile has updated successfully.";
                    }
                    else
                    {
                        TempData["ErrorMessage"] = "Something went wrong.Please try again later";
                    }
                }
                else
                {
                    TempData["ErrorMessage"] = "Something went wrong.Please try again later";
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return RedirectToAction("EditProfile");
        }
        #endregion

        #region _PartialViewSurvey
        public ActionResult _PartialViewSurvey(string surveyId, string uid = "")
        {
            SurveyResponseModel model = new SurveyResponseModel();
            try
            {
                model = comMethods.GetSurvey(surveyId, uid, "admin");
                ViewBag.ProjectId = model.projectModel.Id;

                //if (model.isError)
                //    TempData["ErrorMessage"] = model.ErrorMessage;
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return View(model);
        }
        #endregion


    }
}
