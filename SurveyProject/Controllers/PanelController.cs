﻿using SRV.ActionLayer.Panel;
using SRV.BaseLayer.Panel;
using SurveyProject.Helper;
using SurveyProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SurveyProject.Controllers
{
    [CheckLogin]
    public class PanelController : Controller
    {
        #region Declaration
        PanelBase panelBase = new PanelBase();
        SRV.BaseLayer.ActionResult actionResult = new SRV.BaseLayer.ActionResult();
        PanelAction panelAction = new PanelAction();
        #endregion

        #region Surveys GET
        [HttpGet]
        public ActionResult Surveys()
        {
            PanelModels model = new PanelModels();
            List<SurveyPanelModels> lstSurveyModel = new List<SurveyPanelModels>();
            model.lstSurveyPanelModel = lstSurveyModel;
            panelBase.PanelId = Convert.ToInt32(Session["UserId"]);
            actionResult = panelAction.PanelProjects_LoadByPanelId(panelBase);
            if (actionResult.IsSuccess)
                lstSurveyModel = Helper.CommHelper.ConvertTo<SurveyPanelModels>(actionResult.dtResult);
            model.lstSurveyPanelModel = lstSurveyModel;

            return View(model);
        }
        #endregion
    }
}
