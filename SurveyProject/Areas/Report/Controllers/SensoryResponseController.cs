﻿using PowerPointTemplates;
using SRV.ActionLayer.Admin;
using SRV.BaseLayer.Admin;
using SRV.Utility;
using SurveyProject.Areas.Report;
using SurveyProject.Controllers;
using SurveyProject.Helper;
using SurveyProject.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SurveyProject.Areas.Report.Controllers
{
    [CheckLogin]
    public class SensoryResponseController : Controller
    {

        ProjectBase projectBase = new ProjectBase();
        SRV.BaseLayer.ActionResult actionResult = new SRV.BaseLayer.ActionResult();
        AdminAction adminAction = new AdminAction();
        ProductProjectBase productProjectBase = new ProductProjectBase();


        //
        // GET: /Report/SensoryResponse/

        public ActionResult Curve(string projId, string ReportType)
        {

            CreateProjectModel model = GetProjectDetailById(Convert.ToInt32(projId));

            string projectGUID = model.ProjectGuid;
            ViewBag.ProjectName = model.ProjectName;
            ViewBag.ProjectType = model.ProjectType;
            ViewBag.ProjectId = Convert.ToInt32(projId);
            DataTable dt = new DataTable();
            DataTable dt1 = new DataTable();
            int RespCount = 0;



            if (ReportType.ToLower() == "online")
            {
                ViewBag.ReportType = "online";
            }
            else if (ReportType.ToLower() == "offline")
            {
                ViewBag.ReportType = "offline";
            }
            else if (ReportType.ToLower() == "combined")
            {
                ViewBag.ReportType = "combined";
            }


            Session["ReportDataTable"] = dt;
            ViewBag.RDT = dt;

            ViewBag.ProjectGUID = projectGUID;
            ViewBag.RespCount = RespCount;
            ViewBag.ProjectId = projId;

            return View();
        }

        #region GetProjectDetailById
        public CreateProjectModel GetProjectDetailById(int? id = 0)
        {
            CreateProjectModel model = new CreateProjectModel();
            List<ProductProjectModel> lstproductModel = new List<ProductProjectModel>();
            try
            {
                projectBase.Id = Convert.ToInt32(id);
                actionResult = adminAction.Project_LoadById(projectBase);
                if (actionResult.IsSuccess)
                {
                    DataRow dr = actionResult.dtResult.Rows[0];
                    model.Id = dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0;
                    model.SuperProjectId = dr["SuperProjectId"] != DBNull.Value ? Convert.ToInt32(dr["SuperProjectId"]) : 0;
                    model.ProjectName = dr["ProjectName"] != DBNull.Value ? Convert.ToString(dr["ProjectName"]) : "";
                    model.WelcomeMessage = dr["WelcomeMessage"] != DBNull.Value && !String.IsNullOrEmpty(dr["WelcomeMessage"].ToString()) ? Convert.ToString(dr["WelcomeMessage"]) : Email.ReadFile("Welcome.txt");
                    model.PageHeader = dr["PageHeader"] != DBNull.Value && !String.IsNullOrEmpty(dr["PageHeader"].ToString()) ? Convert.ToString(dr["PageHeader"]) : Email.ReadFile("Header.txt");
                    model.PageFooter = dr["PageFooter"] != DBNull.Value && !String.IsNullOrEmpty(dr["PageFooter"].ToString()) ? Convert.ToString(dr["PageFooter"]) : Email.ReadFile("Footer.txt");
                    model.ProjectCompletionMesssage = dr["ProjectCompletionMesssage"] != DBNull.Value && !String.IsNullOrEmpty(dr["ProjectCompletionMesssage"].ToString()) ? Convert.ToString(dr["ProjectCompletionMesssage"]) : Email.ReadFile("Complete.txt");
                    model.RedirectUrl = dr["RedirectUrl"] != DBNull.Value ? Convert.ToString(dr["RedirectUrl"]) : "";
                    model.InvitationMessage = dr["InvitationMessage"] != DBNull.Value ? Convert.ToString(dr["InvitationMessage"]) : "";
                    model.OwnerId = dr["OwnerId"] != DBNull.Value ? Convert.ToInt32(dr["OwnerId"]) : 0;
                    model.CreatedBy = dr["CreatedBy"] != DBNull.Value ? Convert.ToInt32(dr["CreatedBy"]) : 0;
                    model.ProjectQuota = dr["ProjectQuota"] != DBNull.Value ? Convert.ToInt32(dr["ProjectQuota"]) : 0;
                    model.TerminationMessageUpper = dr["ProjectTerminationMessage"] != DBNull.Value && !String.IsNullOrEmpty(dr["ProjectTerminationMessage"].ToString()) ? Convert.ToString(dr["ProjectTerminationMessage"]) : Email.ReadFile("TerminateUpper.txt");
                    model.ProjectGuid = dr["ProjectGuid"] != DBNull.Value ? Convert.ToString(dr["ProjectGuid"]) : "";
                    model.IsIncludePageHeader = dr["IsIncludePageHeader"] != DBNull.Value ? Convert.ToBoolean(dr["IsIncludePageHeader"]) : false;
                    model.IsIncludePageFooter = dr["IsIncludePageFooter"] != DBNull.Value ? Convert.ToBoolean(dr["IsIncludePageFooter"]) : false;
                    model.IsIncludeProjectCompletionMesssage = dr["IsIncludeProjectCompletionMesssage"] != DBNull.Value ? Convert.ToBoolean(dr["IsIncludeProjectCompletionMesssage"]) : false;
                    model.IsIncludeTerminationMessage = dr["IsIncludeTerminationMessage"] != DBNull.Value ? Convert.ToBoolean(dr["IsIncludeTerminationMessage"]) : false;
                    model.IsIncludeWelcomeMessage = dr["IsIncludeWelcomeMessage"] != DBNull.Value ? Convert.ToBoolean(dr["IsIncludeWelcomeMessage"]) : false;
                    model.ProjectType = dr["ProjectType"] != DBNull.Value ? Convert.ToString(dr["ProjectType"]) : "";
                    //model.ProductMessage = dr["ProductMessage"] != DBNull.Value ? Convert.ToString(dr["ProductMessage"]) : "";
                }

                productProjectBase.ProjectId = Convert.ToInt32(id);
                actionResult = adminAction.ProductProject_LoadById(productProjectBase);
                if (actionResult.IsSuccess)
                {
                    foreach (DataRow dr in actionResult.dtResult.Rows)
                    {
                        lstproductModel.Add(new ProductProjectModel
                        {
                            Id = dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0,
                            ProjectId = dr["ProjectId"] != DBNull.Value ? Convert.ToInt32(dr["ProjectId"]) : 0,
                            ProductLogicalId = dr["ProductLogicalId"] != DBNull.Value ? Convert.ToInt32(dr["ProductLogicalId"]) : 0,
                            Description = dr["Description"] != DBNull.Value ? Convert.ToString(dr["Description"]) : string.Empty,
                            DisplayOrder = dr["DisplayOrder"] != DBNull.Value ? Convert.ToInt32(dr["DisplayOrder"]) : 0
                        });
                    }
                    lstproductModel = lstproductModel.OrderBy(l => l.DisplayOrder).ToList();
                    model.lstProductProjectModel = lstproductModel;

                }

            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return model;
        }
        #endregion


        #region _PartialReportMaker Get
        [HttpGet]
        public ActionResult _PartialReportMaker(string surveyGUID)
        {
            List<QuestionModel> QuestionLst = new List<QuestionModel>();
            RespondentController resp = new RespondentController();
            try
            {
                SurveyResponseModel model = new SurveyResponseModel();
                model = resp.GetProjectPreview(surveyGUID);
                ViewBag.ProjectType = model.projectModel.ProjectType;
                QuestionLst = model.lstQuestionModel;
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return View(QuestionLst);
        }
        #endregion

        //------------------- Export Kano Graph to PPT
        [ValidateInput(false)]
        [HttpPost]
        public void ExportSensoryResponseToPPT(FormCollection fc)
        {
            string base64 = Request.Form["hdnExportKanoToPPTData"].Split(',')[1];
            string fileName = Request.Form["hdnSensoryResponseToPPTData"];
            if (fileName == "" || fileName == null)
            {
                fileName = "Sensory Response Curve";
            }
            byte[] imagebytes = Convert.FromBase64String(base64);
            System.IO.MemoryStream stream = new System.IO.MemoryStream(imagebytes);
            System.Drawing.Image image = new System.Drawing.Bitmap(stream);
            string imageName = System.DateTime.Now.ToString("MM-dd-yyyy") + "_" + Guid.NewGuid().ToString().Substring(0, 5) + "SensoryResponseCurve.png";
            if (!System.IO.Directory.Exists(Server.MapPath("~/Content/Uploads/DownloadFiles")))
            {
                System.IO.Directory.CreateDirectory(Server.MapPath("~/Content/Uploads/DownloadFiles"));
            }
            image.Save(Server.MapPath("~/Content/Uploads/DownloadFiles/" + imageName));
            var templ = new PowerPointTemplate();
            templ.PowerPointParameters.Add(new PowerPointParameter() { Name = "1", Image = new System.IO.FileInfo(Server.MapPath("~/Content/Uploads/DownloadFiles/" + imageName)) });

            var templatePath = Server.MapPath("~/Content/PPT/SensoryResponseCurveTemplate.pptx");
            var outputPath = Server.MapPath("~/Content/Uploads/DownloadFiles/" + DateTime.Today.ToShortDateString().Replace("/", "_") + ".ppt");

            templ.ParseTemplate(templatePath, outputPath);
            DownloadFile(Server.MapPath("~/Content/Uploads/DownloadFiles/" + DateTime.Today.ToShortDateString().Replace("/", "_") + ".ppt"), fileName);
        }

        private void DownloadFile(string filePath, string fileName)
        {
            System.Net.WebClient req = new System.Net.WebClient();
            HttpResponse response = System.Web.HttpContext.Current.Response;
            response.Clear();
            response.ClearContent();
            response.ClearHeaders();
            response.Buffer = true;
            response.AddHeader("Content-Disposition", "attachment;filename=" + fileName + ".ppt");
            byte[] data = req.DownloadData(filePath);
            if (System.IO.File.Exists(filePath))
            {
                System.IO.File.Delete(filePath);
            }
            response.BinaryWrite(data);
            response.End();
        }

    }
}
