﻿using SRV.ActionLayer.Admin;
using SRV.ActionLayer.Respondent;
using SRV.BaseLayer.Admin;
using SRV.BaseLayer.Respondent;
using SRV.Utility;
using SurveyProject.Controllers;
using SurveyProject.Helper;
using SurveyProject.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using Microsoft.Office.Core;
using PowerPoint = Microsoft.Office.Interop.PowerPoint;
using PowerPointTemplates;
using System.Configuration;
using System.IO;
using System.Xml;

namespace SurveyProject.Areas.Report.Controllers
{
    public class HomeController : Controller
    {
        #region Declaration
        QuestionBase questionBase = new QuestionBase();
        CommonMethods comMethods = new CommonMethods();
        ProjectBase projectBase = new ProjectBase();
        QueryBase queryBase = new QueryBase();
        RespondentAction respondentAction = new RespondentAction();
        ProductProjectBase productProjectBase = new ProductProjectBase();
        SRV.BaseLayer.ActionResult actionResult = new SRV.BaseLayer.ActionResult();
        AdminAction adminAction = new AdminAction();
        static int indexCount = 0;
        static DataTable CaseContainer = new DataTable();
        string SiteUrl = ConfigurationManager.AppSettings["site"].ToString();
        #endregion

        #region Index Get
        public ActionResult Index(int projId, string ReportType)
        {
            CreateProjectModel model = new CreateProjectModel();
            model = GetProjectDetailById(projId);
            string projectGUID = model.ProjectGuid;
            ViewBag.ProjectName = model.ProjectName;
            ViewBag.ProjectType = model.ProjectType;
            ViewBag.ProjectId = projId;

            DataTable dt = new DataTable();
            DataTable dt1 = new DataTable();
            int RespCount = 0;



            if (ReportType.ToLower() == "online")
            {
                ViewBag.ReportType = "online";
            }
            else if (ReportType.ToLower() == "offline")
            {
                ViewBag.ReportType = "offline";
            }
            else if (ReportType.ToLower() == "combined")
            {
                ViewBag.ReportType = "combined";
            }





            //ViewBag.TotalResponses = (from DataRow dRow in dt.Rows select dRow["Resp_Id"]).Distinct().Count();
            Session["ReportDataTable"] = dt;
            ViewBag.RDT = dt;

            //string ReportDataHtml = "<table id='hdnReportHtml'>";
            //if (Session["ReportDataTable"] != null)
            //{
            //    DataTable dtReport = (DataTable)Session["ReportDataTable"];
            //    if (dtReport != null)
            //    {
            //        string[] cols = (from dc in dtReport.Columns.Cast<DataColumn>()
            //                         select dc.ColumnName)
            //                       .ToArray();
            //        if (cols != null)
            //        {
            //            ReportDataHtml += "<tr class='thRow'>";
            //            foreach (var item in cols)
            //            {
            //                ReportDataHtml += "<th>" + item + "</th>";
            //            }
            //            ReportDataHtml += "</tr>";
            //        }

            //        foreach (DataRow row in dtReport.Rows)
            //        {
            //            ReportDataHtml += "<tr class='tdRow'>";
            //            foreach (var item in row.ItemArray)
            //            {
            //                ReportDataHtml += "<td>" + item + "</td>";
            //            }
            //            ReportDataHtml += "</tr>";
            //        }
            //    }
            //}
            //ReportDataHtml += "</table>";
            //ViewBag.ReportDataHtml = ReportDataHtml;          




            ViewBag.ProjectGUID = projectGUID;
            ViewBag.RespCount = RespCount;
            ViewBag.ProjectId = projId;
            return View();
        }
        #endregion



        #region Index Get
        public ActionResult Index2(int projId, string ReportType)
        {
            CreateProjectModel model = new CreateProjectModel();
            model = GetProjectDetailById(projId);
            string projectGUID = model.ProjectGuid;
            ViewBag.ProjectName = model.ProjectName;
            ViewBag.ProjectType = model.ProjectType;
            DataTable dt = new DataTable();
            DataTable dt1 = new DataTable();

            int RespCount = 0;

            if (ReportType.ToLower() == "online")
            {
                dt = GetReportDataTable(projectGUID);
            }
            else if (ReportType.ToLower() == "offline")
            {
                dt = GetReportDataTableOffline(projectGUID, (RespCount + 1));
            }
            else if (ReportType.ToLower() == "combined")
            {
                dt = GetReportDataTable(projectGUID);
                RespCount = (from DataRow dRow in dt.Rows select dRow["Resp_Id"]).Distinct().Count();
                dt1 = GetReportDataTableOffline(projectGUID, (RespCount + 1));
                dt.Merge(dt1);
            }

            ViewBag.TotalResponses = (from DataRow dRow in dt.Rows select dRow["Resp_Id"]).Distinct().Count();
            Session["ReportDataTable"] = dt;
            ViewBag.RDT = dt;

            string ReportDataHtml = "<table id='hdnReportHtml'>";
            if (Session["ReportDataTable"] != null)
            {
                DataTable dtReport = (DataTable)Session["ReportDataTable"];
                if (dtReport != null)
                {
                    string[] cols = (from dc in dtReport.Columns.Cast<DataColumn>()
                                     select dc.ColumnName)
                                   .ToArray();
                    if (cols != null)
                    {
                        ReportDataHtml += "<tr class='thRow'>";
                        foreach (var item in cols)
                        {
                            ReportDataHtml += "<th>" + item + "</th>";
                        }
                        ReportDataHtml += "</tr>";
                    }

                    foreach (DataRow row in dtReport.Rows)
                    {
                        ReportDataHtml += "<tr class='tdRow'>";
                        foreach (var item in row.ItemArray)
                        {
                            ReportDataHtml += "<td>" + item + "</td>";
                        }
                        ReportDataHtml += "</tr>";
                    }
                }
            }
            ReportDataHtml += "</table>";
            ViewBag.ReportDataHtml = ReportDataHtml;


            //if (Session["ReportDataTable"] == null)
            //{
            //    DataTable dt = GetReportDataTable(projectGUID);
            //    ViewBag.TotalResponses = (from DataRow dRow in dt.Rows select dRow["Resp_Id"]).Distinct().Count();
            //    Session["ReportDataTable"] = dt;
            //}
            //else
            //{
            //    DataTable dt = (DataTable)Session["ReportDataTable"];
            //    ViewBag.TotalResponses = (from DataRow dRow in dt.Rows select dRow["Resp_Id"]).Distinct().Count();
            //}


            ViewBag.ProjectGUID = projectGUID;
            ViewBag.ProjectId = projId;
            return View();
        }
        #endregion

        #region QueryIndex Get
        public ActionResult QueryIndex(int projId, string ReportType)
        {
            DataTable CaseContainer1 = new DataTable();
            CreateProjectModel model = new CreateProjectModel();
            model = GetProjectDetailById(projId);
            string projectGUID = model.ProjectGuid;
            ViewBag.ProjectName = model.ProjectName;
            ViewBag.ProjectType = model.ProjectType;


            Session["ReportType"] = ReportType;
            ViewBag.ProjectGUID = projectGUID;
            ViewBag.ProjectId = projId;

            //    CreateProjectModel projectModel = new CreateProjectModel();
            //    projectModel = GetProjectDetailById(projId);
            //    List<int> currentQueryList = new List<int>();
            //    int currentQuery = 0;
            //    int currentQuery1 = 0;
            //    int check = 0;
            //    int mainProjectId = 0;

            //    QueryModel queryModel = new QueryModel();
            //    List<QueryModel> QueryList = new List<QueryModel>();            
            //    queryBase.ProjectId = Convert.ToInt32(projId);
            //    actionResult = adminAction.Query_LoadAllByProjectId(queryBase);
            //    if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
            //    {
            //        foreach (DataRow dr in actionResult.dtResult.Rows)
            //        {
            //            queryModel = new QueryModel();
            //            queryModel.ProjectId = dr["ProjectId"] != DBNull.Value ? Convert.ToInt32(dr["ProjectId"]) : 0;
            //            queryModel.CaseName = dr["CaseName"] != DBNull.Value ? Convert.ToString(dr["CaseName"]) : "";
            //            queryModel.Query = dr["Query"] != DBNull.Value ? Convert.ToString(dr["Query"]) : "";
            //            queryModel.QueryHtml = dr["QueryHtml"] != DBNull.Value ? Convert.ToString(dr["QueryHtml"]) : "";
            //            currentQuery = dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0;
            //            QueryList.Add(queryModel);
            //            currentQuery = dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0;
            //            if (currentQuery > 0)
            //            {
            //                currentQueryList.Add(currentQuery);
            //            }                   
            //        }
            //    }
            //    ///
            //    if (projectModel.SuperProjectId > 0)
            //    {
            //        queryBase.SuperProjectId = projectModel.SuperProjectId;
            //        //(projectModel.ProjectType == 2)
            //        actionResult = adminAction.Query_LoadAllBySuperProjectId(queryBase);
            //        if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
            //        {
            //            check = 0;
            //            foreach (DataRow dr in actionResult.dtResult.Rows)
            //            {
            //                queryModel = new QueryModel();
            //                queryModel.ProjectId = dr["ProjectId"] != DBNull.Value ? Convert.ToInt32(dr["ProjectId"]) : 0;
            //                queryModel.CaseName = dr["CaseName"] != DBNull.Value ? Convert.ToString(dr["CaseName"]) : "";
            //                queryModel.Query = dr["Query"] != DBNull.Value ? Convert.ToString(dr["Query"]) : "";
            //                queryModel.QueryHtml = dr["QueryHtml"] != DBNull.Value ? Convert.ToString(dr["QueryHtml"]) : "";
            //                currentQuery1 = dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0;
            //                foreach (var item in currentQueryList)
            //                {
            //                    if (item == currentQuery1)
            //                    {
            //                        check = 1;
            //                    }
            //                }
            //                if (check == 0)
            //                {
            //                    QueryList.Add(queryModel);
            //                }
            //            }
            //        }
            //    }
            //    ///

            //    model.QueryList = QueryList;


            //    string QueryBuilder  = string.Empty;

            //    int ProjectId = projId;
            ////    int ProjectId = mainProjectId;
            //    if (CaseContainer1.Columns.Count == 0)
            //    {
            //        CaseContainer1.Columns.Add("CaseName", typeof(string));
            //        CaseContainer1.Columns.Add("Online", typeof(DataTable));
            //        CaseContainer1.Columns.Add("Offline", typeof(DataTable));
            //        CaseContainer1.Columns.Add("Combined", typeof(DataTable));
            //    }



            //    foreach (var item in QueryList)
            //    {
            //        QueryBuilder = item.Query;
            //        if (QueryBuilder != null && QueryBuilder != string.Empty)
            //        {
            //            string[] query = QueryBuilder.TrimEnd('*').Split('*');
            //            DataTable dtCase = new DataTable();
            //            dtCase.Columns.Add("Id", typeof(int));
            //            dtCase.Columns.Add("QuestionId", typeof(int));
            //            dtCase.Columns.Add("QuestionTypeId", typeof(int));
            //            //dtCase.Columns.Add("OptionIdOrDisplayOrder", typeof(int));
            //            //dtCase.Columns.Add("ColumnIdOrDisplayOrder", typeof(int));
            //            dtCase.Columns.Add("OptionIdOrDisplayOrder", typeof(string));
            //            dtCase.Columns.Add("ColumnIdOrDisplayOrder", typeof(string));
            //            dtCase.Columns.Add("Expression", typeof(string));
            //            dtCase.Columns.Add("NextQuestionAddWith", typeof(string));


            //            if (query.Length > 0)
            //            {
            //                for (int i = 0; i <= query.Length - 1; i++)
            //                {





            //                    string[] questionArr = query[i].Split('_'); //1602_5$>=11#OR|2~
            //                    int questionId = Convert.ToInt32(questionArr[0]);
            //                    string[] exprArr = questionArr[1].Split('$');
            //                    int questionTypeId = Convert.ToInt32(exprArr[0]);
            //                    string Expression = exprArr[1].Split('#')[0];
            //                    string[] optionIdArr = exprArr[1].Split('#')[1].Split('|');
            //                    string NextQuestionAddWith = optionIdArr[0];
            //                    //int optionId = optionIdArr[1].Split('~')[0] != "" ? Convert.ToInt32(optionIdArr[1].Split('~')[0]) : 0;
            //                    //int columnId = optionIdArr[1].Split('~')[1] != "" ? Convert.ToInt32(optionIdArr[1].Split('~')[1]) : 0;
            //                    string optionId = optionIdArr[1].Split('~')[0] != "" ? Convert.ToString(optionIdArr[1].Split('~')[0]) : "0";
            //                    string columnId = optionIdArr[1].Split('~')[1] != "" ? Convert.ToString(optionIdArr[1].Split('~')[1]) : "0";
            //                    DataRow drCase = dtCase.NewRow();
            //                    drCase["Id"] = (i + 1);
            //                    drCase["QuestionId"] = questionId;
            //                    drCase["QuestionTypeId"] = questionTypeId;
            //                    drCase["OptionIdOrDisplayOrder"] = optionId;
            //                    drCase["ColumnIdOrDisplayOrder"] = columnId;
            //                    drCase["Expression"] = Expression;
            //                    drCase["NextQuestionAddWith"] = NextQuestionAddWith;
            //                              dtCase.Rows.Add(drCase);
            //                           }
            //                       }

            //                    projectBase.CaseTable = dtCase;
            //                    List<string> lstEmails = new List<string>();
            //                    List<string> lstResponders = new List<string>();
            //                    List<string> offlineResponders = new List<string>();
            //                    string Email = string.Empty;
            //                    string ExternalResponders = string.Empty;



            //                    actionResult = adminAction.Response_CaseSelectionReport(projectBase);

            //                    if (actionResult.IsSuccess)
            //                    {
            //                        if (Convert.ToString(actionResult.dtResult.Rows[0][0]) == "-1")
            //                        {

            //                        }
            //                        else
            //                        {
            //                            foreach (DataRow dr in actionResult.dtResult.Rows)
            //                            {
            //                                ProjectId = dr["ProjectId"] != DBNull.Value ? Convert.ToInt32(dr["ProjectId"]) : 0;
            //                                Email += dr["Email"] != DBNull.Value ? Convert.ToString(dr["Email"]) + "," : "";
            //                                if (dr["Email"] != DBNull.Value && !String.IsNullOrEmpty(dr["Email"].ToString()))
            //                                    lstEmails.Add(dr["Email"].ToString());
            //                                ExternalResponders += dr["ExternalResponderId"] != DBNull.Value ? Convert.ToString(dr["ExternalResponderId"]) + "," : "";
            //                                if (dr["ExternalResponderId"] != DBNull.Value && !String.IsNullOrEmpty(dr["ExternalResponderId"].ToString()))
            //                                    lstResponders.Add(dr["ExternalResponderId"].ToString());

            //                            }



            //                        }
            //                    }

            //                    CreateProjectModel model1 = new CreateProjectModel();
            //                    model1 = GetProjectDetailById(ProjectId);                           

            //                    DataTable dt = GetReportDataTable1(projectGUID, lstEmails.ToArray(), lstResponders.ToArray());
            //                    actionResult = adminAction.Response_CaseSelectionReportOffline(projectBase);
            //                    if (actionResult.IsSuccess)
            //                    {
            //                        if (Convert.ToString(actionResult.dtResult.Rows[0][0]) == "-1")
            //                        {

            //                        }
            //                        else
            //                        {
            //                            ExternalResponders = "";
            //                            foreach (DataRow dr in actionResult.dtResult.Rows)
            //                            {
            //                                ProjectId = dr["ProjectId"] != DBNull.Value ? Convert.ToInt32(dr["ProjectId"]) : 0;

            //                                ExternalResponders += dr["RespId"] != DBNull.Value ? Convert.ToString(dr["RespId"]) + "," : "";
            //                                if (dr["RespId"] != DBNull.Value && !String.IsNullOrEmpty(dr["RespId"].ToString()))
            //                                    offlineResponders.Add(dr["RespId"].ToString());

            //                            }
            //                        }
            //                    }
            //                    int RespCount = 0;
            //                    DataTable dt1 = GetReportDataTableOffline1(projectGUID, (RespCount + 1), offlineResponders.ToArray());
            //                    //if (dt.Rows.Count == 0 && dt1.Rows.Count == 0)
            //                    //{
            //                    //    TempData["ErrorMessage"] = "There is no data available regarding this query";
            //                    //    return RedirectToAction("QueryBuilder", "QueryBuilder", new { ProjectId = ProjectId, Area = "Report" });
            //                    //}
            //                    DataRow CaseRow = CaseContainer1.NewRow();
            //                    DataTable dt2 = new DataTable();
            //                    CaseRow[0] = item.CaseName;
            //                    CaseRow[1] = dt;
            //                    CaseRow[2] = dt1;
            //                    dt2 = dt.Copy();
            //                    dt2.Merge(dt1, true);
            //                    //     dt.AcceptChanges();
            //                    CaseRow[3] = dt2;
            //        //            ViewBag.TotalResponses = (from DataRow dRow in dt2.Rows select dRow["Resp_Id"]).Distinct().Count();
            //                    CaseContainer1.Rows.Add(CaseRow);


            //        }
            //    }

            ViewBag.TotalResponses = "0";
            Session["CaseContainer"] = null;
            Session["CaseContainer"] = CaseContainer1;


            return View();
        }
        #endregion

        //-------------Azeez-------------//
        #region GetQueryReportDataTable_LoadByAjax
        [HttpPost]
        public JsonResult GetQueryReportDataTable_LoadByAjax(string projIdStr = null, string projectGUID = null, string GenUpdatedReport=null)
        {
            string[] arr = new string[3];
            string LastUpdate = "";
            int projId = Convert.ToInt32(projIdStr);
            if (GenUpdatedReport == "" || GenUpdatedReport == null)
            {
                
                actionResult = adminAction.ReportQueryBuilder_LoadById(projId, projectGUID);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    #region If Response Exists

                    DataRow dr = actionResult.dtResult.Rows[0];
                    string response = dr["Response"] != DBNull.Value ? dr["Response"].ToString() : "";
                    string SessionType = dr["SessionType"] != DBNull.Value ? dr["SessionType"].ToString() : "";
                    string ViewBagType = dr["ViewBagType"] != DBNull.Value ? dr["ViewBagType"].ToString() : "";
                    LastUpdate = dr["CreatedDate"] != DBNull.Value ? dr["CreatedDate"].ToString() : "";

                    if (ViewBagType != "")
                    {
                        ViewBag.ProductCount = ViewBagType;
                    }
                    else {
                        ViewBag.ProductCount = null;
                    }


                    DataSet ds = new DataSet();
                    using (StringReader stringReader = new StringReader(SessionType))
                    {
                        ds = new DataSet();
                        ds.ReadXml(stringReader);
                    }

                    #region Response 
                    DataTable dtCopy = ds.Tables[0].Clone(); //copy the structure 
                    for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++) //iterate through the rows of the source
                    {
                        DataRow currentRow = ds.Tables[0].Rows[i];  //copy the current row 
                        foreach (var colValue in currentRow.ItemArray)//move along the columns 
                        {
                            if (!string.IsNullOrEmpty(colValue.ToString())) // if there is a value in a column, copy the row and finish
                            {
                                dtCopy.ImportRow(currentRow);
                                break; //break and get a new row                        
                            }
                        }
                    }

                    DataTable CaseContainer1 = new DataTable();
                    if (CaseContainer1.Columns.Count == 0)
                    {
                        CaseContainer1.Columns.Add("CaseName", typeof(string));
                        CaseContainer1.Columns.Add("Online", typeof(DataTable));
                        CaseContainer1.Columns.Add("Offline", typeof(DataTable));
                        CaseContainer1.Columns.Add("Combined", typeof(DataTable));
                    }
                    for (int i = 0; i < dtCopy.Rows.Count; i++)
                    {
                        string caseName = dtCopy.Rows[i][0].ToString();
                        actionResult = adminAction.ReportQueryBuilderCases_LoadById(projId, projectGUID, caseName);
                        if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                        {
                            DataRow drCase = actionResult.dtResult.Rows[0];
                            string OnlineResponse = drCase["OnlineResponse"] != DBNull.Value ? drCase["OnlineResponse"].ToString() : "";
                            string OfflineResponse = drCase["OfflineResponse"] != DBNull.Value ? drCase["OfflineResponse"].ToString() : "";
                            string CombineResponse = drCase["CombineResponse"] != DBNull.Value ? drCase["CombineResponse"].ToString() : "";

                            DataRow CaseRow = CaseContainer1.NewRow();
                            DataTable tempdata = new DataTable();
                            CaseRow[0] = caseName;

                            if (OnlineResponse != "")
                            {
                                CaseRow[1] = ConvertXMLToDataTable(OnlineResponse);
                            }
                            else {
                                CaseRow[1] = tempdata;
                            }

                            if (OfflineResponse != "")
                            {
                                CaseRow[2] = ConvertXMLToDataTable(OfflineResponse);
                            }
                            else {
                                CaseRow[2] = tempdata;
                            }

                            if (CombineResponse != "")
                            {
                                CaseRow[3] = ConvertXMLToDataTable(CombineResponse);
                            }
                            else {
                                CaseRow[3] = tempdata;
                            }


                            CaseContainer1.Rows.Add(CaseRow);
                        }
                    }
                    Session["CaseContainer"] = null;
                    Session["CaseContainer"] = CaseContainer1;
                    #endregion

                    arr[0] = response;
                    arr[1] = "Last Updated Report: " + LastUpdate;
                    return Json(arr, JsonRequestBehavior.AllowGet);
                    #endregion
                }
                else {
                    #region If Response Not Exists
                    CreateProjectModel model = new CreateProjectModel();
                    DataTable CaseContainer1 = new DataTable();
                    CreateProjectModel projectModel = new CreateProjectModel();
                    projectModel = GetProjectDetailById(projId);
                    List<int> currentQueryList = new List<int>();
                    int currentQuery = 0;
                    int currentQuery1 = 0;
                    int check = 0;
                    int mainProjectId = 0;

                    QueryModel queryModel = new QueryModel();
                    List<QueryModel> QueryList = new List<QueryModel>();
                    queryBase.ProjectId = Convert.ToInt32(projId);
                    actionResult = adminAction.Query_LoadAllByProjectId(queryBase);
                    if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                    {
                        foreach (DataRow dr in actionResult.dtResult.Rows)
                        {
                            queryModel = new QueryModel();
                            queryModel.ProjectId = dr["ProjectId"] != DBNull.Value ? Convert.ToInt32(dr["ProjectId"]) : 0;
                            queryModel.CaseName = dr["CaseName"] != DBNull.Value ? Convert.ToString(dr["CaseName"]) : "";
                            queryModel.Query = dr["Query"] != DBNull.Value ? Convert.ToString(dr["Query"]) : "";
                            queryModel.QueryHtml = dr["QueryHtml"] != DBNull.Value ? Convert.ToString(dr["QueryHtml"]) : "";
                            currentQuery = dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0;
                            QueryList.Add(queryModel);
                            currentQuery = dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0;
                            if (currentQuery > 0)
                            {
                                currentQueryList.Add(currentQuery);
                            }
                        }
                    }
                    ///
                    if (projectModel.SuperProjectId > 0)
                    {
                        queryBase.SuperProjectId = projectModel.SuperProjectId;
                        //(projectModel.ProjectType == 2)
                        actionResult = adminAction.Query_LoadAllBySuperProjectId(queryBase);
                        if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                        {
                            check = 0;
                            foreach (DataRow dr in actionResult.dtResult.Rows)
                            {
                                queryModel = new QueryModel();
                                queryModel.ProjectId = dr["ProjectId"] != DBNull.Value ? Convert.ToInt32(dr["ProjectId"]) : 0;
                                queryModel.CaseName = dr["CaseName"] != DBNull.Value ? Convert.ToString(dr["CaseName"]) : "";
                                queryModel.Query = dr["Query"] != DBNull.Value ? Convert.ToString(dr["Query"]) : "";
                                queryModel.QueryHtml = dr["QueryHtml"] != DBNull.Value ? Convert.ToString(dr["QueryHtml"]) : "";
                                currentQuery1 = dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0;
                                foreach (var item in currentQueryList)
                                {
                                    if (item == currentQuery1)
                                    {
                                        check = 1;
                                    }
                                }
                                if (check == 0)
                                {
                                    QueryList.Add(queryModel);
                                }
                            }
                        }
                    }
                    ///

                    model.QueryList = QueryList;


                    string QueryBuilder = string.Empty;

                    int ProjectId = projId;
                    //    int ProjectId = mainProjectId;
                    if (CaseContainer1.Columns.Count == 0)
                    {
                        CaseContainer1.Columns.Add("CaseName", typeof(string));
                        CaseContainer1.Columns.Add("Online", typeof(DataTable));
                        CaseContainer1.Columns.Add("Offline", typeof(DataTable));
                        CaseContainer1.Columns.Add("Combined", typeof(DataTable));
                    }


                    foreach (var item in QueryList)
                    {
                        QueryBuilder = item.Query;
                        if (QueryBuilder != null && QueryBuilder != string.Empty)
                        {
                            string[] query = QueryBuilder.TrimEnd('*').Split('*');
                            DataTable dtCase = new DataTable();
                            dtCase.Columns.Add("Id", typeof(int));
                            dtCase.Columns.Add("QuestionId", typeof(int));
                            dtCase.Columns.Add("QuestionTypeId", typeof(int));
                            //dtCase.Columns.Add("OptionIdOrDisplayOrder", typeof(int));
                            //dtCase.Columns.Add("ColumnIdOrDisplayOrder", typeof(int));
                            dtCase.Columns.Add("OptionIdOrDisplayOrder", typeof(string));
                            dtCase.Columns.Add("ColumnIdOrDisplayOrder", typeof(string));
                            dtCase.Columns.Add("Expression", typeof(string));
                            dtCase.Columns.Add("NextQuestionAddWith", typeof(string));


                            if (query.Length > 0)
                            {
                                for (int i = 0; i <= query.Length - 1; i++)
                                {
                                    string[] questionArr = query[i].Split('_'); //1602_5$>=11#OR|2~
                                    int questionId = Convert.ToInt32(questionArr[0]);
                                    string[] exprArr = questionArr[1].Split('$');
                                    int questionTypeId = Convert.ToInt32(exprArr[0]);
                                    string Expression = exprArr[1].Split('#')[0];
                                    string[] optionIdArr = exprArr[1].Split('#')[1].Split('|');
                                    string NextQuestionAddWith = optionIdArr[0];
                                    //int optionId = optionIdArr[1].Split('~')[0] != "" ? Convert.ToInt32(optionIdArr[1].Split('~')[0]) : 0;
                                    //int columnId = optionIdArr[1].Split('~')[1] != "" ? Convert.ToInt32(optionIdArr[1].Split('~')[1]) : 0;
                                    string optionId = optionIdArr[1].Split('~')[0] != "" ? Convert.ToString(optionIdArr[1].Split('~')[0]) : "0";
                                    string columnId = optionIdArr[1].Split('~')[1] != "" ? Convert.ToString(optionIdArr[1].Split('~')[1]) : "0";
                                    DataRow drCase = dtCase.NewRow();
                                    drCase["Id"] = (i + 1);
                                    drCase["QuestionId"] = questionId;
                                    drCase["QuestionTypeId"] = questionTypeId;
                                    drCase["OptionIdOrDisplayOrder"] = optionId;
                                    drCase["ColumnIdOrDisplayOrder"] = columnId;
                                    drCase["Expression"] = Expression;
                                    drCase["NextQuestionAddWith"] = NextQuestionAddWith;
                                    dtCase.Rows.Add(drCase);
                                }
                            }

                            projectBase.CaseTable = dtCase;
                            List<string> lstEmails = new List<string>();
                            List<string> lstResponders = new List<string>();
                            List<string> offlineResponders = new List<string>();
                            string Email = string.Empty;
                            string ExternalResponders = string.Empty;



                            actionResult = adminAction.Response_CaseSelectionReport(projectBase);

                            if (actionResult.IsSuccess)
                            {
                                if (Convert.ToString(actionResult.dtResult.Rows[0][0]) == "-1")
                                {

                                }
                                else
                                {
                                    foreach (DataRow dr in actionResult.dtResult.Rows)
                                    {
                                        ProjectId = dr["ProjectId"] != DBNull.Value ? Convert.ToInt32(dr["ProjectId"]) : 0;
                                        Email += dr["Email"] != DBNull.Value ? Convert.ToString(dr["Email"]) + "," : "";
                                        if (dr["Email"] != DBNull.Value && !String.IsNullOrEmpty(dr["Email"].ToString()))
                                            lstEmails.Add(dr["Email"].ToString());
                                        ExternalResponders += dr["ExternalResponderId"] != DBNull.Value ? Convert.ToString(dr["ExternalResponderId"]) + "," : "";
                                        if (dr["ExternalResponderId"] != DBNull.Value && !String.IsNullOrEmpty(dr["ExternalResponderId"].ToString()))
                                            lstResponders.Add(dr["ExternalResponderId"].ToString());
                                    }
                                }
                            }

                            //CreateProjectModel model1 = new CreateProjectModel();
                            //model1 = GetProjectDetailById(ProjectId);
                            DataTable dt;
                            if (projectModel.ProjectType == "1" || projectModel.ProjectType == "2")
                            {
                                dt = GetReportDataTableForProduct(projectGUID, lstEmails.ToArray(), lstResponders.ToArray());
                            }
                            else
                            {
                                dt = GetReportDataTable1(projectGUID, lstEmails.ToArray(), lstResponders.ToArray());
                            }
                            actionResult = adminAction.Response_CaseSelectionReportOffline(projectBase);
                            if (actionResult.IsSuccess)
                            {
                                if (Convert.ToString(actionResult.dtResult.Rows[0][0]) == "-1")
                                {

                                }
                                else
                                {
                                    ExternalResponders = "";
                                    foreach (DataRow dr in actionResult.dtResult.Rows)
                                    {
                                        ProjectId = dr["ProjectId"] != DBNull.Value ? Convert.ToInt32(dr["ProjectId"]) : 0;

                                        ExternalResponders += dr["RespId"] != DBNull.Value ? Convert.ToString(dr["RespId"]) + "," : "";
                                        if (dr["RespId"] != DBNull.Value && !String.IsNullOrEmpty(dr["RespId"].ToString()))
                                            offlineResponders.Add(dr["RespId"].ToString());

                                    }
                                }
                            }
                            int RespCount = 0;
                            DataTable dt1;
                            if (projectModel.ProjectType == "1" || projectModel.ProjectType == "2")
                            {
                                dt1 = GetReportDataTableOfflineForProduct(projectGUID, (RespCount + 1), offlineResponders.ToArray());
                            }
                            else
                            {
                                dt1 = GetReportDataTableOffline1(projectGUID, (RespCount + 1), offlineResponders.ToArray());
                            }

                            //if (dt.Rows.Count == 0 && dt1.Rows.Count == 0)
                            //{
                            //    TempData["ErrorMessage"] = "There is no data available regarding this query";
                            //    return RedirectToAction("QueryBuilder", "QueryBuilder", new { ProjectId = ProjectId, Area = "Report" });
                            //}
                            DataRow CaseRow = CaseContainer1.NewRow();
                            DataTable dt2 = new DataTable();
                            CaseRow[0] = item.CaseName;
                            CaseRow[1] = dt;
                            CaseRow[2] = dt1;
                            dt2 = dt.Copy();
                            dt2.Merge(dt1, true);
                            //     dt.AcceptChanges();
                            CaseRow[3] = dt2;
                            //            ViewBag.TotalResponses = (from DataRow dRow in dt2.Rows select dRow["Resp_Id"]).Distinct().Count();
                            CaseContainer1.Rows.Add(CaseRow);

                            string OnlineResponse = string.Empty;
                            if (dt.Columns.Count > 0)
                            {
                                OnlineResponse = ConvertDatatableToXML(dt);
                            }

                            string OfflineResponse = string.Empty;
                            if (dt1.Columns.Count > 0)
                            {
                                OfflineResponse = ConvertDatatableToXML(dt1);
                            }

                            string CombineResponse = string.Empty;
                            if (dt2.Columns.Count > 0)
                            {
                                CombineResponse = ConvertDatatableToXML(dt2);
                            }

                            actionResult = adminAction.ReportQueryBuilderCases_Add(projId, projectGUID, item.CaseName, OnlineResponse, OfflineResponse, CombineResponse);
                        }
                    }


                    Session["CaseContainer"] = null;
                    Session["CaseContainer"] = CaseContainer1;

                    int index = 0;
                    string ReportDataHtml = "";
                    string type = projectModel.ProjectType;
                    if (type == "2")
                    {
                        foreach (System.Data.DataRow dr in CaseContainer1.Rows)
                        {
                            index++;
                            ReportDataHtml += "<div class='caseRep" + index + "' style='display:none;'>";
                            ReportDataHtml += "<div class='panel-heading " + index + "' style=''>";
                            ReportDataHtml += "<h4 class='panel-tilte'><span data-name='ReportOf'>Report of </span><span style='color: #3498db'> " + dr["CaseName"] + "</span></h4></div>";
                            ReportDataHtml += "<div class='panel-body " + index + "' style='' data-id='" + index + "' data-name='" + dr["CaseName"] + "'>";
                            ReportDataHtml += "<table class='table table-striped table-bordered reportProductTable'><thead><tr>";
                            ReportDataHtml += "<th id='prodDescHeader' class='prodDescHeader' data-name='ProductDesc'>Product Description --></th>";
                            ReportDataHtml += "<th id='AtrAvgVariance' data-name='AverageVariance'>Average Variance</th>";
                            ReportDataHtml += "<th id='AtrAnova' data-name='Anova'>Anova</th>";
                            ReportDataHtml += "<th id='AtrStndDeviation' data-name='StandardDeviation'>Standard Deviation</th>";
                            ReportDataHtml += "<th id='AtrStndError' data-name='StandardError'>Standard Error</th>";
                            ReportDataHtml += "<th id='AtrSignificant99' data-name='SignificantDiff99'>Significant Difference at 99%</th>";
                            ReportDataHtml += "<th id='AtrSignificant95' data-name='SignificantDiff95'>Significant Difference at 95%</th>";
                            ReportDataHtml += "<th id='AtrSignificant90' data-name='SignificantDiff90'>Significant Difference at 90%</th>";
                            ReportDataHtml += "<th id='AtrSignificant80' data-name='SignificantDiff80'>Significant Difference at 80%</th>";
                            ReportDataHtml += "<th id='AtrFRatio' data-name='FRatio'>F-Ratio</th>";
                            ReportDataHtml += "</tr><tr>";
                            ReportDataHtml += "<th data-name='ProductCode' class='prodLogIdHeader'>Product Code --></th>";
                            ReportDataHtml += "<th></th><th></th><th></th><th></th><th></th><th></th><th></th><th></th><th></th></tr>";
                            ReportDataHtml += "<tr><th data-name='JudgementPerProduct  (Base Size)' class='judgementsPerProd'>Judgments Per Product (Base Size) --></th>";
                            ReportDataHtml += "<th></th><th></th><th></th><th></th><th></th><th></th><th></th><th></th><th></th></tr>";
                            ReportDataHtml += "</thead><tbody></tbody></table></div></div>";
                        }
                    }
                    else if (type == "3")
                    {
                        foreach (System.Data.DataRow dr in CaseContainer1.Rows)
                        {
                            index++;
                            ReportDataHtml += "<div class='caseRepKano" + index + "' style='display:none;'>";
                            ReportDataHtml += "<div class='panel-heading " + index + "' style=''>";
                            ReportDataHtml += "<h4 class='panel-tilte'><span data-name='ReportOf'>Report of </span><span style='color: #3498db'> " + dr["CaseName"] + "</span></h4></div>";
                            ReportDataHtml += "<div class='panel-body " + index + "' style='' data-id='" + index + "' data-name='" + dr["CaseName"] + "'>";
                            ReportDataHtml += "<table id='reportKanoTable" + index + "' class='table table-striped table-bordered'><thead><tr>";
                            ReportDataHtml += "<th  data-name='ResponderId' style='display: none;' class='noExport'>Responder Id</th>";
                            ReportDataHtml += "<th  data-name='tabbedQuestions'>Questions</th>";
                            ReportDataHtml += "<th  data-name='RecodeValue' style='display: none;'>Recode value</th>";
                            ReportDataHtml += "<th data-name='Import_Resp' style='display: none;'>Import Responder</th>";
                            ReportDataHtml += "</tr>";
                            ReportDataHtml += "</thead><tbody id='reportKanoTableTBody" + index + "'>";
                            ReportDataHtml += "<tr><td style='display: none;' class='noExport'></td><td></td><td style='display: none;'></td><td style='display: none;'></td></tr></tbody></table></div></div>";
                        }
                    }
                    string ViewBagType = null;
                    if (projectModel.lstProductProjectModel != null)
                    {
                        ViewBagType = projectModel.lstProductProjectModel.Count.ToString();
                    }
                    string SessionType = ConvertDatatableToXML_QueryBuilder(CaseContainer1);
                    actionResult = adminAction.ReportQueryBuilder_Add(projId, projectGUID, SessionType, ViewBagType, ReportDataHtml);

                    LastUpdate = DateTime.Now.ToString();
                    arr[0] = ReportDataHtml;
                    arr[1] = "Last Updated Report: " + LastUpdate;
                    return Json(arr, JsonRequestBehavior.AllowGet);
                    #endregion
                }
            }
            else
            {
                #region If Refresh Report
                CreateProjectModel model = new CreateProjectModel();
                DataTable CaseContainer1 = new DataTable();
                CreateProjectModel projectModel = new CreateProjectModel();
                projectModel = GetProjectDetailById(projId);
                List<int> currentQueryList = new List<int>();
                int currentQuery = 0;
                int currentQuery1 = 0;
                int check = 0;
                int mainProjectId = 0;

                QueryModel queryModel = new QueryModel();
                List<QueryModel> QueryList = new List<QueryModel>();
                queryBase.ProjectId = Convert.ToInt32(projId);
                actionResult = adminAction.Query_LoadAllByProjectId(queryBase);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    foreach (DataRow dr in actionResult.dtResult.Rows)
                    {
                        queryModel = new QueryModel();
                        queryModel.ProjectId = dr["ProjectId"] != DBNull.Value ? Convert.ToInt32(dr["ProjectId"]) : 0;
                        queryModel.CaseName = dr["CaseName"] != DBNull.Value ? Convert.ToString(dr["CaseName"]) : "";
                        queryModel.Query = dr["Query"] != DBNull.Value ? Convert.ToString(dr["Query"]) : "";
                        queryModel.QueryHtml = dr["QueryHtml"] != DBNull.Value ? Convert.ToString(dr["QueryHtml"]) : "";
                        currentQuery = dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0;
                        QueryList.Add(queryModel);
                        currentQuery = dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0;
                        if (currentQuery > 0)
                        {
                            currentQueryList.Add(currentQuery);
                        }
                    }
                }
                ///
                if (projectModel.SuperProjectId > 0)
                {
                    queryBase.SuperProjectId = projectModel.SuperProjectId;
                    //(projectModel.ProjectType == 2)
                    actionResult = adminAction.Query_LoadAllBySuperProjectId(queryBase);
                    if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                    {
                        check = 0;
                        foreach (DataRow dr in actionResult.dtResult.Rows)
                        {
                            queryModel = new QueryModel();
                            queryModel.ProjectId = dr["ProjectId"] != DBNull.Value ? Convert.ToInt32(dr["ProjectId"]) : 0;
                            queryModel.CaseName = dr["CaseName"] != DBNull.Value ? Convert.ToString(dr["CaseName"]) : "";
                            queryModel.Query = dr["Query"] != DBNull.Value ? Convert.ToString(dr["Query"]) : "";
                            queryModel.QueryHtml = dr["QueryHtml"] != DBNull.Value ? Convert.ToString(dr["QueryHtml"]) : "";
                            currentQuery1 = dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0;
                            foreach (var item in currentQueryList)
                            {
                                if (item == currentQuery1)
                                {
                                    check = 1;
                                }
                            }
                            if (check == 0)
                            {
                                QueryList.Add(queryModel);
                            }
                        }
                    }
                }
                ///

                model.QueryList = QueryList;


                string QueryBuilder = string.Empty;

                int ProjectId = projId;
                //    int ProjectId = mainProjectId;
                if (CaseContainer1.Columns.Count == 0)
                {
                    CaseContainer1.Columns.Add("CaseName", typeof(string));
                    CaseContainer1.Columns.Add("Online", typeof(DataTable));
                    CaseContainer1.Columns.Add("Offline", typeof(DataTable));
                    CaseContainer1.Columns.Add("Combined", typeof(DataTable));
                }


                foreach (var item in QueryList)
                {
                    QueryBuilder = item.Query;
                    if (QueryBuilder != null && QueryBuilder != string.Empty)
                    {
                        string[] query = QueryBuilder.TrimEnd('*').Split('*');
                        DataTable dtCase = new DataTable();
                        dtCase.Columns.Add("Id", typeof(int));
                        dtCase.Columns.Add("QuestionId", typeof(int));
                        dtCase.Columns.Add("QuestionTypeId", typeof(int));
                        //dtCase.Columns.Add("OptionIdOrDisplayOrder", typeof(int));
                        //dtCase.Columns.Add("ColumnIdOrDisplayOrder", typeof(int));
                        dtCase.Columns.Add("OptionIdOrDisplayOrder", typeof(string));
                        dtCase.Columns.Add("ColumnIdOrDisplayOrder", typeof(string));
                        dtCase.Columns.Add("Expression", typeof(string));
                        dtCase.Columns.Add("NextQuestionAddWith", typeof(string));


                        if (query.Length > 0)
                        {
                            for (int i = 0; i <= query.Length - 1; i++)
                            {
                                string[] questionArr = query[i].Split('_'); //1602_5$>=11#OR|2~
                                int questionId = Convert.ToInt32(questionArr[0]);
                                string[] exprArr = questionArr[1].Split('$');
                                int questionTypeId = Convert.ToInt32(exprArr[0]);
                                string Expression = exprArr[1].Split('#')[0];
                                string[] optionIdArr = exprArr[1].Split('#')[1].Split('|');
                                string NextQuestionAddWith = optionIdArr[0];
                                //int optionId = optionIdArr[1].Split('~')[0] != "" ? Convert.ToInt32(optionIdArr[1].Split('~')[0]) : 0;
                                //int columnId = optionIdArr[1].Split('~')[1] != "" ? Convert.ToInt32(optionIdArr[1].Split('~')[1]) : 0;
                                string optionId = optionIdArr[1].Split('~')[0] != "" ? Convert.ToString(optionIdArr[1].Split('~')[0]) : "0";
                                string columnId = optionIdArr[1].Split('~')[1] != "" ? Convert.ToString(optionIdArr[1].Split('~')[1]) : "0";
                                DataRow drCase = dtCase.NewRow();
                                drCase["Id"] = (i + 1);
                                drCase["QuestionId"] = questionId;
                                drCase["QuestionTypeId"] = questionTypeId;
                                drCase["OptionIdOrDisplayOrder"] = optionId;
                                drCase["ColumnIdOrDisplayOrder"] = columnId;
                                drCase["Expression"] = Expression;
                                drCase["NextQuestionAddWith"] = NextQuestionAddWith;
                                dtCase.Rows.Add(drCase);
                            }
                        }

                        projectBase.CaseTable = dtCase;
                        List<string> lstEmails = new List<string>();
                        List<string> lstResponders = new List<string>();
                        List<string> offlineResponders = new List<string>();
                        string Email = string.Empty;
                        string ExternalResponders = string.Empty;



                        actionResult = adminAction.Response_CaseSelectionReport(projectBase);

                        if (actionResult.IsSuccess)
                        {
                            if (Convert.ToString(actionResult.dtResult.Rows[0][0]) == "-1")
                            {

                            }
                            else
                            {
                                foreach (DataRow dr in actionResult.dtResult.Rows)
                                {
                                    ProjectId = dr["ProjectId"] != DBNull.Value ? Convert.ToInt32(dr["ProjectId"]) : 0;
                                    Email += dr["Email"] != DBNull.Value ? Convert.ToString(dr["Email"]) + "," : "";
                                    if (dr["Email"] != DBNull.Value && !String.IsNullOrEmpty(dr["Email"].ToString()))
                                        lstEmails.Add(dr["Email"].ToString());
                                    ExternalResponders += dr["ExternalResponderId"] != DBNull.Value ? Convert.ToString(dr["ExternalResponderId"]) + "," : "";
                                    if (dr["ExternalResponderId"] != DBNull.Value && !String.IsNullOrEmpty(dr["ExternalResponderId"].ToString()))
                                        lstResponders.Add(dr["ExternalResponderId"].ToString());
                                }
                            }
                        }

                        //CreateProjectModel model1 = new CreateProjectModel();
                        //model1 = GetProjectDetailById(ProjectId);
                        DataTable dt;
                        if (projectModel.ProjectType == "1" || projectModel.ProjectType == "2")
                        {
                            dt = GetReportDataTableForProduct(projectGUID, lstEmails.ToArray(), lstResponders.ToArray());
                        }
                        else
                        {
                            dt = GetReportDataTable1(projectGUID, lstEmails.ToArray(), lstResponders.ToArray());
                        }
                        actionResult = adminAction.Response_CaseSelectionReportOffline(projectBase);
                        if (actionResult.IsSuccess)
                        {
                            if (Convert.ToString(actionResult.dtResult.Rows[0][0]) == "-1")
                            {

                            }
                            else
                            {
                                ExternalResponders = "";
                                foreach (DataRow dr in actionResult.dtResult.Rows)
                                {
                                    ProjectId = dr["ProjectId"] != DBNull.Value ? Convert.ToInt32(dr["ProjectId"]) : 0;

                                    ExternalResponders += dr["RespId"] != DBNull.Value ? Convert.ToString(dr["RespId"]) + "," : "";
                                    if (dr["RespId"] != DBNull.Value && !String.IsNullOrEmpty(dr["RespId"].ToString()))
                                        offlineResponders.Add(dr["RespId"].ToString());

                                }
                            }
                        }
                        int RespCount = 0;
                        DataTable dt1;
                        if (projectModel.ProjectType == "1" || projectModel.ProjectType == "2")
                        {
                            dt1 = GetReportDataTableOfflineForProduct(projectGUID, (RespCount + 1), offlineResponders.ToArray());
                        }
                        else
                        {
                            dt1 = GetReportDataTableOffline1(projectGUID, (RespCount + 1), offlineResponders.ToArray());
                        }

                        //if (dt.Rows.Count == 0 && dt1.Rows.Count == 0)
                        //{
                        //    TempData["ErrorMessage"] = "There is no data available regarding this query";
                        //    return RedirectToAction("QueryBuilder", "QueryBuilder", new { ProjectId = ProjectId, Area = "Report" });
                        //}
                        DataRow CaseRow = CaseContainer1.NewRow();
                        DataTable dt2 = new DataTable();
                        CaseRow[0] = item.CaseName;
                        CaseRow[1] = dt;
                        CaseRow[2] = dt1;
                        dt2 = dt.Copy();
                        dt2.Merge(dt1, true);
                        //     dt.AcceptChanges();
                        CaseRow[3] = dt2;
                        //            ViewBag.TotalResponses = (from DataRow dRow in dt2.Rows select dRow["Resp_Id"]).Distinct().Count();
                        CaseContainer1.Rows.Add(CaseRow);

                        string OnlineResponse = string.Empty;
                        if (dt.Columns.Count > 0)
                        {
                            OnlineResponse = ConvertDatatableToXML(dt);
                        }

                        string OfflineResponse = string.Empty;
                        if (dt1.Columns.Count > 0)
                        {
                            OfflineResponse = ConvertDatatableToXML(dt1);
                        }

                        string CombineResponse = string.Empty;
                        if (dt2.Columns.Count > 0)
                        {
                            CombineResponse = ConvertDatatableToXML(dt2);
                        }

                        actionResult = adminAction.ReportQueryBuilderCases_Add(projId, projectGUID, item.CaseName, OnlineResponse, OfflineResponse, CombineResponse);
                    }
                }


                Session["CaseContainer"] = null;
                Session["CaseContainer"] = CaseContainer1;

                int index = 0;
                string ReportDataHtml = "";
                string type = projectModel.ProjectType;
                if (type == "2")
                {
                    foreach (System.Data.DataRow dr in CaseContainer1.Rows)
                    {
                        index++;
                        ReportDataHtml += "<div class='caseRep" + index + "' style='display:none;'>";
                        ReportDataHtml += "<div class='panel-heading " + index + "' style=''>";
                        ReportDataHtml += "<h4 class='panel-tilte'><span data-name='ReportOf'>Report of </span><span style='color: #3498db'> " + dr["CaseName"] + "</span></h4></div>";
                        ReportDataHtml += "<div class='panel-body " + index + "' style='' data-id='" + index + "' data-name='" + dr["CaseName"] + "'>";
                        ReportDataHtml += "<table class='table table-striped table-bordered reportProductTable'><thead><tr>";
                        ReportDataHtml += "<th id='prodDescHeader' class='prodDescHeader' data-name='ProductDesc'>Product Description --></th>";
                        ReportDataHtml += "<th id='AtrAvgVariance' data-name='AverageVariance'>Average Variance</th>";
                        ReportDataHtml += "<th id='AtrAnova' data-name='Anova'>Anova</th>";
                        ReportDataHtml += "<th id='AtrStndDeviation' data-name='StandardDeviation'>Standard Deviation</th>";
                        ReportDataHtml += "<th id='AtrStndError' data-name='StandardError'>Standard Error</th>";
                        ReportDataHtml += "<th id='AtrSignificant99' data-name='SignificantDiff99'>Significant Difference at 99%</th>";
                        ReportDataHtml += "<th id='AtrSignificant95' data-name='SignificantDiff95'>Significant Difference at 95%</th>";
                        ReportDataHtml += "<th id='AtrSignificant90' data-name='SignificantDiff90'>Significant Difference at 90%</th>";
                        ReportDataHtml += "<th id='AtrSignificant80' data-name='SignificantDiff80'>Significant Difference at 80%</th>";
                        ReportDataHtml += "<th id='AtrFRatio' data-name='FRatio'>F-Ratio</th>";
                        ReportDataHtml += "</tr><tr>";
                        ReportDataHtml += "<th data-name='ProductCode' class='prodLogIdHeader'>Product Code --></th>";
                        ReportDataHtml += "<th></th><th></th><th></th><th></th><th></th><th></th><th></th><th></th><th></th></tr>";
                        ReportDataHtml += "<tr><th data-name='JudgementPerProduct  (Base Size)' class='judgementsPerProd'>Judgments Per Product (Base Size) --></th>";
                        ReportDataHtml += "<th></th><th></th><th></th><th></th><th></th><th></th><th></th><th></th><th></th></tr>";
                        ReportDataHtml += "</thead><tbody></tbody></table></div></div>";
                    }
                }
                else if (type == "3")
                {
                    foreach (System.Data.DataRow dr in CaseContainer1.Rows)
                    {
                        index++;
                        ReportDataHtml += "<div class='caseRepKano" + index + "' style='display:none;'>";
                        ReportDataHtml += "<div class='panel-heading " + index + "' style=''>";
                        ReportDataHtml += "<h4 class='panel-tilte'><span data-name='ReportOf'>Report of </span><span style='color: #3498db'> " + dr["CaseName"] + "</span></h4></div>";
                        ReportDataHtml += "<div class='panel-body " + index + "' style='' data-id='" + index + "' data-name='" + dr["CaseName"] + "'>";
                        ReportDataHtml += "<table id='reportKanoTable" + index + "' class='table table-striped table-bordered'><thead><tr>";
                        ReportDataHtml += "<th  data-name='ResponderId' style='display: none;' class='noExport'>Responder Id</th>";
                        ReportDataHtml += "<th  data-name='tabbedQuestions'>Questions</th>";
                        ReportDataHtml += "<th  data-name='RecodeValue' style='display: none;'>Recode value</th>";
                        ReportDataHtml += "<th data-name='Import_Resp' style='display: none;'>Import Responder</th>";
                        ReportDataHtml += "</tr>";
                        ReportDataHtml += "</thead><tbody id='reportKanoTableTBody" + index + "'>";
                        ReportDataHtml += "<tr><td style='display: none;' class='noExport'></td><td></td><td style='display: none;'></td><td style='display: none;'></td></tr></tbody></table></div></div>";
                    }
                }
                string ViewBagType = null;
                if (projectModel.lstProductProjectModel != null)
                {
                    ViewBagType = projectModel.lstProductProjectModel.Count.ToString();
                }
                string SessionType = ConvertDatatableToXML_QueryBuilder(CaseContainer1);
                actionResult = adminAction.ReportQueryBuilder_Add(projId, projectGUID, SessionType, ViewBagType, ReportDataHtml);

                LastUpdate = DateTime.Now.ToString();
                arr[0] = ReportDataHtml;
                arr[1] = "Last Updated Report: " + LastUpdate;
                return Json(arr, JsonRequestBehavior.AllowGet);
                #endregion
            }
        }
        #endregion

        //-------------Azeez-------------//

        #region QueryIndex2 Get
        public ActionResult QueryIndex2(int projId, string ReportType)
        {
            DataTable CaseContainer1 = new DataTable();
            CreateProjectModel model = new CreateProjectModel();
            model = GetProjectDetailById(projId);
            string projectGUID = model.ProjectGuid;
            ViewBag.ProjectName = model.ProjectName;
            ViewBag.ProjectType = model.ProjectType;

            ViewBag.ReportType = ReportType;
            Session["ReportType"] = ReportType;
            ViewBag.ProjectGUID = projectGUID;
            ViewBag.ProjectId = projId;

            //CreateProjectModel projectModel = new CreateProjectModel();
            //projectModel = GetProjectDetailById(projId);
            //List<int> currentQueryList = new List<int>();
            //int currentQuery = 0;
            //int currentQuery1 = 0;
            //int check = 0;
            //int mainProjectId = 0;

            //QueryModel queryModel = new QueryModel();
            //List<QueryModel> QueryList = new List<QueryModel>();
            //queryBase.ProjectId = Convert.ToInt32(projId);
            //actionResult = adminAction.Query_LoadAllByProjectId(queryBase);
            //if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
            //{
            //    foreach (DataRow dr in actionResult.dtResult.Rows)
            //    {
            //        queryModel = new QueryModel();
            //        queryModel.ProjectId = dr["ProjectId"] != DBNull.Value ? Convert.ToInt32(dr["ProjectId"]) : 0;
            //        queryModel.CaseName = dr["CaseName"] != DBNull.Value ? Convert.ToString(dr["CaseName"]) : "";
            //        queryModel.Query = dr["Query"] != DBNull.Value ? Convert.ToString(dr["Query"]) : "";
            //        queryModel.QueryHtml = dr["QueryHtml"] != DBNull.Value ? Convert.ToString(dr["QueryHtml"]) : "";
            //        currentQuery = dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0;
            //        QueryList.Add(queryModel);
            //        currentQuery = dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0;
            //        if (currentQuery > 0)
            //        {
            //            currentQueryList.Add(currentQuery);
            //        }
            //    }
            //}
            /////
            //if (projectModel.SuperProjectId > 0)
            //{
            //    queryBase.SuperProjectId = projectModel.SuperProjectId;
            //    //(projectModel.ProjectType == 2)
            //    actionResult = adminAction.Query_LoadAllBySuperProjectId(queryBase);
            //    if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
            //    {
            //        check = 0;
            //        foreach (DataRow dr in actionResult.dtResult.Rows)
            //        {
            //            queryModel = new QueryModel();
            //            queryModel.ProjectId = dr["ProjectId"] != DBNull.Value ? Convert.ToInt32(dr["ProjectId"]) : 0;
            //            queryModel.CaseName = dr["CaseName"] != DBNull.Value ? Convert.ToString(dr["CaseName"]) : "";
            //            queryModel.Query = dr["Query"] != DBNull.Value ? Convert.ToString(dr["Query"]) : "";
            //            queryModel.QueryHtml = dr["QueryHtml"] != DBNull.Value ? Convert.ToString(dr["QueryHtml"]) : "";
            //            currentQuery1 = dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0;
            //            foreach (var item in currentQueryList)
            //            {
            //                if (item == currentQuery1)
            //                {
            //                    check = 1;
            //                }
            //            }
            //            if (check == 0)
            //            {
            //                QueryList.Add(queryModel);
            //            }
            //        }
            //    }
            //}
            /////

            //model.QueryList = QueryList;


            //string QueryBuilder = string.Empty;

            //int ProjectId = projId;
            //    int ProjectId = mainProjectId;







            return View();
        }
        #endregion

        #region GetQueryReportDataTableAjax
        [HttpPost]
        public JsonResult GetQueryReportDataTableAjax(int projId = 0, string ReportType = "")
        {
            string[] arr = new string[2];
            DataTable CaseContainer1 = new DataTable();
            CreateProjectModel model = new CreateProjectModel();
            model = GetProjectDetailById(projId);
            string projectGUID = model.ProjectGuid;


            CreateProjectModel projectModel = new CreateProjectModel();
            projectModel = GetProjectDetailById(projId);
            List<int> currentQueryList = new List<int>();
            int currentQuery = 0;
            int currentQuery1 = 0;
            int check = 0;
            int mainProjectId = 0;

            QueryModel queryModel = new QueryModel();
            List<QueryModel> QueryList = new List<QueryModel>();
            queryBase.ProjectId = Convert.ToInt32(projId);
            actionResult = adminAction.Query_LoadAllByProjectId(queryBase);
            if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
            {
                foreach (DataRow dr in actionResult.dtResult.Rows)
                {
                    queryModel = new QueryModel();
                    queryModel.ProjectId = dr["ProjectId"] != DBNull.Value ? Convert.ToInt32(dr["ProjectId"]) : 0;
                    queryModel.CaseName = dr["CaseName"] != DBNull.Value ? Convert.ToString(dr["CaseName"]) : "";
                    queryModel.Query = dr["Query"] != DBNull.Value ? Convert.ToString(dr["Query"]) : "";
                    queryModel.QueryHtml = dr["QueryHtml"] != DBNull.Value ? Convert.ToString(dr["QueryHtml"]) : "";
                    currentQuery = dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0;
                    QueryList.Add(queryModel);
                    currentQuery = dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0;
                    if (currentQuery > 0)
                    {
                        currentQueryList.Add(currentQuery);
                    }
                }
            }
            ///
            if (projectModel.SuperProjectId > 0)
            {
                queryBase.SuperProjectId = projectModel.SuperProjectId;
                //(projectModel.ProjectType == 2)
                actionResult = adminAction.Query_LoadAllBySuperProjectId(queryBase);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    check = 0;
                    foreach (DataRow dr in actionResult.dtResult.Rows)
                    {
                        queryModel = new QueryModel();
                        queryModel.ProjectId = dr["ProjectId"] != DBNull.Value ? Convert.ToInt32(dr["ProjectId"]) : 0;
                        queryModel.CaseName = dr["CaseName"] != DBNull.Value ? Convert.ToString(dr["CaseName"]) : "";
                        queryModel.Query = dr["Query"] != DBNull.Value ? Convert.ToString(dr["Query"]) : "";
                        queryModel.QueryHtml = dr["QueryHtml"] != DBNull.Value ? Convert.ToString(dr["QueryHtml"]) : "";
                        currentQuery1 = dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0;
                        foreach (var item in currentQueryList)
                        {
                            if (item == currentQuery1)
                            {
                                check = 1;
                            }
                        }
                        if (check == 0)
                        {
                            QueryList.Add(queryModel);
                        }
                    }
                }
            }
            ///

            model.QueryList = QueryList;


            string QueryBuilder = string.Empty;

            int ProjectId = projId;

            if (CaseContainer1.Columns.Count == 0)
            {
                CaseContainer1.Columns.Add("CaseName", typeof(string));
                CaseContainer1.Columns.Add("Online", typeof(DataTable));
                CaseContainer1.Columns.Add("Offline", typeof(DataTable));
                CaseContainer1.Columns.Add("Combined", typeof(DataTable));
            }
            foreach (var item in QueryList)
            {
                QueryBuilder = item.Query;
                if (QueryBuilder != null && QueryBuilder != string.Empty)
                {
                    string[] query = QueryBuilder.TrimEnd('*').Split('*');
                    DataTable dtCase = new DataTable();
                    dtCase.Columns.Add("Id", typeof(int));
                    dtCase.Columns.Add("QuestionId", typeof(int));
                    dtCase.Columns.Add("QuestionTypeId", typeof(int));
                    //dtCase.Columns.Add("OptionIdOrDisplayOrder", typeof(int));
                    //dtCase.Columns.Add("ColumnIdOrDisplayOrder", typeof(int));
                    dtCase.Columns.Add("OptionIdOrDisplayOrder", typeof(string));
                    dtCase.Columns.Add("ColumnIdOrDisplayOrder", typeof(string));
                    dtCase.Columns.Add("Expression", typeof(string));
                    dtCase.Columns.Add("NextQuestionAddWith", typeof(string));


                    if (query.Length > 0)
                    {
                        for (int i = 0; i <= query.Length - 1; i++)
                        {





                            string[] questionArr = query[i].Split('_'); //1602_5$>=11#OR|2~
                            int questionId = Convert.ToInt32(questionArr[0]);
                            string[] exprArr = questionArr[1].Split('$');
                            int questionTypeId = Convert.ToInt32(exprArr[0]);
                            string Expression = exprArr[1].Split('#')[0];
                            string[] optionIdArr = exprArr[1].Split('#')[1].Split('|');
                            string NextQuestionAddWith = optionIdArr[0];
                            //int optionId = optionIdArr[1].Split('~')[0] != "" ? Convert.ToInt32(optionIdArr[1].Split('~')[0]) : 0;
                            //int columnId = optionIdArr[1].Split('~')[1] != "" ? Convert.ToInt32(optionIdArr[1].Split('~')[1]) : 0;
                            string optionId = optionIdArr[1].Split('~')[0] != "" ? Convert.ToString(optionIdArr[1].Split('~')[0]) : "0";
                            string columnId = optionIdArr[1].Split('~')[1] != "" ? Convert.ToString(optionIdArr[1].Split('~')[1]) : "0";
                            DataRow drCase = dtCase.NewRow();
                            drCase["Id"] = (i + 1);
                            drCase["QuestionId"] = questionId;
                            drCase["QuestionTypeId"] = questionTypeId;
                            drCase["OptionIdOrDisplayOrder"] = optionId;
                            drCase["ColumnIdOrDisplayOrder"] = columnId;
                            drCase["Expression"] = Expression;
                            drCase["NextQuestionAddWith"] = NextQuestionAddWith;
                            dtCase.Rows.Add(drCase);
                        }
                    }

                    projectBase.CaseTable = dtCase;
                    List<string> lstEmails = new List<string>();
                    List<string> lstResponders = new List<string>();
                    List<string> offlineResponders = new List<string>();
                    string Email = string.Empty;
                    string ExternalResponders = string.Empty;



                    actionResult = adminAction.Response_CaseSelectionReport(projectBase);

                    if (actionResult.IsSuccess)
                    {
                        if (Convert.ToString(actionResult.dtResult.Rows[0][0]) == "-1")
                        {

                        }
                        else
                        {
                            foreach (DataRow dr in actionResult.dtResult.Rows)
                            {
                                ProjectId = dr["ProjectId"] != DBNull.Value ? Convert.ToInt32(dr["ProjectId"]) : 0;
                                Email += dr["Email"] != DBNull.Value ? Convert.ToString(dr["Email"]) + "," : "";
                                if (dr["Email"] != DBNull.Value && !String.IsNullOrEmpty(dr["Email"].ToString()))
                                    lstEmails.Add(dr["Email"].ToString());
                                ExternalResponders += dr["ExternalResponderId"] != DBNull.Value ? Convert.ToString(dr["ExternalResponderId"]) + "," : "";
                                if (dr["ExternalResponderId"] != DBNull.Value && !String.IsNullOrEmpty(dr["ExternalResponderId"].ToString()))
                                    lstResponders.Add(dr["ExternalResponderId"].ToString());

                            }



                        }
                    }

                    CreateProjectModel model1 = new CreateProjectModel();
                    model1 = GetProjectDetailById(ProjectId);

                    DataTable dt = GetReportDataTable1(projectGUID, lstEmails.ToArray(), lstResponders.ToArray());
                    actionResult = adminAction.Response_CaseSelectionReportOffline(projectBase);
                    if (actionResult.IsSuccess)
                    {
                        if (Convert.ToString(actionResult.dtResult.Rows[0][0]) == "-1")
                        {

                        }
                        else
                        {
                            ExternalResponders = "";
                            foreach (DataRow dr in actionResult.dtResult.Rows)
                            {
                                ProjectId = dr["ProjectId"] != DBNull.Value ? Convert.ToInt32(dr["ProjectId"]) : 0;

                                ExternalResponders += dr["RespId"] != DBNull.Value ? Convert.ToString(dr["RespId"]) + "," : "";
                                if (dr["RespId"] != DBNull.Value && !String.IsNullOrEmpty(dr["RespId"].ToString()))
                                    offlineResponders.Add(dr["RespId"].ToString());

                            }
                        }
                    }
                    int RespCount = 0;
                    DataTable dt1 = GetReportDataTableOffline1(projectGUID, (RespCount + 1), offlineResponders.ToArray());
                    //if (dt.Rows.Count == 0 && dt1.Rows.Count == 0)
                    //{
                    //    TempData["ErrorMessage"] = "There is no data available regarding this query";
                    //    return RedirectToAction("QueryBuilder", "QueryBuilder", new { ProjectId = ProjectId, Area = "Report" });
                    //}
                    DataRow CaseRow = CaseContainer1.NewRow();
                    DataTable dt2 = new DataTable();
                    CaseRow[0] = item.CaseName;
                    CaseRow[1] = dt;
                    CaseRow[2] = dt1;
                    dt2 = dt.Copy();
                    dt2.Merge(dt1, true);
                    //     dt.AcceptChanges();
                    CaseRow[3] = dt2;
                    //            ViewBag.TotalResponses = (from DataRow dRow in dt2.Rows select dRow["Resp_Id"]).Distinct().Count();
                    CaseContainer1.Rows.Add(CaseRow);


                }
            }

            ViewBag.TotalResponses = "0";
            arr[0] = "0";
            Session["CaseContainer"] = null;
            Session["CaseContainer"] = CaseContainer1;



            return Json(arr, JsonRequestBehavior.AllowGet);
        }
        #endregion

        public JsonResult GetResponderCount(int caseCount)
        {
            int column = 1;
            DataTable MainDt = (DataTable)Session["CaseContainer"];
            if (Session["ReportType"].ToString() == "Online")
            {
                column = 1;
            }
            else if (Session["ReportType"].ToString() == "Offline")
            {
                column = 2;
            }
            else
            {
                column = 3;
            }
            DataTable dtReport = (DataTable)MainDt.Rows[caseCount][column];
            return Json(dtReport.Rows.Count);
        }

        public JsonResult GetResponderCount1(int caseCount)
        {

            int resCount = 0;
            DataTable MainDt = (DataTable)Session["CaseContainer"];
            if (Session["ReportType"].ToString() == "Online")
            {
                DataTable dtReport = (DataTable)MainDt.Rows[caseCount][1];
                resCount = (from DataRow dRow in dtReport.Rows select dRow["Resp_Id"]).Distinct().Count();
            }
            else if (Session["ReportType"].ToString() == "Offline")
            {
                DataTable dtReports = (DataTable)MainDt.Rows[caseCount][2];
                resCount = (from DataRow dRow in dtReports.Rows select dRow["Resp_Id"]).Distinct().Count();
            }
            else
            {
                DataTable dtReport = (DataTable)MainDt.Rows[caseCount][1];
                resCount = (from DataRow dRow in dtReport.Rows select dRow["Resp_Id"]).Distinct().Count();
                DataTable dtReport1 = (DataTable)MainDt.Rows[caseCount][2];
                resCount += (from DataRow dRow in dtReport1.Rows select dRow["Resp_Id"]).Distinct().Count();
            }


            return Json(resCount);
        }

        public JsonResult GetResponderCount2(int caseCount)
        {
            int resCount = 0;
            DataTable MainDt = (DataTable)Session["CaseContainer"];
            if (Session["ReportType"].ToString() == "Online")
            {
                DataTable dtReport = (DataTable)MainDt.Rows[caseCount][1];
                resCount = (from DataRow dRow in dtReport.Rows select dRow["Resp_Id"]).Distinct().Count();
            }
            else if (Session["ReportType"].ToString() == "Offline")
            {
                DataTable dtReport = (DataTable)MainDt.Rows[caseCount][2];
                resCount = (from DataRow dRow in dtReport.Rows select dRow["Resp_Id"]).Distinct().Count();
            }
            else
            {
                DataTable dtReport = (DataTable)MainDt.Rows[caseCount][1];
                resCount = (from DataRow dRow in dtReport.Rows select dRow["Resp_Id"]).Distinct().Count();
                DataTable dtReport1 = (DataTable)MainDt.Rows[caseCount][2];
                resCount += (from DataRow dRow in dtReport1.Rows select dRow["Resp_Id"]).Distinct().Count();
            }
            return Json(resCount);
        }

        #region GetSurveyProducts
        public JsonResult GetSurveyProducts(int surveyId)
        {
            CreateProjectModel model = GetProjectDetailById(surveyId);
            return Json(Newtonsoft.Json.JsonConvert.SerializeObject(model.lstProductProjectModel), JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region GetProjectDetailById
        public CreateProjectModel GetProjectDetailById(int? id = 0)
        {
            CreateProjectModel model = new CreateProjectModel();
            List<ProductProjectModel> lstproductModel = new List<ProductProjectModel>();
            try
            {
                projectBase.Id = Convert.ToInt32(id);
                actionResult = adminAction.Project_LoadById(projectBase);
                if (actionResult.IsSuccess)
                {
                    DataRow dr = actionResult.dtResult.Rows[0];
                    model.Id = dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0;
                    model.SuperProjectId = dr["SuperProjectId"] != DBNull.Value ? Convert.ToInt32(dr["SuperProjectId"]) : 0;
                    model.ProjectName = dr["ProjectName"] != DBNull.Value ? Convert.ToString(dr["ProjectName"]) : "";
                    model.WelcomeMessage = dr["WelcomeMessage"] != DBNull.Value && !String.IsNullOrEmpty(dr["WelcomeMessage"].ToString()) ? Convert.ToString(dr["WelcomeMessage"]) : Email.ReadFile("Welcome.txt");
                    model.PageHeader = dr["PageHeader"] != DBNull.Value && !String.IsNullOrEmpty(dr["PageHeader"].ToString()) ? Convert.ToString(dr["PageHeader"]) : Email.ReadFile("Header.txt");
                    model.PageFooter = dr["PageFooter"] != DBNull.Value && !String.IsNullOrEmpty(dr["PageFooter"].ToString()) ? Convert.ToString(dr["PageFooter"]) : Email.ReadFile("Footer.txt");
                    model.ProjectCompletionMesssage = dr["ProjectCompletionMesssage"] != DBNull.Value && !String.IsNullOrEmpty(dr["ProjectCompletionMesssage"].ToString()) ? Convert.ToString(dr["ProjectCompletionMesssage"]) : Email.ReadFile("Complete.txt");
                    model.RedirectUrl = dr["RedirectUrl"] != DBNull.Value ? Convert.ToString(dr["RedirectUrl"]) : "";
                    model.InvitationMessage = dr["InvitationMessage"] != DBNull.Value ? Convert.ToString(dr["InvitationMessage"]) : "";
                    model.OwnerId = dr["OwnerId"] != DBNull.Value ? Convert.ToInt32(dr["OwnerId"]) : 0;
                    model.CreatedBy = dr["CreatedBy"] != DBNull.Value ? Convert.ToInt32(dr["CreatedBy"]) : 0;
                    model.ProjectQuota = dr["ProjectQuota"] != DBNull.Value ? Convert.ToInt32(dr["ProjectQuota"]) : 0;
                    model.TerminationMessageUpper = dr["ProjectTerminationMessage"] != DBNull.Value && !String.IsNullOrEmpty(dr["ProjectTerminationMessage"].ToString()) ? Convert.ToString(dr["ProjectTerminationMessage"]) : Email.ReadFile("TerminateUpper.txt");
                    model.ProjectGuid = dr["ProjectGuid"] != DBNull.Value ? Convert.ToString(dr["ProjectGuid"]) : "";
                    model.IsIncludePageHeader = dr["IsIncludePageHeader"] != DBNull.Value ? Convert.ToBoolean(dr["IsIncludePageHeader"]) : false;
                    model.IsIncludePageFooter = dr["IsIncludePageFooter"] != DBNull.Value ? Convert.ToBoolean(dr["IsIncludePageFooter"]) : false;
                    model.IsIncludeProjectCompletionMesssage = dr["IsIncludeProjectCompletionMesssage"] != DBNull.Value ? Convert.ToBoolean(dr["IsIncludeProjectCompletionMesssage"]) : false;
                    model.IsIncludeTerminationMessage = dr["IsIncludeTerminationMessage"] != DBNull.Value ? Convert.ToBoolean(dr["IsIncludeTerminationMessage"]) : false;
                    model.IsIncludeWelcomeMessage = dr["IsIncludeWelcomeMessage"] != DBNull.Value ? Convert.ToBoolean(dr["IsIncludeWelcomeMessage"]) : false;
                    model.ProjectType = dr["ProjectType"] != DBNull.Value ? Convert.ToString(dr["ProjectType"]) : "";
                    //model.ProductMessage = dr["ProductMessage"] != DBNull.Value ? Convert.ToString(dr["ProductMessage"]) : "";
                }

                productProjectBase.ProjectId = Convert.ToInt32(id);
                actionResult = adminAction.ProductProject_LoadById(productProjectBase);
                if (actionResult.IsSuccess)
                {
                    foreach (DataRow dr in actionResult.dtResult.Rows)
                    {
                        lstproductModel.Add(new ProductProjectModel
                        {
                            Id = dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0,
                            ProjectId = dr["ProjectId"] != DBNull.Value ? Convert.ToInt32(dr["ProjectId"]) : 0,
                            ProductLogicalId = dr["ProductLogicalId"] != DBNull.Value ? Convert.ToInt32(dr["ProductLogicalId"]) : 0,
                            Description = dr["Description"] != DBNull.Value ? Convert.ToString(dr["Description"]) : string.Empty,
                            DisplayOrder = dr["DisplayOrder"] != DBNull.Value ? Convert.ToInt32(dr["DisplayOrder"]) : 0
                        });
                    }
                    lstproductModel = lstproductModel.OrderBy(l => l.DisplayOrder).ToList();
                    model.lstProductProjectModel = lstproductModel;
                    ViewBag.ProductCount = lstproductModel.Count;
                }

            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return model;
        }
        #endregion

        #region GetReportDataTable
        private DataTable GetReportDataTable(string Id)
        {

            RespondentController respondent = new RespondentController();
            SurveyResponseModel model = new SurveyResponseModel();
            DataTable dtF = new DataTable();
            DataTable newData = new DataTable();
            try
            {
                DataTable dt = new DataTable();

                string projectGuid = string.Empty;
                projectGuid = Id;
                dt.Columns.Add("Resp_Id", typeof(string));

                model = respondent.GetProjectPreview(projectGuid);

                //--ss
                if (model.projectModel.ProjectType == "2")
                {
                    DataRow drnew = dt.NewRow();
                    drnew[0] = "Product_LogicalId";
                    dt.Rows.Add(drnew);
                    DataRow drnew1 = dt.NewRow();
                    drnew1[0] = "Product_Name";
                    dt.Rows.Add(drnew1);
                }
                //--

                // Load all the questions of this project
                List<QuestionList> questionsList = new List<QuestionList>();
                questionBase.ProjectId = Convert.ToInt32(model.projectModel.Id);
                actionResult = adminAction.QuestionsLoadAll_ByProjectId(questionBase);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    foreach (DataRow dr in actionResult.dtResult.Rows)
                    {
                        questionsList.Add(new QuestionList
                        {
                            id = dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0,
                            questionTypeId = dr["QuestionTypeId"] != DBNull.Value ? Convert.ToInt32(dr["QuestionTypeId"]) : 0
                        });
                    }
                }

                string fileName = Guid.NewGuid().ToString().Substring(0, 5) + "_" + DateTime.Now.ToString("MM-dd-yyyy") + "_" + model.projectModel.ProjectName.Replace(" ", "");

                if (model.lstQuestionModel != null && model.lstQuestionModel.Count > 0)
                {
                    foreach (var item in model.lstQuestionModel)
                    {
                        if (item.PipedFrom > 0 && (!item.QuestionTitle.Contains("{{Q}}")))
                        {
                            if (item.QuestionTypeId == 2 || item.QuestionTypeId == 4 || item.QuestionTypeId == 5 || item.QuestionTypeId == 6)//Multi Punch || Constant Sum || Ranking || Matrix Type
                            {
                                if (model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel != null && model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel.Count > 0)
                                {
                                    foreach (var opt in model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel)
                                    {
                                        DataRow drnew = dt.NewRow();
                                        drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                        dt.Rows.Add(drnew);
                                    }
                                }
                            }

                            else if (item.QuestionTypeId == 1 || item.QuestionTypeId == 3 || item.QuestionTypeId == 7 || item.QuestionTypeId == 15)//Single Punch || Range || Simple Text
                            {
                                DataRow drnew = dt.NewRow();
                                drnew[0] = "Q" + item.QuestionId + "_S" + "-" + ReplaceHtml.ReplaceText(item.QuestionTitle);
                                dt.Rows.Add(drnew);
                            }
                            else if (item.QuestionTypeId == 8 || item.QuestionTypeId == 12)//Single punch matrix type  && Rank order type
                            {
                                //if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
                                //{
                                //foreach (var opt in item.lstOptionsModel)
                                //{
                                //    DataRow drnew = dt.NewRow();
                                //    //drnew[0] = "Q" + item.QuestionId + "_M" + opt.OptionId + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                //    drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                //    dt.Rows.Add(drnew);
                                //}
                                //}
                                if (item.lstMatrixTypeModel != null && item.lstMatrixTypeModel.Count > 0)
                                {
                                    foreach (var col in item.lstMatrixTypeModel)
                                    {
                                        DataRow drnew = dt.NewRow();
                                        //drnew[0] = "Q" + item.QuestionId + "_M" + opt.OptionId + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                        drnew[0] = "Q" + item.QuestionId + "_M" + col.DisplayOrder + "-" + ReplaceHtml.ReplaceText(col.ColumnName);
                                        dt.Rows.Add(drnew);
                                    }
                                }
                            }
                            else if (item.QuestionTypeId == 13)//Kano model matrix type 
                            {
                                if (model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel != null && model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel.Count > 0)
                                {
                                    foreach (var opt in model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel)
                                    {
                                        DataRow drnew = dt.NewRow();
                                        drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Present";
                                        dt.Rows.Add(drnew);
                                        DataRow drnew1 = dt.NewRow();
                                        drnew1[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Absent";
                                        dt.Rows.Add(drnew1);
                                    }
                                }
                            }
                            else if (item.QuestionTypeId == 16) // Description type Question
                            {
                            }
                            // Single Punch Matrix Type || Range Matrix Type Question ||
                            // Single punch type answer horizontal position || 
                            // Multiple punch type answer horizontal position || 
                            // Rank order type question || 
                            // KANO Model Matrix type question || Dropdown Matrix type question
                            else if (item.QuestionTypeId > 7 && item.QuestionTypeId != 15)
                            {
                                if (model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel != null && model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel.Count > 0)
                                {
                                    foreach (var opt in model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel)
                                    {
                                        foreach (var column in item.lstMatrixTypeModel)
                                        {
                                            DataRow drnew = dt.NewRow();
                                            drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "_C" + column.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + " " + ReplaceHtml.ReplaceText(column.ColumnName);
                                            dt.Rows.Add(drnew);
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (item.QuestionTypeId == 2 || item.QuestionTypeId == 4 || item.QuestionTypeId == 5 || item.QuestionTypeId == 6)//Multi Punch || Constant Sum || Ranking || Matrix Type
                            {
                                if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
                                {
                                    foreach (var opt in item.lstOptionsModel)
                                    {
                                        DataRow drnew = dt.NewRow();
                                        //drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                        drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                        dt.Rows.Add(drnew);
                                    }
                                }
                            }

                            else if (item.QuestionTypeId == 1 || item.QuestionTypeId == 3 || item.QuestionTypeId == 7 || item.QuestionTypeId == 15)//Single Punch || Range || Simple Text
                            {
                                DataRow drnew = dt.NewRow();
                                //drnew[0] = "Q" + item.DisplayOrder + "_S" + "-" + ReplaceHtml.ReplaceText(item.QuestionTitle);
                                drnew[0] = "Q" + item.QuestionId + "_S" + "-" + ReplaceHtml.ReplaceText(item.QuestionTitle);
                                dt.Rows.Add(drnew);
                            }
                            else if (item.QuestionTypeId == 8 || item.QuestionTypeId == 12)//Single punch matrix type  && Rank order type
                            {
                                //if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
                                //{
                                //foreach (var opt in item.lstOptionsModel)
                                //{
                                //    DataRow drnew = dt.NewRow();
                                //    //drnew[0] = "Q" + item.QuestionId + "_M" + opt.OptionId + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                //    drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                //    dt.Rows.Add(drnew);
                                //}
                                //}
                                if (item.lstMatrixTypeModel != null && item.lstMatrixTypeModel.Count > 0)
                                {
                                    foreach (var col in item.lstMatrixTypeModel)
                                    {
                                        DataRow drnew = dt.NewRow();
                                        //drnew[0] = "Q" + item.QuestionId + "_M" + opt.OptionId + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                        drnew[0] = "Q" + item.QuestionId + "_M" + col.DisplayOrder + "-" + ReplaceHtml.ReplaceText(col.ColumnName);
                                        dt.Rows.Add(drnew);
                                    }
                                }
                            }
                            else if (item.QuestionTypeId == 13)//Kano model matrix type 
                            {
                                if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
                                {
                                    foreach (var opt in item.lstOptionsModel)
                                    {
                                        DataRow drnew = dt.NewRow();
                                        //drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Present";
                                        drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Present";
                                        dt.Rows.Add(drnew);
                                        DataRow drnew1 = dt.NewRow();
                                        //drnew1[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Absent";
                                        drnew1[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Absent";
                                        dt.Rows.Add(drnew1);
                                    }
                                }
                            }
                            // Single Punch Matrix Type || Range Matrix Type Question ||
                            // Single punch type answer horizontal position || 
                            // Multiple punch type answer horizontal position || 
                            // Rank order type question || 
                            // KANO Model Matrix type question || Dropdown Matrix type question
                            else if (item.QuestionTypeId > 7)
                            {
                                if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
                                {
                                    foreach (var opt in item.lstOptionsModel)
                                    {
                                        foreach (var column in item.lstMatrixTypeModel)
                                        {
                                            DataRow drnew = dt.NewRow();
                                            //drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "_C" + column.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + " " + ReplaceHtml.ReplaceText(column.ColumnName);
                                            drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "_C" + column.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + " " + ReplaceHtml.ReplaceText(column.ColumnName);
                                            dt.Rows.Add(drnew);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                // Get the Excel Pattern of dt
                dtF = comMethods.GenerateTransposedTable(dt);

                // Get all the reponders
                projectBase.Id = model.projectModel.Id;
                actionResult = adminAction.CompleteRespondersLoad_ByProjectId(projectBase);
                List<RespondersModel> lstResponders = new List<RespondersModel>();
                foreach (DataRow dr in actionResult.dtResult.Rows)
                {
                    lstResponders.Add(new RespondersModel
                    {

                        InvitationGuid = dr["InvitationGuid"] != DBNull.Value ? Convert.ToString(dr["InvitationGuid"]) : "",
                        Email = dr["Email"] != DBNull.Value ? Convert.ToString(dr["Email"]) : "",

                    });
                }
                IEnumerable<RespondersModel> filteredList = lstResponders
                 .GroupBy(customer => customer.InvitationGuid).Select(group => group.First());
                // Get Responses
                SurveyLoadBase surveyLoadBase = new SurveyLoadBase();
                string json = string.Empty;
                SRV.BaseLayer.ActionResult actionResultResponses = new SRV.BaseLayer.ActionResult();
                int response_id = 1;            //ss
                foreach (var responder in filteredList)
                {
                    DataTable responsesDt = new DataTable();
                    surveyLoadBase.Id = model.projectModel.Id;
                    surveyLoadBase.Email = (!string.IsNullOrEmpty(responder.Email) ? Convert.ToString(responder.Email) : "");
                    surveyLoadBase.ExternalResponderId = (!string.IsNullOrEmpty(responder.InvitationGuid) ? Convert.ToString(responder.InvitationGuid) : "");
                    if (model.projectModel.lstProductProjectModel != null && model.projectModel.lstProductProjectModel.Count > 0)
                    {
                        foreach (var productModel in model.projectModel.lstProductProjectModel)
                        {
                            surveyLoadBase.ProductId = productModel.Id;
                            actionResultResponses = respondentAction.Response_LoadByProjectId(surveyLoadBase);
                            if (actionResultResponses.IsSuccess)
                            {
                                if (responsesDt.Rows.Count == 0)
                                    responsesDt = actionResultResponses.dtResult.Copy();
                                else
                                {
                                    foreach (DataRow drThisRespRow in actionResultResponses.dtResult.Rows)
                                        responsesDt.ImportRow(drThisRespRow);
                                }
                            }
                        }
                    }
                    else
                    {
                        actionResultResponses = respondentAction.Response_LoadByProjectId(surveyLoadBase);
                        if (actionResultResponses.IsSuccess)
                            responsesDt = actionResultResponses.dtResult.Copy();

                    }

                    // surveyLoadBase.OfflineResponderId = dr["RespId"] != DBNull.Value ? Convert.ToString(dr["RespId"]) : "";
                    //DataRow dr_dtF = dtF.NewRow();
                    // if (String.IsNullOrEmpty(surveyLoadBase.Email)) continue;

                    //actionResultResponses = respondentAction.Response_LoadByProjectId(surveyLoadBase);
                    if (responsesDt.Rows.Count > 0)//actionResultResponses.IsSuccess
                    {
                        //dr_dtF[0] = surveyLoadBase.Email;

                        int count = 1;
                        count = (model.projectModel.ProjectType == "2") ? count + 2 : count;        //ss
                        DataRow dr_dtF = dtF.NewRow();
                        DataRow drTemp = dtF.NewRow();
                        string product_desc = string.Empty;
                        string product_logiId = string.Empty;
                        int oldProductID = responsesDt.Rows[0]["ProductId"] != DBNull.Value ? Convert.ToInt32(responsesDt.Rows[0]["ProductId"]) : 0;
                        foreach (DataRow drResponse in responsesDt.Rows)// actionResultResponses.dtResult.Rows
                        {

                            int optionsCount = 0;
                            // response_id = drResponse["ReponderId"] != DBNull.Value ? (responsesDt.Rows[0]["ReponderId"]).ToString() : "";
                            int productId = drResponse["ProductId"] != DBNull.Value ? Convert.ToInt32(drResponse["ProductId"]) : 0;
                            if (productId != oldProductID)
                            {
                                count = (model.projectModel.ProjectType == "2") ? 3 : 1;
                                dr_dtF = dtF.NewRow();
                            }

                            //--ss

                            product_desc = drResponse["ProductDescription"] != DBNull.Value ? Convert.ToString(drResponse["ProductDescription"]) : "";
                            product_logiId = drResponse["ProductLogicalId"] != DBNull.Value ? Convert.ToString(drResponse["ProductLogicalId"]) : "";

                            //--ss
                            int questionId = drResponse["QuestionId"] != DBNull.Value ? Convert.ToInt32(drResponse["QuestionId"]) : 0;
                            string answerExpression = drResponse["AnswerExpression"] != DBNull.Value ? Convert.ToString(drResponse["AnswerExpression"]) : "";
                            int questionTypeId = drResponse["QuestionTypeId"] != DBNull.Value ? Convert.ToInt32(drResponse["QuestionTypeId"]) : 1;
                            int PipedFrom = drResponse["PipedFrom"] != DBNull.Value ? Convert.ToInt32(drResponse["PipedFrom"]) : 0;
                            //dr_dtF[0] = drResponse;

                            // Get Total options count for this question
                            questionBase.Id = Convert.ToInt32(questionId);
                            SRV.BaseLayer.ActionResult actionResultAnswersTemp = new SRV.BaseLayer.ActionResult();
                            actionResultAnswersTemp = adminAction.Option_LoadAllById(questionBase);
                            if (actionResultAnswersTemp.dtResult != null && actionResultAnswersTemp.dtResult.Rows.Count > 0)
                            {
                                optionsCount = actionResultAnswersTemp.dtResult.Rows.Count;
                            }

                            if (questionTypeId == 2 || questionTypeId == 4 || questionTypeId == 5 || questionTypeId == 6)
                            {
                                string[] allAnswers = answerExpression.Split('|');
                                if (allAnswers.Length > 0)
                                {
                                    if (questionTypeId == 2)// Multi Punch
                                    {
                                        string allRec = string.Empty;
                                        List<OptionsModel> matrixAns = new List<OptionsModel>();
                                        if (PipedFrom > 0 && !drResponse["QuestionTitle"].ToString().Contains("{{Q}}"))
                                        {
                                            matrixAns = model.lstQuestionModel.Where(v => v.QuestionId == PipedFrom).FirstOrDefault().lstOptionsModel;
                                        }
                                        else
                                        {
                                            matrixAns = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                        }

                                        foreach (var item in matrixAns)
                                        {
                                            string matchedId = string.Empty;
                                            foreach (string ans in allAnswers)
                                            {
                                                if (Convert.ToString(item.OptionId) == ans)
                                                {
                                                    matchedId = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            allRec += matchedId + "|";
                                        }

                                        string[] allAnswersMat = allRec.Substring(0, allRec.Length - 1).Split('|');
                                        foreach (string ans in allAnswersMat)
                                        {

                                            if (count <= dtF.Columns.Count - 1)
                                            {
                                                if ((Regex.Replace(dtF.Columns[count].ToString().Split('-')[0].Split('_')[1], @"[^\d]", String.Empty)) == ans)
                                                {
                                                    dr_dtF[count] = "1";// ans;
                                                    count++;
                                                    optionsCount--;
                                                }
                                                else
                                                {
                                                    dr_dtF[count] = "2";
                                                    count++;
                                                    optionsCount--;
                                                }
                                            }
                                        }
                                        //count = count + optionsCount;
                                    }
                                    else if (questionTypeId == 6) // matrix Type
                                    {
                                        foreach (string ans in allAnswers)
                                        {
                                            dr_dtF[count] = (ans.ToLower().IndexOf("yes") != -1) ? "1" : "2";
                                            count++;
                                            optionsCount--;
                                        }
                                        count = count + optionsCount;
                                    }
                                    else
                                    {
                                        foreach (string ans in allAnswers)
                                        {
                                            dr_dtF[count] = ans;
                                            count++;
                                            optionsCount--;
                                        }
                                        count = count + optionsCount;
                                    }
                                }
                            }
                            else if (questionTypeId == 1) // Single Punch Answers
                            {
                                //List<OptionsModel> SinglePunchAns = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                List<OptionsModel> SinglePunchAns = new List<OptionsModel>();
                                if (PipedFrom > 0 && !drResponse["QuestionTitle"].ToString().Contains("{{Q}}"))
                                {
                                    SinglePunchAns = model.lstQuestionModel.Where(v => v.QuestionId == PipedFrom).FirstOrDefault().lstOptionsModel;
                                }
                                else
                                {
                                    SinglePunchAns = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                }
                                int ansIndex = 0;
                                foreach (var item in SinglePunchAns)
                                {
                                    ansIndex++;
                                    if (Convert.ToString(item.OptionId) == answerExpression)
                                    {
                                        break;
                                    }
                                }

                                dr_dtF[count] = ansIndex;//answerExpression;
                                count++;
                                optionsCount--;
                            }
                            else if (questionTypeId == 8 || questionTypeId == 12) // single punch matrix answers
                            {
                                List<OptionsModel> optionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                List<MatrixTypeModel> columnDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstMatrixTypeModel;
                                int qustionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).Select(v => v.DisplayOrder).FirstOrDefault();
                                string[] allAnswers = answerExpression.Split('|');

                                List<string> lstColumnOrdinals = new List<string>();

                                string formattedAnswer = string.Empty;
                                string answerValue = string.Empty;
                                if (allAnswers.Length > 0)
                                {
                                    foreach (var ansExpression in allAnswers)
                                    {
                                        string displaycol = string.Empty;
                                        string displayopt = string.Empty;
                                        foreach (var item in optionDisplayOrder)
                                        {

                                            if (Convert.ToString(item.OptionId) == ansExpression.Split('~')[0])
                                            {
                                                displaycol = Convert.ToString(item.DisplayOrder);
                                                break;
                                            }
                                        }
                                        foreach (var item in columnDisplayOrder)
                                        {
                                            if (Convert.ToString(item.Id) == ansExpression.Split('~')[1])
                                            {
                                                displayopt = Convert.ToString(item.DisplayOrder);
                                                break;
                                            }
                                        }
                                        //  formattedAnswer = "Q" + questionId + "_M" + displaycol + "#" + displayopt;
                                        formattedAnswer = "Q" + questionId + "_M" + displayopt + "#" + displaycol;
                                        foreach (DataColumn column in dtF.Columns)
                                        {
                                            string[] columnHeader = column.ColumnName.Split('-');
                                            string[] answer = columnHeader[0].Split('_');
                                            if (columnHeader.Length > 1)
                                            {
                                                string answerInHeader = answer[0] + "_" + answer[1];
                                                string[] formattedAnswerArr = formattedAnswer.Split('#');
                                                if (formattedAnswerArr[0] == answerInHeader)
                                                {
                                                    int index = dr_dtF.Table.Columns[column.ColumnName].Ordinal;
                                                    lstColumnOrdinals.Add(index.ToString());
                                                    count++;
                                                    dr_dtF[index] = formattedAnswerArr[1];
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            else if (questionTypeId == 13) // Kano model matrix type
                            {
                                List<OptionsModel> optionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                List<MatrixTypeModel> columnDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstMatrixTypeModel;
                                int qustionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).Select(v => v.DisplayOrder).FirstOrDefault();
                                string[] allAnswers = answerExpression.Split('|');

                                List<string> lstColumnOrdinals = new List<string>();

                                string formattedAnswer = string.Empty;
                                string answerValue = string.Empty;
                                if (allAnswers.Length > 1)
                                {
                                    int repeatId = 0;
                                    foreach (var ansExpression in allAnswers)
                                    {
                                        string displaycol = string.Empty;
                                        string displayopt = string.Empty;
                                        string displayText = string.Empty;
                                        string text = string.Empty;
                                        if (repeatId == Convert.ToInt32(ansExpression.Split('~')[0]))
                                        {
                                            text = "_Absent";
                                            repeatId = 0;
                                        }
                                        else
                                        {
                                            text = "_Present";
                                        }
                                        foreach (var item in optionDisplayOrder)
                                        {
                                            if (Convert.ToString(item.OptionId) == ansExpression.Split('~')[0])
                                            {
                                                displayText = Convert.ToString(item.Option) + text;
                                                displaycol = Convert.ToString(item.DisplayOrder);
                                                repeatId = Convert.ToInt32(ansExpression.Split('~')[0]);
                                                break;
                                            }
                                        }
                                        foreach (var item in columnDisplayOrder)
                                        {
                                            if (Convert.ToString(item.Id) == ansExpression.Split('~')[1])
                                            {
                                                displayopt = Convert.ToString(item.DisplayOrder);
                                                break;
                                            }
                                        }
                                        //formattedAnswer = "Q" + qustionDisplayOrder + "_M" + displaycol + "-" + displayText + "#" + displayopt;
                                        formattedAnswer = "Q" + questionId + "_M" + displaycol + "-" + displayText + "#" + displayopt;
                                        foreach (DataColumn column in dtF.Columns)
                                        {

                                            string[] columnHeader = column.ColumnName.Split('-');
                                            string[] answer = columnHeader[0].Split('_');
                                            if (columnHeader.Length > 1)
                                            {
                                                string answerInHeader = answer[0] + "_" + answer[1] + "-" + columnHeader[1];
                                                string[] formattedAnswerArr = formattedAnswer.Split('#');
                                                if (Regex.Replace(formattedAnswerArr[0], @"\s+", "", RegexOptions.Multiline) == Regex.Replace(answerInHeader, @"\s+", "", RegexOptions.Multiline))
                                                {
                                                    int index = dr_dtF.Table.Columns[column.ColumnName].Ordinal;
                                                    lstColumnOrdinals.Add(index.ToString());
                                                    count++;
                                                    dr_dtF[index] = formattedAnswerArr[1];
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            else if (questionTypeId == 16) // Description type Question
                            {
                            }
                            else if (questionTypeId > 7 && questionTypeId != 15)
                            {
                                List<OptionsModel> optionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                List<MatrixTypeModel> columnDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstMatrixTypeModel;
                                List<DDLMatrixTypeModel> dropDownDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstDDlMatrixModel;
                                count = count + (optionDisplayOrder.Count * columnDisplayOrder.Count);
                                int qustionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).Select(v => v.DisplayOrder).FirstOrDefault();
                                string[] allAnswers = answerExpression.Split('|');

                                List<string> lstColumnOrdinals = new List<string>();

                                string formattedAnswer = string.Empty;
                                string answerValue = string.Empty;
                                int DDlDisplayOrder = 0;
                                if (allAnswers.Length > 1)
                                {
                                    foreach (var ansExpression in allAnswers)
                                    {
                                        string displaycol = string.Empty;
                                        string displayopt = string.Empty;

                                        if (questionTypeId == 9 || questionTypeId == 14)
                                        {
                                            foreach (var item in optionDisplayOrder)
                                            {
                                                if (Convert.ToString(item.OptionId) == ansExpression.Split('~')[0].Split('*')[0])
                                                {
                                                    displaycol = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            foreach (var item in columnDisplayOrder)
                                            {
                                                if (Convert.ToString(item.Id) == ansExpression.Split('~')[1])
                                                {
                                                    displayopt = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            //formattedAnswer = "Q" + qustionDisplayOrder + "_M" + displaycol + "_C" + displayopt;
                                            formattedAnswer = "Q" + questionId + "_M" + displaycol + "_C" + displayopt;
                                            answerValue = ansExpression.Split('~')[0].Split('*')[1];
                                            if (questionTypeId == 14)
                                                DDlDisplayOrder = dropDownDisplayOrder.Where(l => l.Id == Convert.ToInt32(answerValue)).FirstOrDefault().DDLDisplayOrder;
                                        }
                                        else
                                        {
                                            foreach (var item in optionDisplayOrder)
                                            {
                                                if (Convert.ToString(item.OptionId) == ansExpression.Split('~')[0])
                                                {
                                                    displaycol = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            foreach (var item in columnDisplayOrder)
                                            {
                                                if (Convert.ToString(item.Id) == ansExpression.Split('~')[1])
                                                {
                                                    displayopt = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            //formattedAnswer = "Q" + qustionDisplayOrder + "_M" + displaycol + "_C" + displayopt;
                                            formattedAnswer = "Q" + questionId + "_M" + displaycol + "_C" + displayopt;
                                        }
                                        foreach (DataColumn column in dtF.Columns)
                                        {
                                            string[] columnHeader = column.ColumnName.Split('-');

                                            if (columnHeader.Length > 1)
                                            {
                                                string[] answer = columnHeader[0].Split('_');

                                                if (answer.Length > 2)
                                                {
                                                    string answerInHeader = answer[0] + "_" + answer[1] + "_" + answer[2];

                                                    if (formattedAnswer == answerInHeader)
                                                    {
                                                        int index = dr_dtF.Table.Columns[column.ColumnName].Ordinal;
                                                        lstColumnOrdinals.Add(index.ToString());
                                                        //count++;
                                                        if (questionTypeId == 9)
                                                        {
                                                            dr_dtF[index] = answerValue;
                                                        }
                                                        else if (questionTypeId == 14)
                                                        {
                                                            dr_dtF[index] = DDlDisplayOrder;
                                                        }
                                                        else
                                                        {
                                                            dr_dtF[index] = "1";
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }

                                // Fill All the Empty cells with 2

                                for (int i = 0; i <= dtF.Columns.Count - 1; i++)
                                    if (String.IsNullOrEmpty(Convert.ToString(dr_dtF[i])))
                                        dr_dtF[i] = "";
                            }

                            else
                            {
                                dr_dtF[count] = answerExpression;
                                count++;
                                optionsCount--;
                            }

                            if (productId != oldProductID)
                            {
                                dtF.Rows.Add(drTemp);
                                //  drTemp[0] = dtF.Rows.IndexOf(drTemp) + 1;
                                drTemp[0] = response_id;
                                oldProductID = productId;
                            }

                            drTemp = dr_dtF;
                            //---ss
                            if (model.projectModel.ProjectType == "2")
                            {
                                drTemp[2] = product_desc;
                                drTemp[1] = product_logiId;
                            }
                            //---ss

                        }

                        dtF.Rows.Add(dr_dtF);
                        //dr_dtF[0] = dtF.Rows.IndexOf(dr_dtF) + 1;
                        drTemp[0] = response_id;
                        //---ss
                        if (model.projectModel.ProjectType == "2")
                        {
                            dr_dtF[2] = product_desc;
                            dr_dtF[1] = product_logiId;
                        }
                        response_id++;
                        //---ss
                    }
                }

                if (dtF != null && dtF.Rows.Count > 0)
                {
                    //DataTable dtF = comMethods.GenerateTransposedTable(dt);
                    newData = comMethods.GenerateTransposedTable(dt);
                    foreach (DataRow dr in dtF.Rows)
                    {
                        if (model.projectModel.ProjectType == "2")
                        {
                            DataRow rw = newData.AsEnumerable().FirstOrDefault(tt => tt.Field<string>("Resp_Id") == dr["Resp_Id"].ToString() && tt.Field<string>("Product_Name") == dr["Product_Name"].ToString());
                            if (rw == null)
                            {
                                DataRow dr_dtF = newData.NewRow();

                                newData.Rows.Add(dr_dtF);
                                foreach (DataColumn dcTempKano in dtF.Columns)
                                {

                                    dr_dtF[dcTempKano.ToString()] = dr[dcTempKano.ToString()];

                                }
                                // row exists
                            }
                        }
                        else
                        {
                            DataRow rw = newData.AsEnumerable().FirstOrDefault(tt => tt.Field<string>("Resp_Id") == dr["Resp_Id"].ToString());
                            if (rw == null)
                            {
                                DataRow dr_dtF = newData.NewRow();

                                newData.Rows.Add(dr_dtF);
                                foreach (DataColumn dcTempKano in dtF.Columns)
                                {

                                    dr_dtF[dcTempKano.ToString()] = dr[dcTempKano.ToString()];

                                }
                                // row exists
                            }
                        }


                    }
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return newData;
        }
        #endregion

        #region GetReportDataTableOffline
        private DataTable GetReportDataTableOffline(string Id, int ResponseCount)
        {

            RespondentController respondent = new RespondentController();
            SurveyResponseModel model = new SurveyResponseModel();
            DataTable dtF = new DataTable();
            try
            {
                DataTable dt = new DataTable();

                string projectGuid = string.Empty;
                projectGuid = Id;
                dt.Columns.Add("Resp_Id", typeof(string));

                model = respondent.GetProjectPreview(projectGuid);

                //--ss
                if (model.projectModel.ProjectType == "2")
                {
                    DataRow drnew = dt.NewRow();
                    drnew[0] = "Product_LogicalId";
                    dt.Rows.Add(drnew);
                    DataRow drnew1 = dt.NewRow();
                    drnew1[0] = "Product_Name";
                    dt.Rows.Add(drnew1);
                }
                //--

                // Load all the questions of this project
                List<QuestionList> questionsList = new List<QuestionList>();
                questionBase.ProjectId = Convert.ToInt32(model.projectModel.Id);
                actionResult = adminAction.QuestionsLoadAll_ByProjectId(questionBase);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    foreach (DataRow dr in actionResult.dtResult.Rows)
                    {
                        questionsList.Add(new QuestionList
                        {
                            id = dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0,
                            questionTypeId = dr["QuestionTypeId"] != DBNull.Value ? Convert.ToInt32(dr["QuestionTypeId"]) : 0
                        });
                    }
                }
                string fileName = Guid.NewGuid().ToString().Substring(0, 5) + "_" + DateTime.Now.ToString("MM-dd-yyyy") + "_" + model.projectModel.ProjectName.Replace(" ", "");

                if (model.lstQuestionModel != null && model.lstQuestionModel.Count > 0)
                {
                    foreach (var item in model.lstQuestionModel)
                    {
                        if (item.PipedFrom > 0 && (!item.QuestionTitle.Contains("{{Q}}")))
                        {
                            if (item.QuestionTypeId == 2 || item.QuestionTypeId == 4 || item.QuestionTypeId == 5 || item.QuestionTypeId == 6)//Multi Punch || Constant Sum || Ranking || Matrix Type
                            {
                                if (model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel != null && model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel.Count > 0)
                                {
                                    foreach (var opt in model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel)
                                    {
                                        DataRow drnew = dt.NewRow();
                                        drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                        dt.Rows.Add(drnew);
                                    }
                                }
                            }

                            else if (item.QuestionTypeId == 1 || item.QuestionTypeId == 3 || item.QuestionTypeId == 7 || item.QuestionTypeId == 15)//Single Punch || Range || Simple Text
                            {
                                DataRow drnew = dt.NewRow();
                                drnew[0] = "Q" + item.QuestionId + "_S" + "-" + ReplaceHtml.ReplaceText(item.QuestionTitle);
                                dt.Rows.Add(drnew);
                            }
                            else if (item.QuestionTypeId == 8 || item.QuestionTypeId == 12)//Single punch matrix type  && Rank order type
                            {
                                //if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
                                //{
                                //foreach (var opt in item.lstOptionsModel)
                                //{
                                //    DataRow drnew = dt.NewRow();
                                //    //drnew[0] = "Q" + item.QuestionId + "_M" + opt.OptionId + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                //    drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                //    dt.Rows.Add(drnew);
                                //}
                                //}
                                if (item.lstMatrixTypeModel != null && item.lstMatrixTypeModel.Count > 0)
                                {
                                    foreach (var col in item.lstMatrixTypeModel)
                                    {
                                        DataRow drnew = dt.NewRow();
                                        //drnew[0] = "Q" + item.QuestionId + "_M" + opt.OptionId + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                        drnew[0] = "Q" + item.QuestionId + "_M" + col.DisplayOrder + "-" + ReplaceHtml.ReplaceText(col.ColumnName);
                                        dt.Rows.Add(drnew);
                                    }
                                }
                            }
                            else if (item.QuestionTypeId == 13)//Kano model matrix type 
                            {
                                if (model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel != null && model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel.Count > 0)
                                {
                                    foreach (var opt in model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel)
                                    {
                                        DataRow drnew = dt.NewRow();
                                        drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Present";
                                        dt.Rows.Add(drnew);
                                        DataRow drnew1 = dt.NewRow();
                                        drnew1[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Absent";
                                        dt.Rows.Add(drnew1);
                                    }
                                }
                            }
                            else if (item.QuestionTypeId == 16) // Description type Question
                            {
                            }
                            // Single Punch Matrix Type || Range Matrix Type Question ||
                            // Single punch type answer horizontal position || 
                            // Multiple punch type answer horizontal position || 
                            // Rank order type question || 
                            // KANO Model Matrix type question || Dropdown Matrix type question
                            else if (item.QuestionTypeId > 7 && item.QuestionTypeId != 15)
                            {
                                if (model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel != null && model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel.Count > 0)
                                {
                                    foreach (var opt in model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel)
                                    {
                                        foreach (var column in item.lstMatrixTypeModel)
                                        {
                                            DataRow drnew = dt.NewRow();
                                            drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "_C" + column.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + " " + ReplaceHtml.ReplaceText(column.ColumnName);
                                            dt.Rows.Add(drnew);
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (item.QuestionTypeId == 2 || item.QuestionTypeId == 4 || item.QuestionTypeId == 5 || item.QuestionTypeId == 6)//Multi Punch || Constant Sum || Ranking || Matrix Type
                            {
                                if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
                                {
                                    foreach (var opt in item.lstOptionsModel)
                                    {
                                        DataRow drnew = dt.NewRow();
                                        //drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                        drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                        dt.Rows.Add(drnew);
                                    }
                                }
                            }

                            else if (item.QuestionTypeId == 1 || item.QuestionTypeId == 3 || item.QuestionTypeId == 7 || item.QuestionTypeId == 15)//Single Punch || Range || Simple Text
                            {
                                DataRow drnew = dt.NewRow();
                                //drnew[0] = "Q" + item.DisplayOrder + "_S" + "-" + ReplaceHtml.ReplaceText(item.QuestionTitle);
                                drnew[0] = "Q" + item.QuestionId + "_S" + "-" + ReplaceHtml.ReplaceText(item.QuestionTitle);
                                dt.Rows.Add(drnew);
                            }
                            else if (item.QuestionTypeId == 8 || item.QuestionTypeId == 12)//Single punch matrix type  && Rank order type
                            {
                                //if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
                                //{
                                //foreach (var opt in item.lstOptionsModel)
                                //{
                                //    DataRow drnew = dt.NewRow();
                                //    //drnew[0] = "Q" + item.QuestionId + "_M" + opt.OptionId + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                //    drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                //    dt.Rows.Add(drnew);
                                //}
                                //}
                                if (item.lstMatrixTypeModel != null && item.lstMatrixTypeModel.Count > 0)
                                {
                                    foreach (var col in item.lstMatrixTypeModel)
                                    {
                                        DataRow drnew = dt.NewRow();
                                        //drnew[0] = "Q" + item.QuestionId + "_M" + opt.OptionId + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                        drnew[0] = "Q" + item.QuestionId + "_M" + col.DisplayOrder + "-" + ReplaceHtml.ReplaceText(col.ColumnName);
                                        dt.Rows.Add(drnew);
                                    }
                                }
                            }
                            else if (item.QuestionTypeId == 13)//Kano model matrix type 
                            {
                                if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
                                {
                                    foreach (var opt in item.lstOptionsModel)
                                    {
                                        DataRow drnew = dt.NewRow();
                                        //drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Present";
                                        drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Present";
                                        dt.Rows.Add(drnew);
                                        DataRow drnew1 = dt.NewRow();
                                        //drnew1[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Absent";
                                        drnew1[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Absent";
                                        dt.Rows.Add(drnew1);
                                    }
                                }
                            }
                            else if (item.QuestionTypeId == 16) // Description type Question
                            {
                            }
                            // Single Punch Matrix Type || Range Matrix Type Question ||
                            // Single punch type answer horizontal position || 
                            // Multiple punch type answer horizontal position || 
                            // Rank order type question || 
                            // KANO Model Matrix type question || Dropdown Matrix type question
                            else if (item.QuestionTypeId > 7)
                            {
                                if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
                                {
                                    foreach (var opt in item.lstOptionsModel)
                                    {
                                        foreach (var column in item.lstMatrixTypeModel)
                                        {
                                            DataRow drnew = dt.NewRow();
                                            //drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "_C" + column.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + " " + ReplaceHtml.ReplaceText(column.ColumnName);
                                            drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "_C" + column.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + " " + ReplaceHtml.ReplaceText(column.ColumnName);
                                            dt.Rows.Add(drnew);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                // Get the Excel Pattern of dt
                dtF = comMethods.GenerateTransposedTable(dt);

                // Get all the reponders
                projectBase.Id = model.projectModel.Id;
                actionResult = adminAction.Responders_Offline_LoadBy_ProjectId(projectBase);
                actionResult.dtResult = actionResult.dtResult.AsEnumerable().GroupBy(m => m["RespId"]).Select(m => m.First()).CopyToDataTable();
                // Get Responses
                SurveyLoadBase surveyLoadBase = new SurveyLoadBase();
                string json = string.Empty;
                SRV.BaseLayer.ActionResult actionResultResponses = new SRV.BaseLayer.ActionResult();
                int response_id = ResponseCount;            //ss
                foreach (DataRow dr in actionResult.dtResult.Rows)
                {
                    surveyLoadBase.Id = model.projectModel.Id;
                    surveyLoadBase.OfflineResponderId = dr["RespId"] != DBNull.Value ? Convert.ToString(dr["RespId"]) : "";                  
                    //DataRow dr_dtF = dtF.NewRow();

                    if (String.IsNullOrEmpty(surveyLoadBase.OfflineResponderId)) continue;


                    DataTable responsesDt = new DataTable();


                    if (model.projectModel.lstProductProjectModel != null && model.projectModel.lstProductProjectModel.Count > 0)
                    {
                        foreach (var productModel in model.projectModel.lstProductProjectModel)
                        {
                            surveyLoadBase.ProductId = productModel.Id;
                            actionResultResponses = respondentAction.Response_Offline_LoadBy_ProjectId_RespId(surveyLoadBase);
                            if (actionResultResponses.IsSuccess)
                            {
                                if (responsesDt.Rows.Count == 0)
                                    responsesDt = actionResultResponses.dtResult.Copy();
                                else
                                {
                                    foreach (DataRow drThisRespRow in actionResultResponses.dtResult.Rows)
                                        responsesDt.ImportRow(drThisRespRow);

                                }
                            }
                        }
                    }
                    else
                    {
                        actionResultResponses = respondentAction.Response_Offline_LoadBy_ProjectId_RespId(surveyLoadBase);
                        if (actionResultResponses.IsSuccess)
                            responsesDt = actionResultResponses.dtResult.Copy();

                    }

                    
                    //actionResultResponses = respondentAction.Response_Offline_LoadBy_ProjectId_RespId(surveyLoadBase);
                    if (responsesDt.Rows.Count > 0)//actionResultResponses.IsSuccess
                    {
                        //dr_dtF[0] = surveyLoadBase.Email;

                        int count = 1;
                        count = (model.projectModel.ProjectType == "2") ? count + 2 : count;        //ss
                        DataRow dr_dtF = dtF.NewRow();
                        DataRow drTemp = dtF.NewRow();
                        string product_desc = string.Empty;
                        string product_logiId = string.Empty;
                        int oldProductID = responsesDt.Rows[0]["ProductId"] != DBNull.Value ? Convert.ToInt32(responsesDt.Rows[0]["ProductId"]) : 0;
                        foreach (DataRow drResponse in responsesDt.Rows)// actionResultResponses.dtResult.Rows
                        {

                            int optionsCount = 0;

                            int productId = drResponse["ProductId"] != DBNull.Value ? Convert.ToInt32(drResponse["ProductId"]) : 0;
                            if (productId != oldProductID)
                            {
                                count = (model.projectModel.ProjectType == "2") ? 3 : 1;
                                dr_dtF = dtF.NewRow();
                            }

                            //--ss

                            product_desc = drResponse["ProductDescription"] != DBNull.Value ? Convert.ToString(drResponse["ProductDescription"]) : "";
                            product_logiId = drResponse["ProductLogicalId"] != DBNull.Value ? Convert.ToString(drResponse["ProductLogicalId"]) : "";

                            //--ss
                            int questionId = drResponse["QuestionId"] != DBNull.Value ? Convert.ToInt32(drResponse["QuestionId"]) : 0;
                            string answerExpression = drResponse["AnswerExpression"] != DBNull.Value ? Convert.ToString(drResponse["AnswerExpression"]) : "";
                            int questionTypeId = drResponse["QuestionTypeId"] != DBNull.Value ? Convert.ToInt32(drResponse["QuestionTypeId"]) : 1;
                            int PipedFrom = drResponse["PipedFrom"] != DBNull.Value ? Convert.ToInt32(drResponse["PipedFrom"]) : 0;
                            //dr_dtF[0] = drResponse;

                            // Get Total options count for this question
                            questionBase.Id = Convert.ToInt32(questionId);
                            SRV.BaseLayer.ActionResult actionResultAnswersTemp = new SRV.BaseLayer.ActionResult();
                            actionResultAnswersTemp = adminAction.Option_LoadAllById(questionBase);
                            if (actionResultAnswersTemp.dtResult != null && actionResultAnswersTemp.dtResult.Rows.Count > 0)
                            {
                                optionsCount = actionResultAnswersTemp.dtResult.Rows.Count;
                            }

                            if (questionTypeId == 2 || questionTypeId == 4 || questionTypeId == 5 || questionTypeId == 6)
                            {
                                string[] allAnswers = answerExpression.Split('|');
                                if (allAnswers.Length > 0)
                                {
                                    if (questionTypeId == 2)// Multi Punch
                                    {
                                        string allRec = string.Empty;
                                        //List<OptionsModel> matrixAns = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                        List<OptionsModel> matrixAns = new List<OptionsModel>();
                                        if (PipedFrom > 0 && !drResponse["QuestionTitle"].ToString().Contains("{{Q}}"))
                                        {
                                            matrixAns = model.lstQuestionModel.Where(v => v.QuestionId == PipedFrom).FirstOrDefault().lstOptionsModel;
                                        }
                                        else
                                        {
                                            matrixAns = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                        }
                                        foreach (var item in matrixAns)
                                        {
                                            string matchedId = string.Empty;
                                            foreach (string ans in allAnswers)
                                            {
                                                if (Convert.ToString(item.OptionId) == ans)
                                                {
                                                    matchedId = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            allRec += matchedId + "|";
                                        }

                                        string[] allAnswersMat = allRec.Substring(0, allRec.Length - 1).Split('|');
                                        foreach (string ans in allAnswersMat)
                                        {

                                            if (count <= dtF.Columns.Count - 1)
                                            {
                                                if ((Regex.Replace(dtF.Columns[count].ToString().Split('-')[0].Split('_')[1], @"[^\d]", String.Empty)) == ans)
                                                {
                                                    dr_dtF[count] = "1";// ans;
                                                    count++;
                                                    optionsCount--;
                                                }
                                                else
                                                {
                                                    dr_dtF[count] = "2";
                                                    count++;
                                                    optionsCount--;
                                                }
                                            }
                                        }
                                        //count = count + optionsCount;
                                    }
                                    else if (questionTypeId == 6) // matrix Type
                                    {
                                        foreach (string ans in allAnswers)
                                        {
                                            dr_dtF[count] = (ans.ToLower().IndexOf("yes") != -1) ? "1" : "2";
                                            count++;
                                            optionsCount--;
                                        }
                                        count = count + optionsCount;
                                    }
                                    else
                                    {
                                        foreach (string ans in allAnswers)
                                        {
                                            dr_dtF[count] = ans;
                                            count++;
                                            optionsCount--;
                                        }
                                        count = count + optionsCount;
                                    }
                                }
                            }
                            else if (questionTypeId == 1) // Single Punch Answers
                            {
                                //List<OptionsModel> SinglePunchAns = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                List<OptionsModel> SinglePunchAns = new List<OptionsModel>();
                                if (PipedFrom > 0 && !drResponse["QuestionTitle"].ToString().Contains("{{Q}}"))
                                {
                                    SinglePunchAns = model.lstQuestionModel.Where(v => v.QuestionId == PipedFrom).FirstOrDefault().lstOptionsModel;
                                }
                                else
                                {
                                    SinglePunchAns = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                }
                                int ansIndex = 0;
                                foreach (var item in SinglePunchAns)
                                {
                                    ansIndex++;
                                    if (Convert.ToString(item.OptionId) == answerExpression)
                                    {
                                        break;
                                    }
                                }

                                dr_dtF[count] = ansIndex;//answerExpression;
                                count++;
                                optionsCount--;
                            }
                            else if (questionTypeId == 8 || questionTypeId == 12) // single punch matrix answers
                            {
                                List<OptionsModel> optionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                List<MatrixTypeModel> columnDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstMatrixTypeModel;
                                int qustionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).Select(v => v.DisplayOrder).FirstOrDefault();
                                string[] allAnswers = answerExpression.Split('|');

                                List<string> lstColumnOrdinals = new List<string>();

                                string formattedAnswer = string.Empty;
                                string answerValue = string.Empty;
                                if (allAnswers.Length > 0)
                                {
                                    foreach (var ansExpression in allAnswers)
                                    {
                                        if (!string.IsNullOrEmpty(ansExpression))
                                        {
                                            string displaycol = string.Empty;
                                            string displayopt = string.Empty;
                                            foreach (var item in optionDisplayOrder)
                                            {

                                                if (Convert.ToString(item.OptionId) == ansExpression.Split('~')[0])
                                                {
                                                    displaycol = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            foreach (var item in columnDisplayOrder)
                                            {
                                                if (Convert.ToString(item.Id) == ansExpression.Split('~')[1])
                                                {
                                                    displayopt = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            //  formattedAnswer = "Q" + questionId + "_M" + displaycol + "#" + displayopt;
                                            formattedAnswer = "Q" + questionId + "_M" + displayopt + "#" + displaycol;
                                            foreach (DataColumn column in dtF.Columns)
                                            {
                                                string[] columnHeader = column.ColumnName.Split('-');
                                                string[] answer = columnHeader[0].Split('_');
                                                if (columnHeader.Length > 1)
                                                {
                                                    string answerInHeader = answer[0] + "_" + answer[1];
                                                    string[] formattedAnswerArr = formattedAnswer.Split('#');
                                                    if (formattedAnswerArr[0] == answerInHeader)
                                                    {
                                                        int index = dr_dtF.Table.Columns[column.ColumnName].Ordinal;
                                                        lstColumnOrdinals.Add(index.ToString());
                                                        count++;
                                                        dr_dtF[index] = formattedAnswerArr[1];
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            else if (questionTypeId == 13) // Kano model matrix type
                            {
                                List<OptionsModel> optionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                List<MatrixTypeModel> columnDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstMatrixTypeModel;
                                int qustionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).Select(v => v.DisplayOrder).FirstOrDefault();
                                string[] allAnswers = answerExpression.Split('|');

                                List<string> lstColumnOrdinals = new List<string>();

                                string formattedAnswer = string.Empty;
                                string answerValue = string.Empty;
                                if (allAnswers.Length > 1)
                                {
                                    int repeatId = 0;
                                    foreach (var ansExpression in allAnswers)
                                    {
                                        string displaycol = string.Empty;
                                        string displayopt = string.Empty;
                                        string displayText = string.Empty;
                                        string text = string.Empty;
                                        if (repeatId == Convert.ToInt32(ansExpression.Split('~')[0]))
                                        {
                                            text = "_Absent";
                                            repeatId = 0;
                                        }
                                        else
                                        {
                                            text = "_Present";
                                        }
                                        foreach (var item in optionDisplayOrder)
                                        {
                                            if (Convert.ToString(item.OptionId) == ansExpression.Split('~')[0])
                                            {
                                                displayText = Convert.ToString(item.Option) + text;
                                                displaycol = Convert.ToString(item.DisplayOrder);
                                                repeatId = Convert.ToInt32(ansExpression.Split('~')[0]);
                                                break;
                                            }
                                        }
                                        foreach (var item in columnDisplayOrder)
                                        {
                                            if (Convert.ToString(item.Id) == ansExpression.Split('~')[1])
                                            {
                                                displayopt = Convert.ToString(item.DisplayOrder);
                                                break;
                                            }
                                        }
                                        //formattedAnswer = "Q" + qustionDisplayOrder + "_M" + displaycol + "-" + displayText + "#" + displayopt;
                                        formattedAnswer = "Q" + questionId + "_M" + displaycol + "-" + displayText + "#" + displayopt;
                                        foreach (DataColumn column in dtF.Columns)
                                        {

                                            string[] columnHeader = column.ColumnName.Split('-');
                                            string[] answer = columnHeader[0].Split('_');
                                            if (columnHeader.Length > 1)
                                            {
                                                string answerInHeader = answer[0] + "_" + answer[1] + "-" + columnHeader[1];
                                                string[] formattedAnswerArr = formattedAnswer.Split('#');
                                                if (Regex.Replace(formattedAnswerArr[0], @"\s+", "", RegexOptions.Multiline) == Regex.Replace(answerInHeader, @"\s+", "", RegexOptions.Multiline))
                                                {
                                                    int index = dr_dtF.Table.Columns[column.ColumnName].Ordinal;
                                                    lstColumnOrdinals.Add(index.ToString());
                                                    count++;
                                                    dr_dtF[index] = formattedAnswerArr[1];
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            else if (questionTypeId == 16) // Description type Question
                            {
                            }
                            else if (questionTypeId > 7 && questionTypeId != 15)
                            {
                                List<OptionsModel> optionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                List<MatrixTypeModel> columnDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstMatrixTypeModel;
                                List<DDLMatrixTypeModel> dropDownDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstDDlMatrixModel;
                                count = count + (optionDisplayOrder.Count * columnDisplayOrder.Count);
                                int qustionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).Select(v => v.DisplayOrder).FirstOrDefault();
                                string[] allAnswers = answerExpression.Split('|');

                                List<string> lstColumnOrdinals = new List<string>();

                                string formattedAnswer = string.Empty;
                                string answerValue = string.Empty;
                                int DDlDisplayOrder = 0;
                                if (allAnswers.Length > 1)
                                {
                                    foreach (var ansExpression in allAnswers)
                                    {
                                        string displaycol = string.Empty;
                                        string displayopt = string.Empty;

                                        if (questionTypeId == 9 || questionTypeId == 14)
                                        {
                                            foreach (var item in optionDisplayOrder)
                                            {
                                                if (Convert.ToString(item.OptionId) == ansExpression.Split('~')[0].Split('*')[0])
                                                {
                                                    displaycol = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            foreach (var item in columnDisplayOrder)
                                            {
                                                if (Convert.ToString(item.Id) == ansExpression.Split('~')[1])
                                                {
                                                    displayopt = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            //formattedAnswer = "Q" + qustionDisplayOrder + "_M" + displaycol + "_C" + displayopt;
                                            formattedAnswer = "Q" + questionId + "_M" + displaycol + "_C" + displayopt;
                                            answerValue = ansExpression.Split('~')[0].Split('*')[1];
                                            if (questionTypeId == 14)
                                                DDlDisplayOrder = dropDownDisplayOrder.Where(l => l.Id == Convert.ToInt32(answerValue)).FirstOrDefault().DDLDisplayOrder;
                                        }
                                        else
                                        {
                                            foreach (var item in optionDisplayOrder)
                                            {
                                                if (Convert.ToString(item.OptionId) == ansExpression.Split('~')[0])
                                                {
                                                    displaycol = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            foreach (var item in columnDisplayOrder)
                                            {
                                                if (Convert.ToString(item.Id) == ansExpression.Split('~')[1])
                                                {
                                                    displayopt = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            //formattedAnswer = "Q" + qustionDisplayOrder + "_M" + displaycol + "_C" + displayopt;
                                            formattedAnswer = "Q" + questionId + "_M" + displaycol + "_C" + displayopt;
                                        }
                                        foreach (DataColumn column in dtF.Columns)
                                        {
                                            string[] columnHeader = column.ColumnName.Split('-');

                                            if (columnHeader.Length > 1)
                                            {
                                                string[] answer = columnHeader[0].Split('_');

                                                if (answer.Length > 2)
                                                {
                                                    string answerInHeader = answer[0] + "_" + answer[1] + "_" + answer[2];

                                                    if (formattedAnswer == answerInHeader)
                                                    {
                                                        int index = dr_dtF.Table.Columns[column.ColumnName].Ordinal;
                                                        lstColumnOrdinals.Add(index.ToString());
                                                        //count++;
                                                        if (questionTypeId == 9)
                                                        {
                                                            dr_dtF[index] = answerValue;
                                                        }
                                                        else if (questionTypeId == 14)
                                                        {
                                                            dr_dtF[index] = DDlDisplayOrder;
                                                        }
                                                        else
                                                        {
                                                            dr_dtF[index] = "1";
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }

                                // Fill All the Empty cells with 2

                                for (int i = 0; i <= dtF.Columns.Count - 1; i++)
                                    if (String.IsNullOrEmpty(Convert.ToString(dr_dtF[i])))
                                        dr_dtF[i] = "";
                            }

                            else
                            {
                                dr_dtF[count] = answerExpression;
                                count++;
                                optionsCount--;
                            }



                            if (productId != oldProductID)
                            {
                                dtF.Rows.Add(drTemp);
                                drTemp[0] = response_id;
                                oldProductID = productId;
                            }

                            drTemp = dr_dtF;
                            //---ss
                            if (model.projectModel.ProjectType == "2")
                            {
                                drTemp[2] = product_desc;
                                drTemp[1] = product_logiId;
                            }
                            //---ss


                        }


                        dtF.Rows.Add(dr_dtF);
                        drTemp[0] = response_id;
                        //---ss
                        if (model.projectModel.ProjectType == "2")
                        {
                            dr_dtF[2] = product_desc;
                            dr_dtF[1] = product_logiId;
                        }
                        response_id++;
                        //---ss
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return dtF;
        }
        #endregion

        #region GetReportDataTableAjax
        [HttpPost]
        public JsonResult GetReportDataTableAjax(string projectGUID = null, string ReportType = "", string ProjectId = null, string GenUpdatedReport = null)
        {
            string[] arr = new string[3];
            string LastUpdate = "";
            int TotalResponses = 0;
            string ReportHtml = "";

            DataTable dt = new DataTable();
            DataTable dt1 = new DataTable();
            int RespCount = 0;

            if (ReportType.ToLower() == "online")
            {
                #region Online Report
                if (GenUpdatedReport == null || GenUpdatedReport == "")
                {
                    actionResult = adminAction.OnlineProjectRespose_LoadById(projectGUID);
                    if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                    {
                        #region if Reponse save By Service
                        DataRow dr = actionResult.dtResult.Rows[0];
                        string response = dr["Response"] != DBNull.Value ? dr["Response"].ToString() : "";
                        LastUpdate = dr["CreatedDate"] != DBNull.Value ? dr["CreatedDate"].ToString() : "";
                        ReportHtml = dr["html"] != DBNull.Value ? dr["html"].ToString() : "";
                        dt = ConvertXMLToDataTable(response);
                        #endregion
                    }
                    else {
                        #region Else Reponse not save By Services
                        dt = GetReportDataTable(projectGUID);
                        LastUpdate = DateTime.Now.ToString();

                        if (dt.Columns.Count > 0)
                        {
                            if (ProjectId != null && ProjectId != "")
                            {
                                string onlineResponseXML = ConvertDatatableToXML(dt);

                                ReportHtml = "<table id='hdnReportHtml'>";

                                DataTable dtReport = dt;
                                if (dtReport != null)
                                {
                                    string[] cols = (from dc in dtReport.Columns.Cast<DataColumn>()
                                                     select dc.ColumnName)
                                                   .ToArray();
                                    if (cols != null)
                                    {
                                        ReportHtml += "<tr class='thRow'>";
                                        foreach (var item in cols)
                                        {
                                            ReportHtml += "<th>" + item + "</th>";
                                        }
                                        ReportHtml += "</tr>";
                                    }

                                    foreach (DataRow row in dtReport.Rows)
                                    {
                                        ReportHtml += "<tr class='tdRow'>";
                                        foreach (var item in row.ItemArray)
                                        {
                                            ReportHtml += "<td>" + item + "</td>";
                                        }
                                        ReportHtml += "</tr>";
                                    }
                                }

                                actionResult = adminAction.OnlineProjectRespose_Add(Convert.ToInt32(ProjectId), projectGUID, onlineResponseXML, ReportHtml);
                            }
                        }
                        #endregion
                    }
                }
                else
                {
                    #region If Refresh Report
                    dt = GetReportDataTable(projectGUID);
                    LastUpdate = DateTime.Now.ToString();
                    if (dt.Columns.Count > 0)
                    {
                        if (ProjectId != null && ProjectId != "")
                        {
                            string onlineResponseXML = ConvertDatatableToXML(dt);
                            #region Report Data HTML
                            ReportHtml = "<table id='hdnReportHtml'>";

                            DataTable dtReport = dt;
                            if (dtReport != null)
                            {
                                string[] cols = (from dc in dtReport.Columns.Cast<DataColumn>()
                                                 select dc.ColumnName)
                                               .ToArray();
                                if (cols != null)
                                {
                                    ReportHtml += "<tr class='thRow'>";
                                    foreach (var item in cols)
                                    {
                                        ReportHtml += "<th>" + item + "</th>";
                                    }
                                    ReportHtml += "</tr>";
                                }

                                foreach (DataRow row in dtReport.Rows)
                                {
                                    ReportHtml += "<tr class='tdRow'>";
                                    foreach (var item in row.ItemArray)
                                    {
                                        ReportHtml += "<td>" + item + "</td>";
                                    }
                                    ReportHtml += "</tr>";
                                }
                            }
                            #endregion
                            actionResult = adminAction.OnlineProjectRespose_Add(Convert.ToInt32(ProjectId), projectGUID, onlineResponseXML, ReportHtml);
                        }
                    }
                    #endregion
                }
                #endregion
            }
            else if (ReportType.ToLower() == "offline")
            {
                #region Offline Report
                if (GenUpdatedReport == null || GenUpdatedReport == "")
                {
                    actionResult = adminAction.ProjectRespose_LoadById(projectGUID);
                    if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                    {
                        #region if Reponse save By Service
                        DataRow dr = actionResult.dtResult.Rows[0];
                        string response = dr["Response"] != DBNull.Value ? dr["Response"].ToString() : "";
                        LastUpdate = dr["CreatedDate"] != DBNull.Value ? dr["CreatedDate"].ToString() : "";
                        ReportHtml = dr["html"] != DBNull.Value ? dr["html"].ToString() : "";
                        DataSet ds = new DataSet();
                        using (StringReader stringReader = new StringReader(response))
                        {
                            ds = new DataSet();
                            ds.ReadXml(stringReader);
                        }
                        dt = ds.Tables[0];
                        #endregion
                    }
                    else {
                        #region Else Reponse not save By Services
                        dt = GetReportDataTableOffline(projectGUID, (RespCount + 1));
                        LastUpdate = DateTime.Now.ToString();
                        if (dt.Columns.Count > 0)
                        {
                            if (ProjectId != null && ProjectId != "")
                            {
                                string responseXML = ConvertDatatableToXML(dt);

                                ReportHtml = "<table id='hdnReportHtml'>";

                                DataTable dtReport = dt;
                                if (dtReport != null)
                                {
                                    string[] cols = (from dc in dtReport.Columns.Cast<DataColumn>()
                                                     select dc.ColumnName)
                                                   .ToArray();
                                    if (cols != null)
                                    {
                                        ReportHtml += "<tr class='thRow'>";
                                        foreach (var item in cols)
                                        {
                                            ReportHtml += "<th>" + item + "</th>";
                                        }
                                        ReportHtml += "</tr>";
                                    }

                                    foreach (DataRow row in dtReport.Rows)
                                    {
                                        ReportHtml += "<tr class='tdRow'>";
                                        foreach (var item in row.ItemArray)
                                        {
                                            ReportHtml += "<td>" + item + "</td>";
                                        }
                                        ReportHtml += "</tr>";
                                    }
                                }
                                actionResult = adminAction.ProjectRespose_Add(Convert.ToInt32(ProjectId), projectGUID, responseXML, ReportHtml);
                            }
                        }
                        #endregion
                    }
                }
                else
                {
                    #region If Refresh Report
                    dt = GetReportDataTableOffline(projectGUID, (RespCount + 1));
                    LastUpdate = DateTime.Now.ToString();
                    if (dt.Columns.Count > 0)
                    {
                        if (ProjectId != null && ProjectId != "")
                        {
                            string responseXML = ConvertDatatableToXML(dt);
                            #region Report Data HTML
                            ReportHtml = "<table id='hdnReportHtml'>";

                            DataTable dtReport = dt;
                            if (dtReport != null)
                            {
                                string[] cols = (from dc in dtReport.Columns.Cast<DataColumn>()
                                                 select dc.ColumnName)
                                               .ToArray();
                                if (cols != null)
                                {
                                    ReportHtml += "<tr class='thRow'>";
                                    foreach (var item in cols)
                                    {
                                        ReportHtml += "<th>" + item + "</th>";
                                    }
                                    ReportHtml += "</tr>";
                                }

                                foreach (DataRow row in dtReport.Rows)
                                {
                                    ReportHtml += "<tr class='tdRow'>";
                                    foreach (var item in row.ItemArray)
                                    {
                                        ReportHtml += "<td>" + item + "</td>";
                                    }
                                    ReportHtml += "</tr>";
                                }
                            }
                            #endregion
                            actionResult = adminAction.ProjectRespose_Add(Convert.ToInt32(ProjectId), projectGUID, responseXML, ReportHtml);
                        }
                    }
                    #endregion
                }
                #endregion
            }
            else if (ReportType.ToLower() == "combined")
            {
                #region Combined Report
                if (GenUpdatedReport == null || GenUpdatedReport == "")
                {
                    actionResult = adminAction.CombinedProjectRespose_LoadById(projectGUID);
                    if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                    {
                        #region if Reponse save By Service
                        DataRow dr = actionResult.dtResult.Rows[0];
                        string response = dr["Response"] != DBNull.Value ? dr["Response"].ToString() : "";
                        LastUpdate = dr["CreatedDate"] != DBNull.Value ? dr["CreatedDate"].ToString() : "";
                        ReportHtml = dr["html"] != DBNull.Value ? dr["html"].ToString() : "";
                        DataSet ds = new DataSet();
                        using (StringReader stringReader = new StringReader(response))
                        {
                            ds = new DataSet();
                            ds.ReadXml(stringReader);
                        }
                        dt = ds.Tables[0];
                        #endregion
                    }
                    else {
                        #region Else Reponse not save By Services
                        dt = GetReportDataTable(projectGUID);
                        RespCount = (from DataRow dRow in dt.Rows select dRow["Resp_Id"]).Distinct().Count();
                        dt1 = GetReportDataTableOffline(projectGUID, (RespCount + 1));
                        dt.Merge(dt1);
                        LastUpdate = DateTime.Now.ToString();
                        if (dt.Columns.Count > 0)
                        {
                            if (ProjectId != null && ProjectId != "")
                            {
                                string responseXML = ConvertDatatableToXML(dt);
                                #region Report Data HTML
                                ReportHtml = "<table id='hdnReportHtml'>";

                                DataTable dtReport = dt;
                                if (dtReport != null)
                                {
                                    string[] cols = (from dc in dtReport.Columns.Cast<DataColumn>()
                                                     select dc.ColumnName)
                                                   .ToArray();
                                    if (cols != null)
                                    {
                                        ReportHtml += "<tr class='thRow'>";
                                        foreach (var item in cols)
                                        {
                                            ReportHtml += "<th>" + item + "</th>";
                                        }
                                        ReportHtml += "</tr>";
                                    }

                                    foreach (DataRow row in dtReport.Rows)
                                    {
                                        ReportHtml += "<tr class='tdRow'>";
                                        foreach (var item in row.ItemArray)
                                        {
                                            ReportHtml += "<td>" + item + "</td>";
                                        }
                                        ReportHtml += "</tr>";
                                    }
                                }
                                #endregion
                                actionResult = adminAction.CombinedProjectRespose_Add(Convert.ToInt32(ProjectId), projectGUID, responseXML, ReportHtml);
                            }
                        }
                        #endregion
                    }
                }
                else {
                    #region If Refresh Report
                    dt = GetReportDataTable(projectGUID);
                    RespCount = (from DataRow dRow in dt.Rows select dRow["Resp_Id"]).Distinct().Count();
                    dt1 = GetReportDataTableOffline(projectGUID, (RespCount + 1));
                    dt.Merge(dt1);
                    LastUpdate = DateTime.Now.ToString();
                    if (dt.Columns.Count > 0)
                    {
                        if (ProjectId != null && ProjectId != "")
                        {
                            string responseXML = ConvertDatatableToXML(dt);
                            #region Report Data HTML
                            ReportHtml = "<table id='hdnReportHtml'>";

                            DataTable dtReport = dt;
                            if (dtReport != null)
                            {
                                string[] cols = (from dc in dtReport.Columns.Cast<DataColumn>()
                                                 select dc.ColumnName)
                                               .ToArray();
                                if (cols != null)
                                {
                                    ReportHtml += "<tr class='thRow'>";
                                    foreach (var item in cols)
                                    {
                                        ReportHtml += "<th>" + item + "</th>";
                                    }
                                    ReportHtml += "</tr>";
                                }

                                foreach (DataRow row in dtReport.Rows)
                                {
                                    ReportHtml += "<tr class='tdRow'>";
                                    foreach (var item in row.ItemArray)
                                    {
                                        ReportHtml += "<td>" + item + "</td>";
                                    }
                                    ReportHtml += "</tr>";
                                }
                            }
                            #endregion
                            actionResult = adminAction.CombinedProjectRespose_Add(Convert.ToInt32(ProjectId), projectGUID, responseXML, ReportHtml);
                        }
                    }
                    #endregion
                }
                #endregion
            }


            TotalResponses = (from DataRow dRow in dt.Rows select dRow["Resp_Id"]).Distinct().Count();
            arr[0] = TotalResponses.ToString();
            Session["ReportDataTable"] = dt;
            ViewBag.RDT = dt;

            if (ReportHtml == "")
            {
                string ReportDataHtml = "<table id='hdnReportHtml'>";
                if (Session["ReportDataTable"] != null)
                {
                    DataTable dtReport = (DataTable)Session["ReportDataTable"];
                    if (dtReport != null)
                    {
                        string[] cols = (from dc in dtReport.Columns.Cast<DataColumn>()
                                         select dc.ColumnName)
                                       .ToArray();
                        if (cols != null)
                        {
                            ReportDataHtml += "<tr class='thRow'>";
                            foreach (var item in cols)
                            {
                                ReportDataHtml += "<th>" + item + "</th>";
                            }
                            ReportDataHtml += "</tr>";
                        }

                        foreach (DataRow row in dtReport.Rows)
                        {
                            ReportDataHtml += "<tr class='tdRow'>";
                            foreach (var item in row.ItemArray)
                            {
                                ReportDataHtml += "<td>" + item + "</td>";
                            }
                            ReportDataHtml += "</tr>";
                        }
                    }
                }
                ReportDataHtml += "</table>";
                ViewBag.ReportDataHtml = ReportDataHtml;
                arr[1] = ReportDataHtml;
            }
            else
            {
                ViewBag.ReportDataHtml = ReportHtml;
                arr[1] = ReportHtml;
            }
            arr[2] = "Last Updated Report: " + LastUpdate;

            return Json(arr, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Ajax_ReportHtml
        [HttpPost]
        public JsonResult Ajax_ReportHtml()
        {
            string ReportDataHtml = "<table id='hdnReportHtml'>";
            if (Session["ReportDataTable"] != null)
            {
                DataTable dtReport = (DataTable)Session["ReportDataTable"];
                if (dtReport != null)
                {
                    string[] cols = (from dc in dtReport.Columns.Cast<DataColumn>()
                                     select dc.ColumnName)
                                   .ToArray();
                    if (cols != null)
                    {
                        ReportDataHtml += "<tr class='thRow'>";
                        foreach (var item in cols)
                        {
                            ReportDataHtml += "<th>" + item + "</th>";
                        }
                        ReportDataHtml += "</tr>";
                    }

                    foreach (DataRow row in dtReport.Rows)
                    {
                        ReportDataHtml += "<tr class='tdRow'>";
                        foreach (var item in row.ItemArray)
                        {
                            ReportDataHtml += "<td>" + item + "</td>";
                        }
                        ReportDataHtml += "</tr>";
                    }
                }
            }
            ReportDataHtml += "</table>";
            ViewBag.ReportDataHtml = ReportDataHtml;
            string[] arr = { "111", "222", "333" };

            Array array = arr.ToArray();


            //   return ReportDataHtml;
            return Json(array, JsonRequestBehavior.AllowGet);
        }
        #endregion





        #region QuestionList Get
        public JsonResult QuestionList(int id = 0)
        {
            string json = string.Empty;
            try
            {

                questionBase.ProjectId = id;
                actionResult = adminAction.QuestionsLoadAll_ByProjectId(questionBase);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    json = Newtonsoft.Json.JsonConvert.SerializeObject(actionResult.dtResult);
                }

            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return Json(json, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Bind Option
        public JsonResult BindOption(string Id)
        {
            List<OptionListModel> optionList = new List<OptionListModel>();

            if (!string.IsNullOrEmpty(Id))
            {
                questionBase.Id = Convert.ToInt32(Id);
                actionResult = adminAction.Option_LoadAllById(questionBase);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    optionList = CommHelper.ConvertTo<OptionListModel>(actionResult.dtResult);
                }
            }
            return Json(Newtonsoft.Json.JsonConvert.SerializeObject(optionList), JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region BindColumns
        public ActionResult BindColumns(int QuestionId)
        {
            string json = string.Empty;
            CreateQuestionModel model = new CreateQuestionModel();
            MatrixTypeModel matrixModel = new MatrixTypeModel();
            List<MatrixTypeModel> lstMatxModel = new List<MatrixTypeModel>();
            List<DDLMatrixTypeModel> lstDDLModel = new List<DDLMatrixTypeModel>();
            model.lstMatrixModel = lstMatxModel;
            model.lstDDlMatrixModel = lstDDLModel;
            List<QuotaListModel> QuotaLst = new List<QuotaListModel>();
            var QuestionTypeList = new List<SelectListItem>();
            var GroupTypeList = new List<SelectListItem>();
            List<CreateQuestionModel> QuestionLst = new List<CreateQuestionModel>();
            try
            {

                if (QuestionId > 0)
                {
                    questionBase.Id = QuestionId;
                    actionResult = adminAction.Question_LoadById(questionBase);
                    if (actionResult.IsSuccess)
                    {
                        DataRow drFrst = actionResult.dsResult.Tables[0].Rows[0];
                        model.QuestionTypeId = drFrst["QuestionTypeId"] != DBNull.Value ? Convert.ToInt32(drFrst["QuestionTypeId"]) : 0;
                        foreach (DataRow drScnd in actionResult.dsResult.Tables[1].Rows)
                        {
                            lstMatxModel.Add(new MatrixTypeModel
                            {
                                Id = drScnd["Id"] != DBNull.Value ? Convert.ToInt32(drScnd["Id"]) : 0,
                                QuestionId = drScnd["QuestionId"] != DBNull.Value ? Convert.ToInt32(drScnd["QuestionId"]) : 0,
                                ColumnName = drScnd["ColumnName"] != DBNull.Value ? Convert.ToString(drScnd["ColumnName"]) : "",
                                GroupId = drScnd["GroupId"] != DBNull.Value ? Convert.ToInt32(drScnd["GroupId"]) : 0,
                                DisplayOrder = drScnd["MatrixDisplayOrder"] != DBNull.Value ? Convert.ToInt32(drScnd["MatrixDisplayOrder"]) : 0
                            });
                        }
                        if (model.QuestionTypeId == 14)
                        {
                            if (actionResult.dsResult.Tables[2] != null && actionResult.dsResult.Tables[2].Rows.Count > 0)
                            {
                                foreach (DataRow dr in actionResult.dsResult.Tables[2].Rows)
                                {
                                    lstDDLModel.Add(new DDLMatrixTypeModel
                                    {
                                        Id = dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0,
                                        QuestionId = dr["QuestionId"] != DBNull.Value ? Convert.ToInt32(dr["QuestionId"]) : 0,
                                        OptionName = dr["OptionName"] != DBNull.Value ? Convert.ToString(dr["OptionName"]) : "",
                                        DDLDisplayOrder = dr["DisplayOrder"] != DBNull.Value ? Convert.ToInt32(dr["DisplayOrder"]) : 0
                                    });
                                }
                                lstDDLModel = lstDDLModel.OrderBy(l => l.DDLDisplayOrder).ToList();
                            }
                        }
                    }
                    string matrixColumns = Newtonsoft.Json.JsonConvert.SerializeObject(lstMatxModel);
                    string matrixDropDowns = Newtonsoft.Json.JsonConvert.SerializeObject(lstDDLModel);
                    json = "{\"MatrixColumn\":" + matrixColumns + ",\"MatrixDropDown\":" + matrixDropDowns + "}";
                }
            }
            catch (Exception ex) { }
            return Json(json, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region GetSinglePunchQuestion [Question Type 1]
        public JsonResult GetSinglePunchQuestion(int QuestionId, string optArray)
        {
            System.IO.StringReader theReader = new System.IO.StringReader(optArray);
            DataSet dataSet = new DataSet();
            dataSet.ReadXml(theReader);
            DataTable dtOptions = new DataTable();
            dtOptions = dataSet.Tables[0];
            string json = string.Empty;
            string temp = string.Empty;

            foreach (DataRow dr in dtOptions.Rows)
            {
                string[] optionArray = { Convert.ToString(dr[0]) };
                string colNameFormat = "Q" + Convert.ToString(QuestionId);
                if (Session["ReportDataTable"] != null)
                {
                    DataTable dtReport = (DataTable)Session["ReportDataTable"];
                    string[] colNames = dtReport.Columns.Cast<DataColumn>().Where(x => x.ColumnName.Contains(colNameFormat)).Select(x => x.ColumnName).ToArray();

                    var query = (from row in dtReport.AsEnumerable()
                                 group row by row.Field<string>("Product_LogicalId") into grp
                                 select new
                                 {
                                     Arr = String.Join(",", grp.Where(v => optionArray.Contains(v.Field<string>(colNames[0]))).Select(row => row.Field<string>(colNames[0])).ToArray())
                                 }).ToList();

                    foreach (var item in query)
                    {
                        if (item.Arr != "")
                            temp += "{\"Sum\":\"" + (item.Arr).Split(',').Length + "\"},";
                        else
                            temp += "{\"Sum\":\"0\"},";
                    }
                    temp = "{\"Option\":\"" + Convert.ToString(dr[0]) + "\",\"List\":[" + temp.TrimEnd(',') + "]},";
                    json += temp;
                    temp = string.Empty;
                }
            }
            json = "[" + json.TrimEnd(',') + "]";
            return Json(json, JsonRequestBehavior.AllowGet);


            //string[] optionArray = optArray.TrimEnd(',').Split(',');
            //string colNameFormat = "Q" + Convert.ToString(QuestionId);
            //string json = string.Empty;
            //string temp = string.Empty;
            //if (Session["ReportDataTable"] != null)
            //{
            //    DataTable dtReport = (DataTable)Session["ReportDataTable"];
            //    string[] colNames = dtReport.Columns.Cast<DataColumn>().Where(x => x.ColumnName.Contains(colNameFormat)).Select(x => x.ColumnName).ToArray();
            //    var query = (from row in dtReport.AsEnumerable()
            //                 group row by row.Field<string>("Product_LogicalId") into grp
            //                 select new
            //                 {
            //                     Arr = String.Join(",", grp.Where(v => optionArray.Contains(v.Field<string>(colNames[0]))).Select(row => row.Field<string>(colNames[0])).ToArray())
            //                 }).ToList();

            //    foreach (var item in query)
            //    {
            //        if (item.Arr != "")
            //            temp += "{\"Sum\":\"" + (item.Arr).Split(',').Length + "\"},";
            //        else
            //            temp += "{\"Sum\":\"0\"},";
            //    }
            //    temp = "{\"Option\":\"Single Punch Horizontal\",\"List\":[" + temp.TrimEnd(',') + "]},";
            //    json += temp;
            //    temp = string.Empty;
            //    json = "[" + json.TrimEnd(',') + "]";

            //}
            //return Json(json, JsonRequestBehavior.AllowGet);
        }
        #endregion



        #region GetMultiPunchQuestion [Question Type 2]
        public JsonResult GetMultiPunchQuestion(int QuestionId, string optArray)
        {
            string colNameFormat = "Q" + Convert.ToString(QuestionId);
            string[] optionArray = optArray.TrimEnd(',').Split(',');
            string json = string.Empty;
            string temp = string.Empty;
            if (Session["ReportDataTable"] != null)
            {
                DataTable dtReport = (DataTable)Session["ReportDataTable"];
                string[] colNames = dtReport.Columns.Cast<DataColumn>().Where(x => x.ColumnName.Contains(colNameFormat)).Select(x => x.ColumnName).ToArray();
                List<string> colNamesToBeMatched = new List<string>();

                foreach (var colName in colNames)
                {
                    foreach (var optName in optionArray)
                    {
                        if ((colName.IndexOf(optName) != -1))
                        {
                            colNamesToBeMatched.Add(colName);
                        }
                    }
                }

                colNames = colNamesToBeMatched.ToArray();

                foreach (string columnName in colNames)
                {
                    var query = (from row in dtReport.AsEnumerable()
                                 group row by row.Field<string>("Product_LogicalId") into grp
                                 select new
                                 {
                                     Sum = grp.Sum((r) => decimal.Parse(r[columnName].ToString() == "1" ? r[columnName].ToString() : "0"))
                                 }).ToList();

                    foreach (var item in query)
                    {
                        temp += "{\"Sum\":\"" + item.Sum + "\"},";
                    }

                    temp = "{\"Option\":\"" + columnName.Split('-')[1] + "\",\"List\":[" + temp.TrimEnd(',') + "]},";
                    json += temp;
                    temp = string.Empty;
                }
                json = "[" + json.TrimEnd(',') + "]";
            }
            return Json(json, JsonRequestBehavior.AllowGet);
        }
        #endregion


        #region GetMultiPunchQuestionForGroupedOption [Question Type 2]

        public JsonResult GetMultiPunchQuestionForGroupedOption(int QuestionId, string optArray)
        {
            string colNameFormat = "Q" + Convert.ToString(QuestionId);
            string[] optionArray = optArray.TrimEnd(',').Split(',');
            string json = string.Empty;
            string temp = string.Empty;
            if (Session["ReportDataTable"] != null)
            {
                DataTable dtReport = (DataTable)Session["ReportDataTable"];
                string[] colNames = dtReport.Columns.Cast<DataColumn>().Where(x => x.ColumnName.Contains(colNameFormat)).Select(x => x.ColumnName).ToArray();
                List<string> colNamesToBeMatched = new List<string>();

                foreach (var colName in colNames)
                {
                    foreach (var optName in optionArray)
                    {
                        if ((colName.IndexOf(optName) != -1))
                        {
                            colNamesToBeMatched.Add(colName);
                        }
                    }
                }

                colNames = colNamesToBeMatched.ToArray();

                foreach (string columnName in colNames)
                {
                    var query = (from row in dtReport.AsEnumerable()
                                 group row by row.Field<string>("Product_LogicalId") into grp
                                 select new
                                 {
                                     Sum = grp.Sum((r) => decimal.Parse(r[columnName].ToString() == "1" ? r[columnName].ToString() : "0"))
                                 }).ToList();

                    foreach (var item in query)
                    {
                        temp += "{\"Sum\":\"" + item.Sum + "\"},";
                    }

                    temp = "{\"Option\":\"" + columnName.Split('-')[1] + "\",\"List\":[" + temp.TrimEnd(',') + "]},";
                    json += temp;
                    temp = string.Empty;
                }
                json = "[" + json.TrimEnd(',') + "]";
            }
            return Json(json, JsonRequestBehavior.AllowGet);
        }
        #endregion


        #region GetMeansOfRangeQuestion [Question Type 3]
        public JsonResult GetMeansOfRangeQuestion(int QuestionId)
        {
            string colName = "Q" + Convert.ToString(QuestionId);
            string json = string.Empty;
            if (Session["ReportDataTable"] != null)
            {
                DataTable dtReport = (DataTable)Session["ReportDataTable"];
                string[] colNames = dtReport.Columns.Cast<DataColumn>().Where(x => x.ColumnName.Contains(colName)).Select(x => x.ColumnName).ToArray();
                //var res = (from m in dtReport.AsEnumerable() where m.Field<string>("Code") == "1-1-12" select m).FirstOrDefault();

                var query = (from row in dtReport.AsEnumerable()
                             group row by row.Field<string>("Product_LogicalId") into grp
                             select new
                             {
                                 Sum = grp.Sum((r) => decimal.Parse(!string.IsNullOrEmpty(r[colNames[0]].ToString()) ? r[colNames[0]].ToString() : "0"))
                             }).ToList();
                json = Newtonsoft.Json.JsonConvert.SerializeObject(query);

            }
            return Json(json, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region GetMeansOfConstantSumQuestion [Question Type 4]
        public JsonResult GetMeansOfConstantSumQuestion(int QuestionId, string optArray)
        {
            string colNameFormat = "Q" + Convert.ToString(QuestionId);
            string[] optionArray = optArray.TrimEnd(',').Split(',');

            string json = string.Empty;
            string temp = string.Empty;
            if (Session["ReportDataTable"] != null)
            {
                DataTable dtReport = (DataTable)Session["ReportDataTable"];
                string[] colNames = dtReport.Columns.Cast<DataColumn>().Where(x => x.ColumnName.Contains(colNameFormat)).Select(x => x.ColumnName).ToArray();
                List<string> colNamesToBeMatched = new List<string>();

                foreach (var colName in colNames)
                {
                    foreach (var optName in optionArray)
                    {
                        if ((colName.IndexOf(optName) != -1))
                        {
                            colNamesToBeMatched.Add(colName);
                        }
                    }
                }

                colNames = colNamesToBeMatched.ToArray();

                foreach (var columnName in colNames)
                {
                    var query = (from row in dtReport.AsEnumerable()
                                 group row by row.Field<string>("Product_LogicalId") into grp
                                 select new
                                 {
                                     Sum = grp.Sum((r) => decimal.Parse(r[columnName].ToString()))
                                     //Sum = String.Join(",", grp.Where(v => colNames.Contains(v.Field<string>(colNames[0]))).Select(row => row.Field<string>(colNames[0])).ToArray())
                                 }).ToList();

                    foreach (var item in query)
                    {
                        temp += "{\"Sum\":\"" + item.Sum + "\"},";
                    }
                    temp = "{\"Option\":\"" + columnName + "\",\"List\":[" + temp.TrimEnd(',') + "]},";
                    json += temp;
                    temp = string.Empty;
                }
                json = "[" + json.TrimEnd(',') + "]";

            }
            return Json(json, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region GetMeansOfRankingQuestion [Question Type 5]
        public JsonResult GetMeansOfRankingQuestion(int QuestionId, string optionName, string macthingValue)
        {
            string colNameFormat = "Q" + Convert.ToString(QuestionId);
            string json = string.Empty;
            string temp = string.Empty;
            if (Session["ReportDataTable"] != null)
            {
                DataTable dtReport = (DataTable)Session["ReportDataTable"];
                string[] colNames = dtReport.Columns.Cast<DataColumn>().Where(x => x.ColumnName.Contains(colNameFormat)).Select(x => x.ColumnName).ToArray();

                foreach (string columnName in colNames)
                {
                    if (columnName.ToLower().IndexOf(optionName.ToLower()) != -1)
                    {
                        var query = (from row in dtReport.AsEnumerable()
                                     group row by row.Field<string>("Product_LogicalId") into grp
                                     select new
                                     {
                                         Sum = grp.Where((r) => r[columnName].ToString() == macthingValue).Sum((r) => decimal.Parse(r[columnName].ToString()))
                                     }).ToList();

                        foreach (var item in query)
                        {
                            temp += "{\"Sum\":\"" + item.Sum + "\"},";
                        }
                        temp = "{\"Option\":\"" + columnName.Split('-')[1] + "\",\"List\":[" + temp.TrimEnd(',') + "]},";
                        json += temp;
                        temp = string.Empty;
                    }
                }
                json = "[" + json.TrimEnd(',') + "]";

            }
            return Json(json, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region GetMatrixTypeQuestion [Question Type 6]
        public JsonResult GetMatrixTypeQuestion(int QuestionId, string optArray, string matchingValue)
        {
            string colNameFormat = "Q" + Convert.ToString(QuestionId);
            string[] optionArray = optArray.TrimEnd(',').Split(',');
            string json = string.Empty;
            string temp = string.Empty;
            if (Session["ReportDataTable"] != null)
            {
                DataTable dtReport = (DataTable)Session["ReportDataTable"];
                string[] colNames = dtReport.Columns.Cast<DataColumn>().Where(x => x.ColumnName.Contains(colNameFormat)).Select(x => x.ColumnName).ToArray();


                List<string> colNamesToBeMatched = new List<string>();

                foreach (var colName in colNames)
                {
                    foreach (var optName in optionArray)
                    {
                        if ((colName.IndexOf(optName) != -1))
                        {
                            colNamesToBeMatched.Add(colName);
                        }
                    }
                }

                colNames = colNamesToBeMatched.ToArray();

                foreach (string columnName in colNames)
                {
                    var query = (from row in dtReport.AsEnumerable()
                                 group row by row.Field<string>("Product_LogicalId") into grp
                                 select new
                                 {
                                     //Sum = grp.Sum((r) => decimal.Parse(r[columnName].ToString()))
                                     ans = String.Join(",", grp.Select(row => row.Field<string>(columnName)).ToArray()),
                                     Sum = grp.Sum((r) => decimal.Parse(r[columnName].ToString() == matchingValue ? "1" : "0"))
                                 }).ToList();

                    foreach (var item in query)
                    {
                        temp += "{\"Sum\":\"" + item.Sum + "\"},";
                    }
                    temp = "{\"Option\":\"" + columnName.Split('-')[1] + "\",\"List\":[" + temp.TrimEnd(',') + "]},";
                    json += temp;
                    temp = string.Empty;
                }
                json = "[" + json.TrimEnd(',') + "]";
            }
            return Json(json, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region GetSimpleTextQuestion [Question Type 7]
        public JsonResult GetSimpleTextQuestion(int QuestionId)
        {
            string colName = "Q" + Convert.ToString(QuestionId);
            string json = string.Empty;
            if (Session["ReportDataTable"] != null)
            {
                DataTable dtReport = (DataTable)Session["ReportDataTable"];
                string[] colNames = dtReport.Columns.Cast<DataColumn>().Where(x => x.ColumnName.Contains(colName)).Select(x => x.ColumnName).ToArray();
                var query = (from row in dtReport.AsEnumerable()
                             group row by row.Field<string>("Product_LogicalId") into grp
                             select new
                             {
                                 ans = String.Join(",", grp.Select(row => row.Field<string>(colNames[0])).ToArray()),
                             });
                json = Newtonsoft.Json.JsonConvert.SerializeObject(query);
            }
            return Json(json, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region GetSinglePunchMatrixTypeQuestion [Question Type 8]
        public JsonResult GetSinglePunchMatrixTypeQuestion(int QuestionId, string optArray, string matchingValue)
        {
            System.IO.StringReader theReader = new System.IO.StringReader(optArray);
            DataSet dataSet = new DataSet();
            dataSet.ReadXml(theReader);
            DataTable dtOptions = new DataTable();
            dtOptions = dataSet.Tables[0];
            string json = string.Empty;
            string temp = string.Empty;
            foreach (DataRow dr in dtOptions.Rows)
            {
                string colNameFormat = "Q" + Convert.ToString(QuestionId);
                string[] optArr = Convert.ToString(dr[0]).Split(',');
                string[] optionArray = { optArr[0] != null ? Convert.ToString(optArr[0]) : "" };//OptArray.TrimEnd(',').Split(',');
                int res = 0;

                if (Session["ReportDataTable"] != null)
                {
                    string optionName = optArr[0].Split('-')[1];
                    DataTable dtReport = (DataTable)Session["ReportDataTable"];
                    string[] colNames = dtReport.Columns.Cast<DataColumn>().Where(x => x.ColumnName.Contains(colNameFormat) && x.ColumnName.Contains(optionName)).Select(x => x.ColumnName).ToArray();


                    //string[] colNames = dtReport.Columns.Cast<DataColumn>().Where(x => x.ColumnName.Contains(colNameFormat)).Select(x => x.ColumnName).ToArray();
                    List<string> colNamesToBeMatched = new List<string>();

                    //foreach (var colName in colNames)
                    //{
                    //    foreach (var optName in optionArray)
                    //    {
                    //        if ((colName.IndexOf(optName) != -1))
                    //        {
                    //            colNamesToBeMatched.Add(colName);
                    //        }
                    //    }
                    //}
                    //colNames = colNamesToBeMatched.ToArray();
                    foreach (string columnName in colNames)
                    {
                        var query = (from row in dtReport.AsEnumerable()
                                     group row by row.Field<string>("Product_LogicalId") into grp
                                     select new
                                     {
                                         //Sum = grp.Sum((r) => decimal.Parse(r[columnName].ToString()))
                                         ans = String.Join(",", grp.Select(row => row.Field<string>(columnName)).ToArray()),
                                         Sum = grp.AsEnumerable().Count(row => row.Field<string>(columnName) == Convert.ToString(optArr[1]))
                                         //Sum = grp.Sum((r) => decimal.Parse(r[columnName].ToString() == matchingValue ? "1" : "0")),
                                     }).ToList();

                        foreach (var item in query)
                        {
                            temp += "{\"Sum\":\"" + item.Sum + "\"},";
                        }
                        temp = "{\"Option\":\"" + Convert.ToString(optArr[0]) + "\",\"List\":[" + temp.TrimEnd(',') + "]},";
                        json += temp;
                        temp = string.Empty;
                    }

                }
            }
            json = "[" + json.TrimEnd(',') + "]";

            return Json(json, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region GetMeansOfRangeMatrixTypeQuestion [Question Type 9]
        public JsonResult GetMeansOfRangeMatrixTypeQuestion(int QuestionId, string optArray)
        {
            string colNameFormat = "Q" + Convert.ToString(QuestionId);
            string[] optionArray = optArray.TrimEnd(',').Split(',');

            string json = string.Empty;
            string temp = string.Empty;
            if (Session["ReportDataTable"] != null)
            {
                DataTable dtReport = (DataTable)Session["ReportDataTable"];
                string[] colNames = dtReport.Columns.Cast<DataColumn>().Where(x => x.ColumnName.Contains(colNameFormat)).Select(x => x.ColumnName).ToArray();
                List<string> colNamesToBeMatched = new List<string>();

                foreach (var colName in colNames)
                    foreach (var optName in optionArray)
                        if ((colName.IndexOf(optName) != -1))
                            colNamesToBeMatched.Add(colName);

                colNames = colNamesToBeMatched.ToArray();
                string fieldNames = String.Join(",", colNames);

                foreach (var columnName in colNames)
                {
                    var query = (from row in dtReport.AsEnumerable()
                                 group row by row.Field<string>("Product_LogicalId") into grp
                                 select new
                                 {
                                     Sum = grp.Sum((r) => decimal.Parse(r[columnName].ToString()))
                                 }).ToList();

                    foreach (var item in query)
                    {
                        temp += "{\"Sum\":\"" + item.Sum + "\"},";
                    }
                    temp = "{\"Option\":\"" + columnName + "\",\"List\":[" + temp.TrimEnd(',') + "]},";
                    json += temp;
                    temp = string.Empty;
                }
                json = "[" + json.TrimEnd(',') + "]";

            }
            return Json(json, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region GetRankOrderTypeQuestion [Question Type 10 || 11 || 12]
        public JsonResult GetRankOrderTypeQuestion(int QuestionId, string optArray)
        {
            string colNameFormat = "Q" + Convert.ToString(QuestionId);
            string[] optionArray = optArray.TrimEnd(',').Split(',');
            string json = string.Empty;
            string temp = string.Empty;
            if (Session["ReportDataTable"] != null)
            {

                DataTable dtReport = (DataTable)Session["ReportDataTable"];
                string[] colNames = dtReport.Columns.Cast<DataColumn>().Where(x => x.ColumnName.Contains(colNameFormat)).Select(x => x.ColumnName).ToArray();
                List<string> colNamesToBeMatched = new List<string>();
                foreach (var colName in colNames)
                {
                    foreach (var optName in optionArray)
                    {
                        if ((colName.IndexOf(optName.Trim()) != -1))
                        {
                            colNamesToBeMatched.Add(colName);
                        }
                    }
                }
                colNames = colNamesToBeMatched.ToArray();
                foreach (string columnName in colNames)
                {
                    var query = (from row in dtReport.AsEnumerable()
                                 group row by row.Field<string>("Product_LogicalId") into grp
                                 select new
                                 {
                                     //Sum = grp.Sum((r) => decimal.Parse(r[columnName].ToString()))
                                     ans = String.Join(",", grp.Select(row => row.Field<string>(columnName)).ToArray()),
                                     Sum = grp.Sum((r) => decimal.Parse(r[columnName].ToString() == "1" ? r[columnName].ToString() : "0"))
                                 }).ToList();
                    foreach (var item in query)
                    {
                        temp += "{\"Sum\":\"" + item.Sum + "\"},";
                    }
                    temp = "{\"Option\":\"" + columnName.Split('-')[1] + "\",\"List\":[" + temp.TrimEnd(',') + "]},";
                    json += temp;
                    temp = string.Empty;
                }
                json = "[" + json.TrimEnd(',') + "]";
            }
            return Json(json, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region GetMeansOfKanoTypeQuestion [Question Type 13] Not in use
        //public JsonResult GetMeansOfKanoTypeQuestion(int QuestionId, string optArray)
        //{

        //    string colNameFormat = "Q" + Convert.ToString(QuestionId);
        //    string[] optionArray = optArray.TrimEnd(',').Split(',');
        //    string json = string.Empty;
        //    string temp = string.Empty;
        //    if (Session["ReportDataTable"] != null)
        //    {
        //        DataTable dtReport = (DataTable)Session["ReportDataTable"];
        //        string[] colNames = dtReport.Columns.Cast<DataColumn>().Where(x => x.ColumnName.Contains(colNameFormat)).Select(x => x.ColumnName).ToArray();
        //        List<string> colNamesToBeMatched = new List<string>();
        //        foreach (var colName in colNames)
        //        {
        //            foreach (var optName in optionArray)
        //            {
        //                if ((colName.IndexOf(optName.Trim()) != -1))
        //                {
        //                    colNamesToBeMatched.Add(colName);
        //                }
        //            }
        //        }
        //        colNames = colNamesToBeMatched.ToArray();

        //        var query = (from row in dtReport.AsEnumerable()
        //                     group row by row.Field<string>("Product_LogicalId") into grp
        //                     select new
        //                     {
        //                         IfPresentAnswers = String.Join(",", grp.Select(row => row.Field<string>(colNames[0])).ToArray()),
        //                         IfAbsentAnswers = String.Join(",", grp.Select(row => row.Field<string>(colNames[1])).ToArray()),
        //                         Sum = grp.Sum((r) => decimal.Parse(r[colNames[0]].ToString()))
        //                     }).ToList();


        //        foreach (var item in query)
        //        {
        //            string[] presentAnswers = item.IfPresentAnswers.Split(',');
        //            string[] absentAnswers = item.IfAbsentAnswers.Split(',');
        //            string combinedPN = string.Empty;

        //            for (int i = 0; i <= presentAnswers.Length - 1; i++)
        //            {
        //                combinedPN += "{\"PN\":\"" + presentAnswers[i] + absentAnswers[i] + "\"},";
        //            }

        //            temp += "{\"CombinedPN\":[" + combinedPN + "]},"; // CombinedPN = Combined (Concatenated) result of positive resluts of positive and negative columns
        //            temp = "{\"Option\":\"" + colNames[0].Split('-')[1] + "\",\"List\":[" + combinedPN.TrimEnd(',') + "]},";
        //            json += temp;
        //            temp = string.Empty;
        //        }




        //        //foreach (string columnName in colNames)
        //        //{
        //        //    var query = (from row in dtReport.AsEnumerable()
        //        //                 group row by row.Field<string>("Product_LogicalId") into grp
        //        //                 select new
        //        //                 {
        //        //                     ans = String.Join(",", grp.Select(row => row.Field<string>(columnName)).ToArray()),
        //        //                     Sum = grp.Sum((r) => decimal.Parse(r[columnName].ToString()))
        //        //                 }).ToList();
        //        //    foreach (var item in query)
        //        //    {
        //        //        temp += "{\"Sum\":\"" + item.Sum + "\"},";
        //        //    }
        //        //    temp = "{\"Option\":\"" + columnName.Split('-')[1] + "\",\"List\":[" + temp.TrimEnd(',') + "]},";
        //        //    json += temp;
        //        //    temp = string.Empty;
        //        //}
        //        json = "[" + json.TrimEnd(',') + "]";
        //    }
        //    return Json(json, JsonRequestBehavior.AllowGet);
        //}
        #endregion

        #region GetMeansOfKanoTypeQuestion [Question Type 13]
        [ValidateInput(false)]
        [HttpPost]
        public JsonResult GetMeansOfKanoTypeQuestion(int QuestionId, string OptArray, string responderId, int QuesColsCount)
        {

            System.IO.StringReader theReader = new System.IO.StringReader(OptArray);
            DataSet dataSet = new DataSet();
            dataSet.ReadXml(theReader);
            DataTable dtOptions = new DataTable();
            dtOptions = dataSet.Tables[0];

            string json = string.Empty;
            string temp = string.Empty;

            foreach (DataRow dr in dtOptions.Rows)
            {
                string colNameFormat = "Q" + Convert.ToString(QuestionId);
                string[] optionArray = { Convert.ToString(dr[0]) };//OptArray.TrimEnd(',').Split(',');
                //string json = string.Empty;
                //string temp = string.Empty;
                if (Session["ReportDataTable"] != null)
                {
                    DataTable dtReport = (DataTable)Session["ReportDataTable"];
                    string[] colNames = dtReport.Columns.Cast<DataColumn>().Where(x => x.ColumnName.Contains(colNameFormat)).Select(x => x.ColumnName).ToArray();
                    List<string> colNamesToBeMatched = new List<string>();
                    foreach (var colName in colNames)
                    {
                        foreach (var optName in optionArray)
                        {
                            if ((colName.IndexOf(optName.Trim()) != -1))
                            {
                                colNamesToBeMatched.Add(colName);
                            }
                        }
                    }
                    colNames = colNamesToBeMatched.ToArray();

                    var query = (from row in dtReport.AsEnumerable()
                                 select new
                                 {
                                     IfPresentAnswers = row.Field<string>(colNames[0]),
                                     IfAbsentAnswers = row.Field<string>(colNames[1]),
                                     RespId = row.Field<string>("Resp_Id")
                                 }).Where(l => l.RespId == responderId.ToString()).ToList();


                    foreach (var item in query)
                    {
                        //string[] presentAnswers = item.IfPresentAnswers.Split(',');
                        // string[] absentAnswers = item.IfAbsentAnswers.Split(',');
                        string combinedPN = string.Empty;
                        int negAns = Convert.ToInt32(item.IfAbsentAnswers) % QuesColsCount;
                        combinedPN += "{\"PN\":\"" + item.IfPresentAnswers + (negAns == 0 ? QuesColsCount : negAns) + "\"},";
                        temp += "{\"CombinedPN\":[" + combinedPN + "]},"; // CombinedPN = Combined (Concatenated) result of positive resluts of positive and negative columns
                        temp = "{\"Option\":\"" + colNames[0].Split('-')[1] + "\",\"List\":[" + combinedPN.TrimEnd(',') + "]},";
                        json += temp;
                        temp = string.Empty;
                    }
                    if (indexCount > dtReport.Rows.Count)
                    {
                        indexCount++;
                    }
                }
            }
            json = "[" + json.TrimEnd(',') + "]";

            return Json(json, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region GetMeansOfDropdownMatrixTypeQuestion [Question Type 14]
        public JsonResult GetMeansOfDropdownMatrixTypeQuestion(int QuestionId, string optArray)
        {

            string colNameFormat = "Q" + Convert.ToString(QuestionId);
            string[] optionArray = optArray.TrimEnd(',').Split(',');
            string json = string.Empty;
            string temp = string.Empty;
            if (Session["ReportDataTable"] != null)
            {
                DataTable dtReport = (DataTable)Session["ReportDataTable"];
                string[] colNames = dtReport.Columns.Cast<DataColumn>().Where(x => x.ColumnName.Contains(colNameFormat)).Select(x => x.ColumnName).ToArray();
                List<string> colNamesToBeMatched = new List<string>();
                foreach (var colName in colNames)
                {
                    foreach (var optName in optionArray)
                    {
                        if ((colName.IndexOf(optName.Trim()) != -1))
                        {
                            colNamesToBeMatched.Add(colName);
                        }
                    }
                }
                colNames = colNamesToBeMatched.ToArray();
                foreach (string columnName in colNames)
                {
                    var query = (from row in dtReport.AsEnumerable()
                                 group row by row.Field<string>("Product_LogicalId") into grp
                                 select new
                                 {
                                     //Sum = grp.Sum((r) => decimal.Parse(r[columnName].ToString()))
                                     ans = String.Join(",", grp.Select(row => row.Field<string>(columnName)).ToArray()),
                                     Sum = grp.Sum((r) => decimal.Parse(r[columnName].ToString()))
                                 }).ToList();
                    foreach (var item in query)
                    {
                        temp += "{\"Sum\":\"" + item.Sum + "\"},";
                    }
                    temp = "{\"Option\":\"" + columnName.Split('-')[1] + "\",\"List\":[" + temp.TrimEnd(',') + "]},";
                    json += temp;
                    temp = string.Empty;
                }
                json = "[" + json.TrimEnd(',') + "]";
            }
            return Json(json, JsonRequestBehavior.AllowGet);




            //    string colNameFormat = "Q" + Convert.ToString(QuestionId);
            //    string[] optionArray = optArray.TrimEnd(',').Split(',');
            //    string json = string.Empty;
            //    string temp = string.Empty;
            //    if (Session["ReportDataTable"] != null)
            //    {
            //        DataTable dtReport = (DataTable)Session["ReportDataTable"];
            //        string[] colNames = dtReport.Columns.Cast<DataColumn>().Where(x => x.ColumnName.Contains(colNameFormat)).Select(x => x.ColumnName).ToArray();
            //        List<string> colNamesToBeMatched = new List<string>();
            //        foreach (var colName in colNames)
            //        {
            //            foreach (var optName in optionArray)
            //            {
            //                if ((colName.IndexOf(optName) != -1))
            //                {
            //                    colNamesToBeMatched.Add(colName);
            //                }
            //            }
            //        }
            //        colNames = colNamesToBeMatched.ToArray();
            //        foreach (string columnName in colNames)
            //        {
            //            var query = (from row in dtReport.AsEnumerable()
            //                         group row by row.Field<string>("Product_LogicalId") into grp
            //                         select new
            //                         {
            //                             //Sum = grp.Sum((r) => decimal.Parse(r[columnName].ToString()))
            //                             ans = String.Join(",", grp.Select(row => row.Field<string>(columnName)).ToArray()),
            //                             Sum = grp.Sum((r) => decimal.Parse(r[columnName].ToString() == matchingValue ? "1" : "0"))
            //                         }).ToList();
            //            foreach (var item in query)
            //            {
            //                temp += "{\"Sum\":\"" + item.Sum + "\"},";
            //            }
            //            temp = "{\"Option\":\"" + columnName.Split('-')[1] + "\",\"List\":[" + temp.TrimEnd(',') + "]},";
            //            json += temp;
            //            temp = string.Empty;
            //        }
            //        json = "[" + json.TrimEnd(',') + "]";
            //    }
            //    return Json(json, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region _PartialReportMaker Get
        [HttpGet]
        public ActionResult _PartialReportMaker(string surveyGUID)
        {
            List<QuestionModel> QuestionLst = new List<QuestionModel>();
            RespondentController resp = new RespondentController();
            try
            {
                SurveyResponseModel model = new SurveyResponseModel();
                model = resp.GetProjectPreview(surveyGUID);
                ViewBag.ProjectType = model.projectModel.ProjectType;
                QuestionLst = model.lstQuestionModel;
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return View(QuestionLst);
        }
        #endregion

        #region _PartialQueryReportMaker Get
        [HttpGet]
        public ActionResult _PartialQueryReportMaker(string surveyGUID)
        {
            List<QuestionModel> QuestionLst = new List<QuestionModel>();
            RespondentController resp = new RespondentController();
            SurveyResponseModel model = new SurveyResponseModel();

            Query_Model queryModel = new Query_Model();

            List<Query_Model> QueryList = new List<Query_Model>();

            try
            {

                model = resp.GetProjectPreview(surveyGUID);
                int ProjectId = 0;
                if (model.projectModel != null)
                {
                    ProjectId = Convert.ToInt32(model.projectModel.Id);
                }
                queryBase.ProjectId = ProjectId;

                CreateProjectModel projectModel = new CreateProjectModel();
                projectModel = GetProjectDetailById(ProjectId);
                List<int> currentQueryList = new List<int>();
                int currentQuery = 0;
                int currentQuery1 = 0;
                int check = 0;




                ViewBag.ProjectType = model.projectModel.ProjectType;
                QuestionLst = model.lstQuestionModel;

                model.lstQuestionModel = QuestionLst;

                actionResult = adminAction.Query_LoadAllByProjectId(queryBase);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    foreach (DataRow dr in actionResult.dtResult.Rows)
                    {
                        queryModel = new Query_Model();
                        queryModel.ProjectId = dr["ProjectId"] != DBNull.Value ? Convert.ToInt32(dr["ProjectId"]) : 0;
                        queryModel.CaseName = dr["CaseName"] != DBNull.Value ? Convert.ToString(dr["CaseName"]) : "";
                        queryModel.Query = dr["Query"] != DBNull.Value ? Convert.ToString(dr["Query"]) : "";
                        QueryList.Add(queryModel);
                        currentQuery = dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0;
                        if (currentQuery > 0)
                        {
                            currentQueryList.Add(currentQuery);
                        }
                    }
                }

                if (projectModel.SuperProjectId > 0)
                {
                    queryBase.SuperProjectId = projectModel.SuperProjectId;
                    //(projectModel.ProjectType == 2)
                    actionResult = adminAction.Query_LoadAllBySuperProjectId(queryBase);
                    if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                    {
                        check = 0;
                        foreach (DataRow dr in actionResult.dtResult.Rows)
                        {
                            queryModel = new Query_Model();
                            queryModel.ProjectId = dr["ProjectId"] != DBNull.Value ? Convert.ToInt32(dr["ProjectId"]) : 0;
                            queryModel.CaseName = dr["CaseName"] != DBNull.Value ? Convert.ToString(dr["CaseName"]) : "";
                            queryModel.Query = dr["Query"] != DBNull.Value ? Convert.ToString(dr["Query"]) : "";
                            currentQuery1 = dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0;
                            foreach (var item in currentQueryList)
                            {
                                if (item == currentQuery1)
                                {
                                    check = 1;
                                }
                            }
                            if (check == 0)
                            {
                                QueryList.Add(queryModel);
                            }
                        }
                    }
                }

                model.QueryList = QueryList;
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return View(model);
        }
        #endregion

        #region  _PartialKanoGraphicalReport Get
        public ActionResult _PartialKanoGraphicalReport()
        {
            return View();
        }
        #endregion

        #region  _PartialBasicReportMaker Get
        public ActionResult _PartialBasicReportMaker()
        {
            string ReportDataHtml = "<table id='hdnRDTable'>";
            if (Session["ReportDataTable"] != null)
            {
                DataTable dtReport = (DataTable)Session["ReportDataTable"];
                if (dtReport != null)
                {
                    string[] cols = (from dc in dtReport.Columns.Cast<DataColumn>()
                                     select dc.ColumnName)
                                   .ToArray();
                    if (cols != null)
                    {
                        ReportDataHtml += "<tr class='thRow'>";
                        foreach (var item in cols)
                        {
                            ReportDataHtml += "<th>" + item + "</th>";
                        }
                        ReportDataHtml += "</tr>";
                    }

                    foreach (DataRow row in dtReport.Rows)
                    {
                        ReportDataHtml += "<tr class='tdRow'>";
                        foreach (var item in row.ItemArray)
                        {
                            ReportDataHtml += "<td>" + item + "</td>";
                        }
                        ReportDataHtml += "</tr>";
                    }
                }
            }
            ReportDataHtml += "</table>";
            ViewBag.RDT = ReportDataHtml;
            return View();
        }
        #endregion

        #region  _PartialBasicQueryReportMaker Get
        public ActionResult _PartialBasicQueryReportMaker()
        {
            return View();
        }
        #endregion


        #region GetReportDataTable1
        private DataTable GetReportDataTable1(string Id, string[] Emails, string[] ExternalResponders)
        {

            RespondentController respondent = new RespondentController();
            SurveyResponseModel model = new SurveyResponseModel();
            DataTable dtF = new DataTable();
            try
            {
                DataTable dt = new DataTable();

                string projectGuid = string.Empty;
                projectGuid = Id;
                dt.Columns.Add("Resp_Id", typeof(string));

                DataRow drnewRow = dt.NewRow();
                drnewRow[0] = "Import_Resp";
                dt.Rows.Add(drnewRow);

                model = respondent.GetProjectPreview(projectGuid);

                //--ss
                if (model.projectModel.ProjectType == "2")
                {
                    DataRow drnew = dt.NewRow();
                    drnew[0] = "Product_LogicalId";
                    dt.Rows.Add(drnew);
                    DataRow drnew1 = dt.NewRow();
                    drnew1[0] = "Product_Name";
                    dt.Rows.Add(drnew1);
                }
                //--

                // Load all the questions of this project
                List<QuestionList> questionsList = new List<QuestionList>();
                questionBase.ProjectId = Convert.ToInt32(model.projectModel.Id);
                actionResult = adminAction.QuestionsLoadAll_ByProjectId(questionBase);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    foreach (DataRow dr in actionResult.dtResult.Rows)
                    {
                        questionsList.Add(new QuestionList
                        {
                            id = dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0,
                            questionTypeId = dr["QuestionTypeId"] != DBNull.Value ? Convert.ToInt32(dr["QuestionTypeId"]) : 0
                        });
                    }
                }

                string fileName = Guid.NewGuid().ToString().Substring(0, 5) + "_" + DateTime.Now.ToString("MM-dd-yyyy") + "_" + model.projectModel.ProjectName.Replace(" ", "");

                if (model.lstQuestionModel != null && model.lstQuestionModel.Count > 0)
                {
                    foreach (var item in model.lstQuestionModel)
                    {
                        if (item.PipedFrom > 0 && (!item.QuestionTitle.Contains("{{Q}}")))
                        {
                            if (item.QuestionTypeId == 2 || item.QuestionTypeId == 4 || item.QuestionTypeId == 5 || item.QuestionTypeId == 6)//Multi Punch || Constant Sum || Ranking || Matrix Type
                            {
                                if (model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel != null && model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel.Count > 0)
                                {
                                    foreach (var opt in model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel)
                                    {
                                        DataRow drnew = dt.NewRow();
                                        drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                        dt.Rows.Add(drnew);
                                    }
                                }
                            }

                            else if (item.QuestionTypeId == 1 || item.QuestionTypeId == 3 || item.QuestionTypeId == 7 || item.QuestionTypeId == 15)//Single Punch || Range || Simple Text
                            {
                                DataRow drnew = dt.NewRow();
                                drnew[0] = "Q" + item.QuestionId + "_S" + "-" + ReplaceHtml.ReplaceText(item.QuestionTitle);
                                dt.Rows.Add(drnew);
                            }
                            else if (item.QuestionTypeId == 8 || item.QuestionTypeId == 12)//Single punch matrix type  && Rank order type
                            {
                                //if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
                                //{
                                //foreach (var opt in item.lstOptionsModel)
                                //{
                                //    DataRow drnew = dt.NewRow();
                                //    //drnew[0] = "Q" + item.QuestionId + "_M" + opt.OptionId + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                //    drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                //    dt.Rows.Add(drnew);
                                //}
                                //}
                                if (item.lstMatrixTypeModel != null && item.lstMatrixTypeModel.Count > 0)
                                {
                                    foreach (var col in item.lstMatrixTypeModel)
                                    {
                                        DataRow drnew = dt.NewRow();
                                        //drnew[0] = "Q" + item.QuestionId + "_M" + opt.OptionId + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                        drnew[0] = "Q" + item.QuestionId + "_M" + col.DisplayOrder + "-" + ReplaceHtml.ReplaceText(col.ColumnName);
                                        dt.Rows.Add(drnew);
                                    }
                                }
                            }
                            else if (item.QuestionTypeId == 13)//Kano model matrix type 
                            {
                                if (model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel != null && model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel.Count > 0)
                                {
                                    foreach (var opt in model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel)
                                    {
                                        DataRow drnew = dt.NewRow();
                                        drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Present";
                                        dt.Rows.Add(drnew);
                                        DataRow drnew1 = dt.NewRow();
                                        drnew1[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Absent";
                                        dt.Rows.Add(drnew1);
                                    }
                                }
                            }
                            else if (item.QuestionTypeId == 16) // Description type Question
                            {
                            }
                            // Single Punch Matrix Type || Range Matrix Type Question ||
                            // Single punch type answer horizontal position || 
                            // Multiple punch type answer horizontal position || 
                            // Rank order type question || 
                            // KANO Model Matrix type question || Dropdown Matrix type question
                            else if (item.QuestionTypeId > 7 && item.QuestionTypeId != 15)
                            {
                                if (model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel != null && model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel.Count > 0)
                                {
                                    foreach (var opt in model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel)
                                    {
                                        foreach (var column in item.lstMatrixTypeModel)
                                        {
                                            DataRow drnew = dt.NewRow();
                                            drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "_C" + column.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + " " + ReplaceHtml.ReplaceText(column.ColumnName);
                                            dt.Rows.Add(drnew);
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (item.QuestionTypeId == 2 || item.QuestionTypeId == 4 || item.QuestionTypeId == 5 || item.QuestionTypeId == 6)//Multi Punch || Constant Sum || Ranking || Matrix Type
                            {
                                if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
                                {
                                    foreach (var opt in item.lstOptionsModel)
                                    {
                                        DataRow drnew = dt.NewRow();
                                        //drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                        drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                        dt.Rows.Add(drnew);
                                    }
                                }
                            }

                            else if (item.QuestionTypeId == 1 || item.QuestionTypeId == 3 || item.QuestionTypeId == 7 || item.QuestionTypeId == 15)//Single Punch || Range || Simple Text
                            {
                                DataRow drnew = dt.NewRow();
                                //drnew[0] = "Q" + item.DisplayOrder + "_S" + "-" + ReplaceHtml.ReplaceText(item.QuestionTitle);
                                drnew[0] = "Q" + item.QuestionId + "_S" + "-" + ReplaceHtml.ReplaceText(item.QuestionTitle);
                                dt.Rows.Add(drnew);
                            }
                            else if (item.QuestionTypeId == 8 || item.QuestionTypeId == 12)//Single punch matrix type  && Rank order type
                            {
                                //if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
                                //{
                                //foreach (var opt in item.lstOptionsModel)
                                //{
                                //    DataRow drnew = dt.NewRow();
                                //    //drnew[0] = "Q" + item.QuestionId + "_M" + opt.OptionId + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                //    drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                //    dt.Rows.Add(drnew);
                                //}
                                //}
                                if (item.lstMatrixTypeModel != null && item.lstMatrixTypeModel.Count > 0)
                                {
                                    foreach (var col in item.lstMatrixTypeModel)
                                    {
                                        DataRow drnew = dt.NewRow();
                                        //drnew[0] = "Q" + item.QuestionId + "_M" + opt.OptionId + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                        drnew[0] = "Q" + item.QuestionId + "_M" + col.DisplayOrder + "-" + ReplaceHtml.ReplaceText(col.ColumnName);
                                        dt.Rows.Add(drnew);
                                    }
                                }
                            }
                            else if (item.QuestionTypeId == 13)//Kano model matrix type 
                            {
                                if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
                                {
                                    foreach (var opt in item.lstOptionsModel)
                                    {
                                        DataRow drnew = dt.NewRow();
                                        //drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Present";
                                        drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Present";
                                        dt.Rows.Add(drnew);
                                        DataRow drnew1 = dt.NewRow();
                                        //drnew1[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Absent";
                                        drnew1[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Absent";
                                        dt.Rows.Add(drnew1);
                                    }
                                }
                            }
                            // Single Punch Matrix Type || Range Matrix Type Question ||
                            // Single punch type answer horizontal position || 
                            // Multiple punch type answer horizontal position || 
                            // Rank order type question || 
                            // KANO Model Matrix type question || Dropdown Matrix type question
                            else if (item.QuestionTypeId > 7)
                            {
                                if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
                                {
                                    foreach (var opt in item.lstOptionsModel)
                                    {
                                        foreach (var column in item.lstMatrixTypeModel)
                                        {
                                            DataRow drnew = dt.NewRow();
                                            //drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "_C" + column.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + " " + ReplaceHtml.ReplaceText(column.ColumnName);
                                            drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "_C" + column.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + " " + ReplaceHtml.ReplaceText(column.ColumnName);
                                            dt.Rows.Add(drnew);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                // Get the Excel Pattern of dt
                dtF = comMethods.GenerateTransposedTable(dt);

                // Get all the reponders
                projectBase.Id = model.projectModel.Id;
                actionResult = adminAction.CompleteRespondersLoad_ByProjectId(projectBase);
                List<RespondersModel> lstResponders = new List<RespondersModel>();
                foreach (DataRow dr in actionResult.dtResult.Rows)
                {
                    lstResponders.Add(new RespondersModel
                    {

                        InvitationGuid = dr["InvitationGuid"] != DBNull.Value ? Convert.ToString(dr["InvitationGuid"]) : "",
                        Email = dr["Email"] != DBNull.Value ? Convert.ToString(dr["Email"]) : "",

                    });
                }
                IEnumerable<RespondersModel> filteredList = lstResponders
                 .GroupBy(customer => customer.InvitationGuid).Select(group => group.First());
                // Get Responses
                SurveyLoadBase surveyLoadBase = new SurveyLoadBase();
                string json = string.Empty;
                SRV.BaseLayer.ActionResult actionResultResponses = new SRV.BaseLayer.ActionResult();
                int response_id = 1;            //ss
                foreach (var responder in filteredList)
                {
                    DataTable responsesDt = new DataTable();
                    surveyLoadBase.Id = model.projectModel.Id;
                    surveyLoadBase.Email = (!string.IsNullOrEmpty(responder.Email) ? Convert.ToString(responder.Email) : "");
                    surveyLoadBase.ExternalResponderId = (!string.IsNullOrEmpty(responder.InvitationGuid) ? Convert.ToString(responder.InvitationGuid) : "");

                    if (Array.IndexOf(Emails, surveyLoadBase.Email) > -1 || Array.IndexOf(ExternalResponders, surveyLoadBase.ExternalResponderId) > -1)
                    {
                        if (model.projectModel.lstProductProjectModel != null &&
                            model.projectModel.lstProductProjectModel.Count > 0)
                        {
                            foreach (var productModel in model.projectModel.lstProductProjectModel)
                            {
                                surveyLoadBase.ProductId = productModel.Id;
                                actionResultResponses = respondentAction.Response_LoadByProjectId(surveyLoadBase);
                                if (actionResultResponses.IsSuccess)
                                {
                                    if (responsesDt.Rows.Count == 0)
                                        responsesDt = actionResultResponses.dtResult.Copy();
                                    else
                                    {
                                        foreach (DataRow drThisRespRow in actionResultResponses.dtResult.Rows)
                                            responsesDt.ImportRow(drThisRespRow);
                                    }
                                }
                            }
                        }
                        else
                        {
                            actionResultResponses = respondentAction.Response_LoadByProjectId(surveyLoadBase);
                            if (actionResultResponses.IsSuccess)
                                responsesDt = actionResultResponses.dtResult.Copy();
                        }
                    }
                    // surveyLoadBase.OfflineResponderId = dr["RespId"] != DBNull.Value ? Convert.ToString(dr["RespId"]) : "";
                    //DataRow dr_dtF = dtF.NewRow();
                    // if (String.IsNullOrEmpty(surveyLoadBase.Email)) continue;

                    //actionResultResponses = respondentAction.Response_LoadByProjectId(surveyLoadBase);
                    if (responsesDt.Rows.Count > 0)//actionResultResponses.IsSuccess
                    {

                        int count = 1;
                        count = (model.projectModel.ProjectType == "2") ? count + 2 : count;        //ss
                        DataRow dr_dtF = dtF.NewRow();
                        DataRow drTemp = dtF.NewRow();
                        string product_desc = string.Empty;
                        string product_logiId = string.Empty;
                        int oldProductID = responsesDt.Rows[0]["ProductId"] != DBNull.Value ? Convert.ToInt32(responsesDt.Rows[0]["ProductId"]) : 0;

                        foreach (DataRow drResponse in responsesDt.Rows)// actionResultResponses.dtResult.Rows
                        {

                            int optionsCount = 0;

                            int productId = drResponse["ProductId"] != DBNull.Value ? Convert.ToInt32(drResponse["ProductId"]) : 0;
                            if (productId != oldProductID)
                            {
                                count = (model.projectModel.ProjectType == "2") ? 3 : 1;
                                dr_dtF = dtF.NewRow();
                            }

                            //--ss

                            product_desc = drResponse["ProductDescription"] != DBNull.Value ? Convert.ToString(drResponse["ProductDescription"]) : "";
                            product_logiId = drResponse["ProductLogicalId"] != DBNull.Value ? Convert.ToString(drResponse["ProductLogicalId"]) : "";

                            //--ss
                            int questionId = drResponse["QuestionId"] != DBNull.Value ? Convert.ToInt32(drResponse["QuestionId"]) : 0;
                            string answerExpression = drResponse["AnswerExpression"] != DBNull.Value ? Convert.ToString(drResponse["AnswerExpression"]) : "";
                            int questionTypeId = drResponse["QuestionTypeId"] != DBNull.Value ? Convert.ToInt32(drResponse["QuestionTypeId"]) : 1;
                            int PipedFrom = drResponse["PipedFrom"] != DBNull.Value ? Convert.ToInt32(drResponse["PipedFrom"]) : 0;
                            //dr_dtF[0] = drResponse;

                            // Get Total options count for this question
                            questionBase.Id = Convert.ToInt32(questionId);
                            SRV.BaseLayer.ActionResult actionResultAnswersTemp = new SRV.BaseLayer.ActionResult();
                            actionResultAnswersTemp = adminAction.Option_LoadAllById(questionBase);
                            if (actionResultAnswersTemp.dtResult != null && actionResultAnswersTemp.dtResult.Rows.Count > 0)
                            {
                                optionsCount = actionResultAnswersTemp.dtResult.Rows.Count;
                            }

                            if (questionTypeId == 2 || questionTypeId == 4 || questionTypeId == 5 || questionTypeId == 6)
                            {
                                string[] allAnswers = answerExpression.Split('|');
                                if (allAnswers.Length > 0)
                                {
                                    if (questionTypeId == 2)// Multi Punch
                                    {
                                        string allRec = string.Empty;
                                        List<OptionsModel> matrixAns = new List<OptionsModel>();
                                        if (PipedFrom > 0 && !drResponse["QuestionTitle"].ToString().Contains("{{Q}}"))
                                        {
                                            matrixAns = model.lstQuestionModel.Where(v => v.QuestionId == PipedFrom).FirstOrDefault().lstOptionsModel;
                                        }
                                        else
                                        {
                                            matrixAns = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                        }

                                        foreach (var item in matrixAns)
                                        {
                                            string matchedId = string.Empty;
                                            foreach (string ans in allAnswers)
                                            {
                                                if (Convert.ToString(item.OptionId) == ans)
                                                {
                                                    matchedId = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            allRec += matchedId + "|";
                                        }

                                        string[] allAnswersMat = allRec.Substring(0, allRec.Length - 1).Split('|');
                                        foreach (string ans in allAnswersMat)
                                        {

                                            if (count <= dtF.Columns.Count - 1)
                                            {
                                                if ((Regex.Replace(dtF.Columns[count].ToString().Split('-')[0].Split('_')[1], @"[^\d]", String.Empty)) == ans)
                                                {
                                                    dr_dtF[count] = "1";// ans;
                                                    count++;
                                                    optionsCount--;
                                                }
                                                else
                                                {
                                                    dr_dtF[count] = "2";
                                                    count++;
                                                    optionsCount--;
                                                }
                                            }
                                        }
                                        //count = count + optionsCount;
                                    }
                                    else if (questionTypeId == 6) // matrix Type
                                    {
                                        foreach (string ans in allAnswers)
                                        {
                                            dr_dtF[count] = (ans.ToLower().IndexOf("yes") != -1) ? "1" : "2";
                                            count++;
                                            optionsCount--;
                                        }
                                        count = count + optionsCount;
                                    }
                                    else
                                    {
                                        foreach (string ans in allAnswers)
                                        {
                                            dr_dtF[count] = ans;
                                            count++;
                                            optionsCount--;
                                        }
                                        count = count + optionsCount;
                                    }
                                }
                            }
                            else if (questionTypeId == 1) // Single Punch Answers
                            {
                                //List<OptionsModel> SinglePunchAns = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                List<OptionsModel> SinglePunchAns = new List<OptionsModel>();
                                if (PipedFrom > 0 && !drResponse["QuestionTitle"].ToString().Contains("{{Q}}"))
                                {
                                    SinglePunchAns = model.lstQuestionModel.Where(v => v.QuestionId == PipedFrom).FirstOrDefault().lstOptionsModel;
                                }
                                else
                                {
                                    SinglePunchAns = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                }
                                int ansIndex = 0;
                                foreach (var item in SinglePunchAns)
                                {
                                    ansIndex++;
                                    if (Convert.ToString(item.OptionId) == answerExpression)
                                    {
                                        break;
                                    }
                                }

                                dr_dtF[count] = ansIndex;//answerExpression;
                                count++;
                                optionsCount--;
                            }
                            else if (questionTypeId == 8 || questionTypeId == 12) // single punch matrix answers
                            {
                                List<OptionsModel> optionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                List<MatrixTypeModel> columnDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstMatrixTypeModel;
                                int qustionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).Select(v => v.DisplayOrder).FirstOrDefault();
                                string[] allAnswers = answerExpression.Split('|');

                                List<string> lstColumnOrdinals = new List<string>();

                                string formattedAnswer = string.Empty;
                                string answerValue = string.Empty;
                                if (allAnswers.Length > 0)
                                {
                                    foreach (var ansExpression in allAnswers)
                                    {
                                        string displaycol = string.Empty;
                                        string displayopt = string.Empty;
                                        foreach (var item in optionDisplayOrder)
                                        {

                                            if (Convert.ToString(item.OptionId) == ansExpression.Split('~')[0])
                                            {
                                                displaycol = Convert.ToString(item.DisplayOrder);
                                                break;
                                            }
                                        }
                                        foreach (var item in columnDisplayOrder)
                                        {
                                            if (Convert.ToString(item.Id) == ansExpression.Split('~')[1])
                                            {
                                                displayopt = Convert.ToString(item.DisplayOrder);
                                                break;
                                            }
                                        }
                                        //  formattedAnswer = "Q" + questionId + "_M" + displaycol + "#" + displayopt;
                                        formattedAnswer = "Q" + questionId + "_M" + displayopt + "#" + displaycol;
                                        foreach (DataColumn column in dtF.Columns)
                                        {
                                            string[] columnHeader = column.ColumnName.Split('-');
                                            string[] answer = columnHeader[0].Split('_');
                                            if (columnHeader.Length > 1)
                                            {
                                                string answerInHeader = answer[0] + "_" + answer[1];
                                                string[] formattedAnswerArr = formattedAnswer.Split('#');
                                                if (formattedAnswerArr[0] == answerInHeader)
                                                {
                                                    int index = dr_dtF.Table.Columns[column.ColumnName].Ordinal;
                                                    lstColumnOrdinals.Add(index.ToString());
                                                    count++;
                                                    dr_dtF[index] = formattedAnswerArr[1];
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            else if (questionTypeId == 13) // Kano model matrix type
                            {
                                List<OptionsModel> optionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                List<MatrixTypeModel> columnDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstMatrixTypeModel;
                                int qustionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).Select(v => v.DisplayOrder).FirstOrDefault();
                                string[] allAnswers = answerExpression.Split('|');

                                List<string> lstColumnOrdinals = new List<string>();

                                string formattedAnswer = string.Empty;
                                string answerValue = string.Empty;
                                if (allAnswers.Length > 1)
                                {
                                    int repeatId = 0;
                                    foreach (var ansExpression in allAnswers)
                                    {
                                        string displaycol = string.Empty;
                                        string displayopt = string.Empty;
                                        string displayText = string.Empty;
                                        string text = string.Empty;
                                        if (repeatId == Convert.ToInt32(ansExpression.Split('~')[0]))
                                        {
                                            text = "_Absent";
                                            repeatId = 0;
                                        }
                                        else
                                        {
                                            text = "_Present";
                                        }
                                        foreach (var item in optionDisplayOrder)
                                        {
                                            if (Convert.ToString(item.OptionId) == ansExpression.Split('~')[0])
                                            {
                                                displayText = Convert.ToString(item.Option) + text;
                                                displaycol = Convert.ToString(item.DisplayOrder);
                                                repeatId = Convert.ToInt32(ansExpression.Split('~')[0]);
                                                break;
                                            }
                                        }
                                        foreach (var item in columnDisplayOrder)
                                        {
                                            if (Convert.ToString(item.Id) == ansExpression.Split('~')[1])
                                            {
                                                displayopt = Convert.ToString(item.DisplayOrder);
                                                break;
                                            }
                                        }
                                        //formattedAnswer = "Q" + qustionDisplayOrder + "_M" + displaycol + "-" + displayText + "#" + displayopt;
                                        formattedAnswer = "Q" + questionId + "_M" + displaycol + "-" + displayText + "#" + displayopt;
                                        foreach (DataColumn column in dtF.Columns)
                                        {

                                            string[] columnHeader = column.ColumnName.Split('-');
                                            string[] answer = columnHeader[0].Split('_');
                                            if (columnHeader.Length > 1)
                                            {
                                                string answerInHeader = answer[0] + "_" + answer[1] + "-" + columnHeader[1];
                                                string[] formattedAnswerArr = formattedAnswer.Split('#');
                                                if (Regex.Replace(formattedAnswerArr[0], @"\s+", "", RegexOptions.Multiline) == Regex.Replace(answerInHeader, @"\s+", "", RegexOptions.Multiline))
                                                {
                                                    int index = dr_dtF.Table.Columns[column.ColumnName].Ordinal;
                                                    lstColumnOrdinals.Add(index.ToString());
                                                    count++;
                                                    dr_dtF[index] = formattedAnswerArr[1];
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            else if (questionTypeId == 16) // Description type Question
                            {
                            }
                            else if (questionTypeId > 7 && questionTypeId != 15)
                            {
                                List<OptionsModel> optionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                List<MatrixTypeModel> columnDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstMatrixTypeModel;
                                List<DDLMatrixTypeModel> dropDownDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstDDlMatrixModel;
                                count = count + (optionDisplayOrder.Count * columnDisplayOrder.Count);
                                int qustionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).Select(v => v.DisplayOrder).FirstOrDefault();
                                string[] allAnswers = answerExpression.Split('|');

                                List<string> lstColumnOrdinals = new List<string>();

                                string formattedAnswer = string.Empty;
                                string answerValue = string.Empty;
                                int DDlDisplayOrder = 0;
                                if (allAnswers.Length > 1)
                                {
                                    foreach (var ansExpression in allAnswers)
                                    {
                                        string displaycol = string.Empty;
                                        string displayopt = string.Empty;

                                        if (questionTypeId == 9 || questionTypeId == 14)
                                        {
                                            foreach (var item in optionDisplayOrder)
                                            {
                                                if (Convert.ToString(item.OptionId) == ansExpression.Split('~')[0].Split('*')[0])
                                                {
                                                    displaycol = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            foreach (var item in columnDisplayOrder)
                                            {
                                                if (Convert.ToString(item.Id) == ansExpression.Split('~')[1])
                                                {
                                                    displayopt = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            //formattedAnswer = "Q" + qustionDisplayOrder + "_M" + displaycol + "_C" + displayopt;
                                            formattedAnswer = "Q" + questionId + "_M" + displaycol + "_C" + displayopt;
                                            answerValue = ansExpression.Split('~')[0].Split('*')[1];
                                            if (questionTypeId == 14)
                                                DDlDisplayOrder = dropDownDisplayOrder.Where(l => l.Id == Convert.ToInt32(answerValue)).FirstOrDefault().DDLDisplayOrder;
                                        }
                                        else
                                        {
                                            foreach (var item in optionDisplayOrder)
                                            {
                                                if (Convert.ToString(item.OptionId) == ansExpression.Split('~')[0])
                                                {
                                                    displaycol = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            foreach (var item in columnDisplayOrder)
                                            {
                                                if (Convert.ToString(item.Id) == ansExpression.Split('~')[1])
                                                {
                                                    displayopt = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            //formattedAnswer = "Q" + qustionDisplayOrder + "_M" + displaycol + "_C" + displayopt;
                                            formattedAnswer = "Q" + questionId + "_M" + displaycol + "_C" + displayopt;
                                        }
                                        foreach (DataColumn column in dtF.Columns)
                                        {
                                            string[] columnHeader = column.ColumnName.Split('-');

                                            if (columnHeader.Length > 1)
                                            {
                                                string[] answer = columnHeader[0].Split('_');

                                                if (answer.Length > 2)
                                                {
                                                    string answerInHeader = answer[0] + "_" + answer[1] + "_" + answer[2];

                                                    if (formattedAnswer == answerInHeader)
                                                    {
                                                        int index = dr_dtF.Table.Columns[column.ColumnName].Ordinal;
                                                        lstColumnOrdinals.Add(index.ToString());
                                                        //count++;
                                                        if (questionTypeId == 9)
                                                        {
                                                            dr_dtF[index] = answerValue;
                                                        }
                                                        else if (questionTypeId == 14)
                                                        {
                                                            dr_dtF[index] = DDlDisplayOrder;
                                                        }
                                                        else
                                                        {
                                                            dr_dtF[index] = "1";
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }

                                // Fill All the Empty cells with 2

                                for (int i = 0; i <= dtF.Columns.Count - 1; i++)
                                    if (String.IsNullOrEmpty(Convert.ToString(dr_dtF[i])))
                                        dr_dtF[i] = "";
                            }

                            else
                            {
                                dr_dtF[count] = answerExpression;
                                count++;
                                optionsCount--;
                            }

                            if (productId != oldProductID)
                            {
                                dtF.Rows.Add(drTemp);
                                //  drTemp[0] = dtF.Rows.IndexOf(drTemp) + 1;
                                drTemp[0] = response_id;
                                oldProductID = productId;
                            }


                            dr_dtF[1] = (!string.IsNullOrEmpty(responder.InvitationGuid) ? Convert.ToString(responder.InvitationGuid) : "");

                            drTemp = dr_dtF;
                            //---ss
                            if (model.projectModel.ProjectType == "2")
                            {
                                drTemp[2] = product_desc;
                                drTemp[1] = product_logiId;
                            }
                            //---ss

                        }

                        dtF.Rows.Add(dr_dtF);
                        //dr_dtF[0] = dtF.Rows.IndexOf(dr_dtF) + 1;
                        drTemp[0] = response_id;
                        //---ss
                        if (model.projectModel.ProjectType == "2")
                        {
                            dr_dtF[2] = product_desc;
                            dr_dtF[1] = product_logiId;
                        }
                        response_id++;
                        //---ss
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return dtF;
        }
        #endregion

        #region GetReportDataTableForProduct
        private DataTable GetReportDataTableForProduct(string Id, string[] Emails, string[] ExternalResponders)
        {

            RespondentController respondent = new RespondentController();
            SurveyResponseModel model = new SurveyResponseModel();
            DataTable dtF = new DataTable();
            try
            {
                DataTable dt = new DataTable();

                string projectGuid = string.Empty;
                projectGuid = Id;
                dt.Columns.Add("Resp_Id", typeof(string));

                model = respondent.GetProjectPreview(projectGuid);

                //--ss
                if (model.projectModel.ProjectType == "2")
                {
                    DataRow drnew = dt.NewRow();
                    drnew[0] = "Product_LogicalId";
                    dt.Rows.Add(drnew);
                    DataRow drnew1 = dt.NewRow();
                    drnew1[0] = "Product_Name";
                    dt.Rows.Add(drnew1);
                }
                //--

                // Load all the questions of this project
                List<QuestionList> questionsList = new List<QuestionList>();
                questionBase.ProjectId = Convert.ToInt32(model.projectModel.Id);
                actionResult = adminAction.QuestionsLoadAll_ByProjectId(questionBase);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    foreach (DataRow dr in actionResult.dtResult.Rows)
                    {
                        questionsList.Add(new QuestionList
                        {
                            id = dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0,
                            questionTypeId = dr["QuestionTypeId"] != DBNull.Value ? Convert.ToInt32(dr["QuestionTypeId"]) : 0
                        });
                    }
                }

                string fileName = Guid.NewGuid().ToString().Substring(0, 5) + "_" + DateTime.Now.ToString("MM-dd-yyyy") + "_" + model.projectModel.ProjectName.Replace(" ", "");

                if (model.lstQuestionModel != null && model.lstQuestionModel.Count > 0)
                {
                    foreach (var item in model.lstQuestionModel)
                    {
                        if (item.PipedFrom > 0 && (!item.QuestionTitle.Contains("{{Q}}")))
                        {
                            if (item.QuestionTypeId == 2 || item.QuestionTypeId == 4 || item.QuestionTypeId == 5 || item.QuestionTypeId == 6)//Multi Punch || Constant Sum || Ranking || Matrix Type
                            {
                                if (model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel != null && model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel.Count > 0)
                                {
                                    foreach (var opt in model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel)
                                    {
                                        DataRow drnew = dt.NewRow();
                                        drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                        dt.Rows.Add(drnew);
                                    }
                                }
                            }

                            else if (item.QuestionTypeId == 1 || item.QuestionTypeId == 3 || item.QuestionTypeId == 7 || item.QuestionTypeId == 15)//Single Punch || Range || Simple Text
                            {
                                DataRow drnew = dt.NewRow();
                                drnew[0] = "Q" + item.QuestionId + "_S" + "-" + ReplaceHtml.ReplaceText(item.QuestionTitle);
                                dt.Rows.Add(drnew);
                            }
                            else if (item.QuestionTypeId == 8 || item.QuestionTypeId == 12)//Single punch matrix type  && Rank order type
                            {
                                //if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
                                //{
                                //foreach (var opt in item.lstOptionsModel)
                                //{
                                //    DataRow drnew = dt.NewRow();
                                //    //drnew[0] = "Q" + item.QuestionId + "_M" + opt.OptionId + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                //    drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                //    dt.Rows.Add(drnew);
                                //}
                                //}
                                if (item.lstMatrixTypeModel != null && item.lstMatrixTypeModel.Count > 0)
                                {
                                    foreach (var col in item.lstMatrixTypeModel)
                                    {
                                        DataRow drnew = dt.NewRow();
                                        //drnew[0] = "Q" + item.QuestionId + "_M" + opt.OptionId + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                        drnew[0] = "Q" + item.QuestionId + "_M" + col.DisplayOrder + "-" + ReplaceHtml.ReplaceText(col.ColumnName);
                                        dt.Rows.Add(drnew);
                                    }
                                }
                            }
                            else if (item.QuestionTypeId == 13)//Kano model matrix type 
                            {
                                if (model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel != null && model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel.Count > 0)
                                {
                                    foreach (var opt in model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel)
                                    {
                                        DataRow drnew = dt.NewRow();
                                        drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Present";
                                        dt.Rows.Add(drnew);
                                        DataRow drnew1 = dt.NewRow();
                                        drnew1[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Absent";
                                        dt.Rows.Add(drnew1);
                                    }
                                }
                            }
                            else if (item.QuestionTypeId == 16) // Description type Question
                            {
                            }
                            // Single Punch Matrix Type || Range Matrix Type Question ||
                            // Single punch type answer horizontal position || 
                            // Multiple punch type answer horizontal position || 
                            // Rank order type question || 
                            // KANO Model Matrix type question || Dropdown Matrix type question
                            else if (item.QuestionTypeId > 7 && item.QuestionTypeId != 15)
                            {
                                if (model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel != null && model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel.Count > 0)
                                {
                                    foreach (var opt in model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel)
                                    {
                                        foreach (var column in item.lstMatrixTypeModel)
                                        {
                                            DataRow drnew = dt.NewRow();
                                            drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "_C" + column.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + " " + ReplaceHtml.ReplaceText(column.ColumnName);
                                            dt.Rows.Add(drnew);
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (item.QuestionTypeId == 2 || item.QuestionTypeId == 4 || item.QuestionTypeId == 5 || item.QuestionTypeId == 6)//Multi Punch || Constant Sum || Ranking || Matrix Type
                            {
                                if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
                                {
                                    foreach (var opt in item.lstOptionsModel)
                                    {
                                        DataRow drnew = dt.NewRow();
                                        //drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                        drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                        dt.Rows.Add(drnew);
                                    }
                                }
                            }

                            else if (item.QuestionTypeId == 1 || item.QuestionTypeId == 3 || item.QuestionTypeId == 7 || item.QuestionTypeId == 15)//Single Punch || Range || Simple Text
                            {
                                DataRow drnew = dt.NewRow();
                                //drnew[0] = "Q" + item.DisplayOrder + "_S" + "-" + ReplaceHtml.ReplaceText(item.QuestionTitle);
                                drnew[0] = "Q" + item.QuestionId + "_S" + "-" + ReplaceHtml.ReplaceText(item.QuestionTitle);
                                dt.Rows.Add(drnew);
                            }
                            else if (item.QuestionTypeId == 8 || item.QuestionTypeId == 12)//Single punch matrix type  && Rank order type
                            {
                                //if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
                                //{
                                //foreach (var opt in item.lstOptionsModel)
                                //{
                                //    DataRow drnew = dt.NewRow();
                                //    //drnew[0] = "Q" + item.QuestionId + "_M" + opt.OptionId + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                //    drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                //    dt.Rows.Add(drnew);
                                //}
                                //}
                                if (item.lstMatrixTypeModel != null && item.lstMatrixTypeModel.Count > 0)
                                {
                                    foreach (var col in item.lstMatrixTypeModel)
                                    {
                                        DataRow drnew = dt.NewRow();
                                        //drnew[0] = "Q" + item.QuestionId + "_M" + opt.OptionId + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                        drnew[0] = "Q" + item.QuestionId + "_M" + col.DisplayOrder + "-" + ReplaceHtml.ReplaceText(col.ColumnName);
                                        dt.Rows.Add(drnew);
                                    }
                                }
                            }
                            else if (item.QuestionTypeId == 13)//Kano model matrix type 
                            {
                                if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
                                {
                                    foreach (var opt in item.lstOptionsModel)
                                    {
                                        DataRow drnew = dt.NewRow();
                                        //drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Present";
                                        drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Present";
                                        dt.Rows.Add(drnew);
                                        DataRow drnew1 = dt.NewRow();
                                        //drnew1[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Absent";
                                        drnew1[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Absent";
                                        dt.Rows.Add(drnew1);
                                    }
                                }
                            }
                            // Single Punch Matrix Type || Range Matrix Type Question ||
                            // Single punch type answer horizontal position || 
                            // Multiple punch type answer horizontal position || 
                            // Rank order type question || 
                            // KANO Model Matrix type question || Dropdown Matrix type question
                            else if (item.QuestionTypeId > 7)
                            {
                                if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
                                {
                                    foreach (var opt in item.lstOptionsModel)
                                    {
                                        foreach (var column in item.lstMatrixTypeModel)
                                        {
                                            DataRow drnew = dt.NewRow();
                                            //drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "_C" + column.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + " " + ReplaceHtml.ReplaceText(column.ColumnName);
                                            drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "_C" + column.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + " " + ReplaceHtml.ReplaceText(column.ColumnName);
                                            dt.Rows.Add(drnew);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                // Get the Excel Pattern of dt
                dtF = comMethods.GenerateTransposedTable(dt);

                // Get all the reponders
                projectBase.Id = model.projectModel.Id;
                actionResult = adminAction.CompleteRespondersLoad_ByProjectId(projectBase);
                List<RespondersModel> lstResponders = new List<RespondersModel>();
                foreach (DataRow dr in actionResult.dtResult.Rows)
                {
                    lstResponders.Add(new RespondersModel
                    {

                        InvitationGuid = dr["InvitationGuid"] != DBNull.Value ? Convert.ToString(dr["InvitationGuid"]) : "",
                        Email = dr["Email"] != DBNull.Value ? Convert.ToString(dr["Email"]) : "",

                    });
                }
                IEnumerable<RespondersModel> filteredList = lstResponders
                 .GroupBy(customer => customer.InvitationGuid).Select(group => group.First());
                // Get Responses
                SurveyLoadBase surveyLoadBase = new SurveyLoadBase();
                string json = string.Empty;
                SRV.BaseLayer.ActionResult actionResultResponses = new SRV.BaseLayer.ActionResult();
                int response_id = 1;            //ss
                foreach (var responder in filteredList)
                {
                    DataTable responsesDt = new DataTable();
                    surveyLoadBase.Id = model.projectModel.Id;
                    surveyLoadBase.Email = (!string.IsNullOrEmpty(responder.Email) ? Convert.ToString(responder.Email) : "");
                    surveyLoadBase.ExternalResponderId = (!string.IsNullOrEmpty(responder.InvitationGuid) ? Convert.ToString(responder.InvitationGuid) : "");

                    if (Array.IndexOf(Emails, surveyLoadBase.Email) > -1 || Array.IndexOf(ExternalResponders, surveyLoadBase.ExternalResponderId) > -1)
                    {
                        if (model.projectModel.lstProductProjectModel != null &&
                            model.projectModel.lstProductProjectModel.Count > 0)
                        {
                            foreach (var productModel in model.projectModel.lstProductProjectModel)
                            {
                                surveyLoadBase.ProductId = productModel.Id;
                                actionResultResponses = respondentAction.Response_LoadByProjectId(surveyLoadBase);
                                if (actionResultResponses.IsSuccess)
                                {
                                    if (responsesDt.Rows.Count == 0)
                                        responsesDt = actionResultResponses.dtResult.Copy();
                                    else
                                    {
                                        foreach (DataRow drThisRespRow in actionResultResponses.dtResult.Rows)
                                            responsesDt.ImportRow(drThisRespRow);
                                    }
                                }
                            }
                        }
                        else
                        {
                            actionResultResponses = respondentAction.Response_LoadByProjectId(surveyLoadBase);
                            if (actionResultResponses.IsSuccess)
                                responsesDt = actionResultResponses.dtResult.Copy();
                        }
                    }
                    // surveyLoadBase.OfflineResponderId = dr["RespId"] != DBNull.Value ? Convert.ToString(dr["RespId"]) : "";
                    //DataRow dr_dtF = dtF.NewRow();
                    // if (String.IsNullOrEmpty(surveyLoadBase.Email)) continue;

                    //actionResultResponses = respondentAction.Response_LoadByProjectId(surveyLoadBase);
                    if (responsesDt.Rows.Count > 0)//actionResultResponses.IsSuccess
                    {
                        //dr_dtF[0] = surveyLoadBase.Email;

                        int count = 1;
                        count = (model.projectModel.ProjectType == "2") ? count + 2 : count;        //ss
                        DataRow dr_dtF = dtF.NewRow();
                        DataRow drTemp = dtF.NewRow();
                        string product_desc = string.Empty;
                        string product_logiId = string.Empty;
                        int oldProductID = responsesDt.Rows[0]["ProductId"] != DBNull.Value ? Convert.ToInt32(responsesDt.Rows[0]["ProductId"]) : 0;
                        foreach (DataRow drResponse in responsesDt.Rows)// actionResultResponses.dtResult.Rows
                        {

                            int optionsCount = 0;

                            int productId = drResponse["ProductId"] != DBNull.Value ? Convert.ToInt32(drResponse["ProductId"]) : 0;
                            if (productId != oldProductID)
                            {
                                count = (model.projectModel.ProjectType == "2") ? 3 : 1;
                                dr_dtF = dtF.NewRow();
                            }

                            //--ss

                            product_desc = drResponse["ProductDescription"] != DBNull.Value ? Convert.ToString(drResponse["ProductDescription"]) : "";
                            product_logiId = drResponse["ProductLogicalId"] != DBNull.Value ? Convert.ToString(drResponse["ProductLogicalId"]) : "";

                            //--ss
                            int questionId = drResponse["QuestionId"] != DBNull.Value ? Convert.ToInt32(drResponse["QuestionId"]) : 0;
                            string answerExpression = drResponse["AnswerExpression"] != DBNull.Value ? Convert.ToString(drResponse["AnswerExpression"]) : "";
                            int questionTypeId = drResponse["QuestionTypeId"] != DBNull.Value ? Convert.ToInt32(drResponse["QuestionTypeId"]) : 1;
                            int PipedFrom = drResponse["PipedFrom"] != DBNull.Value ? Convert.ToInt32(drResponse["PipedFrom"]) : 0;
                            //dr_dtF[0] = drResponse;

                            // Get Total options count for this question
                            questionBase.Id = Convert.ToInt32(questionId);
                            SRV.BaseLayer.ActionResult actionResultAnswersTemp = new SRV.BaseLayer.ActionResult();
                            actionResultAnswersTemp = adminAction.Option_LoadAllById(questionBase);
                            if (actionResultAnswersTemp.dtResult != null && actionResultAnswersTemp.dtResult.Rows.Count > 0)
                            {
                                optionsCount = actionResultAnswersTemp.dtResult.Rows.Count;
                            }

                            if (questionTypeId == 2 || questionTypeId == 4 || questionTypeId == 5 || questionTypeId == 6)
                            {
                                string[] allAnswers = answerExpression.Split('|');
                                if (allAnswers.Length > 0)
                                {
                                    if (questionTypeId == 2)// Multi Punch
                                    {
                                        string allRec = string.Empty;
                                        //List<OptionsModel> matrixAns = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                        List<OptionsModel> matrixAns = new List<OptionsModel>();
                                        if (PipedFrom > 0 && !drResponse["QuestionTitle"].ToString().Contains("{{Q}}"))
                                        {
                                            matrixAns = model.lstQuestionModel.Where(v => v.QuestionId == PipedFrom).FirstOrDefault().lstOptionsModel;
                                        }
                                        else
                                        {
                                            matrixAns = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                        }

                                        foreach (var item in matrixAns)
                                        {
                                            string matchedId = string.Empty;
                                            foreach (string ans in allAnswers)
                                            {
                                                if (Convert.ToString(item.OptionId) == ans)
                                                {
                                                    matchedId = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            allRec += matchedId + "|";
                                        }

                                        string[] allAnswersMat = allRec.Substring(0, allRec.Length - 1).Split('|');
                                        foreach (string ans in allAnswersMat)
                                        {

                                            if (count <= dtF.Columns.Count - 1)
                                            {
                                                if ((Regex.Replace(dtF.Columns[count].ToString().Split('-')[0].Split('_')[1], @"[^\d]", String.Empty)) == ans)
                                                {
                                                    dr_dtF[count] = "1";// ans;
                                                    count++;
                                                    optionsCount--;
                                                }
                                                else
                                                {
                                                    dr_dtF[count] = "2";
                                                    count++;
                                                    optionsCount--;
                                                }
                                            }
                                        }
                                        //count = count + optionsCount;
                                    }
                                    else if (questionTypeId == 6) // matrix Type
                                    {
                                        foreach (string ans in allAnswers)
                                        {
                                            dr_dtF[count] = (ans.ToLower().IndexOf("yes") != -1) ? "1" : "2";
                                            count++;
                                            optionsCount--;
                                        }
                                        count = count + optionsCount;
                                    }
                                    else
                                    {
                                        foreach (string ans in allAnswers)
                                        {
                                            dr_dtF[count] = ans;
                                            count++;
                                            optionsCount--;
                                        }
                                        count = count + optionsCount;
                                    }
                                }
                            }
                            else if (questionTypeId == 1) // Single Punch Answers
                            {
                                //List<OptionsModel> SinglePunchAns = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                List<OptionsModel> SinglePunchAns = new List<OptionsModel>();
                                if (PipedFrom > 0 && !drResponse["QuestionTitle"].ToString().Contains("{{Q}}"))
                                {
                                    SinglePunchAns = model.lstQuestionModel.Where(v => v.QuestionId == PipedFrom).FirstOrDefault().lstOptionsModel;
                                }
                                else
                                {
                                    SinglePunchAns = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                }
                                int ansIndex = 0;
                                foreach (var item in SinglePunchAns)
                                {
                                    ansIndex++;
                                    if (Convert.ToString(item.OptionId) == answerExpression)
                                    {
                                        break;
                                    }
                                }

                                dr_dtF[count] = ansIndex;//answerExpression;
                                count++;
                                optionsCount--;
                            }
                            else if (questionTypeId == 8 || questionTypeId == 12) // single punch matrix answers
                            {
                                List<OptionsModel> optionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                List<MatrixTypeModel> columnDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstMatrixTypeModel;
                                int qustionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).Select(v => v.DisplayOrder).FirstOrDefault();
                                string[] allAnswers = answerExpression.Split('|');

                                List<string> lstColumnOrdinals = new List<string>();

                                string formattedAnswer = string.Empty;
                                string answerValue = string.Empty;
                                if (allAnswers.Length > 0)
                                {
                                    foreach (var ansExpression in allAnswers)
                                    {
                                        string displaycol = string.Empty;
                                        string displayopt = string.Empty;
                                        foreach (var item in optionDisplayOrder)
                                        {

                                            if (Convert.ToString(item.OptionId) == ansExpression.Split('~')[0])
                                            {
                                                displaycol = Convert.ToString(item.DisplayOrder);
                                                break;
                                            }
                                        }
                                        foreach (var item in columnDisplayOrder)
                                        {
                                            if (Convert.ToString(item.Id) == ansExpression.Split('~')[1])
                                            {
                                                displayopt = Convert.ToString(item.DisplayOrder);
                                                break;
                                            }
                                        }
                                        //  formattedAnswer = "Q" + questionId + "_M" + displaycol + "#" + displayopt;
                                        formattedAnswer = "Q" + questionId + "_M" + displayopt + "#" + displaycol;
                                        foreach (DataColumn column in dtF.Columns)
                                        {
                                            string[] columnHeader = column.ColumnName.Split('-');
                                            string[] answer = columnHeader[0].Split('_');
                                            if (columnHeader.Length > 1)
                                            {
                                                string answerInHeader = answer[0] + "_" + answer[1];
                                                string[] formattedAnswerArr = formattedAnswer.Split('#');
                                                if (formattedAnswerArr[0] == answerInHeader)
                                                {
                                                    int index = dr_dtF.Table.Columns[column.ColumnName].Ordinal;
                                                    lstColumnOrdinals.Add(index.ToString());
                                                    count++;
                                                    dr_dtF[index] = formattedAnswerArr[1];
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            else if (questionTypeId == 13) // Kano model matrix type
                            {
                                List<OptionsModel> optionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                List<MatrixTypeModel> columnDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstMatrixTypeModel;
                                int qustionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).Select(v => v.DisplayOrder).FirstOrDefault();
                                string[] allAnswers = answerExpression.Split('|');

                                List<string> lstColumnOrdinals = new List<string>();

                                string formattedAnswer = string.Empty;
                                string answerValue = string.Empty;
                                if (allAnswers.Length > 1)
                                {
                                    int repeatId = 0;
                                    foreach (var ansExpression in allAnswers)
                                    {
                                        string displaycol = string.Empty;
                                        string displayopt = string.Empty;
                                        string displayText = string.Empty;
                                        string text = string.Empty;
                                        if (repeatId == Convert.ToInt32(ansExpression.Split('~')[0]))
                                        {
                                            text = "_Absent";
                                            repeatId = 0;
                                        }
                                        else
                                        {
                                            text = "_Present";
                                        }
                                        foreach (var item in optionDisplayOrder)
                                        {
                                            if (Convert.ToString(item.OptionId) == ansExpression.Split('~')[0])
                                            {
                                                displayText = Convert.ToString(item.Option) + text;
                                                displaycol = Convert.ToString(item.DisplayOrder);
                                                repeatId = Convert.ToInt32(ansExpression.Split('~')[0]);
                                                break;
                                            }
                                        }
                                        foreach (var item in columnDisplayOrder)
                                        {
                                            if (Convert.ToString(item.Id) == ansExpression.Split('~')[1])
                                            {
                                                displayopt = Convert.ToString(item.DisplayOrder);
                                                break;
                                            }
                                        }
                                        //formattedAnswer = "Q" + qustionDisplayOrder + "_M" + displaycol + "-" + displayText + "#" + displayopt;
                                        formattedAnswer = "Q" + questionId + "_M" + displaycol + "-" + displayText + "#" + displayopt;
                                        foreach (DataColumn column in dtF.Columns)
                                        {

                                            string[] columnHeader = column.ColumnName.Split('-');
                                            string[] answer = columnHeader[0].Split('_');
                                            if (columnHeader.Length > 1)
                                            {
                                                string answerInHeader = answer[0] + "_" + answer[1] + "-" + columnHeader[1];
                                                string[] formattedAnswerArr = formattedAnswer.Split('#');
                                                if (Regex.Replace(formattedAnswerArr[0], @"\s+", "", RegexOptions.Multiline) == Regex.Replace(answerInHeader, @"\s+", "", RegexOptions.Multiline))
                                                {
                                                    int index = dr_dtF.Table.Columns[column.ColumnName].Ordinal;
                                                    lstColumnOrdinals.Add(index.ToString());
                                                    count++;
                                                    dr_dtF[index] = formattedAnswerArr[1];
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            else if (questionTypeId == 16) // Description type Question
                            {
                            }
                            else if (questionTypeId > 7 && questionTypeId != 15)
                            {
                                List<OptionsModel> optionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                List<MatrixTypeModel> columnDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstMatrixTypeModel;
                                List<DDLMatrixTypeModel> dropDownDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstDDlMatrixModel;
                                count = count + (optionDisplayOrder.Count * columnDisplayOrder.Count);
                                int qustionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).Select(v => v.DisplayOrder).FirstOrDefault();
                                string[] allAnswers = answerExpression.Split('|');

                                List<string> lstColumnOrdinals = new List<string>();

                                string formattedAnswer = string.Empty;
                                string answerValue = string.Empty;
                                int DDlDisplayOrder = 0;
                                if (allAnswers.Length > 1)
                                {
                                    foreach (var ansExpression in allAnswers)
                                    {
                                        string displaycol = string.Empty;
                                        string displayopt = string.Empty;

                                        if (questionTypeId == 9 || questionTypeId == 14)
                                        {
                                            foreach (var item in optionDisplayOrder)
                                            {
                                                if (Convert.ToString(item.OptionId) == ansExpression.Split('~')[0].Split('*')[0])
                                                {
                                                    displaycol = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            foreach (var item in columnDisplayOrder)
                                            {
                                                if (Convert.ToString(item.Id) == ansExpression.Split('~')[1])
                                                {
                                                    displayopt = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            //formattedAnswer = "Q" + qustionDisplayOrder + "_M" + displaycol + "_C" + displayopt;
                                            formattedAnswer = "Q" + questionId + "_M" + displaycol + "_C" + displayopt;
                                            answerValue = ansExpression.Split('~')[0].Split('*')[1];
                                            if (questionTypeId == 14)
                                                DDlDisplayOrder = dropDownDisplayOrder.Where(l => l.Id == Convert.ToInt32(answerValue)).FirstOrDefault().DDLDisplayOrder;
                                        }
                                        else
                                        {
                                            foreach (var item in optionDisplayOrder)
                                            {
                                                if (Convert.ToString(item.OptionId) == ansExpression.Split('~')[0])
                                                {
                                                    displaycol = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            foreach (var item in columnDisplayOrder)
                                            {
                                                if (Convert.ToString(item.Id) == ansExpression.Split('~')[1])
                                                {
                                                    displayopt = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            //formattedAnswer = "Q" + qustionDisplayOrder + "_M" + displaycol + "_C" + displayopt;
                                            formattedAnswer = "Q" + questionId + "_M" + displaycol + "_C" + displayopt;
                                        }
                                        foreach (DataColumn column in dtF.Columns)
                                        {
                                            string[] columnHeader = column.ColumnName.Split('-');

                                            if (columnHeader.Length > 1)
                                            {
                                                string[] answer = columnHeader[0].Split('_');

                                                if (answer.Length > 2)
                                                {
                                                    string answerInHeader = answer[0] + "_" + answer[1] + "_" + answer[2];

                                                    if (formattedAnswer == answerInHeader)
                                                    {
                                                        int index = dr_dtF.Table.Columns[column.ColumnName].Ordinal;
                                                        lstColumnOrdinals.Add(index.ToString());
                                                        //count++;
                                                        if (questionTypeId == 9)
                                                        {
                                                            dr_dtF[index] = answerValue;
                                                        }
                                                        else if (questionTypeId == 14)
                                                        {
                                                            dr_dtF[index] = DDlDisplayOrder;
                                                        }
                                                        else
                                                        {
                                                            dr_dtF[index] = "1";
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }

                                // Fill All the Empty cells with 2

                                for (int i = 0; i <= dtF.Columns.Count - 1; i++)
                                    if (String.IsNullOrEmpty(Convert.ToString(dr_dtF[i])))
                                        dr_dtF[i] = "";
                            }

                            else
                            {
                                dr_dtF[count] = answerExpression;
                                count++;
                                optionsCount--;
                            }

                            if (productId != oldProductID)
                            {
                                dtF.Rows.Add(drTemp);
                                //  drTemp[0] = dtF.Rows.IndexOf(drTemp) + 1;
                                drTemp[0] = response_id;
                                oldProductID = productId;
                            }

                            drTemp = dr_dtF;
                            //---ss
                            if (model.projectModel.ProjectType == "2")
                            {
                                drTemp[2] = product_desc;
                                drTemp[1] = product_logiId;
                            }
                            //---ss

                        }

                        dtF.Rows.Add(dr_dtF);
                        //dr_dtF[0] = dtF.Rows.IndexOf(dr_dtF) + 1;
                        drTemp[0] = response_id;
                        //---ss
                        if (model.projectModel.ProjectType == "2")
                        {
                            dr_dtF[2] = product_desc;
                            dr_dtF[1] = product_logiId;
                        }
                        response_id++;
                        //---ss
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return dtF;
        }
        #endregion

        #region GetReportDataTableOffline1
        private DataTable GetReportDataTableOffline1(string Id, int ResponseCount, string[] ExternalResponders)
        {

            RespondentController respondent = new RespondentController();
            SurveyResponseModel model = new SurveyResponseModel();
            DataTable dtF = new DataTable();
            try
            {
                DataTable dt = new DataTable();

                string projectGuid = string.Empty;
                projectGuid = Id;
                dt.Columns.Add("Resp_Id", typeof(string));

                DataRow drnewRow = dt.NewRow();
                drnewRow[0] = "Import_Resp";
                dt.Rows.Add(drnewRow);

                model = respondent.GetProjectPreview(projectGuid);

                //--ss
                if (model.projectModel.ProjectType == "2")
                {
                    DataRow drnew = dt.NewRow();
                    drnew[0] = "Product_LogicalId";
                    dt.Rows.Add(drnew);
                    DataRow drnew1 = dt.NewRow();
                    drnew1[0] = "Product_Name";
                    dt.Rows.Add(drnew1);
                }
                //--

                // Load all the questions of this project
                List<QuestionList> questionsList = new List<QuestionList>();
                questionBase.ProjectId = Convert.ToInt32(model.projectModel.Id);
                actionResult = adminAction.QuestionsLoadAll_ByProjectId(questionBase);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    foreach (DataRow dr in actionResult.dtResult.Rows)
                    {
                        questionsList.Add(new QuestionList
                        {
                            id = dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0,
                            questionTypeId = dr["QuestionTypeId"] != DBNull.Value ? Convert.ToInt32(dr["QuestionTypeId"]) : 0
                        });
                    }
                }

                string fileName = Guid.NewGuid().ToString().Substring(0, 5) + "_" + DateTime.Now.ToString("MM-dd-yyyy") + "_" + model.projectModel.ProjectName.Replace(" ", "");

                if (model.lstQuestionModel != null && model.lstQuestionModel.Count > 0)
                {
                    foreach (var item in model.lstQuestionModel)
                    {
                        if (item.PipedFrom > 0 && (!item.QuestionTitle.Contains("{{Q}}")))
                        {
                            if (item.QuestionTypeId == 2 || item.QuestionTypeId == 4 || item.QuestionTypeId == 5 || item.QuestionTypeId == 6)//Multi Punch || Constant Sum || Ranking || Matrix Type
                            {
                                if (model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel != null && model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel.Count > 0)
                                {
                                    foreach (var opt in model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel)
                                    {
                                        DataRow drnew = dt.NewRow();
                                        drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                        dt.Rows.Add(drnew);
                                    }
                                }
                            }

                            else if (item.QuestionTypeId == 1 || item.QuestionTypeId == 3 || item.QuestionTypeId == 7 || item.QuestionTypeId == 15)//Single Punch || Range || Simple Text
                            {
                                DataRow drnew = dt.NewRow();
                                drnew[0] = "Q" + item.QuestionId + "_S" + "-" + ReplaceHtml.ReplaceText(item.QuestionTitle);
                                dt.Rows.Add(drnew);
                            }
                            else if (item.QuestionTypeId == 8 || item.QuestionTypeId == 12)//Single punch matrix type  && Rank order type
                            {
                                //if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
                                //{
                                //foreach (var opt in item.lstOptionsModel)
                                //{
                                //    DataRow drnew = dt.NewRow();
                                //    //drnew[0] = "Q" + item.QuestionId + "_M" + opt.OptionId + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                //    drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                //    dt.Rows.Add(drnew);
                                //}
                                //}
                                if (item.lstMatrixTypeModel != null && item.lstMatrixTypeModel.Count > 0)
                                {
                                    foreach (var col in item.lstMatrixTypeModel)
                                    {
                                        DataRow drnew = dt.NewRow();
                                        //drnew[0] = "Q" + item.QuestionId + "_M" + opt.OptionId + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                        drnew[0] = "Q" + item.QuestionId + "_M" + col.DisplayOrder + "-" + ReplaceHtml.ReplaceText(col.ColumnName);
                                        dt.Rows.Add(drnew);
                                    }
                                }
                            }
                            else if (item.QuestionTypeId == 13)//Kano model matrix type 
                            {
                                if (model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel != null && model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel.Count > 0)
                                {
                                    foreach (var opt in model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel)
                                    {
                                        DataRow drnew = dt.NewRow();
                                        drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Present";
                                        dt.Rows.Add(drnew);
                                        DataRow drnew1 = dt.NewRow();
                                        drnew1[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Absent";
                                        dt.Rows.Add(drnew1);
                                    }
                                }
                            }
                            else if (item.QuestionTypeId == 16) // Description type Question
                            {
                            }
                            // Single Punch Matrix Type || Range Matrix Type Question ||
                            // Single punch type answer horizontal position || 
                            // Multiple punch type answer horizontal position || 
                            // Rank order type question || 
                            // KANO Model Matrix type question || Dropdown Matrix type question
                            else if (item.QuestionTypeId > 7 && item.QuestionTypeId != 15)
                            {
                                if (model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel != null && model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel.Count > 0)
                                {
                                    foreach (var opt in model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel)
                                    {
                                        foreach (var column in item.lstMatrixTypeModel)
                                        {
                                            DataRow drnew = dt.NewRow();
                                            drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "_C" + column.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + " " + ReplaceHtml.ReplaceText(column.ColumnName);
                                            dt.Rows.Add(drnew);
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (item.QuestionTypeId == 2 || item.QuestionTypeId == 4 || item.QuestionTypeId == 5 || item.QuestionTypeId == 6)//Multi Punch || Constant Sum || Ranking || Matrix Type
                            {
                                if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
                                {
                                    foreach (var opt in item.lstOptionsModel)
                                    {
                                        DataRow drnew = dt.NewRow();
                                        //drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                        drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                        dt.Rows.Add(drnew);
                                    }
                                }
                            }

                            else if (item.QuestionTypeId == 1 || item.QuestionTypeId == 3 || item.QuestionTypeId == 7 || item.QuestionTypeId == 15)//Single Punch || Range || Simple Text
                            {
                                DataRow drnew = dt.NewRow();
                                //drnew[0] = "Q" + item.DisplayOrder + "_S" + "-" + ReplaceHtml.ReplaceText(item.QuestionTitle);
                                drnew[0] = "Q" + item.QuestionId + "_S" + "-" + ReplaceHtml.ReplaceText(item.QuestionTitle);
                                dt.Rows.Add(drnew);
                            }
                            else if (item.QuestionTypeId == 8 || item.QuestionTypeId == 12)//Single punch matrix type  && Rank order type
                            {
                                //if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
                                //{
                                //foreach (var opt in item.lstOptionsModel)
                                //{
                                //    DataRow drnew = dt.NewRow();
                                //    //drnew[0] = "Q" + item.QuestionId + "_M" + opt.OptionId + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                //    drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                //    dt.Rows.Add(drnew);
                                //}
                                //}
                                if (item.lstMatrixTypeModel != null && item.lstMatrixTypeModel.Count > 0)
                                {
                                    foreach (var col in item.lstMatrixTypeModel)
                                    {
                                        DataRow drnew = dt.NewRow();
                                        //drnew[0] = "Q" + item.QuestionId + "_M" + opt.OptionId + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                        drnew[0] = "Q" + item.QuestionId + "_M" + col.DisplayOrder + "-" + ReplaceHtml.ReplaceText(col.ColumnName);
                                        dt.Rows.Add(drnew);
                                    }
                                }
                            }
                            else if (item.QuestionTypeId == 13)//Kano model matrix type 
                            {
                                if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
                                {
                                    foreach (var opt in item.lstOptionsModel)
                                    {
                                        DataRow drnew = dt.NewRow();
                                        //drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Present";
                                        drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Present";
                                        dt.Rows.Add(drnew);
                                        DataRow drnew1 = dt.NewRow();
                                        //drnew1[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Absent";
                                        drnew1[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Absent";
                                        dt.Rows.Add(drnew1);
                                    }
                                }
                            }
                            // Single Punch Matrix Type || Range Matrix Type Question ||
                            // Single punch type answer horizontal position || 
                            // Multiple punch type answer horizontal position || 
                            // Rank order type question || 
                            // KANO Model Matrix type question || Dropdown Matrix type question
                            else if (item.QuestionTypeId > 7)
                            {
                                if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
                                {
                                    foreach (var opt in item.lstOptionsModel)
                                    {
                                        foreach (var column in item.lstMatrixTypeModel)
                                        {
                                            DataRow drnew = dt.NewRow();
                                            //drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "_C" + column.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + " " + ReplaceHtml.ReplaceText(column.ColumnName);
                                            drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "_C" + column.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + " " + ReplaceHtml.ReplaceText(column.ColumnName);
                                            dt.Rows.Add(drnew);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }


                // Get the Excel Pattern of dt
                dtF = comMethods.GenerateTransposedTable(dt);

                // Get all the reponders
                projectBase.Id = model.projectModel.Id;
                actionResult = adminAction.Responders_Offline_LoadBy_ProjectId(projectBase);

                // Get Responses
                SurveyLoadBase surveyLoadBase = new SurveyLoadBase();
                string json = string.Empty;
                SRV.BaseLayer.ActionResult actionResultResponses = new SRV.BaseLayer.ActionResult();
                int response_id = ResponseCount;            //ss
                foreach (DataRow dr in actionResult.dtResult.Rows)
                {
                    surveyLoadBase.Id = model.projectModel.Id;
                    surveyLoadBase.OfflineResponderId = dr["RespId"] != DBNull.Value ? Convert.ToString(dr["RespId"]) : "";
                    //DataRow dr_dtF = dtF.NewRow();

                    if (String.IsNullOrEmpty(surveyLoadBase.OfflineResponderId)) continue;


                    DataTable responsesDt = new DataTable();

                    if (Array.IndexOf(ExternalResponders, surveyLoadBase.OfflineResponderId) > -1)
                    {
                        if (model.projectModel.lstProductProjectModel != null && model.projectModel.lstProductProjectModel.Count > 0)
                        {
                            foreach (var productModel in model.projectModel.lstProductProjectModel)
                            {
                                surveyLoadBase.ProductId = productModel.Id;
                                actionResultResponses = respondentAction.Response_Offline_LoadBy_ProjectId_RespId(surveyLoadBase);
                                if (actionResultResponses.IsSuccess)
                                {
                                    if (responsesDt.Rows.Count == 0)
                                        responsesDt = actionResultResponses.dtResult.Copy();
                                    else
                                    {
                                        foreach (DataRow drThisRespRow in actionResultResponses.dtResult.Rows)
                                            responsesDt.ImportRow(drThisRespRow);

                                    }
                                }
                            }
                        }
                        else
                        {
                            actionResultResponses = respondentAction.Response_Offline_LoadBy_ProjectId_RespId(surveyLoadBase);
                            if (actionResultResponses.IsSuccess)
                                responsesDt = actionResultResponses.dtResult.Copy();

                        }
                    }


                    //actionResultResponses = respondentAction.Response_Offline_LoadBy_ProjectId_RespId(surveyLoadBase);
                    if (responsesDt.Rows.Count > 0)//actionResultResponses.IsSuccess
                    {
                        //dr_dtF[0] = surveyLoadBase.Email;

                        int count = 1;
                        count = (model.projectModel.ProjectType == "2") ? count + 2 : count;        //ss
                        DataRow dr_dtF = dtF.NewRow();
                        DataRow drTemp = dtF.NewRow();
                        string product_desc = string.Empty;
                        string product_logiId = string.Empty;
                        int oldProductID = responsesDt.Rows[0]["ProductId"] != DBNull.Value ? Convert.ToInt32(responsesDt.Rows[0]["ProductId"]) : 0;
                        foreach (DataRow drResponse in responsesDt.Rows)// actionResultResponses.dtResult.Rows
                        {

                            int optionsCount = 0;

                            int productId = drResponse["ProductId"] != DBNull.Value ? Convert.ToInt32(drResponse["ProductId"]) : 0;
                            if (productId != oldProductID)
                            {
                                count = (model.projectModel.ProjectType == "2") ? 3 : 1;
                                dr_dtF = dtF.NewRow();
                            }

                            //--ss

                            product_desc = drResponse["ProductDescription"] != DBNull.Value ? Convert.ToString(drResponse["ProductDescription"]) : "";
                            product_logiId = drResponse["ProductLogicalId"] != DBNull.Value ? Convert.ToString(drResponse["ProductLogicalId"]) : "";

                            //--ss
                            int questionId = drResponse["QuestionId"] != DBNull.Value ? Convert.ToInt32(drResponse["QuestionId"]) : 0;
                            string answerExpression = drResponse["AnswerExpression"] != DBNull.Value ? Convert.ToString(drResponse["AnswerExpression"]) : "";
                            int questionTypeId = drResponse["QuestionTypeId"] != DBNull.Value ? Convert.ToInt32(drResponse["QuestionTypeId"]) : 1;
                            int PipedFrom = drResponse["PipedFrom"] != DBNull.Value ? Convert.ToInt32(drResponse["PipedFrom"]) : 0;
                            //dr_dtF[0] = drResponse;

                            // Get Total options count for this question
                            questionBase.Id = Convert.ToInt32(questionId);
                            SRV.BaseLayer.ActionResult actionResultAnswersTemp = new SRV.BaseLayer.ActionResult();
                            actionResultAnswersTemp = adminAction.Option_LoadAllById(questionBase);
                            if (actionResultAnswersTemp.dtResult != null && actionResultAnswersTemp.dtResult.Rows.Count > 0)
                            {
                                optionsCount = actionResultAnswersTemp.dtResult.Rows.Count;
                            }

                            if (questionTypeId == 2 || questionTypeId == 4 || questionTypeId == 5 || questionTypeId == 6)
                            {
                                string[] allAnswers = answerExpression.Split('|');
                                if (allAnswers.Length > 0)
                                {
                                    if (questionTypeId == 2)// Multi Punch
                                    {
                                        string allRec = string.Empty;
                                        //List<OptionsModel> matrixAns = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;


                                        List<OptionsModel> matrixAns = new List<OptionsModel>();
                                        if (PipedFrom > 0 && !drResponse["QuestionTitle"].ToString().Contains("{{Q}}"))
                                        {
                                            matrixAns = model.lstQuestionModel.Where(v => v.QuestionId == PipedFrom).FirstOrDefault().lstOptionsModel;
                                        }
                                        else
                                        {
                                            matrixAns = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                        }

                                        foreach (var item in matrixAns)
                                        {
                                            string matchedId = string.Empty;
                                            foreach (string ans in allAnswers)
                                            {
                                                if (Convert.ToString(item.OptionId) == ans)
                                                {
                                                    matchedId = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            allRec += matchedId + "|";
                                        }

                                        string[] allAnswersMat = allRec.Substring(0, allRec.Length - 1).Split('|');
                                        foreach (string ans in allAnswersMat)
                                        {

                                            if (count <= dtF.Columns.Count - 1)
                                            {
                                                if ((Regex.Replace(dtF.Columns[count].ToString().Split('-')[0].Split('_')[1], @"[^\d]", String.Empty)) == ans)
                                                {
                                                    dr_dtF[count] = "1";// ans;
                                                    count++;
                                                    optionsCount--;
                                                }
                                                else
                                                {
                                                    dr_dtF[count] = "2";
                                                    count++;
                                                    optionsCount--;
                                                }
                                            }
                                        }
                                        //count = count + optionsCount;
                                    }
                                    else if (questionTypeId == 6) // matrix Type
                                    {
                                        foreach (string ans in allAnswers)
                                        {
                                            dr_dtF[count] = (ans.ToLower().IndexOf("yes") != -1) ? "1" : "2";
                                            count++;
                                            optionsCount--;
                                        }
                                        count = count + optionsCount;
                                    }
                                    else
                                    {
                                        foreach (string ans in allAnswers)
                                        {
                                            dr_dtF[count] = ans;
                                            count++;
                                            optionsCount--;
                                        }
                                        count = count + optionsCount;
                                    }
                                }
                            }
                            else if (questionTypeId == 1) // Single Punch Answers
                            {
                                //List<OptionsModel> SinglePunchAns = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;

                                List<OptionsModel> SinglePunchAns = new List<OptionsModel>();
                                if (PipedFrom > 0 && !drResponse["QuestionTitle"].ToString().Contains("{{Q}}"))
                                {
                                    SinglePunchAns = model.lstQuestionModel.Where(v => v.QuestionId == PipedFrom).FirstOrDefault().lstOptionsModel;
                                }
                                else
                                {
                                    SinglePunchAns = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                }
                                int ansIndex = 0;
                                foreach (var item in SinglePunchAns)
                                {
                                    ansIndex++;
                                    if (Convert.ToString(item.OptionId) == answerExpression)
                                    {
                                        break;
                                    }
                                }

                                dr_dtF[count] = ansIndex;//answerExpression;
                                count++;
                                optionsCount--;
                            }
                            else if (questionTypeId == 8 || questionTypeId == 12) // single punch matrix answers
                            {
                                List<OptionsModel> optionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                List<MatrixTypeModel> columnDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstMatrixTypeModel;
                                int qustionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).Select(v => v.DisplayOrder).FirstOrDefault();
                                string[] allAnswers = answerExpression.Split('|');

                                List<string> lstColumnOrdinals = new List<string>();

                                string formattedAnswer = string.Empty;
                                string answerValue = string.Empty;
                                if (allAnswers.Length > 0)
                                {
                                    foreach (var ansExpression in allAnswers)
                                    {
                                        if (!string.IsNullOrEmpty(ansExpression))
                                        {
                                            string displaycol = string.Empty;
                                            string displayopt = string.Empty;
                                            foreach (var item in optionDisplayOrder)
                                            {

                                                if (Convert.ToString(item.OptionId) == ansExpression.Split('~')[0])
                                                {
                                                    displaycol = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            foreach (var item in columnDisplayOrder)
                                            {
                                                if (Convert.ToString(item.Id) == ansExpression.Split('~')[1])
                                                {
                                                    displayopt = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            //  formattedAnswer = "Q" + questionId + "_M" + displaycol + "#" + displayopt;
                                            formattedAnswer = "Q" + questionId + "_M" + displayopt + "#" + displaycol;
                                            foreach (DataColumn column in dtF.Columns)
                                            {
                                                string[] columnHeader = column.ColumnName.Split('-');
                                                string[] answer = columnHeader[0].Split('_');
                                                if (columnHeader.Length > 1)
                                                {
                                                    string answerInHeader = answer[0] + "_" + answer[1];
                                                    string[] formattedAnswerArr = formattedAnswer.Split('#');
                                                    if (formattedAnswerArr[0] == answerInHeader)
                                                    {
                                                        int index = dr_dtF.Table.Columns[column.ColumnName].Ordinal;
                                                        lstColumnOrdinals.Add(index.ToString());
                                                        count++;
                                                        dr_dtF[index] = formattedAnswerArr[1];
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            else if (questionTypeId == 13) // Kano model matrix type
                            {
                                List<OptionsModel> optionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                List<MatrixTypeModel> columnDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstMatrixTypeModel;
                                int qustionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).Select(v => v.DisplayOrder).FirstOrDefault();
                                string[] allAnswers = answerExpression.Split('|');

                                List<string> lstColumnOrdinals = new List<string>();

                                string formattedAnswer = string.Empty;
                                string answerValue = string.Empty;
                                if (allAnswers.Length > 1)
                                {
                                    int repeatId = 0;
                                    foreach (var ansExpression in allAnswers)
                                    {
                                        string displaycol = string.Empty;
                                        string displayopt = string.Empty;
                                        string displayText = string.Empty;
                                        string text = string.Empty;
                                        if (repeatId == Convert.ToInt32(ansExpression.Split('~')[0]))
                                        {
                                            text = "_Absent";
                                            repeatId = 0;
                                        }
                                        else
                                        {
                                            text = "_Present";
                                        }
                                        foreach (var item in optionDisplayOrder)
                                        {
                                            if (Convert.ToString(item.OptionId) == ansExpression.Split('~')[0])
                                            {
                                                displayText = Convert.ToString(item.Option) + text;
                                                displaycol = Convert.ToString(item.DisplayOrder);
                                                repeatId = Convert.ToInt32(ansExpression.Split('~')[0]);
                                                break;
                                            }
                                        }
                                        foreach (var item in columnDisplayOrder)
                                        {
                                            if (Convert.ToString(item.Id) == ansExpression.Split('~')[1])
                                            {
                                                displayopt = Convert.ToString(item.DisplayOrder);
                                                break;
                                            }
                                        }
                                        //formattedAnswer = "Q" + qustionDisplayOrder + "_M" + displaycol + "-" + displayText + "#" + displayopt;
                                        formattedAnswer = "Q" + questionId + "_M" + displaycol + "-" + displayText + "#" + displayopt;
                                        foreach (DataColumn column in dtF.Columns)
                                        {

                                            string[] columnHeader = column.ColumnName.Split('-');
                                            string[] answer = columnHeader[0].Split('_');
                                            if (columnHeader.Length > 1)
                                            {
                                                string answerInHeader = answer[0] + "_" + answer[1] + "-" + columnHeader[1];
                                                string[] formattedAnswerArr = formattedAnswer.Split('#');
                                                if (Regex.Replace(formattedAnswerArr[0], @"\s+", "", RegexOptions.Multiline) == Regex.Replace(answerInHeader, @"\s+", "", RegexOptions.Multiline))
                                                {
                                                    int index = dr_dtF.Table.Columns[column.ColumnName].Ordinal;
                                                    lstColumnOrdinals.Add(index.ToString());
                                                    count++;
                                                    dr_dtF[index] = formattedAnswerArr[1];
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            else if (questionTypeId == 16) // Description type Question
                            {
                            }
                            else if (questionTypeId > 7 && questionTypeId != 15)
                            {
                                List<OptionsModel> optionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                List<MatrixTypeModel> columnDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstMatrixTypeModel;
                                List<DDLMatrixTypeModel> dropDownDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstDDlMatrixModel;
                                count = count + (optionDisplayOrder.Count * columnDisplayOrder.Count);
                                int qustionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).Select(v => v.DisplayOrder).FirstOrDefault();
                                string[] allAnswers = answerExpression.Split('|');

                                List<string> lstColumnOrdinals = new List<string>();

                                string formattedAnswer = string.Empty;
                                string answerValue = string.Empty;
                                int DDlDisplayOrder = 0;
                                if (allAnswers.Length > 1)
                                {
                                    foreach (var ansExpression in allAnswers)
                                    {
                                        string displaycol = string.Empty;
                                        string displayopt = string.Empty;

                                        if (questionTypeId == 9 || questionTypeId == 14)
                                        {
                                            foreach (var item in optionDisplayOrder)
                                            {
                                                if (Convert.ToString(item.OptionId) == ansExpression.Split('~')[0].Split('*')[0])
                                                {
                                                    displaycol = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            foreach (var item in columnDisplayOrder)
                                            {
                                                if (Convert.ToString(item.Id) == ansExpression.Split('~')[1])
                                                {
                                                    displayopt = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            //formattedAnswer = "Q" + qustionDisplayOrder + "_M" + displaycol + "_C" + displayopt;
                                            formattedAnswer = "Q" + questionId + "_M" + displaycol + "_C" + displayopt;
                                            answerValue = ansExpression.Split('~')[0].Split('*')[1];
                                            if (questionTypeId == 14)
                                                DDlDisplayOrder = dropDownDisplayOrder.Where(l => l.Id == Convert.ToInt32(answerValue)).FirstOrDefault().DDLDisplayOrder;
                                        }
                                        else
                                        {
                                            foreach (var item in optionDisplayOrder)
                                            {
                                                if (Convert.ToString(item.OptionId) == ansExpression.Split('~')[0])
                                                {
                                                    displaycol = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            foreach (var item in columnDisplayOrder)
                                            {
                                                if (Convert.ToString(item.Id) == ansExpression.Split('~')[1])
                                                {
                                                    displayopt = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            //formattedAnswer = "Q" + qustionDisplayOrder + "_M" + displaycol + "_C" + displayopt;
                                            formattedAnswer = "Q" + questionId + "_M" + displaycol + "_C" + displayopt;
                                        }
                                        foreach (DataColumn column in dtF.Columns)
                                        {
                                            string[] columnHeader = column.ColumnName.Split('-');

                                            if (columnHeader.Length > 1)
                                            {
                                                string[] answer = columnHeader[0].Split('_');

                                                if (answer.Length > 2)
                                                {
                                                    string answerInHeader = answer[0] + "_" + answer[1] + "_" + answer[2];

                                                    if (formattedAnswer == answerInHeader)
                                                    {
                                                        int index = dr_dtF.Table.Columns[column.ColumnName].Ordinal;
                                                        lstColumnOrdinals.Add(index.ToString());
                                                        //count++;
                                                        if (questionTypeId == 9)
                                                        {
                                                            dr_dtF[index] = answerValue;
                                                        }
                                                        else if (questionTypeId == 14)
                                                        {
                                                            dr_dtF[index] = DDlDisplayOrder;
                                                        }
                                                        else
                                                        {
                                                            dr_dtF[index] = "1";
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }

                                // Fill All the Empty cells with 2

                                for (int i = 0; i <= dtF.Columns.Count - 1; i++)
                                    if (String.IsNullOrEmpty(Convert.ToString(dr_dtF[i])))
                                        dr_dtF[i] = "";
                            }

                            else
                            {
                                dr_dtF[count] = answerExpression;
                                count++;
                                optionsCount--;
                            }



                            if (productId != oldProductID)
                            {
                                dtF.Rows.Add(drTemp);
                                drTemp[0] = response_id;
                                oldProductID = productId;
                            }

                            drTemp = dr_dtF;
                            //---ss
                            if (model.projectModel.ProjectType == "2")
                            {
                                drTemp[2] = product_desc;
                                drTemp[1] = product_logiId;
                            }
                            //---ss




                        }


                        dr_dtF[1] = dr["RespId"] != DBNull.Value ? Convert.ToString(dr["RespId"]) : "";

                        dtF.Rows.Add(dr_dtF);
                        drTemp[0] = response_id;
                        //---ss
                        if (model.projectModel.ProjectType == "2")
                        {
                            dr_dtF[2] = product_desc;
                            dr_dtF[1] = product_logiId;
                        }
                        response_id++;
                        //---ss


                    }
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return dtF;
        }
        #endregion

        #region GetReportDataTableOfflineForProduct
        private DataTable GetReportDataTableOfflineForProduct(string Id, int ResponseCount, string[] ExternalResponders)
        {

            RespondentController respondent = new RespondentController();
            SurveyResponseModel model = new SurveyResponseModel();
            DataTable dtF = new DataTable();
            try
            {
                DataTable dt = new DataTable();

                string projectGuid = string.Empty;
                projectGuid = Id;
                dt.Columns.Add("Resp_Id", typeof(string));

                model = respondent.GetProjectPreview(projectGuid);

                //--ss
                if (model.projectModel.ProjectType == "2")
                {
                    DataRow drnew = dt.NewRow();
                    drnew[0] = "Product_LogicalId";
                    dt.Rows.Add(drnew);
                    DataRow drnew1 = dt.NewRow();
                    drnew1[0] = "Product_Name";
                    dt.Rows.Add(drnew1);
                }
                //--

                // Load all the questions of this project
                List<QuestionList> questionsList = new List<QuestionList>();
                questionBase.ProjectId = Convert.ToInt32(model.projectModel.Id);
                actionResult = adminAction.QuestionsLoadAll_ByProjectId(questionBase);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    foreach (DataRow dr in actionResult.dtResult.Rows)
                    {
                        questionsList.Add(new QuestionList
                        {
                            id = dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0,
                            questionTypeId = dr["QuestionTypeId"] != DBNull.Value ? Convert.ToInt32(dr["QuestionTypeId"]) : 0
                        });
                    }
                }

                string fileName = Guid.NewGuid().ToString().Substring(0, 5) + "_" + DateTime.Now.ToString("MM-dd-yyyy") + "_" + model.projectModel.ProjectName.Replace(" ", "");

                if (model.lstQuestionModel != null && model.lstQuestionModel.Count > 0)
                {
                    foreach (var item in model.lstQuestionModel)
                    {
                        if (item.QuestionTypeId == 2 || item.QuestionTypeId == 4 || item.QuestionTypeId == 5 || item.QuestionTypeId == 6)//Multi Punch || Constant Sum || Ranking || Matrix Type
                        {
                            if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
                            {
                                foreach (var opt in item.lstOptionsModel)
                                {
                                    DataRow drnew = dt.NewRow();
                                    //drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                    drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                    dt.Rows.Add(drnew);
                                }
                            }
                        }

                        else if (item.QuestionTypeId == 1 || item.QuestionTypeId == 3 || item.QuestionTypeId == 7 || item.QuestionTypeId == 15)//Single Punch || Range || Simple Text
                        {
                            DataRow drnew = dt.NewRow();
                            //drnew[0] = "Q" + item.DisplayOrder + "_S" + "-" + ReplaceHtml.ReplaceText(item.QuestionTitle);
                            drnew[0] = "Q" + item.QuestionId + "_S" + "-" + ReplaceHtml.ReplaceText(item.QuestionTitle);
                            dt.Rows.Add(drnew);
                        }
                        else if (item.QuestionTypeId == 8 || item.QuestionTypeId == 12)//Single punch matrix type  && Rank order type
                        {
                            //if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
                            //{
                            //foreach (var opt in item.lstOptionsModel)
                            //{
                            //    DataRow drnew = dt.NewRow();
                            //    //drnew[0] = "Q" + item.QuestionId + "_M" + opt.OptionId + "-" + ReplaceHtml.ReplaceText(opt.Option);
                            //    drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
                            //    dt.Rows.Add(drnew);
                            //}
                            //}
                            if (item.lstMatrixTypeModel != null && item.lstMatrixTypeModel.Count > 0)
                            {
                                foreach (var col in item.lstMatrixTypeModel)
                                {
                                    DataRow drnew = dt.NewRow();
                                    //drnew[0] = "Q" + item.QuestionId + "_M" + opt.OptionId + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                    drnew[0] = "Q" + item.QuestionId + "_M" + col.DisplayOrder + "-" + ReplaceHtml.ReplaceText(col.ColumnName);
                                    dt.Rows.Add(drnew);
                                }
                            }
                        }
                        else if (item.QuestionTypeId == 13)//Kano model matrix type 
                        {
                            if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
                            {
                                foreach (var opt in item.lstOptionsModel)
                                {
                                    DataRow drnew = dt.NewRow();
                                    //drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Present";
                                    drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Present";
                                    dt.Rows.Add(drnew);
                                    DataRow drnew1 = dt.NewRow();
                                    //drnew1[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Absent";
                                    drnew1[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Absent";
                                    dt.Rows.Add(drnew1);
                                }
                            }
                        }
                        // Single Punch Matrix Type || Range Matrix Type Question ||
                        // Single punch type answer horizontal position || 
                        // Multiple punch type answer horizontal position || 
                        // Rank order type question || 
                        // KANO Model Matrix type question || Dropdown Matrix type question
                        else if (item.QuestionTypeId > 7)
                        {
                            if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
                            {
                                foreach (var opt in item.lstOptionsModel)
                                {
                                    foreach (var column in item.lstMatrixTypeModel)
                                    {
                                        DataRow drnew = dt.NewRow();
                                        //drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "_C" + column.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + " " + ReplaceHtml.ReplaceText(column.ColumnName);
                                        drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "_C" + column.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + " " + ReplaceHtml.ReplaceText(column.ColumnName);
                                        dt.Rows.Add(drnew);
                                    }
                                }
                            }
                        }
                    }
                }
                // Get the Excel Pattern of dt
                dtF = comMethods.GenerateTransposedTable(dt);

                // Get all the reponders
                projectBase.Id = model.projectModel.Id;
                actionResult = adminAction.Responders_Offline_LoadBy_ProjectId(projectBase);

                // Get Responses
                SurveyLoadBase surveyLoadBase = new SurveyLoadBase();
                string json = string.Empty;
                SRV.BaseLayer.ActionResult actionResultResponses = new SRV.BaseLayer.ActionResult();
                int response_id = ResponseCount;            //ss
                foreach (DataRow dr in actionResult.dtResult.Rows)
                {
                    surveyLoadBase.Id = model.projectModel.Id;
                    surveyLoadBase.OfflineResponderId = dr["RespId"] != DBNull.Value ? Convert.ToString(dr["RespId"]) : "";
                    //DataRow dr_dtF = dtF.NewRow();

                    if (String.IsNullOrEmpty(surveyLoadBase.OfflineResponderId)) continue;


                    DataTable responsesDt = new DataTable();

                    if (Array.IndexOf(ExternalResponders, surveyLoadBase.OfflineResponderId) > -1)
                    {
                        if (model.projectModel.lstProductProjectModel != null && model.projectModel.lstProductProjectModel.Count > 0)
                        {
                            foreach (var productModel in model.projectModel.lstProductProjectModel)
                            {
                                surveyLoadBase.ProductId = productModel.Id;
                                actionResultResponses = respondentAction.Response_Offline_LoadBy_ProjectId_RespId(surveyLoadBase);
                                if (actionResultResponses.IsSuccess)
                                {
                                    if (responsesDt.Rows.Count == 0)
                                        responsesDt = actionResultResponses.dtResult.Copy();
                                    else
                                    {
                                        foreach (DataRow drThisRespRow in actionResultResponses.dtResult.Rows)
                                            responsesDt.ImportRow(drThisRespRow);

                                    }
                                }
                            }
                        }
                        else
                        {
                            actionResultResponses = respondentAction.Response_Offline_LoadBy_ProjectId_RespId(surveyLoadBase);
                            if (actionResultResponses.IsSuccess)
                                responsesDt = actionResultResponses.dtResult.Copy();

                        }
                    }


                    //actionResultResponses = respondentAction.Response_Offline_LoadBy_ProjectId_RespId(surveyLoadBase);
                    if (responsesDt.Rows.Count > 0)//actionResultResponses.IsSuccess
                    {
                        //dr_dtF[0] = surveyLoadBase.Email;

                        int count = 1;
                        count = (model.projectModel.ProjectType == "2") ? count + 2 : count;        //ss
                        DataRow dr_dtF = dtF.NewRow();
                        DataRow drTemp = dtF.NewRow();
                        string product_desc = string.Empty;
                        string product_logiId = string.Empty;
                        int oldProductID = responsesDt.Rows[0]["ProductId"] != DBNull.Value ? Convert.ToInt32(responsesDt.Rows[0]["ProductId"]) : 0;
                        foreach (DataRow drResponse in responsesDt.Rows)// actionResultResponses.dtResult.Rows
                        {

                            int optionsCount = 0;

                            int productId = drResponse["ProductId"] != DBNull.Value ? Convert.ToInt32(drResponse["ProductId"]) : 0;
                            if (productId != oldProductID)
                            {
                                count = (model.projectModel.ProjectType == "2") ? 3 : 1;
                                dr_dtF = dtF.NewRow();
                            }

                            //--ss

                            product_desc = drResponse["ProductDescription"] != DBNull.Value ? Convert.ToString(drResponse["ProductDescription"]) : "";
                            product_logiId = drResponse["ProductLogicalId"] != DBNull.Value ? Convert.ToString(drResponse["ProductLogicalId"]) : "";

                            //--ss
                            int questionId = drResponse["QuestionId"] != DBNull.Value ? Convert.ToInt32(drResponse["QuestionId"]) : 0;
                            string answerExpression = drResponse["AnswerExpression"] != DBNull.Value ? Convert.ToString(drResponse["AnswerExpression"]) : "";
                            int questionTypeId = drResponse["QuestionTypeId"] != DBNull.Value ? Convert.ToInt32(drResponse["QuestionTypeId"]) : 1;
                            int PipedFrom = drResponse["PipedFrom"] != DBNull.Value ? Convert.ToInt32(drResponse["PipedFrom"]) : 0;
                            //dr_dtF[0] = drResponse;

                            // Get Total options count for this question
                            questionBase.Id = Convert.ToInt32(questionId);
                            SRV.BaseLayer.ActionResult actionResultAnswersTemp = new SRV.BaseLayer.ActionResult();
                            actionResultAnswersTemp = adminAction.Option_LoadAllById(questionBase);
                            if (actionResultAnswersTemp.dtResult != null && actionResultAnswersTemp.dtResult.Rows.Count > 0)
                            {
                                optionsCount = actionResultAnswersTemp.dtResult.Rows.Count;
                            }

                            if (questionTypeId == 2 || questionTypeId == 4 || questionTypeId == 5 || questionTypeId == 6)
                            {
                                string[] allAnswers = answerExpression.Split('|');
                                if (allAnswers.Length > 0)
                                {
                                    if (questionTypeId == 2)// Multi Punch
                                    {
                                        string allRec = string.Empty;
                                        //List<OptionsModel> matrixAns = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;


                                        List<OptionsModel> matrixAns = new List<OptionsModel>();
                                        if (PipedFrom > 0 && !drResponse["QuestionTitle"].ToString().Contains("{{Q}}"))
                                        {
                                            matrixAns = model.lstQuestionModel.Where(v => v.QuestionId == PipedFrom).FirstOrDefault().lstOptionsModel;
                                        }
                                        else
                                        {
                                            matrixAns = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                        }

                                        foreach (var item in matrixAns)
                                        {
                                            string matchedId = string.Empty;
                                            foreach (string ans in allAnswers)
                                            {
                                                if (Convert.ToString(item.OptionId) == ans)
                                                {
                                                    matchedId = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            allRec += matchedId + "|";
                                        }

                                        string[] allAnswersMat = allRec.Substring(0, allRec.Length - 1).Split('|');
                                        foreach (string ans in allAnswersMat)
                                        {

                                            if (count <= dtF.Columns.Count - 1)
                                            {
                                                if ((Regex.Replace(dtF.Columns[count].ToString().Split('-')[0].Split('_')[1], @"[^\d]", String.Empty)) == ans)
                                                {
                                                    dr_dtF[count] = "1";// ans;
                                                    count++;
                                                    optionsCount--;
                                                }
                                                else
                                                {
                                                    dr_dtF[count] = "2";
                                                    count++;
                                                    optionsCount--;
                                                }
                                            }
                                        }
                                        //count = count + optionsCount;
                                    }
                                    else if (questionTypeId == 6) // matrix Type
                                    {
                                        foreach (string ans in allAnswers)
                                        {
                                            dr_dtF[count] = (ans.ToLower().IndexOf("yes") != -1) ? "1" : "2";
                                            count++;
                                            optionsCount--;
                                        }
                                        count = count + optionsCount;
                                    }
                                    else
                                    {
                                        foreach (string ans in allAnswers)
                                        {
                                            dr_dtF[count] = ans;
                                            count++;
                                            optionsCount--;
                                        }
                                        count = count + optionsCount;
                                    }
                                }
                            }
                            else if (questionTypeId == 1) // Single Punch Answers
                            {
                                //List<OptionsModel> SinglePunchAns = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                List<OptionsModel> SinglePunchAns = new List<OptionsModel>();
                                if (PipedFrom > 0 && !drResponse["QuestionTitle"].ToString().Contains("{{Q}}"))
                                {
                                    SinglePunchAns = model.lstQuestionModel.Where(v => v.QuestionId == PipedFrom).FirstOrDefault().lstOptionsModel;
                                }
                                else
                                {
                                    SinglePunchAns = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                }
                                int ansIndex = 0;
                                foreach (var item in SinglePunchAns)
                                {
                                    ansIndex++;
                                    if (Convert.ToString(item.OptionId) == answerExpression)
                                    {
                                        break;
                                    }
                                }

                                dr_dtF[count] = ansIndex;//answerExpression;
                                count++;
                                optionsCount--;
                            }
                            else if (questionTypeId == 8 || questionTypeId == 12) // single punch matrix answers
                            {
                                List<OptionsModel> optionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                List<MatrixTypeModel> columnDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstMatrixTypeModel;
                                int qustionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).Select(v => v.DisplayOrder).FirstOrDefault();
                                string[] allAnswers = answerExpression.Split('|');

                                List<string> lstColumnOrdinals = new List<string>();

                                string formattedAnswer = string.Empty;
                                string answerValue = string.Empty;
                                if (allAnswers.Length > 0)
                                {
                                    foreach (var ansExpression in allAnswers)
                                    {
                                        if (!string.IsNullOrEmpty(ansExpression))
                                        {
                                            string displaycol = string.Empty;
                                            string displayopt = string.Empty;
                                            foreach (var item in optionDisplayOrder)
                                            {

                                                if (Convert.ToString(item.OptionId) == ansExpression.Split('~')[0])
                                                {
                                                    displaycol = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            foreach (var item in columnDisplayOrder)
                                            {
                                                if (Convert.ToString(item.Id) == ansExpression.Split('~')[1])
                                                {
                                                    displayopt = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            //  formattedAnswer = "Q" + questionId + "_M" + displaycol + "#" + displayopt;
                                            formattedAnswer = "Q" + questionId + "_M" + displayopt + "#" + displaycol;
                                            foreach (DataColumn column in dtF.Columns)
                                            {
                                                string[] columnHeader = column.ColumnName.Split('-');
                                                string[] answer = columnHeader[0].Split('_');
                                                if (columnHeader.Length > 1)
                                                {
                                                    string answerInHeader = answer[0] + "_" + answer[1];
                                                    string[] formattedAnswerArr = formattedAnswer.Split('#');
                                                    if (formattedAnswerArr[0] == answerInHeader)
                                                    {
                                                        int index = dr_dtF.Table.Columns[column.ColumnName].Ordinal;
                                                        lstColumnOrdinals.Add(index.ToString());
                                                        count++;
                                                        dr_dtF[index] = formattedAnswerArr[1];
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            else if (questionTypeId == 13) // Kano model matrix type
                            {
                                List<OptionsModel> optionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                List<MatrixTypeModel> columnDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstMatrixTypeModel;
                                int qustionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).Select(v => v.DisplayOrder).FirstOrDefault();
                                string[] allAnswers = answerExpression.Split('|');

                                List<string> lstColumnOrdinals = new List<string>();

                                string formattedAnswer = string.Empty;
                                string answerValue = string.Empty;
                                if (allAnswers.Length > 1)
                                {
                                    int repeatId = 0;
                                    foreach (var ansExpression in allAnswers)
                                    {
                                        string displaycol = string.Empty;
                                        string displayopt = string.Empty;
                                        string displayText = string.Empty;
                                        string text = string.Empty;
                                        if (repeatId == Convert.ToInt32(ansExpression.Split('~')[0]))
                                        {
                                            text = "_Absent";
                                            repeatId = 0;
                                        }
                                        else
                                        {
                                            text = "_Present";
                                        }
                                        foreach (var item in optionDisplayOrder)
                                        {
                                            if (Convert.ToString(item.OptionId) == ansExpression.Split('~')[0])
                                            {
                                                displayText = Convert.ToString(item.Option) + text;
                                                displaycol = Convert.ToString(item.DisplayOrder);
                                                repeatId = Convert.ToInt32(ansExpression.Split('~')[0]);
                                                break;
                                            }
                                        }
                                        foreach (var item in columnDisplayOrder)
                                        {
                                            if (Convert.ToString(item.Id) == ansExpression.Split('~')[1])
                                            {
                                                displayopt = Convert.ToString(item.DisplayOrder);
                                                break;
                                            }
                                        }
                                        //formattedAnswer = "Q" + qustionDisplayOrder + "_M" + displaycol + "-" + displayText + "#" + displayopt;
                                        formattedAnswer = "Q" + questionId + "_M" + displaycol + "-" + displayText + "#" + displayopt;
                                        foreach (DataColumn column in dtF.Columns)
                                        {

                                            string[] columnHeader = column.ColumnName.Split('-');
                                            string[] answer = columnHeader[0].Split('_');
                                            if (columnHeader.Length > 1)
                                            {
                                                string answerInHeader = answer[0] + "_" + answer[1] + "-" + columnHeader[1];
                                                string[] formattedAnswerArr = formattedAnswer.Split('#');
                                                if (Regex.Replace(formattedAnswerArr[0], @"\s+", "", RegexOptions.Multiline) == Regex.Replace(answerInHeader, @"\s+", "", RegexOptions.Multiline))
                                                {
                                                    int index = dr_dtF.Table.Columns[column.ColumnName].Ordinal;
                                                    lstColumnOrdinals.Add(index.ToString());
                                                    count++;
                                                    dr_dtF[index] = formattedAnswerArr[1];
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            else if (questionTypeId == 16) // Description type Question
                            {
                            }
                            else if (questionTypeId > 7 && questionTypeId != 15)
                            {
                                List<OptionsModel> optionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                List<MatrixTypeModel> columnDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstMatrixTypeModel;
                                List<DDLMatrixTypeModel> dropDownDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstDDlMatrixModel;
                                count = count + (optionDisplayOrder.Count * columnDisplayOrder.Count);
                                int qustionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).Select(v => v.DisplayOrder).FirstOrDefault();
                                string[] allAnswers = answerExpression.Split('|');

                                List<string> lstColumnOrdinals = new List<string>();

                                string formattedAnswer = string.Empty;
                                string answerValue = string.Empty;
                                int DDlDisplayOrder = 0;
                                if (allAnswers.Length > 1)
                                {
                                    foreach (var ansExpression in allAnswers)
                                    {
                                        string displaycol = string.Empty;
                                        string displayopt = string.Empty;

                                        if (questionTypeId == 9 || questionTypeId == 14)
                                        {
                                            foreach (var item in optionDisplayOrder)
                                            {
                                                if (Convert.ToString(item.OptionId) == ansExpression.Split('~')[0].Split('*')[0])
                                                {
                                                    displaycol = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            foreach (var item in columnDisplayOrder)
                                            {
                                                if (Convert.ToString(item.Id) == ansExpression.Split('~')[1])
                                                {
                                                    displayopt = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            //formattedAnswer = "Q" + qustionDisplayOrder + "_M" + displaycol + "_C" + displayopt;
                                            formattedAnswer = "Q" + questionId + "_M" + displaycol + "_C" + displayopt;
                                            answerValue = ansExpression.Split('~')[0].Split('*')[1];
                                            if (questionTypeId == 14)
                                                DDlDisplayOrder = dropDownDisplayOrder.Where(l => l.Id == Convert.ToInt32(answerValue)).FirstOrDefault().DDLDisplayOrder;
                                        }
                                        else
                                        {
                                            foreach (var item in optionDisplayOrder)
                                            {
                                                if (Convert.ToString(item.OptionId) == ansExpression.Split('~')[0])
                                                {
                                                    displaycol = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            foreach (var item in columnDisplayOrder)
                                            {
                                                if (Convert.ToString(item.Id) == ansExpression.Split('~')[1])
                                                {
                                                    displayopt = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            //formattedAnswer = "Q" + qustionDisplayOrder + "_M" + displaycol + "_C" + displayopt;
                                            formattedAnswer = "Q" + questionId + "_M" + displaycol + "_C" + displayopt;
                                        }
                                        foreach (DataColumn column in dtF.Columns)
                                        {
                                            string[] columnHeader = column.ColumnName.Split('-');

                                            if (columnHeader.Length > 1)
                                            {
                                                string[] answer = columnHeader[0].Split('_');

                                                if (answer.Length > 2)
                                                {
                                                    string answerInHeader = answer[0] + "_" + answer[1] + "_" + answer[2];

                                                    if (formattedAnswer == answerInHeader)
                                                    {
                                                        int index = dr_dtF.Table.Columns[column.ColumnName].Ordinal;
                                                        lstColumnOrdinals.Add(index.ToString());
                                                        //count++;
                                                        if (questionTypeId == 9)
                                                        {
                                                            dr_dtF[index] = answerValue;
                                                        }
                                                        else if (questionTypeId == 14)
                                                        {
                                                            dr_dtF[index] = DDlDisplayOrder;
                                                        }
                                                        else
                                                        {
                                                            dr_dtF[index] = "1";
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }

                                // Fill All the Empty cells with 2

                                for (int i = 0; i <= dtF.Columns.Count - 1; i++)
                                    if (String.IsNullOrEmpty(Convert.ToString(dr_dtF[i])))
                                        dr_dtF[i] = "";
                            }

                            else
                            {
                                dr_dtF[count] = answerExpression;
                                count++;
                                optionsCount--;
                            }



                            if (productId != oldProductID)
                            {
                                dtF.Rows.Add(drTemp);
                                drTemp[0] = response_id;
                                oldProductID = productId;
                            }

                            drTemp = dr_dtF;
                            //---ss
                            if (model.projectModel.ProjectType == "2")
                            {
                                drTemp[2] = product_desc;
                                drTemp[1] = product_logiId;
                            }
                            //---ss


                        }


                        dtF.Rows.Add(dr_dtF);
                        drTemp[0] = response_id;
                        //---ss
                        if (model.projectModel.ProjectType == "2")
                        {
                            dr_dtF[2] = product_desc;
                            dr_dtF[1] = product_logiId;
                        }
                        response_id++;
                        //---ss
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return dtF;
        }
        #endregion

        [HttpPost]
        public ActionResult ExportToCSV()
        {
            try
            {
                var fileName = DateTime.Now.Ticks.ToString();
                var csvContent = Request.Form["csv_text"];
                System.Web.HttpContext.Current.Response.Clear();

                var format = Request.Form["csv_format"];
                if (format == "1")
                {
                    System.Web.HttpContext.Current.Response.ContentType = "text/plain";
                    System.Web.HttpContext.Current.Response.AddHeader("content-disposition",
                        "attachment;filename=" + fileName + ".txt");
                }
                else
                {
                    System.Web.HttpContext.Current.Response.ContentType = "text/csv";
                    System.Web.HttpContext.Current.Response.AddHeader("content-disposition",
                        "attachment;filename=" + fileName + ".csv");
                }
                System.Web.HttpContext.Current.Response.Charset = "";
                System.Web.HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
                System.Web.HttpContext.Current.Response.Write(csvContent);
            }
            catch (Exception ex)
            {
            }
            finally
            {
                System.Web.HttpContext.Current.Response.End();
            }
            return View();
        }

        //------------------Basic type Survey

        #region GetSinglePunchBasicQuestion2 [Question Type 1]
        [HttpPost]
        [ValidateInput(false)]
        public JsonResult GetSinglePunchBasicQuestion2(int QuestionId, string OptArray)
        {
            System.IO.StringReader theReader = new System.IO.StringReader(OptArray);
            DataSet dataSet = new DataSet();
            dataSet.ReadXml(theReader);
            DataTable dtOptions = new DataTable();
            dtOptions = dataSet.Tables[0];
            string json = string.Empty;
            string temp = string.Empty;
            string temp1 = string.Empty;

            if (Session["ReportDataTable"] != null)
            {
                DataTable dtReport = (DataTable)Session["ReportDataTable"];
                foreach (DataRow dr in dtOptions.Rows)
                {
                    string colNameFormat = "Q" + Convert.ToString(QuestionId);
                    string[] cols = (from dc in dtReport.Columns.Cast<DataColumn>()
                                     where dc.ColumnName.Contains(colNameFormat)
                                     select dc.ColumnName)
                                    .ToArray();

                    DataView view = new DataView(dtReport);
                    var selected = view.ToTable(false, cols).AsEnumerable().Where(x => x.Field<string>(cols[0].ToString()) == Convert.ToString(dr[0])).Count();
                    temp += "{\"Option\":\"" + Convert.ToString(dr[0]) + "\",\"Count\":\"" + selected.ToString() + "\"},";
                }
            }
            json = "[" + temp.TrimEnd(',') + "]";
            return Json(json, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region GetSinglePunchBasicQuestion [Question Type 1]
        [HttpPost]
        [ValidateInput(false)]
        public JsonResult GetSinglePunchBasicQuestion(int QuestionId, string OptArray)
        {
            System.IO.StringReader theReader = new System.IO.StringReader(OptArray);
            DataSet dataSet = new DataSet();
            dataSet.ReadXml(theReader);
            DataTable dtOptions = new DataTable();
            dtOptions = dataSet.Tables[0];
            string json = string.Empty;
            string temp = string.Empty;

            foreach (DataRow dr in dtOptions.Rows)
            {
                string[] optionArray = { Convert.ToString(dr[0]) };
                string colNameFormat = "Q" + Convert.ToString(QuestionId);
                if (Session["ReportDataTable"] != null)
                {
                    DataTable dtReport = (DataTable)Session["ReportDataTable"];
                    string[] colNames = dtReport.Columns.Cast<DataColumn>().Where(x => x.ColumnName.Contains(colNameFormat)).Select(x => x.ColumnName).ToArray();

                    var query = (dtReport.AsEnumerable().Count(row => row.Field<string>(colNames[0]) == Convert.ToString(dr[0])));
                    temp += "{\"Option\":\"" + Convert.ToString(dr[0]) + "\",\"Count\":\"" + query + "\"},";
                }
            }
            json = "[" + temp.TrimEnd(',') + "]";
            return Json(json, JsonRequestBehavior.AllowGet);
        }
        #endregion


        #region GetMultiPunchBasicQuestion2 [Question Type 2] [Question Type 6] [Question Type 11]
        [HttpPost]
        [ValidateInput(false)]
        public JsonResult GetMultiPunchBasicQuestion2(int QuestionId, string OptArray)
        {
            System.IO.StringReader theReader = new System.IO.StringReader(OptArray);
            DataSet dataSet = new DataSet();
            dataSet.ReadXml(theReader);
            DataTable dtOptions = new DataTable();
            dtOptions = dataSet.Tables[0];
            string json = string.Empty;
            string temp = string.Empty;

            if (Session["ReportDataTable"] != null)
            {
                DataTable dtReport = (DataTable)Session["ReportDataTable"];
                foreach (DataRow dr in dtOptions.Rows)
                {
                    string colNameFormat = dr[0].ToString();
                    string[] cols = (from dc in dtReport.Columns.Cast<DataColumn>()
                                     where dc.ColumnName.Contains(colNameFormat)
                                     select dc.ColumnName)
                                    .ToArray();

                    DataView view = new DataView(dtReport);
                    var selected = view.ToTable(false, cols).AsEnumerable().Where(x => x.Field<string>(cols[0].ToString()) == "1").Count();
                    temp += "{\"Option\":\"" + Convert.ToString(dr[0]) + "\",\"Count\":\"" + selected.ToString() + "\"},";
                }
            }


            json = "[" + temp.TrimEnd(',') + "]";
            return Json(json, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region GetMultiPunchBasicQuestion [Question Type 2] [Question Type 6] [Question Type 11]
        [HttpPost]
        [ValidateInput(false)]
        public JsonResult GetMultiPunchBasicQuestion(int QuestionId, string OptArray)
        {
            System.IO.StringReader theReader = new System.IO.StringReader(OptArray);
            DataSet dataSet = new DataSet();
            dataSet.ReadXml(theReader);
            DataTable dtOptions = new DataTable();
            dtOptions = dataSet.Tables[0];
            string json = string.Empty;
            string temp = string.Empty;

            foreach (DataRow dr in dtOptions.Rows)
            {
                string colNameFormat = "Q" + Convert.ToString(QuestionId);
                string[] optionArray = { Convert.ToString(dr[0]) };//OptArray.TrimEnd(',').Split(',');
                if (Session["ReportDataTable"] != null)
                {
                    DataTable dtReport = (DataTable)Session["ReportDataTable"];
                    string[] colNames = dtReport.Columns.Cast<DataColumn>().Where(x => x.ColumnName.Contains(colNameFormat)).Select(x => x.ColumnName).ToArray();
                    List<string> colNamesToBeMatched = new List<string>();

                    foreach (var colName in colNames)
                    {
                        foreach (var optName in optionArray)
                        {
                            if ((colName.IndexOf(optName) != -1))
                            {
                                colNamesToBeMatched.Add(colName);
                            }
                        }
                    }

                    colNames = colNamesToBeMatched.ToArray();

                    var query = dtReport.AsEnumerable().Count(row => row.Field<string>(colNames[0]) == "1");

                    temp += "{\"Option\":\"" + Convert.ToString(dr[0]) + "\",\"Count\":\"" + query + "\"},";
                }
            }


            json = "[" + temp.TrimEnd(',') + "]";
            return Json(json, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region GetMeansOfRangeBasicQuestion [Question Type 3]
        //public JsonResult GetMeansOfRangeBasicQuestion(int QuestionId)
        //{
        //    string colName = "Q" + Convert.ToString(QuestionId);
        //    string json = string.Empty;
        //    string temp = string.Empty;
        //    if (Session["ReportDataTable"] != null)
        //    {
        //        DataTable dtReport = (DataTable)Session["ReportDataTable"];
        //        string[] colNames = dtReport.Columns.Cast<DataColumn>().Where(x => x.ColumnName.Contains(colName)).Select(x => x.ColumnName).ToArray();
        //        //var res = (from m in dtReport.AsEnumerable() where m.Field<string>("Code") == "1-1-12" select m).FirstOrDefault();
        //        var query = (from row in dtReport.AsEnumerable()
        //                     group row by row.Field<string>(colNames[0]) into grp
        //                     select new
        //                     {
        //                         Ans = String.Join(",", grp.Select(row => row.Field<string>(colNames[0])).ToArray()),
        //                         count = grp.Count()
        //                     });
        //        foreach (var item in query)
        //        {
        //            temp += "{\"Ans\":\"" + item.Ans + "\",\"Frequency\":\"" + item.count + "\"},";
        //        }
        //        temp = "{\"List\":[" + temp.TrimEnd(',') + "]}";
        //        json = temp;
        //    }
        //    return Json(json, JsonRequestBehavior.AllowGet);
        //}
        public JsonResult GetMeansOfRangeBasicQuestion(int QuestionId)
        {
            string colName = "Q" + Convert.ToString(QuestionId);
            string json = string.Empty;
            string temp = string.Empty;
            if (Session["ReportDataTable"] != null)
            {
                DataTable dtReport = (DataTable)Session["ReportDataTable"];
                string[] colNames = dtReport.Columns.Cast<DataColumn>().Where(x => x.ColumnName.Contains(colName)).Select(x => x.ColumnName).ToArray();

                var query = dtReport.AsEnumerable().Sum((r) => decimal.Parse(r[colNames[0]].ToString()));
                temp = "{\"Sum\":\"" + query + "\"}";
                json = temp;
            }
            return Json(json, JsonRequestBehavior.AllowGet);
        }
        #endregion


        #region GetMeansOfConstantSumAndRankingBasicQuestion [Question Type 4] [Question Type 5]
        [HttpPost]
        [ValidateInput(false)]
        public JsonResult GetMeansOfConstantSumAndRankingBasicQuestion(int QuestionId, string OptArray)
        {
            System.IO.StringReader theReader = new System.IO.StringReader(OptArray);
            DataSet dataSet = new DataSet();
            dataSet.ReadXml(theReader);
            DataTable dtOptions = new DataTable();
            dtOptions = dataSet.Tables[0];
            string json = string.Empty;
            string temp = string.Empty;

            foreach (DataRow dr in dtOptions.Rows)
            {
                string tempList = string.Empty;
                string colNameFormat = "Q" + Convert.ToString(QuestionId);
                string[] optionArray = { Convert.ToString(dr[0]) };//OptArray.TrimEnd(',').Split(',');
                if (Session["ReportDataTable"] != null)
                {
                    DataTable dtReport = (DataTable)Session["ReportDataTable"];
                    string[] colNames = dtReport.Columns.Cast<DataColumn>().Where(x => x.ColumnName.Contains(colNameFormat)).Select(x => x.ColumnName).ToArray();
                    List<string> colNamesToBeMatched = new List<string>();

                    foreach (var colName in colNames)
                    {
                        foreach (var optName in optionArray)
                        {
                            if ((colName.IndexOf(optName) != -1))
                            {
                                colNamesToBeMatched.Add(colName);
                            }
                        }
                    }

                    colNames = colNamesToBeMatched.ToArray();

                    var query = (from row in dtReport.AsEnumerable()
                                 group row by row.Field<string>(colNames[0]) into grp
                                 select new
                                 {
                                     Ans = String.Join(",", grp.Select(row => row.Field<string>(colNames[0])).ToArray()),
                                     count = grp.Count()
                                 });
                    foreach (var item in query)
                    {
                        var answerArray = item.Ans.Split(',');
                        decimal sum = 0;
                        foreach (var ans in answerArray)
                        {
                            sum += Convert.ToDecimal(ans);
                        }

                        tempList += "{\"Ans\":\"" + sum + "\",\"Frequency\":\"" + item.count + "\"},";
                    }
                    temp += "{\"Option\":\"" + Convert.ToString(dr[0]) + "\",\"List\":[" + tempList.TrimEnd(',') + "]},";
                }
            }
            json = "[" + temp.TrimEnd(',') + "]";
            return Json(json, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region GetSinglePunchMatrixTypeBasicQuestion [Question Type 8]
        public JsonResult GetSinglePunchMatrixTypeBasicQuestion(int QuestionId, string OptArray)
        {
            System.IO.StringReader theReader = new System.IO.StringReader(OptArray);
            DataSet dataSet = new DataSet();
            dataSet.ReadXml(theReader);
            DataTable dtOptions = new DataTable();
            dtOptions = dataSet.Tables[0];
            string json = string.Empty;
            string temp = string.Empty;

            foreach (DataRow dr in dtOptions.Rows)
            {
                string colNameFormat = "Q" + Convert.ToString(QuestionId);
                string[] optArr = Convert.ToString(dr[0]).Split(',');
                string[] optionArray = { optArr[0] != null ? Convert.ToString(optArr[0]) : "" };//OptArray.TrimEnd(',').Split(',');
                int res = 0;

                if (Session["ReportDataTable"] != null)
                {
                    string optionName = optArr[0].Split('-')[1];
                    DataTable dtReport = (DataTable)Session["ReportDataTable"];
                    string[] colNames = dtReport.Columns.Cast<DataColumn>().Where(x => x.ColumnName.Contains(colNameFormat) && x.ColumnName.Contains(optionName)).Select(x => x.ColumnName).ToArray();


                    //string[] colNames = dtReport.Columns.Cast<DataColumn>().Where(x => x.ColumnName.Contains(colNameFormat)).Select(x => x.ColumnName).ToArray();
                    List<string> colNamesToBeMatched = new List<string>();

                    //foreach (var colName in colNames)
                    //{
                    //    foreach (var optName in optionArray)
                    //    {
                    //        if ((colName.IndexOf(optName) != -1))
                    //        {
                    //            colNamesToBeMatched.Add(colName);
                    //        }
                    //    }
                    //}
                    //colNames = colNamesToBeMatched.ToArray();
                    foreach (var colName in colNames)
                    {
                        var query = dtReport.AsEnumerable().Count(row => row.Field<string>(colName) == Convert.ToString(optArr[1]));
                        res += query;
                    }
                    temp += "{\"Option\":\"" + Convert.ToString(optArr[0]) + "\",\"Count\":\"" + res + "\"},";

                }
            }
            json = "[" + temp.TrimEnd(',') + "]";
            return Json(json, JsonRequestBehavior.AllowGet);
        }
        #endregion


        #region GetReportOfRangeMatrixTypeBasicQuestion [Question Type 9]
        [HttpPost]
        [ValidateInput(false)]
        public JsonResult GetReportOfRangeMatrixTypeBasicQuestion(int QuestionId, string OptArray)
        {
            System.IO.StringReader theReader = new System.IO.StringReader(OptArray);
            DataSet dataSet = new DataSet();
            dataSet.ReadXml(theReader);
            DataTable dtOptions = new DataTable();
            dtOptions = dataSet.Tables[0];
            string json = string.Empty;
            string temp = string.Empty;

            foreach (DataRow dr in dtOptions.Rows)
            {
                string colNameFormat = "Q" + Convert.ToString(QuestionId);
                string[] optArr = Convert.ToString(dr[0]).Split(',');
                string[] optionArray = { optArr[0] != null ? Convert.ToString(optArr[0]) : "" };//OptArray.TrimEnd(',').Split(',');

                //string[] optionArray = { Convert.ToString(dr[0]) };//OptArray.TrimEnd(',').Split(',');

                if (Session["ReportDataTable"] != null)
                {
                    DataTable dtReport = (DataTable)Session["ReportDataTable"];
                    string[] colNames = dtReport.Columns.Cast<DataColumn>().Where(x => x.ColumnName.Contains(colNameFormat)).Select(x => x.ColumnName).ToArray();
                    List<string> colNamesToBeMatched = new List<string>();

                    foreach (var colName in colNames)
                    {
                        foreach (var optName in optionArray)
                        {
                            if ((colName.IndexOf(optName) != -1))
                            {
                                colNamesToBeMatched.Add(colName);
                            }
                        }
                    }
                    colNames = colNamesToBeMatched.ToArray();
                    int sumVal = 0;
                    foreach (var item in colNames)
                    {
                        var query = dtReport.AsEnumerable().Sum(r => decimal.Parse((!string.IsNullOrEmpty(r[item].ToString()) ? r[item].ToString() : "0")));
                        sumVal += Convert.ToInt32(query);
                    }
                    temp += "{\"Option\":\"" + Convert.ToString(optArr[0]) + "\",\"Sum\":\"" + sumVal + "\"},";
                }
            }
            json = "[" + temp.TrimEnd(',') + "]";
            return Json(json, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region GetReportOfRankOrderAndSinglePunchHorizontalTypeBasicQuestion [Question Type 10] [Question Type 12]
        [HttpPost]
        [ValidateInput(false)]
        public JsonResult GetReportOfRankOrderAndSinglePunchHorizontalTypeBasicQuestion(int QuestionId, string OptArray)
        {
            System.IO.StringReader theReader = new System.IO.StringReader(OptArray);
            DataSet dataSet = new DataSet();
            dataSet.ReadXml(theReader);
            DataTable dtOptions = new DataTable();
            dtOptions = dataSet.Tables[0];
            string json = string.Empty;
            string temp = string.Empty;

            foreach (DataRow dr in dtOptions.Rows)
            {
                string colNameFormat = "Q" + Convert.ToString(QuestionId);
                string[] optionArray = { Convert.ToString(dr[0]) };//OptArray.TrimEnd(',').Split(',');
                int res = 0;
                if (Session["ReportDataTable"] != null)
                {
                    DataTable dtReport = (DataTable)Session["ReportDataTable"];
                    string[] colNames = dtReport.Columns.Cast<DataColumn>().Where(x => x.ColumnName.Contains(colNameFormat)).Select(x => x.ColumnName).ToArray();
                    List<string> colNamesToBeMatched = new List<string>();

                    foreach (var colName in colNames)
                    {
                        foreach (var optName in optionArray)
                        {
                            if ((colName.IndexOf(optName) != -1))
                            {
                                colNamesToBeMatched.Add(colName);
                            }
                        }
                    }
                    colNames = colNamesToBeMatched.ToArray();
                    foreach (var colName in colNames)
                    {
                        var query = dtReport.AsEnumerable().Count(row => row.Field<string>(colName) == "1");
                        res += query;
                    }
                    temp += "{\"Option\":\"" + Convert.ToString(dr[0]) + "\",\"Count\":\"" + res + "\"},";
                }
            }
            json = "[" + temp.TrimEnd(',') + "]";
            return Json(json, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region GetReportOfDropdownMatrixTypeBasicQuestion [Question Type 14]
        [HttpPost]
        [ValidateInput(false)]
        public JsonResult GetReportOfDropdownMatrixTypeBasicQuestion(int QuestionId, string OptArray)
        {
            System.IO.StringReader theReader = new System.IO.StringReader(OptArray);
            DataSet dataSet = new DataSet();
            dataSet.ReadXml(theReader);
            DataTable dtOptions = new DataTable();
            dtOptions = dataSet.Tables[0];
            string json = string.Empty;
            string temp = string.Empty;

            foreach (DataRow dr in dtOptions.Rows)
            {

                string colNameFormat = "Q" + Convert.ToString(QuestionId);
                string[] optionArray = { Convert.ToString(dr[0]).Split('-')[0] };
                if (Session["ReportDataTable"] != null)
                {
                    DataTable dtReport = (DataTable)Session["ReportDataTable"];
                    string[] colNames = dtReport.Columns.Cast<DataColumn>().Where(x => x.ColumnName.Contains(colNameFormat)).Select(x => x.ColumnName).ToArray();
                    List<string> colNamesToBeMatched = new List<string>();

                    foreach (var colName in colNames)
                    {
                        foreach (var optName in optionArray)
                        {
                            if ((colName.IndexOf(optName) != -1))
                            {
                                colNamesToBeMatched.Add(colName);
                            }
                        }
                    }
                    colNames = colNamesToBeMatched.ToArray();
                    var query = dtReport.AsEnumerable().Count(row => row.Field<string>(colNames[0]) == Convert.ToString(dr[0]).Split('-')[1]);
                    temp += "{\"Option\":\"" + Convert.ToString(dr[0]) + "\",\"Count\":\"" + query + "\"},";
                }
            }
            json = "[" + temp.TrimEnd(',') + "]";
            return Json(json, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region RangeTypeQuestionVariance
        //public JsonResult RangeTypeQuestionVariance(int QuestionId)
        //{
        //    string json = string.Empty;
        //    string colName = "Q" + Convert.ToString(QuestionId);
        //    if (Session["ReportDataTable"] != null)
        //    {
        //        DataTable dtReport = (DataTable)Session["ReportDataTable"];
        //        string[] colNames = dtReport.Columns.Cast<DataColumn>().Where(x => x.ColumnName.Contains(colName)).Select(x => x.ColumnName).ToArray();
        //        //var res = (from m in dtReport.AsEnumerable() where m.Field<string>("Code") == "1-1-12" select m).FirstOrDefault();

        //        var query = (from row in dtReport.AsEnumerable()
        //                     group row by row.Field<string>("Product_LogicalId") into grp
        //                     select new
        //                     {
        //                         RangeVal = String.Join(",", grp.Select(row => row.Field<string>(colNames[0])).ToArray()),
        //                     }).ToList();
        //        json = Newtonsoft.Json.JsonConvert.SerializeObject(query);

        //    }
        //    return Json(json, JsonRequestBehavior.AllowGet);
        //}

        public JsonResult RangeTypeQuestionVariance(int QuestionId)
        {
            string json = string.Empty;
            string colName = "Q" + Convert.ToString(QuestionId);
            if (Session["ReportDataTable"] != null)
            {
                DataTable dtReport = (DataTable)Session["ReportDataTable"];
                string[] colNames = dtReport.Columns.Cast<DataColumn>().Where(x => x.ColumnName.Contains(colName)).Select(x => x.ColumnName).ToArray();
                //var res = (from m in dtReport.AsEnumerable() where m.Field<string>("Code") == "1-1-12" select m).FirstOrDefault();

                var query = (from row in dtReport.AsEnumerable()
                             group row by row.Field<string>("Product_LogicalId") into grp
                             select new
                             {
                                 Sum = grp.Sum((r) => decimal.Parse(!string.IsNullOrEmpty(r[colNames[0]].ToString()) ? r[colNames[0]].ToString() : "0")),
                                 SquareMean = grp.Sum((r) => decimal.Parse(!string.IsNullOrEmpty(r[colNames[0]].ToString()) ? r[colNames[0]].ToString() : "0") * decimal.Parse(!string.IsNullOrEmpty(r[colNames[0]].ToString()) ? r[colNames[0]].ToString() : "0"))
                             }).ToList();
                json = Newtonsoft.Json.JsonConvert.SerializeObject(query);

            }
            return Json(json, JsonRequestBehavior.AllowGet);
        }
        #endregion

        //#region RangeTypeQuestionVariance1
        ////public JsonResult RangeTypeQuestionVariance(int QuestionId)
        ////{
        ////    string json = string.Empty;
        ////    string colName = "Q" + Convert.ToString(QuestionId);
        ////    if (Session["ReportDataTable"] != null)
        ////    {
        ////        DataTable dtReport = (DataTable)Session["ReportDataTable"];
        ////        string[] colNames = dtReport.Columns.Cast<DataColumn>().Where(x => x.ColumnName.Contains(colName)).Select(x => x.ColumnName).ToArray();
        ////        //var res = (from m in dtReport.AsEnumerable() where m.Field<string>("Code") == "1-1-12" select m).FirstOrDefault();

        ////        var query = (from row in dtReport.AsEnumerable()
        ////                     group row by row.Field<string>("Product_LogicalId") into grp
        ////                     select new
        ////                     {
        ////                         RangeVal = String.Join(",", grp.Select(row => row.Field<string>(colNames[0])).ToArray()),
        ////                     }).ToList();
        ////        json = Newtonsoft.Json.JsonConvert.SerializeObject(query);

        ////    }
        ////    return Json(json, JsonRequestBehavior.AllowGet);
        ////}

        //public JsonResult RangeTypeQuestionVariance1(int QuestionId, int caseCount)
        //{
        //    string json = string.Empty;
        //    string colName = "Q" + Convert.ToString(QuestionId);
        //    if (Session["CaseContainer"] != null)
        //    {
        //        int column = 1;
        //        DataTable MainDt = (DataTable)Session["CaseContainer"];
        //        if (Session["ReportType"].ToString() == "Online")
        //        {
        //            column = 1;
        //        }
        //        else if (Session["ReportType"].ToString() == "Offline")
        //        {
        //            column = 2;
        //        }
        //        else
        //        {
        //            column = 3;
        //        }
        //        DataTable dtReport = (DataTable)MainDt.Rows[caseCount][column];
        //        string[] colNames = dtReport.Columns.Cast<DataColumn>().Where(x => x.ColumnName.Contains(colName)).Select(x => x.ColumnName).ToArray();
        //        //var res = (from m in dtReport.AsEnumerable() where m.Field<string>("Code") == "1-1-12" select m).FirstOrDefault();

        //        var query = (from row in dtReport.AsEnumerable()
        //                     group row by row.Field<string>("Product_LogicalId") into grp
        //                     select new
        //                     {
        //                         Sum = grp.Sum((r) => decimal.Parse(!string.IsNullOrEmpty(r[colNames[0]].ToString()) ? r[colNames[0]].ToString() : "0")),
        //                         SquareMean = grp.Sum((r) => decimal.Parse(!string.IsNullOrEmpty(r[colNames[0]].ToString()) ? r[colNames[0]].ToString() : "0") * decimal.Parse(!string.IsNullOrEmpty(r[colNames[0]].ToString()) ? r[colNames[0]].ToString() : "0"))
        //                     }).ToList();
        //        json = Newtonsoft.Json.JsonConvert.SerializeObject(query);

        //    }
        //    return Json(json, JsonRequestBehavior.AllowGet);
        //}
        //#endregion

        #region GetSinglePunchBasicQuestion1 [Question Type 1]
        [HttpPost]
        [ValidateInput(false)]
        public JsonResult GetSinglePunchBasicQuestion1(int QuestionId, string OptArray, int caseCount)
        {
            System.IO.StringReader theReader = new System.IO.StringReader(OptArray);
            DataSet dataSet = new DataSet();
            dataSet.ReadXml(theReader);
            DataTable dtOptions = new DataTable();
            dtOptions = dataSet.Tables[0];
            string json = string.Empty;
            string temp = string.Empty;

            foreach (DataRow dr in dtOptions.Rows)
            {
                string[] optionArray = { Convert.ToString(dr[0]) };
                string colNameFormat = "Q" + Convert.ToString(QuestionId);
                if (Session["CaseContainer"] != null)
                {
                    int column = 1;
                    DataTable MainDt = (DataTable)Session["CaseContainer"];
                    if (Session["ReportType"].ToString() == "Online")
                    {
                        column = 1;
                    }
                    else if (Session["ReportType"].ToString() == "Offline")
                    {
                        column = 2;
                    }
                    else
                    {
                        column = 3;
                    }
                    DataTable dtReport = (DataTable)MainDt.Rows[caseCount][column];
                    string[] colNames = dtReport.Columns.Cast<DataColumn>().Where(x => x.ColumnName.Contains(colNameFormat)).Select(x => x.ColumnName).ToArray();

                    var query = (dtReport.AsEnumerable().Count(row => row.Field<string>(colNames[0]) == Convert.ToString(dr[0])));
                    temp += "{\"Option\":\"" + Convert.ToString(dr[0]) + "\",\"Count\":\"" + query + "\"},";
                }
            }
            json = "[" + temp.TrimEnd(',') + "]";
            return Json(json, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region GetMultiPunchBasicQuestion1 [Question Type 2] [Question Type 6] [Question Type 11]
        [HttpPost]
        [ValidateInput(false)]
        public JsonResult GetMultiPunchBasicQuestion1(int QuestionId, string OptArray, int caseCount)
        {
            System.IO.StringReader theReader = new System.IO.StringReader(OptArray);
            DataSet dataSet = new DataSet();
            dataSet.ReadXml(theReader);
            DataTable dtOptions = new DataTable();
            dtOptions = dataSet.Tables[0];
            string json = string.Empty;
            string temp = string.Empty;

            foreach (DataRow dr in dtOptions.Rows)
            {
                string colNameFormat = "Q" + Convert.ToString(QuestionId);
                string[] optionArray = { Convert.ToString(dr[0]) };//OptArray.TrimEnd(',').Split(',');
                if (Session["CaseContainer"] != null)
                {
                    int column = 1;
                    DataTable MainDt = (DataTable)Session["CaseContainer"];
                    if (Session["ReportType"].ToString() == "Online")
                    {
                        column = 1;
                    }
                    else if (Session["ReportType"].ToString() == "Offline")
                    {
                        column = 2;
                    }
                    else
                    {
                        column = 3;
                    }
                    DataTable dtReport = (DataTable)MainDt.Rows[caseCount][column];
                    string[] colNames = dtReport.Columns.Cast<DataColumn>().Where(x => x.ColumnName.Contains(colNameFormat)).Select(x => x.ColumnName).ToArray();
                    List<string> colNamesToBeMatched = new List<string>();

                    foreach (var colName in colNames)
                    {
                        foreach (var optName in optionArray)
                        {
                            if ((colName.IndexOf(optName) != -1))
                            {
                                colNamesToBeMatched.Add(colName);
                            }
                        }
                    }

                    colNames = colNamesToBeMatched.ToArray();

                    var query = dtReport.AsEnumerable().Count(row => row.Field<string>(colNames[0]) == "1");

                    temp += "{\"Option\":\"" + Convert.ToString(dr[0]) + "\",\"Count\":\"" + query + "\"},";
                }
            }
            json = "[" + temp.TrimEnd(',') + "]";
            return Json(json, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region GetMeansOfRangeBasicQuestion1 [Question Type 3]
        //public JsonResult GetMeansOfRangeBasicQuestion(int QuestionId)
        //{
        //    string colName = "Q" + Convert.ToString(QuestionId);
        //    string json = string.Empty;
        //    string temp = string.Empty;
        //    if (Session["ReportDataTable"] != null)
        //    {
        //        DataTable dtReport = (DataTable)Session["ReportDataTable"];
        //        string[] colNames = dtReport.Columns.Cast<DataColumn>().Where(x => x.ColumnName.Contains(colName)).Select(x => x.ColumnName).ToArray();
        //        //var res = (from m in dtReport.AsEnumerable() where m.Field<string>("Code") == "1-1-12" select m).FirstOrDefault();
        //        var query = (from row in dtReport.AsEnumerable()
        //                     group row by row.Field<string>(colNames[0]) into grp
        //                     select new
        //                     {
        //                         Ans = String.Join(",", grp.Select(row => row.Field<string>(colNames[0])).ToArray()),
        //                         count = grp.Count()
        //                     });
        //        foreach (var item in query)
        //        {
        //            temp += "{\"Ans\":\"" + item.Ans + "\",\"Frequency\":\"" + item.count + "\"},";
        //        }
        //        temp = "{\"List\":[" + temp.TrimEnd(',') + "]}";
        //        json = temp;
        //    }
        //    return Json(json, JsonRequestBehavior.AllowGet);
        //}
        public JsonResult GetMeansOfRangeBasicQuestion1(int QuestionId, int caseCount)
        {
            string colName = "Q" + Convert.ToString(QuestionId);
            string json = string.Empty;
            string temp = string.Empty;
            if (Session["CaseContainer"] != null)
            {
                int column = 1;
                DataTable MainDt = (DataTable)Session["CaseContainer"];
                if (Session["ReportType"].ToString() == "Online")
                {
                    column = 1;
                }
                else if (Session["ReportType"].ToString() == "Offline")
                {
                    column = 2;
                }
                else
                {
                    column = 3;
                }


                DataTable dtReport = (DataTable)MainDt.Rows[caseCount][column];
                string[] colNames = dtReport.Columns.Cast<DataColumn>().Where(x => x.ColumnName.Contains(colName)).Select(x => x.ColumnName).ToArray();

                var query = dtReport.AsEnumerable().Sum((r) => decimal.Parse(r[colNames[0]].ToString()));
                temp = "{\"Sum\":\"" + query + "\"}";
                json = temp;
            }
            return Json(json, JsonRequestBehavior.AllowGet);
        }
        #endregion


        #region GetMeansOfConstantSumAndRankingBasicQuestion1 [Question Type 4] [Question Type 5]
        [HttpPost]
        [ValidateInput(false)]
        public JsonResult GetMeansOfConstantSumAndRankingBasicQuestion1(int QuestionId, string OptArray, int caseCount)
        {
            System.IO.StringReader theReader = new System.IO.StringReader(OptArray);
            DataSet dataSet = new DataSet();
            dataSet.ReadXml(theReader);
            DataTable dtOptions = new DataTable();
            dtOptions = dataSet.Tables[0];
            string json = string.Empty;
            string temp = string.Empty;

            foreach (DataRow dr in dtOptions.Rows)
            {
                string tempList = string.Empty;
                string colNameFormat = "Q" + Convert.ToString(QuestionId);
                string[] optionArray = { Convert.ToString(dr[0]) };//OptArray.TrimEnd(',').Split(',');
                if (Session["CaseContainer"] != null)
                {
                    int column = 1;
                    DataTable MainDt = (DataTable)Session["CaseContainer"];
                    if (Session["ReportType"].ToString() == "Online")
                    {
                        column = 1;
                    }
                    else if (Session["ReportType"].ToString() == "Offline")
                    {
                        column = 2;
                    }
                    else
                    {
                        column = 3;
                    }
                    DataTable dtReport = (DataTable)MainDt.Rows[caseCount][column];
                    string[] colNames = dtReport.Columns.Cast<DataColumn>().Where(x => x.ColumnName.Contains(colNameFormat)).Select(x => x.ColumnName).ToArray();
                    List<string> colNamesToBeMatched = new List<string>();

                    foreach (var colName in colNames)
                    {
                        foreach (var optName in optionArray)
                        {
                            if ((colName.IndexOf(optName) != -1))
                            {
                                colNamesToBeMatched.Add(colName);
                            }
                        }
                    }

                    colNames = colNamesToBeMatched.ToArray();

                    var query = (from row in dtReport.AsEnumerable()
                                 group row by row.Field<string>(colNames[0]) into grp
                                 select new
                                 {
                                     Ans = String.Join(",", grp.Select(row => row.Field<string>(colNames[0])).ToArray()),
                                     count = grp.Count()
                                 });
                    foreach (var item in query)
                    {
                        var answerArray = item.Ans.Split(',');
                        decimal sum = 0;
                        foreach (var ans in answerArray)
                        {
                            sum += Convert.ToDecimal(ans);
                        }
                        tempList += "{\"Ans\":\"" + sum + "\",\"Frequency\":\"" + item.count + "\"},";
                    }
                    temp += "{\"Option\":\"" + Convert.ToString(dr[0]) + "\",\"List\":[" + tempList.TrimEnd(',') + "]},";
                }
            }
            json = "[" + temp.TrimEnd(',') + "]";
            return Json(json, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region GetSinglePunchMatrixTypeBasicQuestion1 [Question Type 8]
        public JsonResult GetSinglePunchMatrixTypeBasicQuestion1(int QuestionId, string OptArray, int caseCount)
        {
            System.IO.StringReader theReader = new System.IO.StringReader(OptArray);
            DataSet dataSet = new DataSet();
            dataSet.ReadXml(theReader);
            DataTable dtOptions = new DataTable();
            dtOptions = dataSet.Tables[0];
            string json = string.Empty;
            string temp = string.Empty;

            foreach (DataRow dr in dtOptions.Rows)
            {
                string colNameFormat = "Q" + Convert.ToString(QuestionId);
                string[] optArr = Convert.ToString(dr[0]).Split(',');
                string[] optionArray = { optArr[0] != null ? Convert.ToString(optArr[0]) : "" };//OptArray.TrimEnd(',').Split(',');
                int res = 0;

                if (Session["CaseContainer"] != null)
                {
                    int column = 1;
                    DataTable MainDt = (DataTable)Session["CaseContainer"];
                    if (Session["ReportType"].ToString() == "Online")
                    {
                        column = 1;
                    }
                    else if (Session["ReportType"].ToString() == "Offline")
                    {
                        column = 2;
                    }
                    else
                    {
                        column = 3;
                    }
                    string optionName = optArr[0].Split('-')[1];
                    DataTable dtReport = (DataTable)MainDt.Rows[caseCount][column];
                    string[] colNames = dtReport.Columns.Cast<DataColumn>().Where(x => x.ColumnName.Contains(colNameFormat) && x.ColumnName.Contains(optionName)).Select(x => x.ColumnName).ToArray();
                    List<string> colNamesToBeMatched = new List<string>();

                    //foreach (var colName in colNames)
                    //{
                    //    foreach (var optName in optionArray)
                    //    {
                    //        if ((colName.IndexOf(optName) != -1))
                    //        {
                    //            colNamesToBeMatched.Add(colName);
                    //        }
                    //    }
                    //}
                    //colNames = colNamesToBeMatched.ToArray();
                    foreach (var colName in colNames)
                    {

                        var query = dtReport.AsEnumerable().Count(row => row.Field<string>(colName) == Convert.ToString(optArr[1]));
                        res += query;

                    }
                    temp += "{\"Option\":\"" + Convert.ToString(optArr[0]) + "\",\"Count\":\"" + res + "\"},";

                }
            }
            json = "[" + temp.TrimEnd(',') + "]";
            return Json(json, JsonRequestBehavior.AllowGet);
        }
        #endregion


        #region GetReportOfRangeMatrixTypeBasicQuestion1 [Question Type 9]
        [HttpPost]
        [ValidateInput(false)]
        public JsonResult GetReportOfRangeMatrixTypeBasicQuestion1(int QuestionId, string OptArray, int caseCount)
        {
            System.IO.StringReader theReader = new System.IO.StringReader(OptArray);
            DataSet dataSet = new DataSet();
            dataSet.ReadXml(theReader);
            DataTable dtOptions = new DataTable();
            dtOptions = dataSet.Tables[0];
            string json = string.Empty;
            string temp = string.Empty;

            foreach (DataRow dr in dtOptions.Rows)
            {
                string colNameFormat = "Q" + Convert.ToString(QuestionId);
                string[] optionArray = { Convert.ToString(dr[0]) };//OptArray.TrimEnd(',').Split(',');

                if (Session["CaseContainer"] != null)
                {
                    int column = 1;
                    DataTable MainDt = (DataTable)Session["CaseContainer"];
                    if (Session["ReportType"].ToString() == "Online")
                    {
                        column = 1;
                    }
                    else if (Session["ReportType"].ToString() == "Offline")
                    {
                        column = 2;
                    }
                    else
                    {
                        column = 3;
                    }
                    DataTable dtReport = (DataTable)MainDt.Rows[caseCount][column];
                    string[] colNames = dtReport.Columns.Cast<DataColumn>().Where(x => x.ColumnName.Contains(colNameFormat)).Select(x => x.ColumnName).ToArray();
                    List<string> colNamesToBeMatched = new List<string>();

                    foreach (var colName in colNames)
                    {
                        foreach (var optName in optionArray)
                        {
                            if ((colName.IndexOf(optName) != -1))
                            {
                                colNamesToBeMatched.Add(colName);
                            }
                        }
                    }
                    colNames = colNamesToBeMatched.ToArray();
                    int sumVal = 0;
                    foreach (var item in colNames)
                    {
                        var query = dtReport.AsEnumerable().Sum(r => decimal.Parse(r[item].ToString()));
                        sumVal += Convert.ToInt32(query);
                    }
                    temp += "{\"Option\":\"" + Convert.ToString(dr[0]) + "\",\"Sum\":\"" + sumVal + "\"},";
                }
            }
            json = "[" + temp.TrimEnd(',') + "]";
            return Json(json, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region GetReportOfRankOrderAndSinglePunchHorizontalTypeBasicQuestion1 [Question Type 10] [Question Type 12]
        [HttpPost]
        [ValidateInput(false)]
        public JsonResult GetReportOfRankOrderAndSinglePunchHorizontalTypeBasicQuestion1(int QuestionId, string OptArray, int caseCount)
        {
            System.IO.StringReader theReader = new System.IO.StringReader(OptArray);
            DataSet dataSet = new DataSet();
            dataSet.ReadXml(theReader);
            DataTable dtOptions = new DataTable();
            dtOptions = dataSet.Tables[0];
            string json = string.Empty;
            string temp = string.Empty;

            foreach (DataRow dr in dtOptions.Rows)
            {
                string colNameFormat = "Q" + Convert.ToString(QuestionId);
                string[] optionArray = { Convert.ToString(dr[0]) };//OptArray.TrimEnd(',').Split(',');
                int res = 0;
                if (Session["CaseContainer"] != null)
                {
                    int column = 1;
                    DataTable MainDt = (DataTable)Session["CaseContainer"];
                    if (Session["ReportType"].ToString() == "Online")
                    {
                        column = 1;
                    }
                    else if (Session["ReportType"].ToString() == "Offline")
                    {
                        column = 2;
                    }
                    else
                    {
                        column = 3;
                    }
                    DataTable dtReport = (DataTable)MainDt.Rows[caseCount][column];
                    string[] colNames = dtReport.Columns.Cast<DataColumn>().Where(x => x.ColumnName.Contains(colNameFormat)).Select(x => x.ColumnName).ToArray();
                    List<string> colNamesToBeMatched = new List<string>();

                    foreach (var colName in colNames)
                    {
                        foreach (var optName in optionArray)
                        {
                            if ((colName.IndexOf(optName) != -1))
                            {
                                colNamesToBeMatched.Add(colName);
                            }
                        }
                    }
                    colNames = colNamesToBeMatched.ToArray();
                    foreach (var colName in colNames)
                    {
                        var query = dtReport.AsEnumerable().Count(row => row.Field<string>(colName) == "1");
                        res += query;
                    }
                    temp += "{\"Option\":\"" + Convert.ToString(dr[0]) + "\",\"Count\":\"" + res + "\"},";
                }
            }
            json = "[" + temp.TrimEnd(',') + "]";
            return Json(json, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region GetReportOfDropdownMatrixTypeBasicQuestion1 [Question Type 14]
        [HttpPost]
        [ValidateInput(false)]
        public JsonResult GetReportOfDropdownMatrixTypeBasicQuestion1(int QuestionId, string OptArray, int caseCount)
        {
            System.IO.StringReader theReader = new System.IO.StringReader(OptArray);
            DataSet dataSet = new DataSet();
            dataSet.ReadXml(theReader);
            DataTable dtOptions = new DataTable();
            dtOptions = dataSet.Tables[0];
            string json = string.Empty;
            string temp = string.Empty;

            foreach (DataRow dr in dtOptions.Rows)
            {

                string colNameFormat = "Q" + Convert.ToString(QuestionId);
                string[] optionArray = { Convert.ToString(dr[0]).Split('-')[0] };
                if (Session["CaseContainer"] != null)
                {
                    int column = 1;
                    DataTable MainDt = (DataTable)Session["CaseContainer"];
                    if (Session["ReportType"].ToString() == "Online")
                    {
                        column = 1;
                    }
                    else if (Session["ReportType"].ToString() == "Offline")
                    {
                        column = 2;
                    }
                    else
                    {
                        column = 3;
                    }
                    DataTable dtReport = (DataTable)MainDt.Rows[caseCount][column];
                    string[] colNames = dtReport.Columns.Cast<DataColumn>().Where(x => x.ColumnName.Contains(colNameFormat)).Select(x => x.ColumnName).ToArray();
                    List<string> colNamesToBeMatched = new List<string>();

                    foreach (var colName in colNames)
                    {
                        foreach (var optName in optionArray)
                        {
                            if ((colName.IndexOf(optName) != -1))
                            {
                                colNamesToBeMatched.Add(colName);
                            }
                        }
                    }
                    colNames = colNamesToBeMatched.ToArray();
                    var query = dtReport.AsEnumerable().Count(row => row.Field<string>(colNames[0]) == Convert.ToString(dr[0]).Split('-')[1]);
                    temp += "{\"Option\":\"" + Convert.ToString(dr[0]) + "\",\"Count\":\"" + query + "\"},";
                }
            }
            json = "[" + temp.TrimEnd(',') + "]";
            return Json(json, JsonRequestBehavior.AllowGet);
        }
        #endregion

        //-------------------Range Type variance

        #region RangeTypeQuestionVariance1
        //public JsonResult RangeTypeQuestionVariance(int QuestionId)
        //{
        //    string json = string.Empty;
        //    string colName = "Q" + Convert.ToString(QuestionId);
        //    if (Session["ReportDataTable"] != null)
        //    {
        //        DataTable dtReport = (DataTable)Session["ReportDataTable"];
        //        string[] colNames = dtReport.Columns.Cast<DataColumn>().Where(x => x.ColumnName.Contains(colName)).Select(x => x.ColumnName).ToArray();
        //        //var res = (from m in dtReport.AsEnumerable() where m.Field<string>("Code") == "1-1-12" select m).FirstOrDefault();

        //        var query = (from row in dtReport.AsEnumerable()
        //                     group row by row.Field<string>("Product_LogicalId") into grp
        //                     select new
        //                     {
        //                         RangeVal = String.Join(",", grp.Select(row => row.Field<string>(colNames[0])).ToArray()),
        //                     }).ToList();
        //        json = Newtonsoft.Json.JsonConvert.SerializeObject(query);

        //    }
        //    return Json(json, JsonRequestBehavior.AllowGet);
        //}

        public JsonResult RangeTypeQuestionVariance1(int QuestionId, int caseCount)
        {
            string json = string.Empty;
            string colName = "Q" + Convert.ToString(QuestionId);
            if (Session["CaseContainer"] != null)
            {
                int column = 1;
                DataTable MainDt = (DataTable)Session["CaseContainer"];
                if (Session["ReportType"].ToString() == "Online")
                {
                    column = 1;
                }
                else if (Session["ReportType"].ToString() == "Offline")
                {
                    column = 2;
                }
                else
                {
                    column = 3;
                }
                DataTable dtReport = (DataTable)MainDt.Rows[caseCount][column];
                string[] colNames = dtReport.Columns.Cast<DataColumn>().Where(x => x.ColumnName.Contains(colName)).Select(x => x.ColumnName).ToArray();
                //var res = (from m in dtReport.AsEnumerable() where m.Field<string>("Code") == "1-1-12" select m).FirstOrDefault();

                var query = (from row in dtReport.AsEnumerable()
                             group row by row.Field<string>("Product_LogicalId") into grp
                             select new
                             {
                                 Sum = grp.Sum((r) => decimal.Parse(!string.IsNullOrEmpty(r[colNames[0]].ToString()) ? r[colNames[0]].ToString() : "0")),
                                 SquareMean = grp.Sum((r) => decimal.Parse(!string.IsNullOrEmpty(r[colNames[0]].ToString()) ? r[colNames[0]].ToString() : "0") * decimal.Parse(!string.IsNullOrEmpty(r[colNames[0]].ToString()) ? r[colNames[0]].ToString() : "0"))
                             }).ToList();
                json = Newtonsoft.Json.JsonConvert.SerializeObject(query);

            }
            return Json(json, JsonRequestBehavior.AllowGet);
        }
        #endregion

        //------------------- Export Kano Graph to Excel
        [ValidateInput(false)]
        [HttpPost]
        public void ExportKanoGraphToExcel(FormCollection fc)
        {
            string base64 = Request.Form["hdnExportKanoToExcelData"].Split(',')[1];
            byte[] imagebytes = Convert.FromBase64String(base64);
            System.IO.MemoryStream stream = new System.IO.MemoryStream(imagebytes);
            System.Drawing.Image image = new System.Drawing.Bitmap(stream);

            string imageName = System.DateTime.Now.ToString("MM-dd-yyyy") + "_" + Guid.NewGuid().ToString().Substring(0, 5) + "KanoGraph.png";
            if (!System.IO.Directory.Exists(Server.MapPath("~/Content/Uploads/DownloadFiles")))
            {
                System.IO.Directory.CreateDirectory(Server.MapPath("~/Content/Uploads/DownloadFiles"));
            }
            image.Save(Server.MapPath("~/Content/Uploads/DownloadFiles/" + imageName));

            System.Web.UI.WebControls.Image img1 = new System.Web.UI.WebControls.Image();
            img1.ImageUrl = SiteUrl + "/Content/Uploads/DownloadFiles/" + imageName;
            using (System.IO.StringWriter sw = new System.IO.StringWriter())
            {
                using (System.Web.UI.HtmlTextWriter hw = new System.Web.UI.HtmlTextWriter(sw))
                {
                    System.Web.UI.WebControls.Table table = new System.Web.UI.WebControls.Table();
                    System.Web.UI.WebControls.TableRow row = new System.Web.UI.WebControls.TableRow();
                    row.Cells.Add(new System.Web.UI.WebControls.TableCell());
                    row.Cells[0].Controls.Add(img1);
                    table.Rows.Add(row);
                    table.RenderControl(hw);
                    Response.Clear();
                    Response.Buffer = true;
                    Response.AddHeader("content-disposition", "attachment;filename=Images.xls");
                    Response.Charset = "";
                    Response.ContentType = "application/vnd.ms-excel";
                    Response.Write(sw.ToString());
                    Response.Flush();
                    Response.End();

                }
            }
        }

        //------------------- Export Kano Graph to PPT
        [ValidateInput(false)]
        [HttpPost]
        public void ExportKanoGraphToPPT(FormCollection fc)
        {
            string base64 = Request.Form["hdnExportKanoToPPTData"].Split(',')[1];
            byte[] imagebytes = Convert.FromBase64String(base64);
            System.IO.MemoryStream stream = new System.IO.MemoryStream(imagebytes);
            System.Drawing.Image image = new System.Drawing.Bitmap(stream);
            string imageName = System.DateTime.Now.ToString("MM-dd-yyyy") + "_" + Guid.NewGuid().ToString().Substring(0, 5) + "KanoGraph.png";
            if (!System.IO.Directory.Exists(Server.MapPath("~/Content/Uploads/DownloadFiles")))
            {
                System.IO.Directory.CreateDirectory(Server.MapPath("~/Content/Uploads/DownloadFiles"));
            }
            image.Save(Server.MapPath("~/Content/Uploads/DownloadFiles/" + imageName));
            var templ = new PowerPointTemplate();
            templ.PowerPointParameters.Add(new PowerPointParameter() { Name = "1", Image = new System.IO.FileInfo(Server.MapPath("~/Content/Uploads/DownloadFiles/" + imageName)) });

            var templatePath = Server.MapPath("~/Content/PPT/KanoReportRemplate.pptx");
            var outputPath = Server.MapPath("~/Content/Uploads/DownloadFiles/" + DateTime.Today.ToShortDateString().Replace("/", "_") + ".ppt");

            templ.ParseTemplate(templatePath, outputPath);


            //String strTemplate = Server.MapPath("~/Content/PPT/KanoReportRemplate.potx");
            //PowerPoint.Application objApp;
            //PowerPoint.Presentations objPresSet;
            //PowerPoint._Presentation objPres;
            //PowerPoint.Slides objSlides;
            //PowerPoint._Slide objSlide;
            //PowerPoint.TextRange objTextRng;
            //PowerPoint.Shapes objShapes;
            //PowerPoint.Shape objShape;
            //PowerPoint.SlideShowWindows objSSWs;
            //PowerPoint.SlideShowTransition objSST;
            //PowerPoint.SlideRange objSldRng;

            ////Create a new presentation based on a template.
            //objApp = new PowerPoint.Application();
            //objApp.Visible = MsoTriState.msoTrue;
            //objPresSet = objApp.Presentations;
            //objPres = objPresSet.Open(strTemplate, MsoTriState.msoFalse, MsoTriState.msoTrue, MsoTriState.msoTrue);
            //objSlides = objPres.Slides;

            //// Add Image to Template
            //objPres.Slides[1].Shapes.AddPicture(Server.MapPath("~/Content/Uploads/DownloadFiles/" + imageName), MsoTriState.msoFalse, MsoTriState.msoTrue, 150, 80, 450, 450);

            ////int slideCount = 1;
            //////Adding Image to Slide
            ////objSlide = objSlides.Add(slideCount, PowerPoint.PpSlideLayout.ppLayoutBlank);
            ////objSlide.Shapes.AddPicture(Server.MapPath("~/Content/Uploads/DownloadFiles/" + imageName), MsoTriState.msoFalse, MsoTriState.msoTrue,
            ////    150, 150, 600, 600);

            //////Modify the slide show transition settings for all 3 slides in
            ////int[] SlideIdx = new int[slideCount];
            ////for (int i = 0; i < slideCount; i++) SlideIdx[i] = i + 1;
            ////objSldRng = objSlides.Range(SlideIdx);
            ////objSST = objSldRng.SlideShowTransition;
            ////objSST.AdvanceOnTime = MsoTriState.msoTrue;
            ////objSST.AdvanceTime = 3;
            ////objSST.EntryEffect = PowerPoint.PpEntryEffect.ppEffectBoxOut;

            //////Wait for the slide show to end.
            ////objSSWs = objApp.SlideShowWindows;
            //objPres.SaveAs(Server.MapPath("~/Content/Uploads/DownloadFiles" + DateTime.Today.ToShortDateString().Replace("/", "_") + ".ppt"));
            //objPres.Close();
            //objApp.Quit();
            DownloadFile(Server.MapPath("~/Content/Uploads/DownloadFiles/" + DateTime.Today.ToShortDateString().Replace("/", "_") + ".ppt"));
        }

        private void DownloadFile(string filePath)
        {
            System.Net.WebClient req = new System.Net.WebClient();
            HttpResponse response = System.Web.HttpContext.Current.Response;
            response.Clear();
            response.ClearContent();
            response.ClearHeaders();
            response.Buffer = true;
            response.AddHeader("Content-Disposition", "attachment;filename=kanoGraphReporting.ppt");
            byte[] data = req.DownloadData(filePath);
            if (System.IO.File.Exists(filePath))
            {
                System.IO.File.Delete(filePath);
            }
            response.BinaryWrite(data);
            response.End();
        }

        #region GetSinglePunchQuestion1 [Question Type 1]
        public JsonResult GetSinglePunchQuestion1(int QuestionId, string optArray, int caseCount)
        {


            System.IO.StringReader theReader = new System.IO.StringReader(optArray);
            DataSet dataSet = new DataSet();
            dataSet.ReadXml(theReader);
            DataTable dtOptions = new DataTable();
            dtOptions = dataSet.Tables[0];
            string json = string.Empty;
            string temp = string.Empty;
            foreach (DataRow dr in dtOptions.Rows)
            {
                string[] optionArray = { Convert.ToString(dr[0]) };
                string colNameFormat = "Q" + Convert.ToString(QuestionId);
                if (Session["CaseContainer"] != null)
                {
                    int column = 1;
                    DataTable MainDt = (DataTable)Session["CaseContainer"];
                    if (Session["ReportType"].ToString() == "Online")
                    {
                        column = 1;
                    }
                    else if (Session["ReportType"].ToString() == "Offline")
                    {
                        column = 2;
                    }
                    else
                    {
                        column = 3;
                    }



                    DataTable dtReport = (DataTable)MainDt.Rows[caseCount][column];
                    string[] colNames = dtReport.Columns.Cast<DataColumn>().Where(x => x.ColumnName.Contains(colNameFormat)).Select(x => x.ColumnName).ToArray();

                    var query = (from row in dtReport.AsEnumerable()
                                 group row by row.Field<string>("Product_LogicalId") into grp
                                 select new
                                 {
                                     Arr = String.Join(",", grp.Where(v => optionArray.Contains(v.Field<string>(colNames[0]))).Select(row => row.Field<string>(colNames[0])).ToArray())
                                 }).ToList();

                    foreach (var item in query)
                    {
                        if (item.Arr != "")
                            temp += "{\"Sum\":\"" + (item.Arr).Split(',').Length + "\"},";
                        else
                            temp += "{\"Sum\":\"0\"},";
                    }
                    temp = "{\"Option\":\"" + Convert.ToString(dr[0]) + "\",\"List\":[" + temp.TrimEnd(',') + "]},";
                    json += temp;
                    temp = string.Empty;
                }
            }
            json = "[" + json.TrimEnd(',') + "]";


            return Json(json, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region GetMultiPunchQuestion1 [Question Type 2]
        public JsonResult GetMultiPunchQuestion1(int QuestionId, string optArray, int caseCount)
        {
            string colNameFormat = "Q" + Convert.ToString(QuestionId);
            string[] optionArray = optArray.TrimEnd(',').Split(',');
            string json = string.Empty;
            string temp = string.Empty;
            if (Session["CaseContainer"] != null)
            {
                int column = 1;
                DataTable MainDt = (DataTable)Session["CaseContainer"];
                if (Session["ReportType"].ToString() == "Online")
                {
                    column = 1;
                }
                else if (Session["ReportType"].ToString() == "Offline")
                {
                    column = 2;
                }
                else
                {
                    column = 3;
                }



                DataTable dtReport = (DataTable)MainDt.Rows[caseCount][column];
                string[] colNames = dtReport.Columns.Cast<DataColumn>().Where(x => x.ColumnName.Contains(colNameFormat)).Select(x => x.ColumnName).ToArray();
                List<string> colNamesToBeMatched = new List<string>();

                foreach (var colName in colNames)
                {
                    foreach (var optName in optionArray)
                    {
                        if ((colName.IndexOf(optName) != -1))
                        {
                            colNamesToBeMatched.Add(colName);
                        }
                    }
                }

                colNames = colNamesToBeMatched.ToArray();

                foreach (string columnName in colNames)
                {
                    var query = (from row in dtReport.AsEnumerable()
                                 group row by row.Field<string>("Product_LogicalId") into grp
                                 select new
                                 {
                                     Sum = grp.Sum((r) => decimal.Parse(r[columnName].ToString() == "1" ? r[columnName].ToString() : "0"))
                                 }).ToList();

                    foreach (var item in query)
                    {
                        temp += "{\"Sum\":\"" + item.Sum + "\"},";
                    }

                    temp = "{\"Option\":\"" + columnName.Split('-')[1] + "\",\"List\":[" + temp.TrimEnd(',') + "]},";
                    json += temp;
                    temp = string.Empty;
                }
                json = "[" + json.TrimEnd(',') + "]";
            }
            return Json(json, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region GetMeansOfRangeQuestion1 [Question Type 3]
        public JsonResult GetMeansOfRangeQuestion1(int QuestionId, int caseCount)
        {
            string colName = "Q" + Convert.ToString(QuestionId);
            string json = string.Empty;
            if (Session["CaseContainer"] != null)
            {
                int column = 1;
                DataTable MainDt = (DataTable)Session["CaseContainer"];
                if (Session["ReportType"].ToString() == "Online")
                {
                    column = 1;
                }
                else if (Session["ReportType"].ToString() == "Offline")
                {
                    column = 2;
                }
                else
                {
                    column = 3;
                }
                DataTable dtReport = (DataTable)MainDt.Rows[caseCount][column];
                string[] colNames = dtReport.Columns.Cast<DataColumn>().Where(x => x.ColumnName.Contains(colName)).Select(x => x.ColumnName).ToArray();
                //var res = (from m in dtReport.AsEnumerable() where m.Field<string>("Code") == "1-1-12" select m).FirstOrDefault();

                var query = (from row in dtReport.AsEnumerable()
                             group row by row.Field<string>("Product_LogicalId") into grp
                             select new
                             {
                                 Sum = grp.Sum((r) => decimal.Parse(!string.IsNullOrEmpty(r[colNames[0]].ToString()) ? r[colNames[0]].ToString() : "0"))
                             }).ToList();
                json = Newtonsoft.Json.JsonConvert.SerializeObject(query);

            }
            return Json(json, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region GetMeansOfConstantSumQuestion1 [Question Type 4]
        public JsonResult GetMeansOfConstantSumQuestion1(int QuestionId, string optArray, int caseCount)
        {
            string colNameFormat = "Q" + Convert.ToString(QuestionId);
            string[] optionArray = optArray.TrimEnd(',').Split(',');

            string json = string.Empty;
            string temp = string.Empty;
            if (Session["CaseContainer"] != null)
            {
                int column = 1;
                DataTable MainDt = (DataTable)Session["CaseContainer"];
                if (Session["ReportType"].ToString() == "Online")
                {
                    column = 1;
                }
                else if (Session["ReportType"].ToString() == "Offline")
                {
                    column = 2;
                }
                else
                {
                    column = 3;
                }
                DataTable dtReport = (DataTable)MainDt.Rows[caseCount][column];
                string[] colNames = dtReport.Columns.Cast<DataColumn>().Where(x => x.ColumnName.Contains(colNameFormat)).Select(x => x.ColumnName).ToArray();
                List<string> colNamesToBeMatched = new List<string>();

                foreach (var colName in colNames)
                {
                    foreach (var optName in optionArray)
                    {
                        if ((colName.IndexOf(optName) != -1))
                        {
                            colNamesToBeMatched.Add(colName);
                        }
                    }
                }

                colNames = colNamesToBeMatched.ToArray();

                foreach (var columnName in colNames)
                {
                    var query = (from row in dtReport.AsEnumerable()
                                 group row by row.Field<string>("Product_LogicalId") into grp
                                 select new
                                 {
                                     Sum = grp.Sum((r) => decimal.Parse(r[columnName].ToString()))
                                     //Sum = String.Join(",", grp.Where(v => colNames.Contains(v.Field<string>(colNames[0]))).Select(row => row.Field<string>(colNames[0])).ToArray())
                                 }).ToList();

                    foreach (var item in query)
                    {
                        temp += "{\"Sum\":\"" + item.Sum + "\"},";
                    }
                    temp = "{\"Option\":\"" + columnName + "\",\"List\":[" + temp.TrimEnd(',') + "]},";
                    json += temp;
                    temp = string.Empty;
                }
                json = "[" + json.TrimEnd(',') + "]";

            }
            return Json(json, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region GetMeansOfRankingQuestion1 [Question Type 5]
        public JsonResult GetMeansOfRankingQuestion1(int QuestionId, string optionName, string macthingValue, int caseCount)
        {
            string colNameFormat = "Q" + Convert.ToString(QuestionId);
            string json = string.Empty;
            string temp = string.Empty;
            if (Session["CaseContainer"] != null)
            {
                int column = 1;
                DataTable MainDt = (DataTable)Session["CaseContainer"];
                if (Session["ReportType"].ToString() == "Online")
                {
                    column = 1;
                }
                else if (Session["ReportType"].ToString() == "Offline")
                {
                    column = 2;
                }
                else
                {
                    column = 3;
                }
                DataTable dtReport = (DataTable)MainDt.Rows[caseCount][column];
                string[] colNames = dtReport.Columns.Cast<DataColumn>().Where(x => x.ColumnName.Contains(colNameFormat)).Select(x => x.ColumnName).ToArray();

                foreach (string columnName in colNames)
                {
                    if (columnName.ToLower().IndexOf(optionName.ToLower()) != -1)
                    {
                        var query = (from row in dtReport.AsEnumerable()
                                     group row by row.Field<string>("Product_LogicalId") into grp
                                     select new
                                     {
                                         Sum = grp.Where((r) => r[columnName].ToString() == macthingValue).Sum((r) => decimal.Parse(r[columnName].ToString()))
                                     }).ToList();

                        foreach (var item in query)
                        {
                            temp += "{\"Sum\":\"" + item.Sum + "\"},";
                        }
                        temp = "{\"Option\":\"" + columnName.Split('-')[1] + "\",\"List\":[" + temp.TrimEnd(',') + "]},";
                        json += temp;
                        temp = string.Empty;
                    }
                }
                json = "[" + json.TrimEnd(',') + "]";

            }
            return Json(json, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region GetMatrixTypeQuestion1 [Question Type 6]
        public JsonResult GetMatrixTypeQuestion1(int QuestionId, string optArray, string matchingValue, int caseCount)
        {
            string colNameFormat = "Q" + Convert.ToString(QuestionId);
            string[] optionArray = optArray.TrimEnd(',').Split(',');
            string json = string.Empty;
            string temp = string.Empty;
            if (Session["CaseContainer"] != null)
            {
                int column = 1;
                DataTable MainDt = (DataTable)Session["CaseContainer"];
                if (Session["ReportType"].ToString() == "Online")
                {
                    column = 1;
                }
                else if (Session["ReportType"].ToString() == "Offline")
                {
                    column = 2;
                }
                else
                {
                    column = 3;
                }
                DataTable dtReport = (DataTable)MainDt.Rows[caseCount][column];
                string[] colNames = dtReport.Columns.Cast<DataColumn>().Where(x => x.ColumnName.Contains(colNameFormat)).Select(x => x.ColumnName).ToArray();


                List<string> colNamesToBeMatched = new List<string>();

                foreach (var colName in colNames)
                {
                    foreach (var optName in optionArray)
                    {
                        if ((colName.IndexOf(optName) != -1))
                        {
                            colNamesToBeMatched.Add(colName);
                        }
                    }
                }

                colNames = colNamesToBeMatched.ToArray();

                foreach (string columnName in colNames)
                {
                    var query = (from row in dtReport.AsEnumerable()
                                 group row by row.Field<string>("Product_LogicalId") into grp
                                 select new
                                 {
                                     //Sum = grp.Sum((r) => decimal.Parse(r[columnName].ToString()))
                                     ans = String.Join(",", grp.Select(row => row.Field<string>(columnName)).ToArray()),
                                     Sum = grp.Sum((r) => decimal.Parse(r[columnName].ToString() == matchingValue ? "1" : "0"))
                                 }).ToList();

                    foreach (var item in query)
                    {
                        temp += "{\"Sum\":\"" + item.Sum + "\"},";
                    }
                    temp = "{\"Option\":\"" + columnName.Split('-')[1] + "\",\"List\":[" + temp.TrimEnd(',') + "]},";
                    json += temp;
                    temp = string.Empty;
                }
                json = "[" + json.TrimEnd(',') + "]";
            }
            return Json(json, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region GetSimpleTextQuestion1 [Question Type 7]
        public JsonResult GetSimpleTextQuestion1(int QuestionId, int caseCount)
        {
            string colName = "Q" + Convert.ToString(QuestionId);
            string json = string.Empty;
            if (Session["CaseContainer"] != null)
            {
                int column = 1;
                DataTable MainDt = (DataTable)Session["CaseContainer"];
                if (Session["ReportType"].ToString() == "Online")
                {
                    column = 1;
                }
                else if (Session["ReportType"].ToString() == "Offline")
                {
                    column = 2;
                }
                else
                {
                    column = 3;
                }
                DataTable dtReport = (DataTable)MainDt.Rows[caseCount][column];
                string[] colNames = dtReport.Columns.Cast<DataColumn>().Where(x => x.ColumnName.Contains(colName)).Select(x => x.ColumnName).ToArray();
                var query = (from row in dtReport.AsEnumerable()
                             group row by row.Field<string>("Product_LogicalId") into grp
                             select new
                             {
                                 ans = String.Join(",", grp.Select(row => row.Field<string>(colNames[0])).ToArray()),
                             });
                json = Newtonsoft.Json.JsonConvert.SerializeObject(query);
            }
            return Json(json, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region GetSinglePunchMatrixTypeQuestion1 [Question Type 8]
        public JsonResult GetSinglePunchMatrixTypeQuestion1(int QuestionId, string optArray, string matchingValue, int caseCount)
        {
            System.IO.StringReader theReader = new System.IO.StringReader(optArray);
            DataSet dataSet = new DataSet();
            dataSet.ReadXml(theReader);
            DataTable dtOptions = new DataTable();
            dtOptions = dataSet.Tables[0];
            string json = string.Empty;
            string temp = string.Empty;
            foreach (DataRow dr in dtOptions.Rows)
            {
                string colNameFormat = "Q" + Convert.ToString(QuestionId);
                string[] optArr = Convert.ToString(dr[0]).Split(',');
                string[] optionArray = { optArr[0] != null ? Convert.ToString(optArr[0]) : "" };//OptArray.TrimEnd(',').Split(',');
                int res = 0;

                if (Session["CaseContainer"] != null)
                {
                    int column = 1;
                    DataTable MainDt = (DataTable)Session["CaseContainer"];
                    if (Session["ReportType"].ToString() == "Online")
                    {
                        column = 1;
                    }
                    else if (Session["ReportType"].ToString() == "Offline")
                    {
                        column = 2;
                    }
                    else
                    {
                        column = 3;
                    }
                    DataTable dtReport = (DataTable)MainDt.Rows[caseCount][column];
                    string optionName = optArr[0].Split('-')[1];
                    string[] colNames = dtReport.Columns.Cast<DataColumn>().Where(x => x.ColumnName.Contains(colNameFormat) && x.ColumnName.Contains(optionName)).Select(x => x.ColumnName).ToArray();


                    //string[] colNames = dtReport.Columns.Cast<DataColumn>().Where(x => x.ColumnName.Contains(colNameFormat)).Select(x => x.ColumnName).ToArray();
                    List<string> colNamesToBeMatched = new List<string>();
                    foreach (string columnName in colNames)
                    {
                        var query = (from row in dtReport.AsEnumerable()
                                     group row by row.Field<string>("Product_LogicalId") into grp
                                     select new
                                     {
                                         //Sum = grp.Sum((r) => decimal.Parse(r[columnName].ToString()))
                                         ans = String.Join(",", grp.Select(row => row.Field<string>(columnName)).ToArray()),
                                         Sum = grp.AsEnumerable().Count(row => row.Field<string>(columnName) == Convert.ToString(optArr[1]))
                                         //Sum = grp.Sum((r) => decimal.Parse(r[columnName].ToString() == matchingValue ? "1" : "0")),
                                     }).ToList();

                        foreach (var item in query)
                        {
                            temp += "{\"Sum\":\"" + item.Sum + "\"},";
                        }
                        temp = "{\"Option\":\"" + Convert.ToString(optArr[0]) + "\",\"List\":[" + temp.TrimEnd(',') + "]},";
                        json += temp;
                        temp = string.Empty;
                    }

                }
            }
            json = "[" + json.TrimEnd(',') + "]";



            return Json(json, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region GetMeansOfRangeMatrixTypeQuestion1 [Question Type 9]
        public JsonResult GetMeansOfRangeMatrixTypeQuestion1(int QuestionId, string optArray, int caseCount)
        {
            string colNameFormat = "Q" + Convert.ToString(QuestionId);
            string[] optionArray = optArray.TrimEnd(',').Split(',');

            string json = string.Empty;
            string temp = string.Empty;
            if (Session["CaseContainer"] != null)
            {
                int column = 1;
                DataTable MainDt = (DataTable)Session["CaseContainer"];
                if (Session["ReportType"].ToString() == "Online")
                {
                    column = 1;
                }
                else if (Session["ReportType"].ToString() == "Offline")
                {
                    column = 2;
                }
                else
                {
                    column = 3;
                }
                DataTable dtReport = (DataTable)MainDt.Rows[caseCount][column];
                string[] colNames = dtReport.Columns.Cast<DataColumn>().Where(x => x.ColumnName.Contains(colNameFormat)).Select(x => x.ColumnName).ToArray();
                List<string> colNamesToBeMatched = new List<string>();

                foreach (var colName in colNames)
                    foreach (var optName in optionArray)
                        if ((colName.IndexOf(optName) != -1))
                            colNamesToBeMatched.Add(colName);

                colNames = colNamesToBeMatched.ToArray();
                string fieldNames = String.Join(",", colNames);

                foreach (var columnName in colNames)
                {
                    var query = (from row in dtReport.AsEnumerable()
                                 group row by row.Field<string>("Product_LogicalId") into grp
                                 select new
                                 {
                                     Sum = grp.Sum((r) => decimal.Parse((r[columnName] != null && r[columnName].ToString() != string.Empty ? r[columnName].ToString() : "0")))
                                 }).ToList();

                    foreach (var item in query)
                    {
                        temp += "{\"Sum\":\"" + item.Sum + "\"},";
                    }
                    temp = "{\"Option\":\"" + columnName + "\",\"List\":[" + temp.TrimEnd(',') + "]},";
                    json += temp;
                    temp = string.Empty;
                }
                json = "[" + json.TrimEnd(',') + "]";

            }
            return Json(json, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region GetRankOrderTypeQuestion1 [Question Type 10 || 11 || 12]
        public JsonResult GetRankOrderTypeQuestion1(int QuestionId, string optArray, int caseCount)
        {
            string colNameFormat = "Q" + Convert.ToString(QuestionId);
            string[] optionArray = optArray.TrimEnd(',').Split(',');
            string json = string.Empty;
            string temp = string.Empty;
            if (Session["CaseContainer"] != null)
            {
                int column = 1;
                DataTable MainDt = (DataTable)Session["CaseContainer"];
                if (Session["ReportType"].ToString() == "Online")
                {
                    column = 1;
                }
                else if (Session["ReportType"].ToString() == "Offline")
                {
                    column = 2;
                }
                else
                {
                    column = 3;
                }
                DataTable dtReport = (DataTable)MainDt.Rows[caseCount][column];
                string[] colNames = dtReport.Columns.Cast<DataColumn>().Where(x => x.ColumnName.Contains(colNameFormat)).Select(x => x.ColumnName).ToArray();
                List<string> colNamesToBeMatched = new List<string>();
                foreach (var colName in colNames)
                {
                    foreach (var optName in optionArray)
                    {
                        if ((colName.IndexOf(optName.Trim()) != -1))
                        {
                            colNamesToBeMatched.Add(colName);
                        }
                    }
                }
                colNames = colNamesToBeMatched.ToArray();
                foreach (string columnName in colNames)
                {
                    var query = (from row in dtReport.AsEnumerable()
                                 group row by row.Field<string>("Product_LogicalId") into grp
                                 select new
                                 {
                                     //Sum = grp.Sum((r) => decimal.Parse(r[columnName].ToString()))
                                     ans = String.Join(",", grp.Select(row => row.Field<string>(columnName)).ToArray()),
                                     Sum = grp.Sum((r) => decimal.Parse(r[columnName].ToString() == "1" ? r[columnName].ToString() : "0"))
                                 }).ToList();
                    foreach (var item in query)
                    {
                        temp += "{\"Sum\":\"" + item.Sum + "\"},";
                    }
                    temp = "{\"Option\":\"" + columnName.Split('-')[1] + "\",\"List\":[" + temp.TrimEnd(',') + "]},";
                    json += temp;
                    temp = string.Empty;
                }
                json = "[" + json.TrimEnd(',') + "]";
            }
            return Json(json, JsonRequestBehavior.AllowGet);
        }
        #endregion

        //#region GetMeansOfKanoTypeQuestion111 [Question Type 13]
        ////[ValidateInput(false)]
        ////[HttpPost]
        //public JsonResult GetMeansOfKanoTypeQuestion111(int QuestionId, string OptArray, string responderId, int caseCount, int QuesColsCount)
        //{

        //    System.IO.StringReader theReader = new System.IO.StringReader(OptArray);
        //    DataSet dataSet = new DataSet();
        //    dataSet.ReadXml(theReader);
        //    DataTable dtOptions = new DataTable();
        //    dtOptions = dataSet.Tables[0];

        //    string json = string.Empty;
        //    string temp = string.Empty;

        //    foreach (DataRow dr in dtOptions.Rows)
        //    {
        //        string colNameFormat = "Q" + Convert.ToString(QuestionId);
        //        string[] optionArray = { Convert.ToString(dr[0]) };//OptArray.TrimEnd(',').Split(',');
        //        //string json = string.Empty;
        //        //string temp = string.Empty;
        //        if (Session["CaseContainer"] != null)
        //        {
        //            int column = 1;
        //            DataTable MainDt = (DataTable)Session["CaseContainer"];
        //            if (Session["ReportType"].ToString() == "Online")
        //            {
        //                column = 1;
        //            }
        //            else if (Session["ReportType"].ToString() == "Offline")
        //            {
        //                column = 2;
        //            }
        //            else
        //            {
        //                column = 3;
        //            }
        //            DataTable dtReport = (DataTable)MainDt.Rows[caseCount][column];
        //            string[] colNames = dtReport.Columns.Cast<DataColumn>().Where(x => x.ColumnName.Contains(colNameFormat)).Select(x => x.ColumnName).ToArray();
        //            List<string> colNamesToBeMatched = new List<string>();
        //            foreach (var colName in colNames)
        //            {
        //                foreach (var optName in optionArray)
        //                {
        //                    if ((colName.IndexOf(optName.Trim()) != -1))
        //                    {
        //                        colNamesToBeMatched.Add(colName);
        //                    }
        //                }
        //            }
        //            colNames = colNamesToBeMatched.ToArray();

        //            var query = (from row in dtReport.AsEnumerable()
        //                         select new
        //                         {
        //                             IfPresentAnswers = row.Field<string>(colNames[0]),
        //                             IfAbsentAnswers = row.Field<string>(colNames[1]),
        //                             RespId = row.Field<string>("Resp_Id")
        //                         }).Where(l => l.RespId == responderId.ToString()).ToList();


        //            foreach (var item in query)
        //            {
        //                //string[] presentAnswers = item.IfPresentAnswers.Split(',');
        //                // string[] absentAnswers = item.IfAbsentAnswers.Split(',');
        //                string combinedPN = string.Empty;
        //                int negAns = Convert.ToInt32(item.IfAbsentAnswers) % QuesColsCount;
        //                combinedPN += "{\"PN\":\"" + item.IfPresentAnswers + (negAns == 0 ? QuesColsCount : negAns) + "\"},";
        //                temp += "{\"CombinedPN\":[" + combinedPN + "]},"; // CombinedPN = Combined (Concatenated) result of positive resluts of positive and negative columns
        //                temp = "{\"Option\":\"" + colNames[0].Split('-')[1] + "\",\"List\":[" + combinedPN.TrimEnd(',') + "]},";
        //                json += temp;
        //                temp = string.Empty;
        //            }
        //            if (indexCount > dtReport.Rows.Count)
        //            {
        //                indexCount++;
        //            }
        //        }
        //    }
        //    json = "[" + json.TrimEnd(',') + "]";

        //    return Json(json, JsonRequestBehavior.AllowGet);
        //}
        //#endregion

        #region GetMeansOfKanoTypeQuestion111 [Question Type 13]
        [ValidateInput(false)]
        //[HttpPost]
        public JsonResult GetMeansOfKanoTypeQuestion111(string QuestionId, string OptArray, string responderId, string caseCount, string QuesColsCount)
        {

            int Question_Id = Convert.ToInt32(QuestionId);
            int case_Count = Convert.ToInt32(caseCount) - 1;
            //   case_Count = 1;
            int Ques_ColsCount = Convert.ToInt32(QuesColsCount);
            System.IO.StringReader theReader = new System.IO.StringReader(OptArray);
            DataSet dataSet = new DataSet();
            dataSet.ReadXml(theReader);
            DataTable dtOptions = new DataTable();
            dtOptions = dataSet.Tables[0];

            string json = string.Empty;
            string temp = string.Empty;

            foreach (DataRow dr in dtOptions.Rows)
            {
                string colNameFormat = "Q" + Convert.ToString(Question_Id);
                string[] optionArray = { Convert.ToString(dr[0]) };//OptArray.TrimEnd(',').Split(',');
                //string json = string.Empty;
                //string temp = string.Empty;
                if (Session["CaseContainer"] != null)
                {
                    int column = 1;
                    DataTable MainDt = (DataTable)Session["CaseContainer"];
                    if (Session["ReportType"].ToString() == "Online")
                    {
                        column = 1;
                    }
                    else if (Session["ReportType"].ToString() == "Offline")
                    {
                        column = 2;
                    }
                    else
                    {
                        column = 3;
                    }
                    DataTable dtReport = (DataTable)MainDt.Rows[case_Count][column];
                    string[] colNames = dtReport.Columns.Cast<DataColumn>().Where(x => x.ColumnName.Contains(colNameFormat)).Select(x => x.ColumnName).ToArray();
                    List<string> colNamesToBeMatched = new List<string>();
                    foreach (var colName in colNames)
                    {
                        foreach (var optName in optionArray)
                        {
                            if ((colName.IndexOf(optName.Trim()) != -1))
                            {
                                colNamesToBeMatched.Add(colName);
                            }
                        }
                    }
                    colNames = colNamesToBeMatched.ToArray();

                    var query = (from row in dtReport.AsEnumerable()
                                 select new
                                 {
                                     IfPresentAnswers = row.Field<string>(colNames[0]),
                                     IfAbsentAnswers = row.Field<string>(colNames[1]),
                                     RespId = row.Field<string>("Resp_Id"),
                                     ImportResp = row.Field<string>("Import_Resp"),
                                 }).Where(l => l.RespId == responderId.ToString()).ToList();


                    foreach (var item in query)
                    {
                        //string[] presentAnswers = item.IfPresentAnswers.Split(',');
                        // string[] absentAnswers = item.IfAbsentAnswers.Split(',');
                        string combinedPN = string.Empty;
                        int negAns = Convert.ToInt32(item.IfAbsentAnswers) % Ques_ColsCount;
                        combinedPN += "{\"PN\":\"" + item.IfPresentAnswers + (negAns == 0 ? Ques_ColsCount : negAns) + "\"},";
                        temp += "{\"CombinedPN\":[" + combinedPN + "]},"; // CombinedPN = Combined (Concatenated) result of positive resluts of positive and negative columns
                        temp = "{\"Option\":\"" + colNames[0].Split('-')[1] + "\",\"List\":[" + combinedPN.TrimEnd(',') + "],\"ImportResp\":[" + item.ImportResp + "]},";
                        json += temp;
                        temp = string.Empty;
                    }
                    if (indexCount > dtReport.Rows.Count)
                    {
                        indexCount++;
                    }
                }
            }
            json = "[" + json.TrimEnd(',') + "]";

            return Json(json, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region GetMeansOfDropdownMatrixTypeQuestion1 [Question Type 14]
        public JsonResult GetMeansOfDropdownMatrixTypeQuestion1(int QuestionId, string optArray, int caseCount)
        {

            string colNameFormat = "Q" + Convert.ToString(QuestionId);
            string[] optionArray = optArray.TrimEnd(',').Split(',');
            string json = string.Empty;
            string temp = string.Empty;
            if (Session["CaseContainer"] != null)
            {
                int column = 1;
                DataTable MainDt = (DataTable)Session["CaseContainer"];
                if (Session["ReportType"].ToString() == "Online")
                {
                    column = 1;
                }
                else if (Session["ReportType"].ToString() == "Offline")
                {
                    column = 2;
                }
                else
                {
                    column = 3;
                }
                DataTable dtReport = (DataTable)MainDt.Rows[caseCount][column];
                string[] colNames = dtReport.Columns.Cast<DataColumn>().Where(x => x.ColumnName.Contains(colNameFormat)).Select(x => x.ColumnName).ToArray();
                List<string> colNamesToBeMatched = new List<string>();
                foreach (var colName in colNames)
                {
                    foreach (var optName in optionArray)
                    {
                        if ((colName.IndexOf(optName.Trim()) != -1))
                        {
                            colNamesToBeMatched.Add(colName);
                        }
                    }
                }
                colNames = colNamesToBeMatched.ToArray();
                foreach (string columnName in colNames)
                {
                    var query = (from row in dtReport.AsEnumerable()
                                 group row by row.Field<string>("Product_LogicalId") into grp
                                 select new
                                 {
                                     //Sum = grp.Sum((r) => decimal.Parse(r[columnName].ToString()))
                                     ans = String.Join(",", grp.Select(row => row.Field<string>(columnName)).ToArray()),
                                     Sum = grp.Sum((r) => decimal.Parse(r[columnName].ToString()))
                                 }).ToList();
                    foreach (var item in query)
                    {
                        temp += "{\"Sum\":\"" + item.Sum + "\"},";
                    }
                    temp = "{\"Option\":\"" + columnName.Split('-')[1] + "\",\"List\":[" + temp.TrimEnd(',') + "]},";
                    json += temp;
                    temp = string.Empty;
                }
                json = "[" + json.TrimEnd(',') + "]";
            }
            return Json(json, JsonRequestBehavior.AllowGet);




            //    string colNameFormat = "Q" + Convert.ToString(QuestionId);
            //    string[] optionArray = optArray.TrimEnd(',').Split(',');
            //    string json = string.Empty;
            //    string temp = string.Empty;
            //    if (Session["ReportDataTable"] != null)
            //    {
            //        DataTable dtReport = (DataTable)Session["ReportDataTable"];
            //        string[] colNames = dtReport.Columns.Cast<DataColumn>().Where(x => x.ColumnName.Contains(colNameFormat)).Select(x => x.ColumnName).ToArray();
            //        List<string> colNamesToBeMatched = new List<string>();
            //        foreach (var colName in colNames)
            //        {
            //            foreach (var optName in optionArray)
            //            {
            //                if ((colName.IndexOf(optName) != -1))
            //                {
            //                    colNamesToBeMatched.Add(colName);
            //                }
            //            }
            //        }
            //        colNames = colNamesToBeMatched.ToArray();
            //        foreach (string columnName in colNames)
            //        {
            //            var query = (from row in dtReport.AsEnumerable()
            //                         group row by row.Field<string>("Product_LogicalId") into grp
            //                         select new
            //                         {
            //                             //Sum = grp.Sum((r) => decimal.Parse(r[columnName].ToString()))
            //                             ans = String.Join(",", grp.Select(row => row.Field<string>(columnName)).ToArray()),
            //                             Sum = grp.Sum((r) => decimal.Parse(r[columnName].ToString() == matchingValue ? "1" : "0"))
            //                         }).ToList();
            //            foreach (var item in query)
            //            {
            //                temp += "{\"Sum\":\"" + item.Sum + "\"},";
            //            }
            //            temp = "{\"Option\":\"" + columnName.Split('-')[1] + "\",\"List\":[" + temp.TrimEnd(',') + "]},";
            //            json += temp;
            //            temp = string.Empty;
            //        }
            //        json = "[" + json.TrimEnd(',') + "]";
            //    }
            //    return Json(json, JsonRequestBehavior.AllowGet);
        }
        #endregion


        public ActionResult QueryReports(int projId, string ReportType)
        {
            ViewBag.ProjectId = projId;
            ViewBag.ReportType = ReportType;
            return View();
        }

        // Convert DataTable to XML
        #region ConvertDatatableToXML
        public string ConvertDatatableToXML(DataTable dt)
        {
            string xmlstr;
            if (dt.Rows.Count > 0)
            {
                MemoryStream str = new MemoryStream();
                dt.TableName = "myTable";
                dt.WriteXml(str, true);
                str.Seek(0, SeekOrigin.Begin);
                StreamReader sr = new StreamReader(str);
                xmlstr = sr.ReadToEnd();
            }
            else
            {
                if (dt.Rows.Count == 0)
                {
                    dt.Rows.Add("1Delete1");
                }
                for (int i = 0; i < dt.Columns.Count; i++)
                {
                    dt.Rows[0][i] = "1Delete1";
                }

                MemoryStream str = new MemoryStream();
                dt.TableName = "myTable";
                dt.WriteXml(str, true);
                str.Seek(0, SeekOrigin.Begin);
                StreamReader sr = new StreamReader(str);
                xmlstr = sr.ReadToEnd().Replace("1Delete1", "");
            }
            return (xmlstr);
        }
        #endregion

        #region ConvertDatatableToXML_QueryBuilder
        public string ConvertDatatableToXML_QueryBuilder(DataTable dt)
        {
            string xmlstr;

            XmlDocument doc = new XmlDocument();
            //XmlDeclaration xmlDeclaration = doc.CreateXmlDeclaration("1.0", "UTF-8", null);
            XmlElement root = doc.DocumentElement;
            //  doc.InsertBefore(xmlDeclaration, root);

            XmlElement DocumentElement = doc.CreateElement(string.Empty, "DocumentElement", string.Empty);
            doc.AppendChild(DocumentElement);

            if (dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    XmlElement Contant = doc.CreateElement(string.Empty, "myTable", string.Empty);
                    DocumentElement.AppendChild(Contant);
                    foreach (DataColumn column in dt.Columns)
                    {
                        string rowValue = row[column.ColumnName].ToString();
                        var Heading = column.ColumnName;
                        XmlElement Title = doc.CreateElement(string.Empty, Heading, string.Empty);
                        XmlText textHeading = doc.CreateTextNode(rowValue);
                        Title.AppendChild(textHeading);
                        Contant.AppendChild(Title);
                    }
                    DocumentElement.AppendChild(Contant);
                }
            }
            else
            {
                XmlElement Contant = doc.CreateElement(string.Empty, "myTable", string.Empty);
                DocumentElement.AppendChild(Contant);
                foreach (DataColumn column in dt.Columns)
                {
                    var Heading = column.ColumnName;
                    XmlElement Title = doc.CreateElement(string.Empty, Heading, string.Empty);
                    XmlText textHeading = doc.CreateTextNode(null);
                    Title.AppendChild(textHeading);
                    Contant.AppendChild(Title);
                }
                DocumentElement.AppendChild(Contant);
            }

            xmlstr = doc.InnerXml;

            return (xmlstr);
        }
        #endregion

        // Convert XML to DataTable
        #region ConvertXMLToDataTable
        public DataTable ConvertXMLToDataTable(string xml)
        {
            DataSet ds = new DataSet();
            using (StringReader stringReader = new StringReader(xml))
            {
                ds = new DataSet();
                ds.ReadXml(stringReader);
            }
            DataTable dtCopy = ds.Tables[0].Clone(); //copy the structure 
            for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++) //iterate through the rows of the source
            {
                DataRow currentRow = ds.Tables[0].Rows[i];  //copy the current row 
                foreach (var colValue in currentRow.ItemArray)//move along the columns 
                {
                    if (!string.IsNullOrEmpty(colValue.ToString())) // if there is a value in a column, copy the row and finish
                    {
                        dtCopy.ImportRow(currentRow);
                        break; //break and get a new row                        
                    }
                }
            }
            return dtCopy;
        }
        #endregion
    }

}
