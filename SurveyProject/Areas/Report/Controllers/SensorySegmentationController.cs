﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SRV.BaseLayer.Admin;
using SRV.ActionLayer.Admin;
using SurveyProject.Models;
using System.Data;
using SRV.Utility;
using SurveyProject.Helper;
using SurveyProject.Controllers;
using System.IO;
using System.Text.RegularExpressions;
using SRV.BaseLayer.Respondent;
using SRV.ActionLayer.Respondent;
using Newtonsoft.Json;
using System.Diagnostics;
using RDotNet;
using System.Text;
using System.Configuration;

namespace SurveyProject.Areas.Report.Controllers
{
    [CheckLogin]
    public class SensorySegmentationController : Controller
    {
        #region Declaration

        QuestionBase questionBase = new QuestionBase();
        CommonMethods comMethods = new CommonMethods();
        RespondentAction respondentAction = new RespondentAction();

        #endregion

        //
        // GET: /Report/SensorySegmentation/
        #region Properties
        ProjectBase projectBase = new ProjectBase();
        SRV.BaseLayer.ActionResult actionResult = new SRV.BaseLayer.ActionResult();
        AdminAction adminAction = new AdminAction();
        ProductProjectBase productProjectBase = new ProductProjectBase();
        string FactorAnalysisDrivePath = ConfigurationManager.AppSettings["FactorAnalysisPath"].ToString();
        #endregion

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Curve(string projId, string ReportType)
        {

            CreateProjectModel model = GetProjectDetailById(Convert.ToInt32(projId));

            string projectGUID = model.ProjectGuid;
            ViewBag.ProjectName = model.ProjectName;
            ViewBag.ProjectType = model.ProjectType;
            ViewBag.ProjectId = Convert.ToInt32(projId);
            DataTable dt = new DataTable();
            DataTable dt1 = new DataTable();
            int RespCount = 0;



            if (ReportType.ToLower() == "online")
            {
                ViewBag.ReportType = "online";
            }
            else if (ReportType.ToLower() == "offline")
            {
                ViewBag.ReportType = "offline";
            }
            else if (ReportType.ToLower() == "combined")
            {
                ViewBag.ReportType = "combined";
            }


            Session["ReportDataTable"] = dt;
            ViewBag.RDT = dt;

            ViewBag.ProjectGUID = projectGUID;
            ViewBag.RespCount = RespCount;
            ViewBag.ProjectId = projId;

            return View();
        }

        #region GetProjectDetailById
        public CreateProjectModel GetProjectDetailById(int? id = 0)
        {
            CreateProjectModel model = new CreateProjectModel();
            List<ProductProjectModel> lstproductModel = new List<ProductProjectModel>();
            try
            {
                projectBase.Id = Convert.ToInt32(id);
                actionResult = adminAction.Project_LoadById(projectBase);
                if (actionResult.IsSuccess)
                {
                    DataRow dr = actionResult.dtResult.Rows[0];
                    model.Id = dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0;
                    model.SuperProjectId = dr["SuperProjectId"] != DBNull.Value ? Convert.ToInt32(dr["SuperProjectId"]) : 0;
                    model.ProjectName = dr["ProjectName"] != DBNull.Value ? Convert.ToString(dr["ProjectName"]) : "";
                    model.WelcomeMessage = dr["WelcomeMessage"] != DBNull.Value && !String.IsNullOrEmpty(dr["WelcomeMessage"].ToString()) ? Convert.ToString(dr["WelcomeMessage"]) : Email.ReadFile("Welcome.txt");
                    model.PageHeader = dr["PageHeader"] != DBNull.Value && !String.IsNullOrEmpty(dr["PageHeader"].ToString()) ? Convert.ToString(dr["PageHeader"]) : Email.ReadFile("Header.txt");
                    model.PageFooter = dr["PageFooter"] != DBNull.Value && !String.IsNullOrEmpty(dr["PageFooter"].ToString()) ? Convert.ToString(dr["PageFooter"]) : Email.ReadFile("Footer.txt");
                    model.ProjectCompletionMesssage = dr["ProjectCompletionMesssage"] != DBNull.Value && !String.IsNullOrEmpty(dr["ProjectCompletionMesssage"].ToString()) ? Convert.ToString(dr["ProjectCompletionMesssage"]) : Email.ReadFile("Complete.txt");
                    model.RedirectUrl = dr["RedirectUrl"] != DBNull.Value ? Convert.ToString(dr["RedirectUrl"]) : "";
                    model.InvitationMessage = dr["InvitationMessage"] != DBNull.Value ? Convert.ToString(dr["InvitationMessage"]) : "";
                    model.OwnerId = dr["OwnerId"] != DBNull.Value ? Convert.ToInt32(dr["OwnerId"]) : 0;
                    model.CreatedBy = dr["CreatedBy"] != DBNull.Value ? Convert.ToInt32(dr["CreatedBy"]) : 0;
                    model.ProjectQuota = dr["ProjectQuota"] != DBNull.Value ? Convert.ToInt32(dr["ProjectQuota"]) : 0;
                    model.TerminationMessageUpper = dr["ProjectTerminationMessage"] != DBNull.Value && !String.IsNullOrEmpty(dr["ProjectTerminationMessage"].ToString()) ? Convert.ToString(dr["ProjectTerminationMessage"]) : Email.ReadFile("TerminateUpper.txt");
                    model.ProjectGuid = dr["ProjectGuid"] != DBNull.Value ? Convert.ToString(dr["ProjectGuid"]) : "";
                    model.IsIncludePageHeader = dr["IsIncludePageHeader"] != DBNull.Value ? Convert.ToBoolean(dr["IsIncludePageHeader"]) : false;
                    model.IsIncludePageFooter = dr["IsIncludePageFooter"] != DBNull.Value ? Convert.ToBoolean(dr["IsIncludePageFooter"]) : false;
                    model.IsIncludeProjectCompletionMesssage = dr["IsIncludeProjectCompletionMesssage"] != DBNull.Value ? Convert.ToBoolean(dr["IsIncludeProjectCompletionMesssage"]) : false;
                    model.IsIncludeTerminationMessage = dr["IsIncludeTerminationMessage"] != DBNull.Value ? Convert.ToBoolean(dr["IsIncludeTerminationMessage"]) : false;
                    model.IsIncludeWelcomeMessage = dr["IsIncludeWelcomeMessage"] != DBNull.Value ? Convert.ToBoolean(dr["IsIncludeWelcomeMessage"]) : false;
                    model.ProjectType = dr["ProjectType"] != DBNull.Value ? Convert.ToString(dr["ProjectType"]) : "";
                    //model.ProductMessage = dr["ProductMessage"] != DBNull.Value ? Convert.ToString(dr["ProductMessage"]) : "";
                }

                productProjectBase.ProjectId = Convert.ToInt32(id);
                actionResult = adminAction.ProductProject_LoadById(productProjectBase);
                if (actionResult.IsSuccess)
                {
                    foreach (DataRow dr in actionResult.dtResult.Rows)
                    {
                        lstproductModel.Add(new ProductProjectModel
                        {
                            Id = dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0,
                            ProjectId = dr["ProjectId"] != DBNull.Value ? Convert.ToInt32(dr["ProjectId"]) : 0,
                            ProductLogicalId = dr["ProductLogicalId"] != DBNull.Value ? Convert.ToInt32(dr["ProductLogicalId"]) : 0,
                            Description = dr["Description"] != DBNull.Value ? Convert.ToString(dr["Description"]) : string.Empty,
                            DisplayOrder = dr["DisplayOrder"] != DBNull.Value ? Convert.ToInt32(dr["DisplayOrder"]) : 0
                        });
                    }
                    lstproductModel = lstproductModel.OrderBy(l => l.DisplayOrder).ToList();
                    model.lstProductProjectModel = lstproductModel;

                }

            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return model;
        }
        #endregion

        #region new Method

        #region GetReportDataTableAjax
        [HttpPost]
        public JsonResult GetReportDataTableAjax(string projectGUID = null, string ReportType = "", string ProjectId = null, string GenUpdatedReport = null)
        {
            string[] arr = new string[3];
            string LastUpdate = "";
            int TotalResponses = 0;
            string ReportHtml = "";

            DataTable dt = new DataTable();
            DataTable dt1 = new DataTable();
            int RespCount = 0;

            if (ReportType.ToLower() == "online")
            {
                #region Online Report
                if (GenUpdatedReport == null || GenUpdatedReport == "")
                {
                    actionResult = adminAction.OnlineSegmentation_LoadById(projectGUID);
                    if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                    {
                        #region if Reponse save By Service
                        DataRow dr = actionResult.dtResult.Rows[0];
                        string response = dr["Response"] != DBNull.Value ? dr["Response"].ToString() : "";
                        LastUpdate = dr["CreatedDate"] != DBNull.Value ? dr["CreatedDate"].ToString() : "";
                        ReportHtml = dr["html"] != DBNull.Value ? dr["html"].ToString() : "";
                        dt = ConvertXMLToDataTable(response);
                        #endregion
                    }
                    else {
                        #region Else Reponse not save By Services
                        dt = GetReportDataTable(projectGUID);
                        LastUpdate = DateTime.Now.ToString();

                        if (dt.Columns.Count > 0)
                        {
                            if (ProjectId != null && ProjectId != "")
                            {
                                string onlineResponseXML = ConvertDatatableToXML(dt);

                                ReportHtml = "<table id='hdnReportHtml'>";

                                DataTable dtReport = dt;
                                if (dtReport != null)
                                {
                                    string[] cols = (from dc in dtReport.Columns.Cast<DataColumn>()
                                                     select dc.ColumnName)
                                                   .ToArray();
                                    if (cols != null)
                                    {
                                        ReportHtml += "<tr class='thRow'>";
                                        foreach (var item in cols)
                                        {
                                            ReportHtml += "<th>" + item + "</th>";
                                        }
                                        ReportHtml += "</tr>";
                                    }

                                    foreach (DataRow row in dtReport.Rows)
                                    {
                                        ReportHtml += "<tr class='tdRow'>";
                                        foreach (var item in row.ItemArray)
                                        {
                                            ReportHtml += "<td>" + item + "</td>";
                                        }
                                        ReportHtml += "</tr>";
                                    }
                                }

                                actionResult = adminAction.OnlineSegmentationRespose_Add(Convert.ToInt32(ProjectId), projectGUID, onlineResponseXML, ReportHtml);
                            }
                        }
                        #endregion
                    }
                }
                else
                {
                    #region If Refresh Report
                    dt = GetReportDataTable(projectGUID);
                    LastUpdate = DateTime.Now.ToString();
                    if (dt.Columns.Count > 0)
                    {
                        if (ProjectId != null && ProjectId != "")
                        {
                            string onlineResponseXML = ConvertDatatableToXML(dt);
                            #region Report Data HTML
                            ReportHtml = "<table id='hdnReportHtml'>";

                            DataTable dtReport = dt;
                            if (dtReport != null)
                            {
                                string[] cols = (from dc in dtReport.Columns.Cast<DataColumn>()
                                                 select dc.ColumnName)
                                               .ToArray();
                                if (cols != null)
                                {
                                    ReportHtml += "<tr class='thRow'>";
                                    foreach (var item in cols)
                                    {
                                        ReportHtml += "<th>" + item + "</th>";
                                    }
                                    ReportHtml += "</tr>";
                                }

                                foreach (DataRow row in dtReport.Rows)
                                {
                                    ReportHtml += "<tr class='tdRow'>";
                                    foreach (var item in row.ItemArray)
                                    {
                                        ReportHtml += "<td>" + item + "</td>";
                                    }
                                    ReportHtml += "</tr>";
                                }
                            }
                            #endregion
                            actionResult = adminAction.OnlineSegmentationRespose_Add(Convert.ToInt32(ProjectId), projectGUID, onlineResponseXML, ReportHtml);
                        }
                    }
                    #endregion
                }
                #endregion
            }
            else if (ReportType.ToLower() == "offline")
            {
                #region Offline Report
                if (GenUpdatedReport == null || GenUpdatedReport == "")
                {
                    actionResult = adminAction.OfflineSegmentation_LoadById(projectGUID);
                    if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                    {
                        #region if Reponse save By Service
                        DataRow dr = actionResult.dtResult.Rows[0];
                        string response = dr["Response"] != DBNull.Value ? dr["Response"].ToString() : "";
                        LastUpdate = dr["CreatedDate"] != DBNull.Value ? dr["CreatedDate"].ToString() : "";
                        ReportHtml = dr["html"] != DBNull.Value ? dr["html"].ToString() : "";
                        DataSet ds = new DataSet();
                        using (StringReader stringReader = new StringReader(response))
                        {
                            ds = new DataSet();
                            ds.ReadXml(stringReader);
                        }
                        dt = ds.Tables[0];
                        #endregion
                    }
                    else {
                        #region Else Reponse not save By Services

                        dt = GetReportDataTableOffline(projectGUID, (RespCount + 1));
                        LastUpdate = DateTime.Now.ToString();
                        if (dt.Columns.Count > 0)
                        {
                            if (ProjectId != null && ProjectId != "")
                            {
                                string responseXML = ConvertDatatableToXML(dt);

                                ReportHtml = "<table id='hdnReportHtml'>";

                                DataTable dtReport = dt;
                                if (dtReport != null)
                                {
                                    string[] cols = (from dc in dtReport.Columns.Cast<DataColumn>()
                                                     select dc.ColumnName)
                                                   .ToArray();
                                    if (cols != null)
                                    {
                                        ReportHtml += "<tr class='thRow'>";
                                        foreach (var item in cols)
                                        {
                                            ReportHtml += "<th>" + item + "</th>";
                                        }
                                        ReportHtml += "</tr>";
                                    }

                                    foreach (DataRow row in dtReport.Rows)
                                    {
                                        ReportHtml += "<tr class='tdRow'>";
                                        foreach (var item in row.ItemArray)
                                        {
                                            ReportHtml += "<td>" + item + "</td>";
                                        }
                                        ReportHtml += "</tr>";
                                    }
                                }
                                actionResult = adminAction.OfflineSegmentationRespose_Add(Convert.ToInt32(ProjectId), projectGUID, responseXML, ReportHtml);
                            }
                        }
                        #endregion
                    }
                }
                else
                {
                    #region If Refresh Report
                    dt = GetReportDataTableOffline(projectGUID, (RespCount + 1));
                    LastUpdate = DateTime.Now.ToString();
                    if (dt.Columns.Count > 0)
                    {
                        if (ProjectId != null && ProjectId != "")
                        {
                            string responseXML = ConvertDatatableToXML(dt);

                            #region Report Data HTML
                            ReportHtml = "<table id='hdnReportHtml'>";

                            DataTable dtReport = dt;
                            if (dtReport != null)
                            {
                                string[] cols = (from dc in dtReport.Columns.Cast<DataColumn>()
                                                 select dc.ColumnName)
                                               .ToArray();
                                if (cols != null)
                                {
                                    ReportHtml += "<tr class='thRow'>";
                                    foreach (var item in cols)
                                    {
                                        ReportHtml += "<th>" + item + "</th>";
                                    }
                                    ReportHtml += "</tr>";
                                }

                                foreach (DataRow row in dtReport.Rows)
                                {
                                    ReportHtml += "<tr class='tdRow'>";
                                    foreach (var item in row.ItemArray)
                                    {
                                        ReportHtml += "<td>" + item + "</td>";
                                    }
                                    ReportHtml += "</tr>";
                                }
                            }
                            #endregion

                            actionResult = adminAction.OfflineSegmentationRespose_Add(Convert.ToInt32(ProjectId), projectGUID, responseXML, ReportHtml);
                        }
                    }
                    #endregion
                }
                #endregion
            }
            else if (ReportType.ToLower() == "combined")
            {
                #region Combined Report
                if (GenUpdatedReport == null || GenUpdatedReport == "")
                {
                    actionResult = adminAction.CombinedSegmentationRespose_LoadById(projectGUID);
                    if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                    {
                        #region if Reponse save By Service
                        DataRow dr = actionResult.dtResult.Rows[0];
                        string response = dr["Response"] != DBNull.Value ? dr["Response"].ToString() : "";
                        LastUpdate = dr["CreatedDate"] != DBNull.Value ? dr["CreatedDate"].ToString() : "";
                        ReportHtml = dr["html"] != DBNull.Value ? dr["html"].ToString() : "";
                        DataSet ds = new DataSet();
                        using (StringReader stringReader = new StringReader(response))
                        {
                            ds = new DataSet();
                            ds.ReadXml(stringReader);
                        }
                        dt = ds.Tables[0];
                        #endregion
                    }
                    else {
                        #region Else Reponse not save By Services
                        dt = GetReportDataTable(projectGUID);
                        RespCount = (from DataRow dRow in dt.Rows select dRow["Resp_Id"]).Distinct().Count();
                        dt1 = GetReportDataTableOffline(projectGUID, (RespCount + 1));
                        dt.Merge(dt1);
                        LastUpdate = DateTime.Now.ToString();
                        if (dt.Columns.Count > 0)
                        {
                            if (ProjectId != null && ProjectId != "")
                            {
                                string responseXML = ConvertDatatableToXML(dt);
                                #region Report Data HTML
                                ReportHtml = "<table id='hdnReportHtml'>";

                                DataTable dtReport = dt;
                                if (dtReport != null)
                                {
                                    string[] cols = (from dc in dtReport.Columns.Cast<DataColumn>()
                                                     select dc.ColumnName)
                                                   .ToArray();
                                    if (cols != null)
                                    {
                                        ReportHtml += "<tr class='thRow'>";
                                        foreach (var item in cols)
                                        {
                                            ReportHtml += "<th>" + item + "</th>";
                                        }
                                        ReportHtml += "</tr>";
                                    }

                                    foreach (DataRow row in dtReport.Rows)
                                    {
                                        ReportHtml += "<tr class='tdRow'>";
                                        foreach (var item in row.ItemArray)
                                        {
                                            ReportHtml += "<td>" + item + "</td>";
                                        }
                                        ReportHtml += "</tr>";
                                    }
                                }
                                #endregion
                                actionResult = adminAction.CombineSegmentationRespose_Add(Convert.ToInt32(ProjectId), projectGUID, responseXML, ReportHtml);
                            }
                        }
                        #endregion
                    }
                }
                else {
                    #region If Refresh Report
                    dt = GetReportDataTable(projectGUID);
                    RespCount = (from DataRow dRow in dt.Rows select dRow["Resp_Id"]).Distinct().Count();
                    dt1 = GetReportDataTableOffline(projectGUID, (RespCount + 1));
                    dt.Merge(dt1);
                    LastUpdate = DateTime.Now.ToString();
                    if (dt.Columns.Count > 0)
                    {
                        if (ProjectId != null && ProjectId != "")
                        {
                            string responseXML = ConvertDatatableToXML(dt);
                            #region Report Data HTML
                            ReportHtml = "<table id='hdnReportHtml'>";

                            DataTable dtReport = dt;
                            if (dtReport != null)
                            {
                                string[] cols = (from dc in dtReport.Columns.Cast<DataColumn>()
                                                 select dc.ColumnName)
                                               .ToArray();
                                if (cols != null)
                                {
                                    ReportHtml += "<tr class='thRow'>";
                                    foreach (var item in cols)
                                    {
                                        ReportHtml += "<th>" + item + "</th>";
                                    }
                                    ReportHtml += "</tr>";
                                }

                                foreach (DataRow row in dtReport.Rows)
                                {
                                    ReportHtml += "<tr class='tdRow'>";
                                    foreach (var item in row.ItemArray)
                                    {
                                        ReportHtml += "<td>" + item + "</td>";
                                    }
                                    ReportHtml += "</tr>";
                                }
                            }
                            #endregion
                            actionResult = adminAction.CombineSegmentationRespose_Add(Convert.ToInt32(ProjectId), projectGUID, responseXML, ReportHtml);
                        }
                    }
                    #endregion
                }
                #endregion
            }


            TotalResponses = (from DataRow dRow in dt.Rows select dRow["Resp_Id"]).Distinct().Count();
            arr[0] = TotalResponses.ToString();
            Session["ReportDataTable"] = dt;
            ViewBag.RDT = dt;

            if (ReportHtml == "")
            {
                string ReportDataHtml = "<table id='hdnReportHtml'>";
                if (Session["ReportDataTable"] != null)
                {
                    DataTable dtReport = (DataTable)Session["ReportDataTable"];
                    if (dtReport != null)
                    {
                        string[] cols = (from dc in dtReport.Columns.Cast<DataColumn>()
                                         select dc.ColumnName)
                                       .ToArray();
                        if (cols != null)
                        {
                            ReportDataHtml += "<tr class='thRow'>";
                            foreach (var item in cols)
                            {
                                ReportDataHtml += "<th>" + item + "</th>";
                            }
                            ReportDataHtml += "</tr>";
                        }

                        foreach (DataRow row in dtReport.Rows)
                        {
                            ReportDataHtml += "<tr class='tdRow'>";
                            foreach (var item in row.ItemArray)
                            {
                                ReportDataHtml += "<td>" + item + "</td>";
                            }
                            ReportDataHtml += "</tr>";
                        }
                    }
                }
                ReportDataHtml += "</table>";
                ViewBag.ReportDataHtml = ReportDataHtml;
                arr[1] = ReportDataHtml;
            }
            else
            {
                ViewBag.ReportDataHtml = ReportHtml;
                arr[1] = ReportHtml;
            }
            arr[2] = "Last Updated Report: " + LastUpdate;

            return Json(arr, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region GetReportDataTable
        private DataTable GetReportDataTable(string Id)
        {

            RespondentController respondent = new RespondentController();
            SurveyResponseModel model = new SurveyResponseModel();
            DataTable dtF = new DataTable();
            DataTable newData = new DataTable();
            try
            {
                DataTable dt = new DataTable();

                string projectGuid = string.Empty;
                projectGuid = Id;
                dt.Columns.Add("Resp_Id", typeof(string));

                model = respondent.GetProjectPreview(projectGuid);

                //--ss
                if (model.projectModel.ProjectType == "2")
                {
                    DataRow drnew = dt.NewRow();
                    drnew[0] = "Product_LogicalId";
                    dt.Rows.Add(drnew);
                    DataRow drnew1 = dt.NewRow();
                    drnew1[0] = "Product_Name";
                    dt.Rows.Add(drnew1);
                }
                //--

                // Load all the questions of this project
                List<QuestionList> questionsList = new List<QuestionList>();
                questionBase.ProjectId = Convert.ToInt32(model.projectModel.Id);
                actionResult = adminAction.QuestionsLoadAll_ByProjectId(questionBase);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    foreach (DataRow dr in actionResult.dtResult.Rows)
                    {
                        questionsList.Add(new QuestionList
                        {
                            id = dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0,
                            questionTypeId = dr["QuestionTypeId"] != DBNull.Value ? Convert.ToInt32(dr["QuestionTypeId"]) : 0
                        });
                    }
                }

                string fileName = Guid.NewGuid().ToString().Substring(0, 5) + "_" + DateTime.Now.ToString("MM-dd-yyyy") + "_" + model.projectModel.ProjectName.Replace(" ", "");

                if (model.lstQuestionModel != null && model.lstQuestionModel.Count > 0)
                {
                    foreach (var item in model.lstQuestionModel)
                    {
                        if (item.PipedFrom > 0 && (!item.QuestionTitle.Contains("{{Q}}")))
                        {
                            if (item.QuestionTypeId == 2 || item.QuestionTypeId == 4 || item.QuestionTypeId == 5 || item.QuestionTypeId == 6)//Multi Punch || Constant Sum || Ranking || Matrix Type
                            {
                                if (model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel != null && model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel.Count > 0)
                                {
                                    foreach (var opt in model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel)
                                    {
                                        DataRow drnew = dt.NewRow();
                                        drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                        dt.Rows.Add(drnew);
                                    }
                                }
                            }

                            else if (item.QuestionTypeId == 1 || item.QuestionTypeId == 3 || item.QuestionTypeId == 7 || item.QuestionTypeId == 15)//Single Punch || Range || Simple Text
                            {
                                DataRow drnew = dt.NewRow();
                                drnew[0] = "Q" + item.QuestionId + "_S" + "-" + ReplaceHtml.ReplaceText(item.QuestionTitle);
                                dt.Rows.Add(drnew);
                            }
                            else if (item.QuestionTypeId == 8 || item.QuestionTypeId == 12)//Single punch matrix type  && Rank order type
                            {
                                //if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
                                //{
                                //foreach (var opt in item.lstOptionsModel)
                                //{
                                //    DataRow drnew = dt.NewRow();
                                //    //drnew[0] = "Q" + item.QuestionId + "_M" + opt.OptionId + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                //    drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                //    dt.Rows.Add(drnew);
                                //}
                                //}
                                if (item.lstMatrixTypeModel != null && item.lstMatrixTypeModel.Count > 0)
                                {
                                    foreach (var col in item.lstMatrixTypeModel)
                                    {
                                        DataRow drnew = dt.NewRow();
                                        //drnew[0] = "Q" + item.QuestionId + "_M" + opt.OptionId + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                        drnew[0] = "Q" + item.QuestionId + "_M" + col.DisplayOrder + "-" + ReplaceHtml.ReplaceText(col.ColumnName);
                                        dt.Rows.Add(drnew);
                                    }
                                }
                            }
                            else if (item.QuestionTypeId == 13)//Kano model matrix type 
                            {
                                if (model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel != null && model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel.Count > 0)
                                {
                                    foreach (var opt in model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel)
                                    {
                                        DataRow drnew = dt.NewRow();
                                        drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Present";
                                        dt.Rows.Add(drnew);
                                        DataRow drnew1 = dt.NewRow();
                                        drnew1[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Absent";
                                        dt.Rows.Add(drnew1);
                                    }
                                }
                            }
                            // Single Punch Matrix Type || Range Matrix Type Question ||
                            // Single punch type answer horizontal position || 
                            // Multiple punch type answer horizontal position || 
                            // Rank order type question || 
                            // KANO Model Matrix type question || Dropdown Matrix type question
                            else if (item.QuestionTypeId > 7 && item.QuestionTypeId != 15)
                            {
                                if (model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel != null && model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel.Count > 0)
                                {
                                    foreach (var opt in model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel)
                                    {
                                        foreach (var column in item.lstMatrixTypeModel)
                                        {
                                            DataRow drnew = dt.NewRow();
                                            drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "_C" + column.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + " " + ReplaceHtml.ReplaceText(column.ColumnName);
                                            dt.Rows.Add(drnew);
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (item.QuestionTypeId == 2 || item.QuestionTypeId == 4 || item.QuestionTypeId == 5 || item.QuestionTypeId == 6)//Multi Punch || Constant Sum || Ranking || Matrix Type
                            {
                                if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
                                {
                                    foreach (var opt in item.lstOptionsModel)
                                    {
                                        DataRow drnew = dt.NewRow();
                                        //drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                        drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                        dt.Rows.Add(drnew);
                                    }
                                }
                            }

                            else if (item.QuestionTypeId == 1 || item.QuestionTypeId == 3 || item.QuestionTypeId == 7 || item.QuestionTypeId == 15)//Single Punch || Range || Simple Text
                            {
                                DataRow drnew = dt.NewRow();
                                //drnew[0] = "Q" + item.DisplayOrder + "_S" + "-" + ReplaceHtml.ReplaceText(item.QuestionTitle);
                                drnew[0] = "Q" + item.QuestionId + "_S" + "-" + ReplaceHtml.ReplaceText(item.QuestionTitle);
                                dt.Rows.Add(drnew);
                            }
                            else if (item.QuestionTypeId == 8 || item.QuestionTypeId == 12)//Single punch matrix type  && Rank order type
                            {
                                //if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
                                //{
                                //foreach (var opt in item.lstOptionsModel)
                                //{
                                //    DataRow drnew = dt.NewRow();
                                //    //drnew[0] = "Q" + item.QuestionId + "_M" + opt.OptionId + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                //    drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                //    dt.Rows.Add(drnew);
                                //}
                                //}
                                if (item.lstMatrixTypeModel != null && item.lstMatrixTypeModel.Count > 0)
                                {
                                    foreach (var col in item.lstMatrixTypeModel)
                                    {
                                        DataRow drnew = dt.NewRow();
                                        //drnew[0] = "Q" + item.QuestionId + "_M" + opt.OptionId + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                        drnew[0] = "Q" + item.QuestionId + "_M" + col.DisplayOrder + "-" + ReplaceHtml.ReplaceText(col.ColumnName);
                                        dt.Rows.Add(drnew);
                                    }
                                }
                            }
                            else if (item.QuestionTypeId == 13)//Kano model matrix type 
                            {
                                if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
                                {
                                    foreach (var opt in item.lstOptionsModel)
                                    {
                                        DataRow drnew = dt.NewRow();
                                        //drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Present";
                                        drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Present";
                                        dt.Rows.Add(drnew);
                                        DataRow drnew1 = dt.NewRow();
                                        //drnew1[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Absent";
                                        drnew1[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Absent";
                                        dt.Rows.Add(drnew1);
                                    }
                                }
                            }
                            else if (item.QuestionTypeId == 16) // Description type Question
                            {
                            }
                            // Single Punch Matrix Type || Range Matrix Type Question ||
                            // Single punch type answer horizontal position || 
                            // Multiple punch type answer horizontal position || 
                            // Rank order type question || 
                            // KANO Model Matrix type question || Dropdown Matrix type question
                            else if (item.QuestionTypeId > 7)
                            {
                                if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
                                {
                                    foreach (var opt in item.lstOptionsModel)
                                    {
                                        foreach (var column in item.lstMatrixTypeModel)
                                        {
                                            DataRow drnew = dt.NewRow();
                                            //drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "_C" + column.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + " " + ReplaceHtml.ReplaceText(column.ColumnName);
                                            drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "_C" + column.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + " " + ReplaceHtml.ReplaceText(column.ColumnName);
                                            dt.Rows.Add(drnew);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                // Get the Excel Pattern of dt
                dtF = comMethods.GenerateTransposedTable(dt);

                // Get all the reponders
                projectBase.Id = model.projectModel.Id;
                actionResult = adminAction.CompleteRespondersLoad_ByProjectId(projectBase);
                List<RespondersModel> lstResponders = new List<RespondersModel>();
                foreach (DataRow dr in actionResult.dtResult.Rows)
                {
                    lstResponders.Add(new RespondersModel
                    {

                        InvitationGuid = dr["InvitationGuid"] != DBNull.Value ? Convert.ToString(dr["InvitationGuid"]) : "",
                        Email = dr["Email"] != DBNull.Value ? Convert.ToString(dr["Email"]) : "",

                    });
                }
                IEnumerable<RespondersModel> filteredList = lstResponders
                 .GroupBy(customer => customer.InvitationGuid).Select(group => group.First());
                // Get Responses
                SurveyLoadBase surveyLoadBase = new SurveyLoadBase();
                string json = string.Empty;
                SRV.BaseLayer.ActionResult actionResultResponses = new SRV.BaseLayer.ActionResult();
                int response_id = 1;            //ss
                foreach (var responder in filteredList)
                {
                    DataTable responsesDt = new DataTable();
                    surveyLoadBase.Id = model.projectModel.Id;
                    surveyLoadBase.Email = (!string.IsNullOrEmpty(responder.Email) ? Convert.ToString(responder.Email) : "");
                    surveyLoadBase.ExternalResponderId = (!string.IsNullOrEmpty(responder.InvitationGuid) ? Convert.ToString(responder.InvitationGuid) : "");
                    string tempOnlineResponderId = (!string.IsNullOrEmpty(responder.InvitationGuid) ? Convert.ToString(responder.InvitationGuid) : "");
                    if (model.projectModel.lstProductProjectModel != null && model.projectModel.lstProductProjectModel.Count > 0)
                    {
                        foreach (var productModel in model.projectModel.lstProductProjectModel)
                        {
                            surveyLoadBase.ProductId = productModel.Id;
                            actionResultResponses = respondentAction.Response_LoadByProjectId(surveyLoadBase);
                            if (actionResultResponses.IsSuccess)
                            {
                                if (responsesDt.Rows.Count == 0)
                                    responsesDt = actionResultResponses.dtResult.Copy();
                                else
                                {
                                    foreach (DataRow drThisRespRow in actionResultResponses.dtResult.Rows)
                                        responsesDt.ImportRow(drThisRespRow);
                                }
                            }
                        }
                    }
                    else
                    {
                        actionResultResponses = respondentAction.Response_LoadByProjectId(surveyLoadBase);
                        if (actionResultResponses.IsSuccess)
                            responsesDt = actionResultResponses.dtResult.Copy();

                    }

                    // surveyLoadBase.OfflineResponderId = dr["RespId"] != DBNull.Value ? Convert.ToString(dr["RespId"]) : "";
                    //DataRow dr_dtF = dtF.NewRow();
                    // if (String.IsNullOrEmpty(surveyLoadBase.Email)) continue;

                    //actionResultResponses = respondentAction.Response_LoadByProjectId(surveyLoadBase);
                    if (responsesDt.Rows.Count > 0)//actionResultResponses.IsSuccess
                    {
                        //dr_dtF[0] = surveyLoadBase.Email;

                        int count = 1;
                        count = (model.projectModel.ProjectType == "2") ? count + 2 : count;        //ss
                        DataRow dr_dtF = dtF.NewRow();
                        DataRow drTemp = dtF.NewRow();
                        string product_desc = string.Empty;
                        string product_logiId = string.Empty;
                        int oldProductID = responsesDt.Rows[0]["ProductId"] != DBNull.Value ? Convert.ToInt32(responsesDt.Rows[0]["ProductId"]) : 0;
                        foreach (DataRow drResponse in responsesDt.Rows)// actionResultResponses.dtResult.Rows
                        {

                            int optionsCount = 0;
                            // response_id = drResponse["ReponderId"] != DBNull.Value ? (responsesDt.Rows[0]["ReponderId"]).ToString() : "";
                            int productId = drResponse["ProductId"] != DBNull.Value ? Convert.ToInt32(drResponse["ProductId"]) : 0;
                            if (productId != oldProductID)
                            {
                                count = (model.projectModel.ProjectType == "2") ? 3 : 1;
                                dr_dtF = dtF.NewRow();
                            }

                            //--ss

                            product_desc = drResponse["ProductDescription"] != DBNull.Value ? Convert.ToString(drResponse["ProductDescription"]) : "";
                            product_logiId = drResponse["ProductLogicalId"] != DBNull.Value ? Convert.ToString(drResponse["ProductLogicalId"]) : "";

                            //--ss
                            int questionId = drResponse["QuestionId"] != DBNull.Value ? Convert.ToInt32(drResponse["QuestionId"]) : 0;
                            string answerExpression = drResponse["AnswerExpression"] != DBNull.Value ? Convert.ToString(drResponse["AnswerExpression"]) : "";
                            int questionTypeId = drResponse["QuestionTypeId"] != DBNull.Value ? Convert.ToInt32(drResponse["QuestionTypeId"]) : 1;
                            int PipedFrom = drResponse["PipedFrom"] != DBNull.Value ? Convert.ToInt32(drResponse["PipedFrom"]) : 0;
                            //dr_dtF[0] = drResponse;

                            // Get Total options count for this question
                            questionBase.Id = Convert.ToInt32(questionId);
                            SRV.BaseLayer.ActionResult actionResultAnswersTemp = new SRV.BaseLayer.ActionResult();
                            actionResultAnswersTemp = adminAction.Option_LoadAllById(questionBase);
                            if (actionResultAnswersTemp.dtResult != null && actionResultAnswersTemp.dtResult.Rows.Count > 0)
                            {
                                optionsCount = actionResultAnswersTemp.dtResult.Rows.Count;
                            }

                            if (questionTypeId == 2 || questionTypeId == 4 || questionTypeId == 5 || questionTypeId == 6)
                            {
                                string[] allAnswers = answerExpression.Split('|');
                                if (allAnswers.Length > 0)
                                {
                                    if (questionTypeId == 2)// Multi Punch
                                    {
                                        string allRec = string.Empty;
                                        List<OptionsModel> matrixAns = new List<OptionsModel>();
                                        if (PipedFrom > 0 && !drResponse["QuestionTitle"].ToString().Contains("{{Q}}"))
                                        {
                                            matrixAns = model.lstQuestionModel.Where(v => v.QuestionId == PipedFrom).FirstOrDefault().lstOptionsModel;
                                        }
                                        else
                                        {
                                            matrixAns = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                        }

                                        foreach (var item in matrixAns)
                                        {
                                            string matchedId = string.Empty;
                                            foreach (string ans in allAnswers)
                                            {
                                                if (Convert.ToString(item.OptionId) == ans)
                                                {
                                                    matchedId = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            allRec += matchedId + "|";
                                        }

                                        string[] allAnswersMat = allRec.Substring(0, allRec.Length - 1).Split('|');
                                        foreach (string ans in allAnswersMat)
                                        {

                                            if (count <= dtF.Columns.Count - 1)
                                            {
                                                if ((Regex.Replace(dtF.Columns[count].ToString().Split('-')[0].Split('_')[1], @"[^\d]", String.Empty)) == ans)
                                                {
                                                    dr_dtF[count] = "1";// ans;
                                                    count++;
                                                    optionsCount--;
                                                }
                                                else
                                                {
                                                    dr_dtF[count] = "2";
                                                    count++;
                                                    optionsCount--;
                                                }
                                            }
                                        }
                                        //count = count + optionsCount;
                                    }
                                    else if (questionTypeId == 6) // matrix Type
                                    {
                                        foreach (string ans in allAnswers)
                                        {
                                            dr_dtF[count] = (ans.ToLower().IndexOf("yes") != -1) ? "1" : "2";
                                            count++;
                                            optionsCount--;
                                        }
                                        count = count + optionsCount;
                                    }
                                    else
                                    {
                                        foreach (string ans in allAnswers)
                                        {
                                            dr_dtF[count] = ans;
                                            count++;
                                            optionsCount--;
                                        }
                                        count = count + optionsCount;
                                    }
                                }
                            }
                            else if (questionTypeId == 1) // Single Punch Answers
                            {
                                //List<OptionsModel> SinglePunchAns = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                List<OptionsModel> SinglePunchAns = new List<OptionsModel>();
                                if (PipedFrom > 0 && !drResponse["QuestionTitle"].ToString().Contains("{{Q}}"))
                                {
                                    SinglePunchAns = model.lstQuestionModel.Where(v => v.QuestionId == PipedFrom).FirstOrDefault().lstOptionsModel;
                                }
                                else
                                {
                                    SinglePunchAns = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                }
                                int ansIndex = 0;
                                foreach (var item in SinglePunchAns)
                                {
                                    ansIndex++;
                                    if (Convert.ToString(item.OptionId) == answerExpression)
                                    {
                                        break;
                                    }
                                }

                                dr_dtF[count] = ansIndex;//answerExpression;
                                count++;
                                optionsCount--;
                            }
                            else if (questionTypeId == 8 || questionTypeId == 12) // single punch matrix answers
                            {
                                List<OptionsModel> optionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                List<MatrixTypeModel> columnDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstMatrixTypeModel;
                                int qustionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).Select(v => v.DisplayOrder).FirstOrDefault();
                                string[] allAnswers = answerExpression.Split('|');

                                List<string> lstColumnOrdinals = new List<string>();

                                string formattedAnswer = string.Empty;
                                string answerValue = string.Empty;
                                if (allAnswers.Length > 0)
                                {
                                    foreach (var ansExpression in allAnswers)
                                    {
                                        string displaycol = string.Empty;
                                        string displayopt = string.Empty;
                                        foreach (var item in optionDisplayOrder)
                                        {

                                            if (Convert.ToString(item.OptionId) == ansExpression.Split('~')[0])
                                            {
                                                displaycol = Convert.ToString(item.DisplayOrder);
                                                break;
                                            }
                                        }
                                        foreach (var item in columnDisplayOrder)
                                        {
                                            if (Convert.ToString(item.Id) == ansExpression.Split('~')[1])
                                            {
                                                displayopt = Convert.ToString(item.DisplayOrder);
                                                break;
                                            }
                                        }
                                        //  formattedAnswer = "Q" + questionId + "_M" + displaycol + "#" + displayopt;
                                        formattedAnswer = "Q" + questionId + "_M" + displayopt + "#" + displaycol;
                                        foreach (DataColumn column in dtF.Columns)
                                        {
                                            string[] columnHeader = column.ColumnName.Split('-');
                                            string[] answer = columnHeader[0].Split('_');
                                            if (columnHeader.Length > 1)
                                            {
                                                string answerInHeader = answer[0] + "_" + answer[1];
                                                string[] formattedAnswerArr = formattedAnswer.Split('#');
                                                if (formattedAnswerArr[0] == answerInHeader)
                                                {
                                                    int index = dr_dtF.Table.Columns[column.ColumnName].Ordinal;
                                                    lstColumnOrdinals.Add(index.ToString());
                                                    count++;
                                                    dr_dtF[index] = formattedAnswerArr[1];
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            else if (questionTypeId == 13) // Kano model matrix type
                            {
                                List<OptionsModel> optionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                List<MatrixTypeModel> columnDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstMatrixTypeModel;
                                int qustionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).Select(v => v.DisplayOrder).FirstOrDefault();
                                string[] allAnswers = answerExpression.Split('|');

                                List<string> lstColumnOrdinals = new List<string>();

                                string formattedAnswer = string.Empty;
                                string answerValue = string.Empty;
                                if (allAnswers.Length > 1)
                                {
                                    int repeatId = 0;
                                    foreach (var ansExpression in allAnswers)
                                    {
                                        string displaycol = string.Empty;
                                        string displayopt = string.Empty;
                                        string displayText = string.Empty;
                                        string text = string.Empty;
                                        if (repeatId == Convert.ToInt32(ansExpression.Split('~')[0]))
                                        {
                                            text = "_Absent";
                                            repeatId = 0;
                                        }
                                        else
                                        {
                                            text = "_Present";
                                        }
                                        foreach (var item in optionDisplayOrder)
                                        {
                                            if (Convert.ToString(item.OptionId) == ansExpression.Split('~')[0])
                                            {
                                                displayText = Convert.ToString(item.Option) + text;
                                                displaycol = Convert.ToString(item.DisplayOrder);
                                                repeatId = Convert.ToInt32(ansExpression.Split('~')[0]);
                                                break;
                                            }
                                        }
                                        foreach (var item in columnDisplayOrder)
                                        {
                                            if (Convert.ToString(item.Id) == ansExpression.Split('~')[1])
                                            {
                                                displayopt = Convert.ToString(item.DisplayOrder);
                                                break;
                                            }
                                        }
                                        //formattedAnswer = "Q" + qustionDisplayOrder + "_M" + displaycol + "-" + displayText + "#" + displayopt;
                                        formattedAnswer = "Q" + questionId + "_M" + displaycol + "-" + displayText + "#" + displayopt;
                                        foreach (DataColumn column in dtF.Columns)
                                        {

                                            string[] columnHeader = column.ColumnName.Split('-');
                                            string[] answer = columnHeader[0].Split('_');
                                            if (columnHeader.Length > 1)
                                            {
                                                string answerInHeader = answer[0] + "_" + answer[1] + "-" + columnHeader[1];
                                                string[] formattedAnswerArr = formattedAnswer.Split('#');
                                                if (Regex.Replace(formattedAnswerArr[0], @"\s+", "", RegexOptions.Multiline) == Regex.Replace(answerInHeader, @"\s+", "", RegexOptions.Multiline))
                                                {
                                                    int index = dr_dtF.Table.Columns[column.ColumnName].Ordinal;
                                                    lstColumnOrdinals.Add(index.ToString());
                                                    count++;
                                                    dr_dtF[index] = formattedAnswerArr[1];
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            else if (questionTypeId == 16) // Description type Question
                            {
                            }
                            else if (questionTypeId > 7 && questionTypeId != 15)
                            {
                                List<OptionsModel> optionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                List<MatrixTypeModel> columnDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstMatrixTypeModel;
                                List<DDLMatrixTypeModel> dropDownDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstDDlMatrixModel;
                                //count = count + (optionDisplayOrder.Count * columnDisplayOrder.Count);
                                int optDisplayOrder = 0;
                                if (optionDisplayOrder != null)
                                {
                                    optDisplayOrder = optionDisplayOrder.Count;
                                }
                                int colDisplayOrder = 0;
                                if (columnDisplayOrder != null)
                                {
                                    colDisplayOrder = columnDisplayOrder.Count;
                                }

                                int qustionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).Select(v => v.DisplayOrder).FirstOrDefault();
                                string[] allAnswers = answerExpression.Split('|');

                                List<string> lstColumnOrdinals = new List<string>();

                                string formattedAnswer = string.Empty;
                                string answerValue = string.Empty;
                                int DDlDisplayOrder = 0;
                                if (allAnswers.Length > 1)
                                {
                                    foreach (var ansExpression in allAnswers)
                                    {
                                        string displaycol = string.Empty;
                                        string displayopt = string.Empty;

                                        if (questionTypeId == 9 || questionTypeId == 14)
                                        {
                                            foreach (var item in optionDisplayOrder)
                                            {
                                                if (Convert.ToString(item.OptionId) == ansExpression.Split('~')[0].Split('*')[0])
                                                {
                                                    displaycol = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            foreach (var item in columnDisplayOrder)
                                            {
                                                if (Convert.ToString(item.Id) == ansExpression.Split('~')[1])
                                                {
                                                    displayopt = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            //formattedAnswer = "Q" + qustionDisplayOrder + "_M" + displaycol + "_C" + displayopt;
                                            formattedAnswer = "Q" + questionId + "_M" + displaycol + "_C" + displayopt;
                                            answerValue = ansExpression.Split('~')[0].Split('*')[1];
                                            if (questionTypeId == 14)
                                                DDlDisplayOrder = dropDownDisplayOrder.Where(l => l.Id == Convert.ToInt32(answerValue)).FirstOrDefault().DDLDisplayOrder;
                                        }
                                        else
                                        {
                                            foreach (var item in optionDisplayOrder)
                                            {
                                                if (Convert.ToString(item.OptionId) == ansExpression.Split('~')[0])
                                                {
                                                    displaycol = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            foreach (var item in columnDisplayOrder)
                                            {
                                                if (Convert.ToString(item.Id) == ansExpression.Split('~')[1])
                                                {
                                                    displayopt = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            //formattedAnswer = "Q" + qustionDisplayOrder + "_M" + displaycol + "_C" + displayopt;
                                            formattedAnswer = "Q" + questionId + "_M" + displaycol + "_C" + displayopt;
                                        }
                                        foreach (DataColumn column in dtF.Columns)
                                        {
                                            string[] columnHeader = column.ColumnName.Split('-');

                                            if (columnHeader.Length > 1)
                                            {
                                                string[] answer = columnHeader[0].Split('_');

                                                if (answer.Length > 2)
                                                {
                                                    string answerInHeader = answer[0] + "_" + answer[1] + "_" + answer[2];

                                                    if (formattedAnswer == answerInHeader)
                                                    {
                                                        int index = dr_dtF.Table.Columns[column.ColumnName].Ordinal;
                                                        lstColumnOrdinals.Add(index.ToString());
                                                        //count++;
                                                        if (questionTypeId == 9)
                                                        {
                                                            dr_dtF[index] = answerValue;
                                                        }
                                                        else if (questionTypeId == 14)
                                                        {
                                                            dr_dtF[index] = DDlDisplayOrder;
                                                        }
                                                        else
                                                        {
                                                            dr_dtF[index] = "1";
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }

                                // Fill All the Empty cells with 2

                                for (int i = 0; i <= dtF.Columns.Count - 1; i++)
                                    if (String.IsNullOrEmpty(Convert.ToString(dr_dtF[i])))
                                        dr_dtF[i] = "";
                            }

                            else
                            {
                                dr_dtF[count] = answerExpression;
                                count++;
                                optionsCount--;
                            }

                            if (productId != oldProductID)
                            {
                                dtF.Rows.Add(drTemp);
                                //  drTemp[0] = dtF.Rows.IndexOf(drTemp) + 1;
                                //   drTemp[0] = response_id;
                                drTemp[0] = tempOnlineResponderId;
                                oldProductID = productId;
                            }

                            drTemp = dr_dtF;
                            //---ss
                            if (model.projectModel.ProjectType == "2")
                            {
                                drTemp[2] = product_desc;
                                drTemp[1] = product_logiId;
                            }
                            //---ss

                        }

                        dtF.Rows.Add(dr_dtF);
                        //dr_dtF[0] = dtF.Rows.IndexOf(dr_dtF) + 1;
                        //drTemp[0] = response_id;
                        drTemp[0] = tempOnlineResponderId;
                        //---ss
                        if (model.projectModel.ProjectType == "2")
                        {
                            dr_dtF[2] = product_desc;
                            dr_dtF[1] = product_logiId;
                        }
                        // response_id++;
                        //---ss
                    }
                }

                if (dtF != null && dtF.Rows.Count > 0)
                {
                    //DataTable dtF = comMethods.GenerateTransposedTable(dt);
                    newData = comMethods.GenerateTransposedTable(dt);
                    foreach (DataRow dr in dtF.Rows)
                    {
                        if (model.projectModel.ProjectType == "2")
                        {
                            DataRow rw = newData.AsEnumerable().FirstOrDefault(tt => tt.Field<string>("Resp_Id") == dr["Resp_Id"].ToString() && tt.Field<string>("Product_Name") == dr["Product_Name"].ToString());
                            if (rw == null)
                            {
                                DataRow dr_dtF = newData.NewRow();

                                newData.Rows.Add(dr_dtF);
                                foreach (DataColumn dcTempKano in dtF.Columns)
                                {

                                    dr_dtF[dcTempKano.ToString()] = dr[dcTempKano.ToString()];

                                }
                                // row exists
                            }
                        }
                        else
                        {
                            DataRow rw = newData.AsEnumerable().FirstOrDefault(tt => tt.Field<string>("Resp_Id") == dr["Resp_Id"].ToString());
                            if (rw == null)
                            {
                                DataRow dr_dtF = newData.NewRow();

                                newData.Rows.Add(dr_dtF);
                                foreach (DataColumn dcTempKano in dtF.Columns)
                                {

                                    dr_dtF[dcTempKano.ToString()] = dr[dcTempKano.ToString()];

                                }
                                // row exists
                            }
                        }


                    }
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return newData;
        }
        #endregion

        #region GetReportDataTableOffline
        private DataTable GetReportDataTableOffline(string Id, int ResponseCount)
        {

            RespondentController respondent = new RespondentController();
            SurveyResponseModel model = new SurveyResponseModel();
            DataTable dtF = new DataTable();
            try
            {
                DataTable dt = new DataTable();

                string projectGuid = string.Empty;
                projectGuid = Id;
                dt.Columns.Add("Resp_Id", typeof(string));

                model = respondent.GetProjectPreview(projectGuid);

                //--ss
                if (model.projectModel.ProjectType == "2")
                {
                    DataRow drnew = dt.NewRow();
                    drnew[0] = "Product_LogicalId";
                    dt.Rows.Add(drnew);
                    DataRow drnew1 = dt.NewRow();
                    drnew1[0] = "Product_Name";
                    dt.Rows.Add(drnew1);
                }
                //--

                // Load all the questions of this project
                List<QuestionList> questionsList = new List<QuestionList>();
                questionBase.ProjectId = Convert.ToInt32(model.projectModel.Id);
                actionResult = adminAction.QuestionsLoadAll_ByProjectId(questionBase);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    foreach (DataRow dr in actionResult.dtResult.Rows)
                    {
                        questionsList.Add(new QuestionList
                        {
                            id = dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0,
                            questionTypeId = dr["QuestionTypeId"] != DBNull.Value ? Convert.ToInt32(dr["QuestionTypeId"]) : 0
                        });
                    }
                }

                string fileName = Guid.NewGuid().ToString().Substring(0, 5) + "_" + DateTime.Now.ToString("MM-dd-yyyy") + "_" + model.projectModel.ProjectName.Replace(" ", "");

                if (model.lstQuestionModel != null && model.lstQuestionModel.Count > 0)
                {
                    foreach (var item in model.lstQuestionModel)
                    {
                        if (item.PipedFrom > 0 && (!item.QuestionTitle.Contains("{{Q}}")))
                        {
                            if (item.QuestionTypeId == 2 || item.QuestionTypeId == 4 || item.QuestionTypeId == 5 || item.QuestionTypeId == 6)//Multi Punch || Constant Sum || Ranking || Matrix Type
                            {
                                if (model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel != null && model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel.Count > 0)
                                {
                                    foreach (var opt in model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel)
                                    {
                                        DataRow drnew = dt.NewRow();
                                        drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                        dt.Rows.Add(drnew);
                                    }
                                }
                            }

                            else if (item.QuestionTypeId == 1 || item.QuestionTypeId == 3 || item.QuestionTypeId == 7 || item.QuestionTypeId == 15)//Single Punch || Range || Simple Text
                            {
                                DataRow drnew = dt.NewRow();
                                drnew[0] = "Q" + item.QuestionId + "_S" + "-" + ReplaceHtml.ReplaceText(item.QuestionTitle);
                                dt.Rows.Add(drnew);
                            }
                            else if (item.QuestionTypeId == 8 || item.QuestionTypeId == 12)//Single punch matrix type  && Rank order type
                            {
                                //if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
                                //{
                                //foreach (var opt in item.lstOptionsModel)
                                //{
                                //    DataRow drnew = dt.NewRow();
                                //    //drnew[0] = "Q" + item.QuestionId + "_M" + opt.OptionId + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                //    drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                //    dt.Rows.Add(drnew);
                                //}
                                //}
                                if (item.lstMatrixTypeModel != null && item.lstMatrixTypeModel.Count > 0)
                                {
                                    foreach (var col in item.lstMatrixTypeModel)
                                    {
                                        DataRow drnew = dt.NewRow();
                                        //drnew[0] = "Q" + item.QuestionId + "_M" + opt.OptionId + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                        drnew[0] = "Q" + item.QuestionId + "_M" + col.DisplayOrder + "-" + ReplaceHtml.ReplaceText(col.ColumnName);
                                        dt.Rows.Add(drnew);
                                    }
                                }
                            }
                            else if (item.QuestionTypeId == 13)//Kano model matrix type 
                            {
                                if (model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel != null && model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel.Count > 0)
                                {
                                    foreach (var opt in model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel)
                                    {
                                        DataRow drnew = dt.NewRow();
                                        drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Present";
                                        dt.Rows.Add(drnew);
                                        DataRow drnew1 = dt.NewRow();
                                        drnew1[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Absent";
                                        dt.Rows.Add(drnew1);
                                    }
                                }
                            }
                            else if (item.QuestionTypeId == 16) // Description type Question
                            {
                            }
                            // Single Punch Matrix Type || Range Matrix Type Question ||
                            // Single punch type answer horizontal position || 
                            // Multiple punch type answer horizontal position || 
                            // Rank order type question || 
                            // KANO Model Matrix type question || Dropdown Matrix type question
                            else if (item.QuestionTypeId > 7 && item.QuestionTypeId != 15)
                            {
                                if (model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel != null && model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel.Count > 0)
                                {
                                    foreach (var opt in model.lstQuestionModel.Where(m => m.QuestionId == item.PipedFrom).FirstOrDefault().lstOptionsModel)
                                    {
                                        foreach (var column in item.lstMatrixTypeModel)
                                        {
                                            DataRow drnew = dt.NewRow();
                                            drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "_C" + column.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + " " + ReplaceHtml.ReplaceText(column.ColumnName);
                                            dt.Rows.Add(drnew);
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (item.QuestionTypeId == 2 || item.QuestionTypeId == 4 || item.QuestionTypeId == 5 || item.QuestionTypeId == 6)//Multi Punch || Constant Sum || Ranking || Matrix Type
                            {
                                if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
                                {
                                    foreach (var opt in item.lstOptionsModel)
                                    {
                                        DataRow drnew = dt.NewRow();
                                        //drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                        drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                        dt.Rows.Add(drnew);
                                    }
                                }
                            }

                            else if (item.QuestionTypeId == 1 || item.QuestionTypeId == 3 || item.QuestionTypeId == 7 || item.QuestionTypeId == 15)//Single Punch || Range || Simple Text
                            {
                                DataRow drnew = dt.NewRow();
                                //drnew[0] = "Q" + item.DisplayOrder + "_S" + "-" + ReplaceHtml.ReplaceText(item.QuestionTitle);
                                drnew[0] = "Q" + item.QuestionId + "_S" + "-" + ReplaceHtml.ReplaceText(item.QuestionTitle);
                                dt.Rows.Add(drnew);
                            }
                            else if (item.QuestionTypeId == 8 || item.QuestionTypeId == 12)//Single punch matrix type  && Rank order type
                            {
                                //if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
                                //{
                                //foreach (var opt in item.lstOptionsModel)
                                //{
                                //    DataRow drnew = dt.NewRow();
                                //    //drnew[0] = "Q" + item.QuestionId + "_M" + opt.OptionId + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                //    drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                //    dt.Rows.Add(drnew);
                                //}
                                //}
                                if (item.lstMatrixTypeModel != null && item.lstMatrixTypeModel.Count > 0)
                                {
                                    foreach (var col in item.lstMatrixTypeModel)
                                    {
                                        DataRow drnew = dt.NewRow();
                                        //drnew[0] = "Q" + item.QuestionId + "_M" + opt.OptionId + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                        drnew[0] = "Q" + item.QuestionId + "_M" + col.DisplayOrder + "-" + ReplaceHtml.ReplaceText(col.ColumnName);
                                        dt.Rows.Add(drnew);
                                    }
                                }
                            }
                            else if (item.QuestionTypeId == 13)//Kano model matrix type 
                            {
                                if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
                                {
                                    foreach (var opt in item.lstOptionsModel)
                                    {
                                        DataRow drnew = dt.NewRow();
                                        //drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Present";
                                        drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Present";
                                        dt.Rows.Add(drnew);
                                        DataRow drnew1 = dt.NewRow();
                                        //drnew1[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Absent";
                                        drnew1[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Absent";
                                        dt.Rows.Add(drnew1);
                                    }
                                }
                            }
                            // Single Punch Matrix Type || Range Matrix Type Question ||
                            // Single punch type answer horizontal position || 
                            // Multiple punch type answer horizontal position || 
                            // Rank order type question || 
                            // KANO Model Matrix type question || Dropdown Matrix type question
                            else if (item.QuestionTypeId == 16) // Description type Question
                            {
                            }
                            else if (item.QuestionTypeId > 7)
                            {
                                if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
                                {
                                    foreach (var opt in item.lstOptionsModel)
                                    {
                                        foreach (var column in item.lstMatrixTypeModel)
                                        {
                                            DataRow drnew = dt.NewRow();
                                            //drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "_C" + column.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + " " + ReplaceHtml.ReplaceText(column.ColumnName);
                                            drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "_C" + column.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + " " + ReplaceHtml.ReplaceText(column.ColumnName);
                                            dt.Rows.Add(drnew);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                // Get the Excel Pattern of dt
                dtF = comMethods.GenerateTransposedTable(dt);

                // Get all the reponders
                projectBase.Id = model.projectModel.Id;
                actionResult = adminAction.Responders_Offline_LoadBy_ProjectId(projectBase);

                // Get Responses
                SurveyLoadBase surveyLoadBase = new SurveyLoadBase();
                string json = string.Empty;
                SRV.BaseLayer.ActionResult actionResultResponses = new SRV.BaseLayer.ActionResult();
                int response_id = ResponseCount;            //ss
                foreach (DataRow dr in actionResult.dtResult.Rows)
                {
                    surveyLoadBase.Id = model.projectModel.Id;
                    surveyLoadBase.OfflineResponderId = dr["RespId"] != DBNull.Value ? Convert.ToString(dr["RespId"]) : "";
                    string tempOfflineResponderId = dr["RespId"] != DBNull.Value ? Convert.ToString(dr["RespId"]) : "";
                    //DataRow dr_dtF = dtF.NewRow();

                    if (String.IsNullOrEmpty(surveyLoadBase.OfflineResponderId)) continue;


                    DataTable responsesDt = new DataTable();


                    if (model.projectModel.lstProductProjectModel != null && model.projectModel.lstProductProjectModel.Count > 0)
                    {
                        foreach (var productModel in model.projectModel.lstProductProjectModel)
                        {
                            surveyLoadBase.ProductId = productModel.Id;
                            actionResultResponses = respondentAction.Response_Offline_LoadBy_ProjectId_RespId(surveyLoadBase);
                            if (actionResultResponses.IsSuccess)
                            {
                                if (responsesDt.Rows.Count == 0)
                                    responsesDt = actionResultResponses.dtResult.Copy();
                                else
                                {
                                    foreach (DataRow drThisRespRow in actionResultResponses.dtResult.Rows)
                                        responsesDt.ImportRow(drThisRespRow);

                                }
                            }
                        }
                    }
                    else
                    {
                        actionResultResponses = respondentAction.Response_Offline_LoadBy_ProjectId_RespId(surveyLoadBase);
                        if (actionResultResponses.IsSuccess)
                            responsesDt = actionResultResponses.dtResult.Copy();

                    }


                    //actionResultResponses = respondentAction.Response_Offline_LoadBy_ProjectId_RespId(surveyLoadBase);
                    if (responsesDt.Rows.Count > 0)//actionResultResponses.IsSuccess
                    {
                        //dr_dtF[0] = surveyLoadBase.Email;

                        int count = 1;
                        count = (model.projectModel.ProjectType == "2") ? count + 2 : count;        //ss
                        DataRow dr_dtF = dtF.NewRow();
                        DataRow drTemp = dtF.NewRow();
                        string product_desc = string.Empty;
                        string product_logiId = string.Empty;
                        int oldProductID = responsesDt.Rows[0]["ProductId"] != DBNull.Value ? Convert.ToInt32(responsesDt.Rows[0]["ProductId"]) : 0;
                        foreach (DataRow drResponse in responsesDt.Rows)// actionResultResponses.dtResult.Rows
                        {

                            int optionsCount = 0;

                            int productId = drResponse["ProductId"] != DBNull.Value ? Convert.ToInt32(drResponse["ProductId"]) : 0;
                            if (productId != oldProductID)
                            {
                                count = (model.projectModel.ProjectType == "2") ? 3 : 1;
                                dr_dtF = dtF.NewRow();
                            }

                            //--ss

                            product_desc = drResponse["ProductDescription"] != DBNull.Value ? Convert.ToString(drResponse["ProductDescription"]) : "";
                            product_logiId = drResponse["ProductLogicalId"] != DBNull.Value ? Convert.ToString(drResponse["ProductLogicalId"]) : "";

                            //--ss
                            int questionId = drResponse["QuestionId"] != DBNull.Value ? Convert.ToInt32(drResponse["QuestionId"]) : 0;
                            string answerExpression = drResponse["AnswerExpression"] != DBNull.Value ? Convert.ToString(drResponse["AnswerExpression"]) : "";
                            int questionTypeId = drResponse["QuestionTypeId"] != DBNull.Value ? Convert.ToInt32(drResponse["QuestionTypeId"]) : 1;
                            int PipedFrom = drResponse["PipedFrom"] != DBNull.Value ? Convert.ToInt32(drResponse["PipedFrom"]) : 0;
                            //dr_dtF[0] = drResponse;

                            // Get Total options count for this question
                            questionBase.Id = Convert.ToInt32(questionId);
                            SRV.BaseLayer.ActionResult actionResultAnswersTemp = new SRV.BaseLayer.ActionResult();
                            actionResultAnswersTemp = adminAction.Option_LoadAllById(questionBase);
                            if (actionResultAnswersTemp.dtResult != null && actionResultAnswersTemp.dtResult.Rows.Count > 0)
                            {
                                optionsCount = actionResultAnswersTemp.dtResult.Rows.Count;
                            }

                            if (questionTypeId == 2 || questionTypeId == 4 || questionTypeId == 5 || questionTypeId == 6)
                            {
                                string[] allAnswers = answerExpression.Split('|');
                                if (allAnswers.Length > 0)
                                {
                                    if (questionTypeId == 2)// Multi Punch
                                    {
                                        string allRec = string.Empty;
                                        //List<OptionsModel> matrixAns = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                        List<OptionsModel> matrixAns = new List<OptionsModel>();
                                        if (PipedFrom > 0 && !drResponse["QuestionTitle"].ToString().Contains("{{Q}}"))
                                        {
                                            matrixAns = model.lstQuestionModel.Where(v => v.QuestionId == PipedFrom).FirstOrDefault().lstOptionsModel;
                                        }
                                        else
                                        {
                                            matrixAns = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                        }
                                        foreach (var item in matrixAns)
                                        {
                                            string matchedId = string.Empty;
                                            foreach (string ans in allAnswers)
                                            {
                                                if (Convert.ToString(item.OptionId) == ans)
                                                {
                                                    matchedId = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            allRec += matchedId + "|";
                                        }

                                        string[] allAnswersMat = allRec.Substring(0, allRec.Length - 1).Split('|');
                                        foreach (string ans in allAnswersMat)
                                        {

                                            if (count <= dtF.Columns.Count - 1)
                                            {
                                                if ((Regex.Replace(dtF.Columns[count].ToString().Split('-')[0].Split('_')[1], @"[^\d]", String.Empty)) == ans)
                                                {
                                                    dr_dtF[count] = "1";// ans;
                                                    count++;
                                                    optionsCount--;
                                                }
                                                else
                                                {
                                                    dr_dtF[count] = "2";
                                                    count++;
                                                    optionsCount--;
                                                }
                                            }
                                        }
                                        //count = count + optionsCount;
                                    }
                                    else if (questionTypeId == 6) // matrix Type
                                    {
                                        foreach (string ans in allAnswers)
                                        {
                                            dr_dtF[count] = (ans.ToLower().IndexOf("yes") != -1) ? "1" : "2";
                                            count++;
                                            optionsCount--;
                                        }
                                        count = count + optionsCount;
                                    }
                                    else
                                    {
                                        foreach (string ans in allAnswers)
                                        {
                                            dr_dtF[count] = ans;
                                            count++;
                                            optionsCount--;
                                        }
                                        count = count + optionsCount;
                                    }
                                }
                            }
                            else if (questionTypeId == 1) // Single Punch Answers
                            {
                                //List<OptionsModel> SinglePunchAns = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                List<OptionsModel> SinglePunchAns = new List<OptionsModel>();
                                if (PipedFrom > 0 && !drResponse["QuestionTitle"].ToString().Contains("{{Q}}"))
                                {
                                    SinglePunchAns = model.lstQuestionModel.Where(v => v.QuestionId == PipedFrom).FirstOrDefault().lstOptionsModel;
                                }
                                else
                                {
                                    SinglePunchAns = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                }
                                int ansIndex = 0;
                                foreach (var item in SinglePunchAns)
                                {
                                    ansIndex++;
                                    if (Convert.ToString(item.OptionId) == answerExpression)
                                    {
                                        break;
                                    }
                                }

                                dr_dtF[count] = ansIndex;//answerExpression;
                                count++;
                                optionsCount--;
                            }
                            else if (questionTypeId == 8 || questionTypeId == 12) // single punch matrix answers
                            {
                                List<OptionsModel> optionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                List<MatrixTypeModel> columnDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstMatrixTypeModel;
                                int qustionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).Select(v => v.DisplayOrder).FirstOrDefault();
                                string[] allAnswers = answerExpression.Split('|');

                                List<string> lstColumnOrdinals = new List<string>();

                                string formattedAnswer = string.Empty;
                                string answerValue = string.Empty;
                                if (allAnswers.Length > 0)
                                {
                                    foreach (var ansExpression in allAnswers)
                                    {
                                        if (!string.IsNullOrEmpty(ansExpression))
                                        {
                                            string displaycol = string.Empty;
                                            string displayopt = string.Empty;
                                            foreach (var item in optionDisplayOrder)
                                            {

                                                if (Convert.ToString(item.OptionId) == ansExpression.Split('~')[0])
                                                {
                                                    displaycol = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            foreach (var item in columnDisplayOrder)
                                            {
                                                if (Convert.ToString(item.Id) == ansExpression.Split('~')[1])
                                                {
                                                    displayopt = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            //  formattedAnswer = "Q" + questionId + "_M" + displaycol + "#" + displayopt;
                                            formattedAnswer = "Q" + questionId + "_M" + displayopt + "#" + displaycol;
                                            foreach (DataColumn column in dtF.Columns)
                                            {
                                                string[] columnHeader = column.ColumnName.Split('-');
                                                string[] answer = columnHeader[0].Split('_');
                                                if (columnHeader.Length > 1)
                                                {
                                                    string answerInHeader = answer[0] + "_" + answer[1];
                                                    string[] formattedAnswerArr = formattedAnswer.Split('#');
                                                    if (formattedAnswerArr[0] == answerInHeader)
                                                    {
                                                        int index = dr_dtF.Table.Columns[column.ColumnName].Ordinal;
                                                        lstColumnOrdinals.Add(index.ToString());
                                                        count++;
                                                        dr_dtF[index] = formattedAnswerArr[1];
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            else if (questionTypeId == 13) // Kano model matrix type
                            {
                                List<OptionsModel> optionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                List<MatrixTypeModel> columnDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstMatrixTypeModel;
                                int qustionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).Select(v => v.DisplayOrder).FirstOrDefault();
                                string[] allAnswers = answerExpression.Split('|');

                                List<string> lstColumnOrdinals = new List<string>();

                                string formattedAnswer = string.Empty;
                                string answerValue = string.Empty;
                                if (allAnswers.Length > 1)
                                {
                                    int repeatId = 0;
                                    foreach (var ansExpression in allAnswers)
                                    {
                                        string displaycol = string.Empty;
                                        string displayopt = string.Empty;
                                        string displayText = string.Empty;
                                        string text = string.Empty;
                                        if (repeatId == Convert.ToInt32(ansExpression.Split('~')[0]))
                                        {
                                            text = "_Absent";
                                            repeatId = 0;
                                        }
                                        else
                                        {
                                            text = "_Present";
                                        }
                                        foreach (var item in optionDisplayOrder)
                                        {
                                            if (Convert.ToString(item.OptionId) == ansExpression.Split('~')[0])
                                            {
                                                displayText = Convert.ToString(item.Option) + text;
                                                displaycol = Convert.ToString(item.DisplayOrder);
                                                repeatId = Convert.ToInt32(ansExpression.Split('~')[0]);
                                                break;
                                            }
                                        }
                                        foreach (var item in columnDisplayOrder)
                                        {
                                            if (Convert.ToString(item.Id) == ansExpression.Split('~')[1])
                                            {
                                                displayopt = Convert.ToString(item.DisplayOrder);
                                                break;
                                            }
                                        }
                                        //formattedAnswer = "Q" + qustionDisplayOrder + "_M" + displaycol + "-" + displayText + "#" + displayopt;
                                        formattedAnswer = "Q" + questionId + "_M" + displaycol + "-" + displayText + "#" + displayopt;
                                        foreach (DataColumn column in dtF.Columns)
                                        {

                                            string[] columnHeader = column.ColumnName.Split('-');
                                            string[] answer = columnHeader[0].Split('_');
                                            if (columnHeader.Length > 1)
                                            {
                                                string answerInHeader = answer[0] + "_" + answer[1] + "-" + columnHeader[1];
                                                string[] formattedAnswerArr = formattedAnswer.Split('#');
                                                if (Regex.Replace(formattedAnswerArr[0], @"\s+", "", RegexOptions.Multiline) == Regex.Replace(answerInHeader, @"\s+", "", RegexOptions.Multiline))
                                                {
                                                    int index = dr_dtF.Table.Columns[column.ColumnName].Ordinal;
                                                    lstColumnOrdinals.Add(index.ToString());
                                                    count++;
                                                    dr_dtF[index] = formattedAnswerArr[1];
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            else if (questionTypeId == 16) // Description type Question
                            {
                            }
                            else if (questionTypeId > 7 && questionTypeId != 15)
                            {
                                List<OptionsModel> optionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                List<MatrixTypeModel> columnDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstMatrixTypeModel;
                                List<DDLMatrixTypeModel> dropDownDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstDDlMatrixModel;
                                //count = count + (optionDisplayOrder.Count * columnDisplayOrder.Count);
                                int optDisplayOrder = 0;
                                if (optionDisplayOrder != null)
                                {
                                    optDisplayOrder = optionDisplayOrder.Count;
                                }
                                int colDisplayOrder = 0;
                                if (columnDisplayOrder != null)
                                {
                                    colDisplayOrder = columnDisplayOrder.Count;
                                }
                                int qustionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).Select(v => v.DisplayOrder).FirstOrDefault();
                                string[] allAnswers = answerExpression.Split('|');

                                List<string> lstColumnOrdinals = new List<string>();

                                string formattedAnswer = string.Empty;
                                string answerValue = string.Empty;
                                int DDlDisplayOrder = 0;
                                if (allAnswers.Length > 1)
                                {
                                    foreach (var ansExpression in allAnswers)
                                    {
                                        string displaycol = string.Empty;
                                        string displayopt = string.Empty;

                                        if (questionTypeId == 9 || questionTypeId == 14)
                                        {
                                            foreach (var item in optionDisplayOrder)
                                            {
                                                if (Convert.ToString(item.OptionId) == ansExpression.Split('~')[0].Split('*')[0])
                                                {
                                                    displaycol = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            foreach (var item in columnDisplayOrder)
                                            {
                                                if (Convert.ToString(item.Id) == ansExpression.Split('~')[1])
                                                {
                                                    displayopt = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            //formattedAnswer = "Q" + qustionDisplayOrder + "_M" + displaycol + "_C" + displayopt;
                                            formattedAnswer = "Q" + questionId + "_M" + displaycol + "_C" + displayopt;
                                            answerValue = ansExpression.Split('~')[0].Split('*')[1];
                                            if (questionTypeId == 14)
                                                DDlDisplayOrder = dropDownDisplayOrder.Where(l => l.Id == Convert.ToInt32(answerValue)).FirstOrDefault().DDLDisplayOrder;
                                        }
                                        else
                                        {
                                            foreach (var item in optionDisplayOrder)
                                            {
                                                if (Convert.ToString(item.OptionId) == ansExpression.Split('~')[0])
                                                {
                                                    displaycol = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            foreach (var item in columnDisplayOrder)
                                            {
                                                if (Convert.ToString(item.Id) == ansExpression.Split('~')[1])
                                                {
                                                    displayopt = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            //formattedAnswer = "Q" + qustionDisplayOrder + "_M" + displaycol + "_C" + displayopt;
                                            formattedAnswer = "Q" + questionId + "_M" + displaycol + "_C" + displayopt;
                                        }
                                        foreach (DataColumn column in dtF.Columns)
                                        {
                                            string[] columnHeader = column.ColumnName.Split('-');

                                            if (columnHeader.Length > 1)
                                            {
                                                string[] answer = columnHeader[0].Split('_');

                                                if (answer.Length > 2)
                                                {
                                                    string answerInHeader = answer[0] + "_" + answer[1] + "_" + answer[2];

                                                    if (formattedAnswer == answerInHeader)
                                                    {
                                                        int index = dr_dtF.Table.Columns[column.ColumnName].Ordinal;
                                                        lstColumnOrdinals.Add(index.ToString());
                                                        //count++;
                                                        if (questionTypeId == 9)
                                                        {
                                                            dr_dtF[index] = answerValue;
                                                        }
                                                        else if (questionTypeId == 14)
                                                        {
                                                            dr_dtF[index] = DDlDisplayOrder;
                                                        }
                                                        else
                                                        {
                                                            dr_dtF[index] = "1";
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }

                                // Fill All the Empty cells with 2

                                for (int i = 0; i <= dtF.Columns.Count - 1; i++)
                                    if (String.IsNullOrEmpty(Convert.ToString(dr_dtF[i])))
                                        dr_dtF[i] = "";
                            }

                            else
                            {
                                dr_dtF[count] = answerExpression;
                                count++;
                                optionsCount--;
                            }



                            if (productId != oldProductID)
                            {
                                dtF.Rows.Add(drTemp);
                                drTemp[0] = tempOfflineResponderId;
                                oldProductID = productId;
                            }

                            drTemp = dr_dtF;
                            //---ss
                            if (model.projectModel.ProjectType == "2")
                            {
                                drTemp[2] = product_desc;
                                drTemp[1] = product_logiId;
                            }
                            //---ss


                        }


                        dtF.Rows.Add(dr_dtF);
                        drTemp[0] = tempOfflineResponderId;
                        //---ss
                        if (model.projectModel.ProjectType == "2")
                        {
                            dr_dtF[2] = product_desc;
                            dr_dtF[1] = product_logiId;
                        }
                        //  response_id++;
                        //---ss
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return dtF;
        }
        #endregion

        #region GetRespondersData [Question Type 3]
        public ActionResult GetRespondersData(string QuestionIds)
        {
            List<string> selectedColumns = new List<string>();
            DataTable dtReport;
            string[] json = new string[1];

            dtReport = (DataTable)Session["ReportDataTable"];
            selectedColumns.Add("Resp_Id");
            selectedColumns.Add("Product_LogicalId");
            if (QuestionIds.Length > 0)
            {
                var questions = QuestionIds.Split(',');
                foreach (var QuestionId in questions)
                {
                    string colName = "Q" + Convert.ToString(QuestionId);


                    selectedColumns = selectedColumns.Concat(dtReport.Columns.Cast<DataColumn>().Where(x => x.ColumnName.Contains(colName)).Select(x => x.ColumnName).ToList()).ToList();
                }
            }
            //ViewBag.QuestionsArray = QuestionIds.Split(',');
            DataTable dt = new DataView(dtReport).ToTable(false, selectedColumns.ToArray());

            //var res = (from m in dtReport.AsEnumerable() where m.Field<string>("Code") == "1-1-12" select m).FirstOrDefault();

            //return PartialView("_PartialUserRespTable",dt);


            json[0] = Newtonsoft.Json.JsonConvert.SerializeObject(dt);



            return Json(json);
        }
        #endregion

        #region GetMeansOfRangeQuestion [Question Type 3]
        public JsonResult GetMeansOfRangeQuestion(int QuestionId)
        {
            string colName = "Q" + Convert.ToString(QuestionId);
            string[] json = new string[2];
            if (Session["ReportDataTable"] != null)
            {
                DataTable dtReport = (DataTable)Session["ReportDataTable"];
                string[] colNames = dtReport.Columns.Cast<DataColumn>().Where(x => x.ColumnName.Contains(colName)).Select(x => x.ColumnName).ToArray();
                //var res = (from m in dtReport.AsEnumerable() where m.Field<string>("Code") == "1-1-12" select m).FirstOrDefault();

                List<dynamic> lstKnownMoves = dtReport.Rows
                                         .Cast<DataRow>().Select(r => new
                                         {
                                             respId = r["Resp_Id"].ToString(),
                                             proId = r["Product_LogicalId"].ToString(),
                                             resp = r[colNames[0]].ToString(),
                                             qsenName = colNames[0].ToString()
                                         }).ToList<dynamic>();

                var query = (from row in dtReport.AsEnumerable()
                             group row by row.Field<string>("Product_LogicalId") into grp
                             select new
                             {
                                 Sum = grp.Sum((r) => decimal.Parse(!string.IsNullOrEmpty(r[colNames[0]].ToString()) ? r[colNames[0]].ToString() : "0"))
                             }).ToList();
                json[0] = Newtonsoft.Json.JsonConvert.SerializeObject(query);
                json[1] = Newtonsoft.Json.JsonConvert.SerializeObject(lstKnownMoves);


            }
            return Json(json, JsonRequestBehavior.AllowGet);
        }
        #endregion

        // Convert XML to DataTable
        #region ConvertXMLToDataTable
        public DataTable ConvertXMLToDataTable(string xml)
        {
            DataSet ds = new DataSet();
            using (StringReader stringReader = new StringReader(xml))
            {
                ds = new DataSet();
                ds.ReadXml(stringReader);
            }
            DataTable dtCopy = ds.Tables[0].Clone(); //copy the structure 
            for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++) //iterate through the rows of the source
            {
                DataRow currentRow = ds.Tables[0].Rows[i];  //copy the current row 
                foreach (var colValue in currentRow.ItemArray)//move along the columns 
                {
                    if (!string.IsNullOrEmpty(colValue.ToString())) // if there is a value in a column, copy the row and finish
                    {
                        dtCopy.ImportRow(currentRow);
                        break; //break and get a new row                        
                    }
                }
            }
            return dtCopy;
        }
        #endregion

        // Convert DataTable to XML
        #region ConvertDatatableToXML
        public string ConvertDatatableToXML(DataTable dt)
        {
            string xmlstr;
            if (dt.Rows.Count > 0)
            {
                MemoryStream str = new MemoryStream();
                dt.TableName = "myTable";
                dt.WriteXml(str, true);
                str.Seek(0, SeekOrigin.Begin);
                StreamReader sr = new StreamReader(str);
                xmlstr = sr.ReadToEnd();
            }
            else
            {
                if (dt.Rows.Count == 0)
                {
                    dt.Rows.Add("1Delete1");
                }
                for (int i = 0; i < dt.Columns.Count; i++)
                {
                    dt.Rows[0][i] = "1Delete1";
                }

                MemoryStream str = new MemoryStream();
                dt.TableName = "myTable";
                dt.WriteXml(str, true);
                str.Seek(0, SeekOrigin.Begin);
                StreamReader sr = new StreamReader(str);
                xmlstr = sr.ReadToEnd().Replace("1Delete1", "");
            }
            return (xmlstr);
        }
        #endregion

        public static string DataTableToCSV(DataTable tbl)
        {
            StringBuilder strb = new StringBuilder();

            //column headers
            strb.AppendLine(string.Join(",", tbl.Columns.Cast<DataColumn>()
                .Select(s => "\"" + s.ColumnName + "\"")));

            //rows
            tbl.AsEnumerable().Select(s => strb.AppendLine(
                string.Join(",", s.ItemArray.Select(
                    i => "\"" + i.ToString() + "\"")))).ToList();

            return strb.ToString();
        }

        #endregion

        #region Peak Validation Matrix

        public ActionResult PeakValidationMatrix(string table)
        {
            DataTable data = (DataTable)JsonConvert.DeserializeObject(table, (typeof(DataTable)));
            data.Columns.RemoveAt(0);

            string csvPeakMatrix = DataTableToCSV(data);

            //write data to csv file to be accessed by Peak Validation
            //Here we clear the exisitng file to avoid duplicate records.
            System.IO.File.WriteAllText(FactorAnalysisDrivePath + "\\peakValidationMatrix.csv", String.Empty);
            System.IO.File.WriteAllText(FactorAnalysisDrivePath + "\\peakValidationMatrix.csv", csvPeakMatrix);
            string responseFATable = "";

            //start Peak Validation Mean
            ProcessStartInfo startinfo = new ProcessStartInfo();
            string cwd = System.Reflection.Assembly.GetExecutingAssembly().Location;
            startinfo.FileName = FactorAnalysisDrivePath + "\\" + "SRV.FactorAnalysis.exe";
            startinfo.CreateNoWindow = true;
            startinfo.UseShellExecute = false;
            startinfo.RedirectStandardOutput = true;

            string[] args = { FactorAnalysisDrivePath, "PeakValidation" };
            startinfo.Arguments = String.Join(" ", args);

            using (Process process = Process.Start(startinfo))
            {
                StreamReader sr = process.StandardOutput;
                responseFATable = sr.ReadToEnd();

            }
            string tblJson = "<thead><tr>";
            string[] arrayRes = responseFATable.Split('\n');
            string ques = string.Empty;
            arrayRes = arrayRes.Where(x => !string.IsNullOrEmpty(x)).ToArray();
            for (int i = 0; i < arrayRes.Count(); i++)
            {
                string[] newRow = arrayRes[i].Split(',');
                if (i == 0)
                {                    
                    for (int h = 0; h < newRow.Count(); h++) {
                        if (newRow[h] == "Group.1")
                        {
                            tblJson += "<th id=\"pro_Id\" data-name=\"proId\">pro_Id</th>";
                        }
                        else {
                            tblJson += "<th data-name=\"" + newRow[h].Replace("INDEPENDANT_","").Trim() + "\">" + newRow[h].Trim() + "</th>";
                            ques += newRow[h].Replace("INDEPENDANT_", "").Trim() + ",";
                        }
                    }
                    tblJson += "</tr></thead><tbody>";
                }
                else {
                    string[] quesNo = ques.Split(',');
                    quesNo = quesNo.Where(x => !string.IsNullOrEmpty(x)).ToArray();
                    for (int td = 0; td < newRow.Count(); td++)
                    {
                        if (td == 0)
                        {
                            tblJson += "<tr data-id = \"" + newRow[td].Trim() + "\">";
                            tblJson += "<td>" + newRow[td].Trim() + "</td>";
                        }
                        else {
                            tblJson += "<td data-name=\"" + quesNo[td-1] + "\">" + Math.Round(Convert.ToDecimal(newRow[td].Trim()),2).ToString() + "</td>";
                        }
                    }
                    tblJson += "</tr>";
                }
            }
            tblJson += "</tbody>";

            return Json(tblJson, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region _PartialReportMaker Get
        [HttpGet]
        public ActionResult _PartialReportMaker(string surveyGUID)
        {
            List<QuestionModel> QuestionLst = new List<QuestionModel>();
            RespondentController resp = new RespondentController();
            try
            {
                SurveyResponseModel model = new SurveyResponseModel();
                model = resp.GetProjectPreview(surveyGUID);
                ViewBag.ProjectType = model.projectModel.ProjectType;
                QuestionLst = model.lstQuestionModel;
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return View(QuestionLst);
        }
        #endregion

        #region Quadratic Regression
        public JsonResult QuadraticRegression(string depArr, string inDepArr)
        {
            string[] arr = new string[4];

            // X Independent variable and Y Dependent variable

            double[] xArray = inDepArr.Split(',').Select(Double.Parse).ToArray();
            double[] yArray = depArr.Split(',').Select(Double.Parse).ToArray();


            var xyQuery = from x in xArray
                          from y in yArray
                          select x * y;

            double[] xyArray = xArray.Select((x, index) => x * yArray[index]).ToArray();

            var x2Query = from x in xArray
                          select x * x;
            double[] x2Array = x2Query.ToArray();

            var x3Query = from x in xArray
                          select x * x * x;
            double[] x3Array = x3Query.ToArray();

            var x4Query = from x in xArray
                          select x * x * x * x;
            double[] x4Array = x4Query.ToArray();


            double[] x2yArray = xArray.Select((x, index) => x * x * yArray[index]).ToArray();



            double xTotal = xArray.Sum();
            double yTotal = yArray.Sum();
            double x2Total = x2Array.Sum();
            double x3Total = x3Array.Sum();
            double x4Total = x4Array.Sum();
            double x2yTotal = x2yArray.Sum();

            //Mean of x & y
            double xMean = xTotal / xArray.Length;
            double yMean = yTotal / yArray.Length;
            double x2Mean = x2Total / xArray.Length;

            //Coefficient Calculation
            double Sxx = (x2Mean - (xMean * xMean));
            double Sxy = ((xyArray.Sum() / xyArray.Length) - (xMean * yMean));
            double Sxx2 = ((x3Total / x3Array.Length) - (xMean * x2Mean));
            double Sx2x2 = ((x4Total / x4Array.Length) - (x2Mean * x2Mean));
            double Sx2y = ((x2yTotal / x4Array.Length) - (x2Mean * yMean));

            //Trend line calculation
            double C = (((Sx2y * Sxx) - (Sxy * Sxx2)) / ((Sxx * Sx2x2) - (Sxx2 * Sxx2)));
            double B = (((Sxy * Sx2x2) - (Sx2y * Sxx2)) / ((Sxx * Sx2x2) - (Sxx2 * Sxx2)));
            double A = (yMean - (B * xMean) - (C * x2Mean));

            // Use A + Bx + Cx^2 function and Peak = -B/2C formula

            double peak = Math.Round((-B / (2 * C)), 2);


            arr[0] = Math.Round(A, 2).ToString();
            arr[1] = Math.Round(B, 2).ToString();
            arr[2] = Math.Round(C, 2).ToString();
            arr[3] = peak.ToString();

            return Json(arr, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Factor Analysis
        public ActionResult GetFAData(string peakMatrix)
        {

            DataTable data = (DataTable)JsonConvert.DeserializeObject(peakMatrix, (typeof(DataTable)));
            string csvPeakMatrix = DataTableToCSV(data);

            //write data to csv file to be accessed by factor analysis
            //Here we clear the exisitng file to avoid duplicate records.
            System.IO.File.WriteAllText(FactorAnalysisDrivePath + "\\peakMatrix.csv", String.Empty);
            System.IO.File.WriteAllText(FactorAnalysisDrivePath + "\\peakMatrix.csv", csvPeakMatrix);
            string responseFATable = "";
            //start factor analysis
            ProcessStartInfo startinfo = new ProcessStartInfo();
            string cwd = System.Reflection.Assembly.GetExecutingAssembly().Location;
            startinfo.FileName = FactorAnalysisDrivePath + "\\" + "SRV.FactorAnalysis.exe";
            startinfo.CreateNoWindow = true;
            startinfo.UseShellExecute = false;
            startinfo.RedirectStandardOutput = true;

            string[] args = { FactorAnalysisDrivePath, "FactorAnalysis" };
            startinfo.Arguments = String.Join(" ", args);

            //startinfo.Arguments = FactorAnalysisDrivePath;
            using (Process process = Process.Start(startinfo))
            {
                StreamReader sr = process.StandardOutput;
                responseFATable = sr.ReadToEnd();

            }
            return Json(responseFATable, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region CreateClustermembershipProject Post
        [ValidateInput(false)]
        [HttpPost]
        public ActionResult CreateClusterMembershipProject(FormCollection fc)
        {
            int ProjectId = 0;

            string jsonResult = string.Empty;
            try
            {
                if (Session["UserId"] != null)
                {
                    DataTable dt = new DataTable();
                    string status = "created";
                    projectBase = new ProjectBase();
                    projectBase.LanguageId = 1;
                    projectBase.ProjectName = fc["ProjectTitle"].ToString();
                    projectBase.NotParticipateProjectName = fc["ProjectTitle"].ToString() + "_NotParticipateRes";
                    projectBase.NotParticipateRespondent = fc["hdnotParticipateRes"].ToString();
                    projectBase.OwnerId = Convert.ToInt32(Session["UserId"]);
                    projectBase.Id = Convert.ToInt32(fc["CurrentProjectId"]);
                    projectBase.ProjectType = 1;
                    projectBase.ReportType = fc["ReportType"].ToString();
                    DataTable data = (DataTable)JsonConvert.DeserializeObject(fc["ClusterMembershipData"], (typeof(DataTable)));
                    projectBase.ClusterTable = data;

                    if (fc["hdOverrideNameRes"].ToString() != "true")
                    {
                        actionResult = adminAction.ClusterProjectNameCheck(projectBase);
                        if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                        {
                            jsonResult = "exist";
                            return Json(jsonResult);
                        }
                    }
                    actionResult = adminAction.ClusterProject_Insert(projectBase);
                    if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                    {
                        if (!string.IsNullOrEmpty(fc["hdnotParticipateRes"].ToString()))
                        {
                            ProjectId = Convert.ToInt32(actionResult.dtResult.Rows[0][0]);
                            actionResult = adminAction.NotParticipateRes_Insert(projectBase);
                            if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                            {
                                //TempData["SucessMessage"] = "Cluster Membership exported successfully.";
                                jsonResult = "success";
                            }
                            // jsonResult = "success";
                        }
                    }
                    else
                    {
                        //TempData["ErrorMessage"] = "Error in exporting cluter membership project";
                        jsonResult = "error";
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
                //TempData["ErrorMessage"] = "Error in exporting cluter membership project";
                jsonResult = "error";
            }

            //  return Redirect("/Report/SensorySegmentation/Curve?projId=" + Convert.ToInt32(fc["CurrentProjectId"]) + "&ReportType=" + fc["ReportType"].ToString() + "");
            return Json(jsonResult);
        }
        #endregion

        public PartialViewResult _PartialUserRespTable(DataTable dt)
        {
            return PartialView(dt);
        }

        #region Check Cluster Project Exist or Not

        [HttpPost]
        public ActionResult ClusterProjectNameCheck(string projectName = null)
        {
            System.Web.HttpContext.Current.Response.Clear();
            string jsonResult = string.Empty;
            ProjectBase projectBase = new ProjectBase();
            try
            {
                projectBase.ProjectName = projectName;
                actionResult = adminAction.ClusterProjectNameCheck(projectBase);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    jsonResult = "exist";
                }
                else {
                    jsonResult = "save";
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return Json(jsonResult, JsonRequestBehavior.AllowGet);
        }
        #endregion

    }
}
