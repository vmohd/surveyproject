﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SurveyProject.Models;
using SRV.BaseLayer.Admin;
using SRV.ActionLayer.Admin;
using System.Data;
using SRV.Utility;
using SurveyProject.Controllers;
using SRV.ActionLayer.Respondent;
using SurveyProject.Helper;
using System.Text.RegularExpressions;
using SRV.BaseLayer.Respondent;


namespace SurveyProject.Areas.Report.Controllers
{
    [CheckLogin]
    public class QueryBuilderController : Controller
    {
        #region Declaration
        AdminBase adminBase = new AdminBase();
        QuestionBase questionBase = new QuestionBase();
        QueryBase queryBase = new QueryBase();
        AdminAction adminAction = new AdminAction();
        ProjectBase projectBase = new ProjectBase();
        RespondentAction respondentAction = new RespondentAction();
        SRV.BaseLayer.ActionResult actionResult = new SRV.BaseLayer.ActionResult();
        CommonMethods comMethods = new CommonMethods();
        ProductProjectBase productProjectBase = new ProductProjectBase();
        static DataTable CaseContainer = new DataTable();

        #endregion

        public ActionResult QueryBuilder(int? ProjectId = 0)
        {
            string ReportType = "online";
            ViewBag.ProjectId = ProjectId;
            CreateProjectModel model = new CreateProjectModel();
            model = GetProjectDetailById(ProjectId);
            ViewBag.Super = model.SuperProjectId;
            string projectGUID = model.ProjectGuid;
            ViewBag.ProjectName = model.ProjectName;
            ViewBag.ProjectType = model.ProjectType;




            return View();
        }

        #region _partialQuestionList
        public ActionResult _partialQuestionList(int? ProjectId = 0)
        {
            ViewBag.ProjectId = ProjectId;
            CreateQuestionModel model = new CreateQuestionModel();
            QueryModel queryModel = new QueryModel();
            List<CreateQuestionModel> QuestionLst = new List<CreateQuestionModel>();
            List<QueryModel> QueryList = new List<QueryModel>();
            DataTable CaseContainer1 = new DataTable();

            CreateProjectModel projectModel = new CreateProjectModel();
            projectModel = GetProjectDetailById(ProjectId);
            List<int> currentQueryList = new List<int>();         
            int currentQuery = 0;
            int currentQuery1 = 0;
            int check = 0;

            // model.QuestionList = QuestionLst;
            try
            {
                if (Session["UserId"] != null)
                {
                    questionBase.ProjectId = Convert.ToInt32(ProjectId);
                    actionResult = adminAction.Qusetion_LoadAllByProjectId(questionBase);
                    if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                    {
                        foreach (DataRow dr in actionResult.dtResult.Rows)
                        {
                            model = new CreateQuestionModel();
                           
                            model.ProjectId = dr["ProjectId"] != DBNull.Value ? Convert.ToInt32(dr["ProjectId"]) : 0;
                            model.QuestionId = dr["questionId"] != DBNull.Value ? Convert.ToInt32(dr["questionId"]) : 0;
                            model.QuestionType = dr["QuestionType"] != DBNull.Value ? Convert.ToString(dr["QuestionType"]) : "";
                            model.QuestionTypeId = dr["QuestionTypeId"] != DBNull.Value ? Convert.ToInt32(dr["QuestionTypeId"]) : 0;
                            model.QuestionTitle = dr["QuestionTitle"] != DBNull.Value ? Convert.ToString(dr["QuestionTitle"]) : "";
                            model.QuestionTag = dr["QuestionTag"] != DBNull.Value ? Convert.ToString(dr["QuestionTag"]) : "N/A";
                            // model.Option = Convert.ToString(dr["QuestionType"]);
                            QuestionLst.Add(model);
                        }
                    }
                    //QuestionLst.RemoveAll(q => q.QuestionTypeId == 16);
                    //QuestionLst = QuestionLst.Where(m => m.QuestionTypeId != 1).ToList();
                    model.QuestionList = QuestionLst;
                    queryBase.ProjectId = Convert.ToInt32(ProjectId);
                    actionResult = adminAction.Query_LoadAllByProjectId(queryBase);
                    if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                    {
                        foreach (DataRow dr in actionResult.dtResult.Rows)
                        {
                            queryModel = new QueryModel();
                            queryModel.Id = dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0;
                            queryModel.ProjectId = dr["ProjectId"] != DBNull.Value ? Convert.ToInt32(dr["ProjectId"]) : 0;
                            queryModel.CaseName = dr["CaseName"] != DBNull.Value ? Convert.ToString(dr["CaseName"]) : "";
                            queryModel.Query = dr["Query"] != DBNull.Value ? Convert.ToString(dr["Query"]) : "";
                            queryModel.QueryHtml = dr["QueryHtml"] != DBNull.Value ? Convert.ToString(dr["QueryHtml"]) : "";
                            currentQuery = dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0;
                            if(currentQuery>0){
                            currentQueryList.Add(currentQuery);
                            }
                            QueryList.Add(queryModel);
                        }
                    }
                    ///
                    if (projectModel.SuperProjectId > 0)
                    {
                        queryBase.SuperProjectId = projectModel.SuperProjectId;
                        //(projectModel.ProjectType == 2)
                        actionResult = adminAction.Query_LoadAllBySuperProjectId(queryBase);
                        if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                        {
                            check = 0;
                            foreach (DataRow dr in actionResult.dtResult.Rows)
                            {
                                queryModel = new QueryModel();
                                queryModel.Id = dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0;
                             //   queryModel.ProjectId = dr["ProjectId"] != DBNull.Value ? Convert.ToInt32(dr["ProjectId"]) : 0;
                                queryModel.CaseName = dr["CaseName"] != DBNull.Value ? Convert.ToString(dr["CaseName"]) : "";
                                queryModel.Query = dr["Query"] != DBNull.Value ? Convert.ToString(dr["Query"]) : "";
                                queryModel.QueryHtml = dr["QueryHtml"] != DBNull.Value ? Convert.ToString(dr["QueryHtml"]) : "";
                                currentQuery1 = dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0;
                                foreach(var item in currentQueryList)
                                {
                                    if (item == currentQuery1)
                                    {
                                        check = 1;                                       
                                    }
                                }
                                if (check == 0)
                                {
                                    QueryList.Add(queryModel);
                                }
                            }
                        }
                    }
                    ///





                    model.QueryList = QueryList;



                    if (CaseContainer1.Columns.Count == 0)
                    {
                        CaseContainer1.Columns.Add("CaseId", typeof(int));
                        CaseContainer1.Columns.Add("CaseName", typeof(string));
                        CaseContainer1.Columns.Add("Online", typeof(int));
                        CaseContainer1.Columns.Add("Offline", typeof(int));
                        CaseContainer1.Columns.Add("Combined", typeof(int));
                        CaseContainer1.Columns.Add("QueryHtml", typeof(string));
                    }

                   





                  
                    string QueryBuilder = string.Empty;
                   

                    foreach (var item in QueryList)
                    {
                        QueryBuilder = item.Query;
                        if (QueryBuilder != null && QueryBuilder != string.Empty)
                        {
                            string[] query = QueryBuilder.TrimEnd('*').Split('*');

                            DataTable dtCase = new DataTable();
                            dtCase.Columns.Add("Id", typeof(int));
                            dtCase.Columns.Add("QuestionId", typeof(int));
                            dtCase.Columns.Add("QuestionTypeId", typeof(int));
                            //dtCase.Columns.Add("OptionIdOrDisplayOrder", typeof(int));
                            //dtCase.Columns.Add("ColumnIdOrDisplayOrder", typeof(int));
                            dtCase.Columns.Add("OptionIdOrDisplayOrder", typeof(string));
                            dtCase.Columns.Add("ColumnIdOrDisplayOrder", typeof(string));
                            dtCase.Columns.Add("Expression", typeof(string));
                            dtCase.Columns.Add("NextQuestionAddWith", typeof(string));


                            if (query.Length > 0)
                            {
                                for (int i = 0; i <= query.Length - 1; i++)
                                {



                                    //506_2$=1,1#|2840,2840~*
                                    string[] questionArr = query[i].Split('_'); //1602_5$>=11#OR|2~
                                    int questionId = Convert.ToInt32(questionArr[0]);
                                    string[] exprArr = questionArr[1].Split('$');
                                    int questionTypeId = Convert.ToInt32(exprArr[0]);
                                    string Expression = exprArr[1].Split('#')[0];
                                    string[] optionIdArr = exprArr[1].Split('#')[1].Split('|');
                                    string NextQuestionAddWith = optionIdArr[0];
                                    //int optionId = optionIdArr[1].Split('~')[0] != "" ? Convert.ToInt32(optionIdArr[1].Split('~')[0]) : 0;
                                    //int columnId = optionIdArr[1].Split('~')[1] != "" ? Convert.ToInt32(optionIdArr[1].Split('~')[1]) : 0;
                                    string optionId = optionIdArr[1].Split('~')[0] != "" ? Convert.ToString(optionIdArr[1].Split('~')[0]): "0";
                                    string columnId = optionIdArr[1].Split('~')[1] != "" ? Convert.ToString(optionIdArr[1].Split('~')[1]) : "0";
                                    DataRow drCase = dtCase.NewRow();
                                    drCase["Id"] = (i + 1);
                                    drCase["QuestionId"] = questionId;
                                    drCase["QuestionTypeId"] = questionTypeId;
                                    drCase["OptionIdOrDisplayOrder"] = optionId;
                                    drCase["ColumnIdOrDisplayOrder"] = columnId;
                                    drCase["Expression"] = Expression;
                                    drCase["NextQuestionAddWith"] = NextQuestionAddWith;
                                    dtCase.Rows.Add(drCase);
                                }
                            }

                            projectBase.CaseTable = dtCase;

                            List<string> lstEmails = new List<string>();
                            List<string> lstResponders = new List<string>();
                            List<string> offlineResponders = new List<string>();
                            string Email = string.Empty;
                            string ExternalResponders = string.Empty;


                            actionResult = adminAction.Response_CaseSelectionReport(projectBase);
                            if (actionResult.IsSuccess)
                            {
                                if (Convert.ToString(actionResult.dtResult.Rows[0][0]) == "-1")
                                {

                                }
                                else
                                {
                                    foreach (DataRow dr in actionResult.dtResult.Rows)
                                    {
                                        ProjectId = dr["ProjectId"] != DBNull.Value ? Convert.ToInt32(dr["ProjectId"]) : 0;
                                        Email += dr["Email"] != DBNull.Value ? Convert.ToString(dr["Email"]) + "," : "";
                                        if (dr["Email"] != DBNull.Value && !String.IsNullOrEmpty(dr["Email"].ToString()))
                                            lstEmails.Add(dr["Email"].ToString());
                                        ExternalResponders += dr["ExternalResponderId"] != DBNull.Value ? Convert.ToString(dr["ExternalResponderId"]) + "," : "";
                                        if (dr["ExternalResponderId"] != DBNull.Value && !String.IsNullOrEmpty(dr["ExternalResponderId"].ToString()))
                                            lstResponders.Add(dr["ExternalResponderId"].ToString());

                                    }
                                }
                            }

                            actionResult = adminAction.CompleteRespondersLoad_ByProjectId(projectBase);
                            List<RespondersModel> lstCompleteResponders = new List<RespondersModel>();
                            foreach (DataRow dr in actionResult.dtResult.Rows)
                            {
                                lstCompleteResponders.Add(new RespondersModel
                                {
                                    InvitationGuid = dr["InvitationGuid"] != DBNull.Value ? Convert.ToString(dr["InvitationGuid"]) : "",
                                    Email = dr["Email"] != DBNull.Value ? Convert.ToString(dr["Email"]) : ""
                                });
                            }
                            IEnumerable<RespondersModel> filteredList = lstCompleteResponders.GroupBy(customer => customer.InvitationGuid).Select(group => group.First());
                            string[] Emails= lstEmails.ToArray();
                            string[] ExternalResponder=lstResponders.ToArray();
                            int OnlineBaseSizeResponder = 0;
                            foreach (var responder in filteredList)
                            {     
                                if (Array.IndexOf(Emails, responder.Email) > -1 || Array.IndexOf(ExternalResponder, responder.InvitationGuid) > -1)
                                {
                                    OnlineBaseSizeResponder++;
                                }
                            }

                            actionResult = adminAction.Response_CaseSelectionReportOffline(projectBase);
                            if (actionResult.IsSuccess)
                            {
                                if (Convert.ToString(actionResult.dtResult.Rows[0][0]) == "-1")
                                {

                                }
                                else
                                {
                                    ExternalResponders = "";
                                    foreach (DataRow dr in actionResult.dtResult.Rows)
                                    {
                                        ProjectId = dr["ProjectId"] != DBNull.Value ? Convert.ToInt32(dr["ProjectId"]) : 0;

                                        ExternalResponders += dr["RespId"] != DBNull.Value ? Convert.ToString(dr["RespId"]) + "," : "";
                                        if (dr["RespId"] != DBNull.Value && !String.IsNullOrEmpty(dr["RespId"].ToString()))
                                            offlineResponders.Add(dr["RespId"].ToString());

                                    }
                                }
                            }

                            DataRow CaseRow = CaseContainer1.NewRow();
                            DataTable dt2 = new DataTable();
                            CaseRow[0] = item.Id;
                            CaseRow[1] = item.CaseName;
                            CaseRow[2] = OnlineBaseSizeResponder;
                            CaseRow[3] = offlineResponders.Count();
                            CaseRow[4] = OnlineBaseSizeResponder + offlineResponders.Count();
                            CaseRow[5] = item.QueryHtml;
                            CaseContainer1.Rows.Add(CaseRow);
                        }
                    }


                    ViewBag.CaseContainer = CaseContainer1;
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return View(model);
        }
        #endregion

        #region GetProjectDetailById
        public CreateProjectModel GetProjectDetailById(int? id = 0)
        {
            CreateProjectModel model = new CreateProjectModel();
            List<ProductProjectModel> lstproductModel = new List<ProductProjectModel>();
            try
            {
                projectBase.Id = Convert.ToInt32(id);
                actionResult = adminAction.Project_LoadById(projectBase);
                if (actionResult.IsSuccess)
                {
                    DataRow dr = actionResult.dtResult.Rows[0];
                    model.Id = dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0;
                    model.SuperProjectId = dr["SuperProjectId"] != DBNull.Value ? Convert.ToInt32(dr["SuperProjectId"]) : 0;
                    model.ProjectName = dr["ProjectName"] != DBNull.Value ? Convert.ToString(dr["ProjectName"]) : "";
                    model.WelcomeMessage = dr["WelcomeMessage"] != DBNull.Value && !String.IsNullOrEmpty(dr["WelcomeMessage"].ToString()) ? Convert.ToString(dr["WelcomeMessage"]) : Email.ReadFile("Welcome.txt");
                    model.PageHeader = dr["PageHeader"] != DBNull.Value && !String.IsNullOrEmpty(dr["PageHeader"].ToString()) ? Convert.ToString(dr["PageHeader"]) : Email.ReadFile("Header.txt");
                    model.PageFooter = dr["PageFooter"] != DBNull.Value && !String.IsNullOrEmpty(dr["PageFooter"].ToString()) ? Convert.ToString(dr["PageFooter"]) : Email.ReadFile("Footer.txt");
                    model.ProjectCompletionMesssage = dr["ProjectCompletionMesssage"] != DBNull.Value && !String.IsNullOrEmpty(dr["ProjectCompletionMesssage"].ToString()) ? Convert.ToString(dr["ProjectCompletionMesssage"]) : Email.ReadFile("Complete.txt");
                    model.RedirectUrl = dr["RedirectUrl"] != DBNull.Value ? Convert.ToString(dr["RedirectUrl"]) : "";
                    model.InvitationMessage = dr["InvitationMessage"] != DBNull.Value ? Convert.ToString(dr["InvitationMessage"]) : "";
                    model.OwnerId = dr["OwnerId"] != DBNull.Value ? Convert.ToInt32(dr["OwnerId"]) : 0;
                    model.CreatedBy = dr["CreatedBy"] != DBNull.Value ? Convert.ToInt32(dr["CreatedBy"]) : 0;
                    model.ProjectQuota = dr["ProjectQuota"] != DBNull.Value ? Convert.ToInt32(dr["ProjectQuota"]) : 0;
                    model.TerminationMessageUpper = dr["ProjectTerminationMessage"] != DBNull.Value && !String.IsNullOrEmpty(dr["ProjectTerminationMessage"].ToString()) ? Convert.ToString(dr["ProjectTerminationMessage"]) : Email.ReadFile("TerminateUpper.txt");
                    model.ProjectGuid = dr["ProjectGuid"] != DBNull.Value ? Convert.ToString(dr["ProjectGuid"]) : "";
                    model.IsIncludePageHeader = dr["IsIncludePageHeader"] != DBNull.Value ? Convert.ToBoolean(dr["IsIncludePageHeader"]) : false;
                    model.IsIncludePageFooter = dr["IsIncludePageFooter"] != DBNull.Value ? Convert.ToBoolean(dr["IsIncludePageFooter"]) : false;
                    model.IsIncludeProjectCompletionMesssage = dr["IsIncludeProjectCompletionMesssage"] != DBNull.Value ? Convert.ToBoolean(dr["IsIncludeProjectCompletionMesssage"]) : false;
                    model.IsIncludeTerminationMessage = dr["IsIncludeTerminationMessage"] != DBNull.Value ? Convert.ToBoolean(dr["IsIncludeTerminationMessage"]) : false;
                    model.IsIncludeWelcomeMessage = dr["IsIncludeWelcomeMessage"] != DBNull.Value ? Convert.ToBoolean(dr["IsIncludeWelcomeMessage"]) : false;
                    model.ProjectType = dr["ProjectType"] != DBNull.Value ? Convert.ToString(dr["ProjectType"]) : "";
                    //model.ProductMessage = dr["ProductMessage"] != DBNull.Value ? Convert.ToString(dr["ProductMessage"]) : "";
                }

                productProjectBase.ProjectId = Convert.ToInt32(id);
                actionResult = adminAction.ProductProject_LoadById(productProjectBase);
                if (actionResult.IsSuccess)
                {
                    foreach (DataRow dr in actionResult.dtResult.Rows)
                    {
                        lstproductModel.Add(new ProductProjectModel
                        {
                            Id = dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0,
                            ProjectId = dr["ProjectId"] != DBNull.Value ? Convert.ToInt32(dr["ProjectId"]) : 0,
                            ProductLogicalId = dr["ProductLogicalId"] != DBNull.Value ? Convert.ToInt32(dr["ProductLogicalId"]) : 0,
                            Description = dr["Description"] != DBNull.Value ? Convert.ToString(dr["Description"]) : string.Empty,
                            DisplayOrder = dr["DisplayOrder"] != DBNull.Value ? Convert.ToInt32(dr["DisplayOrder"]) : 0
                        });
                    }
                    lstproductModel = lstproductModel.OrderBy(l => l.DisplayOrder).ToList();
                    model.lstProductProjectModel = lstproductModel;
                    ViewBag.ProductCount = lstproductModel.Count;
                }

            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return model;
        }
        #endregion

        #region GetReportDataTable
        private DataTable GetReportDataTable(string Id, string[] Emails, string[] ExternalResponders)
        {

            RespondentController respondent = new RespondentController();
            SurveyResponseModel model = new SurveyResponseModel();
            DataTable dtF = new DataTable();
            try
            {
                DataTable dt = new DataTable();

                string projectGuid = string.Empty;
                projectGuid = Id;
                dt.Columns.Add("Resp_Id", typeof(string));

                model = respondent.GetProjectPreview(projectGuid);

                //--ss
                if (model.projectModel.ProjectType == "2")
                {
                    DataRow drnew = dt.NewRow();
                    drnew[0] = "Product_LogicalId";
                    dt.Rows.Add(drnew);
                    DataRow drnew1 = dt.NewRow();
                    drnew1[0] = "Product_Name";
                    dt.Rows.Add(drnew1);
                }
                //--

                // Load all the questions of this project
                List<QuestionList> questionsList = new List<QuestionList>();
                questionBase.ProjectId = Convert.ToInt32(model.projectModel.Id);
                actionResult = adminAction.QuestionsLoadAll_ByProjectId(questionBase);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    foreach (DataRow dr in actionResult.dtResult.Rows)
                    {
                        questionsList.Add(new QuestionList
                        {
                            id = dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0,
                            questionTypeId = dr["QuestionTypeId"] != DBNull.Value ? Convert.ToInt32(dr["QuestionTypeId"]) : 0
                        });
                    }
                }

                string fileName = Guid.NewGuid().ToString().Substring(0, 5) + "_" + DateTime.Now.ToString("MM-dd-yyyy") + "_" + model.projectModel.ProjectName.Replace(" ", "");

                if (model.lstQuestionModel != null && model.lstQuestionModel.Count > 0)
                {
                    foreach (var item in model.lstQuestionModel)
                    {
                        if (item.QuestionTypeId == 2 || item.QuestionTypeId == 4 || item.QuestionTypeId == 5 || item.QuestionTypeId == 6)//Multi Punch || Constant Sum || Ranking || Matrix Type
                        {
                            if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
                            {
                                foreach (var opt in item.lstOptionsModel)
                                {
                                    DataRow drnew = dt.NewRow();
                                    //drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                    drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                    dt.Rows.Add(drnew);
                                }
                            }
                        }

                        else if (item.QuestionTypeId == 1 || item.QuestionTypeId == 3 || item.QuestionTypeId == 7)//Single Punch || Range || Simple Text
                        {
                            DataRow drnew = dt.NewRow();
                            //drnew[0] = "Q" + item.DisplayOrder + "_S" + "-" + ReplaceHtml.ReplaceText(item.QuestionTitle);
                            drnew[0] = "Q" + item.QuestionId + "_S" + "-" + ReplaceHtml.ReplaceText(item.QuestionTitle);
                            dt.Rows.Add(drnew);
                        }
                        else if (item.QuestionTypeId == 8 || item.QuestionTypeId == 12)//Single punch matrix type  && Rank order type
                        {
                            //if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
                            //{
                            //foreach (var opt in item.lstOptionsModel)
                            //{
                            //    DataRow drnew = dt.NewRow();
                            //    //drnew[0] = "Q" + item.QuestionId + "_M" + opt.OptionId + "-" + ReplaceHtml.ReplaceText(opt.Option);
                            //    drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
                            //    dt.Rows.Add(drnew);
                            //}
                            //}
                            if (item.lstMatrixTypeModel != null && item.lstMatrixTypeModel.Count > 0)
                            {
                                foreach (var col in item.lstMatrixTypeModel)
                                {
                                    DataRow drnew = dt.NewRow();
                                    //drnew[0] = "Q" + item.QuestionId + "_M" + opt.OptionId + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                    drnew[0] = "Q" + item.QuestionId + "_M" + col.DisplayOrder + "-" + ReplaceHtml.ReplaceText(col.ColumnName);
                                    dt.Rows.Add(drnew);
                                }
                            }
                        }
                        else if (item.QuestionTypeId == 13)//Kano model matrix type 
                        {
                            if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
                            {
                                foreach (var opt in item.lstOptionsModel)
                                {
                                    DataRow drnew = dt.NewRow();
                                    //drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Present";
                                    drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Present";
                                    dt.Rows.Add(drnew);
                                    DataRow drnew1 = dt.NewRow();
                                    //drnew1[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Absent";
                                    drnew1[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Absent";
                                    dt.Rows.Add(drnew1);
                                }
                            }
                        }
                        // Single Punch Matrix Type || Range Matrix Type Question ||
                        // Single punch type answer horizontal position || 
                        // Multiple punch type answer horizontal position || 
                        // Rank order type question || 
                        // KANO Model Matrix type question || Dropdown Matrix type question
                        else if (item.QuestionTypeId > 7)
                        {
                            if (item.lstOptionsModel.Count > 0)
                            {
                                foreach (var opt in item.lstOptionsModel)
                                {
                                    foreach (var column in item.lstMatrixTypeModel)
                                    {
                                        DataRow drnew = dt.NewRow();
                                        //drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "_C" + column.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + " " + ReplaceHtml.ReplaceText(column.ColumnName);
                                        drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "_C" + column.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + " " + ReplaceHtml.ReplaceText(column.ColumnName);
                                        dt.Rows.Add(drnew);
                                    }
                                }
                            }
                        }
                    }
                }
                // Get the Excel Pattern of dt
                dtF = comMethods.GenerateTransposedTable(dt);

                // Get all the reponders
                projectBase.Id = model.projectModel.Id;
                actionResult = adminAction.RespondersLoad_ByProjectId(projectBase);
                List<RespondersModel> lstResponders = new List<RespondersModel>();
                foreach (DataRow dr in actionResult.dtResult.Rows)
                {
                    lstResponders.Add(new RespondersModel
                    {

                        InvitationGuid = dr["InvitationGuid"] != DBNull.Value ? Convert.ToString(dr["InvitationGuid"]) : "",
                        Email = dr["Email"] != DBNull.Value ? Convert.ToString(dr["Email"]) : "",

                    });
                }
                IEnumerable<RespondersModel> filteredList = lstResponders
                 .GroupBy(customer => customer.InvitationGuid).Select(group => group.First());
                // Get Responses
                SurveyLoadBase surveyLoadBase = new SurveyLoadBase();
                string json = string.Empty;
                SRV.BaseLayer.ActionResult actionResultResponses = new SRV.BaseLayer.ActionResult();
                int response_id = 1;            //ss
                foreach (var responder in filteredList)
                {
                    DataTable responsesDt = new DataTable();
                    surveyLoadBase.Id = model.projectModel.Id;
                    surveyLoadBase.Email = (!string.IsNullOrEmpty(responder.Email) ? Convert.ToString(responder.Email) : "");
                    surveyLoadBase.ExternalResponderId = (!string.IsNullOrEmpty(responder.InvitationGuid) ? Convert.ToString(responder.InvitationGuid) : "");

                    if (Array.IndexOf(Emails, surveyLoadBase.Email) > -1 || Array.IndexOf(ExternalResponders, surveyLoadBase.ExternalResponderId) > -1)
                    {
                        if (model.projectModel.lstProductProjectModel != null &&
                            model.projectModel.lstProductProjectModel.Count > 0)
                        {
                            foreach (var productModel in model.projectModel.lstProductProjectModel)
                            {
                                surveyLoadBase.ProductId = productModel.Id;
                                actionResultResponses = respondentAction.Response_LoadByProjectId(surveyLoadBase);
                                if (actionResultResponses.IsSuccess)
                                {
                                    if (responsesDt.Rows.Count == 0)
                                        responsesDt = actionResultResponses.dtResult.Copy();
                                    else
                                    {
                                        foreach (DataRow drThisRespRow in actionResultResponses.dtResult.Rows)
                                            responsesDt.ImportRow(drThisRespRow);
                                    }
                                }
                            }
                        }
                        else
                        {
                            actionResultResponses = respondentAction.Response_LoadByProjectId(surveyLoadBase);
                            if (actionResultResponses.IsSuccess)
                                responsesDt = actionResultResponses.dtResult.Copy();
                        }
                    }
                    // surveyLoadBase.OfflineResponderId = dr["RespId"] != DBNull.Value ? Convert.ToString(dr["RespId"]) : "";
                    //DataRow dr_dtF = dtF.NewRow();
                    // if (String.IsNullOrEmpty(surveyLoadBase.Email)) continue;

                    //actionResultResponses = respondentAction.Response_LoadByProjectId(surveyLoadBase);
                    if (responsesDt.Rows.Count > 0)//actionResultResponses.IsSuccess
                    {
                        //dr_dtF[0] = surveyLoadBase.Email;

                        int count = 1;
                        count = (model.projectModel.ProjectType == "2") ? count + 2 : count;        //ss
                        DataRow dr_dtF = dtF.NewRow();
                        DataRow drTemp = dtF.NewRow();
                        string product_desc = string.Empty;
                        string product_logiId = string.Empty;
                        int oldProductID = responsesDt.Rows[0]["ProductId"] != DBNull.Value ? Convert.ToInt32(responsesDt.Rows[0]["ProductId"]) : 0;
                        foreach (DataRow drResponse in responsesDt.Rows)// actionResultResponses.dtResult.Rows
                        {

                            int optionsCount = 0;

                            int productId = drResponse["ProductId"] != DBNull.Value ? Convert.ToInt32(drResponse["ProductId"]) : 0;
                            if (productId != oldProductID)
                            {
                                count = (model.projectModel.ProjectType == "2") ? 3 : 1;
                                dr_dtF = dtF.NewRow();
                            }

                            //--ss

                            product_desc = drResponse["ProductDescription"] != DBNull.Value ? Convert.ToString(drResponse["ProductDescription"]) : "";
                            product_logiId = drResponse["ProductLogicalId"] != DBNull.Value ? Convert.ToString(drResponse["ProductLogicalId"]) : "";

                            //--ss
                            int questionId = drResponse["QuestionId"] != DBNull.Value ? Convert.ToInt32(drResponse["QuestionId"]) : 0;
                            string answerExpression = drResponse["AnswerExpression"] != DBNull.Value ? Convert.ToString(drResponse["AnswerExpression"]) : "";
                            int questionTypeId = drResponse["QuestionTypeId"] != DBNull.Value ? Convert.ToInt32(drResponse["QuestionTypeId"]) : 1;
                            //dr_dtF[0] = drResponse;

                            // Get Total options count for this question
                            questionBase.Id = Convert.ToInt32(questionId);
                            SRV.BaseLayer.ActionResult actionResultAnswersTemp = new SRV.BaseLayer.ActionResult();
                            actionResultAnswersTemp = adminAction.Option_LoadAllById(questionBase);
                            if (actionResultAnswersTemp.dtResult != null && actionResultAnswersTemp.dtResult.Rows.Count > 0)
                            {
                                optionsCount = actionResultAnswersTemp.dtResult.Rows.Count;
                            }

                            if (questionTypeId == 2 || questionTypeId == 4 || questionTypeId == 5 || questionTypeId == 6)
                            {
                                string[] allAnswers = answerExpression.Split('|');
                                if (allAnswers.Length > 0)
                                {
                                    if (questionTypeId == 2)// Multi Punch
                                    {
                                        string allRec = string.Empty;
                                        List<OptionsModel> matrixAns = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;

                                        foreach (var item in matrixAns)
                                        {
                                            string matchedId = string.Empty;
                                            foreach (string ans in allAnswers)
                                            {
                                                if (Convert.ToString(item.OptionId) == ans)
                                                {
                                                    matchedId = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            allRec += matchedId + "|";
                                        }

                                        string[] allAnswersMat = allRec.Substring(0, allRec.Length - 1).Split('|');
                                        foreach (string ans in allAnswersMat)
                                        {

                                            if (count <= dtF.Columns.Count - 1)
                                            {
                                                if ((Regex.Replace(dtF.Columns[count].ToString().Split('-')[0].Split('_')[1], @"[^\d]", String.Empty)) == ans)
                                                {
                                                    dr_dtF[count] = "1";// ans;
                                                    count++;
                                                    optionsCount--;
                                                }
                                                else
                                                {
                                                    dr_dtF[count] = "2";
                                                    count++;
                                                    optionsCount--;
                                                }
                                            }
                                        }
                                        //count = count + optionsCount;
                                    }
                                    else if (questionTypeId == 6) // matrix Type
                                    {
                                        foreach (string ans in allAnswers)
                                        {
                                            dr_dtF[count] = (ans.ToLower().IndexOf("yes") != -1) ? "1" : "2";
                                            count++;
                                            optionsCount--;
                                        }
                                        count = count + optionsCount;
                                    }
                                    else
                                    {
                                        foreach (string ans in allAnswers)
                                        {
                                            dr_dtF[count] = ans;
                                            count++;
                                            optionsCount--;
                                        }
                                        count = count + optionsCount;
                                    }
                                }
                            }
                            else if (questionTypeId == 1) // Single Punch Answers
                            {
                                List<OptionsModel> SinglePunchAns = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                int ansIndex = 0;
                                foreach (var item in SinglePunchAns)
                                {
                                    ansIndex++;
                                    if (Convert.ToString(item.OptionId) == answerExpression)
                                    {
                                        break;
                                    }
                                }

                                dr_dtF[count] = ansIndex;//answerExpression;
                                count++;
                                optionsCount--;
                            }
                            else if (questionTypeId == 8 || questionTypeId == 12) // single punch matrix answers
                            {
                                List<OptionsModel> optionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                List<MatrixTypeModel> columnDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstMatrixTypeModel;
                                int qustionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).Select(v => v.DisplayOrder).FirstOrDefault();
                                string[] allAnswers = answerExpression.Split('|');

                                List<string> lstColumnOrdinals = new List<string>();

                                string formattedAnswer = string.Empty;
                                string answerValue = string.Empty;
                                if (allAnswers.Length > 1)
                                {
                                    foreach (var ansExpression in allAnswers)
                                    {
                                        string displaycol = string.Empty;
                                        string displayopt = string.Empty;
                                        foreach (var item in optionDisplayOrder)
                                        {

                                            if (Convert.ToString(item.OptionId) == ansExpression.Split('~')[0])
                                            {
                                                displaycol = Convert.ToString(item.DisplayOrder);
                                                break;
                                            }
                                        }
                                        foreach (var item in columnDisplayOrder)
                                        {
                                            if (Convert.ToString(item.Id) == ansExpression.Split('~')[1])
                                            {
                                                displayopt = Convert.ToString(item.DisplayOrder);
                                                break;
                                            }
                                        }
                                        //  formattedAnswer = "Q" + questionId + "_M" + displaycol + "#" + displayopt;
                                        formattedAnswer = "Q" + questionId + "_M" + displayopt + "#" + displaycol;
                                        foreach (DataColumn column in dtF.Columns)
                                        {
                                            string[] columnHeader = column.ColumnName.Split('-');
                                            string[] answer = columnHeader[0].Split('_');
                                            if (columnHeader.Length > 1)
                                            {
                                                string answerInHeader = answer[0] + "_" + answer[1];
                                                string[] formattedAnswerArr = formattedAnswer.Split('#');
                                                if (formattedAnswerArr[0] == answerInHeader)
                                                {
                                                    int index = dr_dtF.Table.Columns[column.ColumnName].Ordinal;
                                                    lstColumnOrdinals.Add(index.ToString());
                                                    count++;
                                                    dr_dtF[index] = formattedAnswerArr[1];
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            else if (questionTypeId == 13) // Kano model matrix type
                            {
                                List<OptionsModel> optionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                List<MatrixTypeModel> columnDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstMatrixTypeModel;
                                int qustionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).Select(v => v.DisplayOrder).FirstOrDefault();
                                string[] allAnswers = answerExpression.Split('|');

                                List<string> lstColumnOrdinals = new List<string>();

                                string formattedAnswer = string.Empty;
                                string answerValue = string.Empty;
                                if (allAnswers.Length > 1)
                                {
                                    int repeatId = 0;
                                    foreach (var ansExpression in allAnswers)
                                    {
                                        string displaycol = string.Empty;
                                        string displayopt = string.Empty;
                                        string displayText = string.Empty;
                                        string text = string.Empty;
                                        if (repeatId == Convert.ToInt32(ansExpression.Split('~')[0]))
                                        {
                                            text = "_Absent";
                                            repeatId = 0;
                                        }
                                        else
                                        {
                                            text = "_Present";
                                        }
                                        foreach (var item in optionDisplayOrder)
                                        {
                                            if (Convert.ToString(item.OptionId) == ansExpression.Split('~')[0])
                                            {
                                                displayText = Convert.ToString(item.Option) + text;
                                                displaycol = Convert.ToString(item.DisplayOrder);
                                                repeatId = Convert.ToInt32(ansExpression.Split('~')[0]);
                                                break;
                                            }
                                        }
                                        foreach (var item in columnDisplayOrder)
                                        {
                                            if (Convert.ToString(item.Id) == ansExpression.Split('~')[1])
                                            {
                                                displayopt = Convert.ToString(item.DisplayOrder);
                                                break;
                                            }
                                        }
                                        //formattedAnswer = "Q" + qustionDisplayOrder + "_M" + displaycol + "-" + displayText + "#" + displayopt;
                                        formattedAnswer = "Q" + questionId + "_M" + displaycol + "-" + displayText + "#" + displayopt;
                                        foreach (DataColumn column in dtF.Columns)
                                        {

                                            string[] columnHeader = column.ColumnName.Split('-');
                                            string[] answer = columnHeader[0].Split('_');
                                            if (columnHeader.Length > 1)
                                            {
                                                string answerInHeader = answer[0] + "_" + answer[1] + "-" + columnHeader[1];
                                                string[] formattedAnswerArr = formattedAnswer.Split('#');
                                                if (Regex.Replace(formattedAnswerArr[0], @"\s+", "", RegexOptions.Multiline) == Regex.Replace(answerInHeader, @"\s+", "", RegexOptions.Multiline))
                                                {
                                                    int index = dr_dtF.Table.Columns[column.ColumnName].Ordinal;
                                                    lstColumnOrdinals.Add(index.ToString());
                                                    count++;
                                                    dr_dtF[index] = formattedAnswerArr[1];
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            else if (questionTypeId > 7)
                            {
                                List<OptionsModel> optionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                List<MatrixTypeModel> columnDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstMatrixTypeModel;
                                List<DDLMatrixTypeModel> dropDownDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstDDlMatrixModel;
                                int qustionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).Select(v => v.DisplayOrder).FirstOrDefault();
                                string[] allAnswers = answerExpression.Split('|');

                                List<string> lstColumnOrdinals = new List<string>();

                                string formattedAnswer = string.Empty;
                                string answerValue = string.Empty;
                                int DDlDisplayOrder = 0;
                                if (allAnswers.Length > 1)
                                {
                                    foreach (var ansExpression in allAnswers)
                                    {
                                        string displaycol = string.Empty;
                                        string displayopt = string.Empty;

                                        if (questionTypeId == 9 || questionTypeId == 14)
                                        {
                                            foreach (var item in optionDisplayOrder)
                                            {
                                                if (Convert.ToString(item.OptionId) == ansExpression.Split('~')[0].Split('*')[0])
                                                {
                                                    displaycol = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            foreach (var item in columnDisplayOrder)
                                            {
                                                if (Convert.ToString(item.Id) == ansExpression.Split('~')[1])
                                                {
                                                    displayopt = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            //formattedAnswer = "Q" + qustionDisplayOrder + "_M" + displaycol + "_C" + displayopt;
                                            formattedAnswer = "Q" + questionId + "_M" + displaycol + "_C" + displayopt;
                                            answerValue = ansExpression.Split('~')[0].Split('*')[1];
                                            if (questionTypeId == 14)
                                                DDlDisplayOrder = dropDownDisplayOrder.Where(l => l.Id == Convert.ToInt32(answerValue)).FirstOrDefault().DDLDisplayOrder;
                                        }
                                        else
                                        {
                                            foreach (var item in optionDisplayOrder)
                                            {
                                                if (Convert.ToString(item.OptionId) == ansExpression.Split('~')[0])
                                                {
                                                    displaycol = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            foreach (var item in columnDisplayOrder)
                                            {
                                                if (Convert.ToString(item.Id) == ansExpression.Split('~')[1])
                                                {
                                                    displayopt = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            //formattedAnswer = "Q" + qustionDisplayOrder + "_M" + displaycol + "_C" + displayopt;
                                            formattedAnswer = "Q" + questionId + "_M" + displaycol + "_C" + displayopt;
                                        }
                                        foreach (DataColumn column in dtF.Columns)
                                        {
                                            string[] columnHeader = column.ColumnName.Split('-');

                                            if (columnHeader.Length > 1)
                                            {
                                                string[] answer = columnHeader[0].Split('_');

                                                if (answer.Length > 2)
                                                {
                                                    string answerInHeader = answer[0] + "_" + answer[1] + "_" + answer[2];

                                                    if (formattedAnswer == answerInHeader)
                                                    {
                                                        int index = dr_dtF.Table.Columns[column.ColumnName].Ordinal;
                                                        lstColumnOrdinals.Add(index.ToString());
                                                        count++;
                                                        if (questionTypeId == 9)
                                                        {
                                                            dr_dtF[index] = answerValue;
                                                        }
                                                        else if (questionTypeId == 14)
                                                        {
                                                            dr_dtF[index] = DDlDisplayOrder;
                                                        }
                                                        else
                                                        {
                                                            dr_dtF[index] = "1";
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }

                                // Fill All the Empty cells with 2

                                for (int i = 0; i <= dtF.Columns.Count - 1; i++)
                                    if (String.IsNullOrEmpty(Convert.ToString(dr_dtF[i])))
                                        dr_dtF[i] = "2";
                            }

                            else
                            {
                                dr_dtF[count] = answerExpression;
                                count++;
                                optionsCount--;
                            }

                            if (productId != oldProductID)
                            {
                                dtF.Rows.Add(drTemp);
                                //  drTemp[0] = dtF.Rows.IndexOf(drTemp) + 1;
                                drTemp[0] = response_id;
                                oldProductID = productId;
                            }

                            drTemp = dr_dtF;
                            //---ss
                            if (model.projectModel.ProjectType == "2")
                            {
                                drTemp[2] = product_desc;
                                drTemp[1] = product_logiId;
                            }
                            //---ss

                        }

                        dtF.Rows.Add(dr_dtF);
                        //dr_dtF[0] = dtF.Rows.IndexOf(dr_dtF) + 1;
                        drTemp[0] = response_id;
                        //---ss
                        if (model.projectModel.ProjectType == "2")
                        {
                            dr_dtF[2] = product_desc;
                            dr_dtF[1] = product_logiId;
                        }
                        response_id++;
                        //---ss
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return dtF;
        }
        #endregion

        #region GetReportDataTableOffline
        private DataTable GetReportDataTableOffline(string Id, int ResponseCount, string[] ExternalResponders)
        {

            RespondentController respondent = new RespondentController();
            SurveyResponseModel model = new SurveyResponseModel();
            DataTable dtF = new DataTable();
            try
            {
                DataTable dt = new DataTable();

                string projectGuid = string.Empty;
                projectGuid = Id;
                dt.Columns.Add("Resp_Id", typeof(string));

                model = respondent.GetProjectPreview(projectGuid);

                //--ss
                if (model.projectModel.ProjectType == "2")
                {
                    DataRow drnew = dt.NewRow();
                    drnew[0] = "Product_LogicalId";
                    dt.Rows.Add(drnew);
                    DataRow drnew1 = dt.NewRow();
                    drnew1[0] = "Product_Name";
                    dt.Rows.Add(drnew1);
                }
                //--

                // Load all the questions of this project
                List<QuestionList> questionsList = new List<QuestionList>();
                questionBase.ProjectId = Convert.ToInt32(model.projectModel.Id);
                actionResult = adminAction.QuestionsLoadAll_ByProjectId(questionBase);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    foreach (DataRow dr in actionResult.dtResult.Rows)
                    {
                        questionsList.Add(new QuestionList
                        {
                            id = dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0,
                            questionTypeId = dr["QuestionTypeId"] != DBNull.Value ? Convert.ToInt32(dr["QuestionTypeId"]) : 0
                        });
                    }
                }

                string fileName = Guid.NewGuid().ToString().Substring(0, 5) + "_" + DateTime.Now.ToString("MM-dd-yyyy") + "_" + model.projectModel.ProjectName.Replace(" ", "");

                if (model.lstQuestionModel != null && model.lstQuestionModel.Count > 0)
                {
                    foreach (var item in model.lstQuestionModel)
                    {
                        if (item.QuestionTypeId == 2 || item.QuestionTypeId == 4 || item.QuestionTypeId == 5 || item.QuestionTypeId == 6)//Multi Punch || Constant Sum || Ranking || Matrix Type
                        {
                            if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
                            {
                                foreach (var opt in item.lstOptionsModel)
                                {
                                    DataRow drnew = dt.NewRow();
                                    //drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                    drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                    dt.Rows.Add(drnew);
                                }
                            }
                        }

                        else if (item.QuestionTypeId == 1 || item.QuestionTypeId == 3 || item.QuestionTypeId == 7 || item.QuestionTypeId == 15)//Single Punch || Range || Simple Text
                        {
                            DataRow drnew = dt.NewRow();
                            //drnew[0] = "Q" + item.DisplayOrder + "_S" + "-" + ReplaceHtml.ReplaceText(item.QuestionTitle);
                            drnew[0] = "Q" + item.QuestionId + "_S" + "-" + ReplaceHtml.ReplaceText(item.QuestionTitle);
                            dt.Rows.Add(drnew);
                        }
                        else if (item.QuestionTypeId == 8)//Single punch matrix type
                        {
                            if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
                            {
                                foreach (var opt in item.lstOptionsModel)
                                {
                                    DataRow drnew = dt.NewRow();
                                    //drnew[0] = "Q" + item.QuestionId + "_M" + opt.OptionId + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                    //drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                    drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option);
                                    dt.Rows.Add(drnew);
                                }
                            }
                        }
                        else if (item.QuestionTypeId == 13)//Kano model matrix type 
                        {
                            if (item.lstOptionsModel != null && item.lstOptionsModel.Count > 0)
                            {
                                foreach (var opt in item.lstOptionsModel)
                                {
                                    DataRow drnew = dt.NewRow();
                                    //drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Present";
                                    drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Present";
                                    dt.Rows.Add(drnew);
                                    DataRow drnew1 = dt.NewRow();
                                    //drnew1[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Absent";
                                    drnew1[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + "_Absent";
                                    dt.Rows.Add(drnew1);
                                }
                            }
                        }
                        // Single Punch Matrix Type || Range Matrix Type Question ||
                        // Single punch type answer horizontal position || 
                        // Multiple punch type answer horizontal position || 
                        // Rank order type question || 
                        // KANO Model Matrix type question || Dropdown Matrix type question
                        else if (item.QuestionTypeId > 7 && item.QuestionTypeId != 15)
                        {
                            if (item.lstOptionsModel.Count > 0)
                            {
                                foreach (var opt in item.lstOptionsModel)
                                {
                                    foreach (var column in item.lstMatrixTypeModel)
                                    {
                                        DataRow drnew = dt.NewRow();
                                        //drnew[0] = "Q" + item.DisplayOrder + "_M" + opt.DisplayOrder + "_C" + column.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + " " + ReplaceHtml.ReplaceText(column.ColumnName);
                                        drnew[0] = "Q" + item.QuestionId + "_M" + opt.DisplayOrder + "_C" + column.DisplayOrder + "-" + ReplaceHtml.ReplaceText(opt.Option) + " " + ReplaceHtml.ReplaceText(column.ColumnName);
                                        dt.Rows.Add(drnew);
                                    }
                                }
                            }
                        }
                    }
                }
                // Get the Excel Pattern of dt
                dtF = comMethods.GenerateTransposedTable(dt);

                // Get all the reponders
                projectBase.Id = model.projectModel.Id;
                actionResult = adminAction.Responders_Offline_LoadBy_ProjectId(projectBase);

                // Get Responses
                SurveyLoadBase surveyLoadBase = new SurveyLoadBase();
                string json = string.Empty;
                SRV.BaseLayer.ActionResult actionResultResponses = new SRV.BaseLayer.ActionResult();
                int response_id = ResponseCount;            //ss
                foreach (DataRow dr in actionResult.dtResult.Rows)
                {
                    surveyLoadBase.Id = model.projectModel.Id;
                    surveyLoadBase.OfflineResponderId = dr["RespId"] != DBNull.Value ? Convert.ToString(dr["RespId"]) : "";
                    //DataRow dr_dtF = dtF.NewRow();

                    if (String.IsNullOrEmpty(surveyLoadBase.OfflineResponderId)) continue;


                    DataTable responsesDt = new DataTable();

                    if (Array.IndexOf(ExternalResponders, surveyLoadBase.OfflineResponderId) > -1)
                    {
                        if (model.projectModel.lstProductProjectModel != null && model.projectModel.lstProductProjectModel.Count > 0)
                        {
                            foreach (var productModel in model.projectModel.lstProductProjectModel)
                            {
                                surveyLoadBase.ProductId = productModel.Id;
                                actionResultResponses = respondentAction.Response_Offline_LoadBy_ProjectId_RespId(surveyLoadBase);
                                if (actionResultResponses.IsSuccess)
                                {
                                    if (responsesDt.Rows.Count == 0)
                                        responsesDt = actionResultResponses.dtResult.Copy();
                                    else
                                    {
                                        foreach (DataRow drThisRespRow in actionResultResponses.dtResult.Rows)
                                            responsesDt.ImportRow(drThisRespRow);

                                    }
                                }
                            }
                        }
                        else
                        {
                            actionResultResponses = respondentAction.Response_Offline_LoadBy_ProjectId_RespId(surveyLoadBase);
                            if (actionResultResponses.IsSuccess)
                                responsesDt = actionResultResponses.dtResult.Copy();

                        }
                    }


                    //actionResultResponses = respondentAction.Response_Offline_LoadBy_ProjectId_RespId(surveyLoadBase);
                    if (responsesDt.Rows.Count > 0)//actionResultResponses.IsSuccess
                    {
                        //dr_dtF[0] = surveyLoadBase.Email;

                        int count = 1;
                        count = (model.projectModel.ProjectType == "2") ? count + 2 : count;        //ss
                        DataRow dr_dtF = dtF.NewRow();
                        DataRow drTemp = dtF.NewRow();
                        string product_desc = string.Empty;
                        string product_logiId = string.Empty;
                        int oldProductID = responsesDt.Rows[0]["ProductId"] != DBNull.Value ? Convert.ToInt32(responsesDt.Rows[0]["ProductId"]) : 0;
                        foreach (DataRow drResponse in responsesDt.Rows)// actionResultResponses.dtResult.Rows
                        {

                            int optionsCount = 0;

                            int productId = drResponse["ProductId"] != DBNull.Value ? Convert.ToInt32(drResponse["ProductId"]) : 0;
                            if (productId != oldProductID)
                            {
                                count = (model.projectModel.ProjectType == "2") ? 3 : 1;
                                dr_dtF = dtF.NewRow();
                            }

                            //--ss

                            product_desc = drResponse["ProductDescription"] != DBNull.Value ? Convert.ToString(drResponse["ProductDescription"]) : "";
                            product_logiId = drResponse["ProductLogicalId"] != DBNull.Value ? Convert.ToString(drResponse["ProductLogicalId"]) : "";

                            //--ss
                            int questionId = drResponse["QuestionId"] != DBNull.Value ? Convert.ToInt32(drResponse["QuestionId"]) : 0;
                            string answerExpression = drResponse["AnswerExpression"] != DBNull.Value ? Convert.ToString(drResponse["AnswerExpression"]) : "";
                            int questionTypeId = drResponse["QuestionTypeId"] != DBNull.Value ? Convert.ToInt32(drResponse["QuestionTypeId"]) : 1;
                            //dr_dtF[0] = drResponse;

                            // Get Total options count for this question
                            questionBase.Id = Convert.ToInt32(questionId);
                            SRV.BaseLayer.ActionResult actionResultAnswersTemp = new SRV.BaseLayer.ActionResult();
                            actionResultAnswersTemp = adminAction.Option_LoadAllById(questionBase);
                            if (actionResultAnswersTemp.dtResult != null && actionResultAnswersTemp.dtResult.Rows.Count > 0)
                            {
                                optionsCount = actionResultAnswersTemp.dtResult.Rows.Count;
                            }

                            if (questionTypeId == 2 || questionTypeId == 4 || questionTypeId == 5 || questionTypeId == 6)
                            {
                                string[] allAnswers = answerExpression.Split('|');
                                if (allAnswers.Length > 0)
                                {
                                    if (questionTypeId == 2)// Multi Punch
                                    {
                                        string allRec = string.Empty;
                                        List<OptionsModel> matrixAns = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;

                                        foreach (var item in matrixAns)
                                        {
                                            string matchedId = string.Empty;
                                            foreach (string ans in allAnswers)
                                            {
                                                if (Convert.ToString(item.OptionId) == ans)
                                                {
                                                    matchedId = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            allRec += matchedId + "|";
                                        }

                                        string[] allAnswersMat = allRec.Substring(0, allRec.Length - 1).Split('|');
                                        foreach (string ans in allAnswersMat)
                                        {

                                            if (count <= dtF.Columns.Count - 1)
                                            {
                                                if ((Regex.Replace(dtF.Columns[count].ToString().Split('-')[0].Split('_')[1], @"[^\d]", String.Empty)) == ans)
                                                {
                                                    dr_dtF[count] = "1";// ans;
                                                    count++;
                                                    optionsCount--;
                                                }
                                                else
                                                {
                                                    dr_dtF[count] = "2";
                                                    count++;
                                                    optionsCount--;
                                                }
                                            }
                                        }
                                        //count = count + optionsCount;
                                    }
                                    else if (questionTypeId == 6) // matrix Type
                                    {
                                        foreach (string ans in allAnswers)
                                        {
                                            dr_dtF[count] = (ans.ToLower().IndexOf("yes") != -1) ? "1" : "2";
                                            count++;
                                            optionsCount--;
                                        }
                                        count = count + optionsCount;
                                    }
                                    else
                                    {
                                        foreach (string ans in allAnswers)
                                        {
                                            dr_dtF[count] = ans;
                                            count++;
                                            optionsCount--;
                                        }
                                        count = count + optionsCount;
                                    }
                                }
                            }
                            else if (questionTypeId == 1) // Single Punch Answers
                            {
                                List<OptionsModel> SinglePunchAns = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                int ansIndex = 0;
                                foreach (var item in SinglePunchAns)
                                {
                                    ansIndex++;
                                    if (Convert.ToString(item.OptionId) == answerExpression)
                                    {
                                        break;
                                    }
                                }

                                dr_dtF[count] = ansIndex;//answerExpression;
                                count++;
                                optionsCount--;
                            }
                            else if (questionTypeId == 8 || questionTypeId == 12) // single punch matrix answers
                            {
                                List<OptionsModel> optionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                List<MatrixTypeModel> columnDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstMatrixTypeModel;
                                int qustionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).Select(v => v.DisplayOrder).FirstOrDefault();
                                string[] allAnswers = answerExpression.Split('|');

                                List<string> lstColumnOrdinals = new List<string>();

                                string formattedAnswer = string.Empty;
                                string answerValue = string.Empty;
                                if (allAnswers.Length > 1)
                                {
                                    foreach (var ansExpression in allAnswers)
                                    {
                                        string displaycol = string.Empty;
                                        string displayopt = string.Empty;
                                        foreach (var item in optionDisplayOrder)
                                        {

                                            if (Convert.ToString(item.OptionId) == ansExpression.Split('~')[0])
                                            {
                                                displaycol = Convert.ToString(item.DisplayOrder);
                                                break;
                                            }
                                        }
                                        foreach (var item in columnDisplayOrder)
                                        {

                                            if (Convert.ToString(item.Id) == ansExpression.Split('~')[1])
                                            {
                                                displayopt = Convert.ToString(item.DisplayOrder);
                                                break;
                                            }
                                        }
                                        // formattedAnswer = "Q" + qustionDisplayOrder + "_M" + displaycol + "#" + displayopt;
                                        formattedAnswer = "Q" + questionId + "_M" + displayopt + "#" + displaycol;
                                        foreach (DataColumn column in dtF.Columns)
                                        {
                                            string[] columnHeader = column.ColumnName.Split('-');
                                            string[] answer = columnHeader[0].Split('_');
                                            if (columnHeader.Length > 1)
                                            {
                                                //if (answer.Length > 2)
                                                //{
                                                string answerInHeader = answer[0] + "_" + answer[1];
                                                string[] formattedAnswerArr = formattedAnswer.Split('#');
                                                if (formattedAnswerArr[0] == answerInHeader)
                                                {
                                                    int index = dr_dtF.Table.Columns[column.ColumnName].Ordinal;
                                                    lstColumnOrdinals.Add(index.ToString());
                                                    count++;
                                                    dr_dtF[index] = formattedAnswerArr[1];
                                                }
                                                //}
                                            }
                                        }
                                    }
                                }
                            }
                            else if (questionTypeId == 13) // Kano model matrix type
                            {
                                List<OptionsModel> optionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                List<MatrixTypeModel> columnDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstMatrixTypeModel;
                                int qustionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).Select(v => v.DisplayOrder).FirstOrDefault();
                                string[] allAnswers = answerExpression.Split('|');

                                List<string> lstColumnOrdinals = new List<string>();

                                string formattedAnswer = string.Empty;
                                string answerValue = string.Empty;
                                if (allAnswers.Length > 1)
                                {
                                    int repeatId = 0;
                                    foreach (var ansExpression in allAnswers)
                                    {
                                        string displaycol = string.Empty;
                                        string displayopt = string.Empty;
                                        string displayText = string.Empty;
                                        string text = string.Empty;
                                        if (repeatId == Convert.ToInt32(ansExpression.Split('~')[0]))
                                        {
                                            text = "_Absent";
                                            repeatId = 0;
                                        }
                                        else
                                        {
                                            text = "_Present";
                                        }
                                        foreach (var item in optionDisplayOrder)
                                        {
                                            if (Convert.ToString(item.OptionId) == ansExpression.Split('~')[0])
                                            {
                                                displayText = Convert.ToString(item.Option) + text;
                                                displaycol = Convert.ToString(item.DisplayOrder);
                                                repeatId = Convert.ToInt32(ansExpression.Split('~')[0]);
                                                break;
                                            }
                                        }
                                        foreach (var item in columnDisplayOrder)
                                        {
                                            if (Convert.ToString(item.Id) == ansExpression.Split('~')[1])
                                            {
                                                displayopt = Convert.ToString(item.DisplayOrder);
                                                break;
                                            }
                                        }
                                        // formattedAnswer = "Q" + qustionDisplayOrder + "_M" + displaycol + "-" + displayText + "#" + displayopt;
                                        formattedAnswer = "Q" + questionId + "_M" + displaycol + "-" + displayText + "#" + displayopt;
                                        foreach (DataColumn column in dtF.Columns)
                                        {
                                            string[] columnHeader = column.ColumnName.Split('-');
                                            string[] answer = columnHeader[0].Split('_');
                                            if (columnHeader.Length > 1)
                                            {
                                                //if (answer.Length > 2)
                                                //{
                                                string answerInHeader = answer[0] + "_" + answer[1] + "-" + columnHeader[1];
                                                string[] formattedAnswerArr = formattedAnswer.Split('#');
                                                if (Regex.Replace(formattedAnswerArr[0], @"\s+", "", RegexOptions.Multiline) == Regex.Replace(answerInHeader, @"\s+", "", RegexOptions.Multiline))
                                                {
                                                    int index = dr_dtF.Table.Columns[column.ColumnName].Ordinal;
                                                    lstColumnOrdinals.Add(index.ToString());
                                                    count++;
                                                    dr_dtF[index] = formattedAnswerArr[1];
                                                }
                                                //}
                                            }
                                        }
                                    }
                                }
                            }
                            else if (questionTypeId > 7)
                            {
                                List<OptionsModel> optionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstOptionsModel;
                                List<MatrixTypeModel> columnDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstMatrixTypeModel;
                                List<DDLMatrixTypeModel> dropDownDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).FirstOrDefault().lstDDlMatrixModel;
                                int qustionDisplayOrder = model.lstQuestionModel.Where(v => v.QuestionId == questionId).Select(v => v.DisplayOrder).FirstOrDefault();
                                string[] allAnswers = answerExpression.Split('|');

                                List<string> lstColumnOrdinals = new List<string>();

                                string formattedAnswer = string.Empty;
                                string answerValue = string.Empty;
                                int DDlDisplayOrder = 0;
                                if (allAnswers.Length > 1)
                                {
                                    foreach (var ansExpression in allAnswers)
                                    {
                                        string displaycol = string.Empty;
                                        string displayopt = string.Empty;

                                        if (questionTypeId == 9 || questionTypeId == 14)
                                        {
                                            foreach (var item in optionDisplayOrder)
                                            {

                                                if (Convert.ToString(item.OptionId) == ansExpression.Split('~')[0].Split('*')[0])
                                                {
                                                    displaycol = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            foreach (var item in columnDisplayOrder)
                                            {

                                                if (Convert.ToString(item.Id) == ansExpression.Split('~')[1])
                                                {
                                                    displayopt = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            formattedAnswer = "Q" + qustionDisplayOrder + "_M" + displaycol + "_C" + displayopt;
                                            answerValue = ansExpression.Split('~')[0].Split('*')[1];
                                            if (questionTypeId == 14)
                                                DDlDisplayOrder = dropDownDisplayOrder.Where(l => l.Id == Convert.ToInt32(answerValue)).FirstOrDefault().DDLDisplayOrder;
                                        }
                                        else
                                        {

                                            foreach (var item in optionDisplayOrder)
                                            {

                                                if (Convert.ToString(item.OptionId) == ansExpression.Split('~')[0])
                                                {
                                                    displaycol = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            foreach (var item in columnDisplayOrder)
                                            {

                                                if (Convert.ToString(item.Id) == ansExpression.Split('~')[1])
                                                {
                                                    displayopt = Convert.ToString(item.DisplayOrder);
                                                    break;
                                                }
                                            }
                                            formattedAnswer = "Q" + qustionDisplayOrder + "_M" + displaycol + "_C" + displayopt;
                                        }
                                        foreach (DataColumn column in dtF.Columns)
                                        {
                                            string[] columnHeader = column.ColumnName.Split('-');

                                            if (columnHeader.Length > 1)
                                            {
                                                string[] answer = columnHeader[0].Split('_');

                                                if (answer.Length > 2)
                                                {
                                                    string answerInHeader = answer[0] + "_" + answer[1] + "_" + answer[2];

                                                    if (formattedAnswer == answerInHeader)
                                                    {
                                                        int index = dr_dtF.Table.Columns[column.ColumnName].Ordinal;
                                                        lstColumnOrdinals.Add(index.ToString());
                                                        count++;
                                                        if (questionTypeId == 9)
                                                        {
                                                            dr_dtF[index] = answerValue;
                                                        }
                                                        else if (questionTypeId == 14)
                                                        {
                                                            dr_dtF[index] = DDlDisplayOrder;
                                                        }
                                                        else
                                                        {
                                                            dr_dtF[index] = "1";
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }

                                // Fill All the Empty cells with 2

                                for (int i = 0; i <= dtF.Columns.Count - 1; i++)
                                    if (String.IsNullOrEmpty(Convert.ToString(dr_dtF[i])))
                                        dr_dtF[i] = "2";
                            }

                            else
                            {
                                dr_dtF[count] = answerExpression;
                                count++;
                                optionsCount--;
                            }



                            if (productId != oldProductID)
                            {
                                dtF.Rows.Add(drTemp);
                                drTemp[0] = response_id;
                                oldProductID = productId;
                            }

                            drTemp = dr_dtF;
                            //---ss
                            if (model.projectModel.ProjectType == "2")
                            {
                                drTemp[2] = product_desc;
                                drTemp[1] = product_logiId;
                            }
                            //---ss


                        }


                        dtF.Rows.Add(dr_dtF);
                        drTemp[0] = response_id;
                        //---ss
                        if (model.projectModel.ProjectType == "2")
                        {
                            dr_dtF[2] = product_desc;
                            dr_dtF[1] = product_logiId;
                        }
                        response_id++;
                        //---ss
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return dtF;
        }
        #endregion

        #region Bind Option By Question
        public ActionResult BindOptionByQuestion(string Id)
        {
            List<OptionListModel> optionList = new List<OptionListModel>();

            if (!string.IsNullOrEmpty(Id))
            {
                questionBase.Id = Convert.ToInt32(Id);
                actionResult = adminAction.Option_LoadAllById(questionBase);
                if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                {
                    optionList = SurveyProject.Helper.CommHelper.ConvertTo<OptionListModel>(actionResult.dtResult);
                }
            }
            return Json(Newtonsoft.Json.JsonConvert.SerializeObject(optionList), JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region GetQueryReport
        [ValidateInputAttribute(false)]
        [HttpPost]
        public ActionResult GetQueryReport(FormCollection fc)
        {
            int CaseID = fc["CaseId"] != null && fc["CaseId"] != "" ? Convert.ToInt32(fc["CaseId"]) : 0;
            string QueryBuilder = fc["hdnValues"] != null && fc["hdnValues"] != "" ? Convert.ToString(fc["hdnValues"]) : "";
            string hdnInsertUpdate = fc["hdnInsertUpdate"] != null && fc["hdnInsertUpdate"] != "" ? Convert.ToString(fc["hdnInsertUpdate"]) : "";
            string QueryHtml = fc["hdnQueryData"] != null && fc["hdnQueryData"] != "" ? Convert.ToString(fc["hdnQueryData"]) : "";
            string CaseName = fc["caseName"] != null && fc["caseName"] != "" ? Convert.ToString(fc["caseName"]) : "";
            string json = string.Empty;
            string Email = string.Empty;
            string ExternalResponders = string.Empty;
            Session["Type"] = "QueryBuilder";
            int ProjectId = fc["hdnProjectId"] != null ? Convert.ToInt32(fc["hdnProjectId"]) : 0;
            //queryBase.ProjectId = ProjectId;            
            //queryBase.CaseName = CaseName;
            //queryBase.Query = QueryBuilder;
            //actionResult = adminAction.Query_Insert_Update(queryBase);
            //if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
            //{
            //    if (Convert.ToInt32(actionResult.dtResult.Rows[0]["result"]) == 1)
            //    {
            //        TempData["SuccessMessage"] = "Query saved successfully.";
            //    }
            //    else
            //    {
            //        TempData["ErrorMessage"] = "Case Name  aleready exits. Please enter new ! ";
            //    }
            //}
            //else
            //{
            //    TempData["ErrorMessage"] = "Error in saving query";
            //}



            List<string> lstEmails = new List<string>();
            List<string> lstResponders = new List<string>();
            List<string> offlineResponders = new List<string>();
            if (CaseContainer.Columns.Count == 0)
            {
                CaseContainer.Columns.Add("CaseName", typeof(string));
                CaseContainer.Columns.Add("Online", typeof(DataTable));
                CaseContainer.Columns.Add("Offline", typeof(DataTable));
                CaseContainer.Columns.Add("Combined", typeof(DataTable));
            }
            DataTable dtCase = new DataTable();
            dtCase.Columns.Add("Id", typeof(int));
            dtCase.Columns.Add("QuestionId", typeof(int));
            dtCase.Columns.Add("QuestionTypeId", typeof(int));
            dtCase.Columns.Add("OptionIdOrDisplayOrder", typeof(int));
            dtCase.Columns.Add("ColumnIdOrDisplayOrder", typeof(int));
            dtCase.Columns.Add("Expression", typeof(string));
            dtCase.Columns.Add("NextQuestionAddWith", typeof(string));
            ViewBag.ProjectId = ProjectId;
            string[] query = QueryBuilder.TrimEnd('*').Split('*');
            try
            {
                if (query.Length > 0)
                {
                    for (int i = 0; i <= query.Length - 1; i++)
                    {
                        string[] questionArr = query[i].Split('_'); //1602_5$>=11#OR|2~
                        int questionId = Convert.ToInt32(questionArr[0]);
                        string[] exprArr = questionArr[1].Split('$');
                        int questionTypeId = Convert.ToInt32(exprArr[0]);
                        string Expression = exprArr[1].Split('#')[0];
                        string[] optionIdArr = exprArr[1].Split('#')[1].Split('|');
                        string NextQuestionAddWith = optionIdArr[0];
                        int optionId = optionIdArr[1].Split('~')[0] != "" ? Convert.ToInt32(optionIdArr[1].Split('~')[0]) : 0;
                        int columnId = optionIdArr[1].Split('~')[1] != "" ? Convert.ToInt32(optionIdArr[1].Split('~')[1]) : 0;
                        DataRow drCase = dtCase.NewRow();
                        drCase["Id"] = (i + 1);
                        drCase["QuestionId"] = questionId;
                        drCase["QuestionTypeId"] = questionTypeId;
                        drCase["OptionIdOrDisplayOrder"] = optionId;
                        drCase["ColumnIdOrDisplayOrder"] = columnId;
                        drCase["Expression"] = Expression;
                        drCase["NextQuestionAddWith"] = NextQuestionAddWith;
                        dtCase.Rows.Add(drCase);
                    }
                }
                projectBase.CaseTable = dtCase;



                bool check = false;
                actionResult = adminAction.Response_CaseSelectionReport(projectBase);
                if (actionResult.IsSuccess)
                {
                    check = true;
                }

                //CreateProjectModel model = new CreateProjectModel();
                //model = GetProjectDetailById(ProjectId);
                //string projectGUID = model.ProjectGuid;

                //DataTable dt = GetReportDataTable(projectGUID, lstEmails.ToArray(), lstResponders.ToArray());
                actionResult = adminAction.Response_CaseSelectionReportOffline(projectBase);
                if (actionResult.IsSuccess)
                {
                    check = true;
                }
                //int RespCount = 0;
                //DataTable dt1 = GetReportDataTableOffline(projectGUID, (RespCount + 1), offlineResponders.ToArray());
                //if (dt.Rows.Count == 0 && dt1.Rows.Count == 0)
                //{
                //    //TempData["ErrorMessage"] = "There is no data available regarding this query";
                //    //return RedirectToAction("QueryBuilder", "QueryBuilder", new { ProjectId = ProjectId, Area = "Report" });
                //}
                //DataRow CaseRow = CaseContainer.NewRow();
                //CaseRow[0] = fc["caseName"].ToString();
                //CaseRow[1] = dt;
                //CaseRow[2] = dt1;
                //dt.Merge(dt1);
                //dt.AcceptChanges();
                //CaseRow[3] = dt;
                //CaseContainer.Rows.Add(CaseRow);
                //ViewBag.CaseContainer = CaseContainer;
                //  Session["CaseContainer"] = CaseContainer;


                if (check == true)
                {
                    queryBase.ProjectId = ProjectId;
                    queryBase.CaseName = CaseName;
                    queryBase.Query = QueryBuilder;
                    queryBase.QueryHtml = QueryHtml;
                    queryBase.CaseId = CaseID;
                    if (hdnInsertUpdate == "Insert")
                    {
                        actionResult = adminAction.Query_Insert_Update(queryBase);
                    }
                    else if (hdnInsertUpdate == "Update")
                    {
                        actionResult = adminAction.QueryUpdate(queryBase);
                    }
                    if (actionResult.dtResult != null && actionResult.dtResult.Rows.Count > 0)
                    {
                        if (Convert.ToInt32(actionResult.dtResult.Rows[0]["result"]) == 1)
                        {
                            TempData["SuccessMessage"] = "Query saved successfully.";
                        }
                        else if (Convert.ToInt32(actionResult.dtResult.Rows[0]["result"]) == 2)
                        {
                            TempData["SuccessMessage"] = "Query Updated successfully.";
                        }
                        else if (Convert.ToInt32(actionResult.dtResult.Rows[0]["result"]) == 3)
                        {
                            TempData["ErrorMessage"] = "Case name alerady exists";
                        }
                        else
                        {
                            TempData["ErrorMessage"] = "Error in saving query";
                        }
                    }
                    else
                    {
                        TempData["ErrorMessage"] = "Error in saving query";
                    }
                }
                else
                {
                    TempData["ErrorMessage"] = "There is no data available regarding this query"; ;
                }

            }
            catch (Exception ex)
            {
                TempData["ErrorMessage"] = "There is no data available regarding this query";
                ErrorReporting.WebApplicationError(ex);
            }
            return RedirectToAction("QueryBuilder", "QueryBuilder", new { ProjectId = ProjectId, Area = "Report" });
        }
        #endregion

        #region Bind Columns By QuestionId
        public ActionResult BindColumnByQuestion(int questionId, int questionTypeId)
        {
            List<MatrixTypeModel> columnList = new List<MatrixTypeModel>();
            List<DDLMatrixTypeModel> dropDownList = new List<DDLMatrixTypeModel>();
            if (questionId > 0 && questionTypeId > 0)
            {
                questionBase.QuestionTypeId = Convert.ToInt32(questionTypeId);
                questionBase.Id = Convert.ToInt32(questionId);
                actionResult = adminAction.MatrixType_LoadByQuestionId(questionBase);
                if (actionResult.dsResult != null && actionResult.dsResult.Tables[0].Rows.Count > 0)
                {
                    columnList = SurveyProject.Helper.CommHelper.ConvertTo<MatrixTypeModel>(actionResult.dsResult.Tables[0]);
                    if (questionTypeId == 14)
                    {
                        if (actionResult.dsResult.Tables[1].Rows.Count > 0)
                            dropDownList = SurveyProject.Helper.CommHelper.ConvertTo<DDLMatrixTypeModel>(actionResult.dsResult.Tables[1]);
                    }
                }
            }
            return Json(Newtonsoft.Json.JsonConvert.SerializeObject(columnList), JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region DisplayProjectList
        public ActionResult DisplayProjectList(int? dir = 0)
        {
            int TotalProject = 0;
            ManageProjectModel model = new ManageProjectModel();
            List<CreateProjectModel> tempLst = new List<CreateProjectModel>();

            List<SuperProjectModel> lstSuperProjectModel = new List<SurveyProject.Models.SuperProjectModel>();
            List<CreateProjectModel> lstProjectModel = new List<CreateProjectModel>();
            model.lstSuperProjectModel = lstSuperProjectModel;
            model.lstProjectModel = lstProjectModel;

            try
            {
                int UserId = Convert.ToInt32(Session["UserId"]);
                adminBase.Id = UserId;
                actionResult = adminAction.Poject_LoadAll(adminBase);
                if (actionResult.dsResult != null && actionResult.dsResult.Tables[0].Rows.Count > 0)
                {
                    //DataTable dtResult = actionResult.dsResult.Tables[0].Select("SuperProjectId is Not Null and SuperProjectId <> '0'").CopyToDataTable();
                    DataTable dtResult1 = actionResult.dsResult.Tables[0].Select("SuperProjectId is Null or SuperProjectId = '0'").CopyToDataTable();

                    if (actionResult.dsResult.Tables[0] != null && actionResult.dsResult.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow dr in actionResult.dsResult.Tables[0].Rows)
                        {
                            if (dr["SuperProjectId"] != DBNull.Value && Convert.ToInt32(dr["SuperProjectId"]) != 0)
                            {
                                tempLst.Add(new CreateProjectModel
                                {
                                    Id = dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0,
                                    ProjectName = dr["ProjectName"] != DBNull.Value ? Convert.ToString(dr["ProjectName"]) : "",
                                    ProjectQuota = dr["ProjectQuota"] != DBNull.Value ? Convert.ToInt32(dr["ProjectQuota"]) : 0,
                                    SuperProjectId = dr["SuperProjectId"] != DBNull.Value ? Convert.ToInt32(dr["SuperProjectId"]) : 0,
                                    ProjectType = dr["ProjectType"] != DBNull.Value ? Convert.ToString(dr["ProjectType"]) : "",
                                    ProjectGuid = dr["ProjectGuid"] != DBNull.Value ? Convert.ToString(dr["ProjectGuid"]) : "",
                                    ProductsCount = dr["ProductsCount"] != DBNull.Value ? Convert.ToInt32(dr["ProductsCount"]) : 0,
                                    DisplayOrder = dr["DisplayOrder"] != DBNull.Value ? Convert.ToInt32(dr["DisplayOrder"]) : 0,
                                    CreatedOn = dr["CreatedOn"] != DBNull.Value ? Convert.ToDateTime(dr["CreatedOn"]).ToShortDateString() : "",
                                    ModifiedOn = dr["ModifiedOn"] != DBNull.Value ? Convert.ToDateTime(dr["ModifiedOn"]).ToShortDateString() : "",
                                    DirectoryId = dr["DirectoryId"] != DBNull.Value ? Convert.ToInt32(dr["DirectoryId"]) : 0,
                                    DirectoryIdCol = dr["DirectoryIdCol"] != DBNull.Value ? Convert.ToInt32(dr["DirectoryIdCol"]) : 0
                                });
                            }
                        }
                    }

                    foreach (DataRow dr in dtResult1.Rows)
                    {
                        lstProjectModel.Add(new CreateProjectModel
                        {
                            Id = dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0,
                            ProjectName = dr["ProjectName"] != DBNull.Value ? Convert.ToString(dr["ProjectName"]) : "",
                            ProjectQuota = dr["ProjectQuota"] != DBNull.Value ? Convert.ToInt32(dr["ProjectQuota"]) : 0,
                            SuperProjectId = dr["SuperProjectId"] != DBNull.Value ? Convert.ToInt32(dr["SuperProjectId"]) : 0,
                            ProjectType = dr["ProjectType"] != DBNull.Value ? Convert.ToString(dr["ProjectType"]) : "",
                            ProjectGuid = dr["ProjectGuid"] != DBNull.Value ? Convert.ToString(dr["ProjectGuid"]) : "",
                            ProductsCount = dr["ProductsCount"] != DBNull.Value ? Convert.ToInt32(dr["ProductsCount"]) : 0,
                            DisplayOrder = dr["DisplayOrder"] != DBNull.Value ? Convert.ToInt32(dr["DisplayOrder"]) : 0,
                            CreatedOn = dr["CreatedOn"] != DBNull.Value ? Convert.ToDateTime(dr["CreatedOn"]).ToShortDateString() : "",
                            ModifiedOn = dr["ModifiedOn"] != DBNull.Value ? Convert.ToDateTime(dr["ModifiedOn"]).ToShortDateString() : "",
                            DirectoryId = dr["DirectoryId"] != DBNull.Value ? Convert.ToInt32(dr["DirectoryId"]) : 0,
                            DirectoryIdCol = dr["DirectoryIdCol"] != DBNull.Value ? Convert.ToInt32(dr["DirectoryIdCol"]) : 0
                        });
                    }
                    TotalProject = (lstProjectModel.Count);
                    if (Convert.ToInt32(dir) > 0)
                        lstProjectModel = lstProjectModel.Where(l => l.DirectoryId == Convert.ToInt32(dir)).ToList();
                    model.lstProjectModel = lstProjectModel;
                }

                if (actionResult.dsResult != null && actionResult.dsResult.Tables[1].Rows.Count > 0)
                {
                    foreach (DataRow dr in actionResult.dsResult.Tables[1].Rows)
                    {
                        SuperProjectModel superProjectModel = new SuperProjectModel();
                        List<CreateProjectModel> projectListModel = new List<CreateProjectModel>();
                        superProjectModel.ProjectListModel = projectListModel;

                        superProjectModel.Id = dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0;
                        superProjectModel.ProjectName = Convert.ToString(dr["SuperProjectName"]);
                        superProjectModel.OpeningDate = dr["OpeningDate"] != DBNull.Value ? Convert.ToDateTime(dr["OpeningDate"]).ToShortDateString() : "";
                        superProjectModel.DirectoryId = dr["DirectoryId"] != DBNull.Value ? Convert.ToInt32(dr["DirectoryId"]) : 0;
                        superProjectModel.DirectoryIdCol = dr["DirectoryIdCol"] != DBNull.Value ? Convert.ToInt32(dr["DirectoryIdCol"]) : 0;
                        //model.lstSuperProjectModel


                        if (tempLst != null && tempLst.Count > 0)
                        {
                            var result = tempLst.Where(m => m.SuperProjectId == superProjectModel.Id).ToList();
                            if (result != null && result.Count > 0)
                            {
                                foreach (var res in result)
                                {
                                    projectListModel.Add(new CreateProjectModel
                                    {
                                        Id = res.Id,
                                        ProjectName = res.ProjectName,
                                        ProjectQuota = res.ProjectQuota,
                                        SuperProjectId = res.SuperProjectId,
                                        ProjectType = res.ProjectType,
                                        ProjectGuid = res.ProjectGuid,
                                        ProductsCount = res.ProductsCount,
                                        DisplayOrder = res.DisplayOrder,
                                        DirectoryId = res.DirectoryId,
                                        DirectoryIdCol = res.DirectoryIdCol
                                    });
                                }
                            }
                        }
                        projectListModel = projectListModel.OrderBy(l => l.DisplayOrder).ToList();
                        superProjectModel.ProjectListModel = projectListModel;
                        model.lstSuperProjectModel.Add(superProjectModel);
                    }
                    TotalProject += model.lstSuperProjectModel.Count;
                    if (Convert.ToInt32(dir) > 0)
                        model.lstSuperProjectModel = model.lstSuperProjectModel.Where(l => l.DirectoryId == Convert.ToInt32(dir)).ToList();
                    // model.ProjectList = ProjectLst;
                }

                ViewBag.TotalProjectCount = TotalProject;
                //-----------Ekta 21 April 2016
                //List<UserDirectoriesModel> lstUserDirectoryModel = new List<UserDirectoriesModel>();
                //List<ProjectDirectoriesModel> lstprojectDirectoryModel = new List<ProjectDirectoriesModel>();
                //model.lstUserDirectoryModel = lstUserDirectoryModel;
                //userDirectoriesBase.UserId = Convert.ToInt32(Session["UserId"]);
                //actionResult = adminAction.UserDirectories_LoadByUserId(userDirectoriesBase);
                //if (actionResult.IsSuccess)
                //{
                //    lstUserDirectoryModel = Helper.CommHelper.ConvertTo<UserDirectoriesModel>(actionResult.dtResult);
                //}
                //model.lstUserDirectoryModel = lstUserDirectoryModel;

                //foreach (var item in lstUserDirectoryModel)
                //{
                //    projectDirectoriesBase.DirectoryId = Convert.ToInt32(item.Id);
                //    actionResult = adminAction.ProjectDirectories_LoadById(projectDirectoriesBase);
                //    if (actionResult.IsSuccess)
                //        lstprojectDirectoryModel = Helper.CommHelper.ConvertTo<ProjectDirectoriesModel>(actionResult.dtResult);
                //    item.lstProjectDirectoryModel = lstprojectDirectoryModel;
                //}
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return View("_PartialProjectList", model);
        }

        #endregion
        public JsonResult DeleteCase(string caseName)
        {
            DataTable MainDt = (DataTable)Session["CaseContainer"];

            DataRow[] rows;
            // rows = MainDt.Select("caseName = " + caseName + "");


            var data = (from DataRow row in MainDt.Rows
                        where row.Field<string>("caseName") == caseName
                        select row);
            foreach (DataRow row in data)
                MainDt.Rows.Remove(row);
            return Json("success");




        }


        public JsonResult DeleteCaseByName(string caseName)
        {
            string json = string.Empty;
            try
            {

                if (caseName != null || caseName != string.Empty)
                {
                    caseName = caseName.Trim();
                    queryBase.CaseName = caseName;
                }
                actionResult = adminAction.QueryDelete_ByCase(queryBase);
                if (actionResult.IsSuccess)
                {
                    TempData["SuccessMessage"] = "Case deleted successfully";
                    json = "success";

                }
                else
                {
                    TempData["ErrorMessage"] = "Project deletion is Ok but prevented while testing.";
                    json = "error";
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
                json = "error";
            }
            return Json(json, JsonRequestBehavior.AllowGet);
        }

        //public ActionResult SaveCases(string QueryBuilder, int ProjectId)
        //{

        //}
    }
}
