/**
 * @license Copyright (c) 2003-2015, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here.
	// For complete reference see:
	// http://docs.ckeditor.com/#!/api/CKEDITOR.config

    config.uiColor = '#0094ff';
    CKEDITOR.config.autoParagraph = false;

    config.extraPlugins = 'image2';
    config.extraPlugins = 'youtube';
    // config.extraPlugins = 'MediaEmbed';
    //config.extraPlugins = 'save';
    //config.extraPlugins = 'font';
    //config.extraPlugins = 'lineutils';
    //config.extraPlugins = 'find';
    //config.extraPlugins = 'selectall';
    //config.extraPlugins = 'removeformat';
    //config.extraPlugins = 'colorbutton';
    //config.extraPlugins = 'autogrow';
    
    config.extraPlugins = 'justify,save,font,find,selectall,removeformat,colorbutton,autogrow,MediaEmbed';
    //config.autoGrow_onStartup = true;

	// Remove some buttons provided by the standard plugins, which are
	// not needed in the Standard(s) toolbar.

    //config.toolbarGroups = [
    //    { name: 'clipboard', groups: ['clipboard', 'undo'] },
    //    { name: 'editing', groups: ['find', 'selection', 'spellchecker'] },
    //    { name: 'links' },
    //    { name: 'insert' },
    //    { name: 'forms' },
    //    { name: 'tools' },
    //    { name: 'document', groups: ['mode', 'document', 'doctools'] },
    //    { name: 'others' },
    //    '/',
    //    { name: 'basicstyles', groups: ['basicstyles', 'cleanup'] },
    //    { name: 'paragraph', groups: ['list', 'indent', 'blocks', 'align', 'bidi'] },
    //    { name: 'alignment', groups: ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'] },
    //    { name: 'styles' },
    //    { name: 'colors' },
    //    { name: 'about' }
    //];


	config.removeButtons = 'Underline,Subscript,Superscript';

	// Set the most common block elements.
	config.format_tags = 'p;h1;h2;h3;pre';

	// Simplify the dialog windows.
    //config.removeDialogTabs = 'image:advanced;link:advanced';
	config.extraAllowedContent = 'iframe[*]';

    config.toolbar = [
        ['Source','Cut', 'Copy', 'Save', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Scayt'],
        ['Undo', 'Redo', '-', 'Find', 'Replace', '-', 'SelectAll', 'RemoveFormat'],
        ['Image', 'Table', 'HorizontalRule', 'SpecialChar', 'Link', 'Unlink', 'Anchor', 'Maximize', 'MediaEmbed'],//'Smiley'MediaEmbed
        '/',
        ['Styles', 'Format', 'Font', 'FontSize', 'Bold', 'Italic', 'Strike', 'NumberedList', 'BulletedList'],['Outdent', 'Indent', 'Blockquote', 'TextColor', 'BGColor'],
        ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock']
       
    ];
};
