﻿<p style="text-align: center;">
	<span style="font-size:14px;"><span style="color: rgb(128, 0, 0);">Thank you for answering this survey.&nbsp; These are all the questions we have for you today. As a result of your participation you will be entered in the prize drawing.</span></span></p>
<p style="text-align: center;">
	<span style="font-size:14px;">To be entered in the prize drawing, please enter your email address below. The e-mail address you provide will only be used to notify draw winners of their prizes and to arrange delivery. E-mail addresses you provide will not be used to re-contact you for any other purpose.</span></p>
