﻿<div>
	<span style="color:#0066cc;"><span style="font-size: 16px;">Dear Sir/Madam,</span></span></div>
<div>
	<p>
		<span style="color:#0066cc;"><span style="font-size: 14px;">Participating in Research with xDesignLab (applied to respondents)</span></span></p>
	<p>
		<span style="color:#0066cc;"><span style="font-size: 14px;">There are many ways you can take part in market research - from completing surveys online to participating in focus groups or product testing.</span></span></p>
	<p>
		<span style="color:#0066cc;"><span style="font-size: 14px;">By doing so, you directly affect decisions made by some of the world&#39;s most influential businesses, institutions and organizations, public and private alike. You get a say in how things happen.</span></span></p>
	<p>
		<span style="color:#0066cc;"><span style="font-size: 14px;">Research can also be interesting, exciting and fun. Sometimes, participants receive generous incentives such as cash or free products. For some people, the chance to try new products or services before everybody else is incentive enough.</span></span></p>
	<p>
		<span style="color:#0066cc;"><span style="font-size: 14px;">If you&#39;d like to join the thousands of others already taking part in xDesignLab Research studies, just follow the appropriate link below.</span></span></p>
	<p>
		<span style="color:#0066cc;"><span style="font-size: 14px;">We will need to create the form to collect future respondent profiles!</span></span></p>
	<p>
		&nbsp;</p>
</div>
<div>
	<span style="color:#003366;"><strong><span style="font-size: 16px;">Please open the following link in your webbrowser:</span></strong></span></div>
<div>
	&nbsp;</div>