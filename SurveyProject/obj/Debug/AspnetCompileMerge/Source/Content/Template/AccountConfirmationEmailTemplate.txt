﻿<h3>xKubuni™</h3>
Hello {0},
<br/><br/>
First, xKubuni™ would like to say congratulations on your Registration!! 
<br/><br/>
<a href='@info/Account/ConfirmRegistration/{1}'>Click here</a> to confirm your email address.!!
<br/><br/>
Please feel free to contact us if you have any questions.
<br/><br/>
Thanks,
<br/>
Your xKubuni™ Family
<br/><br/>
<a href="@info">xKubuni™</a><br/>