﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace SurveyProject.Helper
{
    public class ReplaceHtml
    {
        #region ReplaceText
        public static string ReplaceText(string html = "")
        {
            return Regex.Replace(html, @"<[^>]+>|&nbsp;", "").Trim();
        }
        #endregion
    }
}