﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SRV.BaseLayer.Respondent;
using SRV.ActionLayer.Respondent;
using System.Data;
using System.Xml;
using System.Web.Mvc;
using SRV.Utility;
using System.Text.RegularExpressions;
using SurveyProject.Models;
using SRV.BaseLayer.Admin;
using System.Data.OleDb;
using SRV.ActionLayer.Admin;
using System.Configuration;
using System.Reflection;

namespace SurveyProject.Helper
{
    public class LanguageHelper
    {
        #region ConvertLanguage
        public static string ConvertLanguage(int? Id = 0)
        {
            string json = string.Empty;
            SRV.BaseLayer.ActionResult actionResultLang = new SRV.BaseLayer.ActionResult();
            RespondentAction respondentAction = new RespondentAction();
            LanguageBase languageeBase = new LanguageBase();
            try
            {
                languageeBase.ProjectId = Id.GetValueOrDefault();
                actionResultLang = respondentAction.Language_LoadByProjectId(languageeBase);
                if (actionResultLang.IsSuccess)
                {
                    DataRow dr = actionResultLang.dtResult.Rows[0];
                    string languageAbb = dr["Abbreviation"].ToString();
                    XmlDocument xmlFile = new XmlDocument();
                    xmlFile.Load(HttpContext.Current.Server.MapPath("~/Content/LanguageXml/" + languageAbb.ToLower() + ".xml"));
                    XmlElement root = xmlFile.DocumentElement;
                    XmlNodeList nodes = root.SelectNodes("data");
                    foreach (XmlNode node in nodes)
                    {
                        json = json + "{\"Name\":\"" + node.Attributes["name"].Value + "\",\"Text\":\"" + node.InnerText + "\"},";
                    }
                    json = "[" + json.TrimEnd(',') + "]";
                }
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }
            return json;
        }
        #endregion

        #region ApplyLanguage
        public static string ApplyLanguage(string lang = "en")
        {
            string json = string.Empty;
            try
            {
                if (String.IsNullOrEmpty(lang) || lang == "null") lang = "en";
                XmlDocument xmlFile = new XmlDocument();
                xmlFile.Load(HttpContext.Current.Server.MapPath("~/Content/LanguageXml/" + lang.ToLower() + ".xml"));
                XmlElement root = xmlFile.DocumentElement;
                XmlNodeList nodes = root.SelectNodes("data");
                foreach (XmlNode node in nodes)
                {
                    json = json + "{\"Name\":\"" + node.Attributes["name"].Value.Trim() + "\",\"Text\":\"" + node.InnerText.Trim() + "\"},";
                }
                json = "[" + json.TrimEnd(',') + "]";
                RegexOptions options = RegexOptions.None;
                Regex regex = new Regex(@"[ ]{2,}", options);
                json = regex.Replace(json, @" ");
                json = json.Replace("\r", "");
                json = json.Replace("\n", "");
            }
            catch (Exception ex)
            {
                ErrorReporting.WebApplicationError(ex);
            }            
            return json;
        }
        #endregion
    }

    //public class ReplaceHtml
    //{
    //    #region ReplaceText
    //    public static string ReplaceText(string html = "")
    //    {
    //        return Regex.Replace(html, @"<[^>]+>|&nbsp;", "").Trim();
    //    }
    //    #endregion
    //}

    //public class CommonMethods
    //{
    //    #region Declaration

    //    AdminBase adminBase = new AdminBase();
    //    SRV.BaseLayer.ActionResult actionResult = new SRV.BaseLayer.ActionResult();
    //    SRV.ActionLayer.Admin.AdminAction adminAction = new SRV.ActionLayer.Admin.AdminAction();
    //    UserInfoBase userInfoBase = new UserInfoBase();

    //    QuestionBase questionBaseQ = new QuestionBase();

    //    SurveyLoadBase surveyLoadBase = new SurveyLoadBase();
    //    RespondentAction respondentAction = new RespondentAction();
    //    SurveyQuestionBase surveyQuestionBase = new SurveyQuestionBase();
    //    ProjectInvitationBase projectInvitationBase = new ProjectInvitationBase();
    //    QuestionBase questionBase = new QuestionBase();
    //    #endregion

    //    #region profileDetails Get
    //    public AdminRegisterModel profileDetails(int? Id = 0)
    //    {
    //        AdminRegisterModel model = new AdminRegisterModel();
    //        List<SelectListItem> countryLst = new List<SelectListItem>();
    //        List<SelectListItem> stateLst = new List<SelectListItem>();
    //        List<SelectListItem> languageLst = new List<SelectListItem>();
    //        try
    //        {
    //            if (Id > 0)
    //                userInfoBase.Id = Id.GetValueOrDefault();
    //            else
    //                userInfoBase.Id = System.Web.HttpContext.Current.Session["UserId"] != null ? Convert.ToInt32(System.Web.HttpContext.Current.Session["UserId"]) : 0;
    //            actionResult = adminAction.UserInfo_LoadById(userInfoBase);
    //            if (actionResult.IsSuccess)
    //            {
    //                DataRow dr = actionResult.dtResult.Rows[0];
    //                model.Id = dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0;
    //                model.FirstName = dr["FirstName"] != DBNull.Value ? dr["FirstName"].ToString() : "";
    //                model.LastName = dr["LastName"] != DBNull.Value ? dr["LastName"].ToString() : "";
    //                model.Gender = dr["Gender"] != DBNull.Value ? Convert.ToBoolean(dr["Gender"]) : false;
    //                model.Email = dr["Email"] != DBNull.Value ? dr["Email"].ToString() : "";
    //                model.SecurityQuestion = dr["SecurityQuestion"] != DBNull.Value ? dr["SecurityQuestion"].ToString() : "";
    //                model.AnswerToSecurityQuestion = dr["AnswerToSecurityQuestion"] != DBNull.Value ? dr["AnswerToSecurityQuestion"].ToString() : "";
    //                model.CountryOfResidenceId = dr["CountryOfResidenceId"] != DBNull.Value ? Convert.ToInt32(dr["CountryOfResidenceId"]) : 0;
    //                model.LanguageId = dr["LanguageId"] != DBNull.Value ? Convert.ToInt32(dr["LanguageId"]) : 0;
    //                model.CompanyName = dr["CompanyName"] != DBNull.Value ? dr["CompanyName"].ToString() : "";
    //                model.Address = dr["Address"] != DBNull.Value ? dr["Address"].ToString() : "";
    //                model.CountryId = dr["CountryId"] != DBNull.Value ? dr["CountryId"].ToString() : "";
    //                model.StateId = dr["StateId"] != DBNull.Value ? dr["StateId"].ToString() : "";
    //                model.City = dr["City"] != DBNull.Value ? dr["City"].ToString() : "";
    //                model.PhoneNumber = dr["PhoneNumber"] != DBNull.Value ? dr["PhoneNumber"].ToString() : "";
    //                model.sendEmail = dr["sendEmail"] != DBNull.Value ? Convert.ToBoolean(dr["sendEmail"]) : false;
    //                model.OtherCommunications = dr["OtherCommunications"] != DBNull.Value ? Convert.ToBoolean(dr["OtherCommunications"]) : false;
    //                model.Password = dr["Password"] != DBNull.Value ? dr["Password"].ToString() : "";
    //                model.UserGuid = dr["UserGuid"] != DBNull.Value ? dr["UserGuid"].ToString() : "";
    //            }


    //            SRV.BaseLayer.ActionResult CountryAction = new SRV.BaseLayer.ActionResult();
    //            CountryAction = adminAction.Country_LoadAll();
    //            if (CountryAction.dtResult != null && CountryAction.dtResult.Rows.Count > 0)
    //            {
    //                foreach (DataRow dr in CountryAction.dtResult.Rows)
    //                {
    //                    countryLst.Add(new SelectListItem { Text = dr["CountryName"].ToString(), Value = dr["ID"].ToString() });
    //                }
    //            }
    //            model.CountryList = countryLst;
    //            model.StateList = stateLst;
    //            SRV.BaseLayer.ActionResult LanguageAction = new SRV.BaseLayer.ActionResult();
    //            LanguageAction = adminAction.Language_LoadAll();
    //            if (LanguageAction.IsSuccess)
    //            {
    //                foreach (DataRow dr in LanguageAction.dtResult.Rows)
    //                {
    //                    languageLst.Add(new SelectListItem { Text = dr["Name"].ToString() + " (" + dr["Abbreviation"].ToString() + ")", Value = dr["Id"].ToString() });
    //                }
    //            }
    //            model.LanguageList = languageLst;
    //        }
    //        catch (Exception ex)
    //        {
    //            ErrorReporting.WebApplicationError(ex);
    //        }
    //        return model;
    //    }
    //    #endregion

    //    #region GetSurvey
    //    public SurveyResponseModel GetSurvey(string surveyId, string uid = "", string caller = "")
    //    {
    //        SurveyResponseModel model = new SurveyResponseModel();
    //        CreateProjectModel projectModel = new CreateProjectModel();
    //        ProjectSettingsModel projectSettingsModel = new ProjectSettingsModel();
    //        Responder responder = new Responder();
    //        List<QuestionModel> lstQuestionModel = new List<QuestionModel>();
    //        List<ProjectQuotaOther> lstProjectQuotaOtherModel = new List<ProjectQuotaOther>();
    //        List<MatrixTypeModel> lstMatxModel = new List<MatrixTypeModel>();
    //        List<MatrixTypeGroupModel> lstMatxTypeGroupModel = new List<MatrixTypeGroupModel>();
    //        CreateQuestionModel questionMedel = new CreateQuestionModel();
    //        string url = ConfigurationManager.AppSettings["site"].ToString();
    //        questionMedel.lstMatrixModel = lstMatxModel;

    //        int responseCount = 0;
    //        int projectQuota = 0;
    //        model.projectModel = projectModel;
    //        model.projectSettingsModel = projectSettingsModel;
    //        model.responder = responder;
    //        model.lstQuestionModel = lstQuestionModel;
    //        model.lstProjectQuotaOtherModel = lstProjectQuotaOtherModel;

    //        try
    //        {
    //            surveyLoadBase.GUID = surveyId;
    //            surveyLoadBase.InvitationId = Convert.ToString(uid);
    //            SRV.BaseLayer.ActionResult actionResultInvitation = new SRV.BaseLayer.ActionResult();
    //            actionResultInvitation = respondentAction.ProjectInvitation_LoadByGUID(surveyLoadBase);
    //            if (actionResultInvitation.IsSuccess)
    //            {
    //                DataRow drInv = actionResultInvitation.dtResult.Rows[0];
    //                if (drInv["IsResponded"] != DBNull.Value)
    //                {
    //                    if (Convert.ToBoolean(drInv["IsResponded"]) == true)
    //                    {
    //                        model.isError = true;
    //                        model.ErrorMessage = "You already have responded to this survey.";
    //                        //return model;
    //                    }
    //                }
    //                if (drInv["Email"] != DBNull.Value && !String.IsNullOrEmpty(drInv["Email"].ToString()))
    //                {
    //                    //ViewBag.responderEmail = drInv["Email"] != DBNull.Value ? Convert.ToString(drInv["Email"]) : "";
    //                    model.responder.ResponderEmail = drInv["Email"] != DBNull.Value ? Convert.ToString(drInv["Email"]) : "";
    //                    responseCount = drInv["TotalResponses"] != DBNull.Value ? Convert.ToInt32(drInv["TotalResponses"]) : 0;

    //                    // Check if ProjectQuota is not set then allow to take unlimited no. of responses.
    //                    if (drInv["ProjectQuota"] != DBNull.Value)
    //                    {
    //                        projectQuota = Convert.ToInt32(drInv["ProjectQuota"]);
    //                        if (responseCount >= projectQuota)      // If ProjectQuota is set then check for responses count
    //                        {
    //                            model.isError = true;
    //                            model.ErrorMessage = "We already have collected all the responses for this survey.";
    //                            //return model;
    //                        }
    //                    }
    //                }
    //                else
    //                {
    //                    model.isError = true;
    //                    model.ErrorMessage = "You are not the requested user.";
    //                    //return model;
    //                }
    //            }
    //            else
    //            {
    //                model.isError = true;
    //                model.ErrorMessage = "You are not the requested user.";
    //                //return model;
    //            }
    //            actionResult = new SRV.BaseLayer.ActionResult();
    //            actionResult = respondentAction.Survey_LoadByGUID(surveyLoadBase);
    //            if (actionResult.IsSuccess)
    //            {
    //                DataRow dr = actionResult.dtResult.Rows[0];

    //                if (dr["OpeningDate"] != DBNull.Value && !String.IsNullOrEmpty(Convert.ToString(dr["OpeningDate"])))
    //                {
    //                    if (Convert.ToDateTime(dr["OpeningDate"]) > DateTime.Now.Date)
    //                    {
    //                        model.isError = true;
    //                        model.ErrorMessage = "The survey is not started yet.";
    //                        //return model;
    //                    }
    //                }
    //                if (dr["ClosingDate"] != DBNull.Value && !String.IsNullOrEmpty(Convert.ToString(dr["ClosingDate"])))
    //                {
    //                    if (Convert.ToDateTime(dr["ClosingDate"]) <= DateTime.Now.Date)
    //                    {
    //                        model.isError = true;
    //                        model.ErrorMessage = "The survey has been closed.";
    //                        //return model;
    //                    }
    //                }
    //                if (!Convert.ToBoolean(dr["IsActive"]))
    //                {
    //                    model.isError = true;
    //                    model.ErrorMessage = "The survey is in-active.";
    //                    //return model;
    //                }

    //                // Assign project related values
    //                projectModel.Id = dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0;
    //                projectModel.ProjectName = dr["ProjectName"] != DBNull.Value ? Convert.ToString(dr["ProjectName"]) : "";
    //                projectModel.WelcomeMessage = (dr["WelcomeMessage"] != DBNull.Value && !String.IsNullOrEmpty(dr["WelcomeMessage"].ToString())) ? Convert.ToString(dr["WelcomeMessage"]) : Email.ReadFile("Welcome.txt");
    //                projectModel.PageHeader = (dr["PageHeader"] != DBNull.Value && !String.IsNullOrEmpty(dr["PageHeader"].ToString())) ? Convert.ToString(dr["PageHeader"]) : Email.ReadFile("Header.txt");
    //                projectModel.PageFooter = (dr["PageFooter"] != DBNull.Value && !String.IsNullOrEmpty(dr["PageFooter"].ToString())) ? Convert.ToString(dr["PageFooter"]) : Email.ReadFile("Footer.txt");
    //                projectModel.ProjectCompletionMesssage = (dr["ProjectCompletionMesssage"] != DBNull.Value && !String.IsNullOrEmpty(dr["ProjectCompletionMesssage"].ToString())) ? Convert.ToString(dr["ProjectCompletionMesssage"]) : Email.ReadFile("Complete.txt");
    //                projectModel.TerminationMessageUpper = Email.ReadFile("TerminateUpper.txt");
    //                projectModel.TerminationMessageLower = Email.ReadFile("TerminateLower.txt");
    //                projectModel.RedirectUrl = dr["RedirectUrl"] != DBNull.Value && !String.IsNullOrEmpty(dr["RedirectUrl"].ToString()) ? Convert.ToString(dr["RedirectUrl"]) : url;
    //                if (!String.IsNullOrEmpty(uid) && projectModel.RedirectUrl.Contains("UID"))
    //                {
    //                    string redirectGuid = Convert.ToString(Guid.NewGuid());
    //                    projectModel.RedirectUrl = projectModel.RedirectUrl.Replace("UID", redirectGuid);
    //                    string TotalRedirectUrl = projectModel.RedirectUrl;
    //                    int endPos = TotalRedirectUrl.IndexOf("&&");
    //                    int strPos = TotalRedirectUrl.IndexOf("surveyId=");
    //                    int end = endPos - (strPos + 9);
    //                    string SurveyGuid = TotalRedirectUrl.Substring(strPos + 9, end);
    //                    surveyLoadBase.GUID = SurveyGuid;
    //                    actionResult = new SRV.BaseLayer.ActionResult();
    //                    actionResult = respondentAction.Survey_LoadByGUID(surveyLoadBase);
    //                    if (actionResult.IsSuccess)
    //                    {
    //                        DataRow drSur = actionResult.dtResult.Rows[0];
    //                        projectInvitationBase.ProjectId = drSur["Id"] != DBNull.Value ? Convert.ToInt32(drSur["Id"]) : 0;
    //                        projectInvitationBase.Email = model.responder.ResponderEmail;
    //                        projectInvitationBase.InvitationGuid = redirectGuid;
    //                        actionResult = respondentAction.ProjectInvitation_InsertRedirectUrl(projectInvitationBase);
    //                    }

    //                }
    //                model.projectModel = projectModel;

    //                // Assign project settings related values
    //                projectSettingsModel.OpeningDate = dr["OpeningDate"] != DBNull.Value ? Convert.ToDateTime(dr["OpeningDate"]).ToString("MM/dd/yyyy") : "";
    //                projectSettingsModel.ClosingDate = dr["ClosingDate"] != DBNull.Value ? Convert.ToDateTime(dr["ClosingDate"]).ToString("MM/dd/yyyy") : "";
    //                projectSettingsModel.DisableQuestionNumbering = dr["DisableQuestionNumbering"] != DBNull.Value ? Convert.ToBoolean(dr["DisableQuestionNumbering"]) : false;
    //                projectSettingsModel.IsActive = dr["IsActive"] != DBNull.Value ? Convert.ToBoolean(dr["IsActive"]) : false;
    //                projectSettingsModel.Scored = dr["Scored"] != DBNull.Value ? Convert.ToBoolean(dr["Scored"]) : false;
    //                projectSettingsModel.PrevNextPageNavigation = dr["PrevNextPageNavigation"] != DBNull.Value ? Convert.ToBoolean(dr["PrevNextPageNavigation"]) : false;
    //                projectSettingsModel.TestMode = dr["TestMode"] != DBNull.Value ? Convert.ToBoolean(dr["TestMode"]) : false;
    //                projectSettingsModel.ResumeOfProgress = dr["ResumeOfProgress"] != DBNull.Value ? Convert.ToBoolean(dr["ResumeOfProgress"]) : false;
    //                projectSettingsModel.IsQuesRandomize = dr["IsQuesRandomize"] != DBNull.Value ? Convert.ToBoolean(dr["IsQuesRandomize"]) : false;
    //                model.projectSettingsModel = projectSettingsModel;

    //                surveyLoadBase.Id = projectModel.Id;

    //                SRV.BaseLayer.ActionResult actionResultQuota = new SRV.BaseLayer.ActionResult();
    //                //Load All Quota Other of this project
    //                actionResultQuota = respondentAction.ProjQuotaOther_LoadByProjectId(surveyLoadBase);
    //                if (actionResultQuota.IsSuccess)
    //                {
    //                    foreach (DataRow drQuota in actionResultQuota.dtResult.Rows)
    //                    {
    //                        lstProjectQuotaOtherModel.Add(new ProjectQuotaOther
    //                        {
    //                            Id = Convert.ToInt32(drQuota["Id"]),
    //                            QuotaType = drQuota["QuotaType"] != DBNull.Value ? drQuota["QuotaType"].ToString() : "",
    //                            QuotaPerc = drQuota["QuotaPer"] != DBNull.Value ? Convert.ToDecimal(drQuota["QuotaPer"]) : 0,
    //                            MaxCount = drQuota["MaxCount"] != DBNull.Value ? Convert.ToInt32(drQuota["MaxCount"]) : 0
    //                        });
    //                    }
    //                    model.lstProjectQuotaOtherModel = lstProjectQuotaOtherModel;
    //                }


    //                SRV.BaseLayer.ActionResult actionResultQuestions = new SRV.BaseLayer.ActionResult();

    //                // Load All Questions of this project

    //                // Check if the question is to be loaded in Random Order
    //                if (!String.IsNullOrEmpty(caller) && caller.ToLower() == "resp")
    //                    surveyLoadBase.IsQuesRandomize = projectSettingsModel.IsQuesRandomize;

    //                actionResultQuestions = respondentAction.ProjQues_LoadByProjectId(surveyLoadBase);
    //                if (actionResultQuestions.IsSuccess)
    //                {
    //                    foreach (DataRow drQuestion in actionResultQuestions.dtResult.Rows)
    //                    {
    //                        QuestionModel question = new QuestionModel();
    //                        question.QuestionTypeId = drQuestion["QuestionTypeId"] != DBNull.Value ? Convert.ToInt32(drQuestion["QuestionTypeId"]) : 0;
    //                        question.QuestionId = drQuestion["QuestionId"] != DBNull.Value ? Convert.ToInt32(drQuestion["QuestionId"]) : 0;
    //                        question.QuestionTitle = drQuestion["QuestionTitle"] != DBNull.Value ? Convert.ToString(drQuestion["QuestionTitle"]) : "";
    //                        question.ConditionalExpression = drQuestion["ConditionalExpression"] != DBNull.Value ? Convert.ToString(drQuestion["ConditionalExpression"]) : "";
    //                        question.RangeMin = drQuestion["RangeMin"] != DBNull.Value ? Convert.ToInt32(drQuestion["RangeMin"]) : 0;
    //                        question.RangeMax = drQuestion["RangeMax"] != DBNull.Value ? Convert.ToInt32(drQuestion["RangeMax"]) : 0;
    //                        question.ConstantSum = drQuestion["ConstantSum"] != DBNull.Value ? Convert.ToInt32(drQuestion["ConstantSum"]) : 0;
    //                        question.RedirectUrl = drQuestion["RedirectUrl"] != DBNull.Value ? Convert.ToString(drQuestion["RedirectUrl"]) : "";
    //                        question.OptionsCaption = drQuestion["OptionsCaption"] != DBNull.Value ? Convert.ToString(drQuestion["OptionsCaption"]) : "";
    //                        question.isAnswersPiped = drQuestion["isAnswersPiped"] != DBNull.Value ? Convert.ToBoolean(drQuestion["isAnswersPiped"]) : false;
    //                        question.isAnswersExplicating = drQuestion["isAnswersExplicating"] != DBNull.Value ? Convert.ToBoolean(drQuestion["isAnswersExplicating"]) : false;
    //                        question.PipedFrom = drQuestion["PipedFrom"] != DBNull.Value ? Convert.ToInt32(drQuestion["PipedFrom"]) : 0;
    //                        question.ExplicatingFrom = drQuestion["ExplicatingFrom"] != DBNull.Value ? Convert.ToInt32(drQuestion["ExplicatingFrom"]) : 0;
    //                        question.IsOptRandomize = drQuestion["IsOptRandomize"] != DBNull.Value ? Convert.ToBoolean(drQuestion["IsOptRandomize"]) : false;
    //                        // Load All of the Column Names
    //                        if (question.QuestionTypeId > 7)
    //                        {
    //                            questionBaseQ.Id = question.QuestionId;
    //                            SRV.BaseLayer.ActionResult actionResultColumns = new SRV.BaseLayer.ActionResult();
    //                            actionResultColumns = adminAction.Question_LoadById(questionBaseQ);
    //                            if (actionResult.IsSuccess)
    //                            {
    //                                foreach (DataRow drScnd in actionResultColumns.dsResult.Tables[1].Rows)
    //                                {
    //                                    lstMatxModel.Add(new MatrixTypeModel
    //                                    {
    //                                        Id = drScnd["Id"] != DBNull.Value ? Convert.ToInt32(drScnd["Id"]) : 0,
    //                                        QuestionId = drScnd["QuestionId"] != DBNull.Value ? Convert.ToInt32(drScnd["QuestionId"]) : 0,
    //                                        ColumnName = drScnd["ColumnName"] != DBNull.Value ? Convert.ToString(drScnd["ColumnName"]) : "",
    //                                        GroupId = drScnd["GroupId"] != DBNull.Value ? Convert.ToInt32(drScnd["GroupId"]) : 0
    //                                    });
    //                                    if (!lstMatxTypeGroupModel.Any(v => v.Id == Convert.ToInt32(drScnd["GroupId"])) && Convert.ToInt32(drScnd["GroupId"]) != 0)
    //                                    {
    //                                        lstMatxTypeGroupModel.Add(new MatrixTypeGroupModel
    //                                        {
    //                                            Id = drScnd["GroupId"] != DBNull.Value ? Convert.ToInt32(drScnd["GroupId"]) : 0,
    //                                            GroupCaptionDisplayOrder = drScnd["GroupCaptionDisplayOrder"] != DBNull.Value ? Convert.ToInt32(drScnd["GroupCaptionDisplayOrder"]) : 0,
    //                                            GroupCaption = drScnd["GroupCaption"] != DBNull.Value ? Convert.ToString(drScnd["GroupCaption"]) : ""
    //                                        });
    //                                    }
    //                                }
    //                                question.lstMatrixTypeModel = lstMatxModel;
    //                                question.lstMatrixTypeGroup = lstMatxTypeGroupModel;
    //                            }
    //                        }


    //                        // Load all Options of this question
    //                        SRV.BaseLayer.ActionResult actionResultOptions = new SRV.BaseLayer.ActionResult();
    //                        // Load All Questions of this project

    //                        // Check if the options is to be loaded in Random Order
    //                        if (!String.IsNullOrEmpty(caller) && caller.ToLower() == "resp")
    //                            surveyQuestionBase.IsOptRandomize = question.IsOptRandomize;
    //                        surveyQuestionBase.Id = question.QuestionId;
    //                        actionResultOptions = respondentAction.Options_LoadAllByQuesId(surveyQuestionBase);
    //                        List<OptionsModel> lstOptions = new List<OptionsModel>();
    //                        if (actionResultOptions.IsSuccess)
    //                        {
    //                            foreach (DataRow drOption in actionResultOptions.dtResult.Rows)
    //                            {
    //                                lstOptions.Add(new OptionsModel
    //                                {
    //                                    OptionId = drOption["Id"] != DBNull.Value ? Convert.ToInt32(drOption["Id"]) : 0,
    //                                    QuestionId = drOption["QuestionId"] != DBNull.Value ? Convert.ToInt32(drOption["QuestionId"]) : 0,
    //                                    Option = drOption["OptionTitle"] != DBNull.Value ? Convert.ToString(drOption["OptionTitle"]) : "",
    //                                    IsSkip = drOption["IsSkip"] != DBNull.Value ? Convert.ToBoolean(drOption["IsSkip"]) : false,
    //                                    SkipQuestionId = drOption["SkipQuestionId"] != DBNull.Value ? Convert.ToInt32(drOption["SkipQuestionId"]) : 0,
    //                                    MaxResponsesCount = drOption["MaxResponsesCount"] != DBNull.Value ? drOption["MaxResponsesCount"].ToString() : "unlimited"
    //                                });
    //                            }
    //                            question.lstOptionsModel = lstOptions;

    //                            //Load Drop Down atrix type Options
    //                            SRV.BaseLayer.ActionResult actionResultDropDownOption = new SRV.BaseLayer.ActionResult();
    //                            surveyQuestionBase.Id = question.QuestionId;
    //                            actionResultDropDownOption = respondentAction.DDLMatrixTypeOptions_LoadByQuestionId(surveyQuestionBase);
    //                            List<DDLMatrixTypeModel> lstDDLModel = new List<DDLMatrixTypeModel>();
    //                            if (actionResultDropDownOption.IsSuccess)
    //                            {
    //                                foreach (DataRow drDropDown in actionResultDropDownOption.dtResult.Rows)
    //                                {
    //                                    lstDDLModel.Add(new DDLMatrixTypeModel
    //                                    {
    //                                        Id = drDropDown["Id"] != DBNull.Value ? Convert.ToInt32(drDropDown["Id"]) : 0,
    //                                        QuestionId = drDropDown["QuestionId"] != DBNull.Value ? Convert.ToInt32(drDropDown["QuestionId"]) : 0,
    //                                        OptionName = drDropDown["OptionName"] != DBNull.Value ? Convert.ToString(drDropDown["OptionName"]) : ""
    //                                    });
    //                                }
    //                            }
    //                            question.lstDDlMatrixModel = lstDDLModel;
    //                        }
    //                        lstQuestionModel.Add(question);
    //                        lstMatxModel = new List<MatrixTypeModel>();

    //                        model.lstQuestionModel = lstQuestionModel;
    //                    }
    //                    //model.lstMatrixTypeModel = lstMatxModel; 
    //                }
    //            }
    //            else
    //            {
    //                model.isError = true;
    //                model.ErrorMessage = "Invalid link";
    //                return model;
    //            }
    //        }
    //        catch (Exception ex)
    //        {
    //            ErrorReporting.WebApplicationError(ex);
    //        }
    //        return model;
    //    }

    //    #endregion

    //    #region AllRoles
    //    public List<CreateRolesModel> AllRoles()
    //    {
    //        int UserId = HttpContext.Current.Session["UserId"] != null ? Convert.ToInt32(HttpContext.Current.Session["UserId"]) : 0;

    //        List<CreateRolesModel> rolesList = new List<CreateRolesModel>();
    //        try
    //        {
    //            adminBase.Id = UserId;
    //            actionResult = adminAction.ActionNames_byUserId(adminBase);
    //            if (actionResult.IsSuccess)
    //            {
    //                foreach (DataRow dr in actionResult.dtResult.Rows)
    //                {
    //                    rolesList.Add(new CreateRolesModel
    //                    {
    //                        RoleId = dr["RoleId"] != DBNull.Value ? Convert.ToInt32(dr["RoleId"]) : 0,
    //                        RoleName = dr["RoleName"] != DBNull.Value ? dr["RoleName"].ToString() : "",
    //                        ActionName = dr["ActionName"] != DBNull.Value ? dr["ActionName"].ToString() : ""
    //                    });
    //                }
    //            }
    //        }
    //        catch (Exception ex)
    //        {
    //            ErrorReporting.WebApplicationError(ex);
    //        }
    //        return rolesList;
    //    }
    //    #endregion

    //    #region ImportExcelXLS
    //    public static DataSet ImportExcelXLS(string FileName, bool hasHeaders)
    //    {
    //        string HDR = hasHeaders ? "Yes" : "No";
    //        string filePath = HttpContext.Current.Server.MapPath("~/Content/Uploads/");
    //        DataSet output = new DataSet();
    //        string strConn;
    //        try
    //        {
    //            if (FileName.Substring(FileName.LastIndexOf('.')).ToLower() == ".xlsx")
    //                strConn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + filePath + FileName + ";Extended Properties=\"Excel 12.0;HDR=" + HDR + ";IMEX=0\"";
    //            else
    //                strConn = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + filePath + FileName + ";Extended Properties=\"Excel 8.0;HDR=" + HDR + ";IMEX=0\"";

    //            using (OleDbConnection conn = new OleDbConnection(strConn))
    //            {
    //                conn.Open();

    //                DataTable schemaTable = conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" });

    //                foreach (DataRow schemaRow in schemaTable.Rows)
    //                {
    //                    string sheet = schemaRow["TABLE_NAME"].ToString();
    //                    if (!sheet.EndsWith("_"))
    //                    {
    //                        try
    //                        {
    //                            OleDbCommand cmd = new OleDbCommand("SELECT * FROM [" + sheet + "]", conn);
    //                            cmd.CommandType = CommandType.Text;

    //                            DataTable outputTable = new DataTable(sheet);
    //                            output.Tables.Add(outputTable);
    //                            new OleDbDataAdapter(cmd).Fill(outputTable);
    //                        }
    //                        catch (Exception ex)
    //                        {
    //                            throw new Exception(ex.Message + string.Format("Sheet:{0}.File:F{1}", sheet, FileName), ex);
    //                        }
    //                    }
    //                }
    //                if (conn.State == ConnectionState.Open) // Close the connection
    //                    conn.Close();
    //            }
    //        }
    //        catch (Exception ex)
    //        {
    //            ErrorReporting.WebApplicationError(ex);
    //        }
    //        return output;
    //    }
    //    #endregion

    //    #region saveTodataBase
    //    public bool saveTodataBase(DataTable dt)
    //    {
    //        System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection();
    //        System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand();
    //        con.ConnectionString = System.Configuration.ConfigurationManager.AppSettings["ConnectionString"].ToString();
    //        bool status = true;
    //        try
    //        {
    //            if (con.State == ConnectionState.Closed)
    //            {
    //                con.Open();
    //            }
    //            using (System.Data.SqlClient.SqlBulkCopy bulkCopy = new System.Data.SqlClient.SqlBulkCopy(con))
    //            {
    //                bulkCopy.DestinationTableName = "Responses_Offline";
    //                bulkCopy.ColumnMappings.Add(new System.Data.SqlClient.SqlBulkCopyColumnMapping("ProjectId", "ProjectId"));
    //                bulkCopy.ColumnMappings.Add(new System.Data.SqlClient.SqlBulkCopyColumnMapping("QuestionId", "QuestionId"));
    //                bulkCopy.ColumnMappings.Add(new System.Data.SqlClient.SqlBulkCopyColumnMapping("AnsExpression", "AnswerExpression"));
    //                bulkCopy.ColumnMappings.Add(new System.Data.SqlClient.SqlBulkCopyColumnMapping("RespondentIP", "RespondentIP"));
    //                bulkCopy.ColumnMappings.Add(new System.Data.SqlClient.SqlBulkCopyColumnMapping("RespId", "RespId"));
    //                bulkCopy.ColumnMappings.Add(new System.Data.SqlClient.SqlBulkCopyColumnMapping("CreatedOn", "CreatedOn"));
    //                bulkCopy.ColumnMappings.Add(new System.Data.SqlClient.SqlBulkCopyColumnMapping("Status", "Status"));
    //                // Write from the source to the destination.
    //                bulkCopy.WriteToServer(dt);
    //            }
    //        }
    //        catch (Exception ex)
    //        {
    //            status = false;
    //            ErrorReporting.WebApplicationError(ex);
    //        }
    //        finally
    //        {
    //            if (con.State == ConnectionState.Open)
    //                con.Close();
    //        }
    //        return status;
    //    }
    //    #endregion

    //    #region GenerateTransposedTable
    //    public DataTable GenerateTransposedTable(DataTable inputTable)
    //    {
    //        DataTable outputTable = new DataTable();

    //        // Add columns by looping rows

    //        // Header row's first column is same as in inputTable
    //        outputTable.Columns.Add(inputTable.Columns[0].ColumnName.ToString());

    //        // Header row's second column onwards, 'inputTable's first column taken
    //        foreach (DataRow inRow in inputTable.Rows)
    //        {
    //            string newColName = inRow[0].ToString();
    //            outputTable.Columns.Add(newColName);
    //        }

    //        // Add rows by looping columns        
    //        for (int rCount = 1; rCount <= inputTable.Columns.Count - 1; rCount++)
    //        {
    //            DataRow newRow = outputTable.NewRow();

    //            // First column is inputTable's Header row's second column
    //            newRow[0] = inputTable.Columns[rCount].ColumnName.ToString();
    //            for (int cCount = 0; cCount <= inputTable.Rows.Count - 1; cCount++)
    //            {
    //                string colValue = inputTable.Rows[cCount][rCount].ToString();
    //                newRow[cCount + 1] = colValue;
    //            }
    //            outputTable.Rows.Add(newRow);
    //        }

    //        return outputTable;
    //    }
    //    #endregion

    //    #region GetQuestionListByQuestionType

    //    public CreateQuestionModel GetQuestionListByQuestionType(int? projectId = 0, int? questionTypeId = 0)
    //    {
    //        CreateQuestionModel model = new CreateQuestionModel();
    //        List<CreateQuestionModel> QuestionLst = new List<CreateQuestionModel>();
    //        model.QuestionList = QuestionLst;
    //        try
    //        {
    //            questionBase.ProjectId = Convert.ToInt32(projectId);
    //            SRV.BaseLayer.ActionResult actionResultQuestionList = new SRV.BaseLayer.ActionResult();
    //            actionResultQuestionList = adminAction.Qusetion_LoadAllByProjectId(questionBase);
    //            if (actionResultQuestionList.dtResult != null && actionResultQuestionList.dtResult.Rows.Count > 0)
    //            {
    //                //DataTable filterDT = actionResult.dtResult.AsEnumerable().Where(v => v.Field<Int64>("questionId") != questionId.GetValueOrDefault() && v.Field<Int64>("QuestionTypeId") == questionTypeId.GetValueOrDefault()).ToList().CopyToDataTable();
    //                DataTable filterDT = new DataTable();
    //                var resultList = actionResultQuestionList.dtResult.AsEnumerable().Where(v => v.Field<Int64>("QuestionTypeId") == questionTypeId.GetValueOrDefault()).ToList();
    //                if (resultList != null && resultList.Count > 0)
    //                    filterDT = resultList.CopyToDataTable();
    //                if (filterDT != null && filterDT.Rows.Count > 0)
    //                {
    //                    foreach (DataRow dr in filterDT.Rows)
    //                    {
    //                        model = new CreateQuestionModel();
    //                        model.ProjectId = dr["ProjectId"] != DBNull.Value ? Convert.ToInt32(dr["ProjectId"]) : 0;
    //                        model.QuestionId = dr["questionId"] != DBNull.Value ? Convert.ToInt32(dr["questionId"]) : 0;
    //                        model.QuestionType = dr["QuestionType"] != DBNull.Value ? dr["QuestionType"].ToString() : "";
    //                        model.QuestionTypeId = dr["QuestionTypeId"] != DBNull.Value ? Convert.ToInt32(dr["QuestionTypeId"]) : 0;
    //                        model.QuestionTitle = dr["QuestionTitle"] != DBNull.Value ? ReplaceHtml.ReplaceText(Convert.ToString(dr["QuestionTitle"])) : "";
    //                        model.isAnswersExplicating = dr["isAnswersExplicating"] != DBNull.Value ? Convert.ToBoolean(dr["isAnswersExplicating"]) : false;
    //                        model.isAnswersPiped = dr["isAnswersPiped"] != DBNull.Value ? Convert.ToBoolean(dr["isAnswersPiped"]) : false;
    //                        QuestionLst.Add(model);
    //                    }
    //                }
    //                model.QuestionList = QuestionLst;
    //            }
    //        }
    //        catch (Exception ex)
    //        {
    //            ErrorReporting.WebApplicationError(ex);
    //        }
    //        return model;
    //    }

    //    #endregion
    //}
}