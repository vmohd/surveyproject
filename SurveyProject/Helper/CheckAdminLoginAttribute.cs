﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace SurveyProject.Helper
{
    public class CheckLoginAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (filterContext.ActionDescriptor.ControllerDescriptor.IsDefined(typeof(SkipLogin), true) ||
                filterContext.ActionDescriptor.IsDefined(typeof(SkipLogin), true))
            {
                base.OnActionExecuting(filterContext);
            }
            else
            {
                if (filterContext.HttpContext.Session["UserId"] == null)
                {
                    //string currentUrl = filterContext.HttpContext.Request.RawUrl;
                    
                    filterContext.HttpContext.Response.Redirect("~/Account/Login");//?returnUrl=" + currentUrl);
                }
            }
        }
    }

    public class SkipLogin : ActionFilterAttribute
    {

    }

    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class VerifyRoleAccessAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            CommonMethods methods = new CommonMethods();
            List<Models.CreateRolesModel> rolesList = methods.AllRoles();

            // Add Default roles to "rolesList" which are not being managed by super Admin so that it can navigate to those actions.
            string[] DefaultRoles = {
                                      "BindState", "QuestionList", "DeleteQuestion", "BindOption", "InsertOptions", "ManageAnswers", "EditProfile",
                                      "ChangePassword", "ProjectStartup", "DisplayProjectList", "ProjectQuota", "ManageQuestions", "SurveyResponses",
                                      "ViewSurvey", "UpdateSurvey","BindQuestionDdl","ProjectList","DisplayProjectList","QuestionType","GetProjectDetailById",
                                      "UpdateProjectDetails","CheckRoles","ProjectQuotaOther","GetProjectXML","LanguageConversion","QuestionTypeJson",
                                      "AnswersGet","ProjectReminderEmail","TestSurvey","ProjectSettingsPreview","ImportExcel","ExportSurveyToExcel",
                                      "SurveyOfflineResponses","ViewOfflineSurvey","ProjectSummary","DeleteResponse","ExportOfflineResponses",
                                      "ExportOnlineResponses","GetSurveyList","ManageOptions","CreateQuestionGroup","QuestionGroupList","SetAllQuestions",
                                      "DeleteQuestionGroup","UpdateQuestionGroups","QuestionsRandomization","PagesRandomization","UpdateGroupOrder",
                                      "UpdateQuestionsGroupOrder","GetQuestionListByQuestionType","LinkProduct","BindProductByProjectId","SaveLinkProduct",
                                      "LinkQuestionGoup","BindQuestionByProductId","SaveLinkQuestion","CreateSuperProject","_PartialViewSurvey","DeleteSuperProject"
                                      ,"ProductsRandomization","ManageProductPosition","UpdateSurveyOrder","SurveyRandomization","UpdateProductOrder","AddProductResponder",
                                      "AddResponderToPositionTable","SuperProjectSettings","GroupEmails","GroupEmailList","DeleteGroupEmails","SaveEmailGrp","EmailsForGroup",
                                      "GroupEmailJsonList","UpdateProjectDisplayOrder","UpdateProjectProductDisplayOrder","ProductMessageLayout","GetProductMessage",
                                      "DeleteAllOfflineResponses","DeleteAllOnlineResponses","ProjectTransition","CreateUserDirectory","DeleteDirectory",
                                      "ProjectDirectories_InsertUpdate","UserDirectoryList","ProjectDirectories_LoadAll","AddUrlParameter","GetUrlParameters",
                                      "DeleteUrlParameter","AssignSurveyToPanel","ExistingPanel","_PartialViewQueList","Reports","QueryReports","SurveyIncompleteResponses",
                                      "NotParticipateRespondent","RecoverDeletedProject","recoverDeletedPro","SurveyNameCheck","NotParticipateRespList",
                                      "deletedNoPeak","uploadPartial"
                                    };

            foreach (string role in DefaultRoles)
                rolesList.Add(new Models.CreateRolesModel { ActionName = role });

            string actionName = filterContext.ActionDescriptor.ActionName;
            //string controllerName = filterContext.ActionDescriptor.ControllerDescriptor.ControllerName;

            if (rolesList != null && rolesList.Count > 0 && actionName.ToLower() != "roleerror")
            {
                try
                {
                    if (!rolesList.Any(v => v.ActionName == actionName))
                        filterContext.HttpContext.Response.Redirect("~/Admin/RoleError");
                }
                catch(Exception){}
            }
        }
    }
}