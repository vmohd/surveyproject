﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace SurveyProject.Helper
{
    public class UnHandledExceptionFilterAttribute : FilterAttribute, IExceptionFilter
    {
        public void OnException(ExceptionContext filterContext)
        {
            if (!filterContext.ExceptionHandled)
            {
                string[] msg = new string[2];
                msg[0] = filterContext.Exception.Message;
                LogErrorToFile(filterContext.Exception);
                msg[1] = System.DateTime.Now.ToString("MM-dd-yyyy- hh:mm:ss tt");
                filterContext.Result = new ViewResult
                {
                    ViewName = "Exception",
                    ViewData = new ViewDataDictionary<string[]>(msg)
                };
                filterContext.ExceptionHandled = true;
            }
        }

        #region LogErrorToFile
        private static void LogErrorToFile(Exception appException)
        {
            try
            {
                string filePath = GetErrorLogFilePath();
                if (!System.IO.Directory.Exists(HttpContext.Current.Server.MapPath("~/Content/Uploads/ErrorLogFiles")))
                {
                    System.IO.Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/Content/Uploads/ErrorLogFiles"));
                }
                string strExceptionContent = GetExceptionContent(appException);
                LogException(filePath, strExceptionContent);
            }
            catch (Exception ex)
            {
                
            }
        }
        #endregion

        #region GetExceptionContent
        private static string GetExceptionContent(Exception appException)
        {
            //////////////// is it writing the exception object to file properly?
            string strExceptionContent = "\r\n\n\n\n\n\n\n ----------------------------------------------------------------------------- \r\n An error ocurred at " + DateTime.Now;
            strExceptionContent += "\r\n  Error Message: " + appException.Message + "\r\n  Stack Trace: " + appException.StackTrace;

            return strExceptionContent;
        }
        #endregion

        #region GetErrorLogFilePath
        private static string GetErrorLogFilePath()
        {
            string fileName = "CriticalError_" + System.DateTime.Now.Date.ToShortDateString();
            fileName = fileName.Replace("/", "-");
            string fileExtension = "txt";
            string filePath = HttpContext.Current.Server.MapPath("~/Content/Uploads/ErrorLogFiles") + "\\" + fileName + "." + fileExtension;

            return filePath;
        }
        #endregion

        #region LogException
        private static void LogException(string filePath, string content)
        {
            System.IO.FileStream fs;
            if (System.IO.File.Exists(filePath))
            {
                fs = new System.IO.FileStream(filePath, System.IO.FileMode.Append, System.IO.FileAccess.Write);
            }
            else
            {
                fs = new System.IO.FileStream(filePath, System.IO.FileMode.Create, System.IO.FileAccess.Write);
            }
            System.IO.StreamWriter sw = new System.IO.StreamWriter(fs);
            sw.Write(content);
            sw.Flush();
            sw.Close();
            fs.Close();
        }
        #endregion
        
    }
}