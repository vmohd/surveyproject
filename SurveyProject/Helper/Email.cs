﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Net.Mail;
using System.Configuration;

namespace SurveyProject.Helper
{
    public class Email
    {
        #region Properties
        private static string userName = ConfigurationManager.AppSettings["userName"].ToString();
        private static string password = ConfigurationManager.AppSettings["password"].ToString();
        private static string mailFrom = ConfigurationManager.AppSettings["mailFrom"].ToString();
        private static string mailTo = ConfigurationManager.AppSettings["mailTo"].ToString();
        private static string bccAddress = ConfigurationManager.AppSettings["bccAddress"].ToString();
        private static string smtpServer = ConfigurationManager.AppSettings["smtpServer"].ToString();
        private static string testMode = ConfigurationManager.AppSettings["testMode"].ToString();
        private static string site = ConfigurationManager.AppSettings["site"].ToString();
        private static string commaDelimCCs = "";
        #endregion

        static MailAddress frm = new MailAddress(mailFrom, "xKubuni™");

        #region Registration SendConfirmationEmail
        public static bool SendConfirmationEmail(string emailId, string token,string name)
        {
            try
            {

                MailAddress toa = new MailAddress(emailId);
                MailMessage message = new MailMessage(frm, toa);
                message.IsBodyHtml = true;
                message.Subject = "xKubuni™ User Registration";
                string text = "Click on following link to confirm your email address";
                string url = "/Account/ConfirmRegistration/"+token;
                string content = ReadFileWithName("AccountConfirmationEmailTemplate.html", name,text,url);
                message.Body = string.Format(content, string.Empty);
                // SmtpClient client = new SmtpClient();
                // client.Send(message);
                SetUserCredentialAndProcessMail(message);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        #endregion

        #region ProjectReminderEmail
        public static bool ProjectReminderEmail(string emailId, string name, string surveyName,string expiryDate,string CurrResponse,string maxResponse)
        {
            try
            {

                MailAddress toa = new MailAddress(emailId);
                MailMessage message = new MailMessage(frm, toa);
                message.IsBodyHtml = true;
                message.Subject = "xKubuni™ Closing Reminder";
                string content = ReadFileReminder("SurveyExpiry.html", name,surveyName,expiryDate,CurrResponse,maxResponse);
                message.Body = string.Format(content, string.Empty);               
                SetUserCredentialAndProcessMail(message);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        #endregion

        #region Registration SendConfirmationEmailWithResetPassword
        public static bool SendConfirmationEmailWithResetPassword(string emailId, string token, string name)
        {
            try
            {

                MailAddress toa = new MailAddress(emailId);
                MailMessage message = new MailMessage(frm, toa);
                message.IsBodyHtml = true;
                message.Subject = "xKubuni™ User Registration";
                string text = "Click on following link to reset your password";
                string url = "/Account/ResetPassword?id="+token;
                string content = ReadFileWithName("AccountConfirmationEmailTemplate.html", name, text, url);
                message.Body = string.Format(content, string.Empty);
                // SmtpClient client = new SmtpClient();
                // client.Send(message);
                SetUserCredentialAndProcessMail(message);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        #endregion

        public static bool SendForgotPasswordEmail(string emailid, string token)
        {
            try
            {

                MailAddress to = new MailAddress(emailid);
                MailMessage message = new MailMessage(frm, to);
                message.IsBodyHtml = true;
                message.Subject = "Forgot Password - xKubuni™";
                string content = ReadFile("ForgotPasswordTemplate.html");
                message.Body = string.Format(content, token);

                SetUserCredentialAndProcessMail(message);

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        #region Method SetUserCredentialAndProcessMail
        private static void SetUserCredentialAndProcessMail(MailMessage msg)
        {
            NetworkCredential cred = new NetworkCredential(userName, password);
            SmtpClient mailClient = testMode == "1" ? new SmtpClient(smtpServer, 587) : new SmtpClient(smtpServer, 26);
            mailClient.EnableSsl = testMode == "1" ? true : false;
            mailClient.UseDefaultCredentials = false;
            mailClient.Credentials = cred;
            try
            {
                if (testMode == "1")
                {
                    MailMessage mailMessage = new MailMessage(mailFrom, msg.To.ToString(), msg.Subject, msg.Body);
                    mailMessage.IsBodyHtml = msg.IsBodyHtml;
                    if (msg.Attachments != null && msg.Attachments.Count > 0)
                    {
                        AttachmentCollection attachment = msg.Attachments;
                        foreach (Attachment item in attachment)
                        {
                            mailMessage.Attachments.Add(item);
                        }
                    }
                    mailClient.Send(mailMessage);
                }
                else
                {
                    mailClient.Send(msg);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        public static string ReadFileWithName(string filename, string name,string text,string url)
        {
            try
            {
                string abspath = System.Web.Hosting.HostingEnvironment.MapPath("/Content/Template/");
                string filepath = System.IO.Path.Combine(abspath, filename);
                string content = string.Empty;
                using (System.IO.StreamReader rdr = new System.IO.StreamReader(filepath))
                {
                    content = rdr.ReadToEnd();
                    content = content.Replace("@info", site);
                    content = content.Replace("@userName", name);
                    content = content.Replace("@text", text);
                    content = content.Replace("@url", url);
                }
                return content;
            }
            catch (Exception ex)
            {
                return string.Empty;
            }
        }

        public static string ReadFileReminder(string filename, string name, string surveyName, string expiryDate, string CurrResponse, string maxResponse)
        {
            try
            {
                string abspath = System.Web.Hosting.HostingEnvironment.MapPath("/Content/Template/");
                string filepath = System.IO.Path.Combine(abspath, filename);
                string content = string.Empty;
                using (System.IO.StreamReader rdr = new System.IO.StreamReader(filepath))
                {
                    content = rdr.ReadToEnd();
                    content = content.Replace("@userName", name);
                    content = content.Replace("@surveyName", surveyName);
                    content = content.Replace("@expiryDate", expiryDate);
                    content = content.Replace("@currentResponse", CurrResponse);
                    content = content.Replace("@maxResponse", maxResponse);
                    content = content.Replace("@info", site);
                }
                return content;
            }
            catch (Exception ex)
            {
                return string.Empty;
            }
        }

        public static string ReadFile(string filename)
        {
            try
            {
                string abspath = System.Web.Hosting.HostingEnvironment.MapPath("/Content/Template/");
                string filepath = System.IO.Path.Combine(abspath, filename);
                string content = string.Empty;
                using (System.IO.StreamReader rdr = new System.IO.StreamReader(filepath))
                {
                    content = rdr.ReadToEnd();
                    content = content.Replace("@info", site);                    
                }
                return content;
            }
            catch (Exception ex)
            {
                return string.Empty;
            }
        }

        #region ProjectMailingEmail
        public static bool ProjectMailingEmail(string toEmail,string projectGuid,string InvitationId, string Subject, string messageText,int superSurveyId=0)
        {
            try
            {
                string wholeMessage = messageText;
                MailAddress toa = new MailAddress(toEmail);
                MailMessage msg = new MailMessage(frm, toa);
                msg.IsBodyHtml = true;
                msg.Subject = Subject;
                wholeMessage = wholeMessage.Replace("[-surveyguid-]", projectGuid);
                wholeMessage = wholeMessage.Replace("[-invitationid-]", InvitationId);
                wholeMessage = wholeMessage.Replace("[-superSurveyId-]", (superSurveyId>0?superSurveyId.ToString():""));
                //msg.Body = messageText.Replace("[-surveyguid-]", projectGuid);
                //msg.Body = messageText.Replace("[-invitationid-]", InvitationId);
                msg.Body = wholeMessage;
                SetUserCredentialAndProcessMail(msg, toEmail);

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        #endregion

        #region Method SetUserCredentialAndProcessMail
        private static void SetUserCredentialAndProcessMail(MailMessage msg, string mailTo)
        {
            NetworkCredential cred = new NetworkCredential(userName, password);
            SmtpClient mailClient = testMode == "1" ? new SmtpClient(smtpServer, 587) : new SmtpClient(smtpServer, 25);
            mailClient.EnableSsl = testMode == "1" ? true : false;
            mailClient.UseDefaultCredentials = false;
            mailClient.Credentials = cred;
            try
            {
                if (testMode == "1")
                {
                    using (MailMessage mailMessage = new MailMessage(new MailAddress(mailFrom, "xKubuni™"), new MailAddress(mailTo)))
                    {
                        mailMessage.Subject = msg.Subject;
                        mailMessage.Body = msg.Body;
                        mailMessage.IsBodyHtml = msg.IsBodyHtml;

                        if (msg.Bcc != null && msg.Bcc.Count > 0)
                        {
                            foreach (var item in msg.Bcc)
                            {
                                mailMessage.Bcc.Add(item);
                            }
                        }
                        if (msg.CC != null && msg.CC.Count > 0)
                        {
                            foreach (var item in msg.CC)
                            {
                                mailMessage.CC.Add(item);
                            }
                        }

                        if (msg.Attachments != null && msg.Attachments.Count > 0)
                        {
                            AttachmentCollection attachment = msg.Attachments;
                            foreach (Attachment item in attachment)
                            {
                                mailMessage.Attachments.Add(item);
                            }
                        }
                        mailClient.Send(mailMessage);
                    }
                    //MailMessage mailMessage = new MailMessage(mailFrom, mailTo, msg.Subject, msg.Body);
                    //mailMessage.IsBodyHtml = msg.IsBodyHtml;
                    //if (msg.Attachments != null && msg.Attachments.Count > 0)
                    //{
                    //    AttachmentCollection attachment = msg.Attachments;
                    //    foreach (Attachment item in attachment)
                    //    {
                    //        mailMessage.Attachments.Add(item);
                    //    }
                    //}
                    //mailClient.Send(mailMessage);
                }
                else
                {
                    using (MailMessage mailMessage = new MailMessage(new MailAddress(mailFrom, "xKubuni™"), new MailAddress(mailTo)))
                    {
                        mailMessage.Subject = msg.Subject;
                        mailMessage.Body = msg.Body;
                        mailMessage.IsBodyHtml = msg.IsBodyHtml;

                        if (msg.Bcc != null && msg.Bcc.Count > 0)
                        {
                            foreach (var item in msg.Bcc)
                            {
                                mailMessage.Bcc.Add(item);
                            }
                        }

                        if (msg.CC != null && msg.CC.Count > 0)
                        {
                            foreach (var item in msg.CC)
                            {
                                mailMessage.CC.Add(item);
                            }
                        }

                        if (msg.Attachments != null && msg.Attachments.Count > 0)
                        {
                            AttachmentCollection attachment = msg.Attachments;
                            foreach (Attachment item in attachment)
                            {
                                mailMessage.Attachments.Add(item);
                            }
                        }
                        mailClient.Send(mailMessage);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}