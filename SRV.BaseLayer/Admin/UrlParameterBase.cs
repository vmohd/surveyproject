﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SRV.BaseLayer.Admin
{
    public class UrlParameterBase
    {
        #region Declaration

        private int _id = 0;
        private string _parameterName = string.Empty;
        private string _createdDate = string.Empty;
        private int _displayOrder = 0;

        #endregion

        #region Properties

        public int Id { get { return _id; } set { _id = value; } }
        public string ParameterName { get { return _parameterName; } set { _parameterName = value; } }
        public string CreatedDate { get { return _createdDate; } set { _createdDate = value; } }
        public int DisplayOrder { get { return _displayOrder; } set { _displayOrder = value; } }

        #endregion
    }
}
