﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SRV.BaseLayer.Admin
{
    public class AdminBase
    {
        #region Declarations
        private int _id = 0;
        private int _roleId;
        private int _projectId = 0;
        private string _userGuid = string.Empty;
        private string _firstName = string.Empty;
        private string _lastName = string.Empty;
        private string _email = string.Empty;
        private bool _gender = false;
        private bool _isActive = true;
        private DateTime _createdOn = DateTime.Now;
        private DateTime _modifiedOn = DateTime.Now;
        private string _address = string.Empty;
        private string _city = string.Empty;
        private int _countryId = 0;
        private int _stateId = 0;
        private string _zipCode = string.Empty;
        private string _password = string.Empty;
        private string _phoneNumber;
        private string _companyName;
        private int _languageId;
        private int _countryOfResidenceId;
        private string _answerToSecurityQuestion;
        private string _securityQuestion;
        private bool _otherCommunications;
        private bool _sendEmail;
        #endregion

        #region properties
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public int RoleId
        {
            get { return _roleId; }
            set { _roleId = value; }
        }
        public int ProjectId
        {
            get { return _projectId; }
            set { _projectId = value; }
        }
        public string UserGuid
        {
            get { return _userGuid; }
            set { _userGuid = value; }
        }
        public string FirstName
        {
            get { return _firstName; }
            set { _firstName = value; }
        }
        public string LastName
        {
            get { return _lastName; }
            set { _lastName = value; }
        }
        public string Email
        {
            get { return _email; }
            set { _email = value; }
        }
        public bool Gender
        {
            get { return _gender; }
            set { _gender = value; }
        }

        public DateTime CreatedOn
        {
            get { return _createdOn; }
            set { _createdOn = value; }
        }
        public DateTime ModifiedOn
        {
            get { return _modifiedOn; }
            set { _modifiedOn = value; }
        }

        public string Address
        {
            get { return _address; }
            set { _address = value; }
        }
        public string City
        {
            get { return _city; }
            set { _city = value; }
        }
        public string ZipCode
        {
            get { return _zipCode; }
            set { _zipCode = value; }
        }
        public int StateId
        {
            get { return _stateId; }
            set { _stateId = value; }
        }
        public int CountryId
        {
            get { return _countryId; }
            set { _countryId = value; }
        }
        public string Password
        {
            get { return _password; }
            set { _password = value; }
        }
        public bool isActive
        {
            get { return _isActive; }
            set { _isActive = value; }
        }
        public string PhoneNumber
        {
            get { return _phoneNumber; }
            set { _phoneNumber = value; }
        }

        public string CompanyName
        {
            get { return _companyName; }
            set { _companyName = value; }
        }

        public int LanguageId
        {
            get { return _languageId; }
            set { _languageId = value; }
        }

        public int CountryOfResidenceId
        {
            get { return _countryOfResidenceId; }
            set { _countryOfResidenceId = value; }
        }

        public string AnswerToSecurityQuestion
        {
            get { return _answerToSecurityQuestion; }
            set { _answerToSecurityQuestion = value; }
        }

        public string SecurityQuestion
        {
            get { return _securityQuestion; }
            set { _securityQuestion = value; }
        }

        public bool OtherCommunications
        {
            get { return _otherCommunications; }
            set { _otherCommunications = value; }
        }

        public bool SendEmail
        {
            get { return _sendEmail; }
            set { _sendEmail = value; }
        }
        #endregion

        public int projectType { get; set; }
    }

    public class ProjectBase
    {
        #region Declarations
        private int _id = 0;
        private int _superProjectId = 0;
        private int _languageId = 0;
        private string _projectName = string.Empty;
        private string _welcomeMessage = string.Empty;

        private string _pageHeader = string.Empty;
        private string _pageFooter = string.Empty;
        private string _projectCompletionMesssage = string.Empty;
        private string _redirectUrl = string.Empty;
        private string _invitationMessage = string.Empty;
        private string _projectTerminationMsg = string.Empty;

        private int _ownerId = 0;
        private int _createdBy = 0;
        private int _modifiedBy = 0;
        private bool _isActive = false;
        private int _projectQuota = 0;

        private int _projectType = 0;
        private string _productProjectXML = string.Empty;

        private string _superProjectName = string.Empty;
        private string _superProjectOpeningDate = string.Empty;
        private string _superProjectClosingDate = string.Empty;
        private bool _superProjectIsRandomizeSurveys = false;
        private string _displayOrderXml = string.Empty;
        private bool _isIncludePageHeader = false;
        private bool _isIncludePageFooter = false;
        private bool _isIncludeWelcomeMessage = false;
        private bool _isIncludeProjectCompletionMesssage = false;
        private bool _isIncludeTerminationMessage = false;
        // private string _caseXml = string.Empty;
        private DataTable _caseTable = new DataTable();
        //  private string _productMessage = string.Empty;
        private string _redirectParameters = string.Empty;
        private bool _isRedirectParameter = false;
        private bool _isIncludeFinishImage = false;
        private string _panelXml = string.Empty;
        private string _closeButtonText = string.Empty;
        #endregion

        #region Properties

        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }
        public int SuperProjectId
        {
            get { return _superProjectId; }
            set { _superProjectId = value; }
        }
        public int LanguageId
        {
            get { return _languageId; }
            set { _languageId = value; }
        }
        public string ProjectName
        {
            get { return _projectName; }
            set { _projectName = value; }
        }
        public string WelcomeMessage
        {
            get { return _welcomeMessage; }
            set { _welcomeMessage = value; }
        }
        public int OwnerId
        {
            get { return _ownerId; }
            set { _ownerId = value; }
        }
        public int CreatedBy
        {
            get { return _createdBy; }
            set { _createdBy = value; }
        }
        public int ModifiedBy
        {
            get { return _modifiedBy; }
            set { _modifiedBy = value; }
        }
        public bool isActive
        {
            get { return _isActive; }
            set { _isActive = value; }
        }
        public int ProjectQuota
        {
            get { return _projectQuota; }
            set { _projectQuota = value; }
        }
        public string PageHeader
        {
            get { return _pageHeader; }
            set { _pageHeader = value; }
        }
        public string PageFooter
        {
            get { return _pageFooter; }
            set { _pageFooter = value; }
        }
        public string ProjectCompletionMesssage
        {
            get { return _projectCompletionMesssage; }
            set { _projectCompletionMesssage = value; }
        }
        public string RedirectUrl
        {
            get { return _redirectUrl; }
            set { _redirectUrl = value; }
        }
        public string InvitationMessage
        {
            get { return _invitationMessage; }
            set { _invitationMessage = value; }
        }
        public string ProjectTerminationMessage
        {
            get { return _projectTerminationMsg; }
            set { _projectTerminationMsg = value; }
        }

        public int ProjectType
        {
            get { return _projectType; }
            set { _projectType = value; }
        }
        public string ProductProjectXML
        {
            get { return _productProjectXML; }
            set { _productProjectXML = value; }
        }
        public string SuperProjectName
        {
            get { return _superProjectName; }
            set { _superProjectName = value; }
        }
        public string SuperProjectOpeningDate
        {
            get { return _superProjectOpeningDate; }
            set { _superProjectOpeningDate = value; }
        }
        public string SuperProjectClosingDate
        {
            get { return _superProjectClosingDate; }
            set { _superProjectClosingDate = value; }
        }
        public bool SuperProjectIsRandomizeSurveys
        {
            get { return _superProjectIsRandomizeSurveys; }
            set { _superProjectIsRandomizeSurveys = value; }
        }
        public string DisplayOrderXml
        {
            get { return _displayOrderXml; }
            set { _displayOrderXml = value; }
        }
        public bool IsIncludePageHeader
        {
            get { return _isIncludePageHeader; }
            set { _isIncludePageHeader = value; }
        }
        public bool IsIncludePageFooter
        {
            get { return _isIncludePageFooter; }
            set { _isIncludePageFooter = value; }
        }
        public bool IsIncludeWelcomeMessage
        {
            get { return _isIncludeWelcomeMessage; }
            set { _isIncludeWelcomeMessage = value; }
        }
        public bool IsIncludeProjectCompletionMesssage
        {
            get { return _isIncludeProjectCompletionMesssage; }
            set { _isIncludeProjectCompletionMesssage = value; }
        }
        public bool IsIncludeTerminationMessage
        {
            get { return _isIncludeTerminationMessage; }
            set { _isIncludeTerminationMessage = value; }
        }
        public DataTable CaseTable
        {
            get { return _caseTable; }
            set { _caseTable = value; }
        }
        public string RedirectParameters
        {
            get { return _redirectParameters; }
            set { _redirectParameters = value; }
        }
        public bool IsRedirectParameter
        {
            get { return _isRedirectParameter; }
            set { _isRedirectParameter = value; }
        }
        public bool IsIncludeFinishImage
        {
            get { return _isIncludeFinishImage; }
            set { _isIncludeFinishImage = value; }
        }
        //public string CaseXml
        //{
        //    get { return _caseXml; }
        //    set { _caseXml = value; }
        //}
        //public string ProductMessage
        //{
        //    get { return _productMessage; }
        //    set { _productMessage = value; }
        //}

        public string PanelXml
        {
            get { return _panelXml; }
            set { _panelXml = value; }
        }
        public string CloseButtonText
        {
            get { return _closeButtonText; }
            set { _closeButtonText = value; }
        }

        public DataTable ClusterTable
        {
            get; set;
        }
        public string ReportType
        {
            get; set;
        }

        public string NotParticipateProjectName { get; set; }
        public string NotParticipateRespondent { get; set; }

        #endregion
    }

    public class ProjectSettingsBase
    {
        #region Declaration
        private int _id;
        private int _projectId;
        private string _openingDate;
        private string _closingDate;
        private bool _disableQuestionNumbering;
        private bool _isActive;
        private bool _scored;
        private bool _testMode;
        private bool _prevNextPageNavigation;
        private bool _resumeOfProgress;
        private bool _isQuesRandomize = false;
        private bool _isProductRandomize = false;
        #endregion

        #region Properties
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public int ProjectId
        {
            get { return _projectId; }
            set { _projectId = value; }
        }

        public string OpeningDate
        {
            get { return _openingDate; }
            set { _openingDate = value; }
        }
        public string ClosingDate
        {
            get { return _closingDate; }
            set { _closingDate = value; }
        }

        public bool DisableQuestionNumbering
        {
            get { return _disableQuestionNumbering; }
            set { _disableQuestionNumbering = value; }
        }

        public bool IsActive
        {
            get { return _isActive; }
            set { _isActive = value; }
        }
        public bool Scored
        {
            get { return _scored; }
            set { _scored = value; }
        }
        public bool TestMode
        {
            get { return _testMode; }
            set { _testMode = value; }
        }
        public bool PrevNextPageNavigation
        {
            get { return _prevNextPageNavigation; }
            set { _prevNextPageNavigation = value; }
        }
        public bool ResumeOfProgress
        {
            get { return _resumeOfProgress; }
            set { _resumeOfProgress = value; }
        }
        public bool IsQuesRandomize
        {
            get { return _isQuesRandomize; }
            set { _isQuesRandomize = value; }
        }
        public bool IsProductRandomize
        {
            get { return _isProductRandomize; }
            set { _isProductRandomize = value; }
        }
        #endregion
    }

    public class ProjecQuotaBase
    {
        #region Declarations
        private int _id = 0;
        private int _maxCount = 0;
        private int _projectId = 0;
        private int _projectQuota = 0;
        private int _createdBy = 0;
        private int _modifiedBy = 0;
        private bool _isActive = true;
        private string _openXml = string.Empty;
        #endregion

        #region Properties

        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }


        public int MaxCount
        {
            get { return _maxCount; }
            set { _maxCount = value; }
        }

        public int ProjectId
        {
            get { return _projectId; }
            set { _projectId = value; }
        }
        public int ProjectQuota
        {
            get { return _projectQuota; }
            set { _projectQuota = value; }
        }
        public int CreatedBy
        {
            get { return _createdBy; }
            set { _createdBy = value; }
        }

        public int ModifiedBy
        {
            get { return _modifiedBy; }
            set { _modifiedBy = value; }
        }

        public bool isActive
        {
            get { return _isActive; }
            set { _isActive = value; }
        }
        public string OpenXml
        {
            get { return _openXml; }
            set { _openXml = value; }
        }

        public string QuotaQues { get; set; }
        #endregion
    }

    public class QuestionBase
    {
        #region Declarations
        private int _id = 0;
        private int _projectId = 0;
        private string _questionTitle = string.Empty;
        private int _questionTypeId = 0;
        private string _optionTitle = string.Empty;
        private decimal _constantSum = 0;
        private decimal _rangeMin = 0;
        private decimal _rangeMax = 0;
        private int _createdBy = 0;
        private int _modifiedBy = 0;
        private string _xml = string.Empty;
        private int _qusetionId = 0;
        private string _columnName = string.Empty;
        private string _columnXml = string.Empty;
        private string _type = string.Empty;
        private string _redirectUrl = string.Empty;
        private bool _isMatrix = false;
        private string _optionsCaption = string.Empty;
        //private string _optionsMode = string.Empty;
        private string _ddlXML = string.Empty;
        private string _action = string.Empty;
        private bool _isAnswersPiped = false;
        private bool _isAnswersExplicating = false;
        private int _pipedFrom = 0;
        private int _explicatingFrom = 0;
        private string _questionTag = string.Empty;
        private string _dispOrderXml = string.Empty;
        private bool _isOptRandomize = false;
        private string _redirectPassCode = string.Empty;
        private string _redirectQuesParameters = string.Empty;
        private bool _isRedirectParameter = false;
        #endregion

        #region Properties

        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public int ProjectId
        {
            get { return _projectId; }
            set { _projectId = value; }
        }

        public int QusetionId
        {
            get { return _qusetionId; }
            set { _qusetionId = value; }
        }


        public string QuestionTitle
        {
            get { return _questionTitle; }
            set { _questionTitle = value; }
        }
        public int QuestionTypeId
        {
            get { return _questionTypeId; }
            set { _questionTypeId = value; }
        }
        public string OptionTitle
        {
            get { return _optionTitle; }
            set { _optionTitle = value; }
        }
        public decimal ConstantSum
        {
            get { return _constantSum; }
            set { _constantSum = value; }
        }
        public decimal RangeMin
        {
            get { return _rangeMin; }
            set { _rangeMin = value; }
        }
        public decimal RangeMax
        {
            get { return _rangeMax; }
            set { _rangeMax = value; }
        }
        public int CreatedBy
        {
            get { return _createdBy; }
            set { _createdBy = value; }
        }

        public int ModifiedBy
        {
            get { return _modifiedBy; }
            set { _modifiedBy = value; }
        }
        public string Xml
        {
            get { return _xml; }
            set { _xml = value; }
        }
        public string ColumnXml
        {
            get { return _columnXml; }
            set { _columnXml = value; }
        }
        public string DispOrderXml
        {
            get { return _dispOrderXml; }
            set { _dispOrderXml = value; }
        }
        public string DDlXml
        {
            get { return _ddlXML; }
            set { _ddlXML = value; }
        }

        public string ColumnName
        {
            get { return _columnName; }
            set { _columnName = value; }
        }
        public string Action
        {
            get { return _action; }
            set { _action = value; }
        }
        public string Type
        {
            get { return _type; }
            set { _type = value; }
        }
        public string RedirectUrl
        {
            get { return _redirectUrl; }
            set { _redirectUrl = value; }
        }
        public bool IsMatrix
        {
            get { return _isMatrix; }
            set { _isMatrix = value; }
        }
        public string OptionsCaption
        {
            get { return _optionsCaption; }
            set { _optionsCaption = value; }
        }
        public bool IsRedirectParameter
        {
            get { return _isRedirectParameter; }
            set { _isRedirectParameter = value; }
        }
        //public string OptionsMode
        //{
        //    get { return _optionsMode; }
        //    set { _optionsMode = value; }
        //}

        public int PipedFrom { get { return _pipedFrom; } set { _pipedFrom = value; } }
        public int ExplicatingFrom { get { return _explicatingFrom; } set { _explicatingFrom = value; } }
        public bool isAnswersPiped { get { return _isAnswersPiped; } set { _isAnswersPiped = value; } }
        public bool isAnswersExplicating { get { return _isAnswersExplicating; } set { _isAnswersExplicating = value; } }

        public string QuestionTag
        {
            get { return _questionTag; }
            set { _questionTag = value; }
        }
        public bool IsOptRandomize
        {
            get { return _isOptRandomize; }
            set { _isOptRandomize = value; }
        }
        public string RedirectPassCode
        {
            get { return _redirectPassCode; }
            set { _redirectPassCode = value; }
        }

        public string RedirectQuesParameters
        {
            get { return _redirectQuesParameters; }
            set { _redirectQuesParameters = value; }
        }
        #endregion

    }

    public class UserInfoBase
    {
        #region Declarations
        private int _id = 0;
        private string _title = string.Empty;
        private string _firstName = string.Empty;
        private string _lastName = string.Empty;
        private string _email = string.Empty;
        private string _password = string.Empty;
        private bool _gender = false;
        private string _address = string.Empty;
        private string _city = string.Empty;
        private int _stateId = 0;
        private int _countryId = 0;
        private string _zipCode = string.Empty;
        private DateTime _createdOn = DateTime.Now;
        private DateTime _modifiedOn = DateTime.Now;
        private string _userGuid = string.Empty;
        #endregion

        #region Properties

        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public string Title
        {
            get { return _title; }
            set { _title = value; }
        }

        public string FirstName
        {
            get { return _firstName; }
            set { _firstName = value; }
        }

        public string LastName
        {
            get { return _lastName; }
            set { _lastName = value; }
        }

        public string Email
        {
            get { return _email; }
            set { _email = value; }
        }

        public string Password
        {
            get { return _password; }
            set { _password = value; }
        }

        public bool Gender
        {
            get { return _gender; }
            set { _gender = value; }
        }

        public string Address
        {
            get { return _address; }
            set { _address = value; }
        }

        public string City
        {
            get { return _city; }
            set { _city = value; }
        }

        public int StateId
        {
            get { return _stateId; }
            set { _stateId = value; }
        }

        public int CountryId
        {
            get { return _countryId; }
            set { _countryId = value; }
        }

        public string ZipCode
        {
            get { return _zipCode; }
            set { _zipCode = value; }
        }

        public DateTime CreatedOn
        {
            get { return _createdOn; }
            set { _createdOn = value; }
        }

        public DateTime ModifiedOn
        {
            get { return _modifiedOn; }
            set { _modifiedOn = value; }
        }
        public string UserGuid
        {
            get { return _userGuid; }
            set { _userGuid = value; }
        }

        #endregion
    }

    public class ProjectInvitationBase
    {
        #region Declaration
        private int _id = 0;
        private int _projectId = 0;
        private string _emailXml = string.Empty;
        private string _email = string.Empty;
        private string _invitationGuid = string.Empty;
        #endregion

        #region Properties
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }
        public int ProjectId
        {
            get { return _projectId; }
            set { _projectId = value; }
        }
        public string EmailXml
        {
            get { return _emailXml; }
            set { _emailXml = value; }
        }
        public string Email
        {
            get { return _email; }
            set { _email = value; }
        }
        public string InvitationGuid
        {
            get { return _invitationGuid; }
            set { _invitationGuid = value; }
        }
        #endregion
    }

    public class MatrixTypeBase
    {
        #region Declarations
        private int _id = 0;
        private int _questionId = 0;
        private int _groupId = 0;
        private string _columnName = string.Empty;
        private string _type = string.Empty;
        private int _displayOrder = 0;
        private bool _isActive = false;
        #endregion

        #region Properties

        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public int QuestionId
        {
            get { return _questionId; }
            set { _questionId = value; }
        }

        public int GroupId
        {
            get { return _groupId; }
            set { _groupId = value; }
        }

        public string ColumnName
        {
            get { return _columnName; }
            set { _columnName = value; }
        }

        public string Type
        {
            get { return _type; }
            set { _type = value; }
        }

        public int DisplayOrder
        {
            get { return _displayOrder; }
            set { _displayOrder = value; }
        }

        public bool IsActive
        {
            get { return _isActive; }
            set { _isActive = value; }
        }
        #endregion
    }

    public class ProjectQuestionGroupBase
    {
        #region Declarations
        private string _modifiedOn = string.Empty;
        private string _groupName = string.Empty;
        private string _groupDescription = string.Empty;
        private string _createdOn = string.Empty;
        private int _id = 0;
        private string _action = string.Empty;
        private int _projectId = 0;
        private string _groupXml = string.Empty;
        #endregion

        #region Properties
        public string ModifiedOn { get { return _modifiedOn; } set { _modifiedOn = value; } }
        public string GroupName { get { return _groupName; } set { _groupName = value; } }
        public string GroupDescription { get { return _groupDescription; } set { _groupDescription = value; } }
        public string CreatedOn { get { return _createdOn; } set { _createdOn = value; } }
        public int Id { get { return _id; } set { _id = value; } }
        public string Action { get { return _action; } set { _action = value; } }
        public int ProjectId { get { return _projectId; } set { _projectId = value; } }
        public string GroupXml { get { return _groupXml; } set { _groupXml = value; } }
        #endregion
    }

    public class ProductProjectBase
    {
        #region Declaration
        private int _id = 0;
        private int _projectId = 0;
        private int _productLogicalId = 0;
        private string _description = string.Empty;
        private int _displayOrder = 0;
        private string _createdDate = string.Empty;
        private string _modifiedDate = string.Empty;
        private string _xml = string.Empty;
        private string _productMessage = string.Empty;
        #endregion

        #region Properties
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }
        public int ProjectId
        {
            get { return _projectId; }
            set { _projectId = value; }
        }
        public int ProductLogicalId
        {
            get { return _productLogicalId; }
            set { _productLogicalId = value; }
        }
        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }
        public int DisplayOrder
        {
            get { return _displayOrder; }
            set { _displayOrder = value; }
        }
        public string CreatedDate
        {
            get { return _createdDate; }
            set { _createdDate = value; }
        }
        public string ModifiedDate
        {
            get { return _modifiedDate; }
            set { _modifiedDate = value; }
        }

        public string Xml
        {
            get { return _xml; }
            set { _xml = value; }
        }
        public string ProductMessage
        {
            get { return _productMessage; }
            set { _productMessage = value; }
        }


        #endregion
    }

    public class PositionTable
    {
        #region Declaration
        private string _responderId = string.Empty;
        private bool _isProcessed = false;
        private int _surveyId = 0;
        private int _productId = 0;
        private int _id = 0;
        private string _productsXML = string.Empty;
        private int _superSurveyId = 0;
        private int _surveyDispOrder;
        private int _productDispOrder;
        private string _orderXml;
        #endregion

        #region Properties
        public string ResponderId
        {
            get { return _responderId; }
            set { _responderId = value; }
        }
        public int SurveyId
        {
            get { return _surveyId; }
            set { _surveyId = value; }
        }
        public int ProductId
        {
            get { return _productId; }
            set { _productId = value; }
        }
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }
        public bool IsProcessed
        {
            get { return _isProcessed; }
            set { _isProcessed = value; }
        }
        public string ProductsXML
        {
            get { return _productsXML; }
            set { _productsXML = value; }
        }
        public int SuperSurveyId
        {
            get { return _superSurveyId; }
            set { _superSurveyId = value; }
        }

        public int SurveyDispOrder
        {
            get { return _surveyDispOrder; }
            set { _surveyDispOrder = value; }
        }
        public int ProductDispOrder
        {
            get { return _productDispOrder; }
            set { _productDispOrder = value; }
        }
        public string OrderXml
        {
            get { return _orderXml; }
            set { _orderXml = value; }
        }
        #endregion
    }

    public class GroupEmailMasterBase
    {
        #region Declaration
        private string _modifiedOn = string.Empty;
        private string _groupName = string.Empty;
        private string _createdOn = string.Empty;
        private int _userId = 0;
        private int _id = 0;
        private string _action = string.Empty;
        #endregion

        #region Properties
        public string ModifiedOn { get { return _modifiedOn; } set { _modifiedOn = value; } }
        public string GroupName { get { return _groupName; } set { _groupName = value; } }
        public string CreatedOn { get { return _createdOn; } set { _createdOn = value; } }
        public int UserId { get { return _userId; } set { _userId = value; } }
        public int Id { get { return _id; } set { _id = value; } }
        public string Action
        {
            get { return _action; }
            set { _action = value; }
        }
        #endregion
    }

    public class GroupEmailsBase
    {
        #region Declaration
        private string _modifiedOn = string.Empty;
        private string _email = string.Empty;
        private string _createdOn = string.Empty;
        private int _id = 0;
        private int _groupId = 0;
        private string _action = string.Empty;
        private string _xml = string.Empty;
        #endregion

        #region Properties
        public string ModifiedOn { get { return _modifiedOn; } set { _modifiedOn = value; } }
        public string Email { get { return _email; } set { _email = value; } }
        public string CreatedOn { get { return _createdOn; } set { _createdOn = value; } }
        public int Id { get { return _id; } set { _id = value; } }
        public int GroupId { get { return _groupId; } set { _groupId = value; } }
        public string Xml
        {
            get { return _xml; }
            set { _xml = value; }
        }

        public string Action
        {
            get { return _action; }
            set { _action = value; }
        }
        #endregion
    }

    public class UserDirectoriesBase
    {
        #region Properties
        private DateTime _modifiedDate = DateTime.Now;
        private string _dirName = string.Empty;
        private DateTime _createdDate = DateTime.Now;
        private int _userId = 0;
        private int _id = 0;
        #endregion

        #region Declaration
        public DateTime ModifiedDate { get { return _modifiedDate; } set { _modifiedDate = value; } }
        public string DirName { get { return _dirName; } set { _dirName = value; } }
        public DateTime CreatedDate { get { return _createdDate; } set { _createdDate = value; } }
        public int UserId { get { return _userId; } set { _userId = value; } }
        public int Id { get { return _id; } set { _id = value; } }
        #endregion
    }

    public class ProjectDirectoriesBase
    {
        #region Declaration
        private DateTime _modifiedDate = DateTime.Now;
        private DateTime _createdDate = DateTime.Now;
        private int _projectId = 0;
        private int _id = 0;
        private int _directoryId = 0;
        private string _xml = string.Empty;
        private string _directoryIdList = string.Empty;
        #endregion

        #region Properties
        public DateTime ModifiedDate { get { return _modifiedDate; } set { _modifiedDate = value; } }
        public DateTime CreatedDate { get { return _createdDate; } set { _createdDate = value; } }
        public int ProjectId { get { return _projectId; } set { _projectId = value; } }
        public int Id { get { return _id; } set { _id = value; } }
        public int DirectoryId { get { return _directoryId; } set { _directoryId = value; } }
        public string Xml { get { return _xml; } set { _xml = value; } }
        public string DirectoryIdList { get { return _directoryIdList; } set { _directoryIdList = value; } }
        #endregion
    }

    public class QueryBase
    {
        #region Declarations
       
        private string _query = string.Empty;
        private string _caseName = string.Empty;
        private int _projectId = 0;
        private int _caseId = 0;
        private int _superProjectId = 0;
        private string _queryHtml = string.Empty;
      
        #endregion

        #region Properties

       

        public int ProjectId
        {
            get { return _projectId; }
            set { _projectId = value; }
        }

        public int SuperProjectId
        {
            get { return _superProjectId; }
            set { _superProjectId = value; }
        }

        public string CaseName
        {
            get { return _caseName; }
            set { _caseName = value; }
        }

        public string Query
        {
            get { return _query; }
            set { _query = value; }
        }

        public string QueryHtml
        {
            get { return _queryHtml; }
            set { _queryHtml = value; }
        }

        public int CaseId
        {
            get { return _caseId; }
            set { _caseId = value; }
        }
        #endregion

    }
}
