﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SRV.BaseLayer.SuperAdmin
{
    public class CreateRoleBase
    {
        #region Declarations
        private int _roleId;
        private int _userId;
        private string _roleXml;
        #endregion

        #region Properties
        public int RoleId
        {
            get { return _roleId; }
            set { _roleId = value; }
        }
        public int UserId
        {
            get { return _userId; }
            set { _userId = value; }
        }
        public string RoleXml
        {
            get { return _roleXml; }
            set { _roleXml = value; }
        }
        #endregion
    }
}
