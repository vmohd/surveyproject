﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SRV.BaseLayer.Account
{
    public class user_AppInfoBase
    {
        #region Declaration

        private long _id = 0;
        private long _UserId = 0;
        private long _appId = 0;

        #endregion

        #region Properties

        public long Id { get { return _id; } set { _id = value; } }
        public long UserId { get { return _UserId; } set { _UserId = value; } }
        public long appId { get { return _appId; } set { _appId = value; } }
        #endregion
    }
    public class appInfoBase
    {
        public long appId { get; set; }
        public string appName { get; set; }
        public string appImage { get; set; }
    }
}
