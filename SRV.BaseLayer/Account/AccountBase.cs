﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SRV.BaseLayer.Account
{
    public class AccountBase
    {

        #region Declarations
        private int _id = 0;
        private string _email = string.Empty;
        private string _password = string.Empty;
        private string _userGuid = string.Empty;
        #endregion

        #region properties
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public string Email
        {
            get { return _email; }
            set { _email = value; }
        }

        public string Password
        {
            get { return _password; }
            set { _password = value; }
        }

        public string UserGuid
        {
            get { return _userGuid; }
            set { _userGuid = value; }
        }
        #endregion
    }
}
