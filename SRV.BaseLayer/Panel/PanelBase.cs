﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SRV.BaseLayer.Panel
{
   public class PanelBase
   {
       #region Declaration
       private string _modifiedOn = string.Empty;
       private string _createdOn = string.Empty;
       private int _projectId = 0;
       private int _panelId = 0;
       private int _id = 0;
       #endregion

       #region Properties
       public string ModifiedOn { get { return _modifiedOn; } set { _modifiedOn = value; } }
       public string CreatedOn { get { return _createdOn; } set { _createdOn = value; } }
       public int ProjectId { get { return _projectId; } set { _projectId = value; } }
       public int PanelId { get { return _panelId; } set { _panelId = value; } }
       public int Id { get { return _id; } set { _id = value; } }
       #endregion

   }
}
