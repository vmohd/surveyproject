﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SRV.BaseLayer.Respondent
{
    public class RespondentBase
    {
        #region Declaration
        private int _id = 0;
        #endregion

        #region Properties
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }
        #endregion
    }

    public class SurveyLoadBase
    {
        #region Declaration

        private string _guid = string.Empty;
        private int _id = 0;
        private int _productId = 0;
        private string _invitationId = string.Empty;
        private string _emial = string.Empty;
        private string _offlineResponderId = string.Empty;
        private bool _isQuesRandomize = false;
        private string _externalResponderId = string.Empty;
        #endregion

        #region Properties

        public string GUID
        {
            get { return _guid; }
            set { _guid = value; }
        }
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public int ProductId
        {
            get { return _productId; }
            set { _productId = value; }
        }


        public string InvitationId
        {
            get { return _invitationId; }
            set { _invitationId = value; }
        }
        public string Email
        {
            get { return _emial; }
            set { _emial = value; }
        }
        public string OfflineResponderId
        {
            get { return _offlineResponderId; }
            set { _offlineResponderId = value; }
        }
        public string ExternalResponderId
        {
            get { return _externalResponderId; }
            set { _externalResponderId = value; }
        }
        public bool IsQuesRandomize
        {
            get { return _isQuesRandomize; }
            set { _isQuesRandomize = value; }
        }

        public int superProjectId { get; set; }
        #endregion
    }

    public class SurveyQuestionBase
    {
        #region Declaration
        private int _id = 0;
        private bool _isOptRandomize = false;
        #endregion

        #region Properties
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }
        public bool IsOptRandomize
        {
            get { return _isOptRandomize; }
            set { _isOptRandomize = value; }
        }
        #endregion
    }

    public class SurveyResponseBase
    {
        #region Declaration
        
        private string _email = string.Empty;
        private string _xml = string.Empty;
        private int _quotaId = 0;
        private int _projectId = 0;
        private string _responsesCountXml = string.Empty;
        private string _responderId = string.Empty;
        private string _mode = string.Empty;
        private int _productId = 0;
        private int _positionTblId = 0;
        private string _respList = string.Empty;
        private string _pnlId = string.Empty;
        private bool _isExternal = false;
        private string _externalResponderId = string.Empty;
        private string _terminate = string.Empty;
        #endregion

        #region Properties
        public string Email
        {
            get { return _email; }
            set { _email = value; }
        }

        public string ResponsesCountXml
        {
            get { return _responsesCountXml; }
            set { _responsesCountXml = value; }
        }

        public string Xml
        {
            get { return _xml; }
            set { _xml = value; }
        }

        public int QuotaId
        {
            get { return _quotaId; }
            set { _quotaId = value; }
        }
        public int ProjectId
        {
            get { return _projectId; }
            set { _projectId = value; }
        }
        public string ResponderId
        {
            get { return _responderId; }
            set { _responderId = value; }
        }
        public string Mode
        {
            get { return _mode; }
            set { _mode = value; }
        }
        public int ProductId
        {
            get { return _productId; }
            set { _productId = value; }
        }
        public int PositionTblId
        {
            get { return _positionTblId; }
            set { _positionTblId = value; }
        }
        public string RespList
        {
            get { return _respList; }
            set { _respList = value; }
        }
        public string PnlId {
            get { return _pnlId; }
            set { _pnlId = value; }
        }

        public bool IsExternal
        {
            get { return _isExternal; }
            set { _isExternal = value; }
        }
        public string ExternalResponderId
        {
            get { return _externalResponderId; }
            set { _externalResponderId = value; }
        }
        public string Terminate
        {
            get { return _terminate; }
            set { _terminate = value; }
        }
        #endregion
    }

    public class LanguageBase
    {
        #region Declaration
        
        private int _projectId=0;
        private int _id=0;
        private string _name=string.Empty;
        private string _abbreviation=string.Empty;

        #endregion

        #region Properties
        public int ProjectId
        {
            get { return _projectId; }
            set { _projectId = value; }
        }

        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public string Abbreviation
        {
            get { return _abbreviation; }
            set { _abbreviation = value; }
        }

        #endregion
    }
}
