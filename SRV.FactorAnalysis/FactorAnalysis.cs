﻿using Newtonsoft.Json;
using RDotNet;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SRV.FactorAnalysis
{
    public class FactorAnalysis
    {
        static void Main(String[] args)
        {
            string FactorAnalysisDrivePath = "";
            if (args != null && args.Length > 0)
            {
                FactorAnalysisDrivePath = args[0];
                if (args[1] == "PeakValidation")
                {
                    GetPVResult(FactorAnalysisDrivePath);
                }
                if (args[1] == "FactorAnalysis")
                {
                    GetFAResult(FactorAnalysisDrivePath);
                }
            }

        }
        public static void GetFAResult(string FactorAnalysisDrivePath)
        {
            REngine.SetEnvironmentVariables();
            REngine engine = REngine.GetInstance();

            try
            {
               // FactorAnalysisDrivePath = "E:\\TeamServer\\Survey\\xKubuni\\SurveyProject\\SRV.FactorAnalysis\\bin\\Debug";
                DataTable data = GetDataTableFromCsv(FactorAnalysisDrivePath + "\\peakMatrix.csv", true);
                DataTable dataToProcess = data.Copy();
                dataToProcess.Columns.RemoveAt(0);
                int[] ResponderArray = new int[dataToProcess.Rows.Count];
                double[] dataArray = new double[dataToProcess.Columns.Count * dataToProcess.Rows.Count];
                int row = 0;
                for (int drow = 0; drow < dataToProcess.Rows.Count; drow++)
                {

                    for (int dcol = 0; dcol < dataToProcess.Columns.Count; dcol++)
                        dataArray[row++] = dataToProcess.Rows[drow][dcol] != DBNull.Value ? Convert.ToDouble(dataToProcess.Rows[drow][dcol]) : 0;
                }

                for (int drow = 0; drow < data.Rows.Count; drow++)
                {
                    ResponderArray[drow] = data.Rows[drow][0] != DBNull.Value ? Convert.ToInt32(data.Rows[drow][0]) : 0;

                }

                IntegerVector responders = engine.CreateIntegerVector(ResponderArray);
                engine.SetSymbol("responders", responders);
                NumericVector group = engine.CreateNumericVector(dataArray);
                engine.SetSymbol("group", group);
                engine.Evaluate("mat=matrix(group,ncol=" + dataToProcess.Columns.Count + ",nrow=" + dataToProcess.Rows.Count + ",byrow = TRUE)");
                engine.Evaluate("dataMat = round(mat, 2)");
                engine.Evaluate("library(\"psych\")");
                engine.Evaluate("rotatedmatrix <- principal(dataMat, nfactors = 2, rotate = \"varimax\", scores = TRUE)");
                engine.Evaluate("loadings <- as.data.frame(unclass(rotatedmatrix$loadings))");
                engine.Evaluate("result=rotatedmatrix$scores");
                //  engine.Evaluate("result=(round(result, 2))");


                //For Hierarchical clustering
                engine.Evaluate("hc <- hclust(dist(cor(t(result))), method='average')");
                engine.Evaluate("groups <- cutree(hc, k=2:3)");

                engine.Evaluate("resp <- cbind(responders,groups)");


                // For segmentation report
                engine.Evaluate("dataMat = round(mat, 2)");
                engine.Evaluate("indep1<-c(groups[,1])");// independent variable 1
                engine.Evaluate("seg_1_of_2<-length(which(indep1==1))");//length of segment 1 of independent variable 1

                engine.Evaluate("seg_2_of_2<-length(which(indep1==2))");//length of segment 2 of independent variable 1

                engine.Evaluate("indep2<-c(groups[,2])");// independent variable 2
                engine.Evaluate("seg_1_of_3<-length(which(indep2==1))");//length of segment 1 of independent variable 2

                engine.Evaluate("seg_2_of_3<-length(which(indep2==2))");//length of segment 2 of independent variable 2

                engine.Evaluate("seg_3_of_3<-length(which(indep2==3))");//length of segment 3 of independent variable 2




                //mean with se of  dependent variables grouped by segment 1 of independent variable 1
                engine.Evaluate("mean_of_seg_1_2<-(describe(head(dataMat,seg_1_of_2))) [,c('mean','se')]");
                //mean with se of  dependent variables grouped by segment 2 of independent variable 1
                engine.Evaluate("mean_of_seg_2_2<-(describe(dataMat[(seg_1_of_2+1):(seg_1_of_2+seg_2_of_2),])[,c('mean','se')]) ");
                //mean with se of  dependent variables grouped by segment 1 of independent variable 2
                engine.Evaluate("mean_of_seg_1_3<-(describe(head(dataMat,seg_1_of_3))[,c('mean','se')]) ");
                //mean with se of  dependent variables grouped by segment 2 of independent variable 2
                engine.Evaluate("mean_of_seg_2_3<-(describe(dataMat[(seg_1_of_3+1):(seg_1_of_3+seg_2_of_3),])[,c('mean','se')]) ");
                //mean with se of  dependent variables grouped by segment 3 of independent variable 2

                StringBuilder sb = new StringBuilder();
                sb.Append(@"if((seg_1_of_3+seg_2_of_3+seg_3_of_3)==(seg_1_of_3+seg_2_of_3+1)) mean_of_seg_3_3<-(dataMat[(seg_1_of_3+seg_2_of_3+1),])  else mean_of_seg_3_3<-(describe(dataMat[(seg_1_of_3+seg_2_of_3+1):(seg_1_of_3+seg_2_of_3+seg_3_of_3),])[,c('mean','se')]) 
                        ");
                engine.Evaluate(sb.ToString());


                engine.Evaluate("write.table('SEG 1 OF 2,SEG 2 OF 2,SEG 1 OF 3,SEG 2 OF 3,SEG 3 OF 3',col.names=FALSE,row.names=FALSE, quote = FALSE, sep = ', ') ");
                engine.Evaluate("write.table(paste(seg_1_of_2,seg_2_of_2,seg_1_of_3,seg_2_of_3,seg_3_of_3,sep=','),col.names=FALSE,row.names=FALSE, quote = FALSE, sep = ', ') ");
                engine.Evaluate("write.table('break',col.names=FALSE,row.names=FALSE, quote = FALSE, sep = ', ')");
                engine.Evaluate("write.table(round(mean_of_seg_1_2,2),col.names=FALSE, quote = FALSE, sep = ', ')");
                engine.Evaluate("write.table('break',col.names=FALSE,row.names=FALSE, quote = FALSE, sep = ', ')");
                engine.Evaluate("write.table(round(mean_of_seg_2_2,2),col.names=FALSE, quote = FALSE, sep = ',')");
                engine.Evaluate("write.table('break',col.names=FALSE,row.names=FALSE, quote = FALSE, sep = ', ')");
                engine.Evaluate(" write.table(round(mean_of_seg_1_3,2),col.names=FALSE, quote = FALSE, sep = ',')");
                engine.Evaluate("write.table('break',col.names=FALSE,row.names=FALSE, quote = FALSE, sep = ', ')");
                engine.Evaluate("write.table(round(mean_of_seg_2_3,2),col.names=FALSE, quote = FALSE, sep = ',')");
                engine.Evaluate("write.table('break',col.names=FALSE,row.names=FALSE, quote = FALSE, sep = ', ')");
                engine.Evaluate(" write.table(round(mean_of_seg_3_3,2),col.names=FALSE, quote = FALSE, sep = ',')");

                engine.Evaluate("write.table('last',col.names=FALSE,row.names=FALSE, quote = FALSE, sep = ', ')");

                engine.Evaluate("write.table(resp,col.names=FALSE,row.names=FALSE, quote = FALSE, sep = ',')");


                engine.Dispose();
            }
            catch (Exception)
            {
                Console.Clear();
                engine.Evaluate("print('error')");
            }

        }

        public static void GetPVResult(string FactorAnalysisDrivePath)
        {
            REngine.SetEnvironmentVariables();
            REngine engine = REngine.GetInstance();

           // FactorAnalysisDrivePath = "E:\\TeamServer\\Survey\\xKubuni\\SurveyProject\\SRV.FactorAnalysis\\bin\\Debug";
            string filepath = "\"" + FactorAnalysisDrivePath.Replace('\\', '/') + "/peakValidationMatrix.csv" + "\"";
            try
            {
                engine.Evaluate("MyData <- read.csv(file=" + filepath + ", header=TRUE)");
                engine.Evaluate("result <- aggregate(MyData[-1], list(MyData$pro_Id), mean)");
                engine.Evaluate("write.table(result,col.names=TRUE,row.names=FALSE, quote = FALSE, sep = ',')");
                engine.Dispose();
            }
            catch (Exception ex)
            {
                Console.Clear();
                engine.Evaluate("print('error')");
            }

        }
        public static DataTable GetDataTableFromCsv(string path, bool isFirstRowHeader)
        {
            string header = isFirstRowHeader ? "Yes" : "No";

            string pathOnly = Path.GetDirectoryName(path);
            string fileName = Path.GetFileName(path);

            string sql = @"SELECT * FROM [" + fileName + "]";

            using (OleDbConnection connection = new OleDbConnection(
                      @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + pathOnly +
                      ";Extended Properties=\"Text;HDR=" + header + "\""))
            using (OleDbCommand command = new OleDbCommand(sql, connection))
            using (OleDbDataAdapter adapter = new OleDbDataAdapter(command))
            {
                DataTable dataTable = new DataTable();
                dataTable.Locale = CultureInfo.CurrentCulture;
                adapter.Fill(dataTable);
                return dataTable;
            }
        }
    }
}
